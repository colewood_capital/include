//+------------------------------------------------------------------+
//|                                              mysql_connector.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#ifndef MQL4_MYSQL
#define MQL4_MYSQL
   #include <mql4-mysql.mqh>
#endif 

class MySQL
{
private:
 int id;

public:
 MySQL(string dbHost, string dbUser, string dbPass, string dbSchema, int port);
 ~MySQL();
};

//+------------------------------------------------------------------+
MySQL::MySQL(string dbHost,string dbUser,string dbPass,string dbSchema,int port)
{
  int dbConnectId = 0;
  int socket = 0;
  int client = 0;
  
  //--- Connect to DB 
   bool goodConnect = false;
   
   goodConnect = init_MySQL(dbConnectId, dbHost, dbUser, dbPass, dbSchema, port, socket, client);
   
   if(goodConnect) {
     Alert("Connect to MySQL: true");
     id = dbConnectId;
   }
   else {
     Alert("Connect to MySQL: false");
     id = -1;
   }
}
//+------------------------------------------------------------------+
MySQL::~MySQL(void)
{
  deinit_MySQL(id);
}