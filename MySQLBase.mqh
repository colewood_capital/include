//+------------------------------------------------------------------+
//|                                              mysql_connector.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#ifndef MQL4_MYSQL
#define MQL4_MYSQL
   #include <mql4-mysql.mqh>
#endif 

class MySQLBase
{
protected:
 int id;

public:
  bool query(string query);
  int fetch(string query, string& data[][]);
  MySQLBase(string dbHost, string dbUser, string dbPass, string dbSchema, int port);
 ~MySQLBase();
};

//+------------------------------------------------------------------+
MySQLBase::MySQLBase(string dbHost,string dbUser,string dbPass,string dbSchema,int port)
{
  int dbConnectId = 0;
  int socket = 0;
  int client = 0;
  
  //--- Connect to DB 
   bool goodConnect = false;
   
   goodConnect = init_MySQL(dbConnectId, dbHost, dbUser, dbPass, dbSchema, port, socket, client);
   
   if(goodConnect) {
     Alert("Connect to MySQL: true");
     id = dbConnectId;
   }
   else {
     Alert("Connect to MySQL: false");
     id = -1;
   }
}
//+------------------------------------------------------------------+
MySQLBase::~MySQLBase(void)
{
  deinit_MySQL(id);
}
//+------------------------------------------------------------------+
int MySQLBase::fetch(string query, string& data[][])
{ 
  return MySQL_FetchArray(id, query, data);
}
//+------------------------------------------------------------------+
bool MySQLBase::query(string query)
{
  return MySQL_Query(id, query);
}
//+------------------------------------------------------------------+
string InsertQueryBuilder(string table,string &col[],string &val[])
{
  int colSize = ArraySize(col);
  int valSize = ArraySize(val);
  
  if(colSize != valSize)
  {
   return("error: column and value array sizes do not match");
  }
  
  string columns = "(";
  
  for(int i=0; i<colSize - 1; i++)
  {
   columns += "`" + col[i] + "`, ";
  }
   
  columns += "`" + col[colSize-1] + "`)";
  
  string values = "(";
  
  for(int i=0; i<valSize - 1; i++)
  {
   values += "" + val[i] + ", ";
  }
  
  values += "" + val[valSize-1] + ")";
  
  string query = "INSERT INTO `" + table + "` " + columns + " VALUES " + values + ";";
  
  return(query);
}
//+------------------------------------------------------------------+
string UpdateQueryBuilder(string table,string &col[],string &val[], string constraint)
{
  int colSize = ArraySize(col);
  int valSize = ArraySize(val);
  
  if(colSize != valSize)
  {
   return("error: column and value array sizes do not match");
  }
  
  string query = "UPDATE `" + table + "` SET ";
  
  for(int i=0; i<colSize - 1; i++)
  {
   query += "`" + col[i] + "` = " + val[i] + ", ";
  } 
  
  query += "`" + col[colSize-1] + "` = " + val[valSize-1] + " WHERE " + constraint + ";";
  
  return(query);
}