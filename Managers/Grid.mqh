//+------------------------------------------------------------------+
//|                                                         Grid.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#ifndef MATH
#define MATH
   #include <Packages/Math.mqh>
#endif 

#ifndef DRAW
#define DRAW
   #include <Tasks/Draw.mqh>
#endif

#ifndef MAP
#define MAP
   #include <Map.mqh>
#endif

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
#endif 

Logger grid_logger(__FILE__, "helper_processes");

#define GRID_PREFIX           "Grid -"
#define GRID_UPPER_PREFIX     GRID_PREFIX + " Upper"
#define GRID_LOWER_PREFIX     GRID_PREFIX + " Lower"

enum ENUM_GRID_EVENT {
   GRID_EVENT_NIL,
   GRID_EVENT_REDRAW
};

enum ENUM_GRID_DIRECTION {
   GRID_DIRECTION_NIL,
   GRID_DIRECTION_UPPER,
   GRID_DIRECTION_LOWER
};

struct GridLevel
{
 double upper;
 double lower;
};

struct GridPoint
{
 ENUM_GRID_DIRECTION direction;
 int level;
 double price;
 
 GridPoint() : direction(GRID_DIRECTION_NIL), level(0), price (0) {};
};

class Grid
{
 private:
  long GetLowerRoundNumberFromInteger(long x);
  double p_grid_pips_length;
  double p_pip_redraw_buff;
  double p_standard_unit;
  color p_grid_color;
  CArrayDouble p_lower_prc_level;      //Used to store lower price levels for adjustments
  CArrayDouble p_upper_prc_level;      //Used to store upper price levels for adjustments
  Map<color, int> p_color_level;
 
 public:
  Grid(double, double, double, double, color);
  ~Grid();
  ENUM_GRID_EVENT engine_tick();
  GridPoint engine_chart_drag(string obj_name);
  bool get_price(int level, GridLevel& grid);
  bool bind(color clr, int level, ENUM_GRID_DIRECTION direction);
  bool unbind(color clr);
  double get_price(color clr);
};

double Grid::get_price(color clr)
{
 int level = p_color_level[clr];
 
 GridLevel prices;
 
 if(get_price(level, prices))
 {
  if(level > 0)
   return prices.upper;
   
  else if(level < 0)
   return prices.lower;

  else
   grid_logger.log("Returning zero: color=" + ColorToString(clr, true) + " not found in grid", LOG_WARNING, __LINE__, __FUNCTION__);
 }
 else
  grid_logger.log("Returning zero: unable to get price associated with grid level=" + (string)level + ", clr=" + ColorToString(clr, true), LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
  
 return 0; 
}

bool Grid::unbind(color clr)
{
 int level = p_color_level[clr];
 
 if(level != 0)
 {
  string name;
  
  if(level > 0)
   name = StringConcatenate(GRID_UPPER_PREFIX, " - ", MathAbs(level));
  else
   name = StringConcatenate(GRID_LOWER_PREFIX, " - ", MathAbs(level));
   
  if(ObjectSet(name, OBJPROP_COLOR, p_grid_color))
  {
   p_color_level.del(clr);
   return true;
  }
 
  grid_logger.log("unable to unbind color=" + ColorToString(clr, true) + " for e=" + (string)GetLastError(), LOG_WARNING, __LINE__, __FUNCTION__);
  return false;
 }
 else {
  grid_logger.log("attempt to unbind color=" + ColorToString(clr, true) + " that has not yet been binded", LOG_WARNING, __LINE__, __FUNCTION__);
  return false;
 }
}

bool Grid::bind(color clr,int level,ENUM_GRID_DIRECTION direction)
{
 GridLevel grid_level;
 
 if(clr==p_grid_color) {
  grid_logger.log("Cannot bind a level to main grid color", LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
  return false;
 }
 
 bool res = get_price(level, grid_level); 
 
 if(res)
 {
  double prc;
  string name;
  
  if(direction==GRID_DIRECTION_UPPER)
  {
   prc = grid_level.upper;
   name = StringConcatenate(GRID_UPPER_PREFIX, " - ", level);
  }
  else if(direction==GRID_DIRECTION_LOWER)
  {
   prc = grid_level.lower;
   name = StringConcatenate(GRID_LOWER_PREFIX, " - ", level);
  }
  else {
   grid_logger.log("Invalid grid direction = " + EnumToString(direction), LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
   return false;
  }
  
  if(ObjectSet(name, OBJPROP_COLOR, clr))
  {
   if(direction==GRID_DIRECTION_LOWER)
    level *= -1;
    
   p_color_level.set(clr, level);
   
   return true;
  }
  
   grid_logger.log("Could not set color for e=" + (string)GetLastError(), LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
   return false;
 }
 else {
  grid_logger.log("Unable to get grid level", LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
  return false;
 }
}

bool Grid::get_price(int level, GridLevel& gl)
{
 if(level < MathMin(p_upper_prc_level.Total(), p_lower_prc_level.Total()))
 {
  gl.lower = p_lower_prc_level[level-1];
  gl.upper = p_upper_prc_level[level-1];
  return true;
 }
 else
  return false;
}

ENUM_GRID_EVENT Grid::engine_tick()
{
 int last_element;
 double temp;
 int redraw=0;
 
 //-------------------------------------------
 int upper_index=0;
 int upper_total = p_upper_prc_level.Total();
 
 //--- Check if bid crosses upper level
 for(int i=0; i<upper_total; i++)
 {
  if(Bid < p_upper_prc_level[i] + p_pip_redraw_buff)
   break;
   
  upper_index++;
 }
 
 //--- If yes, then shift grid
 for(int i=0; i<upper_index; upper_index--)
 {
  //-- 1: Add new level to top of grid
   last_element = p_upper_prc_level.Total()-1;
   p_upper_prc_level.Add(p_upper_prc_level[last_element] + p_standard_unit);
  
  //-- 2: Save level to be removed from upper grid and moved to lower grid
   temp = p_upper_prc_level[i];
  
  //-- 3: Delete level from upper
   p_upper_prc_level.Delete(i);
   
  //-- 4: Add level to lower
   p_lower_prc_level.Insert(temp, 0);
   
  //-- 5: Remove level from bottom of grid
   last_element = p_lower_prc_level.Total()-1;
   p_lower_prc_level.Delete(last_element);
  
  //--
   redraw++;
 }
 
 if(redraw)
 {
  DrawGrid(GRID_LOWER_PREFIX, p_lower_prc_level);
  DrawGrid(GRID_UPPER_PREFIX, p_upper_prc_level);
  
  //--- Adjust color bindings
   color clr[];
   int lvl[];
   
   int size=p_color_level.copy(clr, lvl);
   
   for(int i=0; i<size; i++)
   {
    unbind(clr[i]);
    
    int new_level = (lvl[i]-redraw==0) ? -1 : lvl[i]-redraw;
    
    ENUM_GRID_DIRECTION dir = (new_level > 0) ? GRID_DIRECTION_UPPER : GRID_DIRECTION_LOWER;
    
    bind(clr[i], MathAbs(new_level), dir);
   }
  
  return GRID_EVENT_REDRAW;
 }
 
 //------------------------------------------
 int lower_index=0;
 int lower_total = p_lower_prc_level.Total();
 redraw = 0;
 
 //--- Check if bid crosses lower level
 for(int i=0; i<lower_total; i++)
 {
  //if(Bid - (p_lower_prc_level[i] - p_pip_redraw_buff) > Point)
  if(Bid > p_lower_prc_level[i] - p_pip_redraw_buff)
   break;
   
  lower_index++;
 }
 
 //--- If yes, then shift grid
 for(int i=0; i<lower_index; lower_index--)
 {
  //-- 1: Add new level to bottom of grid
   last_element = p_lower_prc_level.Total()-1;
   p_lower_prc_level.Add(p_lower_prc_level[last_element] - p_standard_unit);
  
  //-- 2: Save level to be removed from lower grid and moved to upper grid
   temp = p_lower_prc_level[i];
  
  //-- 3: Delete level from lower
   p_lower_prc_level.Delete(i);
   
  //-- 4: Add level to upper
   p_upper_prc_level.Insert(temp, 0);
   
  //-- 5: Remove level from top of grid
   last_element = p_upper_prc_level.Total()-1;
   p_upper_prc_level.Delete(last_element);
   
  //--
   redraw++;
 }
 
 if(redraw)
 {
  DrawGrid(GRID_LOWER_PREFIX, p_lower_prc_level);
  DrawGrid(GRID_UPPER_PREFIX, p_upper_prc_level);
  
  //--- Adjust color bindings
   color clr[];
   int lvl[];
   
   int size=p_color_level.copy(clr, lvl);
   
   for(int i=0; i<size; i++)
   {
    unbind(clr[i]);
    
    int new_level = (lvl[i]+redraw==0) ? 1 : lvl[i]+redraw;
    
    ENUM_GRID_DIRECTION dir = (new_level > 0) ? GRID_DIRECTION_UPPER : GRID_DIRECTION_LOWER;
    
    bind(clr[i], MathAbs(new_level), dir);
   }
   
  return GRID_EVENT_REDRAW;
 }
 
 return GRID_EVENT_NIL;
}

GridPoint Grid::engine_chart_drag(string obj_name)
{
 double new_price=NormalizeDouble(ObjectGet(obj_name, OBJPROP_PRICE1), Digits);
 string result[];
 GridPoint modified;
 
 StringSplit(obj_name, '-', result);
 
 if(ArraySize(result)==3)
 {
  string direction = StringTrimRight(StringTrimLeft(result[1]));
  StringToUpper(direction);
  
  int lvl = (int)result[2];
  
  if(direction=="LOWER") 
  {
   if(MathAbs(p_lower_prc_level[lvl-1] - new_price) > DBL_EPSILON)
   {
    p_lower_prc_level.Update(lvl-1, new_price);
    grid_logger.log("Changed lower grid level #" + (string)lvl + " to " + (string)new_price, LOG_INFO, __LINE__, __FUNCTION__);
    
    modified.direction = GRID_DIRECTION_LOWER;
    modified.level = lvl;
    modified.price = new_price;
   }
  }
  else if(direction=="UPPER") 
  {
   if(MathAbs(p_upper_prc_level[lvl-1] - new_price) > DBL_EPSILON)
   {
    p_upper_prc_level.Update(lvl-1, new_price);
    grid_logger.log("Changed upper grid level #" + (string)lvl + " to " + (string)new_price, LOG_INFO, __LINE__, __FUNCTION__);
    
    modified.direction = GRID_DIRECTION_UPPER;
    modified.level = lvl;
    modified.price = new_price;
   }
  }
  else
   grid_logger.log("Could direction of grid chart name = " + obj_name, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
 }
 else
 {
  grid_logger.log("Could not parse grid chart name = " + obj_name, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
 }

 return modified;
}

Grid::Grid(double PIP_MODULUS,
           double PIP_REDRAW_BUFFER=0,
           double PIP_GRID_OFFSET=0,
           double PIP_GRID_LENGTH=3000,
           color clr=clrRed)
{
 Rational starting_price = DoubleToRational(Bid);
 p_pip_redraw_buff = PIP_REDRAW_BUFFER * Point * 10;
 p_standard_unit = PIP_MODULUS * Point * 10;
 p_grid_pips_length = PIP_GRID_LENGTH;
 p_grid_color = clr;
 
 double lower;
 double upper;
 
 //--- Given Bid, calculate first lower level and first upper level
 switch(Digits)
 {
  case 5:
  case 4:
  case 3:
  
   if(starting_price.frac[Digits-3] >= 5)
   {
    starting_price.frac[Digits-3] = 5;
    starting_price.frac[Digits-2] = 0;
    starting_price.frac[Digits-1] = 0;
   }
   else
   {
    starting_price.frac[Digits-3] = 0;
    starting_price.frac[Digits-2] = 0;
    starting_price.frac[Digits-1] = 0;
   }
   break;
   
  case 2:
   starting_price.frac[1] = 0;
   
  case 1:
   starting_price.frac[0] = 0;   

  case 0:
   starting_price.whole = GetLowerRoundNumberFromInteger(starting_price.whole);
   break;
           
  default: grid_logger.log("Could not find a match for Digits=" + (string)Digits, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
 }
 
 lower = RationalToDouble(starting_price) + (PIP_GRID_OFFSET * Point * 10);
 upper = lower + p_standard_unit;
 
 //--- Draw initial grid
 double lower_bound = lower - (Point * 10 * (p_grid_pips_length / 2));
 double upper_bound = upper + (Point * 10 * (p_grid_pips_length / 2));

 for(double prc=lower; prc >= lower_bound; prc -= p_standard_unit)
  p_lower_prc_level.Add(prc);
 
 for(double prc=upper; prc <= upper_bound; prc += p_standard_unit)
  p_upper_prc_level.Add(prc);
 
 DrawGrid(GRID_LOWER_PREFIX, p_lower_prc_level, clr);
 DrawGrid(GRID_UPPER_PREFIX, p_upper_prc_level, clr);
 
 //--- Debug messages
 grid_logger.log(Symbol() + " - init upper: " + (string)upper, LOG_DEBUG, __LINE__, __FUNCTION__);
 grid_logger.log("init lower: " + DoubleToStr(lower, Digits), LOG_DEBUG, __LINE__, __FUNCTION__);
 grid_logger.log("p_standard_unit: " + (string)p_standard_unit, LOG_DEBUG, __LINE__, __FUNCTION__);
 grid_logger.log("lower_bound: " + DoubleToStr(lower_bound, Digits), LOG_DEBUG, __LINE__, __FUNCTION__);
 grid_logger.log("upper_bound: " + DoubleToStr(upper_bound, Digits), LOG_DEBUG, __LINE__, __FUNCTION__);
}

Grid::~Grid(void)
{
 DeleteGrid(GRID_UPPER_PREFIX);
 DeleteGrid(GRID_LOWER_PREFIX);
}

long Grid::GetLowerRoundNumberFromInteger(long x)
{
 while(MathMod(x, p_standard_unit) != 0)
  x--;
  
 return x;
}
