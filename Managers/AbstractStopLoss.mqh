//+------------------------------------------------------------------+
//|                                             AbstractStopLoss.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#ifndef LOGGER
#define LOGGER
   #define LOGGER_DEPRECATED
   #include <Logger.mqh>
#endif

#ifndef ARRAYINT
#define ARRAYINT
   #include <Arrays/ArrayInt.mqh>
#endif 

#ifndef ARRAYDOUBLE
#define ARRAYDOUBLE
   #include <Arrays/ArrayDouble.mqh>
#endif

#ifndef STRINGS
#define STRINGS
   #include <Packages/Strings.mqh>
#endif 

#ifndef MAP
#define MAP
   #include <Map.mqh>
#endif 

struct TriggeredStopLoss
{
 int ticket;
 datetime triggered_timestamp;
 double abstract_stop_loss;
};

class AbstractStopLoss
{
 private:
  Logger logger;
  
 protected:
  CArrayInt p_ticket;
  CArrayInt p_type;
  CArrayInt p_activated;
  CArrayDouble p_soft_sl;
  TriggeredStopLoss p_pending_close[];
  int p_pending_close_size;
  color p_clr;
  string p_sl_prefix;
  
  void activate_pending_close(int ticket, double sl);
  void remove_pending_close(int pos);
  void check_for_closed_trades();
  void myInit();
 
  virtual void handle_pending_close(TriggeredStopLoss& pending_close[], int arr_size)=0; 
  bool is_activated(int ticket);
  
  
  AbstractStopLoss(string SL_PREFIX, color clr=clrYellow);
  ~AbstractStopLoss();
 
 public:
  void tick_engine(); 
  void chart_drag_engine(string obj_name);
  void chart_delete_engine(string obj_name);
  bool set(int ticket, double soft_stop_loss, double hard_stop_loss=0);
  bool remove(int ticket, bool delete_obj=true);
  bool modify(int ticket, double soft_stop_loss);
  void deactivate_pending_close(int ticket);
};

bool AbstractStopLoss::is_activated(int ticket)
{
 int i = p_ticket.SearchLinear(ticket);
 
 if(i>=0)
  return(p_activated[i]);
 
 return false; 
}


void AbstractStopLoss::chart_delete_engine(string obj_name)
{
 string result[];
 StringSplit(obj_name, '-', result);
 
 if(ArraySize(result)==2)
 {
  int ticket = (int)StringTrimRight(StringTrimLeft(result[1]));

  int pos = p_ticket.SearchLinear(ticket);
  
  if(pos >=0)
   remove(ticket, false);
 }
 else
  LOG_HIGH("Could not parse grid chart name = " + obj_name);
}

void AbstractStopLoss::chart_drag_engine(string obj_name)
{
 double new_price=NormalizeDouble(ObjectGet(obj_name, OBJPROP_PRICE1), Digits);
 
 string result[];
 StringSplit(obj_name, '-', result);
 
 if(ArraySize(result)==2)
 {
  int ticket = (int)StringTrimRight(StringTrimLeft(result[1]));

  int pos = p_ticket.SearchLinear(ticket);
  
  if(pos >=0)
  {
   if(MathAbs(p_soft_sl[pos] - new_price) > DBL_EPSILON)
   {
    LOG_INFO("Changing soft stop loss ticket #" + (string)ticket + " from " + DoubleToStr(p_soft_sl[pos], Digits) + " to " + DoubleToStr(new_price, Digits));
    p_soft_sl.Update(pos, new_price);
   }
  }
  else
   LOG_HIGH("Cannot modify #" + (string)ticket + " for ticket has not been added");
 }
 else
  LOG_HIGH("Could not parse grid chart name = " + obj_name);
}

AbstractStopLoss::tick_engine()
{
 check_for_closed_trades();
 
 int total = p_ticket.Total();
 
 for(int i=total-1; i >= 0; i--)
 {
  if(p_activated[i]==False)
  {
   bool triggered = false;
   
   if(p_type[i]==OP_BUY)
   {
    if(Bid <= p_soft_sl[i])
     triggered = true;
   }
  
   if(p_type[i]==OP_SELL)
   {
    if(Bid >= p_soft_sl[i])
     triggered = true;
   }
   
   if(triggered)
   {
    p_activated.Update(i, True);
     
    activate_pending_close(p_ticket[i], p_soft_sl[i]);
     
    LOG_INFO("#" + (string)p_ticket[i] + " soft stop loss has been triggered");
   }
  }
 }
 
 handle_pending_close(p_pending_close, p_pending_close_size);
}

void AbstractStopLoss::activate_pending_close(int ticket, double sl)
{
  p_pending_close_size++;
  ArrayResize(p_pending_close, p_pending_close_size);
  int k = p_pending_close_size-1;
  p_pending_close[k].ticket = ticket;
  p_pending_close[k].abstract_stop_loss = sl;
  p_pending_close[k].triggered_timestamp = TimeCurrent();
}

void AbstractStopLoss::remove_pending_close(int pos)
{
 int i = pos;

 int ticket = p_pending_close[i].ticket;
   
 for(int k=i+1; k<p_pending_close_size; k++)
 {
  p_pending_close[k-1].ticket = p_pending_close[k].ticket;
  p_pending_close[k-1].abstract_stop_loss = p_pending_close[k].abstract_stop_loss;
  p_pending_close[k-1].triggered_timestamp = p_pending_close[k].triggered_timestamp;
 }
 
 p_pending_close_size--;
  
 ArrayResize(p_pending_close, p_pending_close_size);
}

void AbstractStopLoss::deactivate_pending_close(int ticket)
{
 int i = p_ticket.SearchLinear(ticket);
  
 if(i>=0)
 {
  p_activated.Update(i, False);
  LOG_INFO("Deactivated previously triggered #" + (string)ticket);
 }
 else
  LOG_HIGH("Unable to find #" + (string)ticket);
}

AbstractStopLoss::~AbstractStopLoss()
{
}

AbstractStopLoss::AbstractStopLoss(string SL_PREFIX, color clr=clrYellow) : logger(__FILE__, "helper_processes")
{
 p_pending_close_size = 0;
 p_clr = clr;
 p_sl_prefix=SL_PREFIX;
 
 myInit();
}

bool AbstractStopLoss::modify(int ticket,double soft_sl)
{
 int pos=p_ticket.SearchLinear(ticket);
 
 if(pos >= 0)
 {
  string name = StringConcatenate(p_sl_prefix, " - ", ticket);
  double prev = ObjectGet(name, OBJPROP_PRICE1);
  
  if(ObjectSet(name, OBJPROP_PRICE1, soft_sl))
  {
   p_soft_sl.Update(pos, soft_sl);
   LOG_INFO("Modified #" + IntegerToString(ticket) + " soft stop loss from " + DoubleToStr(prev, Digits) + " to " + DoubleToStr(soft_sl, Digits));
   return true;
  }
  else
   LOG_LOW_ERROR("Could not modify #" + (string)ticket + ", name=" + name);
 } 

 return false;
}

bool AbstractStopLoss::remove(int ticket, bool delete_obj=true)
{
 int pos=p_ticket.SearchLinear(ticket);
 
 if(pos >= 0)
 {
  string name = StringConcatenate(p_sl_prefix, " - ", ticket);
  
  if(!delete_obj || ObjectDelete(name))
  {
   p_ticket.Delete(pos);
   p_soft_sl.Delete(pos);
   p_type.Delete(pos);
   p_activated.Delete(pos);
   LOG_DEBUG("Removed #" + (string)ticket + " soft stop loss");
   
   //--- Delete order from pending_close queue if added
   int i;

   for(i=0; i<p_pending_close_size; i++)
   {
    if(p_pending_close[i].ticket == ticket) 
    {
     remove_pending_close(i);
     return true;
    }
   }
  }
  else
   LOG_DEBUG_ERROR("Could not remove #" + (string)ticket + ", name=" + name);
 }
 
 LOG_DEBUG("Could not find #" + (string)ticket);
 
 return false;
}

bool AbstractStopLoss::set(int ticket, double soft_sl, double hard_sl=0)
{
 string name = StringConcatenate(p_sl_prefix, " - ", ticket);
 
 //--- set hard stop loss
 if(hard_sl > 0)
  if(OrderSelect(ticket, SELECT_BY_TICKET))
   bool ans = OrderModify(ticket, OrderOpenPrice(), hard_sl, OrderTakeProfit(), 0);
 
 //--- set soft stop loss
 if(OrderSelect(ticket, SELECT_BY_TICKET))
 {
  int ord_type=OrderType();
  double open_prc=OrderOpenPrice();
  bool pass=false;
  string err=NULL;
  
  if(ord_type==OP_BUY)
  {
   pass = soft_sl < open_prc && hard_sl < open_prc;
   
   if(!pass)
    err="Soft and hard stop loss must be less than open prc";
  }
  
  if(ord_type==OP_SELL)
  {
   pass = soft_sl > open_prc && (MathAbs(hard_sl) < DBL_EPSILON || hard_sl > open_prc);
   err="Soft and hard stop loss must be greater than open prc";
  }
  
  if(pass)
  {
   if(ObjectCreate(name, OBJ_HLINE, 0, 0, soft_sl))
   {
    string order_symbol=OrderSymbol();
    string symbol=Symbol();
    if(order_symbol==symbol)
    {
     int type = OrderType();
    
     if(type==OP_BUY || type==OP_SELL)
     {
      ObjectSet(name, OBJPROP_COLOR, p_clr);
      ObjectSet(name, OBJPROP_STYLE, STYLE_DASHDOT);
      p_ticket.Add(ticket);
      p_soft_sl.Add(soft_sl);
      p_type.Add(type);
      p_activated.Add(False);
     
      return true;
     }
     else
      LOG_HIGH("#" + (string)ticket + " must be a market order");
    }
    else
     LOG_HIGH("#" + (string)ticket + " must have same symbol as chart symbol, symbol=" + symbol + ", order_symbol=" + order_symbol);
   }
   else
    LOG_HIGH_ERROR("Could not set soft stop loss, name=" + name);
  }
  else {
    LOG_HIGH(err);
    LOG_HIGH_3(soft_sl, hard_sl, open_prc);
  }
 }
 else
  LOG_HIGH("Unable to select ticket #" + (string)ticket);
 
 return false;
}

void AbstractStopLoss::check_for_closed_trades(void)
{
 int total = p_ticket.Total();
 
 for(int i=total-1; i>=0; i--)
 {
  if(OrderSelect(p_ticket[i],SELECT_BY_TICKET))
  {
   if(OrderCloseTime() > 0)
   {
    LOG_INFO("Removing #" + IntegerToString(p_ticket[i]) + " for ticket is closed");
    remove(p_ticket[i]);
   }
  }
 }
}

void AbstractStopLoss::myInit()
{
  int total = ObjectsTotal();
 
  for(int i=0; i<total; i++)
  {
   string name = ObjectName(i);
  
   if(StringFind(name, p_sl_prefix) >= 0)
   {
    string result[];
    StringParse(name, p_sl_prefix, '-', result);
   
    if(ArraySize(result)==2)
    { 
     int ticket = (int)result[1];
   
     if(OrderSelect(ticket, SELECT_BY_TICKET))
     {
      if(OrderSymbol()==Symbol())
      {
       int type = OrderType();
    
       if(type==OP_BUY || type==OP_SELL)
       {
        double stop_loss = ObjectGet(name, OBJPROP_PRICE1);
       
        ObjectSet(name, OBJPROP_COLOR, p_clr);
        ObjectSet(name, OBJPROP_STYLE, STYLE_DASHDOT);
        p_ticket.Add(ticket);
        p_soft_sl.Add(stop_loss);
        p_type.Add(type);
        p_activated.Add(False);
     
        LOG_INFO("Backfilling #" + (string)ticket);
       }
       else
        LOG_HIGH("#" + (string)ticket + " must be a market order");
      }
      else
       LOG_HIGH("#" + (string)ticket + " must have same symbol as chart symbol");
     }
     else
      LOG_HIGH("Unable to select ticket #" + (string)ticket);
    }
   else
    LOG_HIGH("Invalid parse of name=" + name); 
  }
 }
}