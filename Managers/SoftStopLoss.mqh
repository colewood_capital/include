//+------------------------------------------------------------------+
//|                                                 SoftStopLoss.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
#endif

#ifndef ARRAYINT
#define ARRAYINT
   #include <Arrays/ArrayInt.mqh>
#endif 

#ifndef ARRAYDOUBLE
#define ARRAYDOUBLE
   #include <Arrays/ArrayDouble.mqh>
#endif

#ifndef MAP
#define MAP
   #include <Map.mqh>
#endif 

#define SOFT_SL_PREFIX     "SoftStopLoss"

Logger softstoplosslogger(__FILE__, "helper_processes");

struct SoftStopLossTicket
{
 int ticket;
 double soft_stop_loss;
};

class SoftStopLoss
{
 private:
  CArrayInt p_ticket;
  CArrayInt p_type;
  CArrayInt p_activated;
  CArrayDouble p_soft_sl;
  SoftStopLossTicket p_pending_close[];
  int p_pending_close_size;
  color p_clr;
  
  void check_for_closed_trades();
 
 public:
  void engine_tick();
  void engine_chart_drag(string obj_name);
  void engine_chart_delete(string obj_name);
  bool set(int ticket, double soft_stop_loss, double hard_stop_loss=0);
  bool remove(int ticket, bool delete_obj=true);
  bool modify(int ticket, double soft_stop_loss);
  int get_pending_close(SoftStopLossTicket& pending_close[]);
 
  SoftStopLoss(color clr=clrYellow);
  ~SoftStopLoss();
};

void SoftStopLoss::engine_chart_delete(string obj_name)
{
 string result[];
 StringSplit(obj_name, '-', result);
 
 if(ArraySize(result)==2)
 {
  int ticket = (int)StringTrimRight(StringTrimLeft(result[1]));

  int pos = p_ticket.SearchLinear(ticket);
  
  if(pos >=0)
   remove(ticket, false);
 }
 else
  softstoplosslogger.log("Could not parse grid chart name = " + obj_name, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
}

void SoftStopLoss::engine_chart_drag(string obj_name)
{
 double new_price=NormalizeDouble(ObjectGet(obj_name, OBJPROP_PRICE1), Digits);
 
 string result[];
 StringSplit(obj_name, '-', result);
 
 if(ArraySize(result)==2)
 {
  int ticket = (int)StringTrimRight(StringTrimLeft(result[1]));

  int pos = p_ticket.SearchLinear(ticket);
  
  if(pos >=0)
  {
   if(MathAbs(p_soft_sl[pos] - new_price) > DBL_EPSILON)
   {
    softstoplosslogger.log("Changing soft stop loss ticket #" + (string)ticket + " from " + DoubleToStr(p_soft_sl[pos], Digits) + " to " + DoubleToStr(new_price, Digits), LOG_INFO, __LINE__, __FUNCTION__);
    p_soft_sl.Update(pos, new_price);
   }
  }
  else
   softstoplosslogger.log("Cannot modify #" + (string)ticket + " for ticket has not been added", LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
 }
 else
  softstoplosslogger.log("Could not parse grid chart name = " + obj_name, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
}

SoftStopLoss::engine_tick()
{
 check_for_closed_trades();
 
 int total = p_ticket.Total();
 
 for(int i=total-1; i >= 0; i--)
 {
  if(p_activated[i]==False)
  {
   bool triggered = false;
   
   if(p_type[i]==OP_BUY)
   {
    if(Bid <= p_soft_sl[i])
     triggered = true;
   }
  
   if(p_type[i]==OP_SELL)
   {
    if(Bid >= p_soft_sl[i])
     triggered = true;
   }
   
   if(triggered)
   {
    p_activated.Update(i, True);
     
    p_pending_close_size++;
    ArrayResize(p_pending_close, p_pending_close_size);
    int k = p_pending_close_size-1;
    p_pending_close[k].ticket = p_ticket[i];
    p_pending_close[k].soft_stop_loss = p_soft_sl[i];
     
    softstoplosslogger.log("#" + (string)p_ticket[i] + " soft stop loss has been triggered", LOG_INFO, __LINE__, __FUNCTION__);
   }
  }
 }
}

SoftStopLoss::~SoftStopLoss()
{
 int total = p_ticket.Total();
 
 for(int i=total-1; i>=0; i--)
 {
  string name = StringConcatenate(SOFT_SL_PREFIX, " - ", p_ticket[i]);
  
  if(!ObjectDelete(name))
    softstoplosslogger.log("Could not remove #" + (string)p_ticket[i] + ", name=" + name + ", e=" + (string)GetLastError(), LOG_PRIORITY_LOW, __LINE__, __FUNCTION__);
 } 
}

SoftStopLoss::SoftStopLoss(color clr=clrYellow)
{
 p_pending_close_size = 0;

 if(clr==clrRed) {
  clr = clrYellow;
  softstoplosslogger.log("clrRed is a reserved color, changing color to default clrYellow", LOG_WARNING, __LINE__, __FUNCTION__);
 }
 
 p_clr = clr;
}

bool SoftStopLoss::modify(int ticket,double soft_sl)
{
 int pos=p_ticket.SearchLinear(ticket);
 
 if(pos >= 0)
 {
  string name = StringConcatenate(SOFT_SL_PREFIX, " - ", ticket);
  double prev = ObjectGet(name, OBJPROP_PRICE1);
  
  if(ObjectSet(name, OBJPROP_PRICE1, soft_sl))
  {
   softstoplosslogger.log("Modified #" + IntegerToString(ticket) + " soft stop loss from " + DoubleToStr(prev, Digits) + " to " + DoubleToStr(soft_sl, Digits), LOG_INFO, __LINE__, __FUNCTION__);
   return true;
  }
  else
   softstoplosslogger.log("Could not modify #" + (string)ticket + ", name=" + name + ", e=" + (string)GetLastError(), LOG_PRIORITY_LOW, __LINE__, __FUNCTION__);
 } 

 return false;
}

bool SoftStopLoss::remove(int ticket, bool delete_obj=true)
{
 int pos=p_ticket.SearchLinear(ticket);
 
 if(pos >= 0)
 {
  string name = StringConcatenate(SOFT_SL_PREFIX, " - ", ticket);
  
  if(!delete_obj || ObjectDelete(name))
  {
   p_ticket.Delete(pos);
   p_soft_sl.Delete(pos);
   p_type.Delete(pos);
   p_activated.Delete(pos);
   softstoplosslogger.log("Removed #" + (string)ticket + " soft stop loss", LOG_INFO, __LINE__, __FUNCTION__);
   
   //--- Delete order from pending_close queue if added
   int i;
   bool found_ticket = false;
   
   for(i=0; i<p_pending_close_size; i++)
   {
    if(p_pending_close[i].ticket == ticket) {
     found_ticket = true;
     break;
    }
   }
   
   if(found_ticket)
   {
    for(int k=i+1; k<p_pending_close_size; k++)
    {
     p_pending_close[k-1].ticket = p_pending_close[k].ticket;
     p_pending_close[k-1].soft_stop_loss = p_pending_close[k].soft_stop_loss;
    }
 
    p_pending_close_size--;
    ArrayResize(p_pending_close, p_pending_close_size);
   }

   return true;
  }
  else
   softstoplosslogger.log("Could not remove #" + (string)ticket + ", name=" + name + ", e=" + (string)GetLastError(), LOG_PRIORITY_LOW, __LINE__, __FUNCTION__);
 }
 
 return false;
}

bool SoftStopLoss::set(int ticket, double soft_sl, double hard_sl=0)
{
 string name = StringConcatenate(SOFT_SL_PREFIX, " - ", ticket);
 
 //--- set hard stop loss
 if(hard_sl > 0)
  if(OrderSelect(ticket, SELECT_BY_TICKET))
   bool ans = OrderModify(ticket, OrderOpenPrice(), hard_sl, OrderTakeProfit(), 0);
 
 //--- set soft stop loss
 if(ObjectCreate(name, OBJ_HLINE, 0, 0, soft_sl))
 {
  if(OrderSelect(ticket, SELECT_BY_TICKET))
  {
   if(OrderSymbol()==Symbol())
   {
    int type = OrderType();
    
    if(type==OP_BUY || type==OP_SELL)
    {
     ObjectSet(name, OBJPROP_COLOR, p_clr);
     ObjectSet(name, OBJPROP_STYLE, STYLE_DASHDOT);
     p_ticket.Add(ticket);
     p_soft_sl.Add(soft_sl);
     p_type.Add(type);
     p_activated.Add(False);
     
     return true;
    }
    else
     softstoplosslogger.log("#" + (string)ticket + " must be a market order", LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
   }
   else
    softstoplosslogger.log("#" + (string)ticket + " must have same symbol as chart symbol", LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
  }
  else
   softstoplosslogger.log("Unable to select ticket #" + (string)ticket, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
 }
 else
  softstoplosslogger.log("Could not set soft stop loss, name=" + name + ", e=" + (string)GetLastError(), LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
 
 return false;
}

void SoftStopLoss::check_for_closed_trades(void)
{
 int total = p_ticket.Total();
 
 for(int i=total-1; i>=0; i--)
 {
  if(OrderSelect(p_ticket[i],SELECT_BY_TICKET))
  {
   if(OrderCloseTime() > 0)
   {
    softstoplosslogger.log("Removing #" + IntegerToString(p_ticket[i]) + " for ticket is closed", LOG_DEBUG, __LINE__, __FUNCTION__);
    remove(p_ticket[i]);
   }
  }
 }
}

int SoftStopLoss::get_pending_close(SoftStopLossTicket &pending_close[])
{
 ArrayCopy(pending_close, p_pending_close);
 return p_pending_close_size;
}
