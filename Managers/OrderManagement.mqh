//+------------------------------------------------------------------+
//|                                              OrderManagement.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#ifndef AUTOSAVEARRAYINT
#define AUTOSAVEARRAYINT
   #include <Arrays/AutoSaveArrayInt.mqh>
#endif

#ifndef LONGSHORTPARAMETER
#define LONGSHORTPARAMETER
   #include <Managers/LongShortParameter.mqh>
#endif

#ifndef ENTRYBANDS
#define ENTRYBANDS
   #include <Managers/EntryBands.mqh>
#endif 

#ifndef REENTRY
#define REENTRY
   #include <Managers/ReEntry.mqh>
#endif 

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
#endif

#ifndef ENUMS
#define ENUMS
   #include <Super/Enums.mqh>
#endif 

enum ENUM_ORDER_MANAGEMENT {
   ORDER_MANAGEMENT_NIL,
   ORDER_MANAGEMENT_ALL,
   ORDER_MANAGEMENT_CLOSED_BY_SL,
   ORDER_MANAGEMENT_CLOSED_MANUALLY
};

class OrderManagement
{
 private:
  int p_magic;
  AutoSaveArrayInt* p_ticket;
  AutoSaveArrayInt* p_original_ticket;
  AutoSaveArrayInt* p_reopen_count;
  AutoSaveArrayInt* p_max_reopens;
  AutoSaveArrayInt* p_pending_ticket;
  AutoSaveArrayInt* p_pending_max_reopens;
  AutoSaveArrayInt* p_is_open;
  LongShortParameter* p_long_short_param;
  EntryBands* p_entry_bands;
  ReEntry* p_reentry;
  Logger p_logger;
  
  void myinit();
  void add(int ticket, int original_ticket, int max_reopens=0);
  void add_pending(int ticket, int original_ticket, int max_reopens=0);
  void remove(int ticket, string reason=NULL);
  void remove_pending(int ticket, int& max_reopens, string reason=NULL);
  void update(int old_ticket, int new_ticket);
  int my_order_send(string symbol, int cmd, double volume, double price, int slippage, double stoploss, double takeprofit, string comment=NULL, int magic=0, datetime expiration=0, color arrow_color=clrNONE);
  int send_reopen(int original_booknum, double new_volume=0, double sl=-1, double tp=-1, string comment=NULL);

 public:
  int tick_engine(int& ticket[], ENUM_ORDER_MANAGEMENT& result[], ENUM_ORDER_MANAGEMENT subscribe=ORDER_MANAGEMENT_ALL);
  int open(int cmd, double volume,double sl=0,int max_reopens=0, string comment=NULL,double prc=0);
  bool close(int ticket, double lots, color arrow_color=clrNONE);
  int reopen(int ticket, string comment=NULL, double new_sl=-1, double new_tp=-1);
  int get_reopen_count(int ticket);
  
  OrderManagement(int MAGIC, EntryBands* ptr_entry_bands=NULL, LongShortParameter* ptr_long_short_param=NULL);
  OrderManagement(int MAGIC, const CArrayInt& tickets, EntryBands* ptr_entry_bands=NULL, LongShortParameter* ptr_long_short_param=NULL);
  ~OrderManagement();
};

bool OrderManagement::close(int ticket, double percentage=1.0, color arrow_color=clrNONE)
{
 if(OrderSelect(ticket,SELECT_BY_TICKET))
 {
  double prc;
  int cmd = OrderType();
  double lots = NormalizeDouble(OrderLots() * percentage, 2);
  
  if(cmd==OP_BUY)
   prc = Bid;
  else if(cmd==OP_SELL)
   prc = Ask;
  else {
   p_logger.log("Can only close market orders, ticket=" + (string)ticket + ", cmd=" + (string)cmd, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
   return false;
  }
  
  bool ans = OrderClose(ticket, NormalizeDouble(lots, 2), prc, 5, arrow_color);
  
  if(!ans)
   p_logger.log("Could not close #" + (string)ticket + ", e=" + (string)GetLastError(), LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
   
  return ans;
 }
 else
  p_logger.log("Could not select ticket #" + (string)ticket, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
 
 return false;
}

int OrderManagement::my_order_send(string symbol,int cmd,double volume,double price,int slippage,double stoploss,double takeprofit,string comment=NULL,int magic=0,datetime expiration=0,color arrow_color=4294967295)
{
 //--- check if trading allowed at current price level
 double spread = MarketInfo(symbol, MODE_SPREAD) * MarketInfo(symbol, MODE_POINT);
 double prc = (cmd==OP_BUY || cmd==OP_BUYLIMIT || cmd==OP_BUYSTOP) ? price - spread : (cmd==OP_SELL || cmd==OP_SELLLIMIT || cmd==OP_SELLSTOP) ? price : 0;
 int digits = (int)MarketInfo(symbol, MODE_DIGITS);
 
 if(p_entry_bands != NULL)
 {
  if(!p_entry_bands.is_trading_allowed(prc))
  {
   p_logger.log("Trade not opened for: EntryBand rules failed, symbol=" + symbol + ", cmd=" + (string)cmd + ", volume=" + DoubleToStr(volume, 2) + ", price=" + DoubleToStr(price, digits) + ", prc=" + DoubleToStr(prc, digits) + ", spread=" + DoubleToStr(spread, digits), LOG_INFO, __LINE__, __FUNCTION__);
   SetUserError(ERR_TRADING_DISABLED_ENTRY_BANDS);
   return -1;
  }
 }
 
 //--- check if long / short trading enabled
 if(p_long_short_param != NULL)
 {
  if(cmd==OP_BUY || cmd==OP_BUYLIMIT || cmd==OP_BUYSTOP)
  {
   if(!p_long_short_param.is_long_trading_allowed())
   {
    p_logger.log("Trade not opened for: long trading disabled, symbol=" + symbol + ", cmd=" + (string)cmd + ", volume=" + DoubleToStr(volume, 2) + ", price=" + DoubleToStr(price, digits), LOG_INFO, __LINE__, __FUNCTION__);
    SetUserError(ERR_TRADING_DISABLED_LONG);
    return -1;
   }
  }
  
  if(cmd==OP_SELL || cmd==OP_SELLLIMIT || cmd==OP_SELLSTOP)
  {
   if(!p_long_short_param.is_short_trading_allowed())
   {
    p_logger.log("Trade not opened for: short trading disabled, symbol=" + symbol + ", cmd=" + (string)cmd + ", volume=" + DoubleToStr(volume, 2) + ", price=" + DoubleToStr(price, digits), LOG_INFO, __LINE__, __FUNCTION__);
    SetUserError(ERR_TRADING_DISABLED_SHORT);
    return -1;
   }
  }
 }
 
 return OrderSend(symbol, cmd, volume, price, slippage, stoploss, takeprofit, comment, magic, expiration, arrow_color);
}

int OrderManagement::get_reopen_count(int ticket)
{
 int pos = p_ticket.SearchLinear(ticket);
 
 if(pos >= 0)
 {
  return p_reopen_count[pos];
 }
 else
  p_logger.log("Could not find ticket #" + (string)ticket, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
  
 return 0;
}

int OrderManagement::reopen(int ticket, string comment=NULL, double new_sl=-1, double new_tp=-1)
{
 int new_ticket = -1;
 int i = p_ticket.SearchLinear(ticket);
 
 if(i >= 0)
 {
  new_ticket = send_reopen(ticket, 0, new_sl, new_tp, comment);

  if(new_ticket > 0)
  {
   p_reopen_count.Update(i, p_reopen_count[i]+1);
   p_logger.log("Updating #" + (string)p_ticket[i] + " reopen_count to " + (string)p_reopen_count[i], LOG_INFO, __LINE__, __FUNCTION__);
  }
   
  if(p_reopen_count[i] >= p_max_reopens[i])
  {
   remove(p_ticket[i], StringConcatenate("reopen_count >= max_allowed=", p_max_reopens[i]));
  }
 }
 else
  p_logger.log("Could not find ticket #" + (string)ticket, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
  
 return new_ticket;
}

int OrderManagement::send_reopen(int original_booknum,double new_volume=0.000000,double sl=-1, double tp=-1,string comment=NULL)
{
  if(OrderSelect(original_booknum, SELECT_BY_TICKET))
  {
   int cmd = OrderType();
   double prc;
   double volume;
 
   if(cmd==OP_BUY)
    prc = Ask;
   else if(cmd==OP_SELL)
    prc = Bid;
   else {
    p_logger.log("Can only place market orders", LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
    return -1;
   }
  
   if(new_volume > DBL_EPSILON)
    volume = NormalizeDouble(new_volume, 2);
   else
    volume = NormalizeDouble(OrderLots(), 2);
   
   if(sl > -DBL_EPSILON)
    sl = NormalizeDouble(sl, Digits);
   else
    sl = NormalizeDouble(OrderStopLoss(), Digits);
    
   if(tp > -DBL_EPSILON)
    tp = NormalizeDouble(tp, Digits);
   else
    tp = NormalizeDouble(OrderTakeProfit(), Digits);
 
   int ticket = my_order_send(OrderSymbol(), cmd, volume, prc, 5, sl, tp, comment, p_magic);
 
   if(ticket > 0)
    add(ticket, original_booknum);
   else
    p_logger.log("Could not place trade for e=" + (string)GetLastError(), LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
 
   return ticket;
  }
  else
   p_logger.log("Could not select original_booknum #" + (string)original_booknum, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
  
 return -1;
}

int OrderManagement::open(int cmd,double volume,double sl=0,int max_reopens=0, string comment=NULL,double prc=0)
{
 if(cmd==OP_BUY)
  prc = Ask;
 else if(cmd==OP_SELL)
  prc = Bid;
 else if(cmd==OP_BUYLIMIT || cmd==OP_SELLLIMIT || cmd==OP_BUYSTOP || cmd==OP_SELLSTOP)
 {
  if(MathAbs(prc) < DBL_EPSILON)
  {
   p_logger.log("prc must be greater than zero for pending orders", LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
   return -1;
  }
 }
 else {
  p_logger.log("Unexpected order type, cmd=" + (string)cmd, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
  return -1;
 }
 
 int ticket = my_order_send(Symbol(), cmd, volume, prc, 5, sl, 0, comment, p_magic);
 int original_ticket = ticket;
 
 if(ticket > 0)
 {
  if(cmd==OP_BUY || cmd==OP_SELL)
   add(ticket, original_ticket, max_reopens);
  else
   add_pending(ticket, original_ticket, max_reopens);
 }
 else
  p_logger.log("Could not place trade for e=" + (string)GetLastError() + ", cmd=" + (string)cmd + ", volume=" + (string)volume + ", sl=" + DoubleToStr(sl, Digits) + ", max_reopens=" + (string)max_reopens, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
 
 return ticket;
}

int OrderManagement::tick_engine(int& ticket[], ENUM_ORDER_MANAGEMENT& result[], ENUM_ORDER_MANAGEMENT subscribe=ORDER_MANAGEMENT_ALL)
{
 //---- check for pending open
 //---- check for pending cancel
  int pending_total = p_pending_ticket.Total();
  
  for(int k=0; k<pending_total; k++)
  {
   int pending_ticket = p_pending_ticket[k];
   
   if(OrderSelect(pending_ticket, SELECT_BY_TICKET))
   {
    int max_reopens;
    int type = OrderType();
    
    if(type==OP_BUY)
    {
     remove_pending(pending_ticket, max_reopens, "OPENED");
     add(pending_ticket, pending_ticket, max_reopens);
     p_logger.log("[BUY] Converting pending #" + (string)pending_ticket + " to market order", LOG_INFO, __LINE__, __FUNCTION__);
    }
    
    if(type==OP_SELL)
    {
     remove_pending(pending_ticket, max_reopens, "OPENED");
     add(pending_ticket, pending_ticket, max_reopens);
     p_logger.log("[SELL] Converting pending #" + (string)pending_ticket + " to market order", LOG_INFO, __LINE__, __FUNCTION__);
    }
    
    if(OrderCloseTime() > 0)
    {
     remove_pending(pending_ticket, max_reopens, "CANCELLED");
     p_logger.log("Removing pending #" + (string)pending_ticket + " for order cancelled", LOG_INFO, __LINE__, __FUNCTION__);
    }
   }
  }

 //---- check for market close
 int total = p_ticket.Total();
 int i;
 int size = 0;
 
 for(i=total-1; i>=0; i--)
 {
  bool check_reentry = false;
  
  if(p_is_open[i] && OrderSelect(p_ticket[i],SELECT_BY_TICKET))
  {
   if(StringFind(OrderComment(), "[sl]") >= 0)
   {
    if(OrderCloseTime() > 0)
    {
     check_reentry = true;
     
     if(subscribe==ORDER_MANAGEMENT_CLOSED_BY_SL || subscribe==ORDER_MANAGEMENT_ALL)
     {
      ArrayResize(ticket, size+1);
      ArrayResize(result, size+1);
      ticket[size] = p_original_ticket[i];
      result[size] = ORDER_MANAGEMENT_CLOSED_BY_SL;
      size++;
     }
     
     p_is_open.Update(i, false);
     p_logger.log("Order #" + (string)p_ticket[i] + " SL hit", LOG_INFO, __LINE__, __FUNCTION__);
    }
    else
     p_logger.log("[sl] found in comment, yet OrderCloseTime() not greater than zero", LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
   }
   
   if(OrderCloseTime() > 0)
   {
  //--- check_reentry not set to true
  //--- implement a method / set of conditions to reenter a closed order
   if(subscribe==ORDER_MANAGEMENT_CLOSED_MANUALLY || subscribe==ORDER_MANAGEMENT_ALL)
    {
     ArrayResize(ticket, size+1);
     ArrayResize(result, size+1);
     ticket[size] = p_original_ticket[i];
     result[size] = ORDER_MANAGEMENT_CLOSED_MANUALLY;
     size++;
    }
    
    p_is_open.Update(i, false);
    p_logger.log("Order #" + (string)p_ticket[i] + " was closed", LOG_INFO, __LINE__, __FUNCTION__);
   }
 
   if(!p_is_open[i])
   {
    if(p_max_reopens[i] <= 0)
    {
     remove(p_ticket[i], "order is closed");
     continue;
    }
   }
   
  //--- Set reentry if any order's SL is hit
   if(check_reentry)
   {
    if(OrderSelect(p_original_ticket[i],SELECT_BY_TICKET) && p_reopen_count[i] < p_max_reopens[i])
    {
     int type = OrderType();
    
     if(type==OP_BUY)
     {
      double spread = MarketInfo(NULL, MODE_SPREAD) * Point;
      p_reentry.create(p_original_ticket[i], OrderOpenPrice() - spread);
     }
  
     if(type==OP_SELL)
      p_reentry.create(p_original_ticket[i], OrderOpenPrice());
    }
   }
  }
 }
 
 //--- Reenter if market reverses and crosses reentry line
  int reentry_ticket;
  
  while(p_reentry.tick_engine(reentry_ticket)==REENTRY_TRIGGERED)
  {
   string cmt = "REENTRY_" + (string)(get_reopen_count(reentry_ticket)+1);
   reopen(reentry_ticket, cmt);
  }
 
 return size;
}

OrderManagement::~OrderManagement()
{
 delete p_ticket;
 delete p_reopen_count;
 delete p_original_ticket;
 delete p_max_reopens;
 delete p_pending_ticket;
 delete p_pending_max_reopens;
 delete p_is_open;
 delete p_reentry;
}

OrderManagement::OrderManagement(int MAGIC, EntryBands* ptr_entry_bands=NULL, LongShortParameter* ptr_long_short_param=NULL)
 : p_logger(__FILE__, "helper_processes")
{
 p_magic = MAGIC;
 p_long_short_param = ptr_long_short_param;
 p_entry_bands = ptr_entry_bands;
 myinit();
}

OrderManagement::OrderManagement(int MAGIC, const CArrayInt &tickets, EntryBands* ptr_entry_bands=NULL, LongShortParameter* ptr_long_short_param=NULL)
 : p_logger(__FILE__, "helper_processes")
{
 p_magic = MAGIC;
 p_long_short_param = ptr_long_short_param;
 p_entry_bands = ptr_entry_bands;
 myinit();
 
 int total=tickets.Total();
 
 for(int i=0; i<total; i++)
 {
  int ticket = tickets[i];
  int original_ticket = ticket;
  
  add(ticket, original_ticket);
  
  p_logger.log("Backfilling #" + (string)ticket, LOG_DEBUG, __LINE__, __FUNCTION__);
 }
}

void OrderManagement::remove(int ticket, string reason=NULL)
{
 int pos = p_ticket.SearchLinear(ticket);
 
 if(pos >= 0)
 {
  string msg = (reason) ? StringConcatenate("Removing #", ticket, " for ", reason) : "Removing #" + (string)ticket;
  p_logger.log(msg, LOG_DEBUG, __LINE__, __FUNCTION__);
  
  p_ticket.Delete(pos);
  p_reopen_count.Delete(pos);
  p_original_ticket.Delete(pos);
  p_max_reopens.Delete(pos);
  p_is_open.Delete(pos);
 }
}

void OrderManagement::update(int old_ticket, int new_ticket)
{
 int pos = p_ticket.SearchLinear(old_ticket);
 
 if(pos >= 0)
 {
  p_ticket.Update(pos, new_ticket);
  p_logger.log("updating old_ticket #" + (string)old_ticket + " to #" + (string)new_ticket, LOG_DEBUG, __LINE__, __FUNCTION__);
 }
 else
  p_logger.log("Could not find old_ticket #" + (string)old_ticket, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
}

void OrderManagement::remove_pending(int ticket, int& max_reopens, string reason=NULL)
{
 int pos = p_ticket.SearchLinear(ticket);
 
 if(pos >= 0)
 {
  string msg = (reason) ? StringConcatenate("Removing #", ticket, " for ", reason) : "Removing #" + (string)ticket;
  p_logger.log(msg, LOG_DEBUG, __LINE__, __FUNCTION__);
  
  max_reopens = p_max_reopens[pos];
  
  p_pending_ticket.Delete(pos);
  p_pending_max_reopens.Delete(pos);
 }
}

void OrderManagement::add_pending(int ticket, int original_ticket, int max_reopens=0)
{
 if(p_pending_ticket.SearchLinear(ticket) < 0)
 {
  p_logger.log("Adding #" + (string)ticket, LOG_INFO, __LINE__, __FUNCTION__);
  p_pending_ticket.Add(ticket);
  p_pending_max_reopens.Add(max_reopens);
 }
}

void OrderManagement::add(int ticket, int original_ticket, int max_reopens=0)
{
 if(p_ticket.SearchLinear(ticket) < 0)
 {
  p_logger.log("Adding #" + (string)ticket, LOG_INFO, __LINE__, __FUNCTION__);

  p_ticket.Add(ticket);
  p_original_ticket.Add(original_ticket);
  p_reopen_count.Add(0);
  p_max_reopens.Add(max_reopens);
  p_is_open.Add(1);
 }
}

void OrderManagement::myinit(void)
{
 p_ticket = new AutoSaveArrayInt("DO_NOT_OPEN_1", __FILE__, false);
 p_reopen_count = new AutoSaveArrayInt("DO_NOT_OPEN_2", __FILE__, false);
 p_max_reopens = new AutoSaveArrayInt("DO_NOT_OPEN_3", __FILE__, false);
 p_original_ticket = new AutoSaveArrayInt("DO_NOT_OPEN_4", __FILE__, false);
 p_is_open = new AutoSaveArrayInt("DO_NOT_OPEN_5", __FILE__, false);
 p_pending_ticket = new AutoSaveArrayInt("DO_NOT_OPEN_6", __FILE__, false);
 p_pending_max_reopens = new AutoSaveArrayInt("DO_NOT_OPEN_7", __FILE__, false);
 p_reentry = new ReEntry();
 
 int total=OrdersTotal();
 
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(i,SELECT_BY_POS) && OrderMagicNumber()==p_magic)
  {
   int ticket = OrderTicket();
   int original_ticket = ticket;
   add(ticket, original_ticket);
  }
 }
}