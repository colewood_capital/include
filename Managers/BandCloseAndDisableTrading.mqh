//+------------------------------------------------------------------+
//|                                       CloseAndDisableTrading.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#ifndef ABSTRACTSTOPLOSS
#define ABSTRACTSTOPLOSS
   #include <Managers\AbstractTakeProfit.mqh>
#endif 

#ifndef UTILS2
#define UTILS2
   #include <Utils2.mqh>
#endif 

#define BAND_NAME       "CloseAndDisableTradingBand"

class BandCloseAndDisableTrading : public AbstractTakeProfit
{
private:
 Logger logger;
 
protected:
 void handle_pending_close(TriggeredTakeProfit& pending_close[], int arr_size);
 
public:
 void timer_engine();
 BandCloseAndDisableTrading() : logger(__FILE__, "Managers"), AbstractTakeProfit("BandCloseAndDisableTrading", clrDarkBlue) {};
};

void BandCloseAndDisableTrading::handle_pending_close(TriggeredTakeProfit &pending_close[],int arr_size)
{
 //--- close all trades
  for(int i=arr_size-1; i>=0; i--)
  {
   if(OrderSelect(pending_close[i].ticket, SELECT_BY_TICKET))
   {
    int type=OrderType();
    double prc;
   
    if(type==OP_BUY)
     prc=Bid;
    else if(type==OP_SELL)
     prc=Ask;
    else {
     LOG_HIGH_1(type);
     return; 
    }
   
    bool ans=OrderClose(pending_close[i].ticket, OrderLots(), prc, 10);
   
    if(!ans)
     LOG_HIGH_ERROR("Error closing #" + (string)pending_close[i].ticket);
     
    //--- disable trading
     if(type==OP_BUY)
      TradeEngine::disable_open(NULL, OP_BUY);

     if(type==OP_SELL)
      TradeEngine::disable_open(NULL, OP_SELL);
   }
  }
}

void BandCloseAndDisableTrading::timer_engine(void)
{
 long chart_id=0;
 int obj_total=ObjectsTotal(chart_id);

 for(int i=0; i<obj_total; i++)
 {
  string name=ObjectName(chart_id, i);
  
  if(name==BAND_NAME)
  {
   double prc=ObjectGet(name,OBJPROP_PRICE1);
   
   int order_total=OrdersTotal();
   
   for(int j=0; j<order_total; j++)
   {
    if(OrderSelect(j,SELECT_BY_POS) && OrderSymbol()==Symbol())
    {
     int type=OrderType();
     double open_prc=OrderOpenPrice();
     
     if((type==OP_BUY && prc > open_prc ) || (type==OP_SELL && prc < open_prc))
     {
      int ticket=OrderTicket();

      if(!modify(ticket, prc))
      {
       if(set(ticket, prc))
        LOG_DEBUG("Successfully set #" + (string)ticket + " close & disable trading band -> " + DoubleToStr(prc, Digits));
      }
      else
       LOG_DEBUG("Successfully modified #" + (string)ticket + " close & disable trading band -> " + DoubleToStr(prc, Digits));
     }
    }
   }
   
   LOG_DEBUG("Deleting obj: " + name);
   ObjectDelete(chart_id, name);
  }
 }
}

void CreateCloseAndDisableTradingBand(double prc)
{
 string name = BAND_NAME;
   
 ObjectCreate(name, OBJ_HLINE, 0, 0, prc);
 ObjectSet(name, OBJPROP_COLOR, clrChartreuse);
}