//+------------------------------------------------------------------+
//|                                           AbstractTakeProfit.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#ifndef LOGGER
#define LOGGER
   #define LOGGER_DEPRECATED
   #include <Logger.mqh>
#endif

#ifndef ARRAYINT
#define ARRAYINT
   #include <Arrays/ArrayInt.mqh>
#endif 

#ifndef ARRAYDOUBLE
#define ARRAYDOUBLE
   #include <Arrays/ArrayDouble.mqh>
#endif

#ifndef STRINGS
#define STRINGS
   #include <Packages/Strings.mqh>
#endif 

#ifndef MAP
#define MAP
   #include <Map.mqh>
#endif 

struct TriggeredTakeProfit
{
 int ticket;
 datetime triggered_timestamp;
 double abstract_take_profit;
};

class AbstractTakeProfit
{
 private:
  Logger logger;
  
 protected:
  CArrayInt p_ticket;
  CArrayInt p_type;
  CArrayInt p_activated;
  CArrayDouble p_soft_tp;
  TriggeredTakeProfit p_pending_close[];
  int p_pending_close_size;
  color p_clr;
  string p_tp_prefix;
  
  void activate_pending_close(int ticket, double tp);
  void remove_pending_close(int pos);
  void check_for_closed_trades();
  void myInit();
 
  virtual void handle_pending_close(TriggeredTakeProfit& pending_close[], int arr_size)=0; 
  bool is_activated(int ticket);
  
  AbstractTakeProfit(string TP_PREFIX, color clr=clrYellow);
  ~AbstractTakeProfit();
 
 public:
  void tick_engine(); 
  void chart_drag_engine(string obj_name);
  void chart_delete_engine(string obj_name);
  bool set(int ticket, double soft_take_profit, double hard_take_profit=0);
  bool remove(int ticket, bool delete_obj=true);
  bool modify(int ticket, double soft_take_profit);
  void deactivate_pending_close(int ticket);
};

bool AbstractTakeProfit::is_activated(int ticket)
{
 int i = p_ticket.SearchLinear(ticket);
 
 if(i>=0)
  return(p_activated[i]);
 
 return false; 
}

void AbstractTakeProfit::chart_delete_engine(string obj_name)
{
 string result[];
 StringSplit(obj_name, '-', result);
 
 if(ArraySize(result)==2)
 {
  int ticket = (int)StringTrimRight(StringTrimLeft(result[1]));

  int pos = p_ticket.SearchLinear(ticket);
  
  if(pos>=0)
   remove(ticket, false);
 }
 else
  LOG_HIGH("Could not parse grid chart name = " + obj_name);
}

void AbstractTakeProfit::chart_drag_engine(string obj_name)
{
 double new_price=NormalizeDouble(ObjectGet(obj_name, OBJPROP_PRICE1), Digits);
 
 string result[];
 StringSplit(obj_name, '-', result);
 
 if(ArraySize(result)==2)
 {
  int ticket = (int)StringTrimRight(StringTrimLeft(result[1]));

  int pos = p_ticket.SearchLinear(ticket);
  
  if(pos>=0)
  {
   if(MathAbs(p_soft_tp[pos] - new_price) > DBL_EPSILON)
   {
    LOG_INFO("Changing soft take profit ticket #" + (string)ticket + " from " + DoubleToStr(p_soft_tp[pos], Digits) + " to " + DoubleToStr(new_price, Digits));
    p_soft_tp.Update(pos, new_price);
   }
  }
  else
   LOG_HIGH("Cannot modify #" + (string)ticket + " for ticket has not been added");
 }
 else
  LOG_HIGH("Could not parse grid chart name = " + obj_name);
}

AbstractTakeProfit::tick_engine()
{
 check_for_closed_trades();
 
 int total = p_ticket.Total();
 
 for(int i=total-1; i >= 0; i--)
 {
  if(p_activated[i]==False)
  {
   bool triggered = false;
   
   if(p_type[i]==OP_BUY)
   {
    if(Bid >= p_soft_tp[i])
     triggered = true;
   }
  
   if(p_type[i]==OP_SELL)
   {
    if(Bid <= p_soft_tp[i])
     triggered = true;
   }
   
   if(triggered)
   {
    p_activated.Update(i, True);
     
    activate_pending_close(p_ticket[i], p_soft_tp[i]);
     
    LOG_INFO("#" + (string)p_ticket[i] + " soft take profit has been triggered");
   }
  }
 }
 
 handle_pending_close(p_pending_close, p_pending_close_size);
}

void AbstractTakeProfit::activate_pending_close(int ticket, double tp)
{
  p_pending_close_size++;
  ArrayResize(p_pending_close, p_pending_close_size);
  int k = p_pending_close_size-1;
  p_pending_close[k].ticket = ticket;
  p_pending_close[k].abstract_take_profit = tp;
  p_pending_close[k].triggered_timestamp = TimeCurrent();
}

void AbstractTakeProfit::remove_pending_close(int pos)
{
 int i = pos;

 int ticket = p_pending_close[i].ticket;
   
 for(int k=i+1; k<p_pending_close_size; k++)
 {
  p_pending_close[k-1].ticket = p_pending_close[k].ticket;
  p_pending_close[k-1].abstract_take_profit = p_pending_close[k].abstract_take_profit;
  p_pending_close[k-1].triggered_timestamp = p_pending_close[k].triggered_timestamp;
 }
 
 p_pending_close_size--;
  
 ArrayResize(p_pending_close, p_pending_close_size);
}

void AbstractTakeProfit::deactivate_pending_close(int ticket)
{
 int i = p_ticket.SearchLinear(ticket);
  
 if(i>=0)
 {
  p_activated.Update(i, False);
  LOG_INFO("Deactivated previously triggered #" + (string)ticket);
 }
 else
  LOG_HIGH("Unable to find #" + (string)ticket);
}

AbstractTakeProfit::~AbstractTakeProfit()
{
}

AbstractTakeProfit::AbstractTakeProfit(string TP_PREFIX, color clr=clrYellow) : logger(__FILE__, "helper_processes")
{
 p_pending_close_size = 0;
 p_clr = clr;
 p_tp_prefix=TP_PREFIX;
 
 myInit();
}

bool AbstractTakeProfit::modify(int ticket,double soft_tp)
{
 int pos=p_ticket.SearchLinear(ticket);
 
 if(pos >= 0)
 {
  string name = StringConcatenate(p_tp_prefix, " - ", ticket);
  double prev = ObjectGet(name, OBJPROP_PRICE1);
  
  if(ObjectSet(name, OBJPROP_PRICE1, soft_tp))
  {
   p_soft_tp.Update(pos, soft_tp);
   LOG_INFO("Modified #" + IntegerToString(ticket) + " soft take profit from " + DoubleToStr(prev, Digits) + " to " + DoubleToStr(soft_tp, Digits));
   return true;
  }
  else
   LOG_LOW_ERROR("Could not modify #" + (string)ticket + ", name=" + name);
 } 

 return false;
}

bool AbstractTakeProfit::remove(int ticket, bool delete_obj=true)
{
 int pos=p_ticket.SearchLinear(ticket);
 
 if(pos >= 0)
 {
  string name = StringConcatenate(p_tp_prefix, " - ", ticket);
  
  if(!delete_obj || ObjectDelete(name))
  {
   p_ticket.Delete(pos);
   p_soft_tp.Delete(pos);
   p_type.Delete(pos);
   p_activated.Delete(pos);
   LOG_DEBUG("Removed #" + (string)ticket + " soft take profit");
   
   //--- Delete order from pending_close queue if added
   int i;

   for(i=0; i<p_pending_close_size; i++)
   {
    if(p_pending_close[i].ticket == ticket) 
    {
     remove_pending_close(i);
     return true;
    }
   }
  }
  else
   LOG_DEBUG_ERROR("Could not remove #" + (string)ticket + ", name=" + name);
 }
 
 LOG_DEBUG("Could not find #" + (string)ticket);
 
 return false;
}

bool AbstractTakeProfit::set(int ticket, double soft_tp, double hard_tp=0)
{
 string name = StringConcatenate(p_tp_prefix, " - ", ticket);
 
 //--- set hard take profit
 if(hard_tp > 0)
  if(OrderSelect(ticket, SELECT_BY_TICKET))
   bool ans = OrderModify(ticket, OrderOpenPrice(), OrderStopLoss(), hard_tp, 0);
 
 //--- set soft take profit
 if(OrderSelect(ticket, SELECT_BY_TICKET))
 {
  int ord_type=OrderType();
  double open_prc=OrderOpenPrice();
  bool pass=false;
  string err=NULL;
  
  if(ord_type==OP_BUY)
  {
   pass = soft_tp > open_prc && (MathAbs(hard_tp) < DBL_EPSILON || hard_tp > open_prc);
   
   if(!pass)
    err="Soft and hard take profit must be greater than open prc";
  }
  
  if(ord_type==OP_SELL)
  {
   pass = soft_tp < open_prc && hard_tp < open_prc;
   
   if(!pass)
    err="Soft and hard take profit must be less than open prc";
  }
  
  if(pass)
  {
   if(ObjectCreate(name, OBJ_HLINE, 0, 0, soft_tp))
   {
    string order_symbol=OrderSymbol();
    string symbol=Symbol();
    if(order_symbol==symbol)
    {
     int type = OrderType();
    
     if(type==OP_BUY || type==OP_SELL)
     {
      ObjectSet(name, OBJPROP_COLOR, p_clr);
      ObjectSet(name, OBJPROP_STYLE, STYLE_DASHDOT);
      p_ticket.Add(ticket);
      p_soft_tp.Add(soft_tp);
      p_type.Add(type);
      p_activated.Add(False);
     
      return true;
     }
     else
      LOG_HIGH("#" + (string)ticket + " must be a market order");
    }
    else
     LOG_HIGH("#" + (string)ticket + " must have same symbol as chart symbol, symbol=" + symbol + ", order_symbol=" + order_symbol);
   }
   else
    LOG_HIGH_ERROR("Could not set soft take profit, name=" + name);
  }
  else {
   LOG_HIGH(err);
   LOG_HIGH_3(soft_tp, hard_tp, open_prc);
  }
 }
 else
  LOG_HIGH("Unable to select ticket #" + (string)ticket);
 
 return false;
}

void AbstractTakeProfit::check_for_closed_trades(void)
{
 int total = p_ticket.Total();
 
 for(int i=total-1; i>=0; i--)
 {
  if(OrderSelect(p_ticket[i],SELECT_BY_TICKET))
  {
   if(OrderCloseTime() > 0)
   {
    LOG_INFO("Removing #" + IntegerToString(p_ticket[i]) + " for ticket is closed");
    remove(p_ticket[i]);
   }
  }
 }
}

void AbstractTakeProfit::myInit()
{
  int total = ObjectsTotal();
 
  for(int i=0; i<total; i++)
  {
   string name = ObjectName(i);
  
   if(StringFind(name, p_tp_prefix) >= 0)
   {
    string result[];
    StringParse(name, p_tp_prefix, '-', result);
   
    if(ArraySize(result)==2)
    { 
     int ticket = (int)result[1];
   
     if(OrderSelect(ticket, SELECT_BY_TICKET))
     {
      if(OrderSymbol()==Symbol())
      {
       int type = OrderType();
    
       if(type==OP_BUY || type==OP_SELL)
       {
        double take_profit = ObjectGet(name, OBJPROP_PRICE1);
       
        ObjectSet(name, OBJPROP_COLOR, p_clr);
        ObjectSet(name, OBJPROP_STYLE, STYLE_DASHDOT);
        p_ticket.Add(ticket);
        p_soft_tp.Add(take_profit);
        p_type.Add(type);
        p_activated.Add(False);
     
        LOG_INFO("Backfilling #" + (string)ticket);
       }
       else
        LOG_HIGH("#" + (string)ticket + " must be a market order");
      }
      else
       LOG_HIGH("#" + (string)ticket + " must have same symbol as chart symbol");
     }
     else
      LOG_HIGH("Unable to select ticket #" + (string)ticket);
    }
   else
    LOG_HIGH("Invalid parse of name=" + name); 
  }
 }
}