//+------------------------------------------------------------------+
//|                                               BollingerEntry.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#ifndef EVENTS
#define EVENTS
   #include <Packages/Events.mqh>
#endif

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
#endif 

enum EVENT_BOLLINGER_SIGNAL_BAR
{
 BOLLINGER_SIGNAL_BAR_NIL,
 BOLLINGER_SIGNAL_BAR_INTERIOR,
 BOLLINGER_SIGNAL_BAR_CLOSE_ABOVE,
 BOLLINGER_SIGNAL_BAR_CLOSE_BELOW,
 BOLLINGER_SIGNAL_BAR_WHIPSAW,
 BOLLINGER_SIGNAL_BAR_REJECTED_ABOVE,
 BOLLINGER_SIGNAL_BAR_REJECTED_BELOW,
 BOLLINGER_SIGNAL_BAR_TOUCH_ABOVE,
 BOLLINGER_SIGNAL_BAR_TOUCH_BELOW
};

class BollingerEntry
{
 private:
  ENUM_TIMEFRAMES p_timeframe;
  int p_period;
  double p_deviation;
  bool p_band_touch_above;
  bool p_band_touch_below;
  Logger p_logger;
  Events e;

 public:
  EVENT_BOLLINGER_SIGNAL_BAR tick_engine();
  double get_bollinger_upper();
  double get_bollinger_lower();
  BollingerEntry(int period, double deviation, ENUM_TIMEFRAMES timeframe=PERIOD_CURRENT);
};

EVENT_BOLLINGER_SIGNAL_BAR BollingerEntry::tick_engine()
{
 double boll_upper = get_bollinger_upper();
 double boll_lower = get_bollinger_lower();
 
 if(e.NewBar(p_timeframe)) 
 {
  //--- Reset variables on NewBar()
   p_band_touch_above = false;
   p_band_touch_below = false;
   p_logger.log("NewBar(), tf=" + EnumToString(p_timeframe) + " reset touch above/below variables", LOG_DEBUG, __LINE__, __FUNCTION__);
  
  //--- Check for event at previous bar close
   if(Close[1] >= boll_upper) {
    p_logger.log(EnumToString(BOLLINGER_SIGNAL_BAR_CLOSE_ABOVE), LOG_INFO, __LINE__, __FUNCTION__);
    return BOLLINGER_SIGNAL_BAR_CLOSE_ABOVE;
   }
  
   if(Close[1] <= boll_lower) {
    p_logger.log(EnumToString(BOLLINGER_SIGNAL_BAR_CLOSE_BELOW), LOG_INFO, __LINE__, __FUNCTION__);
    return BOLLINGER_SIGNAL_BAR_CLOSE_BELOW;
   }
   
   if(High[1] >= boll_upper && Low[1] <= boll_lower) {
    p_logger.log(EnumToString(BOLLINGER_SIGNAL_BAR_WHIPSAW), LOG_INFO, __LINE__, __FUNCTION__);
    return BOLLINGER_SIGNAL_BAR_WHIPSAW;
   }
  
   if(High[1] >= boll_upper) {
    p_logger.log(EnumToString(BOLLINGER_SIGNAL_BAR_REJECTED_ABOVE), LOG_INFO, __LINE__, __FUNCTION__);
    return BOLLINGER_SIGNAL_BAR_REJECTED_ABOVE;
   }
  
   if(Low[1] <= boll_lower) {
    p_logger.log(EnumToString(BOLLINGER_SIGNAL_BAR_REJECTED_BELOW), LOG_INFO, __LINE__, __FUNCTION__);
    return BOLLINGER_SIGNAL_BAR_REJECTED_BELOW;
   }
 
   p_logger.log(EnumToString(BOLLINGER_SIGNAL_BAR_INTERIOR), LOG_INFO, __LINE__, __FUNCTION__);
   return BOLLINGER_SIGNAL_BAR_INTERIOR;
 }
 
 //--- Check for bollinger touch if has not occurred within current bar
  if(!p_band_touch_above && Bid >= boll_upper)
  {
   p_band_touch_above = true;
   p_logger.log(EnumToString(BOLLINGER_SIGNAL_BAR_TOUCH_ABOVE), LOG_INFO, __LINE__, __FUNCTION__);
   return BOLLINGER_SIGNAL_BAR_TOUCH_ABOVE;
  }
  
  if(!p_band_touch_below && Bid <= boll_lower)
  {
   p_band_touch_below = true;
   p_logger.log(EnumToString(BOLLINGER_SIGNAL_BAR_TOUCH_BELOW), LOG_INFO, __LINE__, __FUNCTION__);
   return BOLLINGER_SIGNAL_BAR_TOUCH_BELOW;
  }
  
  return BOLLINGER_SIGNAL_BAR_NIL;
}

double BollingerEntry::get_bollinger_upper()
{
 return iBands(NULL, p_timeframe, p_period, p_deviation, 0, PRICE_CLOSE, MODE_UPPER, 1);
}

double BollingerEntry::get_bollinger_lower()
{
 return iBands(NULL, p_timeframe, p_period, p_deviation, 0, PRICE_CLOSE, MODE_LOWER, 1);
}

BollingerEntry::BollingerEntry(int period, double deviation, ENUM_TIMEFRAMES timeframe=PERIOD_CURRENT)
 : p_logger(__FILE__, "helper_processes")
{
 p_period = period;
 p_deviation = deviation;
 p_timeframe = timeframe;
 p_band_touch_above = false;
 p_band_touch_below = false;
}
