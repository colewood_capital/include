//+------------------------------------------------------------------+
//|                                               SoftTakeProfit.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#ifndef LOGGER
#define LOGGER
   #define LOGGER_DEPRECATED
   #include <Logger.mqh>
#endif

#ifndef ARRAYINT
#define ARRAYINT
   #include <Arrays/ArrayInt.mqh>
#endif 

#ifndef ARRAYDOUBLE
#define ARRAYDOUBLE
   #include <Arrays/ArrayDouble.mqh>
#endif

#ifndef MAP
#define MAP
   #include <Map.mqh>
#endif 

#define SOFT_TP_PREFIX     "SoftTakeProfit"

Logger softtakeprofitlogger(__FILE__, "helper_processes");

struct SoftTakeProfitTicket
{
 int ticket;
 double soft_take_profit;
};

class SoftTakeProfit
{
 private:
  CArrayInt p_ticket;
  CArrayInt p_type;
  CArrayInt p_activated;
  CArrayDouble p_soft_tp;
  SoftTakeProfitTicket p_pending_close[];
  int p_pending_close_size;
  color p_clr;
  
  void check_for_closed_trades();
 
 public:
  void engine_tick();
  void engine_chart_drag(string obj_name);
  void engine_chart_delete(string obj_name);
  bool set(int ticket, double soft_take_profit, double hard_stop_loss=0);
  bool remove(int ticket, bool delete_obj=true);
  bool modify(int ticket, double soft_take_profit);
  int get_pending_close(SoftTakeProfitTicket& pending_close[]);
 
  SoftTakeProfit(color clr=clrBlue);
  ~SoftTakeProfit();
};

void SoftTakeProfit::engine_chart_delete(string obj_name)
{
 string result[];
 StringSplit(obj_name, '-', result);
 
 if(ArraySize(result)==2)
 {
  int ticket = (int)StringTrimRight(StringTrimLeft(result[1]));

  int pos = p_ticket.SearchLinear(ticket);
  
  if(pos >=0)
   remove(ticket, false);
 }
 else
  softtakeprofitlogger.log("Could not parse grid chart name = " + obj_name, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
}

void SoftTakeProfit::engine_chart_drag(string obj_name)
{
 double new_price=NormalizeDouble(ObjectGet(obj_name, OBJPROP_PRICE1), Digits);
 
 string result[];
 StringSplit(obj_name, '-', result);
 
 if(ArraySize(result)==2)
 {
  int ticket = (int)StringTrimRight(StringTrimLeft(result[1]));

  int pos = p_ticket.SearchLinear(ticket);
  
  if(pos >=0)
  {
   if(MathAbs(p_soft_tp[pos] - new_price) > DBL_EPSILON)
   {
    softtakeprofitlogger.log("Changing soft take profit ticket #" + (string)ticket + " from " + DoubleToStr(p_soft_tp[pos], Digits) + " to " + DoubleToStr(new_price, Digits), LOG_TYPE_INFO, __LINE__, __FUNCTION__);
    p_soft_tp.Update(pos, new_price);
   }
  }
  else
   softtakeprofitlogger.log("Cannot modify #" + (string)ticket + " for ticket has not been added", LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
 }
 else
  softtakeprofitlogger.log("Could not parse grid chart name = " + obj_name, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
}

SoftTakeProfit::engine_tick()
{
 check_for_closed_trades();
 
 int total = p_ticket.Total();
 
 for(int i=total-1; i >= 0; i--)
 {
  if(p_activated[i]==False)
  {
   bool triggered = false;
   
   if(p_type[i]==OP_BUY)
   {
    if(Bid >= p_soft_tp[i])
     triggered = true;
   }
  
   if(p_type[i]==OP_SELL)
   {
    if(Bid <= p_soft_tp[i])
     triggered = true;
   }
   
   if(triggered)
   {
    p_activated.Update(i, True);
     
    p_pending_close_size++;

    ArrayResize(p_pending_close, p_pending_close_size);

    int k = p_pending_close_size-1;

    p_pending_close[k].ticket = p_ticket[i];
    p_pending_close[k].soft_take_profit = p_soft_tp[i];
     
    softtakeprofitlogger.log("#" + (string)p_ticket[i] + " soft take profit has been triggered", LOG_TYPE_INFO, __LINE__, __FUNCTION__);
   }
  }
 }
}

SoftTakeProfit::~SoftTakeProfit()
{
 int total = p_ticket.Total();
 
 for(int i=total-1; i>=0; i--)
 {
  string name = StringConcatenate(SOFT_TP_PREFIX, " - ", p_ticket[i]);
  
  if(!ObjectDelete(name))
    softtakeprofitlogger.log("Could not remove #" + (string)p_ticket[i] + ", name=" + name + ", e=" + (string)GetLastError(), LOG_TYPE_LOW, __LINE__, __FUNCTION__);
 } 
}

SoftTakeProfit::SoftTakeProfit(color clr=clrBlue)
{
 p_pending_close_size = 0;

 if(clr==clrRed) {
  clr = clrBlue;
  softtakeprofitlogger.log("clrRed is a reserved color, changing color to default clrYellow", LOG_TYPE_WARNING, __LINE__, __FUNCTION__);
 }
 
 p_clr = clr;
}

bool SoftTakeProfit::modify(int ticket,double soft_tp)
{
 int pos=p_ticket.SearchLinear(ticket);
 
 if(pos >= 0)
 {
  string name = StringConcatenate(SOFT_TP_PREFIX, " - ", ticket);
  double prev = ObjectGet(name, OBJPROP_PRICE1);
  
  if(ObjectSet(name, OBJPROP_PRICE1, soft_tp))
  {
   softtakeprofitlogger.log("Modified #" + IntegerToString(ticket) + " soft take profit from " + DoubleToStr(prev, Digits) + " to " + DoubleToStr(soft_tp, Digits), LOG_TYPE_INFO, __LINE__, __FUNCTION__);
   return true;
  }
  else
   softtakeprofitlogger.log("Could not modify #" + (string)ticket + ", name=" + name + ", e=" + (string)GetLastError(), LOG_TYPE_LOW, __LINE__, __FUNCTION__);
 } 

 return false;
}

bool SoftTakeProfit::remove(int ticket, bool delete_obj=true)
{
 int pos=p_ticket.SearchLinear(ticket);
 
 if(pos >= 0)
 {
  string name = StringConcatenate(SOFT_TP_PREFIX, " - ", ticket);
  
  if(!delete_obj || ObjectDelete(name))
  {
   p_ticket.Delete(pos);
   p_soft_tp.Delete(pos);
   p_type.Delete(pos);
   p_activated.Delete(pos);
   softtakeprofitlogger.log("Removed #" + (string)ticket + " soft take profit", LOG_TYPE_INFO, __LINE__, __FUNCTION__);
   
   //--- Delete order from pending_close queue if added
   int i;
   bool found_ticket = false;
   
   for(i=0; i<p_pending_close_size; i++)
   {
    if(p_pending_close[i].ticket == ticket) {
     found_ticket = true;
     break;
    }
   }
   
   if(found_ticket)
   {
    for(int k=i+1; k<p_pending_close_size; k++)
    {
     p_pending_close[k-1].ticket = p_pending_close[k].ticket;
     p_pending_close[k-1].soft_take_profit = p_pending_close[k].soft_take_profit;
    }
   
    p_pending_close_size--;
    ArrayResize(p_pending_close, p_pending_close_size);
   }
   //---
   
   return true;
  }
  else
   softtakeprofitlogger.log("Could not remove #" + (string)ticket + ", name=" + name + ", e=" + (string)GetLastError(), LOG_TYPE_LOW, __LINE__, __FUNCTION__);
 }
 else
  softtakeprofitlogger.log("Could not locate ticket #" + (string)ticket, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
 
 return false;
}

bool SoftTakeProfit::set(int ticket, double soft_tp, double hard_tp=0)
{
 string name = StringConcatenate(SOFT_TP_PREFIX, " - ", ticket);
 
 //--- set hard take profit
 if(hard_tp > 0)
  if(OrderSelect(ticket, SELECT_BY_TICKET))
   bool ans = OrderModify(ticket, OrderOpenPrice(), OrderStopLoss(), hard_tp, 0);
 
 //--- set soft take profit
 if(ObjectCreate(name, OBJ_HLINE, 0, 0, soft_tp))
 {
  if(OrderSelect(ticket, SELECT_BY_TICKET))
  {
   if(OrderSymbol()==Symbol())
   {
    int type = OrderType();
    
    if(type==OP_BUY || type==OP_SELL)
    {
     ObjectSet(name, OBJPROP_COLOR, p_clr);
     ObjectSet(name, OBJPROP_STYLE, STYLE_DASHDOT);
     p_ticket.Add(ticket);
     p_soft_tp.Add(soft_tp);
     p_type.Add(type);
     p_activated.Add(False);
     
     return true;
    }
    else
     softtakeprofitlogger.log("#" + (string)ticket + " must be a market order", LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
   }
   else
    softtakeprofitlogger.log("#" + (string)ticket + " must have same symbol as chart symbol", LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
  }
  else
   softtakeprofitlogger.log("Unable to select ticket #" + (string)ticket, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
 }
 else
  softtakeprofitlogger.log("Could not set soft take profit, name=" + name + ", e=" + (string)GetLastError(), LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
 
 return false;
}

void SoftTakeProfit::check_for_closed_trades(void)
{
 int total = p_ticket.Total();
 
 for(int i=total-1; i>=0; i--)
 {
  if(OrderSelect(p_ticket[i],SELECT_BY_TICKET))
  {
   if(OrderCloseTime() > 0)
   {
    softtakeprofitlogger.log("Removing #" + IntegerToString(p_ticket[i]) + " for ticket is closed", LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
    remove(p_ticket[i]);
   }
  }
 }
}

int SoftTakeProfit::get_pending_close(SoftTakeProfitTicket &pending_close[])
{
 ArrayCopy(pending_close, p_pending_close);
 return p_pending_close_size;
}
