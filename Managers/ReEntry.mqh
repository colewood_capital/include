//+------------------------------------------------------------------+
//|                                                      ReEntry.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#ifndef ENUMS
#define ENUMS
   #include <Super\Enums.mqh>
#endif 

#ifndef MANAGERS
#define MANAGERS
   #include <Managers/Managers.mqh>
#endif 

#ifndef DRAW
#define DRAW
   #include <Tasks/Draw.mqh>
#endif

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
#endif

#ifndef UTILS2
#define UTILS2
   #include <Utils2.mqh>
#endif 

enum ENUM_REENTRY
{
 REENTRY_NIL,
 REENTRY_TRIGGERED
};

class ReEntry : public Managers
{
 protected:
  CArrayInt p_reentry_count;
  CArrayInt p_reentry_max;
  CArrayInt p_new_ticket;
  
  Logger logger;
  
 public:
  void add(int ticket, int max_reentries, int count_reentries);
  int remove(int ticket);
  bool set(int ticket, double price);
  void chart_event_open_engine(const int ticket, KwargsDict &kwargs);
  void chart_event_close_engine(const int ticket);
  void clear();
  void tick_engine();
  
  ReEntry();
};

ReEntry::ReEntry(void) : logger(__FILE__, "Managers")
{
 int obj_total = ObjectsTotal(0, -1, -1);
 
 for(int i=0; i<obj_total; i++)
 { 
  string name = ObjectName(0, i);
  
  if(StringFind(name, REENTRY_PREFIX) == 0)
  {
   string result[];
   int n = StringSplit(name, '-', result);
   if(n==2)
   {  
    int n1=StringFind(result[0],"[") + 1;
    int len=StringFind(result[0],"]") - n1;
    int ticket = (int)StringSubstr(result[0], n1, len);
     
    string desc = ObjectGetString(0, name, OBJPROP_TEXT);
    string res2[];
    StringSplit(desc, '/', res2);
    
    if(ArraySize(res2)==2)
    {
     int reentries_count = (int)res2[0];
     int reentries_max = (int)res2[1];
     add(ticket, reentries_max, reentries_count);
     logger.log("Re-adding #" + (string)ticket + ", count=" + (string)reentries_count + ", max=" + (string)reentries_max, LOG_TYPE_INFO, __LINE__, __FUNCTION__);
    }
   }
  }
 }
}


void ReEntry::add(int ticket, int max_reentries, int count_reentries=0)
{
 bool result = Managers::add(ticket);
 
 if(result)
 {
  p_reentry_count.Add(count_reentries);
  p_reentry_max.Add(max_reentries);
  p_new_ticket.Add(ticket);
 }
}


int ReEntry::remove(int ticket)
{
 int pos = Managers::remove(ticket);
 
 if(pos >= 0)
 {
  p_reentry_count.Delete(pos);
  p_new_ticket.Delete(pos);
  p_reentry_max.Delete(pos);
  
  logger.log("Removed #" + (string)ticket, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
 }
 else
  logger.log("Could not find ticket #" + (string)ticket, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
 
 return pos;
}


void ReEntry::chart_event_open_engine(const int ticket, KwargsDict &kwargs)
{
  if(OrderSelect(ticket,SELECT_BY_TICKET))
  {
   if(OrderSymbol() != p_manager_symbol) {
    logger.log("cannot set #" + (string)ticket + " for ticket symbol != " + p_manager_symbol, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
    return;   
   }
   
   int reentries = (int)kwargs["reentry"];
 
   if(reentries > 0)
   {
    add(ticket, reentries);
    logger.log("Added #" + (string)ticket + ", reentries=" + (string)reentries, LOG_TYPE_INFO, __LINE__, __FUNCTION__);
   }
 
   string cmt = OrderComment();
   string prefix = "re=";
   int prefix_size = StringLen(prefix);
   int beg = StringFind(cmt, prefix);
   if(beg==0)
   {
    int delimiter = StringFind(cmt, "`");
    string order_cmt = StringSubstr(cmt, beg+prefix_size, delimiter-prefix_size);
    logger.log("order_cmt: " + order_cmt, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
    int ti = (int)order_cmt;
    
    int pos = p_ticket.SearchLinear(ti);
    
    if(pos >= 0)
    {
     //--- update re-entry count
      int new_reentry_count = p_reentry_count[pos]+1;
      logger.log("Updating re-entry count: " + (string)p_reentry_count[pos] + " -> " + (string)new_reentry_count, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
      p_reentry_count.Update(pos, new_reentry_count);
      
     //--- remove ticket if max reached
      if(p_reentry_count[pos] >= p_reentry_max[pos])
      {
       logger.log("Removing #" + (string)p_ticket[pos] + " for re-entry count(" + (string)p_reentry_count[pos] + ") >= max(" + (string)p_reentry_max[pos] + ")", LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
       remove(p_ticket[pos]);
       return;
      }
     
     //--- update new ticket
      logger.log("Updating p_new_ticket: #" + (string)p_new_ticket[pos] + " -> " + (string)ticket + ", original ticket #" + (string)p_ticket[pos], LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
      p_new_ticket.Update(pos, ticket);
    }
   } 
  }
}


void ReEntry::chart_event_close_engine(const int ticket)
{
 int k = p_new_ticket.SearchLinear(ticket);
 
 if(k >= 0)
 {
  if(OrderSelect(p_new_ticket[k],SELECT_BY_TICKET))
  {
   if(OrderCloseTime() > 0)
   {
    int type = OrderType();
    double open_price = OrderOpenPrice();
    double close_price = OrderClosePrice();
    bool reopen = false;
  
    if(type==OP_BUY)
    {
     if(close_price < open_price)
      reopen = true;
     else
      logger.log("Removing [BUY] for close_price=" + DoubleToStr(close_price, Digits) + " < open_price=" + DoubleToStr(open_price, Digits), LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
    }
  
    if(type==OP_SELL)
    {
     if(close_price > open_price)
      reopen = true;
     else
      logger.log("Removing [SELL] for close_price=" + DoubleToStr(close_price, Digits) + " > open_price=" + DoubleToStr(open_price, Digits), LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
    }
  
    if(reopen)
    {
     set(p_ticket[k], open_price);
    }
    else
    {
     remove(p_ticket[k]);
    }
   }
  }
  else
   logger.log("Couldn't select #" + (string)ticket, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
 }
}


void ReEntry::clear(void)
{
 int total = ObjectsTotal(0, -1, -1);
 
 for(int i=total-1; i>=0; i--)
 {
  string name = ObjectName(0, i);

  if(StringFind(name, REENTRY_PREFIX) == 0)
   ObjectDelete(name);
 }
}


void ReEntry::tick_engine()
{
 bool triggered = false;
 int obj_total = ObjectsTotal(0, -1, -1);
 
 for(int i=0; i<obj_total; i++)
 { 
  string name = ObjectName(0, i);
  double open_price = ObjectGet(name, OBJPROP_PRICE1);
  
  if(StringFind(name, REENTRY_PREFIX) == 0)
  {
   string result[];
   int n = StringSplit(name, '-', result);
   if(n==2)
   {
    int type;
    
    if(result[1]=="buy") 
     type = OP_BUY;
    else if(result[1]=="sell")
     type = OP_SELL;
    else {
     logger.log("Could not find order type", LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
     return;
    }

    switch(type)
    {
     case OP_BUY:
      if(MarketInfo(p_manager_symbol, MODE_BID) > open_price) 
       triggered=true;
      break;
      
     case OP_SELL:
      if(MarketInfo(p_manager_symbol, MODE_BID) < open_price)
       triggered=true;
      break;
    }
     
    if(triggered)
    {
     int n1=StringFind(result[0],"[") + 1;
     int len=StringFind(result[0],"]") - n1;
     int ticket = (int)StringSubstr(result[0], n1, len);
     
     int k = p_ticket.SearchLinear(ticket);
     
     if(k>=0)
     {
      if(ObjectDelete(name))
       logger.log("Deleted obj=" + name, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
      else
       logger.log("Could not delete obj=" + name + " for " + ErrorDescription(GetLastError()), LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
       
      double prc=0.0;
      
      if(p_cmd[k]==OP_BUY) 
       prc = MarketInfo(p_symbol[k], MODE_ASK);
      else if(p_cmd[k]==OP_SELL)
       prc = MarketInfo(p_symbol[k], MODE_BID);
      else
       logger.log("Couldn't set prc for cmd=" + (string)p_cmd[k], LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
      
      string attempt = (string)(p_reentry_count[k]+1);

      string cmt = "re=" + (string)p_ticket[k] + "`" + (string)attempt + "/" + (string)p_reentry_max[k] + "|";
      string reason = "Reentry line triggered, cmd=" + EnumToString((ENUM_ORDER_TYPE)p_cmd[k]);
      TradeEngine::open(p_symbol[k], p_cmd[k], p_lots[k], __LINE__, __FUNCTION__, reason, 0.0, cmt);
      logger.log("Open ticket #" + (string)ticket, LOG_TYPE_INFO, __LINE__, __FUNCTION__);
     }
     else
      logger.log("Couldn't find ticket #" + (string)ticket, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
    }
   }
  }
 }
}


bool ReEntry::set(int ticket,double price)
{
 int pos = p_ticket.SearchLinear(ticket);
 
 if(pos >= 0)
 {
 string type;
 
 if(OrderSelect(ticket,SELECT_BY_TICKET))
 {
  if(OrderSymbol() == p_manager_symbol)
  {
   int t=OrderType();
  
   if(t==OP_BUY)
    type = "buy";
   else if(t==OP_SELL)
    type = "sell";
   else {
    logger.log("Can only set reentries from market orders", LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
    return false;
   }
  }
  else {
   logger.log("cannot set #" + (string)ticket + " for ticket symbol != " + p_manager_symbol, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
   return false;
  }
 }
 else 
 {
  logger.log("Could not find ticket #" + (string)ticket, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
  return false;
 }
   
 string name = REENTRY_PREFIX + "[" + (string)ticket + "]-" + type;
 
 bool result =  HLineCreate(name, price);
 
 if(result)
 {
  logger.log("Created reetry, name=" + name, LOG_TYPE_INFO, __LINE__, __FUNCTION__);
  
  string desc = (string)p_reentry_count[pos] + "/" + (string)p_reentry_max[pos];
  
  if(!ObjectSetString(0, name, OBJPROP_TEXT, desc))
   logger.log("Error setting text=" + desc + " for name=" + name, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
 } 
 
 return result;
 }
 else {
  logger.log("Could not set #" + (string)ticket + " for unable to locate in p_tickets", LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
  return false;
 }
}
