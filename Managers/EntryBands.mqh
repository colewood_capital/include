//+------------------------------------------------------------------+
//|                                                   EntryBands.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#define ENTRY_BANDS_UPPER_PREFIX        "EntryBand - upper"
#define ENTRY_BANDS_LOWER_PREFIX        "EntryBand - lower"

#include <Managers/BindHLineArray.mqh>

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
#endif 


enum ENUM_GRID_DIRECTION {
   GRID_DIRECTION_NIL,
   GRID_DIRECTION_UPPER,
   GRID_DIRECTION_LOWER
};

struct GridPoint
{
 ENUM_GRID_DIRECTION direction;
 int level;
 double price;
 
 GridPoint() : direction(GRID_DIRECTION_NIL), level(0), price (0) {};
};

class EntryBands
{
 private:
  BindHLineArray p_bands;
  Logger p_logger;
  void get_closest_bands(GridPoint &lowermost,GridPoint &uppermost, double prc=0);
  
 public:
  void create_upper_band(double price);
  void create_lower_band(double price);
  bool is_trading_allowed(double prc=0);
  void tick_engine();
  void chart_drag_engine(string obj_name, ENUM_GRID_DIRECTION direction);
  void chart_delete_engine(string obj_name, ENUM_GRID_DIRECTION direction);
  
  EntryBands();
};

void EntryBands::chart_delete_engine(string obj_name, ENUM_GRID_DIRECTION direction)
{
 string result[];
 StringSplit(obj_name, '-', result);

 if(ArraySize(result)==3)
 {
  int index = (int)StringTrimRight(StringTrimLeft(result[2]));

  if(direction==GRID_DIRECTION_UPPER)
  {
   HorizontalLineArray* upper = p_bands.get_array(ENTRY_BANDS_UPPER_PREFIX);
   
   if(upper[index] < DBL_MAX)
   {
    if(upper.Delete(index))
    {
     p_logger.log("Deleted element, i=" + (string)index + " in upper[]", LOG_INFO, __LINE__, __FUNCTION__);
     ShiftGridIndexingDown(ENTRY_BANDS_UPPER_PREFIX, index);
     //upper.Bind();
    }
    else
     p_logger.log("Error deleting element, i=" + (string)index + " in upper[]", LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
   }
   else
    p_logger.log("Index=" + (string)index + " already deleted. Note: false signal when object already deleted by ObjectDelete() sends a delete object event signal", LOG_DEBUG, __LINE__, __FUNCTION__);
  } 

  if(direction==GRID_DIRECTION_LOWER)
  {
   HorizontalLineArray* lower = p_bands.get_array(ENTRY_BANDS_LOWER_PREFIX);
   
   if(lower[index] < DBL_MAX)
   {
    if(lower.Delete(index))
    {
     p_logger.log("Deleted element, i=" + (string)index + " in lower[]", LOG_INFO, __LINE__, __FUNCTION__);
     ShiftGridIndexingDown(ENTRY_BANDS_LOWER_PREFIX, index);
     //lower.Bind();
    }
    else
     p_logger.log("Error deleting element, i=" + (string)index + " in lower[]", LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
   }
   else
    p_logger.log("Index=" + (string)index + " already deleted. Note: false signal when object already deleted by ObjectDelete() sends a delete object event signal", LOG_DEBUG, __LINE__, __FUNCTION__);
  }
  
  tick_engine();
 }
 else
  p_logger.log("Could not parse grid chart name = " + obj_name, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
}

void EntryBands::chart_drag_engine(string obj_name, ENUM_GRID_DIRECTION direction)
{
 double new_price=NormalizeDouble(ObjectGet(obj_name, OBJPROP_PRICE1), Digits);
 
 string result[];
 StringSplit(obj_name, '-', result);
 
 if(ArraySize(result)==3)
 {
  int index = (int)StringTrimRight(StringTrimLeft(result[2]));

  if(direction==GRID_DIRECTION_UPPER)
  {
   HorizontalLineArray* upper = p_bands.get_array(ENTRY_BANDS_UPPER_PREFIX);
   
   double prc = NormalizeDouble(ObjectGet(obj_name, OBJPROP_PRICE1), Digits);
   
   if(upper.Update(index, prc))
    p_logger.log("Updated upper[], i=" + (string)index + " to prc=" + DoubleToStr(prc, Digits), LOG_INFO, __LINE__, __FUNCTION__);
   else
    p_logger.log("Error updating upper[], i=" + (string)index + ", prc=" + DoubleToStr(prc, Digits), LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
  } 

  if(direction==GRID_DIRECTION_LOWER)
  {
   HorizontalLineArray* lower = p_bands.get_array(ENTRY_BANDS_LOWER_PREFIX);

   double prc = NormalizeDouble(ObjectGet(obj_name, OBJPROP_PRICE1), Digits);
   
   if(lower.Update(index, prc))
    p_logger.log("Updated lower[], i=" + (string)index + " to prc=" + DoubleToStr(prc, Digits), LOG_INFO, __LINE__, __FUNCTION__);
   else
    p_logger.log("Error updating lower[], i=" + (string)index + ", prc=" + DoubleToStr(prc, Digits), LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
  }
 }
}

bool EntryBands::is_trading_allowed(double prc=0)
{
 bool result = true;
 GridPoint lowermost, uppermost;
 
 get_closest_bands(lowermost, uppermost, prc);
 
 if(lowermost.direction==GRID_DIRECTION_UPPER)
 {
  p_logger.log("Trading not allowed for lowermost band is direction=UPPER", LOG_DEBUG, __LINE__, __FUNCTION__);
  result = false;
 }
 
 if(uppermost.direction==GRID_DIRECTION_LOWER)
 {
  p_logger.log("Trading not allowed for uppermost band is direction=LOWER", LOG_DEBUG, __LINE__, __FUNCTION__);
  result = false;
 }
 
 return result;
}

void EntryBands::get_closest_bands(GridPoint &lowermost,GridPoint &uppermost, double prc=0)
{
 HorizontalLineArray* upper_lvl = p_bands.get_array(ENTRY_BANDS_UPPER_PREFIX);
 HorizontalLineArray* lower_lvl = p_bands.get_array(ENTRY_BANDS_LOWER_PREFIX);
 lowermost.price = 0;
 uppermost.price = DBL_MAX;
 
 if(MathAbs(prc) < DBL_EPSILON) {
  prc = Bid;
  p_logger.log("Setting prc to Bid=" + DoubleToStr(prc, Digits), LOG_DEBUG, __LINE__, __FUNCTION__);
 }
 
 int upper_lvl_total = upper_lvl.Total();
 for(int i=0; i<upper_lvl_total; i++)
 {
  if(upper_lvl[i] > prc && upper_lvl[i] < uppermost.price)
  {
   uppermost.direction = GRID_DIRECTION_UPPER;
   uppermost.price = upper_lvl[i];
  }
  
  if(upper_lvl[i] < prc && upper_lvl[i] > lowermost.price)
  {
   lowermost.direction = GRID_DIRECTION_UPPER;
   lowermost.price = upper_lvl[i];
  }
 }
 
 int lower_lvl_total = lower_lvl.Total();
 for(int i=0; i<lower_lvl_total; i++)
 {
  if(lower_lvl[i] > prc && lower_lvl[i] < uppermost.price)
  {
   uppermost.direction = GRID_DIRECTION_LOWER;
   uppermost.price = lower_lvl[i];
  }
  
  if(lower_lvl[i] < prc && lower_lvl[i] > lowermost.price)
  {
   lowermost.direction = GRID_DIRECTION_LOWER;
   lowermost.price = lower_lvl[i];
  }
 }
 
 p_logger.log("Uppermost: " + EnumToString(uppermost.direction) + ": " + DoubleToStr(uppermost.price, Digits), LOG_DEBUG, __LINE__, __FUNCTION__);
 p_logger.log("Lowermost: " + EnumToString(lowermost.direction) + ": " + DoubleToStr(lowermost.price, Digits), LOG_DEBUG, __LINE__, __FUNCTION__);
}

void EntryBands::tick_engine()
{
 p_bands.tick_engine();
}

void EntryBands::create_upper_band(double price)
{
 HorizontalLineArray* upper = p_bands.get_array(ENTRY_BANDS_UPPER_PREFIX);
 
 int pos = upper.SearchLinear(price);
 
 if(pos < 0)
 {
  upper.Add(price);
  upper.Bind();
  tick_engine();
 }
 else
  p_logger.log("Cannot add same price=" + DoubleToStr(price, Digits) + " twice", LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
}

void EntryBands::create_lower_band(double price)
{
 HorizontalLineArray* lower = p_bands.get_array(ENTRY_BANDS_LOWER_PREFIX);
 
 int pos = lower.SearchLinear(price);
 
 if(pos < 0)
 {
  lower.Add(price);
  lower.Bind();
  tick_engine();
 }
 else
  p_logger.log("Cannot add same price=" + DoubleToStr(price, Digits) + " twice", LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
}

EntryBands::EntryBands(void) : p_logger(__FILE__, "helper_processes")
{
 p_bands.bind(ENTRY_BANDS_LOWER_PREFIX, clrDarkRed);
 p_bands.bind(ENTRY_BANDS_UPPER_PREFIX, clrBlue);
}