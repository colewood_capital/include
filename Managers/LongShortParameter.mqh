//+------------------------------------------------------------------+
//|                                           LongShortParameter.mqh |
//|                                                       Jamal Cole |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Jamal Cole"
#property link      "https://www.mql5.com"
#property strict

#ifndef DRAW
#define DRAW
   #include <Tasks/Draw.mqh>
#endif 

#define X_DISTANCE      1480
#define Y_DISTANCE      600

//--- input parameters of the script
string           InpName1="Long-Button";            // Button name
string           InpName2="Short-Button";            // Button name
ENUM_BASE_CORNER InpCorner=CORNER_RIGHT_LOWER; // Chart corner for anchoring
string           InpFont="Arial";             // Font
int              InpFontSize=11;              // Font size
color            InpColor=clrBlack;           // Text color
color            InpBackColor=C'236,233,216'; // Background color
color            InpBorderColor=clrNONE;      // Border color
bool             InpState=false;              // Pressed/Released
bool             InpBack=false;               // Background object
bool             InpSelection=false;          // Highlight to move
bool             InpHidden=true;              // Hidden in the object list
long             InpZOrder=0;                 // Priority for mouse click

class LongShortParameter
{
 private:
  string p_long_button_name;
  string p_short_button_name;
  void CreateButtons();
  Logger p_logger;
  
 public:
  LongShortParameter() : p_logger(__FILE__, "helper_processes") { CreateButtons(); }
  ~LongShortParameter();
  bool is_long_trading_allowed();
  bool is_short_trading_allowed();
  void enable_long()    { ObjectSetInteger(0,InpName1,OBJPROP_STATE,true); }
  void disable_long()   { ObjectSetInteger(0,InpName1,OBJPROP_STATE,false); }
  void enable_short()   { ObjectSetInteger(0,InpName2,OBJPROP_STATE,true); }
  void disable_short()  { ObjectSetInteger(0,InpName2,OBJPROP_STATE,false); }
};

LongShortParameter::~LongShortParameter()
{
 ButtonDelete(0, p_long_button_name);
 ButtonDelete(0, p_short_button_name);
}

bool LongShortParameter::is_long_trading_allowed()
{
 long state;
 bool result = ObjectGetInteger(0, p_long_button_name, OBJPROP_STATE, 0, state);
 
 if(result)
  return state;
 
 p_logger.log("Unable to determine state for p_long_button_name=" + p_long_button_name, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
 return false;
}

bool LongShortParameter::is_short_trading_allowed()
{
 long state;
 bool result = ObjectGetInteger(0, p_short_button_name, OBJPROP_STATE, 0, state);
 
 if(result)
  return state;
 
 p_logger.log("Unable to determine state for p_short_button_name=" + p_short_button_name, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
 return false;
}

void LongShortParameter::CreateButtons()
{
 //--- create the long button
 {
  int x=(int)X_DISTANCE/12;
  int y=(int)Y_DISTANCE/6;
  int x_size=(int)X_DISTANCE*1/16;
  int y_size=(int)Y_DISTANCE*1/16;
  
  if(ObjectFind(0,InpName1) < 0)
  {
   if(!ButtonCreate(0,InpName1,0,x,y,x_size,y_size,InpCorner,"Long",InpFont,InpFontSize,
     InpColor,InpBackColor,InpBorderColor,InpState,InpBack,InpSelection,InpHidden,InpZOrder))
    {
     p_logger.log("Unable to create 'Long' button for e=" + (string)GetLastError(), LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
    }
  }
 }

 //--- create the short button
 {
  int x=(int)X_DISTANCE/12;
  int y=(int)Y_DISTANCE/10;
  int x_size=(int)X_DISTANCE*1/16;
  int y_size=(int)Y_DISTANCE*1/16;
  
  if(ObjectFind(0,InpName2) < 0)
  {
   if(!ButtonCreate(0,InpName2,0,x,y,x_size,y_size,InpCorner,"Short",InpFont,InpFontSize,
     InpColor,InpBackColor,InpBorderColor,InpState,InpBack,InpSelection,InpHidden,InpZOrder))
     {
      p_logger.log("Unable to create 'Short' button for e=" + (string)GetLastError(), LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
     }
  }
 }
 
 //--- default, press both press
 ObjectSetInteger(0,InpName1,OBJPROP_STATE,true);
 ObjectSetInteger(0,InpName2,OBJPROP_STATE,true);  
 p_long_button_name = InpName1;
 p_short_button_name = InpName2;
      
 //--- redraw the chart
  ChartRedraw();
}
      