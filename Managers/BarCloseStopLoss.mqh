//+------------------------------------------------------------------+
//|                                             BarCloseStopLoss.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#define SL_PREFIX    "BarCloseStopLoss"

#ifndef ABSTRACTSTOPLOSS
#define ABSTRACTSTOPLOSS
   #include <Managers/StopLoss/AbstractStopLoss.mqh>
#endif

#ifndef ORDERMANAGEMENT
#define ORDERMANAGEMENT
   #include <Managers/OrderManagement.mqh>
#endif 

#ifndef EVENTS
#define EVENTS
   #include <Packages/Events.mqh>
#endif 

#ifndef ENUMS
#define ENUMS
   #include <Super/Enums.mqh>
#endif 

class BarCloseStopLoss : public AbstractStopLoss
{
 
 protected:
  bool p_delete_order_management_ptr;
  OrderManagement* p_order_manager;
  void handle_pending_close(TriggeredStopLoss& pending_close[]);
  Events e;
 
 public:
  ~BarCloseStopLoss();
  BarCloseStopLoss(OrderManagement* order_manager=NULL, color clr=clrDarkRed);
};

BarCloseStopLoss::~BarCloseStopLoss()
{
 if(p_delete_order_management_ptr)
  delete p_order_manager;
}

BarCloseStopLoss::BarCloseStopLoss(OrderManagement *existing_order_manager=NULL, color clr=clrDarkRed) 
  : AbstractStopLoss(clr)
{
 if(existing_order_manager)
 {
  p_order_manager = existing_order_manager;
  p_delete_order_management_ptr = false;
 }
 else
 {
  p_order_manager = new OrderManagement(MAGIC_NUMBER_BAR_CLOSE_STOP_LOSS);
  p_delete_order_management_ptr = true;
 }
}

void BarCloseStopLoss::handle_pending_close(TriggeredStopLoss& pending_close[])
{ 
 if(e.NewBar())
 {
  int total = ArraySize(pending_close);
 
  for(int i=total-1; i>=0; i--)
  {
   int ticket = pending_close[i].ticket;
   
   if(!is_activated(ticket) && OrderSelect(ticket,SELECT_BY_TICKET))
   {
    int type = OrderType();
    double sl = pending_close[i].abstract_stop_loss;
    
    if(type==OP_BUY)
    {
     if(Bid > sl)
     {
      p_logger.log("Deactivate #" + (string)ticket + " for Bid=" + DoubleToStr(Bid, Digits) + " < sl=" + DoubleToStr(sl, Digits), LOG_INFO, __LINE__, __FUNCTION__);
      deactivate_pending_close(ticket);
      continue;
     }
    }
    
    if(type==OP_SELL)
    {
     if(Bid < sl)
     {
      p_logger.log("Deactivate #" + (string)ticket + " for Bid=" + DoubleToStr(Bid, Digits) + " > sl=" + DoubleToStr(sl, Digits), LOG_INFO, __LINE__, __FUNCTION__);
      deactivate_pending_close(ticket);
      continue;
     }
    }
   }
   
   p_order_manager.close(ticket);
  }
 }
}
