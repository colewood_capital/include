//+------------------------------------------------------------------+
//|                                                      Managers.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#ifndef ARRAYINT
#define ARRAYINT
   #include <Arrays\ArrayInt.mqh>
#endif

#ifndef ARRAYDOUBLE
#define ARRAYDOUBLE
   #include <Arrays\ArrayDouble.mqh>
#endif 

#ifndef ARRAYSTRING
#define ARRAYSTRING
   #include <Arrays\ArrayString.mqh>
#endif 

#ifndef ARRAYDATETIME
#define ARRAYDATETIME
   #include <Arrays\ArrayDatetime.mqh>
#endif 

#ifndef LOGGER
#define LOGGER
   #define LOGGER_DEPRECATED
   #include <Logger.mqh>
#endif 

#ifndef ENUMS
#define ENUMS
   #include <Super\Enums.mqh>
#endif 

class Managers
{
private:
 Logger logger;
 
protected:
 CArrayInt p_ticket;
 CArrayInt p_cmd;
 CArrayDatetime p_open_time;
 CArrayString p_symbol;
 CArrayDouble p_open_price;
 CArrayDouble p_lots;
 
 int p_total;
 string p_manager_symbol;
 
public:
 virtual int remove(int ticket);
 virtual bool add(int ticket);
 virtual void tick_engine();
 virtual void timer_engine();
 virtual void chart_event_open_engine(const int ticket, KwargsDict &kwargs);
 virtual void chart_event_close_engine(const int ticket);
 virtual string symbol() { return p_manager_symbol; };
 
 Managers(string symbol=NULL) : logger(__FILE__), p_total(0) { p_manager_symbol=(symbol!=NULL) ? symbol : Symbol(); }
};


class Manager
{
private:
 Managers *p_man[];
 int p_total; 
 Logger logger;

public:
 void add(Managers *manager);
 void tick_engine();
 void chart_event_open_engine(const int booknum, KwargsDict &kwargs);
 void chart_event_close_engine(const int booknum);
 
 Manager() : logger(__FILE__) {};
 ~Manager();
};

void Manager::tick_engine()
{
 for(int i=0; i<p_total; i++)
 {
  p_man[i].tick_engine();
 }
}

void Manager::chart_event_open_engine(int booknum, KwargsDict &kwargs)
{
 for(int i=0; i<p_total; i++)
 {
  if(OrderSelect(booknum,SELECT_BY_TICKET))
  {
   if(OrderSymbol()==p_man[i].symbol())
    p_man[i].chart_event_open_engine(booknum, kwargs);
  }
  else
   logger.log("Could not select #" + (string)booknum, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
 }
}

void Manager::chart_event_close_engine(int booknum)
{
 for(int i=0; i<p_total; i++)
  p_man[i].chart_event_close_engine(booknum);
}

Manager::add(Managers *m)
{
 ArrayResize(p_man, ++p_total);
 p_man[p_total-1] = m;
}

Manager::~Manager()
{
 for(int i=0; i<p_total; i++)
 {
  if(CheckPointer(p_man[i])==POINTER_DYNAMIC)
   delete p_man[i];
 }
}

//-----------------------------------


void Managers::tick_engine()
{
}


void Managers::timer_engine()
{
}


void Managers::chart_event_open_engine(const int ticket, KwargsDict &kwargs)
{
}


void Managers::chart_event_close_engine(const int ticket)
{
}

bool Managers::add(int ticket)
{
 if(OrderSelect(ticket,SELECT_BY_TICKET))
 {
  {
   p_ticket.Add(ticket);
   p_cmd.Add(OrderType());
   p_open_time.Add(OrderOpenTime());
   p_symbol.Add(OrderSymbol());
   p_open_price.Add(OrderOpenPrice());
   p_lots.Add(OrderLots());
   
   p_total++;
   logger.log("Added #" + (string)ticket, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
   return true;
  }
 }
 else
  logger.log("Unable to select #" + (string)ticket, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
  
 return false;
}

int Managers::remove(int ticket)
{
 int pos = p_ticket.SearchLinear(ticket);
 
 if(pos >= 0)
 {
  p_ticket.Delete(pos);
  p_cmd.Delete(pos);
  p_open_time.Delete(pos);
  p_symbol.Delete(pos);
  p_open_price.Delete(pos);
  p_lots.Delete(pos);
  
  p_total--;
  logger.log("Removed #" + (string)ticket, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
 }
 else
  logger.log("Unable to find #" + (string)ticket, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
  
 return pos;
}
