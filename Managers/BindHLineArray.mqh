//+------------------------------------------------------------------+
//|                                                         Bind.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#ifndef ARRAYLIST
#define ARRAYLIST
   #include <Arrays/ArrayList.mqh>
#endif

#ifndef ARRAYDOUBLE
#define ARRAYDOUBLE
   #include <Arrays/ArrayDouble.mqh>
#endif 

#ifndef ARRAYSTRING
#define ARRAYSTRING
   #include <Arrays/ArrayString.mqh>
#endif 

#ifndef MAP
#define MAP
   #include <Map.mqh>
#endif

#ifndef UUID
#define UUID
   #include <Packages/UUID.mqh>
#endif

#ifndef DRAW
#define DRAW
   #include <Tasks/Draw.mqh>
#endif

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
#endif

class HorizontalLineArray : public CArrayDouble
{
 protected:
  CArrayString p_comments;
  bool p_is_bound;
  bool err_not_yet_implement(string __FUNCSIG__macro) { p_logger.log(__FUNCSIG__macro + " not yet implemented + ", LOG_TYPE_HIGH, __LINE__, __FUNCTION__); return false; }
  bool p_auto_bind;
  Logger p_logger;
  
 public:
  bool is_bound() { return p_is_bound; }
  void set_binding(bool is_bound) { p_is_bound = is_bound; }
  void Bind() { p_is_bound = false; }
  bool Add(const double element);
  bool AssignArray(const CArrayDouble *src);
  bool Add(const double element, const string comment);
  bool Delete(const int index);
  void Clear(void);
  string get_comment(int pos) { return p_comments[pos]; }
  const CArrayString* get_comments() { return GetPointer(p_comments); }
  
  ~HorizontalLineArray();
  HorizontalLineArray(bool auto_bind=false) : p_logger(__FILE__, "helper_processes"), p_is_bound(true)  { p_auto_bind = auto_bind; }
  
  //--- Not yet implemented
  bool AddArray(const double &src[]) { return err_not_yet_implement(__FUNCSIG__); }
  bool AddArray(const CArrayDouble *src) { return err_not_yet_implement(__FUNCSIG__); }
  bool Insert(const double element,const int pos) { return err_not_yet_implement(__FUNCSIG__); }
  bool InsertArray(const double &src[],const int pos) { return err_not_yet_implement(__FUNCSIG__); }
  bool InsertArray(const CArrayDouble *src,const int pos) { return err_not_yet_implement(__FUNCSIG__); }
  bool AssignArray(const double &src[]) { return err_not_yet_implement(__FUNCSIG__); }
  //doesnt conflict w/ p_comments   bool Update(const int index,const double element) { return err_not_yet_implement(__FUNCSIG__); }
  bool Shift(const int index,const int shift) { return err_not_yet_implement(__FUNCSIG__); }
  bool DeleteRange(int from,int to) { return err_not_yet_implement(__FUNCSIG__); }
};

HorizontalLineArray::~HorizontalLineArray()
{
 Clear();
}

void HorizontalLineArray::Clear(void)
{
 p_comments.Clear();
 
 CArrayDouble::Clear();
 
 if(p_auto_bind)  Bind();
}

bool HorizontalLineArray::Delete(const int index)
{
 bool delete_comment = p_comments.Delete(index);
 
 if(!delete_comment)
  p_logger.log("unable to delete index=" + (string)index, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
 
 bool delete_value = CArrayDouble::Delete(index);
 
 if(!delete_value)
  p_logger.log("unable to delete index=" + (string)index, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
 
 if(p_auto_bind)  Bind();
 
 return delete_value;
}

bool HorizontalLineArray::Add(const double element,const string comment)
{
 bool add_comment = p_comments.Add(comment);
 
 if(!add_comment)
  p_logger.log("unable to add element=" + (string)element + ", comment=" + comment, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
 
 bool add_element = CArrayDouble::Add(element);
 
 if(!add_element)
  p_logger.log("unable to add element=" + (string)element + ", comment=" + comment, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
 
 if(p_auto_bind)  Bind();
 
 return add_element;
}

bool HorizontalLineArray::Add(const double element)
{
 bool add_comment = p_comments.Add(NULL);
 
 if(!add_comment)
  p_logger.log("unable to add element=" + (string)element + ", comment=NULL", LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
 
 bool add_element = CArrayDouble::Add(element);
 
 if(!add_element)
  p_logger.log("unable to add element=" + (string)element + ", comment=NULL", LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
 
 if(p_auto_bind)  Bind();
 
 return add_element;
}

bool HorizontalLineArray::AssignArray(const CArrayDouble *src)
{
 p_comments.Clear();
 
 bool assign_array = CArrayDouble::AssignArray(src);
 
 if(!assign_array)
   p_logger.log("Unable to assign array!!!", LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
 
 if(p_auto_bind)  Bind();
 
 return assign_array;
}



class BindHLineArray
{
 private:
  CArrayList<HorizontalLineArray> p_data_bank;
  UUIDNameGenerator p_uuid; 
  Map<string,uint> p_name_dict;
  Map<uint,uint> p_uuid_index;
  Map<uint,color> p_id_2_color_dict;
  Logger p_logger;
  long p_chart_id;
  
  HorizontalLineArray* New(int uuid);
  HorizontalLineArray* Get(int uuid);
  
 public:
  HorizontalLineArray* bind(string name, color clr=clrGreen);
  HorizontalLineArray* bind(string name, const HorizontalLineArray& data, color clr=clrGreen);
  HorizontalLineArray* get_array(string name);
  void unbind(string name);
  void tick_engine();
  void chart_engine_drag(string obj_name);
  void chart_engine_delete(string obj_name);
  
  BindHLineArray(long chart_id=0) : p_chart_id(chart_id), p_logger(__FILE__, "helper_processes") {};
  ~BindHLineArray();
};

HorizontalLineArray* BindHLineArray::New(int uuid)
{
 int index;
 HorizontalLineArray *p = p_data_bank.Add(index);
 p_uuid_index.set(uuid, index);
 return p;
}

HorizontalLineArray* BindHLineArray::Get(int uuid)
{
 uint i = p_uuid_index[uuid];
 HorizontalLineArray *p = p_data_bank.Get(i);
 return p;
}

void BindHLineArray::chart_engine_delete(string obj_name)
{
 string result[];
 StringSplit(obj_name, '-', result);
 string prefix;
 
 if(ArraySize(result) > 1)
  prefix = result[0] + "-" + StringTrimRight(result[1]);
 else
  p_logger.log("Unable to find name: " + obj_name, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
 
 HorizontalLineArray* arr = get_array(prefix);
 
 if(arr)
 {
  int index = (int)result[2];

  if(arr[index] < DBL_MAX)
  {
   if(arr.Delete(index))
   {
    p_logger.log("Deleted [" + obj_name + "]", LOG_TYPE_INFO, __LINE__, __FUNCTION__);
    ShiftGridIndexingDown(p_chart_id, prefix, index);
   }
   else
    p_logger.log("Error deleting [" + obj_name + "]", LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
  }
  else
  p_logger.log("Index=" + (string)index + " already deleted. Note: false signal when object already deleted by ObjectDelete() sends a delete object event signal", LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
 } 
}

void BindHLineArray::chart_engine_drag(string obj_name)
{
 double new_price=NormalizeDouble(ObjectGetDouble(p_chart_id, obj_name, OBJPROP_PRICE1), Digits);
 
 string result[];
 StringSplit(obj_name, '-', result);
 string prefix;
 
 if(ArraySize(result) > 1)
  prefix = result[0] + "-" + StringTrimRight(result[1]);
 else
  p_logger.log("Unable to find name, obj_name=" + obj_name, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
 
 HorizontalLineArray* arr = get_array(prefix);
 
 if(arr)
 {
  string remainder = StringSubstr(obj_name, StringLen(prefix) + 1);
  
  string index_res[];
  
  StringSplit(remainder, '-', index_res);
  
  int index = (int)index_res[0];
  
  double prc = NormalizeDouble(ObjectGetDouble(p_chart_id, obj_name, OBJPROP_PRICE1), Digits);
   
   if(arr.Update(index, prc))
    p_logger.log("HLINE: Updated [" + obj_name + "] to prc=" + DoubleToStr(prc, Digits), LOG_TYPE_INFO, __LINE__, __FUNCTION__);
   else
    p_logger.log("HLINE: Error updating [" + obj_name + "] to prc=" + DoubleToStr(prc, Digits), LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
 }
}

BindHLineArray::~BindHLineArray()
{
 string name[]; uint uuid[];
 int sz=p_name_dict.copy(name, uuid);
 
 for(int i=sz-1; i>=0; i--)
 {
  DeleteGrid(p_chart_id, name[i]);
 }
}

void BindHLineArray::tick_engine()
{
 string name[]; uint uuid[];
 int sz=p_name_dict.copy(name, uuid);
 
 for(int i=0; i<sz; i++)
 {
  uint id = uuid[i];
 
  HorizontalLineArray* arr = Get(id);

  if(!arr.is_bound())
  {
   if(arr.Total() > 0)
   {
    color clr = p_id_2_color_dict[id];

    DrawGrid(p_chart_id, name[i], arr, arr.get_comments(), clr, 0);
   
    arr.set_binding(true);
   
    p_logger.log("Binding " + name[i] + ", i=" + (string)i + ", clr=" + ColorToString(clr,true), LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
   }
   else
   {
    DeleteGrid(p_chart_id, name[i]);
    p_logger.log("Deleting grid: " + name[i] + ", i=" + (string)i, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
   }
  }
 }
}

HorizontalLineArray* BindHLineArray::get_array(string name)
{
 if(p_name_dict.find(name))
 {
  uint uuid = p_name_dict[name];
  return Get(uuid);
 }
 
 p_logger.log("Couldn't find key, name=" + name, LOG_TYPE_HIGH, __LINE__, __FILE__);
 return NULL;
}

void BindHLineArray::unbind(string name)
{
 p_name_dict.del(name);
}

HorizontalLineArray* BindHLineArray::bind(string name, const HorizontalLineArray& data, color clr=clrGreen)
{
 if(p_name_dict.find(name))
 {
  uint uuid = p_name_dict[name];
  p_logger.log("Overwrite name=" + name + " with data from new array and clr=" + ColorToString(clr, true), LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
  HorizontalLineArray* arr = Get(uuid);
  arr.AssignArray(GetPointer(data));
  p_id_2_color_dict.set(uuid, clr);
  
  return Get(uuid);
 }

 uint uuid = p_uuid[name];
 
 p_logger.log("Binding name=" + name + " to uuid=" + (string)uuid, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
 p_name_dict.set(name, uuid);
 p_id_2_color_dict.set(uuid, clr);
 HorizontalLineArray* arr = New(uuid);
 arr.AssignArray(GetPointer(data));
 
 return arr;
}

HorizontalLineArray* BindHLineArray::bind(string name, color clr=clrGreen)
{
 if(p_name_dict.find(name))
 {
  p_logger.log("Name=" + name + " has already been binded", LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
  return NULL;
 }

 uint uuid = p_uuid[name];
 
 p_logger.log("Binding name=" + name + " to uuid=" + (string)uuid, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
 p_name_dict.set(name, uuid);
 p_id_2_color_dict.set(uuid, clr);
 
 HorizontalLineArray* p = New(uuid);
 return p;
}

