//+------------------------------------------------------------------+
//|                                               TradeParameter.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#ifndef ARRAYINT
#define ARRAYINT
   #include <Arrays/ArrayInt.mqh>
#endif

#ifndef LOGGER
#define LOGGER
   #define LOGGER_DEPRECATED
   #include <Logger.mqh>
#endif

#ifndef UUIDGENERATOR
#define UUIDGENERATOR
   #include <Packages/UUID.mqh>
#endif 

class TradeParameter
{
 private:
  CArrayInt p_buy_tickets;
  CArrayInt p_sell_tickets;
  
  int p_max_allowed_orders;
  double p_min_pips_average_up;
  double p_min_pips_average_down;
  double p_min_bars_between_orders;
  string p_symbol;
 
  double p_buy_worst_price;
  double p_buy_best_price;
  double p_sell_worst_price;
  double p_sell_best_price;
  datetime p_buy_last_timestamp;
  datetime p_sell_last_timestamp;
  int p_buy_best_price_ticket;
  int p_buy_worst_price_ticket;
  int p_sell_best_price_ticket;
  int p_sell_worst_price_ticket;
  
  UUIDGenerator uuid_generator;
  
  Logger logger;
 
  int p_digits;
  double p_point;
  
  void check_for_closed_buy_trades();
  void check_for_closed_sell_trades();
  void reassign_best_and_worst_prices(int cmd);
 
 public:
  TradeParameter(string symbol, int MAX_ALLOWED_ORDERS, double MIN_MINUTES_BETWEEN_ORDERS, double MIN_PIPS_AVERAGE_UP,double MIN_PIPS_AVERAGE_DOWN);
  TradeParameter(const CArrayInt& tickets, string symbol, int MAX_ALLOWED_ORDERS, double MIN_MINUTES_BETWEEN_ORDERS, double MIN_PIPS_AVERAGE_UP,double MIN_PIPS_AVERAGE_DOWN);
  void add(int ticket);
  void del(int ticket);
  void reset();
  bool is_long_trading_allowed();
  bool is_short_trading_allowed();
};

void TradeParameter::reassign_best_and_worst_prices(int cmd)
{
 int total;
 int worst_price_ticket = -1;
 double worst_price;
 int best_price_ticket = -1;
 double best_price;
 
 if(cmd==OP_BUY)
 {
  total = p_buy_tickets.Total();
  worst_price = 0;
  best_price  = DBL_MAX;
  
  for(int i=0; i<total; i++)
  {
   if(OrderSelect(p_buy_tickets[i], SELECT_BY_TICKET))
   {
    int ticket = p_buy_tickets[i];
    double price = OrderOpenPrice();
   
    if(price > worst_price)
    {  
     worst_price = price;
     worst_price_ticket = ticket;
    }
    
    if(price < best_price)
    {    
     best_price = price; 
     best_price_ticket = ticket;
    }
   }
  }
  
  if(total==0)
  {
   p_buy_best_price = DBL_MAX;
   p_buy_worst_price = 0;
   p_buy_last_timestamp = 0;
   
   logger.log("[BUY] Resetting best and worst price", LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
  }
  
  logger.log("[BUY] Setting new worst price from, " + DoubleToStr(p_buy_worst_price, p_digits) + " -> " + DoubleToStr(worst_price, p_digits) + ", ticket #" + (string)worst_price_ticket, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
  p_buy_worst_price = worst_price;
  p_buy_worst_price_ticket = worst_price_ticket;
  
  logger.log("[BUY] Setting new best price from, " + DoubleToStr(p_buy_best_price, p_digits) + " -> " + DoubleToStr(best_price, p_digits) + ", ticket #" + (string)best_price_ticket, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
  p_buy_best_price = best_price; 
  p_buy_best_price_ticket = best_price_ticket;
 } 
  
 if(cmd==OP_SELL)
 {
  total = p_sell_tickets.Total();
  worst_price = DBL_MAX;
  best_price  = 0;
  
  for(int i=0; i<total; i++)
  {
   if(OrderSelect(p_sell_tickets[i], SELECT_BY_TICKET))
   {
    int ticket = p_sell_tickets[i];
    double price = OrderOpenPrice();
   
    if(price < worst_price)
    {  
     worst_price = price;
     worst_price_ticket = ticket;
    }
    
    if(price > best_price)
    {    
     best_price = price; 
     best_price_ticket = ticket;
    }
   }
  }
  
  if(total==0)
  {
   p_sell_best_price = 0;
   p_sell_worst_price = DBL_MAX;
   p_sell_last_timestamp = 0;
   
   logger.log("[SELL] Resetting best and worst price", LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
  }
  
  logger.log("[SELL] Setting new worst price from, " + DoubleToStr(p_sell_worst_price, p_digits) + " -> " + DoubleToStr(worst_price, p_digits) + ", ticket #" + (string)worst_price_ticket, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
  p_sell_worst_price = worst_price;
  p_sell_worst_price_ticket = worst_price_ticket;
  
  logger.log("[SELL] Setting new best price from, " + DoubleToStr(p_sell_best_price, p_digits) + " -> " + DoubleToStr(best_price, p_digits) + ", ticket #" + (string)best_price_ticket, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
  p_sell_best_price = best_price; 
  p_sell_best_price_ticket = best_price_ticket;
 }
}

void TradeParameter::check_for_closed_buy_trades(void)
{
 int total = p_buy_tickets.Total();
 
 for(int i=total-1; i>=0; i--)
 {
  if(OrderSelect(p_buy_tickets[i],SELECT_BY_TICKET))
  {
   if(OrderCloseTime() > 0)
   {
    int cmd = OrderType();
    
    logger.log("Removing #" + IntegerToString(p_buy_tickets[i]) + " for ticket is closed", LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
    p_buy_tickets.Delete(i);
    
    reassign_best_and_worst_prices(cmd);
   }
  }
 }
}

void TradeParameter::check_for_closed_sell_trades(void)
{
 int total = p_sell_tickets.Total();
 
 for(int i=total-1; i>=0; i--)
 {
  if(OrderSelect(p_sell_tickets[i],SELECT_BY_TICKET))
  {
   if(OrderCloseTime() > 0)
   {
    int cmd = OrderType();
    logger.log("Removing #" + IntegerToString(p_sell_tickets[i]) + " for ticket is closed", LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
    p_sell_tickets.Delete(i);
    
    reassign_best_and_worst_prices(cmd);
   }
  }
 }
}

bool TradeParameter::is_long_trading_allowed()
{
 check_for_closed_buy_trades();
 int period = Period() * 60;

 int time_current_bar_remaining_secs = (int)((period-1) - MathMod(p_buy_last_timestamp, period));
 int time_future_bars_remaining_secs = (int)(MathMax((p_min_bars_between_orders-1) * period, 0));
 int secs_elapsed = (int)(TimeCurrent() - p_buy_last_timestamp);
 double point = MarketInfo(p_symbol, MODE_POINT);

 if(secs_elapsed < time_current_bar_remaining_secs + time_future_bars_remaining_secs)
 {
  logger.log("[BUY] Did not open trade for secs_elapsed=" + IntegerToString(secs_elapsed) + " < " + (string)(time_current_bar_remaining_secs + time_future_bars_remaining_secs) + ", (current, future_remaining)=(" + (string)time_current_bar_remaining_secs + "," + (string)time_current_bar_remaining_secs + ")", LOG_TYPE_INFO, __LINE__, __FUNCTION__, true, uuid_generator.retrive(__FILE__, __LINE__));
  return false;
 }
 
 if(p_buy_tickets.Total() >= p_max_allowed_orders)
 {
  logger.log("[BUY] Did not open trade for number_of_tickets_tracking=" + IntegerToString(p_buy_tickets.Total()) + " >= " + (string)p_max_allowed_orders, LOG_TYPE_INFO, __LINE__, __FUNCTION__, true);
  return false;
 }
 
 double ask = MarketInfo(p_symbol, MODE_ASK);
 bool diff = MathAbs(p_buy_best_price - p_buy_worst_price) > DBL_EPSILON;
 
 if(diff && p_buy_best_price < ask && ask < p_buy_worst_price)
 {
  logger.log("[BUY] Did not open trade for ask=" + DoubleToStr(ask, p_digits) + " in between best and worse price [" + DoubleToStr(p_buy_best_price, p_digits) + ", " + DoubleToStr(p_buy_worst_price, p_digits) + "]", LOG_TYPE_INFO, __LINE__, __FUNCTION__, true, uuid_generator.retrive(__FILE__, __LINE__)); 
  return false;
 }
  
 double average_up_distance = ask - p_buy_worst_price + point;
   
 if(average_up_distance >= 0 && average_up_distance < p_min_pips_average_up * p_point * 10)
 {
  logger.log("[BUY][AVG_UP] Did not open trade for ask - worst_price [" + DoubleToStr(ask, p_digits) + " - " + DoubleToStr(p_buy_worst_price, p_digits) + "] = " + DoubleToStr(average_up_distance, p_digits) + " < " + DoubleToStr(p_min_pips_average_up * p_point * 10, p_digits) + " [MIN_PIPS_AVERAGE_UP]", LOG_TYPE_INFO, __LINE__, __FUNCTION__, true, uuid_generator.retrive(__FILE__, __LINE__));
  return false;
 }
  
 double average_down_distance = p_buy_best_price - ask + point;
  
 if(average_down_distance > 0 && average_down_distance < p_min_pips_average_down * p_point * 10)
 {
  logger.log("[BUY][AVG_DOWN] Did not open trade for best_price - ask [" + DoubleToStr(p_buy_best_price, p_digits) + " - " + DoubleToStr(ask, p_digits) + "] = " + DoubleToStr(average_down_distance, p_digits) + " < " + DoubleToStr(p_min_pips_average_down * p_point * 10, p_digits) + " [MIN_PIPS_AVERAGE_DOWN]", LOG_TYPE_INFO, __LINE__, __FUNCTION__, true, uuid_generator.retrive(__FILE__, __LINE__));
  return false;
 }
 
 logger.log("secs_elapsed=" + DoubleToStr(secs_elapsed, 1) + ", time_current_bar_remaining_secs=" + DoubleToStr(time_current_bar_remaining_secs, 1) + ", time_future_bars_remaining_secs=" + DoubleToStr(time_future_bars_remaining_secs, 1) + ", buy_tickets_total=" + (string)p_buy_tickets.Total() + ", p_max_allowed_orders=" + (string)p_max_allowed_orders + ", ask=" + DoubleToStr(ask, p_digits) + ", p_buy_best_price=" + DoubleToStr(p_buy_best_price, p_digits) + ", p_buy_worst_price=" + DoubleToStr(p_buy_worst_price, p_digits) + ", average_up_distance=" + DoubleToStr(average_up_distance, p_digits) + ", p_min_pips_average_up=" + DoubleToStr(p_min_pips_average_up, p_digits) + ", average_down_distance=" + DoubleToStr(average_down_distance, p_digits) + ", p_point=" + DoubleToStr(p_point, p_digits), LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
 return true;
}

bool TradeParameter::is_short_trading_allowed()
{
 check_for_closed_sell_trades();
 int period = Period() * 60;

 int time_current_bar_remaining_secs = (int)((period-1) - MathMod(p_sell_last_timestamp, period));
 int time_future_bars_remaining_secs = (int)(MathMax((p_min_bars_between_orders-1) * period, 0));
 int secs_elapsed = (int)(TimeCurrent() - p_sell_last_timestamp);
 double point = MarketInfo(p_symbol, MODE_POINT);

 if(secs_elapsed < time_current_bar_remaining_secs + time_future_bars_remaining_secs)
 {
  logger.log("[SELL] Did not open trade for secs_elapsed=" + IntegerToString(secs_elapsed) + " < " + (string)(time_current_bar_remaining_secs + time_future_bars_remaining_secs) + ", (current, future_remaining)=(" + (string)time_current_bar_remaining_secs + "," + (string)time_current_bar_remaining_secs + ")", LOG_TYPE_INFO, __LINE__, __FUNCTION__, false, uuid_generator.retrive(__FILE__, __LINE__));
  return false;
 }
 
 if(p_sell_tickets.Total() >= p_max_allowed_orders)
 {
  logger.log("[SELL] Did not open trade for number_of_tickets_tracking=" + IntegerToString(p_sell_tickets.Total()) + " >= " + (string)p_max_allowed_orders, LOG_TYPE_INFO, __LINE__, __FUNCTION__, false);
  return false;
 }
 
 double bid = MarketInfo(p_symbol, MODE_BID);
 bool diff = MathAbs(p_sell_worst_price - p_sell_best_price) > DBL_EPSILON;
 
 if(diff && p_sell_worst_price < bid && bid < p_sell_best_price)
 {  
  logger.log("[SELL] Did not open trade for bid=" + DoubleToStr(bid, p_digits) + " in between worse and best price [" + DoubleToStr(p_sell_worst_price, p_digits) + ", " + DoubleToStr(p_sell_best_price, p_digits) + "]", LOG_TYPE_INFO, __LINE__, __FUNCTION__, false, uuid_generator.retrive(__FILE__, __LINE__)); 
  return false;
 }

 double average_up_distance = p_sell_worst_price - bid + point;
   
 if(average_up_distance >= 0 && average_up_distance < p_min_pips_average_up * p_point * 10)
 {
  logger.log("[SELL, AVG_UP] Did not open trade for worst_price - bid [" + DoubleToStr(p_sell_worst_price, p_digits) + " - " + DoubleToStr(bid, p_digits) + "] = " + DoubleToStr(average_up_distance, p_digits) + " < " + DoubleToStr(p_min_pips_average_up * p_point * 10, p_digits) + " [MIN_PIPS_AVERAGE_UP]", LOG_TYPE_INFO, __LINE__, __FUNCTION__, false, uuid_generator.retrive(__FILE__, __LINE__));
  return false;
 }
   
 double average_down_distance = bid - p_sell_best_price + point;
  
 if(average_down_distance > 0 && average_down_distance < p_min_pips_average_down * p_point * 10)
 {
  logger.log("[SELL, AVG_DOWN] Did not open trade for bid - best_price [" + DoubleToStr(bid, p_digits) + " - " + DoubleToStr(p_sell_best_price, p_digits) + "] = " + DoubleToStr(average_down_distance, p_digits) + " < " + DoubleToStr(p_min_pips_average_down * p_point * 10, p_digits) + " [MIN_PIPS_AVERAGE_DOWN]", LOG_TYPE_INFO, __LINE__, __FUNCTION__, false, uuid_generator.retrive(__FILE__, __LINE__));
  return false;
 }
 
 logger.log("secs_elapsed=" + DoubleToStr(secs_elapsed, 1) + ", time_current_bar_remaining_secs=" + DoubleToStr(time_current_bar_remaining_secs, 1) + ", time_future_bars_remaining_secs=" + DoubleToStr(time_future_bars_remaining_secs, 1) + ", sell_tickets_total=" + (string)p_sell_tickets.Total() + ", p_max_allowed_orders=" + (string)p_max_allowed_orders + ", bid=" + DoubleToStr(bid, p_digits) + ", p_sell_best_price=" + DoubleToStr(p_sell_best_price, p_digits) + ", p_sell_worst_price=" + DoubleToStr(p_sell_worst_price, p_digits) + ", average_up_distance=" + DoubleToStr(average_up_distance, p_digits) + ", p_min_pips_average_up=" + DoubleToStr(p_min_pips_average_up, p_digits) + ", average_down_distance=" + DoubleToStr(average_down_distance, p_digits) + ", p_point=" + DoubleToStr(p_point, p_digits), LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
 return true;
}

void TradeParameter::reset(void)
{
 p_buy_tickets.Clear();
 p_sell_tickets.Clear();
}

void TradeParameter::del(int ticket)
{
 int pos = p_buy_tickets.SearchLinear(ticket);
 
 if(pos >= 0)
 {
  p_buy_tickets.Delete(pos);
 }
 else
 {
  pos = p_sell_tickets.SearchLinear(ticket);
  
  if(pos >= 0)
  { 
   p_sell_tickets.Delete(pos);
  }
  else
  {
   logger.log("Could not delete #" + (string)ticket, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
  }
 }
}

void TradeParameter::add(int ticket)
{
 if(OrderSelect(ticket,SELECT_BY_TICKET))
 {
  if(OrderSymbol()==p_symbol)
  {
   int type = OrderType();
   double price = OrderOpenPrice();
   
   if(type==OP_BUY)
   {
    if(p_buy_tickets.Total() == 0)
    {
     p_buy_worst_price = price;
     p_buy_worst_price_ticket = ticket;
     p_buy_best_price = price;
     p_buy_best_price_ticket = ticket;
     p_buy_last_timestamp = 0;
     logger.log("Resetting parameters: p_symbol=" + p_symbol + ", p_buy_worst_price=" + DoubleToStr(p_buy_worst_price, p_digits) + ", p_buy_best_price=" + DoubleToStr(p_buy_best_price, p_digits) + ", ticket #" + (string)ticket, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
    }
   
    if(price > p_buy_worst_price)
    {  
     logger.log("[BUY] Setting new worst price from, " + DoubleToStr(p_buy_worst_price, p_digits) + " -> " + DoubleToStr(price, p_digits) + ", ticket #" + (string)ticket, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
     p_buy_worst_price = price;
     p_buy_worst_price_ticket = ticket;
    }
     
    if(price < p_buy_best_price)
    {    
     logger.log("[BUY] Setting new best price from, " + DoubleToStr(p_buy_best_price, p_digits) + " -> " + DoubleToStr(price, p_digits) + ", ticket #" + (string)ticket, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
     p_buy_best_price = price; 
     p_buy_best_price_ticket = ticket;
    }

    datetime open_time = OrderOpenTime();
    logger.log("Updating last timestamp from " + TimeToStr(p_buy_last_timestamp) + " to " + TimeToStr(open_time), LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
    p_buy_last_timestamp = open_time;
      
    p_buy_tickets.Add(ticket);
    logger.log("Added #" + (string)ticket, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
   }
       
  else if(type==OP_SELL)
  {
   if(p_sell_tickets.Total() == 0)
   {
    p_sell_worst_price = price;
    p_sell_worst_price_ticket = ticket;
    p_sell_best_price = price;
    p_sell_best_price_ticket = ticket;
    p_sell_last_timestamp = 0;
    logger.log("Resetting parameters: p_symbol=" + p_symbol + ", p_sell_worst_price=" + DoubleToStr(p_sell_worst_price, p_digits) + ", p_sell_best_price=" + DoubleToStr(p_sell_best_price, p_digits) + ", ticket #" + (string)ticket, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
   }
  
   if(price < p_sell_worst_price)
   {
    logger.log("[SELL] Setting new worst price from, " + DoubleToStr(p_sell_worst_price, p_digits) + " -> " + DoubleToStr(price, p_digits) + ", ticket #" + (string)ticket, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
    p_sell_worst_price = price;
    p_sell_worst_price_ticket = ticket;
   }
     
   if(price > p_sell_best_price)
   {  
    logger.log("[SELL] Setting new best price from, " + DoubleToStr(p_sell_best_price, p_digits) + " -> " + DoubleToStr(price, p_digits) + ", ticket #" + (string)ticket, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
    p_sell_best_price = price;
    p_sell_best_price_ticket = ticket;
   }
       
   datetime open_time = OrderOpenTime();
   logger.log("Updating last timestamp from " + TimeToStr(p_sell_last_timestamp) + " to " + TimeToStr(open_time), LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
   p_sell_last_timestamp = open_time;
      
   p_sell_tickets.Add(ticket);
   logger.log("Added #" + (string)ticket, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
  }
  else
   logger.log("Can only add market orders, ticket #" + (string)ticket, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);  
  }//END: OrderSelect() where OrderSymbol() == p_symbol
 }
 else
  logger.log("Could not select ticket #" + (string)ticket, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
}

TradeParameter::TradeParameter(string symbol,int MAX_ALLOWED_ORDERS,double MIN_BARS_BETWEEN_ORDERS,double MIN_PIPS_AVERAGE_UP,double MIN_PIPS_AVERAGE_DOWN)
 : logger(__FILE__)
{
 p_symbol = symbol;
 p_digits = (int)MarketInfo(symbol, MODE_DIGITS);
 p_point = MarketInfo(symbol, MODE_POINT);
 p_max_allowed_orders = MAX_ALLOWED_ORDERS;
 p_min_bars_between_orders = MIN_BARS_BETWEEN_ORDERS;
 p_min_pips_average_up = MIN_PIPS_AVERAGE_UP;
 p_min_pips_average_down = MIN_PIPS_AVERAGE_DOWN;
 
 p_buy_best_price = DBL_MAX;
 p_buy_worst_price = 0;
 p_sell_best_price = 0;
 p_sell_worst_price = DBL_MAX;
 p_buy_last_timestamp = 0;
 p_sell_last_timestamp = 0;
}

TradeParameter::TradeParameter(const CArrayInt &tickets,string symbol,int MAX_ALLOWED_ORDERS,double MIN_BARS_BETWEEN_ORDERS,double MIN_PIPS_AVERAGE_UP,double MIN_PIPS_AVERAGE_DOWN)
 : logger(__FILE__)
{
 p_symbol = symbol;
 p_digits = (int)MarketInfo(symbol, MODE_DIGITS);
 p_point = MarketInfo(symbol, MODE_POINT);
 p_max_allowed_orders = MAX_ALLOWED_ORDERS;
 p_min_bars_between_orders = MIN_BARS_BETWEEN_ORDERS;
 p_min_pips_average_up = MIN_PIPS_AVERAGE_UP;
 p_min_pips_average_down = MIN_PIPS_AVERAGE_DOWN;
 
 p_buy_best_price = DBL_MAX;
 p_buy_worst_price = 0;
 p_sell_best_price = 0;
 p_sell_worst_price = DBL_MAX;
 p_buy_last_timestamp = 0;
 p_sell_last_timestamp = 0;
 
 CArrayInt copy;
 copy.AssignArray(GetPointer(tickets));
 copy.Sort();
 
 int total=copy.Total();
 
 for(int i=0; i<total; i++)
  add(copy[i]);
}