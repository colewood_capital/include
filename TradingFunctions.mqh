//+------------------------------------------------------------------+
//|                                             TradingFunctions.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#ifndef MQLERRORS
#define MQLERRORS
   #include <MQLErrors.mqh>
#endif 

bool TradingFunctionsModifySL(int booknum, double sl)
{
  if(OrderSelect(booknum,SELECT_BY_TICKET)) 
   {
    if(MathAbs(sl-OrderStopLoss()) > DBL_EPSILON)
     {
      if(OrderModify(booknum,OrderOpenPrice(),sl,OrderTakeProfit(),0))
       {
         return(true);
       }
      else
       {
         Alert("Error: could not modify ticket #", booknum," for e: ", ErrorDescription(GetLastError()),", sl=", sl," in ",__FUNCTION__);
         ResetLastError();
         return(false);
       }
     }
    else return(false);
   }
   else
   {
     Alert("Error: could not select ticket #", booknum," for e: ", ErrorDescription(GetLastError()),", sl=", sl," in ",__FUNCTION__);
     ResetLastError();
     return(false);
   }
}
//+-----------------------------------------------------------------+