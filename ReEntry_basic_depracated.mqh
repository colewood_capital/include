//+------------------------------------------------------------------+
//|                                                      ReEntry.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#define REENTRY_PREFIX     "ReEntry"

#ifndef DRAW
#define DRAW
   #include <Tasks/Draw.mqh>
#endif

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
#endif

Logger reentrylogger(__FILE__, "helper_processes");

enum ENUM_REENTRY
{
 REENTRY_NIL,
 REENTRY_TRIGGERED
};

class ReEntry
{
 private:
  int p_id;
  
 public:
  bool create(double lots, double price);
  void clear();
  ENUM_REENTRY tick_engine(int& type, double& lots);
  ReEntry();
};

void ReEntry::clear(void)
{
 int total = ObjectsTotal();
 
 for(int i=total-1; i>=0; i--)
 {
  string name = ObjectName(i);

  if(StringFind(name, REENTRY_PREFIX) == 0)
   ObjectDelete(name);
 }
}

ENUM_REENTRY ReEntry::tick_engine(int& type, double& lots)
{
 int total = ObjectsTotal();
 bool triggered = false;

 for(int i=total-1; i>=0; i--)
 { 
  string name = ObjectName(i);
  double open_price = ObjectGet(name, OBJPROP_PRICE1);
  
  if(StringFind(name, REENTRY_PREFIX) == 0)
  {
   string result[];
   int n = StringSplit(name, '-', result);
   if(n >= 3)
   {
    if(result[1]=="buy") 
     type = OP_BUY;
    else if(result[1]=="sell")
     type = OP_SELL;
    else {
     reentrylogger.log("Could not find order type", LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
     return -1;
    }
    
    lots = NormalizeDouble((double)result[2], 2);
    
    switch(type)
    {
     case OP_BUY:
      if(Bid > open_price) 
       triggered=true;
      break;
      
     case OP_SELL:
      if(Bid < open_price)
       triggered=true;
      break;
    }
     
    if(triggered)
    {
     ObjectDelete(name);
     return REENTRY_TRIGGERED;
    }
   }
  }
 }
 
 return REENTRY_NIL;
}

bool ReEntry::create(double lots,double price)
{
 string type;
 
 if(price > Bid)
  type = "buy";
 else if(price < Bid)
  type = "sell";
 else {
  int last_direction = LastDirection(price);
  
  if(last_direction==OP_BUY)
   type = "buy";
  else if(last_direction==OP_SELL)
   type = "sell";
  else {
   reentrylogger.log("Could not determine direction", LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
   return false;
  }
 }
   
 string name = REENTRY_PREFIX + "[" + (string)p_id + "]-" + type + "-" + (string)lots;
 p_id++;
 return HLineCreate(name, price);
}

ReEntry::ReEntry(void)
{
 int total = ObjectsTotal((int)OBJ_HLINE);
 int max = -1;
 
 for(int i=0; i<total; i++)
 {
  string name = ObjectName(i);
  
  if(StringFind(name, REENTRY_PREFIX) == 0)
  {
   int p1 = StringLen(REENTRY_PREFIX) + 1;
   int p2 = StringFind(name, "]");
   
   max = MathMax(max, (int)StringSubstr(name, p1, p2-p1));
  }
 }
 
 p_id = max + 1;
}

int LastDirection(double close_price)
{
 for(int i=1; i<Bars-1; i++)
 {
  if(Close[i] < close_price)
   return OP_BUY;
  
  if(Close[i] > close_price)
   return OP_SELL;
 }
 
 return -1;
}
