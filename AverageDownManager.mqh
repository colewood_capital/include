//+------------------------------------------------------------------+
//|                                        AverageDownManagement.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "CARM INC"
#property strict

#define FILENAME_MINBOOKNUM      "ADM_DO_NOT_DELETE_1.txt"

#ifndef ARRAYINT
#define ARRAYINT
   #include <Arrays/ArrayInt.mqh>
#endif

#ifndef ARRAYDOUBLE
#define ARRAYDOUBLE
   #include <Arrays/ArrayDouble.mqh>
#endif

#ifndef AUTOSAVEINTEGER
#define AUTOSAVEINTEGER
   #include <DataTypes/AutoSaveInteger.mqh>
#endif 

#ifndef EA
   input double AVG_DOWN_LOTS_ON_PIPS_LESS_THAN = 10;
   input double AVG_DOWN_END_ON_PIPS_GAINED = 50;
#endif

class AverageDownManager
{
 private:
  CArrayInt buy_order_queue, sell_order_queue; 
  CArrayDouble buy_order_close_price;
  CArrayDouble sell_order_close_price;
  CArrayDouble buy_lots;
  CArrayDouble sell_lots;
  CArrayDouble buy_reentry_price;
  CArrayDouble sell_reentry_price;
  int p_magic;
  
  double Implicit_PL(int type, double lots, double close_price);
  double Adjusted_Lots(int type, double sl, double implicit_pl);
  void FillQueue();
  
 public: 
  double StopLossTotalAdjustedLots(int type, double sl, double& total_lots_closed, int& total_order_count);
  bool Add(int ticket);
  void ClearQueue(int type);
  
  AverageDownManager(int external_magic_number);
  ~AverageDownManager(void);
};
//+------------------------------------------------------------------+
AverageDownManager::~AverageDownManager(void)
{
 
}
//+------------------------------------------------------------------+
AverageDownManager::AverageDownManager(int external_magic_number)
{
 p_magic = external_magic_number;
 
 FillQueue();
}
//+------------------------------------------------------------------+
double AverageDownManager::StopLossTotalAdjustedLots(int type, double sl, double& total_lots_closed, int& total_order_count)
{
 if(sl==0)        return(0);           //Order must have SL to properly calculate risk
 
 FillQueue();
 
 total_lots_closed = 0.0;
 total_order_count = 0;
 
 if(type==OP_BUY)
  {
   int total = buy_order_queue.Total();
   double adj_lots = 0.0;
   
   for(int i=0; i<total; i++)
    {
     double impl_pl = Implicit_PL(OP_BUY, buy_lots[i], buy_order_close_price[i]);
            adj_lots += Adjusted_Lots(OP_BUY, sl, impl_pl);
            total_lots_closed += buy_lots[i];
            total_order_count++;
    }
   
   ClearQueue(OP_BUY); ClearQueue(OP_SELL);
   return(adj_lots);
  }
 else if(type==OP_SELL)
  {
   int total = sell_order_queue.Total();
   double adj_lots = 0.0;
   
   for(int i=0; i<total; i++)
    {
     double impl_pl = Implicit_PL(OP_SELL, sell_lots[i], sell_order_close_price[i]); 
            adj_lots += Adjusted_Lots(OP_SELL, sl, impl_pl);
            total_lots_closed += sell_lots[i]; 
            total_order_count++;
    }
   
   ClearQueue(OP_BUY); ClearQueue(OP_SELL);
   return(adj_lots);
  }
 else
  {
   Alert("Invalid type=", type,". sl=",sl," in ",__FUNCTION__);
   return(0);
  }
}
//+------------------------------------------------------------------+
void AverageDownManager::ClearQueue(int type)
{
 if(type==OP_BUY)
  {
   buy_order_queue.Clear();
   buy_lots.Clear();
   buy_order_close_price.Clear();
  }
 else if(type==OP_SELL)
  {
   sell_order_queue.Clear();
   sell_lots.Clear();
   sell_order_close_price.Clear();
  }
 else
  {
   Alert("Error: invalid type=",type," in ",__FUNCTION__);
  }
}
//+------------------------------------------------------------------+
bool AverageDownManager::Add(int ticket)
{
 if(OrderSelect(ticket,SELECT_BY_TICKET))
  {
   if(OrderSymbol()==Symbol())
    {
     if(OrderMagicNumber()==p_magic)
      {
       int type = OrderType();
       
       if(type==OP_BUY)
        {
         buy_order_queue.Add(OrderTicket());
         buy_lots.Add(OrderLots());
         buy_order_close_price.Add(OrderClosePrice());
         return(true);
        }
        
       else if(type==OP_SELL)
        {
         sell_order_queue.Add(OrderTicket());
         sell_lots.Add(OrderLots());
         sell_order_close_price.Add(OrderClosePrice());
         return(true);
        }
        
       else
        {
         Alert("Error: invalid type=",type," in ",__FUNCTION__);
        }
      }
     else
      {
       Alert("Error: invalid magic number=",OrderMagicNumber()," in ",__FUNCTION__);
      }
    }
   else
    {
     Alert("Error: invalid symbol=",OrderSymbol()," in ",__FUNCTION__);
    }
  }
 else
  {
   Alert("Error: invalid ticket=",ticket," in ",__FUNCTION__);
  }
   
 return(false);  
}
//+------------------------------------------------------------------+
void AverageDownManager::FillQueue(void)
//Keep adding orders closed by [sl] until a normal close is found
{
 string sym = Symbol();

 datetime max_open_time = 0;
 
 int total = OrdersTotal();
 
 for(int i=0; i<total; i++)
  {
   if(OrderSelect(i, SELECT_BY_POS))
    {
     if(OrderSymbol()==sym && OrderMagicNumber()==p_magic) 
      {
       max_open_time = MathMax(max_open_time, OrderOpenTime());
      }
    }
  }
  
 int history_total = OrdersHistoryTotal();

 bool buy_toggle_off=false, sell_toggle_off=false;
 
 for(int i=history_total-1; i>=0; i--)
  {
   if(OrderSelect(i,SELECT_BY_POS,MODE_HISTORY))
    { 
    //--- Stop adding tickets: looking at each side separately, stop adding when an order's pips_gained >= AVG_DOWN_END_ON_PIPS_GAINED
     if(buy_toggle_off && sell_toggle_off)   break;
     
    //--- Stop adding tickets: an order was opened after the close time of all remaining orders in the history queue 
     if(OrderCloseTime() < max_open_time)    break;
     
     
     if(OrderSymbol()==sym && OrderMagicNumber()==p_magic)   
      {
       if(StringFind(OrderComment(),"[sl]") >= 0)
        { 
         int type = OrderType();
         double pips_gained;
         
         if(type==OP_BUY && !buy_toggle_off)
          { 
           pips_gained = (Bid - OrderOpenPrice()) / (Point * 10);
           
           if(pips_gained >= AVG_DOWN_END_ON_PIPS_GAINED)      
            buy_toggle_off=true;
           
           if(pips_gained <= AVG_DOWN_LOTS_ON_PIPS_LESS_THAN)
            {
             buy_lots.Add(OrderLots());
             buy_order_queue.Add(OrderTicket());
             buy_order_close_price.Add(OrderClosePrice());
            }
          }
          
         if(type==OP_SELL && !sell_toggle_off)
          {
           pips_gained = (OrderOpenPrice() - Ask) / (Point * 10);
           
           if(pips_gained >= AVG_DOWN_END_ON_PIPS_GAINED)      
            sell_toggle_off=true;
           
           if(pips_gained <= AVG_DOWN_LOTS_ON_PIPS_LESS_THAN)
            {
             sell_lots.Add(OrderLots());
             sell_order_queue.Add(OrderTicket());
             sell_order_close_price.Add(OrderClosePrice());
            }
          }
        }
      }
    }
  }//end for
}
/*
//+------------------------------------------------------------------+ 
void AverageDownManager::FillQueue(void)
//Keep adding orders closed by [sl] until a normal close is found
{
 string sym = Symbol();
 
 datetime max_open_time = 0;
 
 int total = OrdersTotal();
 
 for(int i=0; i<total; i++)
  {
   if(OrderSelect(i, SELECT_BY_POS))
    {
     if(OrderSymbol()==sym && OrderMagicNumber()==p_magic) 
      {
       max_open_time = MathMax(max_open_time, OrderOpenTime());
      }
    }
  }
 
 int history_total = OrdersHistoryTotal();

 bool buy_toggle_off=false, sell_toggle_off=false;
 
 for(int i=history_total-1; i>=0; i--)
  {
   if(OrderSelect(i,SELECT_BY_POS,MODE_HISTORY))
    { 
    //--- Stop adding tickets: looking at each side separately, stop adding on the first order that was not closed by SL after an order that was closed by SL is found
     if(buy_toggle_off && sell_toggle_off)   break;
     
    //--- Stop adding tickets: an order was opened after the close time of all remaining orders in the history queue 
     if(OrderCloseTime() < max_open_time)    break;
     
    //--- Add ticket to order queue
     if(OrderSymbol()==sym && OrderMagicNumber()==p_magic)   
      {
       if(StringFind(OrderComment(),"[sl]") >= 0)
        { 
         int type = OrderType();
         
         if(type==OP_BUY && !buy_toggle_off)
          { 
           buy_lots.Add(OrderLots());
           buy_order_queue.Add(OrderTicket());
           buy_order_close_price.Add(OrderClosePrice());
          }
          
         if(type==OP_SELL && !sell_toggle_off)
          {
           sell_lots.Add(OrderLots());
           sell_order_queue.Add(OrderTicket());
           sell_order_close_price.Add(OrderClosePrice());
          }
        }
       else
        {
         int type = OrderType();
         
         if(type==OP_BUY)
          buy_toggle_off = true;
        
         if(type==OP_SELL)
          sell_toggle_off = true;
        }
      }
    }
  }//end for
}
*/
//+------------------------------------------------------------------+
double AverageDownManager::Adjusted_Lots(int type, double sl, double implicit_pl)
{
 double tick_value = MarketInfo(Symbol(), MODE_TICKVALUE);
 double points;
 
 if(type==OP_BUY)
  points = (Bid - sl) / Point;
 else if(type==OP_SELL)
  points = (sl - Ask) / Point;
 else
  {
   Alert("Error: invalid type=",type,". sl=",sl,". implicit_pl=",implicit_pl);
   return(0);
  }
  
 return(implicit_pl / (points * tick_value));
}
//+------------------------------------------------------------------+
double AverageDownManager::Implicit_PL(int type, double lots, double close_price)
{
 string symbol = Symbol();
 double tick_value = MarketInfo(symbol, MODE_TICKVALUE);
 double points;
 
 if(type==OP_BUY)
  points = (close_price - Ask) / Point;
 else if(type==OP_SELL)
  points = (Bid - close_price) / Point;
 else
  {
   Alert("Error: invalid type=",type,". lots=",lots,"close_price=",close_price," in ",__FUNCTION__);
   return(DBL_MIN);
  }

 return(points * lots * tick_value);
}
