//+------------------------------------------------------------------+
//|                                                       utils2.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#import "kernel32.dll" 
   int GetComputerNameA(char& lpBuffer[], string nSize);
#import

#ifndef ENUMS
#define ENUMS
   #include <Super\Enums.mqh>
#endif 

#ifndef FUNCTIONS
#define FUNCTIONS
   #include <Super\Functions.mqh>
#endif 

#ifndef MAP
#define MAP
   #include <Map.mqh>
#endif 

#ifndef ARRAYINT
#define ARRAYINT
   #include <Arrays\ArrayInt.mqh>
#endif 

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
#endif 

#ifndef HEADERS
#define HEADERS
   #include <super\\headers.mqh>
#endif 

class TerminalComment
{
private:
 Map<string,string> cmt;
 
public:
 void set(string key, string value) { cmt.set(key, value); }
 void remove(string key) { cmt.del(key); }
 void engine();
 
 ~TerminalComment();
};

TerminalComment::~TerminalComment()
{
 Comment("");
}

void TerminalComment::engine()
{
 string _comment="";
 
 cmt.set("Last Update", TimeToStr(TimeLocal(), TIME_MINUTES|TIME_SECONDS));
 
 string k[], v[];
 int t=cmt.copy(k,v);
 
 for(int i=0; i<t; i++)
  _comment += k[i] + ": " + v[i] + "\n";
  
 Comment(_comment);
}
        

#define END_EVENT_LOOP  Comment(StringConcatenate("Last Update: ",TimeLocal(),"\n")); \
                        GlobalVariableSet("last_update_" + GetEAName(__FILE__), TimeCurrent());


void GetTickets(CArrayInt& tickets, string symbol, ENUM_ORDER_TYPE type, int magic)
{
 int total = OrdersTotal();
 
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(i,SELECT_BY_POS) && OrderSymbol()==symbol && OrderType()==type && OrderMagicNumber()==magic )
   tickets.Add(OrderTicket());
 }
}



void GetTickets(CArrayInt& tickets, string symbol)
{
 int total = OrdersTotal();
 
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(i,SELECT_BY_POS) && OrderSymbol()==symbol)
   tickets.Add(OrderTicket());
 }
}



class Utils2
{
 public:
  string ShowTimeframe(int period);
  string GetEAName(string __FILE__macro);
};

Utils2 utils2;

string Utils2::ShowTimeframe(int period)
{
 switch(period)
 {
  case 1:       return("PERIOD_M1");
  case 5:       return("PERIOD_M5");
  case 15:      return("PERIOD_M15");
  case 30:      return("PERIOD_M30");
  case 60:      return("PERIOD_H1");
  case 240:     return("PERIOD_H4");
  case 1440:    return("PERIOD_D1");
  case 10080:   return("PERIOD_W1");
  case 43200:   return("PERIOD_M1");
  default:      return(NULL);
 }
}

//+------------------------------------------------------------------+
string Utils2::GetEAName(string __FILE__macro)
{
 int delimiter = StringFind(__FILE__macro, ".");
 string ea_name = StringSubstr(__FILE__macro, 0, delimiter);
 
 StringReplace(ea_name, " ", "_");
 
 return ea_name;
}
//+------------------------------------------------------------------+
string PeriodStr(int period)
{
 switch(period)
 {
  case PERIOD_M1:  return("M1");
  case PERIOD_M5:  return("M5");
  case PERIOD_M15: return("M15");
  case PERIOD_M30: return("M30");
  case PERIOD_H1:  return("H1");
  case PERIOD_H4:  return("H4");
  case PERIOD_D1:  return("D1");
  case PERIOD_MN1: return("M1");
 }
 
 return "";
}
//+------------------------------------------------------------------+
int GetParentBooknum(int ticket)
{
 string symbol = NULL;
 datetime opentime = 0;
 
 if(OrderSelect(ticket,SELECT_BY_TICKET))
 {
  symbol = OrderSymbol();
  opentime = OrderOpenTime(); 
 }
  
 int total = OrdersHistoryTotal();
 
 int parent = ticket;
 
 for(int i=total-1; i>=0; i--)
 {
  if(OrderSelect(i,SELECT_BY_POS,MODE_HISTORY))
  {
   if(OrderTicket() < ticket)
   {
    if(OrderSymbol()==symbol && OrderType() < 2)
    { 
     if(OrderCloseTime() >= opentime)
      parent = OrderTicket();
    }
   }
  }
 }
 
 return parent;
}
//+------------------------------------------------------------------+
double prevFractal(int IN_count, int &OUT_bar, int mode,int timeframe=PERIOD_CURRENT,string symbol=NULL)
{
 double frac=-1;
 OUT_bar=-1;
 int count=0;
 
 if(IN_count < 1) {
  Alert("[HIGH] count(" + (string)IN_count + ") < 1");
  return frac;
 }
 
 if(mode==MODE_UPPER)
 {
  for(OUT_bar=0; OUT_bar<Bars; OUT_bar++)
  {
   frac = iFractals(symbol, timeframe, MODE_UPPER, OUT_bar);
   
   if(frac > 0)
   {
    count++;
    
    if(count >= IN_count)
     break;
   }
  }
 }
 else if(mode==MODE_LOWER)
 {
  for(OUT_bar=0; OUT_bar<Bars; OUT_bar++)
  {
   frac = iFractals(symbol, timeframe, MODE_LOWER, OUT_bar);
   
   if(frac > 0)
   {
    count++;
    
    if(count >= IN_count)
     break;
   }
  }
 }
 else
  Alert("[HIGH] invalid mode=" + (string)mode);
 
 return frac;
}
//+------------------------------------------------------------------+
double getOpenPrice(int cmd, int& open_prc_bar, int start_index=1)
{
 double cls_prc = iClose(NULL, 0, start_index);
 int bars = iBars(NULL, 0);
 
 if(cmd==OP_BUY)
 {
  for(int i=start_index; i<bars-1; i++)
  {
   double open_prc = iOpen(NULL, 0, i);
   
   if(open_prc < cls_prc)
   {
    open_prc_bar = i;
    Print("[BUY] open_prc=" + DoubleToStr(open_prc, Digits) + ", open_prc_bar=" + (string)open_prc_bar + ", start_index=" + (string)start_index);
    return open_prc;
   }
  }
 }
 
 if(cmd==OP_SELL)
 {
  for(int i=start_index; i<bars-1; i++)
  {
   double open_prc = iOpen(NULL, 0, i);
    
   if(open_prc > cls_prc)
   {
    open_prc_bar = i;
    Print("[SELL] open_prc=" + DoubleToStr(open_prc, Digits) + ", open_prc_bar=" + (string)open_prc_bar + ", start_index=" + (string)start_index);
    return open_prc;
   }
  }
 }
 
 Alert("Invalid cmd=" + (string)cmd);
 return 0;
}
//+------------------------------------------------------------------+
bool EventChartCustomBroadcast(string symbol, ushort custom_event_id, long lparam, double dparam, string sparam)
{
 const int ATTEMPTS =3;
 bool b_success=false;
 
 for(int i=0; i<ATTEMPTS; i++)
 {
  long chart_id = ChartFirst();
  if(symbol==NULL) symbol = Symbol();
  b_success=true;
 
  do 
  {
   if(ChartSymbol(chart_id)==symbol)
   {
    if(!EventChartCustom(chart_id, custom_event_id, lparam, dparam, sparam))
    {
     int e = GetLastError();
   
     if(e != 4024) //internal error (MT4 bug - returns true if alert chart_id before running EventChartCustom())
     {
      b_success=false;
      break;
     }
    }
   }
   
   chart_id = ChartNext(chart_id);
  } 
  while(chart_id > 0); // inner loop
  
  if(b_success)
   break;
   
  Sleep(100);
 }
 
 return b_success;
}

//+------------------------------------------------------------------+
bool EventChartCustomBroadcast(ushort custom_event_id, long lparam, double dparam, string sparam)
{
 const int ATTEMPTS =3;
 bool b_success=false;
 
 for(int i=0; i<ATTEMPTS; i++)
 {
  long chart_id = ChartFirst();
  b_success=true;
  
  do 
  {
   if(!EventChartCustom(chart_id, custom_event_id, lparam, dparam, sparam))
   {
    int e = GetLastError();
   
    if(e != 4024) //internal error (MT4 bug - returns true if alert chart_id before running EventChartCustom())
    {
     b_success=false;
     break;
    }
   }
   
   chart_id = ChartNext(chart_id);
  } while(chart_id > 0);  // inner loop
 
  if(b_success)
   break;
   
  Sleep(100);
 }
 
 return b_success;
}
//+------------------------------------------------------------------+
void DecodeStringDictDeprecated(const string& sparam, KwargsDict& kwargs)
{
 string rows[];
 int len = StringLen(sparam);

 if(StringSubstr(sparam, len-1, 1) == ";")
 {
  int row_size = StringSplit(StringSubstr(sparam, 0, len-1), ',', rows);
  
  for(int i=0; i<row_size; i++)
  {
   string res[];
   int res_size = StringSplit(rows[i], ':', res);
   
   if(res_size == 2)
   {
    kwargs.set(res[0], res[1]);
   }
   else {
    Alert("[ERROR] DecodeStringDict(): Could not parse row = " + rows[i]);
    Alert("sparam: ", sparam);
   }
  }
 }
 else {
  Alert("[ERROR] DecodeStringDict(): terminating character ';' not found");
  Alert("sparam: ", sparam);
 }

 return;
}
//+------------------------------------------------------------------+
string _segment(string &remaining, string delimiter)
{
 int pos = StringFind(remaining, delimiter);
 string segment = "";
 
 if(pos >= 0)
 {
  segment = StringSubstr(remaining, 0, pos);
  remaining = StringSubstr(remaining, pos);
 }
 
 return segment;
}
//+------------------------------------------------------------------+
int _nextSegment(string str, string delimiter)
{
 int pos = 0;
 
 int p1 = StringFind(str, delimiter);
 int p2 = StringFind(str, delimiter, p1+1);
 Print(str);
 Print(p1, " .. ", p2);
 if(p2 == p1 + 1)  //nested dictionaries
  p1 += _nextSegment(StringSubstr(str, p2), delimiter);
 
 if(p1 >= 0)
  return p1+1;
 else
  return -1;
}
//+------------------------------------------------------------------+
int _nestedDictEndPos(string sparam)
{
 uchar buf[];
 int begin = StringFind(sparam, ":");
 int len = StringToCharArray(StringSubstr(sparam, begin+1), buf);
 int i=0, count=1;

 for(i=0; i<len; i++)
 { 
  if(count==0)
   break;
    
  if(buf[i]==':')
   count++;
  
  else if(buf[i]==',')
   count--;
   
  else if(buf[i]==';')
   count--;
 }

 return begin + i;
}
//+------------------------------------------------------------------+
void DecodeStringDict(string sparam, KwargsDict &kwargs)
{
 int colon1 = StringFind(sparam, ":");
 int comma1 = StringFind(sparam, ",");
 int colon2 = StringFind(sparam, ":", colon1+1);

 if(comma1 >= 0 && colon2 < 0)
 {
  Alert(__FUNCTION__," [ERROR] Unsupported format, sparam=", sparam);
  kwargs.clear();
  return;
 }
 
 if(comma1 < 0 || colon2 < 0)
 {
  if(StringLen(sparam) > 0)
  {
   string key = (colon1 > 0) ? StringSubstr(sparam, 0, colon1) : "";
  
   int v_size = StringLen(sparam)-colon1-2;
   string val = (v_size > 0) ? StringSubstr(sparam, colon1+1, v_size) : "";
  
   kwargs.set(key, val);
  }
 }
 else if(comma1 < colon2)
 {
  string key = (colon1 > 0) ? StringSubstr(sparam, 0, colon1) : "";
  
  int v_size = comma1-colon1-1;
  string val = (v_size > 0) ? StringSubstr(sparam, colon1+1, v_size) : "";

  kwargs.set(key, val);
  DecodeStringDict(StringSubstr(sparam, comma1+1), kwargs);
 }
 else
 { 
  string key = (colon1 > 0) ? StringSubstr(sparam, 0, colon1) : "";
  
  int start_pos = colon1 + 1;
  int end_pos = _nestedDictEndPos(sparam);
  
  int v_size = end_pos-start_pos-1;
  string val = (v_size > 0) ? StringSubstr(sparam, start_pos, v_size) : "";

  kwargs.set(key, val);
  DecodeStringDict(StringSubstr(sparam, end_pos+1), kwargs);
 }
}
//+------------------------------------------------------------------+
void DecodeStringDict(string sparam, string& keys[], string& values[], int &IN_OUT_size)
{
 int colon1 = StringFind(sparam, ":");
 int comma1 = StringFind(sparam, ",");
 int colon2 = StringFind(sparam, ":", colon1+1);

 if(comma1 >= 0 && colon2 < 0)
 {
  Alert(__FUNCTION__," [ERROR] Unsupported format, sparam=", sparam);
  IN_OUT_size = 0;
  ArrayResize(keys, IN_OUT_size);
  ArrayResize(values, IN_OUT_size);
  return;
 }
 
 if(comma1 < 0 || colon2 < 0)
 {
  if(StringLen(sparam) > 0)
  {
   string key = (colon1 > 0) ? StringSubstr(sparam, 0, colon1) : "";
  
   int v_size = StringLen(sparam)-colon1-2;
   string val = (v_size > 0) ? StringSubstr(sparam, colon1+1, v_size) : "";
  
   IN_OUT_size++;
   ArrayResize(keys, IN_OUT_size);
   ArrayResize(values, IN_OUT_size);
   keys[IN_OUT_size-1] = key;
   values[IN_OUT_size-1] = val;
  }
 }
 else if(comma1 < colon2)
 {
  string key = (colon1 > 0) ? StringSubstr(sparam, 0, colon1) : "";
  
  int v_size = comma1-colon1-1;
  string val = (v_size > 0) ? StringSubstr(sparam, colon1+1, v_size) : "";

  IN_OUT_size++;
  ArrayResize(keys, IN_OUT_size);
  ArrayResize(values, IN_OUT_size);
  keys[IN_OUT_size-1] = key;
  values[IN_OUT_size-1] = val;

  DecodeStringDict(StringSubstr(sparam, comma1+1), keys, values, IN_OUT_size);
 }
 else
 {
  string key = (colon1 > 0) ? StringSubstr(sparam, 0, colon1) : "";
  
  int start_pos = colon1 + 1;
  int end_pos = _nestedDictEndPos(sparam);
  
  int v_size = end_pos-start_pos-1;
  string val = (v_size > 0) ? StringSubstr(sparam, start_pos, v_size) : "";

  IN_OUT_size++;
  ArrayResize(keys, IN_OUT_size);
  ArrayResize(values, IN_OUT_size);
  keys[IN_OUT_size-1] = key;
  values[IN_OUT_size-1] = val;
  
  DecodeStringDict(StringSubstr(sparam, end_pos+1), keys, values, IN_OUT_size);
 }
}
//+------------------------------------------------------------------+
int DecodeStringDictDeprecated(const string& sparam, string& keys[], string& values[])
{
 string rows[];
 int len = StringLen(sparam);
 int size = 0;

 if(StringSubstr(sparam, len-1, 1) == ";")
 {
  int row_size = StringSplit(StringSubstr(sparam, 0, len-1), ',', rows);
  
  for(int i=0; i<row_size; i++)
  {
   string res[];
   int res_size = StringSplit(rows[i], ':', res);
   
   if(res_size == 2)
   {
    size++;
    ArrayResize(keys, size);
    ArrayResize(values, size);
    keys[size-1] = res[0];
    values[size-1] = res[1];
   }
   else {
    for(int j=0; j<ArraySize(res); j++) Alert(j, ": ", res[j]);
   
    Alert("[ERROR] DecodeStringDict(): Could not parse row = " + rows[i]);
    Alert("sparam: ", sparam);
   }
  }
 }
 else {
  Alert("[ERROR] DecodeStringDict(): terminating character ';' not found");
  Alert("sparam: ", sparam);
 }
 
 return size;
}
//+------------------------------------------------------------------+
string EncodeStringDict(const string& keys[], const string& values[])
{
 string ret_str = "";
 
 int key_size = ArraySize(keys);
 
 for(int i=0; i<key_size; i++)
 {
  ret_str += keys[i] + ":" + values[i] + ",";
 }
 
 int len = StringLen(ret_str);
 
 if(len > 0)
  ret_str = StringSubstr(ret_str, 0, len-1);

 return ret_str += ";";
}
//+------------------------------------------------------------------+
string EncodeStringDict(const string& values[])
{
 string ret_str = "";
 
 int val_size = ArraySize(values);
 
 for(int i=0; i<val_size; i++)
 {
  ret_str += "" + ":" + values[i] + ",";
 }
 
 int len = StringLen(ret_str);
 
 if(len > 0)
  ret_str = StringSubstr(ret_str, 0, len-1);

 return ret_str += ";";
}
//+------------------------------------------------------------------+
datetime PsarStartingTime(int cmd)
{
 if(cmd==OP_BUY)
 {
  for(int i=1; i<Bars-1; i++)
  {
   double _psar = iSAR(NULL, Period(), 0.008, 0.2, i);
   
   if(_psar >= iHigh(NULL, Period(), i))
    return iTime(NULL, Period(), i-1);
  }
 }
 
 if(cmd==OP_SELL)
 {
  for(int i=1; i<Bars-1; i++)
  {
   double _psar = iSAR(NULL, Period(), 0.008, 0.2, i);
   
   if(_psar <= iLow(NULL, Period(), i))
    return iTime(NULL, Period(), i-1);
  }
 }      
 
 return NULL;
}


class Reporting
{
private:
 Logger logger;
 
protected:
 int TERMINAL_PK;
 int SLIPPAGE;
 uint RETRIES;
 int EXPIRATION;
 double netTrade(string symbol, int cmd, double lots);
  
public:
 string getPosID(int count);
 string getTradePositionID(string symbol, int cmd);
 
 bool ReportSiteParent(int parent_booknum, int child_booknum, int child_acct_num);
 
 bool ReportSiteCloseBy(int booknum, int opposite_booknum);
 
 bool ReportSiteComparison(int booknum, uint simple_id, Map<string,string> &kwargs);
 
 bool ReportSiteClose(int booknum,
                             string reason,
                             double close_requested, 
                             uint close_latency, 
                             double bid_before, 
                             double bid_after,
                             double ask_before, 
                             double ask_after,
                             Map<string,string> *kwarg);
                     
 bool ReportSiteOpen(int booknum, 
                           string reason,
                           double sl,
                           double tp,
                           int parent_booknum, 
                           int parent_login, 
                           string parent_server,
                           string position,
                           double open_requested, 
                           uint open_latency, 
                           double bid_before, 
                           double bid_after,
                           double ask_before, 
                           double ask_after,
                            Map<string,string> *kwargs);

 int OrderSend(string reason,
                     string symbol,
                     int ticket,
                     double volume,
                     double price,
                     double sl,
                     double tp,
                     const string comment,
                     int magic,
                     int parent_login,
                     int parent_booknum,
                     string parent_server,
                     color arrow_color,
                     Map<string,string> *kwargs=NULL);

 bool OrderCloseBy(int ticket, int opposite, color arrow_color);
 
 bool OrderClose(string reason,
                       int ticket,       
                       double price,
                       double lots,
                       color arrow_color,
                       Map<string,string> *kwargs=NULL);

 bool OrderClose(string reason,
                       uint& latency,
                       double& bid_before,
                       double& bid_after,
                       double& ask_before,
                       double& ask_after,
                       int ticket,       
                       double price,
                       double lots,
                       color arrow_color,
                       Map<string,string> *kwargs);
                         
 int OrderSend(string reason,
                     uint& latency,
                     double& bid_before,
                     double& bid_after,
                     double& ask_before,
                     double& ask_after,
                     string& position,
                     string symbol,
                     int cmd,
                     double volume,
                     double price,
                     double sl,
                     double tp,
                     const string comment,
                     int magic,
                     int parent_login,
                     int parent_booknum,
                     string parent_server,
                     color arrow_color,
                     Map<string,string> *kwargs);
 
 Reporting(string base_directory, int terminal_pk, int slippage, uint retries, int expiration);
};

Reporting::Reporting(string base_directory, int terminal_pk, int slippage=10000, uint retries=3, int expiration=0)
 : logger(__FILE__, "helper_processes"),
   TERMINAL_PK(terminal_pk),
   SLIPPAGE(slippage),
   RETRIES(retries),
   EXPIRATION(expiration)
{
}
//+------------------------------------------------------------------+
double Reporting::netTrade(string symbol,int cmd,double lots)
/*
Purpose of this function is to automatically net any trades
returns: lot amount after netting takes place
*/
{
 BEGIN
 
 int total=OrdersTotal();
 bool bRes=false;
 
 //-- Net open trades in opposite direction (if necessary)
 double cls_lots=0, total_cls_lots=0;
 
 if(cmd==OP_BUY)
 {
  for(int j=0; j<total && cls_lots < lots; j++)
  {
   if(OrderSelect(j,SELECT_BY_POS) && OrderSymbol()==symbol && OrderType()==OP_SELL)
   {
    double vol=MathMin(lots,OrderLots());
    double prc=MarketInfo(symbol,MODE_ASK);
    if(Reporting::OrderClose("Close by", OrderTicket(), prc, vol)) {
     cls_lots += vol;
     j--;
    }
   }
  }
 }
 
 if(cmd==OP_SELL)
 {
  for(int j=0; j<total && cls_lots < lots; j++)
  {
   if(OrderSelect(j,SELECT_BY_POS) && OrderSymbol()==symbol && OrderType()==OP_BUY)
   { 
    double vol=MathMin(lots,OrderLots());
    double prc=MarketInfo(symbol,MODE_BID);
   
    if(Reporting::OrderClose("Close by", OrderTicket(), prc, vol)) {
     cls_lots += vol;
     j--;
    }
   }
  }
 }
 
 lots -= cls_lots;
 
 return lots;
}
//+------------------------------------------------------------------+
string Reporting::getPosID(int count)
{
 switch(count)
 {
  case 1: return "A";
  case 2: return "B";
  case 3: return "C";
  case 4: return "D";
  case 5: return "E";
  case 6: return "F";
  case 7: return "G";
  case 8: return "H";
  case 9: return "I";
  case 10: return "J";
  case 11: return "K";
  case 12: return "L";
  case 13: return "M";
  case 14: return "N";
  case 15: return "O";
  case 16: return "P";
  case 17: return "Q";
  case 18: return "R";
  case 19: return "S";
  case 20: return "T";
  case 21: return "U";
  case 22: return "V";
  case 23: return "W";
  case 24: return "X";
  case 25: return "Y";
  case 26: return "Z";
  case 27: return "AA";
  case 28: return "AB";
  case 29: return "AC";
  case 30: return "AD";
  default: return "";
 }
}
//+------------------------------------------------------------------+
string Reporting::getTradePositionID(string symbol, int cmd)
/*
*/
{
 BEGIN
 
 int total = OrdersTotal();
 int count = 0;
 
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(i,SELECT_BY_POS))
  {
   if(OrderSymbol() == symbol && OrderType()==cmd) 
   {
    LOG_EXTENSIVE("OrderBin::counting #" + (string)OrderTicket());
    count++;
   }
  }
 }
 
 LOG_DEBUG_1_RAW(count);
 return getPosID(count);
}
//+------------------------------------------------------------------+
string GetMachineName()
{
 string machine_name = NULL;
 char buf[128];
 int res = GetComputerNameA(buf, (string)sizeof(buf));
 if (res > 0)
  machine_name = CharArrayToString(buf);
    
 return machine_name;
}
//+------------------------------------------------------------------+
string AccountType()
{
 if(IsDemo())
  return "demo";
 else
  return "live";
}
//+------------------------------------------------------------------+
string GetServerName()
{
 return AccountServer();
}
//+------------------------------------------------------------------+
bool Reporting::ReportSiteClose(int booknum,
                                string reason,
                                double close_requested=0, 
                                uint close_latency=0, 
                                double bid_before=0, 
                                double bid_after=0,
                                double ask_before=0, 
                                double ask_after=0,
                                Map<string,string> *kwargs=NULL)
 {   
   BEGIN
   
   if(StringFind(reason, "=") >= 0)
    LOG_DEV_WARNING("reason not allowed to contain character '='. will be incorrected parsed in file");
   
   string filename = DB_UPDATE_FOLDER_NAME + "\\" + (string)booknum + "_update_close" + ".csv";
     
   int handle = FileOpen(filename, FILE_READ|FILE_WRITE|FILE_CSV);
  
   if(handle >= 0)
   {   
    FileSeek(handle, 0, SEEK_END);
   
    //--- Log trade data
     FileWrite(handle,"booknum=" + (string)booknum);
     
     FileWrite(handle,"terminal_pk=" + (string)Reporting::TERMINAL_PK);
     
     if(close_requested!=0)
      FileWrite(handle,"close_requested="+(string)close_requested);
      
     if(close_latency!=0)
      FileWrite(handle,"close_latency="+(string)close_latency);
     
     if(bid_before!=0)
      FileWrite(handle,"close_bid_before="+(string)bid_before);
     
     if(bid_after!=0)
      FileWrite(handle,"close_bid_after="+(string)bid_after);
      
     if(ask_before!=0)
      FileWrite(handle,"close_ask_before="+(string)ask_before);
     
     if(ask_after!=0)
      FileWrite(handle,"close_ask_after="+(string)ask_after);
     
     if(StringLen(reason) > 0)
      FileWrite(handle,"close_reason="+reason);
      
     if(GetPointer(kwargs)!= NULL) {
      string k[], v[];
      kwargs.copy(k,v);
      string sparam=EncodeStringDict(k,v);
      FileWrite(handle,"kwargs="+sparam);
     }
      
     FileWrite(handle,"upload=TRUE");
     
     LOG_DEBUG("Writing file=" + filename);
   
    //--- Close File
     FileClose(handle);
     
    //--- sent event to site_reporting expert advisor
     EventChartCustomBroadcast(CHARTEVENT_UPDATE_TRADES, 0, 0.0, NULL);
     
     return(true);
   }
  
  LOG_HIGH_ERROR("Could not open filename=" + filename)
  
  return(false);
 }
 //+------------------------------------------------------------------+
bool Reporting::ReportSiteCloseBy(int booknum, int opposite_booknum)
 {   
   __TRACE__

   string filename = DB_UPDATE_FOLDER_NAME + "\\" + (string)booknum + "_update_close_by" + ".csv";

   int handle = FileOpen(filename, FILE_WRITE|FILE_CSV);
  
   if(handle >= 0)
   {   
    //--- Log trade data
     FileWrite(handle,"booknum="+(string)booknum);
     FileWrite(handle,"terminal_pk=" + (string)Reporting::TERMINAL_PK);
     FileWrite(handle,"opposite_booknum="+(string)opposite_booknum);
     FileWrite(handle,"upload=TRUE");
   
    //--- Close File
     FileClose(handle);

    //--- sent event to site_reporting expert advisor
     if(!EventChartCustomBroadcast(CHARTEVENT_UPDATE_TRADES, 0, 0, ""))
      LOG_LOW_ERROR("Failed to broadcase update trades event");

    return(true);
   }

 LOG_HIGH_ERROR("Couldn't open filename=" + filename);
 
 return(false);
 }
//+------------------------------------------------------------------+
bool Reporting::ReportSiteParent(int parent_booknum, int child_booknum, int child_acct_num)
 {   
   __TRACE__

   string filename = DB_UPDATE_FOLDER_NAME + "\\" + (string)parent_booknum + "_update_parent" + ".csv";

   int handle = FileOpen(filename, FILE_WRITE|FILE_CSV);
  
   if(handle >= 0)
   {   
    //--- Log trade data
     FileWrite(handle,"booknum="+(string)parent_booknum);
     FileWrite(handle,"terminal_pk=" + (string)Reporting::TERMINAL_PK);
     FileWrite(handle,"child_booknum="+(string)child_booknum); 
     FileWrite(handle,"child_acct_num="+(string)child_acct_num);
     
     FileWrite(handle,"upload=TRUE");
   
    //--- Close File
     FileClose(handle);

    //--- sent event to site_reporting expert advisor
     if(!EventChartCustomBroadcast(CHARTEVENT_UPDATE_TRADES, 0, 0, ""))
      LOG_LOW_ERROR("Failed to broadcase update trades event");

    return(true);
   }

 LOG_HIGH_ERROR("Couldn't open filename=" + filename);
 
 return(false);
 }
//+------------------------------------------------------------------+
bool Reporting::ReportSiteOpen(int booknum, 
                               string reason,
                               double sl,
                               double tp,
                               int parent_booknum=0, 
                               int parent_login=0, 
                               string parent_server="",
                               string position="",
                               double open_requested=0, 
                               uint open_latency=0, 
                               double bid_before=0, 
                               double bid_after=0,
                               double ask_before=0, 
                               double ask_after=0,
                               Map<string,string> *kwargs=NULL)
{   
  BEGIN
  
  if(StringFind(reason, "=") >= 0)
   LOG_DEV_WARNING("reason not allowed to contain character '='. will be incorrected parsed in file");
  
  string filename = DB_UPDATE_FOLDER_NAME + "\\" + (string)booknum + "_update_open" + ".csv";
     
  int handle = FileOpen(filename, FILE_WRITE|FILE_CSV);
  
  if(handle >= 0)
  {   
   //--- Log trade data
    FileWrite(handle,"terminal_pk=" + (string)Reporting::TERMINAL_PK);
    FileWrite(handle,"chart_id=" + (string)ChartID());
    FileWrite(handle,"booknum=" + (string)booknum);
    FileWrite(handle,"acctNumber=" + (string)AccountNumber());
    FileWrite(handle,"acctServer=" + GetServerName());
    FileWrite(handle,"position="+position);
    FileWrite(handle,"open_reason="+reason);
    FileWrite(handle,"sl="+(string)sl);
    FileWrite(handle,"tp="+(string)tp);
     
    if(parent_booknum > 0 || parent_login > 0 || StringLen(parent_server) > 0)
    {
     if(parent_booknum<=0 || parent_login<=0 || StringLen(parent_server)<=0)
     {
      LOG_HIGH_3(parent_booknum, parent_login, parent_server);
      return false;
     }
     
     FileWrite(handle,"parent_booknum="+(string)parent_booknum);
     FileWrite(handle,"parent_login="+(string)parent_login);
     FileWrite(handle,"parent_server="+(string)parent_server);
    }
      
    if(open_requested!=0)
     FileWrite(handle,"open_requested="+(string)open_requested);
     
    if(open_latency!=0)
     FileWrite(handle,"open_latency="+(string)open_latency);
     
    if(bid_before!=0)
     FileWrite(handle,"open_bid_before="+(string)bid_before);
     
    if(bid_after!=0)
     FileWrite(handle,"open_bid_after="+(string)bid_after);
     
    if(ask_before!=0)
     FileWrite(handle,"open_ask_before="+(string)ask_before);
     
    if(ask_after!=0)
     FileWrite(handle,"open_ask_after="+(string)ask_after);
   
    if(CheckPointer(kwargs)!=NULL) {
      string k[], v[];
      kwargs.copy(k,v);
      string sparam=EncodeStringDict(k,v);
      FileWrite(handle,"kwargs="+sparam);
     }
     
    FileWrite(handle,"upload=TRUE");
    
    LOG_DEBUG("Writing file=" + filename);
   
   //--- Close File
    FileClose(handle);
    
   //--- sent event to site_reporting expert advisor
    if(!EventChartCustomBroadcast(CHARTEVENT_UPDATE_TRADES, 0, 0, ""))
     LOG_LOW_ERROR("Failed to broadcase update trades event");
     
    return(true);
  }
  
 LOG_HIGH_ERROR("Couldn't open filename=" + filename);
 
 return(false);
}
//+------------------------------------------------------------------+
bool Reporting::ReportSiteComparison(int booknum, uint simple_id, Map<string,string> &kwargs)
{
 /*
 simple identifier assumes requests unique at terminal level
 namespacing should include unique identifier terminal_level=<acctName><acctServer> &
 a unique identifier for simultaneous requests inside the terminal
 because multiple terminals could each make simultaneous requests
 */
 
 __TRACE__
 
 string filename = DB_UPDATE_FOLDER_NAME + "\\" + (string)booknum + "_update_comparison_" + (string)simple_id + ".csv";

 LOG_DEBUG_1_RAW(filename);

   int handle = FileOpen(filename, FILE_WRITE|FILE_CSV);

   if(handle >= 0)
   {   
    //--- Log trade data
     FileWrite(handle,"booknum="+(string)booknum);
     FileWrite(handle,"terminal_pk=" + (string)Reporting::TERMINAL_PK);
     
     string k[], v[];
     kwargs.copy(k,v);
     string sparam=EncodeStringDict(k,v);
     FileWrite(handle,"kwargs="+sparam);
   
     FileWrite(handle,"upload=TRUE");
   
    //--- Close File
     FileClose(handle);
     
     LOG_DEBUG("Send comparison [" + (string)booknum + "]: " + sparam);

    //--- sent event to site_reporting expert advisor
     if(!EventChartCustomBroadcast(CHARTEVENT_UPDATE_TRADES, 0, 0, ""))
      LOG_LOW_ERROR("Failed to broadcase update trades event");

    return(true);
   }

 LOG_HIGH_ERROR("Couldn't open filename=" + filename);
 
 return(false);
}
//+------------------------------------------------------------------+
bool Reporting::OrderClose(string reason,
                           uint& latency,
                           double& bid_before,
                           double& bid_after,
                           double& ask_before,
                           double& ask_after,
                           int ticket,       
                           double price,
                           double lots=0.0,
                           color arrow_color=clrNONE,
                           Map<string,string> *kwargs=NULL)
{
 BEGIN
 
 LOG_DEBUG("Attempting to close #" + (string)ticket);
 
 bool ans = false;
 int e=0;
 
 if(OrderSelect(ticket, SELECT_BY_TICKET))
 {
  uint attempts = 0;
  string symbol = OrderSymbol();
  int cmd = OrderType();
  int digits = (int)MarketInfo(symbol, MODE_DIGITS);
  double before_close_ticket_price=0, after_close_ticket_price=0;
  
  if(MathAbs(lots) < DBL_EPSILON)
   lots=OrderLots();
 
  do
  {
   price = NormalizeDouble(price, digits);
   
   RefreshRates();
   bid_before = MarketInfo(symbol, MODE_BID);
   ask_before = MarketInfo(symbol, MODE_ASK);
   before_close_ticket_price = OrderClosePrice();
 
   uint t0 = GetTickCount();
   ans = OrderClose(ticket, lots, price, Reporting::SLIPPAGE, arrow_color);
   uint t1 = GetTickCount();
 
   RefreshRates();
   bid_after = MarketInfo(symbol, MODE_BID);
   ask_after = MarketInfo(symbol, MODE_ASK);
   after_close_ticket_price = OrderClosePrice();
 
   latency = t1 - t0;
   
   if(!ans) {
    e=GetLastError();
    
    if(e==131)    //ERR_INVALID_TRADE_VOLUME
    {
     lots=MathMin(lots, OrderLots());
    }
    
    double new_price=0;
    RefreshRates();
    if(cmd==OP_BUY)
     new_price = MarketInfo(symbol,MODE_BID);
    else if(cmd==OP_SELL)
     new_price = MarketInfo(symbol,MODE_ASK);
    
    if(new_price > DBL_EPSILON && MathAbs(new_price - price) > DBL_EPSILON) {
     LOG_DEBUG("modifying requested price(" + (string)price + " to market price(" + (string)new_price + ")");
     price = new_price;
    }
   
    Sleep(250);
   }
  }
  while(!ans && attempts++ < Reporting::RETRIES);
  
  LOG_DEBUG_5_UUID(ticket, symbol, lots, price, Reporting::SLIPPAGE);
  LOG_DEBUG("***** Previous variables passed to OrderClose() *****");
  
  if(MathAbs(before_close_ticket_price - price) > DBL_EPSILON)
   LOG_WARNING("requested_cls_price(" + (string)price + ") <> close price on ticket (" + (string)before_close_ticket_price + ")"); 
  
  before_close_ticket_price = NormalizeDouble(before_close_ticket_price, digits);
  after_close_ticket_price = NormalizeDouble(after_close_ticket_price, digits);
  LOG_DEBUG_2(before_close_ticket_price, ", ", after_close_ticket_price);
  
  if(ans)
  {
   ReportSiteClose(ticket, reason, price, latency, bid_before, bid_after, ask_before, ask_after, kwargs);
   LOG_DEBUG_4(ticket, reason, price, latency);
   LOG_DEBUG_4(bid_before, bid_after, ask_before, ask_after);
   LOG_DEBUG("***** Previous variables passed to ReportSiteClose()");
   
   if(!EventChartCustomBroadcast(CHARTEVENT_UPDATE_TRADES, 0, 0, ""))
    LOG_LOW_ERROR("Failed to broadcase update trades event");
  }
  else
   LOG_HIGH("Failed to close #" + (string)ticket + " for e=" + ErrorDescription(e));
   
  if(attempts >= Reporting::RETRIES)
   LOG_INFO_2(attempts, ">=", Reporting::RETRIES);
 
  return ans;
 }
 
 LOG_HIGH("Could not select #" + (string)ticket);
 return false;
}
//+------------------------------------------------------------------+
void OrdersRefresh()
// this function does nothing, but is needed to refresh order queue
{
 int t=OrdersTotal();
 for(int i=0;i<t;i++)
  bool ans=OrderSelect(i,SELECT_BY_POS);
}
//+------------------------------------------------------------------+
bool Reporting::OrderCloseBy(int ticket, int opposite, color arrow_color=clrNONE)
{
 __TRACE__

 if(!ReportSiteCloseBy(ticket, opposite))
  LOG_LOW("Failed to log order close by, ticket=" + (string)ticket + ", opposite=" + (string)opposite); 
  
 //--- pre-closeby snapshot: calculate max booknum
  int max_booknum=0;
  int total;
  total=OrdersTotal();
  for(int i=0; i<total; i++)
  {
   if(OrderSelect(i,SELECT_BY_POS))
    max_booknum=MathMax(max_booknum, OrderTicket());
  }
 
 //--- check for partial open
  int parent_booknum=-1, child_booknum=-1;
  int ticket_type, opposite_type;
  double ticket_lots, opposite_lots;
  string ticket_symbol, opposite_symbol;
  bool b_child_booknum_expected=true;
  
 if(!OrderSelect(ticket,SELECT_BY_TICKET))
  LOG_HIGH("Unable to select ticket #" + (string)ticket);
  
 ticket_type=OrderType();
 ticket_lots=OrderLots();
 ticket_symbol=OrderSymbol();
 
 if(!OrderSelect(opposite,SELECT_BY_TICKET))
  LOG_HIGH("Unable to select opposite ticket #" + (string)opposite);
  
 opposite_type=OrderType();
 opposite_lots=OrderLots();
 opposite_symbol=OrderSymbol();
 
 bool ans=::OrderCloseBy(ticket, opposite, arrow_color);
 
 if(!ans) {
  LOG_HIGH("Failed to execute OrderCloseBy, ticket=" + (string)ticket + ", opposite=" + (string)opposite);
  return ans;
 }
 
 if(ticket_lots > opposite_lots)
 {
  double excess_lots = ticket_lots - opposite_lots;
  total=OrdersTotal();
  OrdersRefresh();
  for(int i=0; i<total; i++)
  { 
   int _ticket=OrderTicket();
   if(OrderSelect(i,SELECT_BY_POS) && _ticket > max_booknum)
   {
    if(OrderType()==ticket_type && OrderSymbol()==ticket_symbol && MathAbs(OrderLots()-excess_lots) < DBL_EPSILON)
    {
     child_booknum=_ticket;
     break;
    }
   }
  }
  parent_booknum = ticket;
 }
 
 else if(ticket_lots < opposite_lots)
 {
  double excess_lots = opposite_lots - ticket_lots;
  total=OrdersTotal();
  OrdersRefresh();
  for(int i=0; i<total; i++)
  { 
   int _ticket=OrderTicket();
   if(OrderSelect(i,SELECT_BY_POS) && _ticket > max_booknum)
   {
    if(OrderType()==opposite_type && OrderSymbol()==opposite_symbol && MathAbs(OrderLots()-excess_lots) < DBL_EPSILON)
    {
     child_booknum=_ticket;
     break;
    }
   }
  }
  parent_booknum = opposite;
 }
 else
  return ans;
  
 if(child_booknum > 0)
 {
  ReportSiteParent(parent_booknum, child_booknum, AccountNumber());
  LOG_DEBUG("Report parent #" + (string)parent_booknum + " -> child #" + (string)child_booknum);
 }
 else
 {
  LOG_LOW("Failed to find child booknum for ticket(#" + (string)ticket + ", " + (string)ticket_lots + " lots), opposite(#" + (string)opposite + ", " + (string)opposite_lots + " lots)");
 }
 
 return ans;
}
//+------------------------------------------------------------------+
int Reporting::OrderSend(string reason,
                         uint& latency,
                         double& bid_before,
                         double& bid_after,
                         double& ask_before,
                         double& ask_after,
                         string& position,
                         string symbol,
                         int cmd,
                         double volume,
                         double price,
                         double sl=0.0,
                         double tp=0.0,
                         const string comment=NULL,
                         int magic=0,
                         int parent_login=0,
                         int parent_booknum=0,
                         string parent_server="",
                         color arrow_color=clrNONE,
                         Map<string,string> *kwargs=NULL)
{
 BEGIN
 
 LOG_DEBUG_RAW("Request new order open ...");
 
 if(symbol==NULL)
   symbol=Symbol();
 
 int ticket = 0;
 string error = NULL;
 
 __ volume = netTrade(symbol, cmd, volume);
 
 if(volume > 0)
 {
  uint attempts = 0;
  int digits = (int)MarketInfo(symbol, MODE_DIGITS);
 
  do 
  {
   price = NormalizeDouble(price, digits);
  
   RefreshRates();
   bid_before = MarketInfo(symbol,MODE_BID);
   ask_before = MarketInfo(symbol,MODE_ASK);
 
   uint t0 = GetTickCount();
   ticket = OrderSend(symbol,cmd,volume,price,Reporting::SLIPPAGE,sl,tp,comment,magic,Reporting::EXPIRATION,arrow_color);
   uint t1 = GetTickCount();
 
   RefreshRates();
   bid_after = MarketInfo(symbol,MODE_BID);
   ask_after = MarketInfo(symbol,MODE_ASK);
  
   LOG_DEBUG_5_UUID(ticket, symbol, cmd, volume, price);
   LOG_DEBUG_5_UUID(Reporting::SLIPPAGE, sl, tp, comment, magic);
   LOG_DEBUG_2(Reporting::EXPIRATION, ", ", arrow_color);
   LOG_DEBUG_3(parent_booknum, parent_login, parent_server);
   LOG_DEBUG("***** Previous variable passed to OrderSend() *****");
 
   latency = t1 - t0;
   
   LOG_DEBUG_5(latency, bid_before, bid_after, ask_before, ask_after);
 
   if(ticket > 0)
   {
    position = getTradePositionID(symbol, cmd);
    
    if(!ReportSiteOpen(ticket, reason, sl, tp, parent_booknum, parent_login, parent_server, position, price, latency,
                      bid_before, bid_after, ask_before, ask_after, kwargs))
       LOG_HIGH("Failed to log #" + (string)ticket + " to web server");
      
    LOG_DEBUG_1(position);
   }
   else 
   {
    error = ErrorDescription(GetLastError());
    int wait=250;
    Sleep(wait);
    
    string err_msg="Unable to open trade (" +symbol+", "+EnumToString((ENUM_ORDER_TYPE)cmd)+", vol="+(string)volume+", e=" + error + "), try again in " + (string)wait + " ms";
    if(attempts < Reporting::RETRIES)    //dont call GetLastError twice
    { 
     LOG_LOW(err_msg);
    }
    else
    {
     LOG_INFO(err_msg);
    }
    double new_price=0;
    RefreshRates();
    if(cmd==OP_BUY)
     new_price = MarketInfo(symbol,MODE_ASK);
    else if(cmd==OP_SELL)
     new_price = MarketInfo(symbol,MODE_BID);
    
    if(new_price > DBL_EPSILON && MathAbs(new_price - price) > DBL_EPSILON) {
     LOG_DEBUG("modifying requested price(" + (string)price + " to market price(" + (string)new_price + ")");
     price = new_price;
    }
   }
  }
  while(ticket <= 0 && attempts++ < Reporting::RETRIES);
 
  if(attempts >= Reporting::RETRIES)
   LOG_DEBUG_2(attempts, ">=", Reporting::RETRIES);
 
  if(ticket <= 0)
   LOG_HIGH("Failed to open order: (" + symbol + ",cmd=" + EnumToString((ENUM_ORDER_TYPE)cmd) + ", volume=" + (string)volume + "), e=" + error);  
 }
 
 return ticket;
}
//+------------------------------------------------------------------+
int Reporting::OrderSend(string reason,
                                string symbol,
                                int cmd,
                                double volume,
                                double price,
                                double sl=0.000000,
                                double tp=0.000000,
                                const string comment=NULL,
                                int magic=0,
                                int parent_login=0,
                                int parent_booknum=0,
                                string parent_server=NULL,
                                color arrow_color=4294967295,
                                Map<string,string> *kwargs=NULL)
{ 
 uint latency;
 double bid_before, bid_after, ask_before, ask_after;
 string position;
 
 return Reporting::OrderSend(reason, latency, bid_before, bid_after, ask_before, ask_after, position, symbol, cmd, volume, price,
                                 sl, tp, comment, magic, parent_login, parent_booknum, parent_server, arrow_color, kwargs);
}
//+------------------------------------------------------------------+
bool Reporting::OrderClose(string reason,
                                  int ticket,
                                  double price,
                                  double lots,
                                  color arrow_color=clrNONE,
                                  Map<string,string> *kwargs=NULL)
{
 uint latency;
 double bid_before, bid_after, ask_before, ask_after;
 
 return Reporting::OrderClose(reason, latency, bid_before, bid_after, ask_before, ask_after, ticket, price, lots, arrow_color, kwargs);
}


//+------------------------------------------------------------------+
class TradeEngine
{
protected:
 static Logger logger;
 
public:
 static void disable_open(string symbol, int cmd);
 static void disable_close(string symbol, int cmd);
 static void enable_open(string symbol, int cmd);
 static void enable_close(string symbol, int cmd);
 static bool is_open_enabled(string symbol, int cmd);
 static bool is_close_enabled(string symbol, int cmd);
 static void close(int ticket, int __LINE__macro, string __FUNCTION__macro, string reason, double lots, double cls_prc, string ref_id); 
 static void open(string symbol, int cmd, double volume, int __LINE__macro, string __FUNCTION__macro, string reason, double price, string order_comment, int magic, string ref_id);
 static void open(string symbol, int cmd, double volume, KwargsDict &kwargs, int __LINE__macro, string __FUNCTION__macro, string reason, double price, string order_comment, int magic, string ref_id);
 static bool trade_confirmation_close(const long lparam, const double dparam, const string sparam);
 static int trade_confirmation_open(const long lparam, const double dparam, const string sparam, KwargsDict& OUT_kwargs);
};

static Logger TradeEngine::logger(__FILE__, "helper_processes");

static bool TradeEngine::is_open_enabled(string symbol, int cmd)
{
 return _isEnabledOpen(symbol, cmd);
}

static bool TradeEngine::is_close_enabled(string symbol, int cmd)
{
 return _isEnabledClose(symbol, cmd);
}

static void TradeEngine::enable_close(string symbol,int cmd)
{
 if(symbol==NULL)  symbol = Symbol();
 
 string glb_var_name=get_glb_var_name(OPERATION_CLOSE_TRADE, symbol, cmd);
 
 if(StringLen(glb_var_name) > 0)
 {
  GlobalVariableDel(glb_var_name);
  logger.log("Enabling close for symbol=" + symbol + ", cmd=" + EnumToString((ENUM_ORDER_TYPE)cmd), LOG_TYPE_INFO, __LINE__, __FUNCTION__, true);
 }
}

static void TradeEngine::disable_close(string symbol,int cmd)
{
 if(symbol==NULL)  symbol = Symbol();
 
 string glb_var_name=get_glb_var_name(OPERATION_CLOSE_TRADE, symbol, cmd);
 
 if(StringLen(glb_var_name) > 0)
 {
  GlobalVariableSet(glb_var_name, 1);
  logger.log("Disabling close for symbol=" + symbol + ", cmd=" + EnumToString((ENUM_ORDER_TYPE)cmd), LOG_TYPE_INFO, __LINE__, __FUNCTION__, true);
 }
}

static void TradeEngine::enable_open(string symbol,int cmd)
{
 if(symbol==NULL)  symbol = Symbol();
 
 string glb_var_name=get_glb_var_name(OPERATION_OPEN_TRADE, symbol, cmd);
 
 if(StringLen(glb_var_name) > 0)
 {
  GlobalVariableDel(glb_var_name);
  logger.log("Enabling open for symbol=" + symbol + ", cmd=" + EnumToString((ENUM_ORDER_TYPE)cmd), LOG_TYPE_INFO, __LINE__, __FUNCTION__, true);
 }
}

static void TradeEngine::disable_open(string symbol,int cmd)
{
 if(symbol==NULL)  symbol = Symbol();
  
 string glb_var_name=get_glb_var_name(OPERATION_OPEN_TRADE, symbol, cmd);
 
 if(StringLen(glb_var_name) > 0)
 {
  GlobalVariableSet(glb_var_name, 1);
  logger.log("Disabling open for symbol=" + symbol + ", cmd=" + EnumToString((ENUM_ORDER_TYPE)cmd), LOG_TYPE_INFO, __LINE__, __FUNCTION__, true);
 }
}

static bool TradeEngine::trade_confirmation_close(const long lparam,const double dparam,const string sparam)
{
  int ticket = (int)lparam;
  double lots = dparam;
  string response_msg = "Confirmed closed #" + (string)ticket + ", " + DoubleToStr(lots, 2) + " lots";
 
  if(StringLen(sparam) > 0)
  {
   response_msg += ", " + sparam;
   logger.log(response_msg, LOG_TYPE_INFO, __LINE__, __FUNCTION__);
   return true;
  }
  else
  {
   logger.log("Error closing #" + (string)ticket + ", " + sparam, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
   return false;
  }
}


static int TradeEngine::trade_confirmation_open(const long lparam,const double dparam,const string sparam,KwargsDict& OUT_kwargs)
{
  int ticket = (int)lparam;
  double lots = dparam;
  string response_msg = "Confirmed open #" + (string)ticket + ", " + DoubleToStr(lots, 2) + " lots";
  
  DecodeStringDict(sparam, OUT_kwargs);
  
  string error = OUT_kwargs["error"];
  if(error != "None")
   logger.log("Error opening order, e=" + error, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
  else
   logger.log(response_msg, LOG_TYPE_INFO, __LINE__, __FUNCTION__);
 
  return ticket;
}


static void TradeEngine::open(string symbol,int cmd,double volume,KwargsDict& kwargs,int __LINE__macro,string __FUNCTION__macro,string reason,double price=0.0,string order_comment=NULL,int magic=0,string ref_id=NULL)
{
 if(symbol==NULL)
  symbol = Symbol();
 
 string v[7];
 v[0] = WindowExpertName();
 v[1] = ref_id;
 v[2] = symbol;
 v[3] = IntegerToString(cmd);
 v[4] = DoubleToStr(volume, 2);
 v[5] = order_comment;
 
 string key[], val[];
 int sz=kwargs.copy(key,val);
 v[6] = EncodeStringDict(key,val);
 
 string sparam = EncodeStringDict(v);

 if(!EventChartCustomBroadcast(CHARTEVENT_TRADE_OPEN, magic, price, sparam)) {
   logger.log("EventChartCustomBroadcast() returned false", LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
   logger.log(symbol + ", " + EnumToString((ENUM_ORDER_TYPE)cmd) + ", prc=" + DoubleToStr(price, Digits) + ", time_current=" + TimeToStr(TimeCurrent()), LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
  }
}


static void TradeEngine::open(string symbol,int cmd,double volume,int __LINE__macro,string __FUNCTION__macro,string reason,double price=0.0,string order_comment=NULL,int magic=0,string ref_id=NULL)
{
 if(symbol==NULL)
  symbol = Symbol();
 
 string v[7];
 v[0] = WindowExpertName();
 v[1] = ref_id;
 v[2] = symbol;
 v[3] = IntegerToString(cmd);
 v[4] = DoubleToStr(volume, 2);
 v[5] = order_comment;
 v[6] = "";
 
 string sparam = EncodeStringDict(v);

 if(!EventChartCustomBroadcast(CHARTEVENT_TRADE_OPEN, magic, price, sparam))
   logger.log("EventChartCustomBroadcast() returned false", LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
}


static void TradeEngine::close(int ticket, int __LINE__macro, string __FUNCTION__macro, string reason, double lots=0.0, double cls_prc=0.0, string ref_id=NULL)
{
 logger.log("Attempt close #" + (string)ticket + " for " + reason, LOG_TYPE_INFO, __LINE__macro, __FUNCTION__macro, true);
 
 if(OrderSelect(ticket, SELECT_BY_TICKET))
 {
  string symbol = OrderSymbol();
  int digits = (int)MarketInfo(symbol, MODE_DIGITS);
  
  if(MathAbs(lots) < DBL_EPSILON)
   lots = OrderLots();
  
  if(MathAbs(cls_prc) < DBL_EPSILON)
  {
   int type = OrderType();
   
   if(type == OP_BUY)
    cls_prc = MarketInfo(symbol, MODE_BID);
   else if(type == OP_SELL)
    cls_prc = MarketInfo(symbol, MODE_ASK);
   else {
    logger.log("can only close market orders", LOG_TYPE_HIGH, __LINE__, __FILE__);
    return;
   }
  }
  
  string v[5];
  v[0] = WindowExpertName();
  v[1] = ref_id;
  v[2] = IntegerToString(ticket);
  v[3] = DoubleToStr(lots, 2);
  v[4] = DoubleToStr(cls_prc, digits);
  
  string sparam = EncodeStringDict(v);
  
  if(!EventChartCustomBroadcast(CHARTEVENT_TRADE_CLOSE, ChartID(), 0.0, sparam))
   logger.log("EventChartCustomBroadcast() returned false", LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
 }
 else {
  logger.log("Could not select ticket #" + (string)ticket, LOG_TYPE_HIGH, __LINE__, __FILE__);
  return;
 }
}

void GlobalVariableSetString(string glb_var_prefix, string glb_var_value)
{
 string var_str = StringConcatenate(glb_var_prefix, glb_var_value);
 GlobalVariableSet(var_str, 0);
}


string GlobalVariableGetString(string glb_var_prefix)
{
 int total=GlobalVariablesTotal();
 int prefix_len=StringLen(glb_var_prefix);
 
 for(int i=0; i<total; i++)
 {
  string name=GlobalVariableName(i);
  
  if(StringSubstr(name, 0, prefix_len)==glb_var_prefix)
   return StringSubstr(name, prefix_len);
 }
 
 return NULL;
}

void FilePathSplit(string IN_fullpath, string &OUT_filepath, string &OUT_filename)
{
 OUT_filepath=NULL;
 OUT_filename=NULL;
 
 string res[];
 StringSplit(IN_fullpath, '\\', res);
 int sz=ArraySize(res);
 
 for(int i=0; i<sz-1; i++)
  OUT_filepath += res[i] + "\\"; 

 OUT_filename = res[sz-1];
}

bool CreateFileIfNotExists(string path)
{
 string res[];
 StringReplace(path, "//", "/");
 StringSplit(path,'/',res);
 
 string p = "";
 int size = ArraySize(res);
 for(int i=0; i<size-1; i++) 
 {
  FolderCreate(p + res[i]);
  p += res[i] + "//";
 }
    
 if(!FileIsExist(path))
 {
  int handle = FileOpen(path, FILE_WRITE);
  if(handle >= 0)
  FileClose(handle);
  return true;
 }
 
 return false; 
}

class Async
{
protected:
 Logger logger;
 ulong p_uuid; 
 string p_name;

public:
 bool GetResponse(string account_server, string account_name, ulong uuid, ASYNC_RESPONSE &type, string &msg, uint timeout);
 ulong SendRequest(ASYNC_REQUEST type, string account_server, string account_name, string args);
 Async(string sender_name) : p_name(sender_name), logger(__FILE__, "helper_processes"), p_uuid(0) {};
};

//+------------------------------------------------------------------+
bool Async::GetResponse(string sender_server_name, string account_name, ulong uuid, ASYNC_RESPONSE &type, string &msg, uint timeout)
{
 __TRACE__
 
 string response_dir = "AsyncServer\\" + sender_server_name + "\\" + account_name + "\\Response\\" + AccountServer() + "\\" + p_name + "\\";
 
 string fpath = response_dir+(string)uuid+".dat";
 
 LOG_DEBUG_RAW("Fetching from " + fpath);
 
 uint t0 = (uint)TimeLocal();
 
 while(TimeLocal() - t0 <= timeout)
 {
    int handle=FileOpen(fpath, FILE_READ|FILE_BIN|FILE_COMMON);
   
    if(handle>=0)
    {
     type=(ASYNC_RESPONSE)FileReadInteger(handle);
     
     char resp[100];
     FileReadArray(handle, resp);
     msg = CharArrayToString(resp);
     
     FileClose(handle);
     
     FileDelete(fpath, FILE_COMMON);
     
     return type;
    }
    
    Sleep(100);
 }
 
 msg = "AsyncGetResponse() timeout after " + (string)timeout + " seconds";
 return false;
}

ulong Async::SendRequest(ASYNC_REQUEST type, string sender_server_name, string account_name, string args)
/*
writes a .dat file to the common directory namespaced by the given account name.
returns a unique identifier generated by GetMicrosecond count if successful,
 otherwise return 0.
 args: identifier - unique identifier of the agent who sent the request. e.g. - "<MT4_Server><MT4_Account_Number>"
*/
{
   __TRACE__
   
   p_uuid++;
   
   LOG_DEBUG("request=" + EnumToString(type) + " for " + sender_server_name + "::" + account_name + ", uuid=" + (string)p_uuid + ",args=" + args);
   
   string _BASE_DIR = "AsyncServer\\" + sender_server_name + "\\" + account_name + "\\";
   
   int handle=FileOpen(_BASE_DIR+(string)p_uuid+".dat", FILE_WRITE|FILE_BIN|FILE_COMMON);

   AsyncRequest request(p_name, type, args);
   
   if(handle>=0)
   {
    FileWriteStruct(handle, request);
   }
   else {
    Alert("error: ", GetLastError());
    return 0;
   }
   
   FileClose(handle);
   return p_uuid;
}

bool FetchCredsFromSiteConfig(string &host, string &account_name)
{
 string filename="site\\config.txt";
 const int ATTEMTPS=20;
 
 for(int i=0; i<ATTEMTPS; i++)
 {
  int handle=FileOpen(filename, FILE_READ|FILE_SHARE_READ);
 
  if(handle>=0)
  {
   bool b_found_settings=false;
  
   while(!FileIsEnding(handle))
   {
    string line=StringTrimLeft(StringTrimRight(FileReadString(handle)));
   
    if(b_found_settings) 
    {
     int pos=StringFind(line, ":");
     if(pos > 0)
     {
      string key=StringTrimLeft(StringTrimRight(StringSubstr(line, 0, pos)));
      string val=StringTrimLeft(StringTrimRight(StringSubstr(line, pos+1)));
     
      if(key=="host") {
       host = val;
      }
      
      if(key=="account_name") {
       account_name = val;
      }
     }
    }
    
    if(StringSubstr(line,0,9) == "[settings")
     b_found_settings=True;
   }
  
   FileClose(handle);
   return true;
  }
 
  Sleep(100);
 }
 
 Print("Failed to open " + filename + ", " + (string)ATTEMTPS + " attempts, e='" + ErrorDescription(GetLastError()), "' in ", __FUNCTION__);
 return false;
}
