//+------------------------------------------------------------------+
//|                                                     Context.mqh  |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#ifndef FILEMESSENGER
#define FILEMESSENGER
   #include <FileMessenger.mqh>
#endif

#ifndef UTILS
#define UTILS
   #include <utils.mqh>
#endif

#ifndef ARRAYDOUBLE
#define ARRAYDOUBLE
   #include <Arrays/ArrayDouble.mqh>
#endif

#ifndef ARRAYLONG
#define ARRAYLONG
   #include <Arrays/ArrayLong.mqh>
#endif 

#ifndef AUTOSAVEARRAYSTRINGCOMMUNITY
#define AUTOSAVEARRAYSTRINGCOMMUNITY
   #include <Arrays/AutoSaveArrayStringCommunity.mqh>
#endif 

#ifndef STRINGS
#define STRINGS
   #include <Packages/Strings.mqh>
#endif 

#ifndef WEBINTERFACE
#define WEBINTERFACE
   #include <Packages\WebInterface.mqh>
#endif 

#define ASCII_RECORD_SEPERATOR         30

input string HOST_SITE = "http://127.0.0.1";

class Context
{
 private:
  Logger logger;
  FileMessenger* p_messenger;
  string get_equity();
  string get_exposure();
  string fetch_new_job();
  string check_last_update(string program_name);
  string close_terminal();
  string ping();
  string post_log_to_server(string level, datetime timestamp, string message, string symbol, string timeframe, string traceback); 
  string open_trades(const CArrayString& symbol, const CArrayString& volume);
  string close_trades(const CArrayString& symbol);
  string close_trades(const CArrayInt& ticket);
  string enable_trades();
  string enable_trades(const CArrayString& symbol);
  string disable_trades();
  string disable_trades(const CArrayString& symbol);
  string apply_ea(string name);
  string apply_template(long chart_id, string file_name);
  string save_template(long chart_id, string file_name);
  string create_chart(string symbol, ENUM_TIMEFRAMES period, string chart_template=NULL);
  string chart_set_symbol_period(long chart_id, string symbol, int period);
  
  long _create_chart(string symbol, int timeframe, string& err, string chart_template=NULL);
  ENUM_TIMEFRAMES _StringToTimeframe(string period);
  
  template <typename T>
  bool _get_value(const CArrayString &key, const CArrayString &value, const string IN_key, string &OUT_value);
  
  WebServer webserver;
  
  void ChartsCloseAll();
 public:
  void serve(string request, string _args);
  Context(FileMessenger* messenger);
};
//+------------------------------------------------------------------+
template <typename T>
bool Context::_get_value(const CArrayString &key,const CArrayString &value,const string IN_key, T &OUT_value)
{
  if(key.Total() != value.Total()) {
   logger.log("key[total]=" + (string)key.Total() + " != " + "value[total]=" + (string)value.Total(), LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
   return false;
  }

  int i = key.SearchLinear(IN_key);
  
  if(i >= 0)
  {
   OUT_value = (T)value[i];
   return true;
  }
  else
  {
   logger.log("could not find `" + IN_key + "` in args", LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
   LOG_HIGH_1(key);
   LOG_HIGH_1(value);
   
   return false;
  }
}
//+------------------------------------------------------------------+
void Context::serve(string _request,string _args)
{
 CArrayString key;
 CArrayString value;
 
 if(_request == NULL) {
  logger.log("Ignoring NULL request", LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
  return;
 }

 if(_request == "get_equity")
  get_equity();
  
 else if(_request == "post_log_to_server")
 {
  SplitArgs(_args, key, value, ASCII_RECORD_SEPERATOR, '\t');
  
  datetime timestamp;
  string level, message, traceback, symbol, timeframe, terminal_pk;
  
  if(!_get_value(key, value, "timeframe", timeframe))
   return;
   
  if(!_get_value(key, value, "level", level))
   return;
   
  if(!_get_value(key, value, "message", message))
   return;
   
  if(!_get_value(key, value, "traceback", traceback))
   return;
   
  if(!_get_value(key, value, "symbol", symbol))
   return;
   
  if(!_get_value(key, value, "timestamp", timestamp))
   return;
  
  
  post_log_to_server(level, timestamp, message, symbol, timeframe, traceback);
 }
 
 else if(_request == "get_exposure")
  get_exposure();
 
 else if(_request == "open_trades")
 {
  SplitArgs(_args, key, value, ',');
  open_trades(key, value);
 }
 
 else if(_request == "enable_symbols")
 {
  if(_args == "all")
   enable_trades();
  else {
   SplitArgs(_args, key, value);
   enable_trades(key);
  }
 }
 
 else if(_request == "disable_symbols")
 {
  if(_args == "all")
   disable_trades();
  else {
   SplitArgs(_args, key, value);
   disable_trades(key);
  }
 }
 
 else if(_request == "close_trades")
 {
  CArrayInt Ticket;
  bool is_tickets = true;
  SplitArgs(_args, key, value);
  
  int total = key.Total();
  for(int i=0; i<total; i++)
  {
   int val = (int)key[i];
   
   if(val <= 0) {
    is_tickets = false;
    break;
   }
   else
    Ticket.Add(val);
  }
  
  if(is_tickets) {
   close_trades(Ticket);
  }
  else
   close_trades(key);
 } 
 
 else if(_request == "apply_ea")
 {
  string name = _args;
  apply_ea(name);
 }
 
 else if(_request == "apply_template")
 {
  SplitArgs(_args, key, value);
  
  if(key.Total() == 2)
  {
   long chart_id = (long)key[0];
   string file_name = key[1];
  
   apply_template(chart_id, file_name);
  }
  else
   Alert("Error splitting args=" + _args);
 }
 
 else if(_request == "save_template")
 { 
  SplitArgs(_args, key, value);
  
  if(key.Total() == 2)
  {
   long chart_id = (long)key[0];
   string file_name = key[1];
  
   save_template(chart_id, file_name);
  }
  else
   Alert("Error splitting args=" + _args);
 }
 
 else if(_request == "ping")
 {
  ping();
 }
 
 else if(_request == "close_terminal")
 {
  close_terminal();
 }
 
 else if(_request == "check_last_update")
 {
  SplitArgs(_args, key, value);
  string program_name = key[0];
  check_last_update(program_name);
 }
 
 else if(_request == "chart_set_symbol_period")
 {
  SplitArgs(_args, key, value);
  
  if(key.Total() == 3)
  {
   long chart_id = (long)key[0];
   string symbol = key[1];
   int period = (int)_StringToTimeframe(key[2]);

   chart_set_symbol_period(chart_id, symbol, period);
  }
  else
   Alert("Error splitting args=" + _args);
 }
 
 else if(_request == "create_chart")
 {
  SplitArgs(_args, key, value);
  
  if(key.Total() == 2)
  { 
   string symbol = key[0];
   ENUM_TIMEFRAMES period = (ENUM_TIMEFRAMES)key[1];
   
   create_chart(symbol, period);
  }
  else if(key.Total() == 3)
  {
   string symbol = key[0];
   ENUM_TIMEFRAMES period = _StringToTimeframe(key[1]);
   string chart_template = key[2];
   
   create_chart(symbol, period, chart_template);
  }
  else
    Alert("Error splitting args=" + _args);
 }
  
  
 else if(_request == "fetch_new_job")
  fetch_new_job();
  
 else
  logger.log("Request=" + _request + " not found", LOG_TYPE_HIGH, __LINE__, __FUNCTION__); 
}
//+------------------------------------------------------------------+
string Context::post_log_to_server(string level,datetime timestamp,string message,string symbol,string timeframe,string traceback)
{
 string error_msg = NULL;
 POSTDATA data;
 data.set("terminal_pk", (string)webserver.terminal_pk());
 data.set("level", level);
 data.set("timestamp", WebServer::toSQLTime(timestamp));
 data.set("message", message);
 data.set("traceback", traceback);
 data.set("timeframe", timeframe);
 data.set("symbol", symbol);
 
 int response = webserver.WebRequestPOST("/logs/new", data);
 
 if(response==OK_200)
  p_messenger.send_response_success();
 else
  p_messenger.send_response_failed("web_response error code=" + (string)response);
 
 return error_msg;
}
//+------------------------------------------------------------------+
string Context::chart_set_symbol_period(long chart_id, string symbol, int period)
{
 string error_msg = NULL;

 if(period >= 0)
 {
  bool result = ChartSetSymbolPeriod(chart_id, symbol, period);

  if(result)
   p_messenger.send_response_success();
  else
   error_msg = "error=" + (string)GetLastError();
 }
 else
  error_msg = "Invalid period=" + (string)period;
 
 p_messenger.send_response_failed(error_msg);
 return error_msg;
}
//+------------------------------------------------------------------+
string Context::check_last_update(string program_name)
{
 string search_string = StringConcatenate("last_update_", program_name);
 double value;
 
 bool result = GlobalVariableGet(search_string, value);
 
 if(result)
 {
  p_messenger.send_response_success((string)(datetime)value);
 }
 else
 {
  p_messenger.send_response_failed("Could not find " + search_string + " in global variables");
 }
 
 return "";
}
//+------------------------------------------------------------------+
string Context::create_chart(string symbol, ENUM_TIMEFRAMES period, string chart_template=NULL)
{
 string success_msg = NULL;
 string error_msg = "";
 
 long chart_id = _create_chart(symbol, period, error_msg, chart_template);

 if(chart_id > 0)
  success_msg += (string)chart_id + "; ";

 if(error_msg == "")
  p_messenger.send_response_success(success_msg);
 else
  p_messenger.send_response_failed("success_msg=" + success_msg + ", error_msg=" + error_msg);

 
 return "success_msg=" + success_msg + ", error_msg=" + error_msg;
}
//+------------------------------------------------------------------+
string Context::save_template(long chart_id, string file_name)
{
 bool result = ChartSaveTemplate(chart_id, file_name);

 if(result)
  p_messenger.send_response_success();
 else {
  int e=GetLastError();
  p_messenger.send_response_failed("error: " + (string)e);
 }
 
 return (string)result;
}
//+------------------------------------------------------------------+
string Context::apply_template(long chart_id, string file_name)
{
 bool result = ChartApplyTemplate(chart_id, file_name);

 if(result)
  p_messenger.send_response_success();
 else {
  int e=GetLastError();
  p_messenger.send_response_failed("error: " + (string)e);
 }
 
 return (string)result;
}
//+------------------------------------------------------------------+
string Context::apply_ea(string name)
{
 string ret_msg = "write dll";
 
 p_messenger.send_response_success(ret_msg);
 return ret_msg; 
}
//+------------------------------------------------------------------+
string Context::get_exposure()
{
 CArrayString symbols;
 CArrayDouble exposure;
 int total;

 total = OrdersTotal();
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(i,SELECT_BY_POS))
  {
   int pos = symbols.SearchLinear(OrderSymbol());
     
   if(pos < 0)
   {
    symbols.Add(OrderSymbol());
    exposure.Add(GetExposure(OrderType(), OrderLots()));
   }
   else
   {
    exposure.Update(pos, exposure[pos] + GetExposure(OrderType(), OrderLots()));
   }
  }
 }
 
 string ret_msg;
 total = symbols.Total();
 for(int i=0; i<total; i++)
 {
  ret_msg += symbols[i] + ": " + (string)exposure[i] + ",";
 }
 
 p_messenger.send_response_success(ret_msg);
 return ret_msg;
}
//+------------------------------------------------------------------+
string Context::close_terminal()
{
 bool result = TerminalClose(0);
 
 if(result)
  p_messenger.send_response_success();
 else
  p_messenger.send_response_failed();
 
 return NULL;
}
//+------------------------------------------------------------------+
string Context::ping()
{
 logger.log("ping received ... ", LOG_TYPE_INFO, __LINE__, __FUNCTION__);
 p_messenger.send_response_success();
 return NULL;
}
//+------------------------------------------------------------------+
string Context::get_equity()
{
 string equity = DoubleToStr(AccountEquity(), 2);
 logger.log("returning equity = $" + equity, LOG_TYPE_INFO, __LINE__, __FUNCTION__);
 p_messenger.send_response_success(equity);
 return equity;
}
//+------------------------------------------------------------------+
string Context::fetch_new_job()
{
 int timeout_secs = 5;
 GlobalVariableSet("fetch_new_job", -1);
 datetime start_time = TimeLocal();
 while(TimeLocal() - start_time < timeout_secs)
 {
  int j_id = (int)GlobalVariableGet("fetch_new_job");
  if(j_id > 0) {
   p_messenger.send_response_success("fetched job(s) starting with id: " + (string)j_id);
   return("success, id=" + (string)j_id);
  }
  
  if(j_id == 0) {
   p_messenger.send_response_success("no new jobs found");
   return("success, no new jobs found");
  }
 }
 
 p_messenger.send_response_failed("could not confirm within " + (string)timeout_secs + " seconds");
 return("failed");
}
//+------------------------------------------------------------------+
string Context::close_trades(const CArrayString& symbol)
{
 int sz = OrdersTotal();
 string ret = NULL;
 bool success = True;
 bool close_all = (symbol.SearchLinear("all") >= 0) ? True : False;
 
 for(int i=sz-1; i>=0; i--)
 {
  if(OrderSelect(i,SELECT_BY_POS) && (symbol.SearchLinear(OrderSymbol()) >= 0 || close_all))
  {
   double prc;
   int type = OrderType();
   if(type==OP_BUY)
    prc = MarketInfo(OrderSymbol(), MODE_BID);
   else if(type==OP_SELL)
    prc = MarketInfo(OrderSymbol(), MODE_ASK);
   else
    continue;
    
   bool ans = OrderClose(OrderTicket(), OrderLots(), prc, 5);
  
   if(ans) {
    logger.log("Closed ticket #" + (string)OrderTicket(), LOG_TYPE_INFO, __LINE__, __FUNCTION__);
    ret += "Closed #" + (string)OrderTicket() + "; ";
   }
   else {
    success = false;
    logger.log("Failed to close: #" + (string)OrderTicket(), LOG_TYPE_HIGH, __LINE__, __FUNCTION__); 
    ret += "Failed to close: #" + (string)OrderTicket();
   } 
  }
 }
  
 if(success)
  p_messenger.send_response_success(ret);
 else
  p_messenger.send_response_failed(ret);
 
 return ret;
}
//+------------------------------------------------------------------+
string Context::close_trades(const CArrayInt& ticket)
{
 int sz = ticket.Total();
 string ret = NULL;
 bool success = True;

 for(int i=0; i<sz; i++)
 {
  if(OrderSelect(ticket[i], SELECT_BY_TICKET))
  {
   double prc;
   int type = OrderType();
   if(type==OP_BUY)
    prc = MarketInfo(OrderSymbol(), MODE_BID);
   else if(type==OP_SELL)
    prc = MarketInfo(OrderSymbol(), MODE_ASK);
   else
    continue;
    
   bool ans = OrderClose(OrderTicket(), OrderLots(), prc, 5);
  
   if(ans) {
    logger.log("Closed ticket #" + (string)OrderTicket(), LOG_TYPE_INFO, __LINE__, __FUNCTION__);
    ret += "Closed #" + (string)OrderTicket() + "; ";
   }
   else {
    success = false;
    logger.log("Failed to close: #" + (string)OrderTicket(), LOG_TYPE_HIGH, __LINE__, __FUNCTION__); 
    ret += "Failed to close: #" + (string)OrderTicket();
   } 
  }
 }
  
 if(success)
  p_messenger.send_response_success(ret);
 else
  p_messenger.send_response_failed(ret);
 
 return ret;
}
//+------------------------------------------------------------------+
string Context::enable_trades(const CArrayString &symbol)
{
 AutoSaveArrayStringCommunity disable_trading_symbol("disable_trading_symbol");
 int sz = symbol.Total();
 string ret_msg = NULL;
 bool error_found = false;
 
 for(int i=0; i<sz; i++) {
  if(SymbolSelect(symbol[i], true))
  {
   int pos = disable_trading_symbol.SearchLinear(symbol[i]);
	if(pos >= 0) {
	 disable_trading_symbol.Delete(pos);
	 ret_msg += "Enabling trading for " + symbol[i];
	}
	else
	{
    ret_msg += "Error enabling " + symbol[i] + " for trading";
    error_found = true;
   }
  }
  else {
   ret_msg += "Could not enable trading for unknown symbol=" + symbol[i];
   error_found = true;
  }
 }	
 
 if(!error_found)
  p_messenger.send_response_success(ret_msg);
 else
  p_messenger.send_response_failed(ret_msg);
  
 return ret_msg;
}
//+------------------------------------------------------------------+
string Context::enable_trades(void)
{
 AutoSaveArrayStringCommunity disable_trading_symbol("disable_trading_symbol");
 string ret_msg = NULL;
 
 int total = disable_trading_symbol.Total();
 int pos = disable_trading_symbol.SearchLinear("Disable all symbols");
 bool error_found = false;
 
 if(pos >= 0) {
  disable_trading_symbol.Delete(pos);
  ret_msg = "Re-enabling trading by web request";
  logger.log(ret_msg, LOG_TYPE_INFO, __LINE__, __FUNCTION__);
  p_messenger.send_response_success(ret_msg);
 }
 else if(total > 0)
 {
  for(int i=total-1; i>=0; i--)
  {
   string symbol = disable_trading_symbol[i];
   if(disable_trading_symbol.Delete(i)) {
    ret_msg += "Re-enabling " + symbol + "; ";
   }
   else {
    ret_msg += "Error, e=" + (string)GetLastError() + " re-enabling " + symbol;
    error_found = true;
   }
  }
  
  if(!error_found)
   p_messenger.send_response_success(ret_msg);
  else
   p_messenger.send_response_failed(ret_msg);
 }
 else
 {
  ret_msg = "Trading already enabled";
  logger.log(ret_msg, LOG_TYPE_INFO, __LINE__, __FUNCTION__);
  p_messenger.send_response_failed(ret_msg); 
 }
 
 return ret_msg;
}
//+------------------------------------------------------------------+
string Context::disable_trades(const CArrayString &symbol)
{
 AutoSaveArrayStringCommunity disable_trading_symbol("disable_trading_symbol");
 int sz = symbol.Total();
 string ret_msg = NULL;
 bool error_found = false;
 
 for(int i=0; i<sz; i++) {
  if(SymbolSelect(symbol[i], true))
  {
   if(disable_trading_symbol.Add(symbol[i]))
    ret_msg += "Disabled trading for " + symbol[i];
   else {
    ret_msg += "Error disabling " + symbol[i] + " for trading";
    error_found = true;
   }
  }
  else {
   ret_msg += "Could not disable trading for unknown symbol=" + symbol[i];
   error_found = true;
  }
 }	
 
 if(!error_found)
  p_messenger.send_response_success(ret_msg);
 else
  p_messenger.send_response_failed(ret_msg);
  
 return ret_msg;
}
//+------------------------------------------------------------------+
string Context::disable_trades(void)
{
 AutoSaveArrayStringCommunity disable_trading_symbol("disable_trading_symbol");
 string ret_msg = NULL;
 
 if(disable_trading_symbol.Add("Disable all symbols")) {
   
   ret_msg = "Disabled trading by web request";
 
   logger.log(ret_msg, LOG_TYPE_INFO, __LINE__, __FUNCTION__);
 
   p_messenger.send_response_success(ret_msg);
 }
 else {
   
   ret_msg = "Unable to add symbol";
   
   logger.log(ret_msg, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
 
   p_messenger.send_response_failed(ret_msg);
 }
 
 return ret_msg;
}
//+------------------------------------------------------------------+
string Context::open_trades(const CArrayString& symbol, const CArrayString& _args)
{
 int sz = symbol.Total();
 string ret = NULL;
 bool success = True;
 int open_count = 0;
 
 for(int i=0; i<sz; i++)
 {
  double vol=0, tp=0, sl=0;
  int magic=0;
  string comment=NULL;
   
  if(MathAbs((double)_args[i]) <= DBL_EPSILON)
  {
   CArrayString key;
   CArrayString value;
   
   SplitArgs(_args[i], key, value, '_', '?');

   int digits = (int)MarketInfo(symbol[i], MODE_DIGITS);
   
   for(int j=0; j<key.Total(); j++)
   {
    if(key[j]=="lots")
     vol = NormalizeDouble((double)value[j], 2);
    if(key[j]=="sl")
     sl = NormalizeDouble((double)value[j], digits);
    if(key[j]=="tp")
     tp = NormalizeDouble((double)value[j], digits);
    if(key[j]=="magic")
     magic = (int)value[j];
    if(key[j]=="comment")
     comment = value[j];
   }
  }
  else {
    vol = (double)_args[i];
  }
  
  double lots = MathAbs(vol);
  int type = (vol > 0) ? OP_BUY : OP_SELL;
  double prc = (type==OP_BUY) ? MarketInfo(symbol[i], MODE_ASK) : MarketInfo(symbol[i], MODE_BID);
  int ticket = OrderSend(symbol[i], type, lots, prc, 5, sl, tp, comment, magic);
  string msg;
  
  if(ticket > 0) {
   msg = "Opened ticket #" + (string)ticket;
   open_count++;
  
   if(comment != NULL)
    msg += ", comment=" + comment;
    
   if(magic != 0)
    msg += ", magic_number=" + (string)magic;
    
   if(sl != 0)
    msg += ", sl=" + (string)sl;
    
   if(tp != 0)
    msg += ", tp=" + (string)tp;
   
   logger.log(msg, LOG_TYPE_INFO, __LINE__, __FUNCTION__);
  }
  else {
   msg = "Failed to open: symbol=" + symbol[i] + ", lots=" + (string)vol + ", sl=" + (string)sl + ", tp=" + (string)tp + ", comment=" + comment + ", e=" + (string)GetLastError();
   success = false;
   logger.log(msg, LOG_TYPE_HIGH, __LINE__, __FUNCTION__); 
  }
  
  ret += msg + "; ";
 }
  
 if(success && open_count > 0)
  p_messenger.send_response_success(ret);
 else
  p_messenger.send_response_failed(ret);
 
 return ret;
}
//+------------------------------------------------------------------+
Context::Context(FileMessenger *_messenger) : webserver(HOST_SITE), logger(__FILE__)
{
 p_messenger = _messenger;
}
//+------------------------------------------------------------------+
double GetExposure(int type, double lots)
{
 if(type==OP_BUY || type==OP_BUYLIMIT || type==OP_BUYSTOP)
 {
  return lots;
 }
 
 if(type==OP_SELL || type==OP_SELLLIMIT || type==OP_SELLSTOP)
 {
  return lots * -1;
 }
 
 return 0;
}
//+------------------------------------------------------------------+
ENUM_TIMEFRAMES Context::_StringToTimeframe(string period)
{
 if(period==EnumToString(PERIOD_CURRENT))       return PERIOD_CURRENT;
 else if(period==EnumToString(PERIOD_M1))       return PERIOD_M1;
 else if(period==EnumToString(PERIOD_M5))       return PERIOD_M5;
 else if(period==EnumToString(PERIOD_M15))      return PERIOD_M15;
 else if(period==EnumToString(PERIOD_M30))      return PERIOD_M30;
 else if(period==EnumToString(PERIOD_H1))       return PERIOD_H1;
 else if(period==EnumToString(PERIOD_H4))       return PERIOD_H4;
 else if(period==EnumToString(PERIOD_D1))       return PERIOD_D1;
 else if(period==EnumToString(PERIOD_W1))       return PERIOD_W1;
 else if(period==EnumToString(PERIOD_MN1))      return PERIOD_MN1;
 
 else
  logger.log("could not match period = " + period, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
  
 return -1;
}
//+------------------------------------------------------------------+
long Context::_create_chart(string symbol, int timeframe, string& err, string chart_template=NULL)
{
 err = "";
 
 long chart_id = ChartOpen(symbol, timeframe);
 bool template_result = true, timeframe_result = true;
  
 if(chart_template != NULL)
  template_result = ChartApplyTemplate(chart_id, chart_template);
  
 if(!template_result)
  err += "Error applying template " + chart_template + ", symbol=" + symbol + ", chart_id=" + (string)chart_id + "; ";
  
 timeframe_result = ChartSetSymbolPeriod(chart_id, symbol, (ENUM_TIMEFRAMES)timeframe);

 if(!timeframe_result)
  err += "Error setting timeframe " + (string)(ENUM_TIMEFRAMES)timeframe + ", symbol=" + symbol + ", chart_id=" + (string)chart_id + "; ";
  
 if(chart_id <= 0)
  err += "Error opening chart, symbol=" + symbol + ", timeframe=" + (string)timeframe + "; ";
  
 return(chart_id);
}
//+------------------------------------------------------------------+
void Context::ChartsCloseAll()
{
 long chart_id = ChartFirst();
 
 do {
   ChartClose(chart_id);
   chart_id = ChartNext(chart_id);
  } 
  while(chart_id >= 0);
}