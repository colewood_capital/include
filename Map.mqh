//+------------------------------------------------------------------+
//|                                                          Map.mqh |
//|                                                         Carm Inc |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Carm Inc"
#property link      "https://www.mql5.com"
#property strict

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
#endif

template <typename T1, typename T2>
class Map
{
 private:
  T1 p_keys[];
  T2 p_values[];
  int p_size;
  
  int search(T1 key);
  void shift_map(int start_index);
  T2 get(T1 key);
  
  Logger logger;
  
 public:
  void set(T1 key, T2 value);
  void del(T1 key);
  bool find(T1 key);
  int copy(T1& key[], T2& value[]) const { __TRACE__ ArrayCopy(key, p_keys); ArrayCopy(value, p_values); return p_size; }
  void clear() { __TRACE__ p_size=0; ArrayFree(p_keys); ArrayFree(p_values); }
  void set_warnings(bool enable);
  
  T2 operator[](T1 key) { return get(key); }
  
  Map() : p_size(0), logger(__FILE__, "helper_processes") {};
};

template <typename T1, typename T2>
void Map::set_warnings(bool enable=true)
{
 __TRACE__
 
 if(enable)
  logger.enable(LOG_TYPE_DEBUG);
 else
  logger.disable(LOG_TYPE_DEBUG);
}

template <typename T1, typename T2>
void Map::shift_map(int start_index)
{
 __TRACE__
 
 for(int i=start_index+1; i<p_size; i++)
 {
  p_keys[i-1] = p_keys[i];
  p_values[i-1] = p_values[i];
 }
 
 p_size--;
 ArrayResize(p_keys, p_size);
 ArrayResize(p_values, p_size);
}

template <typename T1>
void Map::del(T1 key)
{
 __TRACE__
 
 int i = search(key);
 
 if(i >= 0)
  shift_map(i);
 else
  logger.log("Could not find key=" + (string)key, LOG_TYPE_DEBUG,  __LINE__, __FUNCTION__);
}

template <typename T1>
int Map::search(T1 key)
{
 __TRACE__
 
 for(int i=0; i<p_size; i++)
 {
  if(p_keys[i] == key)
   return i;
 }
 
 return -1;
}

template <typename T1>
bool Map::find(T1 key)
{
 __TRACE__
 
 return search(key) >= 0;
}

template <typename T1, typename T2>
T2 Map::get(T1 key)
{
 __TRACE__
 
 int i = search(key);
 
 LOG_DEBUG("get: " + (string)key);
 
 if(i >= 0)
  return p_values[i];
 else {
  logger.log("returning NULL value=" +(string)(T2)NULL + " to unitialized key=" + (string)key, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
  return NULL;
 }
}

template <typename T1, typename T2>
void Map::set(T1 key, T2 value)
{
 __TRACE__
 
 int i = search(key);
 
 if(i >= 0)
 {
  p_values[i] = value;
 }
 else
 {
  p_size++;
  ArrayResize(p_keys, p_size);
  ArrayResize(p_values, p_size);
 
  p_keys[p_size-1] = key;
  p_values[p_size-1] = value;
 }
}
