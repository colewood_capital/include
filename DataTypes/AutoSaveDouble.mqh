//+------------------------------------------------------------------+
//|                                               AutoSaveDouble.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#include <DataTypes\AutoSave.mqh>
#include <DataTypes\Double.mqh>


class AutoSaveDouble : public Double
{
  private:
   string p_filename;
   
  public:
   void set(double value);
   
   void operator=(double value) { set(value); }
   
   AutoSaveDouble(string filename, double value=0.0);
   AutoSaveDouble(string program_name, string filename, double value=0.0);
   AutoSaveDouble(string program_name, string filename, float max_double_age_in_minutes, double value=0.0);
};
//------------------------------------------------
void AutoSaveDouble::set(double value)
{
  int handle = FileOpen(p_filename, FILE_WRITE|FILE_BIN);
  
  if(handle >= 0)
   {
     Double::set(value);
     FileWriteInteger(handle, (int)TimeCurrent());
     FileWriteDouble(handle, value);
     FileClose(handle);
   }
  else
   {
     Alert("Error: could not update '",p_filename," for e: ",GetLastError()," in ",__FUNCTION__);
     ResetLastError();
   }
}
//------------------------------------------------
AutoSaveDouble::AutoSaveDouble(string program_name, string filename, double value=0.0)
{
  string temp[];
  StringSplit(program_name,'.',temp);
  program_name = temp[0];
  
  p_filename = VARIABLEFILEPATH + program_name + "_" + Symbol() + "_" + (string)Period() + "_" + filename;
 
  if(FileIsExist(p_filename))
   {
     int handle = FileOpen(p_filename, FILE_READ|FILE_BIN);
     
     if(handle >= 0)
      {
        int timestamp = FileReadInteger(handle);
        
        my_double = FileReadDouble(handle);
    
        FileClose(handle);
      }
     else
      {
        Alert("Error: could not update '",p_filename," for e: ",GetLastError()," in ",__FUNCTION__);
        ResetLastError();
      }
   }
  else 
   {
     set(value);
   }
}
