//+------------------------------------------------------------------+
//|                                                     AutoSave.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#ifndef VARIABLEFILEPATH
   #define VARIABLEFILEPATH               "DataTypes\\AutoSave\\"
#endif

#ifndef ERRORS
#define ERRORS
   #include <Packages\Errors.mqh>
#endif

string GetAutoSaveFilename(string name, string __FILE__macro)
{
 string path = VARIABLEFILEPATH + _ProgramName(__FILE__macro) + "_" + name;
 
 return path;
}


string GetAutoSaveFilename(string name, string __FILE__macro, string symbol)
{
 string path = VARIABLEFILEPATH + _ProgramName(__FILE__macro)+ "_" + symbol + "_" + name;
 
 return path;
}


string GetAutoSaveFilename(string name, string __FILE__macro, int period)
{
 string path = VARIABLEFILEPATH + _ProgramName(__FILE__macro) + "_" + (string)period + "_" + name;
 
 return path;
}


string GetAutoSaveFilename(string name, string __FILE__macro, string symbol, int period)
{
 string path = VARIABLEFILEPATH + _ProgramName(__FILE__macro) + "_" + symbol + "_" + (string)period + "_" + name;
 
 return path;
}


string _ProgramName(string __FILE__macro)
{
  string temp[];
  StringSplit(__FILE__macro,'.',temp);
  string program_name = temp[0];
  
  StringReplace(program_name, " ", "_");
  
  return program_name;
}


bool _CheckFileIsExist(const string file_name, int common_flag=0)
{
 bool res = FileIsExist(file_name, common_flag);
 
 if(!res)
  SetUserError(ERR_FILE_NOT_CREATED);
 
 return res;
}


/*
AutoSaveDouble::AutoSaveDouble(string program_name, string filename, float max_double_age_in_minutes, double value=0.0)
{
  string temp[];
  StringSplit(program_name,'.',temp);
  program_name = temp[0];
  
  //--- NAME SHOULD HAVE A PARAMETER THAT HINTS IT IS EXPECTING AN EXPIRATION: allows expiring and non-expiring files to coexist
  p_filename = VARIABLEFILEPATH + program_name + "_" + Symbol() + "_" + (string)Period() + "_" + filename;
  
  if(FileIsExist(p_filename))
   {
     int handle = FileOpen(p_filename, FILE_READ|FILE_BIN);
     
     if(handle >= 0)
      {
          int last_save_timestamp = FileReadInteger(handle);

          if(((int)TimeCurrent() - last_save_timestamp) / 60.0 < max_double_age_in_minutes)
           {
             my_double = FileReadDouble(handle);
       
             FileClose(handle);
             
             return;
           } //--- if false, fall through to set(value)
      }
     else
      {
        Alert("Error: could not access '",p_filename," for read, e: ",GetLastError()," in ",__FUNCSIG__);
        ResetLastError();
        return;
      } 
      
     FileClose(handle);
   }
  
  set(value);
 
}
*/