//+------------------------------------------------------------------+
//|                                             AutoSaveDatetime.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#include <DataTypes\AutoSave.mqh>
#include <DataTypes\Datetime.mqh>

class AutoSaveDatetime : public Datetime
{
  private:
   string p_filename;
   
  public:
   void set(datetime value);
   
   void operator=(datetime value) { set(value); }
   
   AutoSaveDatetime(string program_name, string filename, datetime value=0);
   AutoSaveDatetime(string program_name, string filename, float max_int_age_in_minutes, datetime value=0);
};
//------------------------------------------------
void AutoSaveDatetime::set(datetime value)
{
  int handle = FileOpen(p_filename, FILE_WRITE|FILE_BIN);
  
  if(handle >= 0)
   {
     Datetime::set(value);
     FileWriteInteger(handle, (int)TimeCurrent());
     FileWriteInteger(handle, (int)value);
     FileClose(handle);
   }
  else
   {
     Alert("Error: could not update '",p_filename," for e: ",GetLastError()," in ",__FUNCTION__);
     ResetLastError();
   }
}
//------------------------------------------------
AutoSaveDatetime::AutoSaveDatetime(string program_name, string filename, datetime value=0)
{ 
  string temp[];
  StringSplit(program_name,'.',temp);
  program_name = temp[0];
 
  p_filename = VARIABLEFILEPATH + program_name + "_" + Symbol() + "_" + (string)Period() + "_" + filename;
  
  if(FileIsExist(p_filename))
   {
     int handle = FileOpen(p_filename, FILE_READ|FILE_BIN);
     
     if(handle >= 0)
      {
        int timestamp = FileReadInteger(handle);
        
        my_datetime = FileReadInteger(handle);
    
        FileClose(handle);
      }
     else
      {
        Alert("Error: could not update '",p_filename," for e: ",GetLastError()," in ",__FUNCTION__);
        ResetLastError();
      }
   }
  else 
   {
     set(value);
   }
}
//------------------------------------------------
AutoSaveDatetime::AutoSaveDatetime(string program_name, string filename, float max_int_age_in_minutes, datetime value=0)
{
  string temp[];
  StringSplit(program_name,'.',temp);
  program_name = temp[0];
  
  p_filename = VARIABLEFILEPATH + program_name + "_" + Symbol() + "_" + (string)Period() + "_" + filename;
  
  if(FileIsExist(p_filename))
   {
     int handle = FileOpen(p_filename, FILE_READ|FILE_BIN);
     
     if(handle >= 0)
      {
          int last_save_timestamp = FileReadInteger(handle);

          if(((int)TimeCurrent() - last_save_timestamp) / 60.0 < max_int_age_in_minutes)
           {
             my_datetime = FileReadInteger(handle);
       
             FileClose(handle);
             
             return;
           } //--- if false, fall through to set(value)
      }
     else
      {
        Alert("Error: could not access '",p_filename," for read, e: ",GetLastError()," in ",__FUNCSIG__);
        ResetLastError();
        return;
      } 
      
     FileClose(handle);
   }
  
  set(value);
 
}