//+------------------------------------------------------------------+
//|                                                      Integer.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

class Integer
{
 protected:
   int my_int;
   
 public:
   void set(int value)   { my_int = value;  }
   int get()             { return(my_int);  }
   
   int operator-() const { return(-my_int); }
   
   void AutoIncrement()  { my_int++;        }
  
   
   Integer(int value=0)  { my_int = value;  } 
};