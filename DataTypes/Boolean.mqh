//+------------------------------------------------------------------+
//|                                                      Boolean.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

class Boolean
{
 protected:
   bool my_bool;
   
 public:
   void set(bool value)      { my_bool = value; }
   bool get()                { return(my_bool); }
   void toggle()             { my_bool = (my_bool) ? false : true; }
   
   Boolean(bool value=false) { my_bool = value; }
};