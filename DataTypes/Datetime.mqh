//+------------------------------------------------------------------+
//|                                                     Datetime.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

class Datetime
{
 protected:
   datetime my_datetime;
   
 public:
   void set(datetime value)    { my_datetime = value; }
   datetime get()              { return(my_datetime); }
   
   datetime operator-() const { return(-my_datetime); }
   
   Datetime(datetime value=0)  { my_datetime = value; } 
};