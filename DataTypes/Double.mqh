//+------------------------------------------------------------------+
//|                                                       Double.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

class Double
{
 protected:
   double my_double;
   
 public:
   void set(double value)      { my_double = value; }
   double get()                { return(my_double); }
   
   double operator-() const    { return(-my_double);}
   
   Double(double value=0.0)    { my_double = value; }
};