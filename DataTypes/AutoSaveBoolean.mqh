//+------------------------------------------------------------------+
//|                                              AutoSaveBoolean.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#include <DataTypes\AutoSave.mqh>
#include <DataTypes\Boolean.mqh>


class AutoSaveBoolean : public Boolean
{
  private:
   string p_filename;
   
   void Init(bool value);
   
  public:
   void set(bool value);
   void toggle(void);
   
   AutoSaveBoolean(string name, string __FILE__macro, bool value=false);
   AutoSaveBoolean(string name, string __FILE__macro, string symbol, bool value=false);
   AutoSaveBoolean(string name, string __FILE__macro, int period, bool value=false);
   AutoSaveBoolean(string name, string __FILE__macro, string symbol, int period, bool value=false);
};
//------------------------------------------------
void AutoSaveBoolean::toggle()
{
  int handle = FileOpen(p_filename, FILE_WRITE|FILE_BIN);
  
  if(handle >= 0)
   {
     Boolean::toggle();
     FileWriteInteger(handle, (int)TimeCurrent());
     FileWriteInteger(handle, my_bool);
     FileClose(handle);
   }
  else
   {
     Alert("Error: could not update '",p_filename," for e: ",GetLastError()," in ",__FUNCTION__);
     ResetLastError();
   }
}
//------------------------------------------------
void AutoSaveBoolean::set(bool value)
{  
  int handle = FileOpen(p_filename, FILE_WRITE|FILE_BIN);
  
  if(handle >= 0)
   {
     Boolean::set(value);
     FileWriteInteger(handle, (int)TimeCurrent());
     FileWriteInteger(handle, my_bool);
     FileClose(handle);
   }
  else
   {
     Alert("Error: could not update '",p_filename," for e: ",GetLastError()," in ",__FUNCTION__);
     ResetLastError();
   }
}
//------------------------------------------------
AutoSaveBoolean::AutoSaveBoolean(string name, string __FILE__macro, bool value=false)
{ 
 p_filename = GetAutoSaveFilename(name, __FILE__macro);

 Init(value);
}
//------------------------------------------------
AutoSaveBoolean::AutoSaveBoolean(string name, string __FILE__macro, string symbol, bool value=false)
{ 
 p_filename = GetAutoSaveFilename(name, __FILE__macro, symbol);

 Init(value);
}
//------------------------------------------------
AutoSaveBoolean::AutoSaveBoolean(string name, string __FILE__macro, int period, bool value=false)
{ 
 p_filename = GetAutoSaveFilename(name, __FILE__macro, period);

 Init(value);
}
//------------------------------------------------
AutoSaveBoolean::AutoSaveBoolean(string name, string __FILE__macro, string symbol, int period, bool value=false)
{ 
 p_filename = GetAutoSaveFilename(name, __FILE__macro, symbol, period);

 Init(value);
}
//------------------------------------------------
void AutoSaveBoolean::Init(bool value)
{
 if(FileIsExist(p_filename))
   { 
     int handle = FileOpen(p_filename, FILE_READ|FILE_BIN);
     
     if(handle >= 0)
      {
        int timestamp = FileReadInteger(handle);
    
        my_bool = (bool)FileReadInteger(handle);
    
        FileClose(handle);
      }
     else
      {
        Alert("Error: could not update '",p_filename," for e: ",GetLastError()," in ",__FUNCTION__);
      }
   }
  else 
   {
     set(value);
   }
}