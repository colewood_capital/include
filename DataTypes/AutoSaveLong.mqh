//+------------------------------------------------------------------+
//|                                                 AutoSaveLong.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#include <DataTypes\AutoSave.mqh>
#include <DataTypes\Long.mqh>

class AutoSaveLong : public Long
{
  private:
   string p_filename;
   void Init(string program_name, bool namespace_symbol, bool namespace_period, string filename);
   
  public:
   void set(long value);
   void AutoIncrement();
   
   void operator=(long value) { set(value); }
   void operator++()         { AutoIncrement(); }
   
   AutoSaveLong(string filename, long value=0, bool namespace_symbol=true, bool namespace_period=true);
   AutoSaveLong(string program_name, string filename, long value=0, bool namespace_symbol=true, bool namespace_period=true);
   AutoSaveLong(string program_name, string filename, float max_long_age_in_minutes, long value=0, bool namespace_symbol=true, bool namespace_period=true);
};
//------------------------------------------------
void AutoSaveLong::AutoIncrement()
{
  int handle = FileOpen(p_filename, FILE_WRITE|FILE_BIN);
  
  if(!FileSeek(handle, 0, SEEK_SET))
      Alert("[ERROR] setting file '",p_filename," for read, e: ",GetLastError()," in ",__FUNCSIG__);
  
  if(handle >= 0)
   {
     Long::AutoIncrement();
     FileWriteInteger(handle, (int)TimeLocal());
     FileWriteLong(handle, my_long);
     FileClose(handle);
   }
  else
   {
     Alert("[ERROR] could not update '",p_filename," for e: ",GetLastError()," in ",__FUNCTION__);
     ResetLastError();
   }
}
//------------------------------------------------
void AutoSaveLong::set(long value)
{
  int handle = FileOpen(p_filename, FILE_WRITE|FILE_BIN);
  
  if(!FileSeek(handle, 0, SEEK_SET))
      Alert("[ERROR] setting file '",p_filename," for read, e: ",GetLastError()," in ",__FUNCSIG__);
  
  if(handle >= 0)
   {
     Long::set(value);
     FileWriteInteger(handle, (int)TimeLocal());
     FileWriteLong(handle, value);
     FileClose(handle);
   }
  else
   {
     Alert("[ERROR] could not update '",p_filename," for e: ",GetLastError()," in ",__FUNCTION__);
     ResetLastError();
   }
}
//------------------------------------------------
AutoSaveLong::AutoSaveLong(string filename, long value=0, bool namespace_symbol=true, bool namespace_period=true)
{
  Init(NULL, namespace_symbol, namespace_period, filename);
  
  if(FileIsExist(p_filename))
   {
     int handle = FileOpen(p_filename, FILE_READ|FILE_BIN);
     
     if(!FileSeek(handle, 0, SEEK_SET))
      Alert("[ERROR] setting file '",p_filename," for read, e: ",GetLastError()," in ",__FUNCSIG__);
     
     if(handle >= 0)
      {
        int timestamp = FileReadInteger(handle);
        
        my_long = FileReadLong(handle);
    
        FileClose(handle);
      }
     else
      {
        Alert("[ERROR] could not update '",p_filename," for e: ",GetLastError()," in ",__FUNCTION__);
        ResetLastError();
      }
   }
  else 
   {
     set(value);
   }
}
//------------------------------------------------
AutoSaveLong::AutoSaveLong(string program_name, string filename, long value=0, bool namespace_symbol=true, bool namespace_period=true)
{
  Init(program_name, namespace_symbol, namespace_period, filename);
  
  if(FileIsExist(p_filename))
   {
     int handle = FileOpen(p_filename, FILE_READ|FILE_BIN);
     
     if(!FileSeek(handle, 0, SEEK_SET))
      Alert("[ERROR] setting file '",p_filename," for read, e: ",GetLastError()," in ",__FUNCSIG__);
     
     if(handle >= 0)
      {
        int timestamp = FileReadInteger(handle);
        
        my_long = FileReadLong(handle);
    
        FileClose(handle);
      }
     else
      {
        Alert("[ERROR] could not update '",p_filename," for e: ",GetLastError()," in ",__FUNCTION__);
        ResetLastError();
      }
   }
  else 
   {
     set(value);
   }
}
//------------------------------------------------
AutoSaveLong::AutoSaveLong(string program_name, string filename, float max_long_age_in_minutes, long value=0, bool namespace_symbol=true, bool namespace_period=true)
{
  Init(program_name, namespace_symbol, namespace_period, filename);
  
  if(FileIsExist(p_filename))
   {
     int handle = FileOpen(p_filename, FILE_READ|FILE_BIN);
     
     if(!FileSeek(handle, 0, SEEK_SET))
      Alert("[ERROR] setting file '",p_filename," for read, e: ",GetLastError()," in ",__FUNCSIG__);
      
     if(handle >= 0)
      {
          int last_save_timestamp = FileReadInteger(handle);

          if(((int)TimeLocal() - last_save_timestamp) / 60.0 < max_long_age_in_minutes)
           {
             my_long = FileReadLong(handle);
       
             FileClose(handle);
             
             return;
           } //--- if false, fall through to set(value)
      }
     else
      {
        Alert("[ERROR] could not access '",p_filename," for read, e: ",GetLastError()," in ",__FUNCSIG__);
        ResetLastError();
        return;
      } 
      
     FileClose(handle);
   }
  
  set(value);
}
//------------------------------------------------
void AutoSaveLong::Init(string program_name, bool namespace_symbol, bool namespace_period, string filename)
{
 if(StringLen(program_name) > 0)
 {
  string temp[];
  StringSplit(program_name,'.',temp);
  program_name = temp[0];
  p_filename = VARIABLEFILEPATH + program_name;
 }
 else
  p_filename = VARIABLEFILEPATH;
 
 if(namespace_symbol)
  p_filename += "_" + Symbol();
  
 if(namespace_period)
  p_filename += "_" + (string)Period();
  
  p_filename += "_" + filename;
}