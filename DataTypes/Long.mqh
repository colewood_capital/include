//+------------------------------------------------------------------+
//|                                                         Long.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

class Long
{
 protected:
   long my_long;
   
 public:
   void set(long value)   { my_long = value;  }
   long get()             { return(my_long);  }
   
   long operator-() const { return(-my_long); }
   
   void AutoIncrement()  { my_long++;        }
  
   
   Long(long value=0)  { my_long = value;  } 
};
