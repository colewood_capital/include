//+------------------------------------------------------------------+
//|                                                  BandManager.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#ifndef EA
   input int MAGIC_NUMBER=23412047;
   input int BOLLINGER_PERIOD=20;
   input double BOLLINGER_DEVIATION=2.5;
   input int PSAR_MIN_TIMEFRAME=0;
   input double LONG_BAND_TOUCH_EXTENSION_PERCENT=0;
   input double SHORT_BAND_TOUCH_EXTENSION_PERCENT=0;
   input double LONG_CLOSE_ON_PIPS_GAINED=0.0;
   input double SHORT_CLOSE_ON_PIPS_GAINED=0.0;
   input double SINGLE_LONG_CLOSE_PERCENT_ON_BANDS = 0.0;
   input bool SINGLE_LONG_TRIGGER_ON_CLOSE=true;
   input double SINGLE_LONG_EXIT_LIMIT_FRACTAL_PIP_BUFFER= 0.0;
   input double SINGLE_SHORT_EXIT_LIMIT_FRACTAL_PIP_BUFFER= 0.0;
   input int MAX_TIMES_EXIT_LIMIT_CLOSE=0;   //SHOULD REMOVE AND PUT IN SEPARATE SECTION
   input double SINGLE_MIN_PIPS_ON_LONG_EXIT_LIMIT_CLOSE = 0.0;
   input bool SINGLE_SHORT_TRIGGER_ON_CLOSE=true;
   input double SINGLE_CLOSE_PERCENT_ON_LONG_EXIT_LIMIT=0.0;
   input double SINGLE_MIN_PIPS_ON_SHORT_EXIT_LIMIT_CLOSE = 0.0;
#endif 

enum OnTradeCriterion { BULLISH,BEARISH,HH,LL,HH_LL,HIGHER_CLOSE,LOWER_CLOSE,NIL };

enum CloseReason { CLS_REASON_PSAR,CLS_REASON_BANDS,CLS_REASON_PIPS,CLS_REASON_REVERSE,CLS_REASON_EXIT_LIMIT,CLS_REASON_SINGLE_PSAR,CLS_REASON_SINGLE_BANDS,CLS_REASON_SINGLE_PIPS_GAINED };

class BandManager
  {
private:
   int               open_criterion_1;                //Bollinger band touch
   int               open_criterion_2;                //HH or LL
   int               open_criterion_3;                //Higher close or lower close
   int               close_criterion_1;               //Psar
   int               close_criterion_2;               //Opposite band touch
   int               close_criterion_3;               //X Pips Gained
   int               close_criterion_4;               //Reverse momentum

   bool              isTradingAllowed;
   bool              isBelowResetPrice;
   double            trigger_price;
   double            trigger_upper_band;
   double            trigger_lower_band;
   int               prev_state;

public:
   void              CheckTradeOnCriterion1(double percent_buffer=0);
   void              CheckOnTradeCriterion2And3();
   void              CheckOffTradeCriterion1(double exposure,double step,double max);
   void              CheckOffTradeCriterion2(double exposure);
   void              CheckOffTradeCriterion3(double exposure,double profit_in_pips);
   void              CheckOffTradeCriterion4();

   void              GetOpenCriteria(int &criteria[]);
   void              GetCloseCriteria(int &criteria[]);
   
   
   void              ResetTradeCriteria();

                     BandManager();
                    ~BandManager();
  };
//+--------------
void BandManager::CheckOffTradeCriterion4()
  {
   string sym= Symbol();
   int total = OrdersTotal();

   for(int i=total-1; i>=0; i--)
     {
      if(OrderSelect(i,SELECT_BY_POS))
        {
         if(OrderSymbol()==sym && OrderMagicNumber()==MAGIC_NUMBER)
           {
            int ticket = OrderTicket();
            int period = iBarShift(NULL,0,OrderOpenTime());
            int type=OrderType();
            double fractal=0;
            bool ret;

            if(type==OP_BUY)
              {
               fractal=volume.GetExitLimitPrice(OrderTicket());

               for(int j=period; j>=0; j--)
                 {
                  int count=volume.Count(ticket,CLS_REASON_EXIT_LIMIT);

                  double price=(SINGLE_LONG_TRIGGER_ON_CLOSE) ? Close[j+1]: Low[j+1];

                  if(fractal>0 && price<fractal -(SINGLE_LONG_EXIT_LIMIT_FRACTAL_PIP_BUFFER*Point*10) && (MAX_TIMES_EXIT_LIMIT_CLOSE<=0 || count<MAX_TIMES_EXIT_LIMIT_CLOSE))
                    {
                     if(Bid-OrderOpenPrice()>SINGLE_MIN_PIPS_ON_LONG_EXIT_LIMIT_CLOSE*Point*10)
                       {
                        ret=orderManager.CloseTradePercent(ticket,SINGLE_CLOSE_PERCENT_ON_LONG_EXIT_LIMIT);
                        volume.Update(ticket,CLS_REASON_EXIT_LIMIT);
                       }

                     break;
                    }
                 }
              }
            else if(type==OP_SELL)
              {
               fractal=volume.GetExitLimitPrice(OrderTicket());

               for(int j=period; j>=0; j--)
                 {
                  int count=volume.Count(ticket,CLS_REASON_EXIT_LIMIT);

                  double price=(SINGLE_SHORT_TRIGGER_ON_CLOSE) ? Close[j+1]: High[j+1];

                  if(fractal>0 && price>fractal+(SINGLE_SHORT_EXIT_LIMIT_FRACTAL_PIP_BUFFER*Point*10) && (MAX_TIMES_EXIT_LIMIT_CLOSE<=0 || count<MAX_TIMES_EXIT_LIMIT_CLOSE))
                    {
                     if(OrderOpenPrice()-Ask>SINGLE_MIN_PIPS_ON_SHORT_EXIT_LIMIT_CLOSE*Point*10)
                       {
                        ret=orderManager.CloseTradePercent(ticket,SINGLE_CLOSE_PERCENT_ON_LONG_EXIT_LIMIT);
                        volume.Update(ticket,CLS_REASON_EXIT_LIMIT);
                       }

                     break;
                    }
                 }
              }
            else
               continue;
           }
        }
     }
  }
//+--------------
void BandManager::CheckOffTradeCriterion3(double exposure,double profit_in_pips)
  {
   if(exposure>0 && LONG_CLOSE_ON_PIPS_GAINED>0 && profit_in_pips>LONG_CLOSE_ON_PIPS_GAINED)
     {
      close_criterion_3=BEARISH;
      return;
     }

   if(exposure<0 && SHORT_CLOSE_ON_PIPS_GAINED>0 && profit_in_pips>SHORT_CLOSE_ON_PIPS_GAINED)
     {
      close_criterion_3=BULLISH;
      return;
     }

   close_criterion_3=NIL;
  }
//+--------------
void BandManager::CheckOffTradeCriterion2(double exposure)
  {
   string sym= Symbol();
   int shift = 0;

   double upper = iBands(sym,NULL,BOLLINGER_PERIOD,BOLLINGER_DEVIATION,0,PRICE_CLOSE,MODE_UPPER,shift);
   double lower = iBands(sym,NULL,BOLLINGER_PERIOD,BOLLINGER_DEVIATION,0,PRICE_CLOSE,MODE_LOWER,shift);
   double prev_upper = iBands(sym,NULL,BOLLINGER_PERIOD,BOLLINGER_DEVIATION,0,PRICE_CLOSE,MODE_UPPER,shift+1);
   double prev_lower = iBands(sym,NULL,BOLLINGER_PERIOD,BOLLINGER_DEVIATION,0,PRICE_CLOSE,MODE_LOWER,shift+1);
   double reset_price= iBands(sym,NULL,BOLLINGER_PERIOD,BOLLINGER_DEVIATION,0,PRICE_CLOSE,MODE_MAIN,shift);
//NOTE: changed reset_price from static to regular double

//---- Condition to reenable trading
   if(!isTradingAllowed)
     {
      if(isBelowResetPrice)
        {
         if(Bid>=reset_price)
           {
            isTradingAllowed=true;
            trigger_price=0;
           }
        }
      else
        {
         if(Bid<=reset_price)
           {
            isTradingAllowed=true;
            trigger_price=0;
           }
        }
     }

//---- Condition to measure bollinger band extensions
   if(LONG_BAND_TOUCH_EXTENSION_PERCENT>0 && trigger_price==0 && exposure>0 && Bid>=prev_upper)
     {
      trigger_price=prev_upper;
      trigger_upper_band=prev_upper;
      trigger_lower_band=prev_lower;
     }

   if(SHORT_BAND_TOUCH_EXTENSION_PERCENT>0 && trigger_price==0 && exposure<0 && Bid<=prev_lower)
     {
      trigger_price=prev_lower;
      trigger_upper_band=prev_upper;
      trigger_lower_band=prev_lower;
     }

   if(exposure>0 && isTradingAllowed)
     {
      if((trigger_price>0 && Bid>trigger_price+(LONG_BAND_TOUCH_EXTENSION_PERCENT *(trigger_upper_band-trigger_lower_band))) || //NOTE: upper and lower should technically be fixed to the index of trigger price
         (LONG_BAND_TOUCH_EXTENSION_PERCENT==0 && Bid>upper))
        {
         close_criterion_2=BEARISH;
         isTradingAllowed=false;         //Disable signal until it is reset
         isBelowResetPrice=false;
         trigger_price=0;
        }

      return;
     }

   if(exposure<0 && isTradingAllowed)
     {
      if((trigger_price>0 && Bid<trigger_price -(SHORT_BAND_TOUCH_EXTENSION_PERCENT *(trigger_upper_band-trigger_lower_band))) || 
         (SHORT_BAND_TOUCH_EXTENSION_PERCENT==0 && Bid<lower))
        {
         close_criterion_2=BULLISH;
         isTradingAllowed=false;
         isBelowResetPrice=true;
         trigger_price=0;
        }

      return;
     }

   close_criterion_2=NIL;
  }
//+--------------
void BandManager::CheckOffTradeCriterion1(double exposure,double step=0.008,double max=0.2)
  {
   string sym=Symbol();

   double high=iHigh(sym,NULL,0);
   double past_high=iHigh(sym,NULL,1);
   double low=iLow(sym,NULL,0);
   double past_low=iLow(sym,NULL,1);
   double psar=iSAR(sym,PSAR_MIN_TIMEFRAME,step,max,0);
   double past_psar=iSAR(sym,PSAR_MIN_TIMEFRAME,step,max,1);

   if(past_psar>=past_high && psar<=low && prev_state!=BEARISH) //Bullish
     {
      if(exposure<0)
         close_criterion_1=BEARISH;

      prev_state=BEARISH;

      return;
     }

   if(past_psar<=past_low && psar>=high && prev_state!=BULLISH) //Bearish
     {
      if(exposure>0)
         close_criterion_1=BULLISH;

      prev_state=BULLISH;

      return;
     }

   if(MathAbs(exposure)<DBL_EPSILON)
      prev_state=NIL;

   close_criterion_1=NIL;
  }
//+--------------
void BandManager::ResetTradeCriteria()
  {
   open_criterion_1=NIL;
   open_criterion_2=NIL;
   open_criterion_3=NIL;
  }
//+--------------
void BandManager::CheckOnTradeCriterion2And3()
  {
   string sym= Symbol();
   int shift = 1;

//---- Trade Condition 2: Check HH or LL
   double high= iHigh(sym,NULL,shift);
   double low = iLow(sym,NULL,shift);
   double prev_high= iHigh(sym,NULL,shift+1);
   double prev_low = iLow(sym,NULL,shift+1);

   if(high>prev_high && low<prev_low)
      open_criterion_2=HH_LL;
   else if(high>prev_high)
      open_criterion_2=HH;
   else if(low<prev_low)
      open_criterion_2=LL;
   else
      open_criterion_2=NIL;

//---- Trade Condition 3: Check higher close or lower close
   double close=iClose(sym,NULL,shift);
   double prev_close=iClose(sym,NULL,shift+1);

   if(close>prev_close)
      open_criterion_3=HIGHER_CLOSE;
   else if(close<prev_close)
      open_criterion_3=LOWER_CLOSE;
   else
      open_criterion_3=NIL;
  }
//+--------------
void BandManager::GetCloseCriteria(int &criteria[])
  {
   ArrayResize(criteria,3);

   criteria[0] = close_criterion_1;
   criteria[1] = close_criterion_2;
   criteria[2] = close_criterion_3;

   return;
  }
//+--------------
void BandManager::GetOpenCriteria(int &criteria[])
  {
   ArrayResize(criteria,3);

   criteria[0] = open_criterion_1;
   criteria[1] = open_criterion_2;
   criteria[2] = open_criterion_3;

   return;
  }
//+--------------
void BandManager::CheckTradeOnCriterion1(double percent_buffer=0) //percent_buffer = %dist(band,MA) to allowed for near touches
  {
   string sym= Symbol();
   int shift = 1;
   double high= iHigh(sym,NULL,shift);
   double low = iLow(sym,NULL,shift);
   double ma=iBands(sym,NULL,BOLLINGER_PERIOD,BOLLINGER_DEVIATION,0,PRICE_CLOSE,MODE_MAIN,shift);
   double upper = iBands(sym,NULL,BOLLINGER_PERIOD,BOLLINGER_DEVIATION,0,PRICE_CLOSE,MODE_UPPER,shift);
   double lower = iBands(sym,NULL,BOLLINGER_PERIOD,BOLLINGER_DEVIATION,0,PRICE_CLOSE,MODE_LOWER,shift);

//---- Check if both bands have been hit
   if(high>=upper && low<=lower) 
     {
      open_criterion_1=NIL;
      return;
     }

//---- Check if upper band has been hit 
   if(upper!=ma)
     {
      if((high-ma)/(upper-ma)>=1 -(percent_buffer/100)) 
        {
         open_criterion_1=BEARISH;
         return;
        }
     }

//---- Check if lower band has been hit 
   if(lower!=ma)
     {
      if((ma-low)/(ma-lower)>=1 -(percent_buffer/100)) 
        {
         open_criterion_1=BULLISH;
         return;
        }
     }

   return;
  }
//+--------------
BandManager::~BandManager()
  {
/*
  delete open_criterion_1;
  delete open_criterion_2;
  delete open_criterion_3;
  delete close_criterion_1;
  delete close_criterion_2;
  delete close_criterion_3;
  delete close_criterion_4;
  
  delete isTradingAllowed;
  delete isBelowResetPrice;
  delete trigger_price;
  delete trigger_upper_band;
  delete trigger_lower_band;
  delete prev_state;
  */
  }
//+--------------
BandManager::BandManager()
  {
/*
  string sym = Symbol()+"_";
  
  open_criterion_1 = new AutoSaveInteger(sym + FILENAME_OPEN_1, AUTOSAVE_EXPIRATION_IN_MINS, NIL);            //Bollinger band touch
  open_criterion_2 = new AutoSaveInteger(sym + FILENAME_OPEN_2, AUTOSAVE_EXPIRATION_IN_MINS, NIL);            //HH or LL
  open_criterion_3 = new AutoSaveInteger(sym + FILENAME_OPEN_3, AUTOSAVE_EXPIRATION_IN_MINS, NIL);            //Higher close or lower close
  close_criterion_1 = new AutoSaveInteger(sym + FILENAME_CLOSE_1, AUTOSAVE_EXPIRATION_IN_MINS, NIL);          //Psar
  close_criterion_2 = new AutoSaveInteger(sym + FILENAME_CLOSE_2, AUTOSAVE_EXPIRATION_IN_MINS, NIL);          //Opposite band touch
  close_criterion_3 = new AutoSaveInteger(sym + FILENAME_CLOSE_3, AUTOSAVE_EXPIRATION_IN_MINS, NIL);          //X Pips Gained
  close_criterion_4 = new AutoSaveInteger(sym + FILENAME_CLOSE_4, AUTOSAVE_EXPIRATION_IN_MINS, NIL);          //Reverse momentum   
  
  isTradingAllowed = new AutoSaveBoolean(sym + FILENAME_IS_TRADING_ALLOWED, AUTOSAVE_EXPIRATION_IN_MINS, true);
  isBelowResetPrice = new AutoSaveBoolean(sym + FILENAME_IS_BELOW_RESET_PRICE, AUTOSAVE_EXPIRATION_IN_MINS, false);
  trigger_price = new AutoSaveDouble(sym + FILENAME_TRIGGER_PRICE, AUTOSAVE_EXPIRATION_IN_MINS, 0.0);
  trigger_upper_band = new AutoSaveDouble(sym + FILENAME_TRIGGER_UPPER_BAND, AUTOSAVE_EXPIRATION_IN_MINS, 0.0);
  trigger_lower_band = new AutoSaveDouble(sym + FILENAME_TRIGGER_LOWER_BAND, AUTOSAVE_EXPIRATION_IN_MINS, 0.0);
  prev_state = new AutoSaveInteger(sym + FILENAME_PREV_STATE, AUTOSAVE_EXPIRATION_IN_MINS, NIL);
  
  //NOTE: Originally only open_criterion_1 and close_criterion_1 set to NIL
  */
  }