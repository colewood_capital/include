//+------------------------------------------------------------------+
//|                                                  ServerTools.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#define SYMBOL_ARRAY_SIZE     12
#define CMT_ARRAY_SIZE    32
#define DEALER_MAGIC_NUMBER   -858993460

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
#endif 

#ifndef ENUMS
#define ENUMS
   #include <Enums.mqh>
#endif 

#define MANAGER_CONNECT(RET_VALUE) if(!TestServer::ManagerConnect(MANAGER_HOST, MANAGER_ACCT, MANAGER_PASSWD)) return RET_VALUE

//+------------------------------------------------------------------+
//| DLL imports                                                      |
//+------------------------------------------------------------------+
struct RateInfo
{
 uint ctm;
 int open;
 int high;
 int low;
 int close;
 double volume;
};

#define __time32_t   uint

struct TradeRecord
{
 int               order;            // order ticket
 int               login;            // owner's login
 
 // security
 char s_0; char s_1; char s_2; char s_3; char s_4; char s_5; char s_6; char s_7; char s_8; char s_9; char s_10; char s_11;    
 int               digits;           // security precision
 int               cmd;              // trade command
 int               volume;           // volume
   
 __time32_t        open_time;        // open time
 int               state;            // reserved
 double            open_price;       // open price
 double            sl,tp;            // stop loss & take profit
 __time32_t        close_time;       // close time
 __time32_t        value_date;       // value date
 __time32_t        expiration;       // pending order's expiration time
 char              reason;           // trade reason
 
 // reserved fields
 char conv_reserv_0; char conv_reserv_1; char conv_reserv_2;
 
 // convertation rates from profit currency to group deposit currency
 double conv_rates_0; double conv_rates_1;   
                                       // (first element-for open time, second element-for close time)
 double            commission;       // commission
 double            commission_agent; // agent commission
 double            storage;          // order swaps
 double            close_price;      // close price
 double            profit;           // profit
 double            taxes;            // taxes
 int               magic;            // special value used by client experts
 
 // comment
 char c_0; char c_1; char c_2; char c_3; char c_4; char c_5; char c_6; char c_7; char c_8;
 char c_9; char c_10; char c_11; char c_12; char c_13; char c_14; char c_15; char c_16; char c_17;
 char c_18; char c_19; char c_20; char c_21; char c_22; char c_23; char c_24; char c_25; char c_26;
 char c_27; char c_28; char c_29; char c_30; char c_31;
 
 int               internal_id;      // trade order ticket on master server in STP
 int               activation;       // used by MT Manager
 int               spread;           // spread
 double            margin_rate;      // margin convertation rate (rate of convertation from margin currency to deposit one)
 __time32_t        timestamp;        // timestamp
 
 // reserved
 int reserved_0; int reserved_1; int reserved_2; int reserved_3; 
 
 //TradeRecord*      next;            // internal data (4 bytes)
 int internal_data;
};

string TradeRecordComment(const TradeRecord& tr)
{
 char cmt[32];
 
 cmt[0]=tr.c_0;
 cmt[1]=tr.c_1;
 cmt[2]=tr.c_2;
 cmt[3]=tr.c_3;
 cmt[4]=tr.c_4;
 cmt[5]=tr.c_5;
 cmt[6]=tr.c_6;
 cmt[7]=tr.c_7;
 cmt[8]=tr.c_8;
 cmt[9]=tr.c_9;
 cmt[10]=tr.c_10;
 cmt[11]=tr.c_11;
 cmt[12]=tr.c_12;
 cmt[13]=tr.c_13;
 cmt[14]=tr.c_14;
 cmt[15]=tr.c_15;
 cmt[16]=tr.c_16;
 cmt[17]=tr.c_17;
 cmt[18]=tr.c_18;
 cmt[19]=tr.c_19;
 cmt[20]=tr.c_20;
 cmt[21]=tr.c_21;
 cmt[22]=tr.c_22;
 cmt[23]=tr.c_23;
 cmt[24]=tr.c_24;
 cmt[25]=tr.c_25;
 cmt[26]=tr.c_26;
 cmt[27]=tr.c_27;
 cmt[28]=tr.c_28;
 cmt[29]=tr.c_29;
 cmt[30]=tr.c_30;
 cmt[31]=tr.c_31;
 
 return CharArrayToString(cmt);
}

string TradeRecordSymbol(const TradeRecord& tr)
{
 char __symbol[6];
 
 __symbol[0] = tr.s_0;
 __symbol[1] = tr.s_1;
 __symbol[2] = tr.s_2;
 __symbol[3] = tr.s_3;
 __symbol[4] = tr.s_4;
 __symbol[5] = tr.s_5;
 
 return CharArrayToString(__symbol);
}

struct MqlTrade
{
 int login;
 int booknum;
 string symbol;
 int cmd;
 double lots;
 double sl;
 double tp;
 string cmt;
 double open_price;
 datetime open_time;
 double close_price;
 datetime close_time;
 double swaps;
 double commission;
 double profit;
 int magic;
};

struct ClosedTradeInfo
{
 int booknum;
 string symbol;
 double sl;
 double tp;
 datetime close_time;
 double close_price;
 double swaps;
 double profit;
 int state;
 int reason;
};

#import "TestServerInterface.dll"
   int _ManagerConnect(char& ip_address[], int login, char& passwd[]);
   int _ManagerSendTick(char& symbol[], double bid, double ask);
   
   int _ManagerChartRequest(char& symbol[], int s_size, int period, uint begin);
   void _ManagerChartGet(RateInfo& rates[], int rates_size);
   void _ManagerChartDelete(char& symbol[], int s_size, uint begin);
   
   int _ManagerTradeOpen(int login, char& symbol[], int s_size, int cmd, double lots, double price, double sl,
                         double tp, char& comment[], int c_size, string& error_msg);
   bool _ManagerTradeInfo(int booknum, int& login, int& cmd, char& symbol[], int s_size, double& lots, 
                          double& open_price, uint& open_time, double& sl, double& tp, double& close_price, uint& close_time,
                          double& commission, double& swap, double& profit, char& comment[], int c_size, int& magic);                    
   void _ManagerTradesRequest(int& count);
   void _ManagerTradesGet(TradeRecord& trades[], int count);
   int _ManagerTradesGetByLogin(TradeRecord& trades[], int count, int login);
   int _ManagerTradesGetBySymbol(TradeRecord& trades[], int count, char& symbol[], int symbol_size);
   int _ManagerTradesGetBySymbolLogin(TradeRecord& trades[], int count,  char& symbol[], int symbol_size, int login);
   bool _ManagerTradeClose(int booknum, double lots, double price, string& error_msg);
   void _ManagerTradeCloseAll(int& booknum[], double& cls_price[], string& error_msg, int arrays_size);
   void _ManagerClosedTradeInfo(int booknum, char& symbol[], double& sl, double& tp, uint& close_time, double& close_price, double& swaps, double& profit, int& state, int& reason);
   uint _ServerTime();
   bool _ManagerIsConnected();
   //void ManagerTradesGet(TradeRecord& trades[], int count);     --- see comment in DLL  
#import

enum ENUM_SERVER_RET_CODE {
   RET_CODE_BAD_INTERNET_CONNECTION_OR_INVALID_IP = -10,
   RET_CODE_INVALID_USER_PASSWD = -9,
   RET_CODE_INVALID_MANAGER = -8,
   RET_CODE_UNABLE_TO_SEND_TICK = -1,
   RET_OK = 0,
   RET_DUPLICATE_TICK=1,
   RET_CODE_NOT_CONNECTED = 6,
};

int _k;                       //Counter for SendTickWhile MACRO
#define STEP_UP +
#define STEP_DOWN -
#define SendTickWhile(CONDITION, PIPS_STEP, PLUS_MINUS, MAX_ATTEMPTS)  _k=0; while(CONDITION && _k < MAX_ATTEMPTS && !IsStopped()) { RefreshRates(); TradeServer::ManagerSendTick(symbol, Bid PLUS_MINUS (PIPS_STEP * Point * 10), Ask PLUS_MINUS (PIPS_STEP * Point * 10));  _k++; }    if(_k >= MAX_ATTEMPTS) logger.log("SendTicksWhile() terminated for MAX_ATTEMPTS", __LINE__, __FUNCTION__, true)
#define SendTickUntil(CONDITION, PIPS_STEP, PLUS_MINUS, MAX_ATTEMPTS)  _k=0; while(!(CONDITION) && _k < MAX_ATTEMPTS && !IsStopped()) { RefreshRates(); TradeServer::ManagerSendTick(symbol, Bid PLUS_MINUS (PIPS_STEP * Point * 10), Ask PLUS_MINUS (PIPS_STEP * Point * 10));  _k++; } if(_k >= MAX_ATTEMPTS) logger.log("Warning: SendTicksUntil() terminated for MAX_ATTEMPTS", __LINE__, __FUNCTION__, true)


class TestServer
{
 private:
  static Logger logger;
  
  static bool _HasTickUpdated(string symbol, double new_bid, double new_ask);
  static bool _CheckTickUpdated(string symbol, double new_bid, double new_ask);
  
 public:
  static int ManagerTradesRequest(TradeRecord& trades[]);
  static int ManagerTradesRequest(TradeRecord& trades[], int login);
  static int ManagerTradesRequest(TradeRecord& trades[], string symbol);
  static int ManagerTradesRequest(TradeRecord& trades[], int login, string symbol);
  static bool ManagerTradesCloseAll();
  static bool ManagerTradesCloseAll(int login);
  static bool ManagerTradesCloseAll(int login, string symbol);
  static bool ManagerTradesCloseAll(string symbol);
  static bool ManagerTradesClose(int& booknum[]);
  static bool ManagerTradesClose(int booknum, double lots, double price=0);
  static int ManagerTradeOpen(int login, string symbol, int cmd, double lots, double prc=0, double sl=0, double tp=0, string cmt=NULL);
  static bool ManagerTradeInfo(int booknum, MqlTrade& info);
  static int ManagerChartRequest(string symbol, ENUM_TIMEFRAMES period, RateInfo& rates[], datetime begin=0);
  static bool ManagerChartDelete(string symbol, datetime begin);
  static bool ManagerSendTick(string symbol, double prc);
  static bool ManagerSendTick(string symbol, double bid, double ask);
  static bool ManagerConnect(string ip, int login, string passwd);
  static void ManagerClosedTradeInfo(int booknum, ClosedTradeInfo& info);
  static uint TimeServer();
  static bool ManagerIsConnected();
  static int ManagerOrdersTotal(int account_number, string symbol);
};

Logger TestServer::logger(__FILE__, "Unittests");

static int TestServer::ManagerOrdersTotal(int account_number=0, string symbol=NULL)
{
 TradeRecord trades[];
 int trades_total;
 
 if(account_number>0 && symbol!=NULL)
  trades_total=ManagerTradesRequest(trades, account_number, symbol);
 else if(account_number>0)
  trades_total=ManagerTradesRequest(trades, account_number);
 else if(symbol!=NULL)
  trades_total=ManagerTradesRequest(trades, symbol);
 else
  trades_total=ManagerTradesRequest(trades);
  
 return trades_total;
}

static void TestServer::ManagerClosedTradeInfo(int booknum,ClosedTradeInfo& info)
{
 char symb[12];
 uint _close_time;

 _ManagerClosedTradeInfo(booknum, symb, info.sl, info.tp, _close_time, info.close_price, info.swaps, info.profit, info.state, info.reason);
 
 info.symbol = CharArrayToString(symb);
 info.close_time = (datetime)_close_time;
}

static uint TestServer::TimeServer()
{
 return _ServerTime();
}

static bool TestServer::ManagerIsConnected()
{
 return _ManagerIsConnected();
}

static bool TestServer::ManagerTradesCloseAll()
{
 int booknum[];
 double cls_price[];
 TradeRecord trades[];

 int trades_total = TestServer::ManagerTradesRequest(trades);
 ArrayResize(booknum, trades_total);
 ArrayResize(cls_price, trades_total);
 string error_msg = "";
 
 for(int j=0; j<trades_total; j++)
 {
  booknum[j] = trades[j].order;
   
  if(trades[j].cmd==OP_BUY)
   cls_price[j] = MarketInfo(TradeRecordSymbol(trades[j]), MODE_BID);
      
  else if(trades[j].cmd==OP_SELL)
   cls_price[j] = MarketInfo(TradeRecordSymbol(trades[j]), MODE_ASK);
      
  else
  {
   logger.log("order type must be of type market, cmd=" + (string)trades[j].cmd, LOG_TYPE_HIGH, __LINE__, __FUNCTION__, true);
   return false;
  }
 }

 _ManagerTradeCloseAll(booknum, cls_price, error_msg, trades_total);
 
 if(error_msg != "")
  logger.log("error_msg=" + (string)error_msg, LOG_TYPE_HIGH, __LINE__, __FUNCTION__, true);
 
 return (error_msg == "");
}


static bool TestServer::ManagerTradesCloseAll(int login, string symbol)
{
 int booknum[];
 double cls_price[];
 TradeRecord trades[];
 int size = 0;

 int trades_total = ManagerTradesRequest(trades);
 ArrayResize(booknum, trades_total);
 ArrayResize(cls_price, trades_total);
 string error_msg = "";
 
 for(int j=0; j<trades_total; j++)
 {
  if(trades[j].login == login && TradeRecordSymbol(trades[j]) == symbol)
  {
   booknum[size] = trades[j].order;
   
   if(trades[j].cmd==OP_BUY)
    cls_price[size] = MarketInfo(symbol, MODE_BID);
      
   else if(trades[j].cmd==OP_SELL)
    cls_price[size] = MarketInfo(symbol, MODE_ASK);
      
   else
   {
    logger.log("order type must be of type market, cmd=" + (string)trades[j].cmd, LOG_TYPE_HIGH, __LINE__, __FUNCTION__, true);
    return false;
   }
   
   size++;
  }
 }

 _ManagerTradeCloseAll(booknum, cls_price, error_msg, size);
 
 if(error_msg != "")
  logger.log("error_msg=" + (string)error_msg, LOG_TYPE_HIGH, __LINE__, __FUNCTION__, true);
 
 return (error_msg == "");
}


static bool TestServer::ManagerTradesCloseAll(int login)
{
 int booknum[];
 double cls_price[];
 TradeRecord trades[];
 int size = 0;

 int trades_total = ManagerTradesRequest(trades);
 ArrayResize(booknum, trades_total);
 ArrayResize(cls_price, trades_total);
 string error_msg = "";
 
 for(int j=0; j<trades_total; j++)
 {
  if(trades[j].login == login)
  {
   booknum[size] = trades[j].order;
   
   if(trades[j].cmd==OP_BUY)
    cls_price[size] = MarketInfo(TradeRecordSymbol(trades[j]), MODE_BID);
      
   else if(trades[j].cmd==OP_SELL)
    cls_price[size] = MarketInfo(TradeRecordSymbol(trades[j]), MODE_ASK);
      
   else
   {
    logger.log("order type must be of type market, cmd=" + (string)trades[j].cmd, LOG_TYPE_HIGH, __LINE__, __FUNCTION__, true);
    return false;
   }
   
   size++;
  }
 }

 _ManagerTradeCloseAll(booknum, cls_price, error_msg, size);
 
 if(error_msg != "")
  logger.log("error_msg=" + (string)error_msg, LOG_TYPE_HIGH, __LINE__, __FUNCTION__, true);
 
 return (error_msg == "");
}


static bool TestServer::ManagerTradesCloseAll(string symbol)
{
 int booknum[];
 double cls_price[];
 TradeRecord trades[];
 int size = 0;

 int trades_total = ManagerTradesRequest(trades);
 ArrayResize(booknum, trades_total);
 ArrayResize(cls_price, trades_total);
 string error_msg = "";
 
 for(int j=0; j<trades_total; j++)
 {
  if(TradeRecordSymbol(trades[j]) == symbol)
  {
   booknum[size] = trades[j].order;
   
   if(trades[j].cmd==OP_BUY)
    cls_price[size] = MarketInfo(symbol, MODE_BID);
      
   else if(trades[j].cmd==OP_SELL)
    cls_price[size] = MarketInfo(symbol, MODE_ASK);
      
   else
   {
    logger.log("order type must be of type market, cmd=" + (string)trades[j].cmd, LOG_TYPE_HIGH, __LINE__, __FUNCTION__, true);
    return false;
   }
   
   size++;
  }
 }

 _ManagerTradeCloseAll(booknum, cls_price, error_msg, size);
 
 if(error_msg != "")
  logger.log("error_msg=" + (string)error_msg, LOG_TYPE_HIGH, __LINE__, __FUNCTION__, true);
 
 return (error_msg == "");
}


static int TestServer::ManagerTradesRequest(TradeRecord& trades[], int login, string symbol)
{
 int count;
 char __symb[SYMBOL_ARRAY_SIZE];
 StringToCharArray(symbol, __symb);
   
 _ManagerTradesRequest(count);
 
 ArrayResize(trades, count);
 
 return _ManagerTradesGetBySymbolLogin(trades, count, __symb, SYMBOL_ARRAY_SIZE, login);
}


static int TestServer::ManagerTradesRequest(TradeRecord& trades[], string symbol)
{
 int count;
 char __symb[SYMBOL_ARRAY_SIZE];
 StringToCharArray(symbol, __symb);
   
 _ManagerTradesRequest(count);
 
 ArrayResize(trades, count);
 
 return _ManagerTradesGetBySymbol(trades, count, __symb, SYMBOL_ARRAY_SIZE);
}


static int TestServer::ManagerTradesRequest(TradeRecord& trades[], int login)
{
 int count;
  
 _ManagerTradesRequest(count);
 
 ArrayResize(trades, count);
 
 return _ManagerTradesGetByLogin(trades, count, login);
}


static int TestServer::ManagerTradesRequest(TradeRecord& trades[])
{
  int count;
  
  _ManagerTradesRequest(count);
  
  ArrayResize(trades, count);

  _ManagerTradesGet(trades, count);
  
  return count;
}


static bool TestServer::ManagerTradeInfo(int booknum, MqlTrade& info)
{
 char symb_input[SYMBOL_ARRAY_SIZE];
 char cmt_input[CMT_ARRAY_SIZE];
 uint tmp_open_time, tmp_close_time;
 
 bool result = _ManagerTradeInfo(booknum, info.login, info.cmd, symb_input, SYMBOL_ARRAY_SIZE, info.lots, info.open_price, tmp_open_time, info.sl, info.tp,
                                 info.close_price, tmp_close_time, info.commission, info.swaps, info.profit, cmt_input, CMT_ARRAY_SIZE, info.magic);
                                 
 info.booknum = booknum;
 info.symbol = CharArrayToString(symb_input);
 info.cmt = CharArrayToString(cmt_input);
 info.open_time = (datetime)tmp_open_time;
 info.close_time = (datetime)tmp_close_time;
 
 return result;
}


static bool TestServer::ManagerTradesClose(int& booknum[])
{
 double cls_price[];
 TradeRecord trades[];
 int size = ArraySize(booknum);
 ArrayResize(cls_price, size);

 int trades_total = ManagerTradesRequest(trades);
 
 string error_msg = "";
 
 for(int i=0; i<size; i++)
 {
  for(int j=0; j<trades_total; j++)
  {
   if(trades[j].order == booknum[i])
   {
    if(trades[j].cmd==OP_BUY)
     cls_price[i] = MarketInfo(TradeRecordSymbol(trades[j]), MODE_BID);
      
    else if(trades[j].cmd==OP_SELL)
     cls_price[i] = MarketInfo(TradeRecordSymbol(trades[j]), MODE_ASK);
      
    else
    {
     logger.log("order type must be of type market, cmd=" + (string)trades[j].cmd, LOG_TYPE_HIGH, __LINE__, __FUNCTION__, true);
     return false;
    }
   }
  }
 }
 
 _ManagerTradeCloseAll(booknum, cls_price, error_msg, size);
 
 if(error_msg != "")
  logger.log("error_msg=" + (string)error_msg, LOG_TYPE_HIGH, __LINE__, __FUNCTION__, true);
 
 return (error_msg == "");
}


static bool TestServer::ManagerTradesClose(int booknum, double lots, double price=0)
{
 string error_msg = "";     // if passed in as NULL, will cause access violation write

 MqlTrade trade;
 double cls_prc;
 
 bool result = ManagerTradeInfo(booknum, trade);
 
 if(price < DBL_EPSILON)
 {
  if(trade.cmd == OP_BUY)
   cls_prc = MarketInfo(trade.symbol, MODE_BID);
  else if(trade.cmd == OP_SELL)
   cls_prc = MarketInfo(trade.symbol, MODE_ASK);
  else
  {
   logger.log("order type must be of type market, cmd=" + (string)trade.cmd, LOG_TYPE_HIGH, __LINE__, __FUNCTION__, true);
   return false;
  }
 }
 else
  cls_prc = price;
 
 result = _ManagerTradeClose(trade.booknum, MathMin(lots, trade.lots), cls_prc, error_msg);
 
 if(error_msg != "")
  logger.log("error_msg=" + (string)error_msg, LOG_TYPE_HIGH, __LINE__, __FUNCTION__, true);
                           
 return result;
}


static int TestServer::ManagerTradeOpen(int login, string symbol, int cmd, double lots, double prc=0, double sl=0, double tp=0, string cmt=NULL)
{
 if(symbol==NULL)
  symbol = Symbol();
  
 string error_msg = "";     // if passed in as NULL, will cause access violation write
 
 if(cmt==NULL)
  cmt = "";
  
 char s_input[SYMBOL_ARRAY_SIZE];
 StringToCharArray(symbol, s_input);
 
 char cmt_input[CMT_ARRAY_SIZE];
 StringToCharArray(cmt, cmt_input);
 
 if(cmd==OP_BUY) 
 {
  if(MathAbs(prc) < DBL_EPSILON)
    prc = MarketInfo(symbol, MODE_ASK);
 }
 else if(cmd==OP_SELL)
 {
  if(MathAbs(prc) < DBL_EPSILON)
    prc = MarketInfo(symbol, MODE_BID);
 }
 else
 {
  logger.log("order type must be of type market, cmd=" + (string)cmd, LOG_TYPE_HIGH, __LINE__, __FUNCTION__, true);
  return -1;
 }

 int booknum = _ManagerTradeOpen(login, s_input, SYMBOL_ARRAY_SIZE, cmd, lots, prc, sl, tp, cmt_input, CMT_ARRAY_SIZE, error_msg);
 
 if(error_msg != "")
  logger.log("error_msg=" + (string)error_msg, LOG_TYPE_HIGH, __LINE__, __FUNCTION__, true);
 
 if(booknum <= 0) {
  logger.log("booknum = " + (string)booknum, LOG_TYPE_HIGH, __LINE__, __FUNCTION__, true);
  logger.log("symbol=" + symbol + ", cmd=" + (string)cmd + ", lots=" + (string)lots + ", prc=" + (string)prc + ", sl=" + (string)sl + ", tp=" + (string)tp + ", cmt_input=" + cmt, LOG_TYPE_HIGH, __LINE__, __FUNCTION__, true);
  logger.log("bid=" + DoubleToStr(MarketInfo(symbol, MODE_BID), (int)MarketInfo(symbol, MODE_DIGITS)) + ", ask=" + DoubleToStr(MarketInfo(symbol, MODE_ASK)) + ", digits=" + (string)(int)MarketInfo(symbol, MODE_DIGITS), LOG_TYPE_HIGH, __LINE__, __FUNCTION__, true);
  logger.log("error_msg= " + (string)error_msg, LOG_TYPE_HIGH, __LINE__, __FUNCTION__, true);
 }
 
 return booknum;
}


static bool TestServer::ManagerChartDelete(string symbol, datetime begin)
{
 datetime server_time = TimeCurrent();
 
 if(begin < server_time)
 {
  if(symbol==NULL)    
   symbol = Symbol();
   
  char s_input[SYMBOL_ARRAY_SIZE];
  
  StringToCharArray(symbol, s_input, 0, SYMBOL_ARRAY_SIZE);
   
  _ManagerChartDelete(s_input, SYMBOL_ARRAY_SIZE, (uint)begin);
  
  return true;
 }
 
 logger.log("begin=" + TimeToStr(begin,TIME_DATE|TIME_MINUTES|TIME_SECONDS) + " > server_time=" + TimeToStr(server_time,TIME_DATE|TIME_MINUTES|TIME_SECONDS), LOG_TYPE_HIGH, __LINE__, __FUNCTION__, true);
 return false;
}


static int TestServer::ManagerChartRequest(string symbol, ENUM_TIMEFRAMES period, RateInfo& rates[], datetime begin=0)
{
 datetime server_time = TimeCurrent();
 
 if(begin < server_time)
 {
  if(symbol==NULL)    
   symbol = Symbol();
 
  char s_input[SYMBOL_ARRAY_SIZE];
  
  StringToCharArray(symbol, s_input, 0, SYMBOL_ARRAY_SIZE);
   
  int total = _ManagerChartRequest(s_input, SYMBOL_ARRAY_SIZE, period, (uint)begin);
  
  ArrayResize(rates, total);
  
  if(total > 0)
   _ManagerChartGet(rates, total);
  
  return total;
 }
 
 logger.log("begin=" + TimeToStr(begin,TIME_DATE|TIME_MINUTES|TIME_SECONDS) + " > server_time=" + TimeToStr(server_time,TIME_DATE|TIME_MINUTES|TIME_SECONDS), LOG_TYPE_HIGH, __LINE__, __FUNCTION__, true);
 return -1;
}


static bool TestServer::ManagerSendTick(string symbol, double bid, double ask)
{
 if(symbol==NULL)
  symbol = Symbol();
  
 char s_input[50];
 StringToCharArray(symbol, s_input);

 //--- Send tick
  ENUM_SERVER_RET_CODE ret_code = (ENUM_SERVER_RET_CODE)_ManagerSendTick(s_input, bid, ask);
 
  if(ret_code != RET_OK)
   logger.log(EnumToString(ret_code), LOG_TYPE_HIGH, __LINE__, __FUNCTION__, true);
 
 _HasTickUpdated(symbol, bid, ask);
 
 RefreshRates();
 
 return (ret_code==RET_OK);
}


static bool TestServer::ManagerSendTick(string symbol, double prc)
{
 return TestServer::ManagerSendTick(symbol, prc, prc);
}


static bool TestServer::ManagerConnect(string ip, int login, string passwd)
{
 if(!IsConnected()) {
  //Print("[ERROR] Terminal not connected in " + __FUNCTION__);
  return false;
 }
 
 char ip_input[50];
 StringToCharArray(ip, ip_input);
 
 char passwd_input[50];
 StringToCharArray(passwd, passwd_input);

 int ret_code = _ManagerConnect(ip_input, login, passwd_input);
 
 if(ret_code == RET_CODE_BAD_INTERNET_CONNECTION_OR_INVALID_IP)
  logger.log(EnumToString(RET_CODE_BAD_INTERNET_CONNECTION_OR_INVALID_IP), LOG_TYPE_HIGH, __LINE__, __FUNCTION__, true);
 else if(ret_code == RET_CODE_INVALID_USER_PASSWD)
  logger.log(EnumToString(RET_CODE_INVALID_USER_PASSWD), LOG_TYPE_HIGH, __LINE__, __FUNCTION__, true);
 else if(ret_code == RET_CODE_INVALID_MANAGER) {
  logger.log(EnumToString(RET_CODE_INVALID_MANAGER), LOG_TYPE_HIGH, __LINE__, __FUNCTION__, true);
  logger.log("Check Manager DLL in Libraries\\", LOG_TYPE_HIGH, __LINE__, __FUNCTION__, true);
 }
 else if(ret_code < 0)
  logger.log("unknown RET_CODE = " + (string)ret_code, LOG_TYPE_HIGH, __LINE__, __FUNCTION__, true);
  
 return (ret_code >= 0);
}


bool TestServer::_HasTickUpdated(string symbol, double new_bid, double new_ask)
{ //Sometimes returns a false negative; however, effectively reduces new ticks
  //sent before previous tick updates
 int digits = (int)MarketInfo(symbol, MODE_DIGITS);
 uint TIMEOUT_MILLISECONDS = 8000;
 ulong now = GetMicrosecondCount();
 new_bid = NormalizeDouble(new_bid, digits);
 new_ask = NormalizeDouble(new_ask, digits);
 
 while((uint)(GetMicrosecondCount() - now) < TIMEOUT_MILLISECONDS * 1000)
 {
  RefreshRates();
  
  if(_CheckTickUpdated(symbol, new_bid, new_ask))
   break;
  
  Sleep(300);
 }
 
 RefreshRates();
 
 double bid = NormalizeDouble(MarketInfo(symbol, MODE_BID), digits);
 double ask = NormalizeDouble(MarketInfo(symbol, MODE_ASK), digits);
 
 if(MathAbs(bid - new_bid) < DBL_EPSILON && MathAbs(ask - new_ask) < DBL_EPSILON) {
   return true;
 }
 
 logger.log("tick not updated (" + (string)bid + ", " + (string)ask + ") -> (" + DoubleToStr(new_bid, digits) + ", " + DoubleToStr(new_ask, digits) + ") ", LOG_TYPE_HIGH, __LINE__, __FUNCTION__, true);
 return false;
}

bool TestServer::_CheckTickUpdated(string symbol, double new_bid, double new_ask)
{
 double bid = MarketInfo(symbol, MODE_BID);
 double ask = MarketInfo(symbol, MODE_ASK);
 
 if(MathAbs(bid - new_bid) < DBL_EPSILON && MathAbs(ask - new_ask) < DBL_EPSILON)
   return true;
   
 return false;
}