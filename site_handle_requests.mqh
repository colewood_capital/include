//+------------------------------------------------------------------+
//|                                         site_handle_requests.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#ifndef BASEDIR
#define BASEDIR
   input string BASE_DIR = "";
#endif

#ifndef FILEMESSENGER
#define FILEMESSENGER   
   #include <FileMessenger.mqh>
#endif 

#ifndef CONTEXT
#define CONTEXT
   #include <Context.mqh>
#endif 

#include <utils2.mqh>

string SERVER_SYNC_PATH = BASE_DIR + "\\server\\queue";
const int MILLISECOND_SET_TIMER = 300;

FileMessenger messenger(SERVER_SYNC_PATH);

Context request(GetPointer(messenger));

ENUM_INIT_RETCODE Init()
{

//--- create folder for python server
   FolderCreate(SERVER_SYNC_PATH);
   
   
   return(INIT_SUCCEEDED);
}

void Timer()
{
 //---- UPDATE PYTHON TRADES -----------------------------------------|
 string client_request, client_args;
 do
 {
  client_request = messenger.get_request(client_args);
  
  if(client_request=="")
   continue;
  else
   request.serve(client_request, client_args);
  
  GlobalVariableSet("refresh_disable_panel", 1);
 }
 while(messenger.next());
 
 END_EVENT_LOOP
}

void DeInit(const int reason)
{

}