//+------------------------------------------------------------------+
//|                                                     DllUtils.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

//An attempt at resetting the chart for all time frames simultaneously. However, realized that it is
//not urgent

#define DLL_LOGGER_INIT(name)        string path = TerminalInfoString(TERMINAL_DATA_PATH) + "\\MQL4\\Libraries\\" + name + ".log"; \
                                     StringReplace(path, "\\", "\\\\"); \
                                     LoggerInit(path)


#define LOGGER_DEPRECATED                
#include <Map.mqh>

struct SYSTEMTIME
{
 unsigned short wYear;
 unsigned short wMonth;
 unsigned short wDayOfWeek;
 unsigned short wDay;
 unsigned short wHour;
 unsigned short wMinute;
 unsigned short wSecond;
 unsigned short wMilliseconds;
};
                              
#import "Utils.dll"
   void LoggerInit(string full_log_path);
   bool CFileDelete(string name);
   void CFileLastWriteTime(string full_path, SYSTEMTIME& utctime);
#import

//+------------------------------------------------------------------+
bool DeleteHistoryFile(string symbol)
{
  bool ret_val = true;
  
 //--- close all charts and take a count of which charts were closed
  Map<ENUM_TIMEFRAMES,int> count;
 
  _ChartsCloseAll(symbol, count);
  
  ENUM_TIMEFRAMES key[];
  int val[];
  count.copy(key, val);
  int size = ArraySize(key);
 
 //--- sleep to all .hst file to be updated
  Sleep(12000);
 
 //--- 
 
 for(int i=0; i<size; i++)
 {
  int period = (int)key[i];
  string filename = symbol + (string)period + ".hst";
 
  string path = TerminalInfoString(TERMINAL_DATA_PATH) + "\\history\\" + AccountServer() + "\\" + filename;
  StringReplace(path, "\\", "\\\\");
 
  datetime last_modified = FileLastWriteTime(path);
  bool first_iteration = true;
  int attempts = 0;
 
  while(attempts < 5)
  {
   if(_DeleteHistoryFile(symbol, path, last_modified))
    break;
 
   Sleep(3000);
   attempts++;
  }
 
  if(attempts==5) {
   Alert("ERROR: DeleteHistoryFile(): Could not confirm ", filename, " has been deleted");
   ret_val = false;
  }
 }
 
 return ret_val;
}
//+------------------------------------------------------------------+
bool _DeleteHistoryFile(string symbol, string path, datetime last_modified)
{
  if(!IsConnected()) {
   Alert("ERROR: _DeleteHistoryFile(): not conncected to server");
   return true;
  }
 
  if(Symbol()==symbol) {
   Alert("ERROR: _DeleteHistoryFile(): cannot run script on (", symbol, ")");
   return true;
  }

  if(MathAbs(FileLastWriteTime(path) - last_modified) < DBL_EPSILON)
   return false;

  if(!CFileDelete(path))
   Alert("ERROR: _DeleteHistoryFile(): could not delete " + path);
  
  return true;
}
//+------------------------------------------------------------------+
void _ChartsCloseAll(string symbol, Map<ENUM_TIMEFRAMES,int>& count)
{
 long chart_id = ChartFirst();
 
 do {
   if(ChartSymbol(chart_id)==symbol) {
    ChartClose(chart_id);
    
    ENUM_TIMEFRAMES period = ChartPeriod(chart_id);
    int n = count[period];
    count.set(period, n+1);
   }
    
   chart_id = ChartNext(chart_id);
  } 
  while(chart_id >= 0);
}
//+------------------------------------------------------------------+
datetime FileLastWriteTime(string full_path)
{
 SYSTEMTIME s;
 
 CFileLastWriteTime(full_path, s);
 
 MqlDateTime t;
 t.day = s.wDay;
 t.hour = s.wHour;
 t.min = s.wMinute;
 t.sec = s.wSecond;
 t.mon = s.wMonth;
 t.year = s.wYear;
 
 return StructToTime(t);
} 
