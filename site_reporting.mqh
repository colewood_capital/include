//+------------------------------------------------------------------+
//|                                               site_reporting.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#define IPADDRESS

#ifndef LOGGER
#define LOGGER
   #define LOGGER_DEPRECATED
   #include <Logger.mqh>
#endif 

#ifndef AUTOSAVEARRAYSTRING
#define AUTOSAVEARRAYSTRING
   #include <Arrays/AutoSaveArrayString.mqh>
#endif 

#ifndef AUTOSAVEARRAYINT
#define AUTOSAVEARRAYINT
   #include <Arrays/AutoSaveArrayInt.mqh>
#endif

#ifndef AUTOSAVEARRAYDOUBLE
#define AUTOSAVEARRAYDOUBLE
   #include <Arrays/AutoSaveArrayDouble.mqh>
#endif

#ifndef AUTOSAVEARRAYDATETIME
#define AUTOSAVEARRAYDATETIME
   #include <Arrays/AutoSaveArrayDatetime.mqh>
#endif

#ifndef AUTOSAVEORDEREDBIN
#define AUTOSAVEORDEREDBIN
   #include <Arrays/AutoSaveOrderedBin.mqh>
#endif

#ifndef MT4GUI2
#define MT4GUI2
   #include <mt4gui2.mqh>
#endif 

#ifndef UTILS
#define UTILS
   #include <utils.mqh>
#endif

#ifndef UTILS2
#define UTILS2
   #include <utils2.mqh>
#endif

#ifndef BASEDIR
#define BASEDIR
   input string BASE_DIR = "";
#endif

#define OK_200         200 

class EquityCandle
{
 private:
  string m_timestamp;
  
 public:
  double high;
  double low;
  double open;
  double close;
  string GetTimestamp() { return m_timestamp; }
  void reset();
  
  EquityCandle() { reset(); }
};

class OrderQueue
{
private:
 AutoSaveArrayInt* m_booknum;
 AutoSaveArrayDouble* m_swaps;
 AutoSaveArrayDouble* m_profit; 
 AutoSaveArrayDatetime* m_min_tstamp;
 AutoSaveArrayDouble* m_min;
 AutoSaveArrayDatetime* m_max_tstamp;
 AutoSaveArrayDouble* m_max;
 AutoSaveOrderedBin* m_bin;
 int m_bookmark;
 
public:
 void add(int booknum, double swaps, double profit, double min, double max);
 bool update(int booknum, double swaps, double profit);
 void remove(int ticket, datetime& min_profit_tstamp, double& min_profit, datetime& max_profit_tstamp, double& max_profit);
 void checkbackfill();
 AutoSaveArrayInt* getTrades() { return GetPointer(m_booknum); }
 OrderQueue();
 ~OrderQueue();
};

// global variables
input string IP_ADDRESS = "http://127.0.0.1";
string FILENAME_BACKLOG = "Equity_Candlesticks_Backlog.txt";
input string URL_LOGIN = "/accounts/login";
input string URL_SET_EQUITY = "/accounts/setequity";
input string URL_SET_CLOSED_TRADES = "/trades/set";
input string URL_SET_UPDATE_CLOSED_TRADES = "/trades/set";
input string URL_GET_ALL_BOOKNUMS = "/trades/all";
input string URL_GET_JOBS = "/jobs";
input double DOLLAR_PL_UPDATE_THRESHOLD = 0;
const int MILLISECOND_SET_TIMER = 300;
string FILE_DELIMITER = "\n";
string QUEUE_SYNC_PATH = BASE_DIR + "\\queue";

AutoSaveArrayString* backlog;
EquityCandle* candle;
Logger logger(__FILE__, "helper_processes");
OrderQueue market;

int primary_key;
string job_ids;

#ifndef JOBS
#define JOBS
   #include <Jobs.mqh>
#endif 

// global variables - disable trading GUI
int hwnd = 0; 
int panel1,display_panel,symbol_list,add_button,remove_button;
int symbols_market_watch_total;
AutoSaveArrayStringCommunity disable_trading_symbol("disable_trading_symbol");

struct MyTicket
{
 int booknum;
 int cmd;
 double open_price;
 datetime open_time;
 string symbol;
 string cmt;
};

ENUM_INIT_RETCODE Init()
{
  //--- pointer initialization
   backlog = new AutoSaveArrayString(FILENAME_BACKLOG);
   
  //--- login to server or create a new account
   string headers;
   char post[], result[];

   string data = "acctNumber=" + (string)AccountNumber() + 
                 ";acctServer=" + GetServerName() + 
                 ";acctName=" + AccountName() +
                 ";acctType=" + AccountType() +
                 ";pcName=" + GetMachineName() + 
                 ";baseDir=" + BASE_DIR + 
                 ";terminalPath=" + TerminalInfoString(TERMINAL_PATH) +
                 ";terminalDataPath=" + TerminalInfoString(TERMINAL_DATA_PATH) + 
                 ";terminalCommonDataPath=" + TerminalInfoString(TERMINAL_COMMONDATA_PATH) + ";";
                 
   string response;
   
   if(BASE_DIR=="")
   {
    string msg = "Init failed for BASE_DIR not set!!!";
    MessageBox(msg, "CRITICAL ERROR", MB_OK|MB_ICONERROR);
    logger.log(msg, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
    return(INIT_FAILED);
   }
   
   ResetLastError();

   StringToCharArray(data, post, 0, WHOLE_ARRAY, CP_UTF8);
   
   int res = WebRequest("POST", IP_ADDRESS + URL_LOGIN, "", NULL, 10000, post, ArraySize(post), result, headers);
   
   if(res!=OK_200) {
    logger.log(StringConcatenate("Error: ", GetLastError()), LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
    return(INIT_FAILED);
   }
   
   response = CharArrayToString(result);
   string message[];
   StringReplace(response,"\"","");
   StringSplit(response,';',message);
   
   logger.log(StringConcatenate("Web server response code: " , res), LOG_TYPE_INFO, __LINE__, __FUNCTION__);
   logger.log(StringConcatenate("Web server response: ", message[0]), LOG_TYPE_INFO, __LINE__, __FUNCTION__);
   
   if(ArraySize(message) < 2) {
    string msg = "Init failed for did not receive inital response from server!!!";
    MessageBox(msg, "CRITICAL ERROR", MB_OK|MB_ICONERROR);
    logger.log(msg, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
    return(INIT_PARAMETERS_INCORRECT);
   }
  
   primary_key = (int)message[1];
   logger.log(StringConcatenate("Acquired pk = ", primary_key), LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
   
   char get[];
   res = WebRequest("GET", StringConcatenate(IP_ADDRESS, URL_GET_ALL_BOOKNUMS, "/", primary_key),
               "", NULL, 10000, get, ArraySize(get), result, headers);
   
   if(res!=OK_200) {
    string msg = "Init failed for unable to fetch booknums from server!!!";
    MessageBox(msg, "CRITICAL ERROR", MB_OK|MB_ICONERROR);
    logger.log(msg, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
    return(INIT_PARAMETERS_INCORRECT);
   }            
  
   response = CharArrayToString(result);
   
   string db_snapshot[];
   StringSplit(response,';',db_snapshot);
  
   int ret_size = ArraySize(db_snapshot);
   if(ret_size > 0)
   {
    BackFill(db_snapshot);
    logger.log("db_booknum[first]: " + db_snapshot[0], LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
    logger.log("db_booknum[last]: " + db_snapshot[ret_size-1], LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
   }
   logger.log("db_booknum[size] = " + (string)ret_size, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
    
   candle = new EquityCandle();
   
   DisableGUIinit();
   
   FetchJob(job_ids);
   
   return(INIT_SUCCEEDED);
}

void Timer()
{
//---- EQUITY CANDLESTICKS ------------------------------------------|
   double equity = AccountEquity();
   
   if(equity < candle.low)    
    candle.low = equity;
      
   if(equity > candle.high)   
    candle.high = equity;
   
   BackLogCleanUp();
   
   if(NewMinuteCandle())
   {
    POSTEquity(candle.open, candle.low, candle.high, equity, OpenLots());
    candle.reset();
   }
   
//---- CLOSED TRADES ------------------------------------------------|  
 //--- check for new opens  
   int orders_total=OrdersTotal();
   
   for(int i=0; i<orders_total; i++)
    {
     if(OrderSelect(i,SELECT_BY_POS))
      {
       int ticket = OrderTicket();
       int type = OrderType();
       
       switch(type)
        {
         case 0:
         case 1: if(market.update(ticket, OrderSwap(), OrderProfit()))
                 {
                  POSTTradeOpen(ticket, OrderSymbol(), type, OrderLots(),
                                 OrderOpenTime(), OrderOpenPrice());
                  logger.log("Opening #"+(string)ticket , LOG_TYPE_INFO, __LINE__, __FUNCTION__);
                 }
                 break;
        }
      }
    }
    
  //--- check for new closes
    AutoSaveArrayInt* trades = market.getTrades();
    int trades_total = trades.Total();
    
    for(int i=0; i<trades_total; i++)
    {
     if(OrderSelect(trades[i],SELECT_BY_TICKET))
     {
      if(OrderCloseTime() > 0)
      {
       int ticket = OrderTicket();
       datetime min_tstamp, max_tstamp;
       double min_profit, max_profit;
       market.remove(ticket, min_tstamp, min_profit, max_tstamp, max_profit);
        
       POSTTradeClose(ticket, OrderSymbol(), OrderStopLoss(), OrderTakeProfit(), 
         OrderCloseTime(), OrderClosePrice(), OrderCommission(), OrderSwap(), OrderComment(),
         min_tstamp, min_profit, max_tstamp, max_profit, OrderProfit());
       
       logger.log("Closing #"+(string)ticket , LOG_TYPE_INFO, __LINE__, __FUNCTION__);
      }
     }
    }
    
    //market.checkbackfill();
    
//---- UPDATE CLOSED TRADES -----------------------------------------|  
//---- check for new files with update content 
    POSTUpdateTrade();
    POSTUpdateCloseBy();
    
//---- UPDATE DISABLE TRADING GUI------------------------------------|
    DisableGUIengine();

//---- UPDATE JOBS --------------------------------------------------|
    string id;
    if(CheckJobStatus(id)) {
     string headers;
     char post[], result[];
     string msg = "ids=" + (string)id + ";result=completed;";
     StringToCharArray(msg, post, 0, WHOLE_ARRAY, CP_UTF8);
     int response = WebRequest("POST", StringConcatenate(IP_ADDRESS, URL_GET_JOBS, "/set/")
                                    , "", NULL, 8000, post, ArraySize(post), result, headers);
     if(response == OK_200) {
       logger.log("job server response: " + (string)response, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
       job_ids = NULL;
       cmt_jobs = NULL;
     }
     else {
       response = (response < 0) ? GetLastError() : response;
       logger.log("job server response: " + (string)response, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
      }
     
     ResetLastJob();
    }
    
   if(GlobalVariableGet("fetch_new_job") == -1) {
    string res[];
    FetchJob(job_ids);
    
    int pos = StringSplit(job_ids,',',res);
    if(pos > 0) {
     int fetch_id = (int)res[0];
     if(MathIsValidNumber(fetch_id))
      GlobalVariableSet("fetch_new_job", fetch_id);
     else {
      logger.log("Invalid id=" + (string)fetch_id + ", job_ids=" + job_ids, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
      GlobalVariableSet("fetch_new_job", 0);
     }
    }
    else
     GlobalVariableSet("fetch_new_job", 0);
   }

//---- ASSERTION ----------------------------------------------------| 
    if(OrdersTotal() > 99)
     logger.log("ASSERTION max trades < 100 failed !!!", LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
  
//---- Update last update time
    Comment(StringConcatenate("Last Update: ",TimeLocal(),"\nJob ID(s): ", job_ids, "\n", cmt_jobs,"\n"));
    GlobalVariableSet(GetEAName(__FILE__) + "_last_update", TimeCurrent());
}

//+------------------------------------------------------------------+
void EquityCandle::reset()
{
  double equity = AccountEquity();
  
 //--- set new timestamp
  m_timestamp = SQLTimeCurrent();
  
 //--- reset candle
  open = equity;
  high = equity;
  low = equity;
  close = equity;
}
//+------------------------------------------------------------------+
bool POSTUpdateCloseBy()
 {
   string filename;
   int    i=1;
  
//--- receive search handle in local folder's root
   string filepath = StringConcatenate(QUEUE_SYNC_PATH + "\\*" + "_closebyupdate" + "*.csv");
  
   long search_handle=FileFindFirst(filepath,filename);
   
   bool reset_file_handler = false;
   
//--- check if FileFindFirst() function executed successfully
   if(search_handle!=INVALID_HANDLE)
     {
      bool all_updated = true;
      
     //--- check if the passed strings are file or directory names in the loop
      do
        {
         if(reset_file_handler) {
          search_handle=FileFindFirst(filepath,filename);
          reset_file_handler=false;
         } 
      
       //--- Append directory to filename
         filename = StringConcatenate(QUEUE_SYNC_PATH,"\\",filename);
         
         ResetLastError();
         
       //--- if this is a file, then send to webrequest
         if(FileIsExist(filename))
          {
           int handle = FileOpen(filename, FILE_READ|FILE_CSV);
           string query = "";
            
           if(handle>=0)
            {
             bool send=false;
             MyTicket a, b;
             
             while(!FileIsEnding(handle))
             {
              string value = FileReadString(handle);
              string res[];
                
              StringSplit(value,'=',res);                                   //Expecting format "key=value" from each line in file
                
              if(res[0]=="booknum") 
              {
               a.booknum=(int)res[1];
               if(OrderSelect(a.booknum,SELECT_BY_TICKET))
               {
                a.cmd = OrderType();
                a.cmt = OrderComment();
                a.open_price = OrderOpenPrice();
                a.open_time = OrderOpenTime();
                a.symbol = OrderSymbol();
               }
               continue;
              }
              
              if(res[0]=="opposite_booknum") 
              {
               b.booknum=(int)res[1];
               if(OrderSelect(b.booknum,SELECT_BY_TICKET))
               {
                b.cmd = OrderType();
                b.cmt = OrderComment();
                b.open_price = OrderOpenPrice();
                b.open_time = OrderOpenTime();
                b.symbol = OrderSymbol();
               }
               continue;
              }
                
              if(res[0]=="upload" && res[1]=="TRUE") {
               send=true;
               continue;
              }
                   
              query = StringConcatenate(query,res[0],"=",res[1],";");                 //Add value of each line in file to query
             }

             FileClose(handle);
             
            if(a.booknum > 0 && b.booknum > 0)
             FindMatchBuildQuery(a, b, query); 
  
            if(send && StringLen(query) > 0) {
               string headers;
               char post[], result[];
            
               StringToCharArray(query, post, 0, WHOLE_ARRAY, CP_UTF8);
               logger.log("Send to server: " + query, LOG_TYPE_INFO, __LINE__, __FUNCTION__);
               int response = WebRequest("POST", IP_ADDRESS + URL_SET_UPDATE_CLOSED_TRADES, "", NULL, 3000, post, ArraySize(post), result, headers);
  
               if(response==OK_200)
               {
                FileDelete(filename);
                reset_file_handler = true;
                continue;
               } 
               else 
               {
                logger.log(StringConcatenate("MQL Error: ", GetLastError()), LOG_TYPE_LOW, __LINE__, __FUNCTION__);
                logger.log(StringConcatenate("Web server response code: ", response), LOG_TYPE_LOW, __LINE__, __FUNCTION__);
                logger.log(StringConcatenate("Server response: ", CharArrayToString(result)), LOG_TYPE_LOW, __LINE__, __FUNCTION__);
               }  
            }
            
            all_updated = false;
          }
        } 
       //---- Increment index: open next file 
        i++;
   
     }//end do
     while(FileFindNext(search_handle,filename));
     
     //--- close search handle
       FileFindClose(search_handle);
 
     return(all_updated);     
    }
     
   return(true);        //No files exist, so DB is already up-to-date
 }
//+------------------------------------------------------------------+
bool POSTUpdateTrade()
 {
   string filename;
   int    i=1;
  
//--- receive search handle in local folder's root
   string filepath = StringConcatenate(QUEUE_SYNC_PATH + "\\*" + "_update" + "*.csv");
   
   long search_handle=FileFindFirst(filepath,filename);
   
   bool reset_file_handler = false;
   
//--- check if FileFindFirst() function executed successfully
   if(search_handle!=INVALID_HANDLE)
     {
      bool all_updated = true;
      
     //--- check if the passed strings are file or directory names in the loop
      do
        {
         if(reset_file_handler) {
          search_handle=FileFindFirst(filepath,filename);
          reset_file_handler=false;
         } 
      
       //--- Append directory to filename
         filename = StringConcatenate(QUEUE_SYNC_PATH,"\\",filename);
         
         ResetLastError();
         
       //--- if this is a file, then send to webrequest
         if(FileIsExist(filename))
          {
           int handle = FileOpen(filename, FILE_READ|FILE_CSV);
           string query = "";
           int booknum = -1;
            
           if(handle>=0)
            {
             bool send=false;
             
             while(!FileIsEnding(handle))
             {
              string value = FileReadString(handle);
              string res[];
                
              StringSplit(value,'=',res);                                   //Expecting format "key=value" from each line in file
                
              if(res[0]=="upload" && res[1]=="TRUE") {
               send=true;
               continue;
              }
              
              if(res[0]=="booknum")
               booknum = (int)res[1];
                   
              query = StringConcatenate(query,res[0],"=",res[1],";");                 //Add value of each line in file to query
             }
               
             FileClose(handle);
               
            if(send) {
               string headers;
               char post[], result[];
            
               StringToCharArray(query, post, 0, WHOLE_ARRAY, CP_UTF8);
   
               int response = WebRequest("POST", IP_ADDRESS + URL_SET_UPDATE_CLOSED_TRADES, "", NULL, 3000, post, ArraySize(post), result, headers);
  
               if(response==OK_200)
               {
                FileDelete(filename);
                reset_file_handler = true;
                continue;
               } 
               else 
               {
                logger.log(StringConcatenate("File: ", filename, " in ", __FUNCTION__), LOG_TYPE_LOW, __LINE__, __FUNCTION__);
                string res = CharArrayToString(result);
                logger.log(StringConcatenate("MQL Error: ", GetLastError()), LOG_TYPE_LOW, __LINE__, __FUNCTION__);
                logger.log(StringConcatenate("Web server response code: ", response), LOG_TYPE_LOW, __LINE__, __FUNCTION__);
                logger.log(StringConcatenate("Server response: ", res), LOG_TYPE_LOW, __LINE__, __FUNCTION__);
    
                if(StringFind(res, "matching query does not exist") >= 0 && booknum > 0 && OrderSelect(booknum,SELECT_BY_TICKET))
                 POSTTradeOpen(booknum, OrderSymbol(), OrderType(), OrderLots(), OrderOpenTime(), OrderOpenPrice());
               }  
            }
            
            all_updated = false;
          }
        } 
       //---- Increment index: open next file 
        i++;
   
     }//end do
     while(FileFindNext(search_handle,filename));
     
     //--- close search handle
       FileFindClose(search_handle);
 
     return(all_updated);     
    }
     
   return(true);        //No files exist, so DB is already up-to-date
 }
void POSTTradeClose(int booknum, 
                    string symbol,
                    double sl, 
                    double tp, 
                    datetime close_time, 
                    double close_price,
                    double commission, 
                    double swaps, 
                    string cmt, 
                    datetime min_profit_tstamp, 
                    double min_profit,
                    datetime max_profit_tstamp, 
                    double max_profit, 
                    double profit)
{
  int digits = (int)MarketInfo(symbol, MODE_DIGITS);
 
 //--- Attempt 1: post equity to server
  string headers;
  char post[], result[];
  string data = "acctNumber=" + (string)AccountNumber() + ";acctServer=" + GetServerName();
  data += ";booknum=" + IntegerToString(booknum);
  data += ";sl=" + DoubleToStr(sl, digits);
  data += ";tp=" + DoubleToStr(tp, digits);
  data += ";close_time=" + SQLTime(close_time);
  data += ";close_price=" + DoubleToStr(close_price, digits);
  data += ";close_reason=" + GetCloseReason(booknum);
  data += ";commission=" + DoubleToStr(commission, 2);
  data += ";swaps=" + DoubleToStr(swaps, 2);
  data += ";close_base_conversion=" + (string)SymbolInfoDouble(symbol, SYMBOL_TRADE_TICK_VALUE);
  data += ";close_comment=" + cmt;
  
  if(min_profit_tstamp > 0) {
   data += ";min_profit_tstamp=" + SQLTime(min_profit_tstamp);
   data += ";min_profit=" + DoubleToStr(min_profit, 2);
  }
  if(max_profit_tstamp > 0) {
   data += ";max_profit_tstamp=" + SQLTime(max_profit_tstamp);
   data += ";max_profit=" + DoubleToStr(max_profit, 2);
  }
  
  data += ";profit=" + DoubleToStr(profit, 2);
  data += ";";
  
  StringToCharArray(data, post, 0, WHOLE_ARRAY, CP_UTF8);
   
  int response = WebRequest("POST", IP_ADDRESS + URL_SET_UPDATE_CLOSED_TRADES, "", NULL, 3000, post, ArraySize(post), result, headers);
  
  if(response==OK_200)
   return; 
   
  logger.log(StringConcatenate("MQL Error: ", GetLastError()), LOG_TYPE_LOW, __LINE__, __FUNCTION__);
  logger.log(StringConcatenate("Web server response code: ", response), LOG_TYPE_LOW, __LINE__, __FUNCTION__);
  logger.log(StringConcatenate("Server response: ", CharArrayToString(result)), LOG_TYPE_LOW, __LINE__, __FUNCTION__);
  
 //--- Attempt 2: save to auto save array
 // if(backlog.Add(data))
  // return;
  
 //--- Error message
  logger.log(StringConcatenate("Unable to post #", booknum," to server nor save to file"), LOG_TYPE_LOW, __LINE__, __FUNCTION__);
  logger.log(data, LOG_TYPE_LOW, __LINE__, __FUNCTION__);
  return;
}
//+------------------------------------------------------------------+
void POSTTradeOpen(int booknum, string symbol, int cmd, double lots, datetime open_time, double open_price)
{
  int digits = (int)MarketInfo(symbol, MODE_DIGITS);

 //--- Attempt 1: post equity to server
  string headers;
  char post[], result[];
  string data = "acctNumber=" + (string)AccountNumber() + ";acctServer=" + GetServerName();
  data += ";booknum=" + IntegerToString(booknum);
  data += ";symbol=" + symbol;
  
  switch(cmd) {
   case OP_BUY:  data += ";type=BUY"; break;
   case OP_SELL: data += ";type=SELL"; break;
  }
  
  data += ";lots=" + DoubleToStr(lots, 2);
  data += ";open_time=" + SQLTime(open_time);
  data += ";open_price=" + DoubleToStr(open_price, digits);
  data += ";close_base_conversion=" + (string)SymbolInfoDouble(symbol, SYMBOL_TRADE_TICK_VALUE);
  data += ";";
  
  StringToCharArray(data, post, 0, WHOLE_ARRAY, CP_UTF8);
   
  int response = WebRequest("POST", IP_ADDRESS + URL_SET_CLOSED_TRADES, "", NULL, 3000, post, ArraySize(post), result, headers);
  
  if(response==OK_200)
   return; 
  
  string res = CharArrayToString(result);
  logger.log(StringConcatenate("MQL Error: ", GetLastError()), LOG_TYPE_LOW, __LINE__, __FUNCTION__);
  logger.log(StringConcatenate("Web server response code: ", response), LOG_TYPE_LOW, __LINE__, __FUNCTION__);
  logger.log(StringConcatenate("Server response: ", res), LOG_TYPE_LOW, __LINE__, __FUNCTION__);
  
 //--- Attempt 2: save to auto save array
 // if(backlog.Add(data))
  // return;
  
 //--- Error message
  if(StringFind(res, "Duplicate entry") >= 0)
   return;
   
  logger.log(StringConcatenate("Critical error: unable to post #", booknum," to server nor save to file"), LOG_TYPE_LOW, __LINE__, __FUNCTION__);
  logger.log(data, LOG_TYPE_LOW, __LINE__, __FUNCTION__);
  return;
}
//+------------------------------------------------------------------+
void POSTEquity(double open, double low, double high, double close, double volume)
{
 //--- Attempt 1: post equity to server
  string headers;
  char post[], result[];
  string data = "acctNumber=" + (string)AccountNumber() + ";acctServer=" + GetServerName();
  data += ";timestamp=" + SQLTimeCurrent();
  data += ";openLots=" + DoubleToStr(volume, 2);
  data += ";equityOpen=" + (string)round(open) + ";equityLow=" + (string)round(low) + ";equityHigh=" + (string)round(high) + ";equityClose=" + (string)round(close) + ";";
  
  StringToCharArray(data, post, 0, WHOLE_ARRAY, CP_UTF8);
   
  int response = WebRequest("POST", IP_ADDRESS + URL_SET_EQUITY, "", NULL, 3000, post, ArraySize(post), result, headers);
  
  if(response==OK_200)
   return; 
   
  logger.log(StringConcatenate("MQL Error: ", GetLastError()), LOG_TYPE_LOW, __LINE__, __FUNCTION__);
  logger.log(StringConcatenate("Web server response code: ", response), LOG_TYPE_LOW, __LINE__, __FUNCTION__);
  logger.log(StringConcatenate("Server response: ", CharArrayToString(result)), LOG_TYPE_LOW, __LINE__, __FUNCTION__);
  
 //--- Attempt 2: save to auto save array
  if(backlog.Add(data))
   return;
  
 //--- Error message
  logger.log("site_reporting - Critical error: unable to post equity to server nor save to file", LOG_TYPE_LOW, __LINE__, __FUNCTION__);
  logger.log(data, LOG_TYPE_LOW, __LINE__, __FUNCTION__);
  return;
}
//+------------------------------------------------------------------+
void BackLogCleanUp()
{
 string headers;
 char post[], result[];
 
 for(int i=0; i<backlog.Total(); i++)
 {
  string data = backlog[i];
  
  StringToCharArray(data, post, 0, WHOLE_ARRAY, CP_UTF8); 
  
  int response = WebRequest("POST", IP_ADDRESS + URL_SET_EQUITY, "", NULL, 3000, post, ArraySize(post), result, headers);
  
  if(response==OK_200)
  {
   backlog.Delete(i);
   i--;
   ArrayFree(post);
   ArrayFree(result);
   continue;
  }
  
  break;
 }
}
//+------------------------------------------------------------------+
double OpenLots()
{
 int total = OrdersTotal();
 double lots = 0;
 
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(i,SELECT_BY_POS))
   lots += OrderLots();
 }
 
 return lots;
}
//+------------------------------------------------------------------+
bool NewMinuteCandle()
{
 static MqlDateTime prev_tstamp;
 MqlDateTime tstamp;
 
 TimeCurrent(tstamp);

 if(tstamp.sec < prev_tstamp.sec)
 {
  prev_tstamp = tstamp;
  return true;
 }
 
 prev_tstamp = tstamp;
 
 return false;
}
//+------------------------------------------------------------------+
string SQLTime(datetime time)
{
 string str_time = (string)time;
 
 StringReplace(str_time,".","-");
 
 return str_time;
}
//+------------------------------------------------------------------+
string SQLTimeCurrent()
{
 string time = (string)TimeCurrent();
 
 StringReplace(time,".","-");
 
 return time;
}
//+------------------------------------------------------------------+
void OrderQueue::checkbackfill()
{ 
 CArrayInt temp;
 int total;
 static bool clear = false;
 
 total = OrdersHistoryTotal();
 for(int i=total-1; i>=0; i--)
 {
  if(OrderSelect(i,SELECT_BY_POS,MODE_HISTORY))
  {
   if(OrderTicket() == m_bookmark)
    break;
   else
    temp.Add(OrderTicket());
  }
 }
 
 total = temp.Total();
 for(int i=total-1; i>=0; i--)
 {
  int ticket = temp[i];
  
  if(!m_bin.Search(ticket))
  {
   if(OrderSelect(ticket,SELECT_BY_TICKET))
   {
    logger.log(StringConcatenate("Backfilling: ", ticket), LOG_TYPE_INFO, __LINE__, __FUNCTION__);
    POSTTradeOpen(ticket, OrderSymbol(), OrderType(), OrderLots(), OrderOpenTime(), OrderOpenPrice()); 
    POSTTradeClose(ticket,
                   OrderSymbol(),
                   OrderStopLoss(),
                   OrderTakeProfit(),
                   OrderCloseTime(),
                   OrderClosePrice(),
                   OrderCommission(),
                   OrderSwap(),
                   OrderComment(),
                   0, 0, 0, 0,
                   OrderProfit());

    m_bin.Add(ticket);
    clear = false;
   }
  }
 }
 
 if(!clear)
 {
  int min = MathMax(m_bookmark, m_bin.Max());
  
  if(min != m_bookmark)
  {
   logger.log(StringConcatenate("Changing m_bookmark from ", m_bookmark, " to ", min), LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
   m_bookmark = min;
  }
  
  clear = true;
 }
}
//+------------------------------------------------------------------+
void OrderQueue::remove(int ticket,datetime& min_profit_tstamp,double& min_profit,datetime& max_profit_tstamp,double& max_profit)
{
 int pos = m_booknum.SearchLinear(ticket);
 string ret_str = NULL;
  
 if(pos >= 0)
  {
   min_profit_tstamp = m_min_tstamp[pos];
   min_profit = m_min[pos];
   max_profit_tstamp = m_max_tstamp[pos];
   max_profit = m_max[pos];
   
   m_booknum.Delete(pos);   
   m_swaps.Delete(pos);
   m_profit.Delete(pos); 
   m_min_tstamp.Delete(pos);
   m_min.Delete(pos);
   m_max_tstamp.Delete(pos);
   m_max.Delete(pos);
  }
}
//+------------------------------------------------------------------+
void OrderQueue::add(int booknum, double swaps, double profit, double min, double max)
{
  m_booknum.Add(booknum);
  
  int pos = m_booknum.SearchLinear(booknum);
  m_swaps.Insert(swaps, pos);
  m_profit.Insert(profit, pos);
  m_min_tstamp.Insert(TimeCurrent(),pos);
  m_min.Insert(min, pos);
  m_max_tstamp.Insert(TimeCurrent(),pos);
  m_max.Insert(max, pos);
  m_bin.Add(booknum);
  logger.log(StringConcatenate("inserting ", booknum), LOG_TYPE_INFO, __LINE__, __FUNCTION__);
}
//+------------------------------------------------------------------+
bool OrderQueue::update(int booknum, double swaps, double profit)
{
  int pos = m_booknum.SearchLinear(booknum);
 
  if(pos < 0)
   {
    m_booknum.Add(booknum);
    
    int idx = m_booknum.SearchLinear(booknum);
    m_swaps.Insert(swaps,idx);
    m_profit.Insert(profit,idx);
    m_min_tstamp.Insert(TimeCurrent(),idx);
    m_min.Insert(profit,idx);
    m_max_tstamp.Insert(TimeCurrent(),idx);
    m_max.Insert(profit,idx);
    m_bin.Add(booknum);
    return true;
   }
  else
   {
    if(MathAbs(m_profit[pos]-profit) > DOLLAR_PL_UPDATE_THRESHOLD) 
      m_profit.Update(pos, profit);
    
    if(MathAbs(m_swaps[pos]-swaps) > DBL_EPSILON) 
      m_swaps.Update(pos, swaps);
    
    if(profit > m_max[pos])
    {
     m_max_tstamp.Update(pos, TimeCurrent());
     m_max.Update(pos, profit);
    }
    
    if(profit < m_min[pos])
    {
     m_min_tstamp.Update(pos, TimeCurrent());
     m_min.Update(pos, profit);
    }
    
    return false;
   }
}
//+------------------------------------------------------------------+
OrderQueue::OrderQueue(void)
{
 m_booknum = new AutoSaveArrayInt("m_booknum.txt", __FILE__);
 m_swaps = new AutoSaveArrayDouble("m_swaps.txt");
 m_profit = new AutoSaveArrayDouble("m_profit.txt");
 m_min_tstamp = new AutoSaveArrayDatetime("m_min_tstamp.txt");
 m_min = new AutoSaveArrayDouble("m_min.txt");
 m_max_tstamp = new AutoSaveArrayDatetime("m_max_tstamp.txt");
 m_max = new AutoSaveArrayDouble("m_max.txt");
 m_bin = new AutoSaveOrderedBin("m_bin.txt", 15);
 
 if(OrderSelect(OrdersHistoryTotal()-1, SELECT_BY_POS, MODE_HISTORY))
  m_bookmark = OrderTicket();
  
 logger.log(StringConcatenate("m_bookmark: ", m_bookmark), LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
}
//+------------------------------------------------------------------+
OrderQueue::~OrderQueue(void)
{
 delete m_booknum;
 delete m_swaps;
 delete m_profit;
 delete m_min_tstamp;
 delete m_min;
 delete m_max_tstamp;
 delete m_max;
 delete m_bin;
}
//+------------------------------------------------------------------+
string GetCloseReason(int ticket)
{
 if(OrderSelect(ticket,SELECT_BY_TICKET))
  {
   string cmt = OrderComment();

   //--- search comments for close reason
    if(StringFind(cmt,"[sl]") >= 0)
     return("SL");
      
    if(StringFind(cmt,"[tp]") >= 0)
     return("TP");
       
    if(StringFind(cmt,"so:") >= 0)
     return("S.O.");
       
    if(StringFind(cmt,"to #") >= 0)
     return("Partial");
  }
  
 return(NULL);
}
//+------------------------------------------------------------------+
bool FindMatchBuildQuery(MyTicket& t1, MyTicket& t2, string& query)
{
 int max_booknum = MathMax(t1.booknum, t2.booknum);
 int total;

 //--- check history queue
 total = OrdersHistoryTotal();
 for(int k=total-1; k>=0; k--)
 {
  if(OrderSelect(k,SELECT_BY_POS,MODE_HISTORY))
  { 
   int new_ticket = OrderTicket();
   string comment = OrderComment();

   if(new_ticket == max_booknum)
   {
    for(int n=k+1; n<total; n++)
    {
     if(OrderSelect(n,SELECT_BY_POS,MODE_HISTORY))
     { 
      if(OrderTicket()!=t1.booknum &&
         OrderType()==t1.cmd &&
         OrderSymbol()==t1.symbol &&
         OrderOpenPrice()==t1.open_price &&
         OrderOpenTime()==t1.open_time &&
      (StringFind(comment,"from #") >=0 || StringFind(comment,"partial close") >= 0 || StringFind(comment,"close hedge by") >= 0))
      {
       query = StringConcatenate(query,"acctNumber=",AccountNumber(),";");
       query = StringConcatenate(query,"acctServer=",GetServerName(),";");
       query = StringConcatenate(query,"booknum=",OrderTicket(),";");
       query = StringConcatenate(query,"parent_booknum=",t1.booknum,";");
       query = StringConcatenate(query,"parent_login=",AccountNumber(),";");
       query = StringConcatenate(query,"parent_server=",GetServerName(),";");
       return true;
      }

      if(OrderTicket()!=t2.booknum &&
         OrderType()==t2.cmd &&
         OrderSymbol()==t2.symbol &&
         OrderOpenPrice()==t2.open_price &&
         OrderOpenTime()==t2.open_time &&
         (StringFind(comment,"from #") >=0 || StringFind(comment,"partial close") >= 0 || StringFind(comment,"close hedge by") >= 0))
      { 
       query = StringConcatenate(query,"acctNumber=",AccountNumber(),";");
       query = StringConcatenate(query,"acctServer=",GetServerName(),";");
       query = StringConcatenate(query,"booknum=",OrderTicket(),";");
       query = StringConcatenate(query,"parent_booknum=",t2.booknum,";");
       query = StringConcatenate(query,"parent_login=",AccountNumber(),";");
       query = StringConcatenate(query,"parent_server=",GetServerName(),";");
       return true;
      }
     }
    }//end if
    break;
   }//end if
  }//end for
 }
 
 //--- check order queue
 total = OrdersTotal();
 for(int k=total-1; k>=0; k--)
 {
  if(OrderSelect(k,SELECT_BY_POS))
  {
   int new_ticket = OrderTicket();
   string comment = OrderComment();
   
   if(OrderTicket()!=t1.booknum &&
      OrderType()==t1.cmd &&
      OrderSymbol()==t1.symbol &&
      OrderOpenPrice()==t1.open_price &&
      OrderOpenTime()==t1.open_time &&
   (StringFind(comment,"from #") >=0 || StringFind(comment,"partial close") >= 0 || StringFind(comment,"close hedge by") >= 0))
   {
    query = StringConcatenate(query,"acctNumber=",AccountNumber(),";");
    query = StringConcatenate(query,"acctServer=",GetServerName(),";");
    query = StringConcatenate(query,"booknum=",OrderTicket(),";");
    query = StringConcatenate(query,"parent_booknum=",t1.booknum,";");
    query = StringConcatenate(query,"parent_login=",AccountNumber(),";");
    query = StringConcatenate(query,"parent_server=",GetServerName(),";");
    return true;
   }
   
   if(OrderTicket()!=t2.booknum &&
      OrderType()==t2.cmd &&
      OrderSymbol()==t2.symbol &&
      OrderOpenPrice()==t2.open_price &&
      OrderOpenTime()==t2.open_time &&
   (StringFind(comment,"from #") >=0 || StringFind(comment,"partial close") >= 0 || StringFind(comment,"close hedge by") >= 0))
   {
    query = StringConcatenate(query,"acctNumber=",AccountNumber(),";");
    query = StringConcatenate(query,"acctServer=",GetServerName(),";");
    query = StringConcatenate(query,"booknum=",OrderTicket(),";");
    query = StringConcatenate(query,"parent_booknum=",t2.booknum,";");
    query = StringConcatenate(query,"parent_login=",AccountNumber(),";");
    query = StringConcatenate(query,"parent_server=",GetServerName(),";");
    return true;
   }
  }    
 }//end for
 
 return false;
} 
//+------------------------------------------------------------------+
void BackFill(const string& booknum[])
{
 CArrayInt db;
 int total=ArraySize(booknum);
 for(int i=0; i<total; i++)
  db.Add((int)booknum[i]);
 
 db.Sort();
 
 int orders_total = OrdersTotal();
 
 for(int i=0; i<orders_total; i++)
 {
  if(OrderSelect(i,SELECT_BY_POS))
  {
   int ticket = OrderTicket();
   
   if(db.SearchLinear(ticket) < 0) {
    POSTTradeOpen(ticket, OrderSymbol(), OrderType(), OrderLots(), OrderOpenTime(), OrderOpenPrice());
    logger.log(StringConcatenate("Open #", ticket), LOG_TYPE_INFO, __LINE__, __FUNCTION__);
   }
  }
 }
 
 int history_total = OrdersHistoryTotal();
 
 for(int i=history_total-1; i>=0; i--)
 {
  if(OrderSelect(i,SELECT_BY_POS,MODE_HISTORY))
  {
   int ticket = OrderTicket();
   
   logger.log(StringConcatenate("Back-filling #", ticket), LOG_TYPE_INFO, __LINE__, __FUNCTION__);
   
   if(db.SearchLinear(ticket) < 0) {
    POSTTradeOpen(ticket, OrderSymbol(), OrderType(), OrderLots(), OrderOpenTime(), OrderOpenPrice()); 
    POSTTradeClose(ticket,
                   OrderSymbol(),
                   OrderStopLoss(),
                   OrderTakeProfit(),
                   OrderCloseTime(),
                   OrderClosePrice(),
                   OrderCommission(),
                   OrderSwap(),
                   OrderComment(),
                   0, 0, 0, 0,
                   OrderProfit());
   }
  }
 }
}
//+------------------------------------------------------------------+
string RefreshDisabledSymbolsStr()
{
 int total = disable_trading_symbol.Total();
 string ret="";
 
 for(int i=0; i<total; i++) {
  ret += disable_trading_symbol[i] + "\n";
  
  if(disable_trading_symbol[i] == "Disable all symbols")
   return "Trading is disabled";
 }
  
 return ret;
}
//+------------------------------------------------------------------+
void RefreshSymbolListBox()
{
  CArrayString symbols;
  guiRemoveListAll(hwnd, symbol_list);
	
  //-- true -> only symbols in market watch
	int symbols_total = SymbolsTotal(true);
	
   for(int i=0; i<symbols_total; i++)
    symbols.Add(SymbolName(i, true));
   
   symbols.Sort(); 
   symbols.Insert("Disable all symbols", 0);
   symbols_total = symbols.Total();
   
   for(int i=0; i<symbols_total; i++)
    guiAddListItem(hwnd, symbol_list, symbols[i]);
}
//+------------------------------------------------------------------+
void DisableGUIinit()
{
  hwnd = WindowHandle(Symbol(),Period());
  symbols_market_watch_total = SymbolsTotal(true);
  guiRemoveAll(hwnd);	
  // Build the panel
  panel1 = guiAdd(hwnd,"label",30,50,250,120," Select Symbols to Disable"); guiSetTextColor(hwnd,panel1,Black);
  guiSetBgColor(hwnd,panel1,Orange);
  display_panel = guiAdd(hwnd,"label",30,170,250,250,RefreshDisabledSymbolsStr());
  guiSetBgColor(hwnd,display_panel,White);
  // Add buttons to panel
  add_button = guiAdd(hwnd,"button",35,100,240,25,"Add"); guiSetBorderColor(hwnd,add_button,Moccasin);
  remove_button = guiAdd(hwnd,"button",35,130,240,25,"Remove"); guiSetBorderColor(hwnd,remove_button,Chocolate);
  // Add ListBox
  symbol_list = guiAdd(hwnd,"list",35,70,240,150,"");
  RefreshSymbolListBox();
}
//+------------------------------------------------------------------+
void DisableGUIdeinit()
{
  if (hwnd>0) guiRemoveAll(hwnd); 
  // Call this as last mt4gui function
  guiCleanup(hwnd);
} 
//+------------------------------------------------------------------+
void DisableGUIengine()
{
   // Update symbols list
   int t = SymbolsTotal(true);
   if(symbols_market_watch_total != t) {
    RefreshSymbolListBox();
    symbols_market_watch_total = t;
   }
   
   // Refresh symbols list
   if(GlobalVariableGet("refresh_disable_panel") == 1)
   {
    guiSetText(hwnd, display_panel, RefreshDisabledSymbolsStr(), 0, "");
    GlobalVariableSet("refresh_disable_panel", 0);
   }

	// Button "Add Symbol"
	if (guiIsClicked(hwnd,add_button))
			{
				// We require that at least one item has been selected
				if (guiGetListSel(hwnd,symbol_list)>-1)
					{
						// We can read the selected items index by guiGetListSel
						// and Selected Item using guiGetText() function
						string selected_symbol = guiGetText(hwnd,symbol_list);
						disable_trading_symbol.Add(selected_symbol);
						guiSetText(hwnd, display_panel, RefreshDisabledSymbolsStr(), 0, "");
                  PlaySound("tick.wav");							
					}
				
			}

	
	// Button "Remove Symbol"
	if (guiIsClicked(hwnd,remove_button))
			{
				// We require that at least one item has been selected
				if (guiGetListSel(hwnd,symbol_list)>-1)
					{
						// We can read the selected items index by guiGetListSel
						// and Selected Item using guiGetText() function
						string selected_symbol = guiGetText(hwnd,symbol_list);
						int pos = disable_trading_symbol.SearchLinear(selected_symbol);
						if(pos >= 0)
						{
						 disable_trading_symbol.Delete(pos);
						 guiSetText(hwnd, display_panel, RefreshDisabledSymbolsStr(), 0, "");
                   PlaySound("tick.wav");
                  }
                  else if("Disable all symbols")
                  {
                   int total = disable_trading_symbol.Total();
                   for(int i=total-1; i>=0; i--)
                    disable_trading_symbol.Delete(i);
                   
                   guiSetText(hwnd, display_panel, RefreshDisabledSymbolsStr(), 0, "");
                   PlaySound("tick.wav");
                  }
                  else
                   PlaySound("timeout.wav");
					}
				
			}
}
//+---------------------------------------------------------------------------------------------+
void DeInit(const int reason)
{
  delete candle;
   
  delete backlog;
   
  DisableGUIdeinit();
}