//+------------------------------------------------------------------+
//|                                                  AlphaConfig.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#define  DEFAULT_GROUP_ID           0

#define  RET_FALSE                  105
#define  RET_TRUE                   106
#define  RET_NIL                    107

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
   Logger logger(__FILE__, "Alpha HOG");
#endif

#include <AlphaHOGInputVariables.mqh>

enum ret_alpha_profile_get { IGNORE_TRADER=-10, RET_IGNORE_SYMBOL, RET_NO_PROFILE, RET_SYMBOL_NOT_FOUND };
enum ret_pyramid_max_loss_type { ALL, INDIVIDUAL, NONE }; 

struct group_config
{ 
 int group_num;
 int symbol_set;
 bool trade_unknown_symbols;
 float sl_tp_delay;
 float max_delay;
 float pos_pip_alpha;
 float neg_pip_alpha;
 float multiplier;
 int sl;
 int tp;
 bool reject_on_max_delay;
 int stopout_count_trigger;
 float multiplier_on_stopout;
 float timelimit;
 float max_allowable_market_deviation;
 float pyramid_on_pips_lost;
 float close_pyramid_on_pips_gained;
 bool close_pyramid_on_direction_change;
 float pyramid_max_loss;
 bool pyramid_max_loss_type;
};

class symb_set
{
 private:
  string     name[];
  int        id[];
  
 public:
  bool       LoadSymbolSet();
  int        ID(string symbol);
  
  symb_set() { LoadSymbolSet(); };
};
//+------------------------------------------------------------------+
int symb_set::ID(string symbol)
 {
   int size=ArraySize(name);
   
   for(int i=0;i<size;i++)
    {  
      if(name[i]==symbol)
       return(id[i]);
    }
    
   return(RET_SYMBOL_NOT_FOUND);
 }
//+------------------------------------------------------------------+
bool symb_set::LoadSymbolSet(void)
 {
   int handle = FileOpen(ALPHA_SYMBOL_SETS_FILE_NAME,FILE_CSV|FILE_READ,',');
   
   int no_of_columns_in_header = 2;
 
   if(handle >= 0)
    {
     int i=0;
     
     //Skip header line
     for(int z=1;z<=no_of_columns_in_header;z++)
      FileReadString(handle);
      
     int iid=1;
   
     while(true)
      {
       string strVar = FileReadString(handle);
              strVar = StringTrimLeft(StringTrimRight(strVar));
              
       if(strVar=="/end" || i > 10000)
        break;
        
       if(strVar=="*")
        iid=RET_IGNORE_SYMBOL;
        
       ArrayResize(name,i+1);      
        name[i]=strVar;
       
       ArrayResize(id,i+1);
        id[i]=iid;
        
       i++;

       while(!FileIsLineEnding(handle)) {
         ArrayResize(name,i+1);      
          name[i]=strVar;
       
         ArrayResize(id,i+1);
          id[i]=iid;
      
         strVar = StringTrimLeft(StringTrimRight(FileReadString(handle)));
         name[i]=strVar;
         id[i]=iid;
         i++;
       }
       
       iid++;
      }
    
     FileClose(handle);
       
     return(true);
   }
  else
   return(false);
 }
//+------------------------------------------------------------------+ 
class alpha_profile
 {
  private:
    int              group[];
    symb_set*        symbol_set;
    group_config     alpha_config[];
    int              GroupID(int mt4_id);
    void             Init();
    
  public:
    int              mt4[];
    group_config     Get(int mt4_id,string symbol,double sl,double tp);
    group_config     Get(int mt4_id,string symbol);
    
  alpha_profile();
  ~alpha_profile();
 };

//+------------------------------------------------------------------+
alpha_profile::alpha_profile()
 {
  ReadAccountsIntoArray(mt4,group);
  
  symbol_set = new symb_set();
  
  Init();
 } 
//+------------------------------------------------------------------+
alpha_profile::~alpha_profile()
 {
   delete(symbol_set);
 } 
//+------------------------------------------------------------------+
int alpha_profile::GroupID(int mt4_id)
 {
  int size = ArraySize(mt4);
  
  for(int i=0;i<size;i++)
   if(mt4[i]==mt4_id)         return(group[i]);
   
  return(-1);
 }
//+------------------------------------------------------------------+
alpha_profile::Init(void)
 {
   __TRACE__
   
   int grp[];
   string sym_set[];
   bool trade_unknown_symbols[];
   int sl[];
   int tp[];
   float sltp_delay[];
   float max_delay[];
   bool reject_on_max_delay[];
   bool reverse[];
   float multiplier[];
   float pos_pip_alpha[];
   float neg_pip_alpha[];
   float max_allowable_market_deviation [];
   float pyramid_on_pips_lost[];
   float close_on_pips_gained[];
   bool close_on_direction_change[];
   int max_loss_type[];
   float max_loss[];
   
 __ if(!ReadAlphaOpenConfigIntoArray(grp, sym_set, trade_unknown_symbols, sl, tp, sltp_delay, max_delay, reject_on_max_delay, reverse, multiplier, pos_pip_alpha, neg_pip_alpha, max_allowable_market_deviation ))
     LOG_HIGH("Error Reading: " + ALPHA_OPEN_CONFIG_FILE_NAME);
   
  for(int i=0;i<ArraySize(grp);i++)
   {
     ArrayResize(alpha_config,i+1);
     
     alpha_config[i].group_num = grp[i];
   
     alpha_config[i].multiplier = multiplier[i];
     
     if(reverse[i]==true)  alpha_config[i].multiplier *= -1;
     
     alpha_config[i].symbol_set = symbol_set.ID(sym_set[i]);
     
     alpha_config[i].trade_unknown_symbols = trade_unknown_symbols[i];
     
     alpha_config[i].sl = sl[i];
     alpha_config[i].tp = tp[i];
     alpha_config[i].sl_tp_delay = sltp_delay[i];
     alpha_config[i].max_delay = max_delay[i]; 
     alpha_config[i].reject_on_max_delay = reject_on_max_delay[i];
     
     if(reverse[i])
      alpha_config[i].multiplier = multiplier[i] * -1;
     else
      alpha_config[i].multiplier = multiplier[i];
     
     alpha_config[i].pos_pip_alpha = pos_pip_alpha[i];
     alpha_config[i].neg_pip_alpha = neg_pip_alpha[i];
     alpha_config[i].max_allowable_market_deviation  = max_allowable_market_deviation [i];
   }
  
  int stopout_count_trigger[];
  float new_multiplier[];
  
  if(!ReadStopoutIntoArray(grp, sym_set, stopout_count_trigger, reverse, new_multiplier))
   LOG_HIGH("Error Reading: " + ALPHA_STOPOUT_FILE_NAME);
  
  for(int i=0;i<ArraySize(grp);i++)
   {
   for(int j=0;j<ArraySize(alpha_config);j++)
    {
    if(alpha_config[j].group_num==grp[i] && (alpha_config[j].symbol_set==symbol_set.ID(sym_set[i]) || grp[i]==DEFAULT_GROUP_ID))   //Same group and symbol set
     {
      alpha_config[j].stopout_count_trigger = stopout_count_trigger[i];
      
      if(reverse[i])
       alpha_config[j].multiplier_on_stopout = new_multiplier[i] * -1;
      else
       alpha_config[j].multiplier_on_stopout = new_multiplier[i];
     }
    }
   }//end for ArraySize(grp)
   
  if(!ReadAlphaCloseConfigIntoArray(grp, sym_set, trade_unknown_symbols, pyramid_on_pips_lost, close_on_pips_gained, close_on_direction_change, max_loss_type, max_loss))
   LOG_HIGH("Error Reading: " + ALPHA_CLOSE_CONFIG_FILE_NAME);
   
  for(int i=0;i<ArraySize(grp);i++)
   {
   for(int j=0;j<ArraySize(alpha_config);j++)
    {
    if(alpha_config[j].group_num==grp[i] && (alpha_config[j].symbol_set==symbol_set.ID(sym_set[i]) || grp[i]==DEFAULT_GROUP_ID))   //Same group and symbol set
     {
      alpha_config[j].pyramid_on_pips_lost = pyramid_on_pips_lost[i];
      
      alpha_config[j].close_pyramid_on_pips_gained = close_on_pips_gained[i];
      
      alpha_config[j].close_pyramid_on_direction_change = close_on_direction_change[i];
      
      alpha_config[j].pyramid_max_loss_type = max_loss_type[i];
      
      alpha_config[j].pyramid_max_loss = max_loss[i];
     }
    }
   }//end for ArraySize(grp)
 }
//+------------------------------------------------------------------+
group_config alpha_profile::Get(int mt4_id,string symbol)
 {
  int group_id = GroupID(mt4_id);
 
  int symbol_id = symbol_set.ID(symbol);
  group_config default_config = {0,0,0.0,0.0,0.0,0.0,0.0,0,0,0,0.0,0.0,0.0};

  if(group_id == 0)  {      //ignore trader
    default_config.group_num = IGNORE_TRADER;
    return(default_config);
   }

  if(symbol_id == RET_IGNORE_SYMBOL)  {                        //ignore trade: symbol on exclusion_list
    default_config.group_num = RET_IGNORE_SYMBOL;
    return(default_config); 
  }
  
  if(symbol_id == RET_SYMBOL_NOT_FOUND)  {                     //ignore trade: symbol on exclusion_list
    default_config.group_num = RET_SYMBOL_NOT_FOUND;
  }
 
  int size = ArraySize(alpha_config);
 
  for(int i=0;i<size;i++) {   
   if(alpha_config[i].group_num == group_id && (
        alpha_config[i].symbol_set == symbol_id || alpha_config[i].trade_unknown_symbols))
    {  
     return(alpha_config[i]);
    }  
  }

  return(default_config);   //the default config
 }
//+------------------------------------------------------------------+
group_config alpha_profile::Get(int mt4_id,string symbol,double sl,double tp)
 {
  __TRACE__
  
  int group_id = GroupID(mt4_id);
 
  int symbol_id = symbol_set.ID(symbol);
  group_config default_config = {0,0,0.0,0.0,0.0,0.0,0.0,0,0,0,0.0,0.0,0.0};

  if(group_id < 0)
    LOG_HIGH(StringConcatenate("Error: MT4 Account Number #",mt4_id," could not be found"));
    
  if(group_id == 0)  {      //ignore trader
    default_config.group_num = IGNORE_TRADER;
    return(default_config);
   }

  if(symbol_id == RET_IGNORE_SYMBOL)  {                        //ignore trade: symbol on exclusion_list
    default_config.group_num = RET_IGNORE_SYMBOL;
    return(default_config); 
  }
  
  if(symbol_id == RET_SYMBOL_NOT_FOUND)  {                     //ignore trade: symbol on exclusion_list
    default_config.group_num = RET_SYMBOL_NOT_FOUND;
  }
 
  int size = ArraySize(alpha_config);
 
  bool hasSL=false, hasTP=false;
  
  if(sl > 0)
    hasSL = true;
    
  if(tp > 0)
    hasTP = true;
 
  for(int i=0;i<size;i++) {   
   if(alpha_config[i].group_num == group_id && (
        alpha_config[i].symbol_set == symbol_id || alpha_config[i].trade_unknown_symbols))
    {  
     if(alpha_config[i].sl == RET_TRUE)
      if(hasSL==false)   continue;
     
     if(alpha_config[i].sl == RET_FALSE)
      if(hasSL==true)    continue;
     
     if(alpha_config[i].tp == RET_TRUE)
      if(hasTP==false)   continue;
      
     if(alpha_config[i].tp == RET_FALSE)
      if(hasTP==true)    continue;
 
     return(alpha_config[i]);
    }  
  }

  return(default_config);   //the default config
 }
//+------------------------------------------------------------------+
bool ReadStopoutIntoArray(int& group[], string& symbol_set[], int& stopout_count_trigger[], bool& reverse[], float& new_multiplier[])
 {
   __TRACE__
   
   int handle = FileOpen(ALPHA_STOPOUT_FILE_NAME,FILE_CSV|FILE_READ,',');
   int no_of_columns = 5;
 
   if(handle >= 0)
    {
     int i=0;
     
     //Skip header line
     for(int z=1;z<=no_of_columns;z++)
      FileReadString(handle);
   
     while(true)
      {
       string first_col = FileReadString(handle);
              first_col = StringTrimLeft(StringTrimRight(first_col));
              
       if(first_col=="/end" || i > 10000)
        break;
       
       ArrayResize(group,i+1); 
       if(first_col=="default"||first_col=="DEFAULT")
        group[i]=DEFAULT_GROUP_ID;
       else 
        group[i]=(int)first_col;
 
       ArrayResize(symbol_set,i+1);
       symbol_set[i]=StringTrimLeft(StringTrimRight(FileReadString(handle)));
       
       ArrayResize(stopout_count_trigger,i+1);      
       stopout_count_trigger[i]=(int)FileReadNumber(handle);
       
       ArrayResize(reverse,i+1);
       string direction = FileReadString(handle);
       if(direction=="follow")
         reverse[i]=false;
       else if(direction=="reverse")
         reverse[i]=true;
       else
         return(false);

       ArrayResize(new_multiplier,i+1);
       new_multiplier[i]=(float)FileReadNumber(handle);
      
       i++;
      }
    
     FileClose(handle);
     
     LOG_EXTENSIVE_1(group);
     LOG_EXTENSIVE_1(symbol_set);
     LOG_EXTENSIVE_1(stopout_count_trigger);
     LOG_EXTENSIVE_1(reverse);
     LOG_EXTENSIVE_1(new_multiplier);
     
     return(true);
   }
  else
   return(false);
 }
//+------------------------------------------------------------------+
bool ReadAlphaCloseConfigIntoArray(int& group[], string& symbol_set[], bool& trade_unknown_symbols[], float& pyramid_on_pips_lost[], float& close_on_pips_gained[], bool& close_on_direction_change[], int& max_loss_type[], float& max_loss[])
 {
   __TRACE__
   
   int handle = FileOpen(ALPHA_CLOSE_CONFIG_FILE_NAME,FILE_CSV|FILE_READ,',');
   int no_of_columns = 9;
   
   if(handle >= 0)
    {
     int i=0;
     
     //Skip header line
     for(int z=1;z<=no_of_columns;z++)
      FileReadString(handle);
   
     while(true)
      {
       string first_col = FileReadString(handle);
              first_col = StringTrimLeft(StringTrimRight(first_col));
              
       if(first_col=="/end" || i > 10000)
        break;
        
       ArrayResize(group,i+1); 
       if(first_col=="default"||first_col=="DEFAULT") 
        group[i]=DEFAULT_GROUP_ID;
       else 
        group[i]=(int)first_col;
 
       ArrayResize(symbol_set,i+1);
       symbol_set[i]=StringTrimLeft(StringTrimRight(FileReadString(handle)));
       
       ArrayResize(trade_unknown_symbols,i+1);
       if(symbol_set[i]=="*")    
         trade_unknown_symbols[i]=true; 
       else
         trade_unknown_symbols[i]=false;
   
       string buf = "";
   
       ArrayResize(pyramid_on_pips_lost,i+1);
       buf = StringTrimLeft(StringTrimRight(FileReadString(handle)));
       if(buf=="true" || buf=="TRUE" || buf=="1") {
         pyramid_on_pips_lost[i]=(float)FileReadNumber(handle);
         //if(pyramid_on_pips_lost[i] < 0) { LOG_HIGH("pyramid_on_pips_gained must be set >= 0"); return(false); }
       }
       else if(buf=="false" || buf=="FALSE" || buf=="0") {
         pyramid_on_pips_lost[0]=FLT_MAX;
         FileReadNumber(handle);
       }
       else {
         LOG_HIGH("Error reading pyramid_on_pips_gained: must be {TRUE, true, FALSE, false}");
         LOG_HIGH("pyramid_on_pips_gained=" + buf);
         return(false);
       }
       
       ArrayResize(close_on_pips_gained,i+1);
       buf = StringTrimLeft(StringTrimRight(FileReadString(handle)));
       if(buf=="true" || buf=="TRUE" || buf=="1") {
         close_on_pips_gained[i]=(float)FileReadNumber(handle);
         //if(close_on_pips_gained[i] < 0) { LOG_HIGH("close_on_pips_lost must be set >= 0"); return(false); }
       }
       else if(buf=="false" || buf=="FALSE" || buf=="0") {
         close_on_pips_gained[0]=FLT_MAX;
         FileReadNumber(handle);
       }
       else {
         LOG_HIGH("Error reading CLOSE_ON_Pips_Lost: must be {TRUE, true, FALSE, false}");
         LOG_HIGH("CLOSE_ON_Pips_Lost=" + buf);
         return(false);
       }
       
       ArrayResize(close_on_direction_change,i+1);
       buf = StringTrimLeft(StringTrimRight(FileReadString(handle)));
       if(buf=="true" || buf=="TRUE" || buf=="1") {
         close_on_direction_change[i]=true;
       }
       else if(buf=="false" || buf=="FALSE" || buf=="0") {
         close_on_direction_change[i]=false;
       }
       else {
         LOG_HIGH("Error reading CLOSE_ON_External_Direction_Change: must be {TRUE, true, FALSE, false}");
         LOG_HIGH("CLOSE_ON_External_Direction_Change=" + buf);
         return(false);
       }
       
       ArrayResize(max_loss_type,i+1);
       ArrayResize(max_loss,i+1);
       buf = StringTrimLeft(StringTrimRight(FileReadString(handle)));
       if(buf=="ALL" || buf=="all") {
         max_loss_type[i]=ALL;
         max_loss[i]=(float)FileReadNumber(handle);
         if(max_loss[i] < 0) { LOG_HIGH("max_loss must be set >= 0"); return(false); }
       }
       else if(buf=="NONE" || buf=="none") {
         max_loss_type[i]=NONE;
         max_loss[i]=FLT_MAX;
         FileReadNumber(handle);
       }
       else {
         LOG_HIGH("Error reading CLOSE_ON_Max_Loss: must be {ALL, INDIVIDUAL, NONE, all, individual, none}"); 
         LOG_HIGH("CLOSE_ON_Max_Loss="+buf);
         return(false);
       }
      
       i++;
      }
    
     FileClose(handle);
     
     LOG_EXTENSIVE_1(group);
     LOG_EXTENSIVE_1(symbol_set);
     LOG_EXTENSIVE_1(trade_unknown_symbols);
     LOG_EXTENSIVE_1(pyramid_on_pips_lost);
     LOG_EXTENSIVE_1(close_on_pips_gained);
     LOG_EXTENSIVE_1(close_on_direction_change);
     LOG_EXTENSIVE_1(max_loss_type);
     LOG_EXTENSIVE_1(max_loss);
     
     return(true);
   }
  else
   return(false); 
 }
//+------------------------------------------------------------------+
bool ReadAlphaOpenConfigIntoArray(int& group[], string& symbol_set[], 
                                  bool& trade_unknown_symbols[], int& sl[], int& tp[], 
                                  float& sltp_delay[], float& max_delay[], bool& reject_on_max_delay[], 
                                  bool& reverse[], float& multiplier[], float& pos_pip_alpha[], 
                                  float& neg_pip_alpha[], float& max_allowable_market_deviation [])
 {
   __TRACE__ 
   
   int handle = FileOpen(ALPHA_OPEN_CONFIG_FILE_NAME,FILE_CSV|FILE_READ,',');
   int no_of_columns = 11;
   
   if(handle >= 0)
    {
     int i=0;
     
     //Skip header line
     for(int z=1;z<=no_of_columns;z++)
      FileReadString(handle);
   
     while(true)
      {
       string first_col = FileReadString(handle);
              first_col = StringTrimLeft(StringTrimRight(first_col));
              
       if(first_col=="/end" || i > 10000)
        break;
        
       ArrayResize(group,i+1); 
       if(first_col=="default"||first_col=="DEFAULT") 
        group[i]=DEFAULT_GROUP_ID;
       else 
        group[i]=(int)first_col;
 
       ArrayResize(symbol_set,i+1);
       symbol_set[i]=StringTrimLeft(StringTrimRight(FileReadString(handle)));
       
       ArrayResize(trade_unknown_symbols,i+1);
       if(symbol_set[i]=="*")    
         trade_unknown_symbols[i]=true; 
       else
         trade_unknown_symbols[i]=false;
   
       ArrayResize(sl,i+1);
       ArrayResize(tp,i+1);
       string action = StringTrimLeft(StringTrimRight(FileReadString(handle))); 
       if(action=="sl" || action=="SL") {
         sl[i]=RET_TRUE;
         tp[i]=RET_NIL;
       }
       else if(action=="!sl" || action=="!SL") {
         sl[i]=RET_FALSE;
         tp[i]=RET_NIL;
       }
       else if(action=="tp" || action=="TP") {
         sl[i]=RET_NIL;
         tp[i]=RET_TRUE;
       }
       else if(action=="!tp" || action=="!TP") {
         sl[i]=RET_NIL;
         tp[i]=RET_FALSE;
       }
       else if(action=="sl&&tp" || action=="SL&&TP") {
         sl[i]=RET_TRUE;
         tp[i]=RET_TRUE;
       }
       else if(action=="!sl&&tp" || action=="!SL&&TP") {
         sl[i]=RET_FALSE;
         tp[i]=RET_TRUE;
       }
       else if(action=="sl&&!tp" || action=="SL&&!TP") {
         sl[i]=RET_TRUE;
         tp[i]=RET_FALSE;
       }
       else if(action=="!sl&&!tp" || action=="!SL&&!TP") {
         sl[i]=RET_FALSE;
         tp[i]=RET_FALSE;
       }
       else if(action=="*") {
         sl[i]=RET_NIL;
         tp[i]=RET_NIL;
       }
       else {
         LOG_HIGH("Error reading: sl_tp");
         return(false); 
       }
         
       ArrayResize(sltp_delay,i+1);
       sltp_delay[i]=(float)FileReadNumber(handle);
       if(sltp_delay[i] < 0) { LOG_HIGH("sltp_delay must be set >= 0"); return(false); }
       
       ArrayResize(max_delay,i+1);
       max_delay[i]=(float)FileReadNumber(handle);
       if(max_delay[i] < sltp_delay[i]) { LOG_HIGH("max_delay must be greater than or equal to sltp_delay"); return(false); }
       if(max_delay[i] < 0) { LOG_HIGH("max_delay must be set >= 0"); return(false); }
       
       ArrayResize(reject_on_max_delay,i+1);
       string buf = StringTrimLeft(StringTrimRight(FileReadString(handle)));
       if(buf=="true" || buf=="TRUE") {
         if(max_delay[i]==0) { LOG_HIGH("Error: please set max_delay > 0 if reject_on_max_delay TRUE"); return(false); }
         reject_on_max_delay[i]=true;
       }
       else if(buf=="false" || buf=="FALSE")
         reject_on_max_delay[i]=false;
       else if(max_delay[i] < 0) {
        LOG_HIGH("Error: max_delay must be set >= 0");
        return(false);
       }
       else {
        LOG_HIGH("Error reading reject_on_max_delay value");
        return(false);
       }
       
       ArrayResize(reverse,i+1);
       string direction = FileReadString(handle);
       if(direction=="follow" || direction=="FOLLOW")
         reverse[i]=false;
       else if(direction=="reverse" || direction=="REVERSE")
         reverse[i]=true;
       else {
         LOG_HIGH("Error reading direction");
         return(false);
       }
         
       ArrayResize(multiplier,i+1);
       multiplier[i]=(float)FileReadNumber(handle);
       if(multiplier[i] == 0)   
         LOG_WARNING("no trades will be placed for group with multiplier = 0");
       
       ArrayResize(pos_pip_alpha,i+1);
       pos_pip_alpha[i]=(float)FileReadNumber(handle);
       if(pos_pip_alpha[i] < 0) { LOG_HIGH("pos_pip_alpha must be set >= 0"); return(false); }
       
       ArrayResize(neg_pip_alpha,i+1);
       neg_pip_alpha[i]=(float)FileReadNumber(handle);
       if(neg_pip_alpha[i] < 0) { LOG_HIGH("neg_pip_alpha must be set >= 0"); return(false); }
       
       ArrayResize(max_allowable_market_deviation ,i+1);
       max_allowable_market_deviation[i]=(float)FileReadNumber(handle); 
       if(max_allowable_market_deviation[i] > 0 && max_allowable_market_deviation[i] < neg_pip_alpha[i]) {
        LOG_HIGH("neg_pip_alpha must be less than or equal to max_allowable_market_deviation");
        return(false);
       }
      
       i++;
      }
    
     FileClose(handle);
     
    LOG_EXTENSIVE_1(group);
    LOG_EXTENSIVE_1(symbol_set);
    LOG_EXTENSIVE_1(trade_unknown_symbols);
    LOG_EXTENSIVE_1(sl);
    LOG_EXTENSIVE_1(tp);
    LOG_EXTENSIVE_1(sltp_delay);
    LOG_EXTENSIVE_1(max_delay);
    LOG_EXTENSIVE_1(reject_on_max_delay);
    LOG_EXTENSIVE_1(reverse);
    LOG_EXTENSIVE_1(multiplier);
    LOG_EXTENSIVE_1(pos_pip_alpha);
    LOG_EXTENSIVE_1(neg_pip_alpha);
    LOG_EXTENSIVE_1(max_allowable_market_deviation);

     return(true);
   }
  else
   return(false); 
 }
//+------------------------------------------------------------------+
bool ReadAccountsIntoArray(int& account[], int& group[])
 {
   __TRACE__
   
   int handle = FileOpen(ACCOUNT_SETTINGS_FILE_NAME,FILE_CSV|FILE_READ,',');
   int no_of_columns = 2;
 
   if(handle >= 0)
    {
     int i=0;
     
     //Skip header line
     for(int z=1;z<=no_of_columns;z++)
      FileReadString(handle);
   
     while(true)
      {
       string first_col = FileReadString(handle);
              first_col = StringTrimLeft(StringTrimRight(first_col));
              
       if(first_col=="/end" || i > 10000)
        break;
        
       ArrayResize(account,i+1);      
       account[i]=(int)first_col;
 
       ArrayResize(group,i+1);
       group[i]=(int)FileReadNumber(handle);
      
       i++;
      }
    
     FileClose(handle);
     
     LOG_EXTENSIVE_1(account);
     LOG_EXTENSIVE_1(group);
    
     return(true);
   }
  else
   return(false);
 }