//+------------------------------------------------------------------+
//|                                                    Simulator.mqh |
//|                                                         Carm Inc |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Carm Inc"
#property link      "https://www.mql5.com"
#property strict

#ifndef ARRAYINT
#define ARRAYINT
  #include  <Arrays/ArrayInt.mqh>
#endif

class Simulator
{
 private:
  string m_long;
  string m_short;
  string m_lots;
  
 public:
  void run();
  
  Simulator();
  ~Simulator();
};

void Simulator::run()
{
 int i,ticket=-1,type=-1;
 int p=(int)GlobalVariableGet("zOrder");
 double price=-1;
 string name, prefix;
 string id;
 double sl=-1, tp=-1;
 datetime close_time;
 static CArrayInt sl_bin;
 static CArrayInt tp_bin;
 
 double vol_open=GlobalVariableGet("Open_Lots");
   
 double vol_close=GlobalVariableGet("Close_Lots");
   
 //---- Modify Orders
 int total=ObjectsTotal();

 for(i=total-1;i>=0;i--)
  {
   name=ObjectName(i);
   prefix=StringSubstr(name,0,5);
   id=StringSubstr(name,5);
   int scen;
      
   if(prefix == "or - ")
    scen=1;
   else if(prefix == "sl - ")
    scen=2;
   else if(prefix == "tp - ")
    scen=3;
   else
    continue;
       
   int t=(int)NormalizeDouble(StrToDouble(id),0); 
      
   if(OrderSelect(t,SELECT_BY_TICKET))
   {
     ticket=OrderTicket();
     type=OrderType();
     sl=OrderStopLoss();
     tp=OrderTakeProfit();
     close_time=OrderCloseTime();
     price=OrderOpenPrice();
   
                    
   if(t!=ticket)        continue;            
                  
   switch(scen)
   {
    case 1: 
    {
    double obj_price= NormalizeDouble(ObjectGet(name,OBJPROP_PRICE1), Digits); 
        
    //Delete line if pending order triggers or is deleted
    if(type<2)          {     ObjectDelete(name); break; }
    if(close_time != 0) {     ObjectDelete(name); break; }
  
    //Modify order price when user moves line
    if(MathAbs(price - obj_price) > DBL_EPSILON)
    bool ans = OrderModify(ticket,obj_price,sl,tp,0,CLR_NONE);
                   
    break;
    }
        
    case 2: 
    {
    double obj_sl= NormalizeDouble(ObjectGet(name,OBJPROP_PRICE1), Digits);
                
    //Delete
    if(close_time != 0) {     ObjectDelete(name); break; }
               
    //Modify SL
    if(MathAbs(sl - obj_sl) > DBL_EPSILON)
    {
     bool ans = OrderModify(ticket,price,obj_sl,tp,0,CLR_NONE);         
     
     if(ans)
     {
      int pos = sl_bin.SearchLinear(ticket);
     
      if(pos < 0)
      {
       sl_bin.Add(ticket);
      }
     }
    }
          
    break;
     
    }           
    case 3: 
    {
    double obj_tp= NormalizeDouble(ObjectGet(name,OBJPROP_PRICE1), Digits);
                
    //Delete
    if(close_time != 0) {    ObjectDelete(name); break; }
    
    //Modify TP
    if(MathAbs(tp - obj_tp) > DBL_EPSILON)
    {
     bool ans = OrderModify(ticket,price,sl,obj_tp,0,CLR_NONE);
    
     if(ans)
     {
      int pos = tp_bin.SearchLinear(ticket);
     
      if(pos < 0)
      {
       tp_bin.Add(ticket);
      }
     }
    }    
          
    break;
    }  
   }//end switch
   }//end if(OrderSelect)      
  }//end for
     
  //---- Delete sl/tp when the respective object is deleted
  bool hasSL;
  bool hasTP;
    
  for(i=0;i<OrdersTotal();i++)
   {
    hasSL=false;
    hasTP=false;
      
    if(OrderSelect(i,SELECT_BY_POS))
     {
      ticket=OrderTicket();
      price=OrderOpenPrice();
     }
   
    total=ObjectsTotal();
   
    for(int j=total-1;j>=0;j--)
     {
      name=ObjectName(j);
      prefix=StringSubstr(name,0,5);
      id=StringSubstr(name,5);
      int t=(int)NormalizeDouble(StrToDouble(id),0);
        
      if(prefix=="sl - " && t==ticket)
        hasSL=true;
        
        
      if(prefix=="tp - " && t==ticket)
        hasTP=true;
     }
    
    if(OrderSelect(ticket,SELECT_BY_TICKET))
    {
     ticket=OrderTicket();
     sl=OrderStopLoss();
     tp=OrderTakeProfit();
    
     if(sl > DBL_EPSILON && !hasSL)
     {
      int pos = sl_bin.SearchLinear(ticket);
      
      if(pos >= 0)
      {
       bool ans = OrderModify(ticket,price,0,tp,0,CLR_NONE);
       sl_bin.Delete(pos);
      }
     }  
       
     if(tp > DBL_EPSILON && !hasTP)
     {
      int pos = tp_bin.SearchLinear(ticket);
      
      if(pos >= 0)
      {
       bool ans = OrderModify(ticket,price,sl,0,0,CLR_NONE);
       tp_bin.Delete(pos);
      }
     }
    }
   }//end for
   
   
   
  //---- Handle New Orders from Scripts
  int new_ticket;
  
  switch(p)
   {
    case 1: new_ticket = OrderSend(Symbol(),OP_BUY,vol_open,Ask,2,0,0);
            GlobalVariableSet("zOrder",0);
            break;
     
    case 2: new_ticket = OrderSend(Symbol(),OP_SELL,vol_open,Bid,2,0,0);
            GlobalVariableSet("zOrder",0);
            break;
             
    case 3: price=GlobalVariableGet("zClose");
    
            while(vol_close>0 && OrdersTotal()>0)
            {
              int cls_ticket = GetNearestBooknum(price);
              
              if(OrderSelect(cls_ticket,SELECT_BY_TICKET))
               {
                type=OrderType();
                double order_lots=OrderLots();
               
                  
                double lots=MathMin(order_lots,vol_close);
                        
                if(type==OP_BUY)       price=Bid;
                else                   price=Ask;
               
                if(OrderClose(cls_ticket,lots,price,2,CLR_NONE))
                 vol_close -= lots;
               }
             }
             
            GlobalVariableSet("zOrder",0);
            break;
             
    case 4: 
            {
            price=GlobalVariableGet("zLimit");
            
            int order;
             
            if(price<=Bid)
             order=OrderSend(Symbol(),OP_BUYLIMIT,vol_open,price,2,0,0);
            else
             order=OrderSend(Symbol(),OP_SELLLIMIT,vol_open,price,2,0,0);
              
            name=StringConcatenate("or - ",DoubleToStr(order,0)); 
             
            ObjectCreate(name,OBJ_HLINE,0,0,price);
            ObjectSet(name,OBJPROP_COLOR,ForestGreen);
            ObjectSet(name,OBJPROP_STYLE,STYLE_DASHDOT);
             
            GlobalVariableSet("zOrder",0);
            break;      
            }
          
    case 5: 
           {
            price=GlobalVariableGet("zStop");
            
            int order;
            
            if(price>Ask)
             order=OrderSend(Symbol(),OP_BUYSTOP,vol_open,price,2,0,0);
            else
             order=OrderSend(Symbol(),OP_SELLSTOP,vol_open,price,2,0,0);
             
            name=StringConcatenate("or - ",DoubleToStr(order,0)); 
             
            ObjectCreate(name,OBJ_HLINE,0,0,price);
            ObjectSet(name,OBJPROP_COLOR,ForestGreen);
            ObjectSet(name,OBJPROP_STYLE,STYLE_DASHDOT);
             
            GlobalVariableSet("zOrder",0);
            break;
          }
          
    case 6: 
          {  
            double Win_Price=GlobalVariableGet("zDelete");                                   //Delete Pending Orders
             
            string Symb=Symbol();                        // Symbol
            double Dist=1000000.0;                       // Presetting
            int Real_Order=-1;                           // No market orders yet
            int Ticket=-1;
           //----
              for(i=1; i<=OrdersTotal(); i++)          // Order searching cycle
              {
               if(OrderSelect(i-1,SELECT_BY_POS)==true) // If the next is available
               {                                       // Order analysis:
                //-----------------------------------------------------------------------
                if (OrderSymbol()!= Symb) continue;    // Symbol is not ours
                int Tip=OrderType();                   // Order type
                if (Tip<=1) continue;                  // Market order  
                //-----------------------------------------------------------------------
                double Price=OrderOpenPrice();         // Order price
                if (NormalizeDouble(MathAbs(Price-Win_Price),Digits)< //Selection
                    NormalizeDouble(Dist,Digits))       // of the closest order       
                   {
                    Dist=MathAbs(Price-Win_Price);      // New value
                    Real_Order=Tip;                     // Market order available
                    Ticket=OrderTicket();               // Order ticket
                    double Lot=OrderLots();             // Amount of lots
                   }
                //-----------------------------------------------------------------------
               }                                       //End of order analysis
              }                                          //End of order searching
          //----
           bool ans = OrderDelete(Ticket);
      
           GlobalVariableSet("zOrder",0);
           break;
          }
          
    case 7: 
           {
            double Win_Price=GlobalVariableGet("zSL_TP");
            int selected_ticket=(int)GlobalVariableGet("zSelectedTicket");
            int Ticket=-1;
            
            if(selected_ticket==0 && OrdersTotal()>1)
            {     
             string Symb=Symbol();                        // Symbol
             double Dist=1000000.0;                       // Presetting
             int Real_Order=-1;                           // No market orders yet
           
            //----
             for(i=1; i<=OrdersTotal(); i++)          // Order searching cycle
             {
              if(OrderSelect(i-1,SELECT_BY_POS)==true) // If the next is available
              {                                       // Order analysis:
              //-----------------------------------------------------------------------
               if (OrderSymbol()!= Symb) continue;    // Symbol is not ours
               int Tip=OrderType();                   // Order type
              //-----------------------------------------------------------------------
               double Price=OrderOpenPrice();         // Order price
               if (NormalizeDouble(MathAbs(Price-Win_Price),Digits)< //Selection
                   NormalizeDouble(Dist,Digits))       // of the closest order       
                  {
                   Dist=MathAbs(Price-Win_Price);      // New value
                   Real_Order=Tip;                     // Market order available
                   Ticket=OrderTicket();           // Order ticket
                   double Lot=OrderLots();             // Amount of lots
                  }
              //-----------------------------------------------------------------------
              }                                       //End of order analysis
             }                                          //End of order searching
          //----
               
             GlobalVariableSet("zSelectedTicket",Ticket);
             selected_ticket = Ticket;
            }
      // else
     //   {
         if(OrdersTotal()==1)
          if(OrderSelect(0,SELECT_BY_POS))
           selected_ticket=OrderTicket();
         
         if(OrderSelect(selected_ticket,SELECT_BY_TICKET))
          {
           price=OrderOpenPrice();
           type=OrderType();
           sl=OrderStopLoss();
           tp=OrderTakeProfit();
          }
         int c;
         
         if(type==0)
              c=0;
         else if(type==1)
              c=1;
         else if(type==2||type==4)
              c=2;  
         else
              c=3;
         
         bool ans_sl, ans_tp;
        
         switch(c)
          {
           
           
           case 0: if(Win_Price <= Bid)
                     ans_sl=OrderModify(selected_ticket,price,Win_Price,tp,0,CLR_NONE);
                    
                   if(Win_Price > Ask)
                     ans_tp=OrderModify(selected_ticket,price,sl,Win_Price,0,CLR_NONE);
           
                   break;
                  
           case 1: if(Win_Price >= Ask)
                     ans_sl=OrderModify(selected_ticket,price,Win_Price,tp,0,CLR_NONE);
                    
                   if(Win_Price < Bid)
                     ans_tp=OrderModify(selected_ticket,price,sl,Win_Price,0,CLR_NONE);
            
                   break;
           
           case 2: if(Win_Price <= price)
                     ans_sl=OrderModify(selected_ticket,price,Win_Price,tp,0,CLR_NONE);
                    
                   if(Win_Price > price)
                     ans_tp=OrderModify(selected_ticket,price,sl,Win_Price,0,CLR_NONE);
            
                   break; 
           
           case 3: if(Win_Price >= price)
                     ans_sl=OrderModify(selected_ticket,price,Win_Price,tp,0,CLR_NONE);
                    
                   if(Win_Price < price)
                     ans_tp=OrderModify(selected_ticket,price,sl,Win_Price,0,CLR_NONE);
            
                   break;      
          }
         
         if(ans_sl) //Set a horizontal line for stop losses
          {
           name=StringConcatenate("sl - ",DoubleToStr(selected_ticket,0)); 
             
           ObjectCreate(name,OBJ_HLINE,0,0,Win_Price);
           ObjectSet(name,OBJPROP_COLOR,Maroon);
           ObjectSet(name,OBJPROP_STYLE,STYLE_DASHDOT);
          }
          
         if(ans_tp) //Set a horizontal line for take profits
          {
           name=StringConcatenate("tp - ",DoubleToStr(selected_ticket,0)); 
             
           ObjectCreate(name,OBJ_HLINE,0,0,Win_Price);
           ObjectSet(name,OBJPROP_COLOR,Maroon);
           ObjectSet(name,OBJPROP_STYLE,STYLE_DASHDOT);
          }
         
         GlobalVariableSet("zSelectedTicket",0);
    //    }
        
        
       GlobalVariableSet("zOrder",0);
       break;
    }
  }//end case 7
}

Simulator::Simulator()
{
 GlobalVariableSet("zOrder",0);
 GlobalVariableSet("Open_Lots",1.0);
 GlobalVariableSet("Close_Lots",1.0);
 GlobalVariableSet("zSelectedTicket",0);
}

Simulator::~Simulator()
{
 GlobalVariableDel("zOrder");
 GlobalVariableDel("Open_Lots");
 GlobalVariableDel("Close_Lots");
 GlobalVariableDel("zSelectedTicket");
 GlobalVariableDel("zSL_TP");
}
//+------------------------------------------------------------------+
int GetNearestBooknum(double price)
{
 int total = OrdersTotal();
 int booknum = -1;
 double min_dist = DBL_MAX;
 
 for(int i=0; i<total; i++)
  {
   if(OrderSelect(i,SELECT_BY_POS))
    {
     if(OrderSymbol()==Symbol())
      {
       if(MathAbs(OrderOpenPrice()-price) < min_dist)
        {
          min_dist = MathAbs(OrderOpenPrice()-price);
          booknum = OrderTicket();
        }
      }
    }
  }
 
 return(booknum);
}