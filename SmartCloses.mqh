//+------------------------------------------------------------------+
//|                                                  SmartCloses.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
#endif

#ifndef REENTRY
#define REENTRY
   #include <ReEntry.mqh>
#endif 

#ifndef UTILS
#define UTILS
   #include <Utils.mqh>
#endif 

#define SMARTCLOSE_SMA_PREFIX    "smartclose_ma"

//---- Crossover Recognition Tags
#define CLOSE_ABOVE                 10
#define HIGH_REJECTED               11
#define CLOSE_BELOW                 12
#define LOW_REJECTED                13
#define NIL                         -412515

class OrderCloseEngine
{
 private:
  int p_sma_close_id;
  bool check_sma_already_set(int ticket);
  
 public:
  void bollinger();
  bool spike();
  bool sma();
  void sma_set_close(int ticket);
  OrderCloseEngine();
};

bool OrderCloseEngine::check_sma_already_set(int ticket)
{
 int total = GlobalVariablesTotal();

 for(int i=0; i<total; i++)
 {
  string name = GlobalVariableName(i);
  
  if(GlobalVariableGet(name) == ticket)
   return true;
 }
 
 return false;
}

bool OrderCloseEngine::sma()
{
 int total = GlobalVariablesTotal();
 int type = -1;
 bool b_triggered = false;
 
 if(MACrossoverEvent(50)==CLOSE_BELOW)
  type = OP_BUY;
  
 if(MACrossoverEvent(50)==CLOSE_ABOVE)
  type = OP_SELL;
 
 if(type >= 0)
 {
  datetime earliest_open_order = INT_MAX;
  double total_lots = 0;
  
  for(int i=0; i<total; i++)
  {
   string name = GlobalVariableName(i);
   
   if(StringFind(name, SMARTCLOSE_SMA_PREFIX) == 0)
   {
    int ticket = (int)GlobalVariableGet(name);
    
    if(OrderSelect(ticket, SELECT_BY_TICKET) && OrderType()==type)
    {
     if(OrderCloseTime()==0)
     {
      double cls_prc = (type==OP_BUY) ? Bid : (type==OP_SELL) ? Ask : -1;
      earliest_open_order = MathMin(earliest_open_order, OrderOpenTime());
      
      if(cls_prc >= 0) {
       double ord_lots = OrderLots();
       bool ans = OrderClose(ticket, ord_lots, cls_prc, 5);
       //bool ans = SemiClose(ticket, ord_lots);
       if(ans) {
        Alert("Closing #" + (string)ticket + " by " + __FUNCTION__);
        total_lots += ord_lots;
        b_triggered = true;
       }
      }
      else
       Alert("Unable to determine type and close price");
     }
     else {
      Alert("I: removing #" + (string)ticket + " from globals for already closed");
      GlobalVariableDel(name);
     }
    }
    else {
     Alert("I: removing #" + (string)ticket + " from globals for could not select");
     GlobalVariableDel(name);
    }
   }
  }
  
  if(earliest_open_order < INT_MAX)
  {
   double swing_pt;
   double atr = iATR(NULL, 0, 14, 0) * 1.5;
   if(type==OP_BUY)
    swing_pt = GetSwingHigh(earliest_open_order) + atr;
   else if(type==OP_SELL)
    swing_pt = GetSwingLow(earliest_open_order) - atr;
   else
    Alert("invalid order type");

   Alert("Setting reentry @ " + (string)swing_pt + ", lots=", total_lots);
   reentry.create(total_lots, swing_pt);
  }
 }
 
 return b_triggered;
}

OrderCloseEngine::OrderCloseEngine(void)
{
 int total = GlobalVariablesTotal();
 int max = -1;
 
 for(int i=0; i<total; i++)
 {
  string name = GlobalVariableName(i);
  
  if(StringFind(name, SMARTCLOSE_SMA_PREFIX) == 0)
  {
   int p1 = StringLen(SMARTCLOSE_SMA_PREFIX) + 1;
   int p2 = StringFind(name, "]");
   
   max = MathMax(max, (int)StringSubstr(name, p1, p2-p1));
  }
 }
 
 p_sma_close_id = max + 1;
}

void OrderCloseEngine::sma_set_close(int ticket)
{
 if(!check_sma_already_set(ticket))
 {
  string name = SMARTCLOSE_SMA_PREFIX + "[" + (string)p_sma_close_id + "]";
  GlobalVariableSet(name, ticket);
  p_sma_close_id++;
  Alert("I: set #" + (string)ticket + " to close at next sma touch");
 }
}

void OrderCloseEngine::bollinger(void)
{
 int total = OrdersTotal();
 
 for(int i=total-1; i>=0; i--)
 {
  if(OrderSelect(i,SELECT_BY_POS))
  { 
   double CLOSE_PERCENT = 0.75;
   string ord_symbol = OrderSymbol();
   double upper_band = iBands(ord_symbol, 0, 50, 2.5, 0, PRICE_CLOSE, MODE_UPPER, 0);
   double lower_band = iBands(ord_symbol, 0, 50, 2.5, 0, PRICE_CLOSE, MODE_LOWER, 0);
   double prc = iClose(ord_symbol, 0, 0);
   double atr_buffer = iATR(ord_symbol, 0, 14, 0) * 1.8;
   double close_prc;
   
   switch(OrderType())
   {
    case OP_BUY:
     close_prc = (upper_band * CLOSE_PERCENT) + (lower_band * (1-CLOSE_PERCENT));
     
     if(OrderOpenPrice() - prc > atr_buffer && prc >= close_prc) {
      int ticket = OrderTicket();
      double bid = MarketInfo(ord_symbol, MODE_BID);
      Alert("Closing #" + (string)ticket + " by " + __FUNCTION__);
      bool ans = OrderClose(ticket, OrderLots(), bid, 5);
     }
     
     break;
     
    case OP_SELL:
     close_prc = (lower_band * CLOSE_PERCENT) + (upper_band * (1-CLOSE_PERCENT));
     
     if(prc - OrderOpenPrice() > atr_buffer && prc <= close_prc) {
      int ticket = OrderTicket();
      double ask = MarketInfo(ord_symbol, MODE_ASK);
      Alert("Closing #" + (string)ticket + " by " + __FUNCTION__);
      bool ans = OrderClose(ticket, OrderLots(), ask, 5);
     }
     
     break;
   }
  }
 }
}

bool OrderCloseEngine::spike(void)
{
  int total = OrdersTotal();
  string ord_symbol = Symbol();
  double upper_band = iBands(ord_symbol, 0, 50, 2.5, 0, PRICE_CLOSE, MODE_UPPER, 0);
  double lower_band = iBands(ord_symbol, 0, 50, 2.5, 0, PRICE_CLOSE, MODE_LOWER, 0); 
  double ma = iMA(ord_symbol, 0, 50, 0, MODE_SMA, PRICE_CLOSE, 0);
  double upper_envelope = iEnvelopes(ord_symbol, 0, 50, MODE_SMA, 0, PRICE_CLOSE, 0.3, MODE_UPPER, 0);
  double lower_envelope = iEnvelopes(ord_symbol, 0, 50, MODE_SMA, 0, PRICE_CLOSE, 0.3, MODE_LOWER, 0);
  double cole_force_index = iCustom(ord_symbol, 0, "Cole Force Index", 13, MODE_SMA, PRICE_CLOSE, 2.5, 55, 0, 0);
  double cole_force_index_upper_ma = iCustom(ord_symbol, 0, "Cole Force Index", 13, MODE_SMA, PRICE_CLOSE, 2.5, 55, 1, 0);
  double cole_force_index_lower_ma = iCustom(ord_symbol, 0, "Cole Force Index", 13, MODE_SMA, PRICE_CLOSE, 2.5, 55, 2, 0);
  double prc = iClose(ord_symbol, 0, 0);
  bool cfi=false, env=false, boll=false;
  double cls = 0;
  int type=-1;
   
  if(prc > ma)
   type = OP_BUY;
  if(prc < ma)
   type = OP_SELL;
   
  switch(type)
  {
   case OP_BUY:
    cls = MarketInfo(ord_symbol, MODE_BID);
     
    if(cole_force_index_upper_ma != INT_MAX && cole_force_index > cole_force_index_upper_ma)
     cfi = true;
     
    if(prc >= upper_band)
     boll = true;
      
    if(prc >= upper_envelope)
     env = true;
     
    break;
     
   case OP_SELL:
     cls = MarketInfo(ord_symbol, MODE_ASK);
     
    if(cole_force_index_lower_ma != INT_MAX && cole_force_index < cole_force_index_lower_ma)
     cfi = true;
     
    if(prc <= lower_band)
     boll = true;
      
    if(prc <= lower_envelope)
     env = true;
     
    break;
  }
 
 if(cfi && env && boll)
 {
  double total_lots = 0;
  datetime earliest_open_order = INT_MAX;
  
  for(int i=total-1; i>=0; i--)
  {
   if(OrderSelect(i,SELECT_BY_POS) && OrderSymbol()==ord_symbol && OrderType()==type)
   { 
    int ticket = OrderTicket();
    sma_set_close(ticket);
   }
  }
  
  return true;
 }
 
 return false;
}

OrderCloseEngine order_close_engine;

//+------------------------------------------------------------------+
double GetSwingHigh(datetime start_time)
{
 int pos = iBarShift(Symbol(), 0, start_time);
 double swing_high = 0;
 
 for(int i=pos; i>=0; i--)
  swing_high = MathMax(swing_high, High[i]);
  
 return swing_high;
}
//+------------------------------------------------------------------+
double GetSwingLow(datetime start_time)
{
 int pos = iBarShift(Symbol(), 0, start_time);
 double swing_low = DBL_MAX;
 
 for(int i=pos; i>=0; i--)
  swing_low = MathMin(swing_low, Low[i]);
  
 return swing_low;
}
//+------------------------------------------------------------------+
int MACrossoverEvent(int ma_period)
 {
  string sym=Symbol();
  double past_ma=iMA(sym,0,ma_period,0,MODE_SMA,PRICE_CLOSE,1);
  double present_ma=iMA(sym,0,ma_period,0,MODE_SMA,PRICE_CLOSE,0);
  
  int ret=GenericCrossoverEvent(past_ma,present_ma);
  
  return(ret);
 }
//+------------------------------------------------------------------+
int GenericCrossoverEvent(double past_obj_close,double present_obj_close,int price_shift=0)
 {
  string symb=Symbol();

  double price_open=iOpen(symb,0,price_shift);
  double price_close=iClose(symb,0,price_shift);
  double price_high=iHigh(symb,0,price_shift);
  double price_low=iLow(symb,0,price_shift);
  
  if(price_open < past_obj_close)
   {
    if(price_close > present_obj_close)
     return(CLOSE_ABOVE);
    else if(price_high > present_obj_close)
     return(HIGH_REJECTED);
    else
     return(NIL);
   }
  
  if(price_open > past_obj_close)
   {
    if(price_close < present_obj_close)
     return(CLOSE_BELOW);
    else if(price_low < present_obj_close)
     return(LOW_REJECTED);
    else
     return(NIL);
   }
   
  if(price_open == past_obj_close)
   {
    if(price_close > present_obj_close)
     return(CLOSE_ABOVE);
    else if(price_high > present_obj_close)
     return(HIGH_REJECTED);
    else if(price_close < present_obj_close)
     return(CLOSE_BELOW);
    else if(price_low < present_obj_close)
     return(LOW_REJECTED);
    else
     return(NIL);
   }
   
  return(NIL);
 }