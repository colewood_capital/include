//+------------------------------------------------------------------+
//|                                                      ScaleIn.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#ifndef ARRAYDOUBLE
#define ARRAYDOUBLE
   #include <Arrays/ArrayDouble.mqh>
#endif 

#ifndef FILEBASE
#define FILEBASE
   #include <FileBase.mqh>
#endif 

#ifndef UTILS
#define UTILS
   #include <utils.mqh>
#endif 

#ifndef UUIDGENERATOR
#define UUIDGENERATOR
   #include <UUIDGenerator.mqh>
#endif 

#ifndef BASEDIR
#define BASEDIR
   input string BASE_DIR = "";
#endif 

Logger scaleinlogger(__FILE__, "helper_processes");

class ConfigDict
{
 private:
  CArrayString p_symbol;
  CArrayString p_name;
  CArrayDouble p_pips;
  CArrayDouble p_percentage;
  CArrayInt p_level;
  CArrayInt p_req_close;
  
  void load(const CArrayString& parsed_file_data, string symbol);
  
 public:
  void get_parameters(string rule_name, string symbol, int level, double& pips, double& percentage);
  ConfigDict();
};
//+------------------------------------------------------------------+
void ConfigDict::load(const CArrayString &parsed_file_data, string symbol)
{
 string name = NULL;
 int level=-1;
 
 for(int i=0; i<parsed_file_data.Total(); i++)
 {
  string PREFIX = "NAME=";
  string line = StringChangeToUpperCase(parsed_file_data[i]);
  string data[];
  StringSplit(line, ',', data);
  int size = ArraySize(data);
  
  if(size==1) {
   if(StringFind(line, PREFIX)==0)
   {
    name = StringSubstr(line, StringLen(PREFIX));
    level=0;
   }
   else
    scaleinlogger.log("Invalid line in config file=" + line, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
  }
  else if(size==2) {
   p_symbol.Add(symbol);
   p_name.Add(name);
   p_level.Add(level);
   p_pips.Add((double)data[0]);
   p_percentage.Add((double)data[1]);
   p_req_close.Add(0);
  }
  else if(size==3) 
  {
   p_symbol.Add(symbol);
   p_name.Add(name);
   p_level.Add(level);
   p_pips.Add((double)data[0]);
   p_percentage.Add((double)data[1]);
   
   string val = StringChangeToUpperCase(data[2]);
   if(StringFind(val,"TRUE") >= 0)
    p_req_close.Add(1);
   else if(StringFind(val,"FALSE") >= 0)
    p_req_close.Add(0);
   else {
    scaleinlogger.log("Invalid line in config file=" + line, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
    p_req_close.Add(0);
   }
  }
  else {
    scaleinlogger.log("Line in config file has invalid number of parameters=" + (string)size + ", line=" + line, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
  }
  
  level++;
 }
}
//+------------------------------------------------------------------+
ConfigDict::ConfigDict()
{
 CArrayString output;
 
 string config_file_path = BASE_DIR + "\\config\\scale_in\\";
 
 FileBase* filemanager = new FileBase(config_file_path, "txt");
 
 string symbol;
 bool found_default_file = false;
 
 while(filemanager.next())
 {
  string filename = filemanager.filename();
  ReadRAWConfig(config_file_path + filename, output); 
  symbol = FileStripExtension(filename);
  load(output, symbol);
  output.Clear();
  
  if(filename=="default.txt")
   found_default_file = true;
   
  scaleinlogger.log("Loaded config file: " + filename, LOG_INFO, __LINE__, __FUNCTION__);
 }
 
 string filename = filemanager.filename();
 ReadRAWConfig(config_file_path + filename, output); 
 symbol = FileStripExtension(filename);
 load(output, symbol);
 output.Clear();
 
 if(filename=="default.txt")
   found_default_file = true;
   
 scaleinlogger.log("Loaded config file: " + filename, LOG_INFO, __LINE__, __FUNCTION__);
 
 delete filemanager;
 
 if(!found_default_file)
   scaleinlogger.log("Could not find default config file in " + config_file_path, LOG_PRIORITY_LOW, __LINE__, __FUNCTION__);
 
 // ---- DEBUG
 scaleinlogger.log(StringConcatenate("internal array total: sym-", p_symbol.Total(),
                   ", name-", p_name.Total(), ", lvl-", p_level.Total(), ", pips-", p_pips.Total(), 
                   ", percent-", p_percentage.Total(), ", req_close-", p_req_close.Total()), LOG_DEBUG, __LINE__, __FUNCTION__);
 
 for(int i=0; i<p_symbol.Total(); i++) {
  scaleinlogger.log("symbol: " + p_symbol[i], LOG_DEBUG, __LINE__, __FUNCTION__, true);
  scaleinlogger.log("lvl: " + (string)p_level[i], LOG_DEBUG, __LINE__, __FUNCTION__, true);
  scaleinlogger.log("name: " + p_name[i], LOG_DEBUG, __LINE__, __FUNCTION__, true);
  scaleinlogger.log("pips: " + (string)p_pips[i], LOG_DEBUG, __LINE__, __FUNCTION__, true);
  scaleinlogger.log("percentage: " + (string)p_percentage[i], LOG_DEBUG, __LINE__, __FUNCTION__, true);
  scaleinlogger.log("required: " + (string)p_req_close[i], LOG_DEBUG, __LINE__, __FUNCTION__, true);
  scaleinlogger.log("------------------------------------", LOG_DEBUG, __LINE__, __FUNCTION__, true);
 }
}
//+------------------------------------------------------------------+
void ConfigDict::get_parameters(string rule_name, string symbol,int level,double &pips,double &percentage)
{
 rule_name = StringChangeToUpperCase(rule_name);
 int total = p_symbol.Total();
 
 for(int i=0; i<total; i++)
 {
  if(p_symbol[i]==symbol)
  {
   if(p_name[i]==rule_name)
   {
    if(p_level[i]==level)
    {
     pips = p_pips[i];
     percentage = p_percentage[i];
     return;
    }
   }
  }
 }
 
 for(int i=0; i<total; i++)
 {
  if(p_symbol[i]=="default")
  {
   if(p_name[i]==rule_name)
   {
    if(p_level[i]==level)
    {
     pips = p_pips[i];
     percentage = p_percentage[i];
     return;
    }
   }
  }
 }
 
 scaleinlogger.log("Config files could not find match for symbol=" + symbol + ", rule_name=" + rule_name + ", level=" + (string)level, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
}

ConfigDict config_dict();

class ScaleIn
{
 private:
  CArrayString p_name;
  CArrayInt p_ticket;
  CArrayDouble original_lots;
  CArrayDouble filled_lots;
  CArrayInt p_level;
  CArrayDouble p_activation_price;
  CArrayDouble p_sl;
  
  int get_level(int ticket);
  void record_open_event(int ticket, double lots, bool& removed_ticket);
  void update(int old_ticket, int new_ticket);
  void remove(int ticket);
  
 public:
  void add(string rule_name, int ticket, double lots);
  void add(string rule_name, int ticket, double lots, double activation_price);
  void run();
};
//+------------------------------------------------------------------+
void ScaleIn::update(int old_ticket,int new_ticket)
{
 int pos = p_ticket.SearchLinear(old_ticket);
 
 if(pos >= 0)
 {
  p_ticket.Update(pos, new_ticket);
 }
 else
  scaleinlogger.log("Could not find #" + (string)old_ticket, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);

}
//+------------------------------------------------------------------+
void ScaleIn::run(void)
{
 int total=p_ticket.Total();
 
 for(int i=total-1; i>=0; i--)
 {
  if(OrderSelect(p_ticket[i], SELECT_BY_TICKET))
  {
   string order_symbol=OrderSymbol();
   double bid = MarketInfo(order_symbol, MODE_BID);
   int level = get_level(p_ticket[i]);
   
   double pips_above_below_activation_price, open_percentage_original_lots;
   config_dict.get_parameters(p_name[i], order_symbol, level, pips_above_below_activation_price, open_percentage_original_lots);
   
   double open_lots = NormalizeDouble(original_lots[i] * open_percentage_original_lots, 2);
   double point = MarketInfo(order_symbol, MODE_POINT);
   double open_price;
   bool update_ticket=false;
   int partial_reopen_ticket = -1;
   string debug_msg;
   uint log_uuid;
   
   switch(OrderType())
   {
    case OP_BUY:
      open_price = p_activation_price[i] + (pips_above_below_activation_price * point * 10);
      log_uuid = uuid_generator.retrive(__FILE__, __LINE__, i);
      debug_msg = StringConcatenate("#", p_ticket[i], " - Checking bid=", bid, " > open_price=", open_price);
      scaleinlogger.log(debug_msg, LOG_DEBUG, __LINE__, __FUNCTION__, false, log_uuid);
      
      if(bid > open_price) {
       string cmt = StringConcatenate("ScaleIn[", p_ticket[i], "], lvl=", level);
       double ask = MarketInfo(order_symbol, MODE_ASK);
       partial_reopen_ticket = OrderSend(order_symbol, OP_BUY, open_lots, ask, 5, p_sl[i], 0, cmt); 
       
       if(partial_reopen_ticket > 0)
       {
        record_open_event(p_ticket[i], open_lots, update_ticket);
       }
       else
       {
        scaleinlogger.log("Failed to open ticket #" + (string)partial_reopen_ticket + ", e=" + (string)GetLastError(), LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
       }
      }
      
      break;
      
    case OP_SELL:
      open_price = p_activation_price[i] - (pips_above_below_activation_price * point * 10);
      log_uuid = uuid_generator.retrive(__FILE__, __LINE__, i);
      debug_msg = StringConcatenate("#", p_ticket[i], " - Checking bid=", bid, " < open_price=", open_price);
      scaleinlogger.log(debug_msg, LOG_DEBUG, __LINE__, __FUNCTION__, false, log_uuid);
      
      if(bid < open_price) {
       string cmt = StringConcatenate("ScaleIn[", p_ticket[i], "], lvl=", level);
       partial_reopen_ticket = OrderSend(order_symbol, OP_SELL, open_lots, bid, 5, p_sl[i], 0, cmt); 
      
       if(partial_reopen_ticket > 0)
       {
        record_open_event(p_ticket[i], open_lots, update_ticket);
       }
       else
       {
        scaleinlogger.log("Failed to open ticket #" + (string)partial_reopen_ticket + ", e=" + (string)GetLastError(), LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
       }
      }
      
      break;
   }
   
   if(update_ticket)
   {
    //this will mess up record_open_event (ie - when to remove ticket)
    //int old_ticket = p_ticket[i];
    //int new_ticket = partial_reopen_ticket;
   // update(old_ticket, new_ticket);
    //scaleinlogger.log("updated ticket from #" + (string)old_ticket + " to #" + (string)new_ticket, LOG_DEBUG, __LINE__, __FUNCTION__);
   }
  }
 }
}
//+------------------------------------------------------------------+
int ScaleIn::get_level(int ticket)
{
 int pos = p_ticket.SearchLinear(ticket);
 
 if(pos >= 0)
 {
  return p_level[pos];
 }
 else
 {
  scaleinlogger.log("Could not find #" + (string)ticket, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
  return -1;
 }
}
//+------------------------------------------------------------------+
void ScaleIn::add(string rule_name, int ticket, double lots)
{
 double activation_price;
 double sl;
 string symbol;
 
 if(p_ticket.SearchLinear(ticket) >= 0) {
  scaleinlogger.log("Ticket #" + (string)ticket + " already added", LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
  return;
 }
 
 if(OrderSelect(ticket,SELECT_BY_TICKET))
 {
  if(OrderCloseTime() > 0) {
   activation_price = OrderClosePrice();
   sl = OrderStopLoss();
   symbol = OrderSymbol();
   p_activation_price.Add(activation_price);
  }
  else {
   scaleinlogger.log("Cannot add a ticket that is still open" + (string)ticket, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
   return;
  }  
 }
 else
 {
  scaleinlogger.log("Could not find/add #" + (string)ticket, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
  return;
 }
 
 p_name.Add(rule_name);
 p_ticket.Add(ticket);
 original_lots.Add(lots);
 filled_lots.Add(0);
 p_level.Add(1);
 p_sl.Add(sl);
 
 scaleinlogger.log("Successfully added #" + (string)ticket + " [" + symbol + "] to queue, rule_name=" + rule_name + ", activation_price=" + (string)activation_price, LOG_INFO, __LINE__, __FUNCTION__);
}
//+------------------------------------------------------------------+
void ScaleIn::add(string rule_name, int ticket, double lots, double activation_price)
{
 double sl;
 string symbol;
 
 if(p_ticket.SearchLinear(ticket) >= 0) {
  scaleinlogger.log("Ticket #" + (string)ticket + " already added", LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
  return;
 }
 
 if(OrderSelect(ticket,SELECT_BY_TICKET))
 {
  if(OrderCloseTime() > 0) {
   sl = OrderStopLoss();
   symbol = OrderSymbol();
  }
  else {
   scaleinlogger.log("Cannot add a ticket that is still open" + (string)ticket, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
   return;
  }
 }
 else
 {
  scaleinlogger.log("Could not find/add #" + (string)ticket, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
  return;
 }
 
 p_name.Add(rule_name);
 p_ticket.Add(ticket);
 original_lots.Add(lots);
 filled_lots.Add(0);
 p_level.Add(1);
 p_activation_price.Add(activation_price);
 p_sl.Add(sl);
 
 scaleinlogger.log("Successfully added #" + (string)ticket + " [" + symbol + "] to queue, rule_name=" + rule_name + ", activation_price=" + (string)activation_price, LOG_INFO, __LINE__, __FUNCTION__);
}
//+------------------------------------------------------------------+
void ScaleIn::record_open_event(int ticket, double lots, bool& update_ticket)
{
 int pos = p_ticket.SearchLinear(ticket);
 
 if(pos >= 0)
 {
  filled_lots.Update(pos, filled_lots[pos] + lots);
  p_level.Update(pos, p_level[pos] + 1);
  
  if(filled_lots[pos] >= original_lots[pos]) {
   scaleinlogger.log("#" + (string)ticket + " filled - removing from queue", LOG_INFO, __LINE__, __FUNCTION__);
   remove(ticket);
   update_ticket = false;
   return;
  }
 }
 else
 {
  scaleinlogger.log("Could not #" + (string)ticket, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
 }
 
 update_ticket = true;
}
//+------------------------------------------------------------------+
void ScaleIn::remove(int ticket) //change to pos
{
 int pos = p_ticket.SearchLinear(ticket);
 
 if(pos >= 0)
 {
  p_name.Delete(pos);
  p_ticket.Delete(pos);
  original_lots.Delete(pos);
  filled_lots.Delete(pos);
  p_level.Delete(pos);
  p_sl.Delete(pos);
  
  scaleinlogger.log("Successfully removed #" + (string)ticket + " from queue", LOG_DEBUG, __LINE__, __FUNCTION__);
 }
 else
 {
  scaleinlogger.log("Could not #" + (string)ticket, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
  return;
 }
}
//+------------------------------------------------------------------+