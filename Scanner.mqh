//+------------------------------------------------------------------+
//|                                                      Scanner.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#include <Arrays/OrderedBin.mqh>

enum SCANNER_RET_CODE { SCANNER_RET_NIL = INT_MIN };

class Scanner
{
 private:
  OrderedBin p_ticket_bin;
 
 public:
  int new_orders(const bool all_symbols=false);
  int new_orders(string comment, const bool all_symbols=false);
  int new_orders(int magic, const bool all_symbols=false);
  
  Scanner() : p_ticket_bin(100){}
};

int Scanner::new_orders(const bool all_symbols)
{
 int total = OrdersTotal();
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(i,SELECT_BY_POS) && (all_symbols || OrderSymbol()==Symbol()))
  {
   int ticket = OrderTicket();
   
   if(!p_ticket_bin.Search(ticket))
   {
    p_ticket_bin.Add(ticket);
    return ticket;
   }
  }
 }
 
 return SCANNER_RET_NIL;
}

int Scanner::new_orders(string comment, const bool all_symbols)
{
 int total = OrdersTotal();
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(i,SELECT_BY_POS) && OrderComment()==comment && (all_symbols || OrderSymbol()==Symbol()))
  {
   int ticket = OrderTicket();
   
   if(!p_ticket_bin.Search(ticket))
   {
    p_ticket_bin.Add(ticket);
    return ticket;
   }
  }
 }
 
 return SCANNER_RET_NIL;
}

int Scanner::new_orders(int magic, const bool all_symbols)
{
 int total = OrdersTotal();
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(i,SELECT_BY_POS) && OrderMagicNumber()==magic && (all_symbols || OrderSymbol()==Symbol()))
  {
   int ticket = OrderTicket();
   
   if(!p_ticket_bin.Search(ticket))
   {
    p_ticket_bin.Add(ticket);
    return ticket;
   }
  }
 }
 
 return SCANNER_RET_NIL;
}

Scanner scanner;