//+------------------------------------------------------------------+
//|                                                     FileBase.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Jamal Cole"
#property strict

#ifndef LOGGER
#define LOGGER
   #define LOGGER_DEPRECATED
   #include <Logger.mqh>
#endif 

#ifndef MAP
#define MAP
   #include <Map.mqh>
#endif 

#define UUID_1       612315631

Logger file_base_logger(__FILE__, "helper_processes");

class FileBase
{
 private:
  string p_filename;
  string p_filepath;
  string p_searchpath;
  long p_search_handle;
  int p_file_read_handle;
  int p_file_write_handle;
  Map<string,int> p_counter_unable_to_open_chart;
  
  bool find_first_file_if_exists();
  void safe_close_file();
  
 public:
  int write_handle();      //Erases file data when called
  int read_handle(bool reset_file_pointer=false);
  bool del();
  bool next();
  string filename()     { return p_filename; }
  
  FileBase(string path, string extension, string tag="");
  ~FileBase() { safe_close_file(); }
};
//-------------------------------------------------------------------+
int FileBase::read_handle(bool reset_file_pointer=false)
{
 string target = StringConcatenate(p_filepath, "\\", p_filename);

 if(p_filename==NULL) {
  file_base_logger.log("p_filename==NULL. Return -1", LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
  return -1;
 }

 safe_close_file();
 
 if(p_file_read_handle < 0)
 {
  p_file_read_handle = FileOpen(target, FILE_READ|FILE_TXT|FILE_SHARE_READ);
  int failed_count = p_counter_unable_to_open_chart[target];
  
  if(p_file_read_handle < 0) 
  {
   LOG_TYPE priority;
   
   if(failed_count >= 3)
    priority = LOG_TYPE_HIGH;
   else
    priority = LOG_TYPE_DEBUG;
    
   file_base_logger.log("unable to open " + target + " for read (" + (string)failed_count + " times), e=" + (string)GetLastError(), priority, __LINE__, __FUNCTION__, false, UUID_1);
   
   p_counter_unable_to_open_chart.set(target, failed_count + 1);
   return -1;
  }
  
  p_counter_unable_to_open_chart.del(target);
  file_base_logger.log("new open=" + p_filename + ". return p_file_read_handle = " + (string)p_file_read_handle, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
 }
 else
  file_base_logger.log(p_filename + " already opened. return p_file_read_handle = " + (string)p_file_read_handle, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
 
 if(reset_file_pointer) {
  if(FileSeek(p_file_read_handle, 0, SEEK_SET))
   file_base_logger.log("Reseting " + p_filename + " file pointer", LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
  else
   file_base_logger.log("Set file pointer error: " + (string)GetLastError() + ", handle=" + (string)p_file_read_handle, LOG_TYPE_HIGH, __LINE__, __FUNCTION__); 
 }
 
 return p_file_read_handle;
}
//-------------------------------------------------------------------+
int FileBase::write_handle()
{
 string target = StringConcatenate(p_filepath, "\\", p_filename);
 
 if(p_filename==NULL)
  return -1;
 
 safe_close_file();
 
 if(p_file_write_handle < 0)
 {
  p_file_write_handle = FileOpen(target, FILE_WRITE|FILE_TXT|FILE_SHARE_READ);
  if(p_file_write_handle < 0)
   file_base_logger.log("unable to open " + target + " for write, e=" + (string)GetLastError(), LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
 
  file_base_logger.log("new open=" + target + ". return p_file_write_handle = " + (string)p_file_write_handle, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
  return p_file_write_handle;
 }
 
 file_base_logger.log(target + " already opened. return p_file_write_handle = " + (string)p_file_write_handle, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
 return p_file_write_handle;
}
//-------------------------------------------------------------------
void FileBase::safe_close_file()
{
 if(p_file_read_handle >=0) {
  FileClose(p_file_read_handle);
  file_base_logger.log("closing [read] file=" + filename() + ", handle=" + (string)p_file_write_handle + " -> -1", LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
  p_file_read_handle = -1;
 }
 
 if(p_file_write_handle >=0) {
  FileClose(p_file_write_handle);
  file_base_logger.log("closing [write] file=" + filename() + ", handle=" + (string)p_file_write_handle + " -> -1", LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
  p_file_write_handle = -1;
 }
}
//-------------------------------------------------------------------
bool FileBase::find_first_file_if_exists()
{
 p_search_handle = FileFindFirst(p_searchpath, p_filename);
 if(p_search_handle != INVALID_HANDLE) {
  file_base_logger.log("return true", LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
  return true;
 }
 
 file_base_logger.log("return false", LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
 return false;
}
//-------------------------------------------------------------------
bool FileBase::next()
{
 if(p_search_handle!=INVALID_HANDLE)
 {
  file_base_logger.log("valid file handle", LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
  safe_close_file();
  
  bool file_exists = FileFindNext(p_search_handle, p_filename); 
  if(file_exists) {
   file_base_logger.log("selecting file=" + p_filename, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
   return True;
  }
 }

 bool file_exists = find_first_file_if_exists();
 if(file_exists)
  file_base_logger.log("selecting file=" + p_filename, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
  
 return False;
}
//-------------------------------------------------------------------
bool FileBase::del()
{
 string target = StringConcatenate(p_filepath, "\\", p_filename);

 safe_close_file();

 if(FileIsExist(target)) 
 {
  bool ans = FileDelete(target);
  
  if(ans) {
   file_base_logger.log("Deleting file = . Set p_filename=NULL" + (string)p_filename, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
   p_filename = NULL;
  }
  else
   file_base_logger.log("Could not delete: " + target + ", e=" + (string)GetLastError(), LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
  
  return ans;
 }
 
 return false;
}
//-------------------------------------------------------------------
FileBase::FileBase(string path, string extension, string tag="")
{
 p_file_read_handle = -1;
 p_file_write_handle = -1;
 p_filepath = path;
 p_searchpath = StringConcatenate(path + "\\*" + tag + "*." + extension);
 
 find_first_file_if_exists();
}