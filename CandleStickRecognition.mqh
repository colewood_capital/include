//+------------------------------------------------------------------+
//|                                       CandleStickRecognition.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+

#property strict

enum TWO_BAR_COMBINATIONS { COMBO_NIL = 0, 
                            COMBO_BULLISH_BREAKOUT, 
                            COMBO_BULLISH_CONSOLIDATION, 
                            COMBO_BULLISH_REVERSAL, 
                            COMBO_BEARISH_BREAKOUT, 
                            COMBO_BEARISH_CONSOLIDATION, 
                            COMBO_BEARISH_REVERSAL };
                            
enum CANDLESTICK_RET_CODES { CANDLE_BULLISH_STRONG_TREND=1, 
                             CANDLE_DOJI=2, 
                             CANDLE_BULLISH_UPPER_REVERSAL=3, 
                             CANDLE_BULLISH_LOWER_REVERSAL=4, 
                             CANDLE_BULLISH_UPPER_CONSOLIDATION=5, 
                             CANDLE_BULLISH_LOWER_CONSOLIDATION=6, 
                             CANDLE_BULLISH_WEAK_TREND=7, 
                             CANDLE_BEARISH_STRONG_TREND=8,
                             CANDLE_BEARISH_UPPER_REVERSAL=9, 
                             CANDLE_BEARISH_LOWER_REVERSAL=10, 
                             CANDLE_BEARISH_UPPER_CONSOLIDATION=11, 
                             CANDLE_BEARISH_LOWER_CONSOLIDATION=12,
                             CANDLE_BEARISH_WEAK_TREND=13 };
                             
enum ENUM_CANDLESTICK {
   CANDLESTICK_NIL = -1,
   CANDLESTICK_STRONG_TREND_HIGHER = 1,
   CANDLESTICK_DOJI,
   CANDLESTICK_UP_PINBAR_HIGHER,
   CANDLESTICK_DOWN_PINBAR_HIGHER,
   CANDLESTICK_UP_CONSOLIDATION_HIGHER,
   CANDLESTICK_DOWN_CONSOLIDATION_HIGHER,
   CANDLESTICK_WEAK_TREND_HIGHER,
   CANDLESTICK_STRONG_TREND_LOWER,
   CANDLESTICK_UP_PINBAR_LOWER,
   CANDLESTICK_DOWN_PINBAR_LOWER,
   CANDLESTICK_UP_CONSOLIDATION_LOWER,
   CANDLESTICK_DOWN_CONSOLIDATION_LOWER,
   CANDLESTICK_WEAK_TREND_LOWER,
};
//+------------------------------------------------------------------+
int CandlestickTwoBarCombo(int index, int timeframe=0)
 {
  int bar_1 = CandlestickRecognition(index+1, timeframe);
  int bar_2 = CandlestickRecognition(index, timeframe);
  
//-------------- Check bullish breakout combination ---------------
  switch(bar_1)
   {
    case CANDLE_BULLISH_STRONG_TREND:
         
         switch(bar_2)
          {
           case CANDLE_BULLISH_STRONG_TREND:
           case CANDLE_BULLISH_UPPER_CONSOLIDATION:
           case CANDLE_BULLISH_WEAK_TREND:
                return(COMBO_BULLISH_BREAKOUT);
          }
          
         break;
          
    case CANDLE_BULLISH_UPPER_CONSOLIDATION:
    
         switch(bar_2)
          {
           case CANDLE_BULLISH_STRONG_TREND:
           case CANDLE_BULLISH_UPPER_CONSOLIDATION:
           case CANDLE_BULLISH_WEAK_TREND:
                return(COMBO_BULLISH_BREAKOUT);
          }
          
         break; 
         
    case CANDLE_BULLISH_WEAK_TREND:
    
         switch(bar_2)
          {
           case CANDLE_BULLISH_STRONG_TREND:
           case CANDLE_BULLISH_UPPER_CONSOLIDATION:
           case CANDLE_BULLISH_WEAK_TREND:
                return(COMBO_BULLISH_BREAKOUT);
          }
          
         break;      
   }
//-------------- Check bullish consolidation combination ---------------
  switch(bar_1)
   {
    case CANDLE_BULLISH_STRONG_TREND:
         
         switch(bar_2)
          {
           case CANDLE_BULLISH_LOWER_REVERSAL:
           case CANDLE_BEARISH_LOWER_REVERSAL:
           case CANDLE_BEARISH_LOWER_CONSOLIDATION:
                return(COMBO_BULLISH_CONSOLIDATION);
          }
          
         break;
         
    case CANDLE_BULLISH_WEAK_TREND:
         
         switch(bar_2)
          {
           case CANDLE_BULLISH_LOWER_REVERSAL:
           case CANDLE_BEARISH_LOWER_REVERSAL:
           case CANDLE_BEARISH_LOWER_CONSOLIDATION:
                return(COMBO_BULLISH_CONSOLIDATION);
          }
          
         break;
   }
//-------------- Check bullish reversal combination  ---------------
  switch(bar_1)
   {
    case CANDLE_BULLISH_STRONG_TREND:
         
         switch(bar_2)
          {
           case CANDLE_BEARISH_STRONG_TREND:
           case CANDLE_BEARISH_WEAK_TREND:
                return(COMBO_BULLISH_REVERSAL);
          }
          
         break;
         
    case CANDLE_BULLISH_WEAK_TREND:
         
         switch(bar_2)
          {
           case CANDLE_BEARISH_STRONG_TREND:
           case CANDLE_BEARISH_WEAK_TREND:
                return(COMBO_BULLISH_REVERSAL);
          }
          
         break;
   } 
//--------------------------------------------------------------
  //--- Check bearish breakout combination
  switch(bar_1)
   {
    case CANDLE_BEARISH_STRONG_TREND:
         
         switch(bar_2)
          {
           case CANDLE_BEARISH_STRONG_TREND:
           case CANDLE_BEARISH_LOWER_CONSOLIDATION:
           case CANDLE_BEARISH_WEAK_TREND:
                return(COMBO_BEARISH_BREAKOUT);
          }
          
         break;
          
    case CANDLE_BEARISH_LOWER_CONSOLIDATION:
    
         switch(bar_2)
          {
           case CANDLE_BEARISH_STRONG_TREND:
           case CANDLE_BEARISH_LOWER_CONSOLIDATION:
           case CANDLE_BEARISH_WEAK_TREND:
                return(COMBO_BEARISH_BREAKOUT);
          }
          
         break; 
         
    case CANDLE_BEARISH_WEAK_TREND:
    
         switch(bar_2)
          {
           case CANDLE_BEARISH_STRONG_TREND:
           case CANDLE_BEARISH_LOWER_CONSOLIDATION:
           case CANDLE_BEARISH_WEAK_TREND:
                return(COMBO_BEARISH_BREAKOUT);
          }
          
         break;      
   }
//-------------- Check bearish consolidation combination ---------------
  switch(bar_1)
   {
    case CANDLE_BEARISH_STRONG_TREND:
         
         switch(bar_2)
          {
           case CANDLE_BULLISH_UPPER_REVERSAL:
           case CANDLE_BULLISH_UPPER_CONSOLIDATION:
           case CANDLE_BEARISH_UPPER_REVERSAL:
                return(COMBO_BEARISH_CONSOLIDATION);
          }
          
         break;
         
    case CANDLE_BEARISH_WEAK_TREND:
         
         switch(bar_2)
          {
           case CANDLE_BULLISH_UPPER_REVERSAL:
           case CANDLE_BULLISH_UPPER_CONSOLIDATION:
           case CANDLE_BEARISH_UPPER_REVERSAL:
                return(COMBO_BEARISH_CONSOLIDATION);
          }
          
         break;
   }
//-------------- Check bullish reversal combination ---------------
  switch(bar_1)
   {
    case CANDLE_BEARISH_STRONG_TREND:
         
         switch(bar_2)
          {
           case CANDLE_BULLISH_STRONG_TREND:
           case CANDLE_BULLISH_WEAK_TREND:
                return(COMBO_BEARISH_REVERSAL);
          }
          
         break;
         
    case CANDLE_BEARISH_WEAK_TREND:
         
         switch(bar_2)
          {
           case CANDLE_BULLISH_STRONG_TREND:
           case CANDLE_BULLISH_WEAK_TREND:
                return(COMBO_BEARISH_REVERSAL);
          }
          
         break;
   }
//---------------- No pattern matches -----------------
   return(COMBO_NIL);
 }
//+------------------------------------------------------------------+
ENUM_CANDLESTICK CandlestickRecognition(int shift, int timeframe=0, int count=1)
 {                      
  return algo(p1(shift,timeframe, count),p2(shift,timeframe, count));
 }
//+------------------------------------------------------------------+
ENUM_CANDLESTICK algo(double p1,double p2)
//will return integer, 1<=i<=13, based on candlestick k, where result=algo(p1(k),p2(k))
 {
  //----------------- parameters
  //for p1
   double x1=32,x2=84;
  //for p2
   double y1=0.33,y2=0.65;
 
  //logic 1
  if(p1>=x2)                                   return(CANDLESTICK_STRONG_TREND_HIGHER);
  
  //logic 2
  if(p1>=-x1 && p1<=x1 && p2>y1 && p2<y2)      return(CANDLESTICK_DOJI);
  
  //logic 3
  if(p1>=0 && p1<=x1 && p2>=y2)                return(CANDLESTICK_UP_PINBAR_HIGHER);
  
  //logic 4
  if(p1>=0 && p1<=x1 && p2>0 && p2<=y1)        return(CANDLESTICK_DOWN_PINBAR_HIGHER);
  
  //logic 5
  if(p1>x1 && p1<x2 && p2>=y2)                 return(CANDLESTICK_UP_CONSOLIDATION_HIGHER);
  
  //logic 6
  if(p1>x1 && p1<x2 && p2>0 && p2<=y1)         return(CANDLESTICK_DOWN_CONSOLIDATION_HIGHER);
  
  //logic 7
  if(p1>x1 && p1<x2 && p2>y1 && p2<y2)         return(CANDLESTICK_WEAK_TREND_HIGHER);
  
  //logic 8
  if(p1<=-x2)                                  return(CANDLESTICK_STRONG_TREND_LOWER);
 
  //logic 9
  if(p1<=0 && p1>=-x1 && p2>=y2)               return(CANDLESTICK_UP_PINBAR_LOWER);
  
  //logic 10
  if(p1<=0 && p1>=-x1 && p2>0 && p2<=y1)       return(CANDLESTICK_DOWN_PINBAR_LOWER);
  
  //logic 11
  if(p1<-x1 && p1>-x2 && p2>=y2)               return(CANDLESTICK_UP_CONSOLIDATION_LOWER);
  
  //logic 12
  if(p1<-x1 && p1>-x2 && p2>0 && p2<=y1)       return(CANDLESTICK_DOWN_CONSOLIDATION_LOWER);
  
  //logic 13
  if(p1<-x1 && p1>-x2 && p2>y1 && p2<y2)       return(CANDLESTICK_WEAK_TREND_LOWER);
  
  if(p1>0 || p2>0)
    Alert("Error: pattern match failed in ",__FUNCSIG__,". p1=", p1," p2=", p2);
    
  return(CANDLESTICK_NIL );
 }
//+------------------------------------------------------------------+
/*count is used to look at previous bars from start_idx and combine their values into one large candlebar */
double p1(int idx_start, int timeframe=0, int count=1)
 {
  string sym=Symbol();
  
  double n1=iClose(sym,timeframe,idx_start);
  double n2=iOpen(sym,timeframe,idx_start+(count-1));
  double body=n1-n2;
  
  double n3=iHigh(sym,timeframe,iHighest(sym,timeframe,MODE_HIGH,count,idx_start));
  double n4=iLow(sym,timeframe,iLowest(sym,timeframe,MODE_LOW,count,idx_start));
  double segment=n3-n4;
  
  if(segment==0)        segment=DBL_EPSILON;
  
  return((body/segment)*100);
 }
//+------------------------------------------------------------------+
/*count is used to look at previous bars from start_idx and combine their values into one large candlebar */
double p2(int idx_start, int timeframe=0, int count=1)
 {
  string sym=Symbol();
  double p2;
  
  //define variables 
  double high=iHigh(sym,timeframe,iHighest(sym,timeframe,MODE_HIGH,count,idx_start));
  double low=iLow(sym,timeframe,iLowest(sym,timeframe,MODE_LOW,count,idx_start));
  double open=iOpen(sym,timeframe,idx_start+(count-1));
  double close=iClose(sym,timeframe,idx_start);
  
  double absolute_center=(high+low)/2;
  double center=(close+open)/2;
  
  //forumla logic
  double r1, r2;
  
  if(center>absolute_center)
   {
    r1=center-absolute_center;
    r2=high-absolute_center;
    
    if(r2==0)        r2=DBL_EPSILON;
    
    p2=0.5+MathAbs(0.5*(r1/r2));
   }
  else if(center<absolute_center)
   {
    r1=absolute_center-center;
    r2=absolute_center-low;
    
    if(r2==0)        r2=DBL_EPSILON;
    
    p2=0.5-MathAbs(0.5*(r1/r2));
   }
  else
   p2=0.5;
   
  return(p2);
 }