//+------------------------------------------------------------------+
//|                                             AutoSaveArrayInt.mqh |
//|                                                         Carm Inc |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Carm Inc"
#property link      "https://www.mql5.com"
#property strict

#ifndef ARRAYINT
#define ARRAYINT
   #include <Arrays\ArrayInt.mqh>
#endif

#ifndef AUTOSAVEARRAYBASE
#define AUTOSAVEARRAYBASE
   #include <Arrays\AutoSaveArrayBase.mqh>
#endif

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
#endif 

class AutoSaveArrayInt : public AutoSaveArrayBase
{
 private:
  CArrayInt p_arr;
  Logger p_logger;
  
 public:
  bool Add(const int element);
  int SearchLinear(const int element) { return p_arr.SearchLinear(element); }
  bool Update(const int index,const int element);
  bool Delete(const int index);
  bool DeleteFile();
  void AutoSaveArrayInt(string filename, string __FILE__macro, bool disjoint_timeframes=true, bool disjoint_symbols=true, bool disjoint_program_names=true);
  
  int operator[](const int index) const { return(p_arr[index]); }
};

//---------------------------------------------
bool AutoSaveArrayInt::Delete(const int index)
{
  bool ans = p_arr.Delete(index);
  
  if(!ans) {
   p_logger.log("Unable to delete index = " + (string)index + ", total=" + (string)p_arr.Total() + ", p_data_total: " + (string)p_data_total, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
   return(false);
  }
 
  return BinaryFileWrite(p_arr, __FUNCTION__);
}
//---------------------------------------------
bool AutoSaveArrayInt::Update(const int index,const int element)
{
  bool ans = p_arr.Update(index, element);
  
  if(!ans) {
   p_logger.log("Unable to update " + (string)element +". Index = " + (string)index, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
   return(false);
  }
  
  return BinaryFileWrite(p_arr, __FUNCTION__);
}
//---------------------------------------------
bool AutoSaveArrayInt::Add(const int element)
{
  bool ans = p_arr.Add(element);

  if(!ans) {
   p_logger.log("Unable to add " + (string)element, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
   return(false);
  }

  return BinaryFileWrite(p_arr, __FUNCTION__);
}
//---------------------------------------------
void AutoSaveArrayInt::AutoSaveArrayInt(string filename, 
                                        string __FILE__macro,
                                        bool disjoint_timeframes=true, 
                                        bool disjoint_symbols=true,
                                        bool disjoint_program_names=true)
 : AutoSaveArrayBase(filename, __FILE__macro, disjoint_timeframes, disjoint_symbols, disjoint_program_names), 
   p_logger(__FILE__, "helper_processes")
{
 int total = 0;
 
 if(FileIsExist(p_filename))
 {
  int temp[];
  BinaryFileRead(p_arr, temp, total);
 }
   
 //--- debug message
  p_logger.log("Elements in filename: " + filename + ", total=" + (string)total, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
  
  for(int i=0; i<total; i++)
   p_logger.log("filename=[" + filename + "] pos(i)=" + (string)i + ", val=" + (string)p_arr[i], LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
}

