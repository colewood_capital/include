//+------------------------------------------------------------------+
//|                                            ArrayDoubleDouble.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#ifndef ARRAYINT
#define ARRAYINT
   #include <Arrays\ArrayInt.mqh>
#endif 

#ifndef ARRAYDOUBLE
#define ARRAYDOUBLE
   #include <Arrays\ArrayDouble.mqh>
#endif

class CArrayDoubleDouble
{
 private:
  CArrayDouble       p_dbl[];
  CArrayInt          p_index;
  int                p_index_count;
  
 public:
  CArrayDouble* New(int index);
  CArrayDouble* Get(int index);
  bool Delete(int index);
  bool UpdateIndex(int old_index, int new_index);
  
  CArrayDoubleDouble();
};
//+------------------------------------------------------------------+
CArrayDoubleDouble::CArrayDoubleDouble()
{
  p_index_count = 0;
}
//+------------------------------------------------------------------+
bool CArrayDoubleDouble::UpdateIndex(int old_index,int new_index)
{
  int pos =  p_index.SearchLinear(old_index);

  if(pos >= 0)
   {
    if(p_index.SearchLinear(new_index) < 0)
     {
       p_index.Update(pos, new_index);
       return(true);
     }
    else
     {
       Alert("Error: could not update element for 'index not unique' in ",__FUNCTION__);
       return(false);
     }
   }
   
  Print("Could not locate index=",old_index," in ",__FUNCTION__);
  return(false); 
}
//+------------------------------------------------------------------+
bool CArrayDoubleDouble::Delete(int index)
{
  int pos =  p_index.SearchLinear(index);
  
  if(pos >= 0)
   {
    //--- Shift p_dbl[] down started from the item to be deleted
    for(int i=pos; i < p_index_count-1; i++)
     {
       p_dbl[i].AssignArray(GetPointer(p_dbl[i+1]));   
     }
     
    //--- Decrement tracker variables
    p_index_count--;
    p_index.Delete(pos);
    
    return(true);
   }
   
  Print("Could not locate index=",index," in ",__FUNCTION__);
  return(false); 
}
//+------------------------------------------------------------------+
CArrayDouble* CArrayDoubleDouble::Get(int index)
{
  int pos = p_index.SearchLinear(index);
 
  if(pos >= 0)
   { 
     return(GetPointer(p_dbl[pos]));
   }
  
  Print("Could not locate index=",index," in ",__FUNCTION__);
  return(NULL);
}
//+------------------------------------------------------------------+
CArrayDouble* CArrayDoubleDouble::New(int index)
{ 
  if(p_index.SearchLinear(index) < 0)
   {
     p_index.Add(index);

     p_index_count++;
     
     ArrayResize(p_dbl, p_index_count);
    
     return(GetPointer(p_dbl[p_index_count-1]));
   }
  else
   {
    Alert("Error: could not create new element for 'index not unique' in ",__FUNCTION__);
    return(NULL);
   }
}