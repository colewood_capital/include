//+------------------------------------------------------------------+
//|                                          AutoSaveArrayDouble.mqh |
//|                                                         Carm Inc |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Carm Inc"
#property link      "https://www.mql5.com"
#property strict

#ifndef ARRAYDOUBLE
#define ARRAYDOUBLE
   #include <Arrays\ArrayDouble.mqh>
#endif

#ifndef ARRAYFILEPATH
   #define ARRAYFILEPATH               "DataTypes\ClassArrays\\"
#endif

class AutoSaveArrayDouble : public CArrayDouble
{
 private:
  string p_filename;
  
 public:
  bool Add(const double element);
  bool Update(const int index,const double element);
  bool Delete(const int index);
  bool DeleteFile();
  void AutoSaveArrayDouble(string filename);
};
//---------------------------------------------
bool AutoSaveArrayDouble::DeleteFile()
{
  if(FileIsExist(p_filename))
   {
    return(FileDelete(p_filename));
   } 
  else   
   {
    Alert("Error: could not delete file '",p_filename,"' in ",__FUNCTION__," for file does not exist");
    return(false);
   }
}
//---------------------------------------------
bool AutoSaveArrayDouble::Delete(const int index)
{
  bool ans = CArrayDouble::Delete(index);
  
  if(!ans) {
   Alert("Error: unable to delete '",m_data[index],"', index = ",index," in ",__FUNCTION__);
   return(false);
  }
  
  int handle = FileOpen(p_filename,FILE_WRITE|FILE_BIN);
  
  if(handle >= 0)
   {
    FileSeek(handle,0,SEEK_SET);
    FileWriteInteger(handle,m_data_total);
    FileWriteArray(handle,m_data);
    FileClose(handle);
    return(true);
   }
  
  Alert("Error: unable to open ",p_filename," in ",__FUNCTION__);
  
  return(false);
}
//---------------------------------------------
bool AutoSaveArrayDouble::Update(const int index,const double element)
{
  bool ans = CArrayDouble::Update(index, element);
  
  if(!ans) {
   Alert("Error: unable to update ",element,". Index = ",index," in ",__FUNCTION__);
   return(false);
  }
  
  int handle = FileOpen(p_filename,FILE_WRITE|FILE_BIN);
  
  if(handle >= 0)
   {
    FileSeek(handle,0,SEEK_SET);
    FileWriteInteger(handle,m_data_total);
    FileWriteArray(handle,m_data);
    FileClose(handle);
    return(true);
   }
  
  Alert("Error: unable to open ",p_filename," in ",__FUNCTION__);
  
  return(false);
}
//---------------------------------------------
bool AutoSaveArrayDouble::Add(const double element)
{
  bool ans = CArrayDouble::Add(element);

  if(!ans) {
   Alert("Error: unable to add ",element," in ",__FUNCTION__);
   return(false);
  }
  
  int handle = FileOpen(p_filename,FILE_WRITE|FILE_BIN);
  
  if(handle >= 0)
   {
    FileSeek(handle,0,SEEK_SET);
    FileWriteInteger(handle,m_data_total);
    FileWriteArray(handle,m_data);
    FileClose(handle);
    return(true);
   }
  
  Alert("Error: unable to open ",p_filename," in ",__FUNCTION__);
  
  return(false);
}
//---------------------------------------------
void AutoSaveArrayDouble::AutoSaveArrayDouble(string filename)
{
  p_filename = ARRAYFILEPATH + filename;
  
  if(FileIsExist(p_filename))
   {
     int handle = FileOpen(p_filename,FILE_READ|FILE_BIN);
     
     m_data_total = FileReadInteger(handle);
     
     FileReadArray(handle, m_data);
     
    //--- re-initialize protected data
     m_data_max=ArraySize(m_data);
     
     FileClose(handle);
   }
}