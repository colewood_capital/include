//+------------------------------------------------------------------+
//|                                                    ArrayList.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#ifndef ARRAYINT
#define ARRAYINT
   #include <Arrays\ArrayInt.mqh>
#endif

#ifndef LOGGER
#define LOGGER
   #define LOGGER_DEPRECATED
   #include <Logger.mqh>
#endif 

template <typename CArrayType>
class CArrayList
{
 private:
  CArrayType         p_list[];
  CArrayInt          p_index;
  int                p_index_count;
  Logger             p_logger;
  
 public:
  CArrayType* New(int index);
  CArrayType* Get(int index);
  bool Delete(int index);
  bool ChangeIndex(int old_index, int new_index);
  
  CArrayList() : p_logger(__FILE__, "helper_processes") { p_index_count = 0; }
};


template <typename CArrayType>
bool CArrayList::ChangeIndex(int old_index,int new_index)
{
  int pos =  p_index.SearchLinear(old_index);

  if(pos >= 0)
   {
    if(p_index.SearchLinear(new_index) < 0)
     {
       p_index.Update(pos, new_index);
       return(true);
     }
    else
     {
       p_logger.log("Error: could not update element for 'index not unique'", LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
       return(false);
     }
   }
   
  p_logger.log("Could not locate index=" + (string)old_index, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
  return(false); 
}


template <typename CArrayType>
bool CArrayList::Delete(int index)
{
  int pos =  p_index.SearchLinear(index);
  Alert("sizeee: ", p_index.Total());
  for(int j=0; j< p_index.Total(); j++)
   Alert(j, ": ", p_index[j]); Alert("STOP C");
  if(pos >= 0)
   {
    //--- Shift p_list[] down started from the item to be deleted
    for(int i=pos; i < p_index_count-1; i++)
     {
       p_list[i].AssignArray(GetPointer(p_list[i+1]));   
     }
     
    //--- Decrement tracker variables
    p_index_count--;
    p_index.Delete(pos);
    Alert("del pos: ", pos);
    if(p_index_count >= 0)
     ArrayResize(p_list, p_index_count);
    
    Alert("c: ", p_index_count);
    return(true);
   }
   
  p_logger.log("Could not locate index=" + (string)index, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
  return(false); 
}


template <typename CArrayType>
CArrayType* CArrayList::Get(int index)
{
  int pos = p_index.SearchLinear(index);
 
  if(pos >= 0)
   { 
     return(GetPointer(p_list[pos]));
   }
  
  p_logger.log("Could not locate index=" + (string)index, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
  return(NULL);
}


template <typename CArrayType>
CArrayType* CArrayList::New(int index)
{ 
  if(p_index.SearchLinear(index) < 0)
   {
     p_index.Add(index);

     p_index_count++;
     
     ArrayResize(p_list, p_index_count);
     
     Alert(__FUNCTION__, " ... ", index, " .... ", p_index_count);
    
     return(GetPointer(p_list[p_index_count-1]));
   }
  else
   {
    p_logger.log("Could not create new element for 'index not unique'", LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
    return(NULL);
   }
}