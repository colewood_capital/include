//+------------------------------------------------------------------+
//|                                            AutoSaveArrayBase.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
#endif 

#ifndef UTILS2
#define UTILS2
   #include <Utils2.mqh>
#endif 

class AutoSaveArrayBase
{
 private:
  string ShowTimeframe(int period);
  Logger p_logger;
  
 protected:
  int p_data_total;
  string p_filename;
  
  template <typename CArrayType, typename CArrayInternalType> 
  void BinaryFileRead(CArrayType& arr, CArrayInternalType& temp_arr[], int& total);
  
  template <typename CArrayType>
  bool BinaryFileWrite(CArrayType& arr, string __FUNCTION__macro);
  
 public:
  virtual bool Add(const int element);
  virtual bool Update(const int index,const int element);
  virtual bool Delete(const int index);
  int Total() { return p_data_total; }
  bool DeleteFile();
  void AutoSaveArrayBase(string filename, string __FILE__macro, bool disjoint_timeframes=true, bool disjoint_symbols=true, bool disjoint_program_names=true);
};

//---------------------------------------------
template <typename CArrayType> 
bool AutoSaveArrayBase::BinaryFileWrite(CArrayType& arr, string __FUNCTION__macro)
{
  int handle = FileOpen(p_filename,FILE_WRITE|FILE_BIN);
  
  if(handle >= 0)
  {
   FileSeek(handle,0,SEEK_SET);
   FileWriteInteger(handle,arr.Total()); 
   FileWriteArray(handle,arr.m_data);
   FileClose(handle);
    
   return(true);
  }
  
  p_logger.log("[" + __FUNCTION__macro + "] Error: unable to open " + p_filename + ", e=" + (string)GetLastError(), LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
  
  return(false);
}

//---------------------------------------------
template <typename CArrayType, typename CArrayInternalType> 
void AutoSaveArrayBase::BinaryFileRead(CArrayType& arr, CArrayInternalType& temp_arr[], int& total)
{
  int handle = FileOpen(p_filename,FILE_READ|FILE_BIN);
     
  total = FileReadInteger(handle);
     
  FileReadArray(handle, temp_arr);
  FileClose(handle);

  for(int i=0; i<total; i++)
   arr.Add(temp_arr[i]);

  arr.SetDataMax(total); 
  
  p_data_total = arr.Total();
}

//---------------------------------------------
bool AutoSaveArrayBase::DeleteFile()
{
 if(FileIsExist(p_filename))
 {
  return(FileDelete(p_filename));
 } 
 else   
 {
  p_logger.log("Error: could not delete file '" + p_filename + "' for file does not exist", LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
  return(false);
 }
}

//---------------------------------------------
void AutoSaveArrayBase::AutoSaveArrayBase(string filename, 
                                          string __FILE__macro, 
                                          bool disjoint_timeframes=true, 
                                          bool disjoint_symbols=true,
                                          bool disjoint_program_names=true)
 : p_logger(__FILE__, "helper_processes")
{
 string res[];
 
 int del = StringFind(filename, ".");
 
 filename = StringSubstr(filename, 0, del) + ".txt";
 
 p_data_total = 0;
 
 StringSplit(__FILE__macro, '.', res);
 
 string program_name = res[0];
 
 StringReplace(program_name, " ", "_");
 
 string timeframe = utils2.ShowTimeframe(Period());
 
 if(timeframe==NULL) 
 {
  timeframe = "UNKNOWN";
  p_logger.log("Could not read timeframe, period=" + (string)Period(), LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
 }
 
 string component_A = (disjoint_program_names) ? program_name + "\\" : NULL;
 
 string component_B = (disjoint_symbols) ? Symbol() + "\\" : NULL;
 
 string component_C = (disjoint_timeframes) ?  timeframe + "\\" : NULL;
 
 p_filename = StringConcatenate("AutoSaveArray\\", component_A, component_B, component_C, filename);
}
