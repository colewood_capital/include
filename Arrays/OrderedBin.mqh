//+------------------------------------------------------------------+
//|                                                   OrderedBin.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

class OrderedBin
{
 protected:
  int m_data[];
  int m_counter;
  int m_data_total;
  
 public:
  void Add(const int element);
  bool Search(const int element) const;
  int Min();
  int Max();
  OrderedBin(int max_size);
}; 
//+------------------------------------------------------------------+
bool OrderedBin::Search(const int element) const
{
 for(int i=0; i<m_data_total; i++)
  if(m_data[i]==element)
   return true;
   
 return false;
}
//+------------------------------------------------------------------+
void OrderedBin::Add(const int element)
{ 
 m_data[m_counter] = element;
 
 if(m_counter == m_data_total-1)
  m_counter = 0;
 else
  m_counter++;
}
//+------------------------------------------------------------------+
OrderedBin::OrderedBin(int max_size=1000)
{
 m_counter = 0;
 m_data_total = max_size;
 ArrayResize(m_data, m_data_total);
}
//+------------------------------------------------------------------+
int OrderedBin::Min()
{
 int min = INT_MAX;

 for(int i=0; i<m_data_total; i++)
  min = MathMin(min, m_data[i]);
  
 return min;
}
//+------------------------------------------------------------------+
int OrderedBin::Max()
{
 int max = INT_MIN;
 
 for(int i=0; i<m_data_total; i++)
  max = MathMax(max, m_data[i]);
  
 return max;
}