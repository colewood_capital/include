//+------------------------------------------------------------------+
//|                                           AutoSaveOrderedBin.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#ifndef ORDEREDBIN
#define ORDEREDBIN
   #include <Arrays\OrderedBin.mqh>
#endif

#ifndef ARRAYFILEPATH
   #define ARRAYFILEPATH               "DataTypes\\ClassArrays\\"
#endif


class AutoSaveOrderedBin : public OrderedBin
{
 private:
  string p_filename;
  
 public:
  void Add(const int element);
  bool DeleteFile();
  void AutoSaveOrderedBin(string filename, int max_size);
};
//---------------------------------------------
bool AutoSaveOrderedBin::DeleteFile()
{
  if(FileIsExist(p_filename))
   {
    return(FileDelete(p_filename));
   } 
  else   
   {
    Alert("Error: could not delete file '",p_filename,"' in ",__FUNCTION__," for file does not exist");
    return(false);
   }
}
//---------------------------------------------
void AutoSaveOrderedBin::Add(const int element)
{
  OrderedBin::Add(element);

  int handle = FileOpen(p_filename,FILE_WRITE|FILE_BIN);
  
  if(handle >= 0)
   {
    FileSeek(handle,0,SEEK_SET);
    FileWriteInteger(handle,m_counter);
    FileWriteArray(handle,m_data);
    FileClose(handle);
   }
  else
   Alert("Error: unable to open ",p_filename," in ",__FUNCTION__, " for e=", GetLastError());
}
//---------------------------------------------
void AutoSaveOrderedBin::AutoSaveOrderedBin(string filename, int max_size)
{
 p_filename = ARRAYFILEPATH + filename;
 
 if(FileIsExist(p_filename))
 {
  int handle = FileOpen(p_filename,FILE_READ|FILE_BIN);
  
  if(handle >= 0)
  { 
   FileSeek(handle,0,SEEK_SET);
   m_counter = MathMin(FileReadInteger(handle), max_size-1);
   m_data_total = max_size;
   FileReadArray(handle,m_data);
   ArrayResize(m_data, m_data_total);
   FileClose(handle);
  }
  else
   Alert("Error: unable to open ",p_filename," in ",__FUNCTION__, " for e=", GetLastError());
 }
 else
 {
  m_counter = 0;
  m_data_total = max_size;
  ArrayResize(m_data, m_data_total);
   
  int handle = FileOpen(p_filename,FILE_WRITE|FILE_BIN);
  if(handle>=0)   FileClose(handle);
 }
}