//+------------------------------------------------------------------+
//|                                                     AutoSave.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#include <Arrays\ArrayInt.mqh>
#include <Arrays\ArrayDouble.mqh>
#include <Arrays\ArrayString.mqh>

#ifndef ARRAYFILEPATH
   #define ARRAYFILEPATH               "DataTypes\ClassArrays\\"
#endif

class AutoSaveArrayString : public CArrayString
{
 private:
  string p_filename;
  
 public:
  bool Add(const string element);
  bool Update(const int index,const string element);
  bool Delete(const int index);
  bool DeleteFile();
  void AutoSaveArrayString(string filename);
  void AutoSaveArrayString(string filename, float max_array_age_in_minutes);
};
//---------------------------------------------
bool AutoSaveArrayString::DeleteFile()
{
  if(FileIsExist(p_filename))
   {
    return(FileDelete(p_filename));
   } 
  else   
   {
    Alert("Error: could not delete file '",p_filename,"' in ",__FUNCTION__," for file does not exist");
    return(false);
   }
}
//---------------------------------------------
bool AutoSaveArrayString::Delete(const int index)
{
  bool ans = CArrayString::Delete(index);
  
  if(!ans) {
   Alert("Error: unable to delete '",m_data[index],"', index = ",index," in ",__FUNCTION__);
   return(false);
  }
  
  int handle = FileOpen(p_filename,FILE_WRITE|FILE_TXT);
  
  if(handle >= 0)
   {
    FileWrite(handle,(int)TimeCurrent());
    FileWrite(handle,m_data_total);
    FileWriteArray(handle,m_data);
    FileClose(handle);
    return(true);
   }
  
  Alert("Error: unable to open ",p_filename," in ",__FUNCTION__);
  
  return(false);
}
//---------------------------------------------
bool AutoSaveArrayString::Update(const int index,const string element)
{
  bool ans = CArrayString::Update(index, element);
  
  if(!ans) {
   Alert("Error: unable to update ",element,". Index = ",index," in ",__FUNCTION__);
   return(false);
  }
  
  int handle = FileOpen(p_filename,FILE_WRITE|FILE_TXT);
  
  if(handle >= 0)
   {
    FileWrite(handle,(int)TimeCurrent());
    FileWrite(handle,m_data_total);
    FileWriteArray(handle,m_data);
    FileClose(handle);
    return(true);
   }
  
  Alert("Error: unable to open ",p_filename," in ",__FUNCTION__);
  
  return(false);
}
//---------------------------------------------
bool AutoSaveArrayString::Add(const string element)
{
  bool ans = CArrayString::Add(element);

  if(!ans) {
   Alert("Error: unable to add ",element," in ",__FUNCTION__);
   return(false);
  }
  
  int handle = FileOpen(p_filename,FILE_WRITE|FILE_TXT);
  
  if(handle >= 0)
   {
    FileWrite(handle,(int)TimeCurrent());
    FileWrite(handle,m_data_total);
    FileWriteArray(handle,m_data);
    FileClose(handle);
    return(true);
   }
  
  Alert("Error: unable to open ",p_filename," in ",__FUNCTION__);
  
  return(false);
}
//---------------------------------------------
void AutoSaveArrayString::AutoSaveArrayString(string filename)
{
  p_filename = ARRAYFILEPATH + filename;
  
  if(FileIsExist(p_filename))
   {
     int handle = FileOpen(p_filename,FILE_READ|FILE_TXT);
     
     int last_save_timestamp = (int)FileReadString(handle);
     
     FileSeek(handle,0,SEEK_SET);
     
     m_data_total = (int)FileReadString(handle);
     
     FileReadArray(handle, m_data);
   
    //--- re-initialize protected data
     m_data_max=ArraySize(m_data);
     
     FileClose(handle);
   }
}
//---------------------------------------------
void AutoSaveArrayString::AutoSaveArrayString(string filename, float max_array_age_in_minutes)
{
  p_filename = ARRAYFILEPATH + filename;
  
  if(FileIsExist(p_filename))
   {
     int handle = FileOpen(p_filename,FILE_READ|FILE_TXT);
     
     if(handle >= 0)
      {
          int last_save_timestamp = (int)FileReadString(handle);

          if(((int)TimeCurrent() - last_save_timestamp) / 60.0 < max_array_age_in_minutes)
           {
              m_data_total = (int)FileReadString(handle);
              
              FileReadArray(handle, m_data);
            
             //--- re-initialize protected data
              m_data_max=ArraySize(m_data);
              
              FileClose(handle);
              
              return;
          } //--- if false fall through
     }
    else
     {
       Alert("Error: could not access '",p_filename," for read, e: ",GetLastError()," in ",__FUNCSIG__);
       ResetLastError();
       return;
     } 
     
    FileClose(handle);
   }
}

class AutoSaveArrayDouble : public CArrayDouble
{
 private:
  string p_filename;
  
 public:
  bool Add(const double element);
  bool Update(const int index,const double element);
  bool Delete(const int index);
  bool DeleteFile();
  void AutoSaveArrayDouble(string filename);
};
//---------------------------------------------
bool AutoSaveArrayDouble::DeleteFile()
{
  if(FileIsExist(p_filename))
   {
    return(FileDelete(p_filename));
   } 
  else   
   {
    Alert("Error: could not delete file '",p_filename,"' in ",__FUNCTION__," for file does not exist");
    return(false);
   }
}
//---------------------------------------------
bool AutoSaveArrayDouble::Delete(const int index)
{
  bool ans = CArrayDouble::Delete(index);
  
  if(!ans) {
   Alert("Error: unable to delete '",m_data[index],"', index = ",index," in ",__FUNCTION__);
   return(false);
  }
  
  int handle = FileOpen(p_filename,FILE_WRITE|FILE_BIN);
  
  if(handle >= 0)
   {
    FileSeek(handle,0,SEEK_SET);
    FileWriteInteger(handle,m_data_total);
    FileWriteArray(handle,m_data);
    FileClose(handle);
    return(true);
   }
  
  Alert("Error: unable to open ",p_filename," in ",__FUNCTION__);
  
  return(false);
}
//---------------------------------------------
bool AutoSaveArrayDouble::Update(const int index,const double element)
{
  bool ans = CArrayDouble::Update(index, element);
  
  if(!ans) {
   Alert("Error: unable to update ",element,". Index = ",index," in ",__FUNCTION__);
   return(false);
  }
  
  int handle = FileOpen(p_filename,FILE_WRITE|FILE_BIN);
  
  if(handle >= 0)
   {
    FileSeek(handle,0,SEEK_SET);
    FileWriteInteger(handle,m_data_total);
    FileWriteArray(handle,m_data);
    FileClose(handle);
    return(true);
   }
  
  Alert("Error: unable to open ",p_filename," in ",__FUNCTION__);
  
  return(false);
}
//---------------------------------------------
bool AutoSaveArrayDouble::Add(const double element)
{
  bool ans = CArrayDouble::Add(element);

  if(!ans) {
   Alert("Error: unable to add ",element," in ",__FUNCTION__);
   return(false);
  }
  
  int handle = FileOpen(p_filename,FILE_WRITE|FILE_BIN);
  
  if(handle >= 0)
   {
    FileSeek(handle,0,SEEK_SET);
    FileWriteInteger(handle,m_data_total);
    FileWriteArray(handle,m_data);
    FileClose(handle);
    return(true);
   }
  
  Alert("Error: unable to open ",p_filename," in ",__FUNCTION__);
  
  return(false);
}
//---------------------------------------------
void AutoSaveArrayDouble::AutoSaveArrayDouble(string filename)
{
  p_filename = ARRAYFILEPATH + filename;
  
  if(FileIsExist(p_filename))
   {
     int handle = FileOpen(p_filename,FILE_READ|FILE_BIN);
     
     m_data_total = FileReadInteger(handle);
     
     FileReadArray(handle, m_data);
     
    //--- re-initialize protected data
     m_data_max=ArraySize(m_data);
     
     FileClose(handle);
   }
}


class AutoSaveArrayInt : public CArrayInt
{
 private:
  string p_filename;
  
 public:
  bool Add(const int element);
  bool Update(const int index,const int element);
  bool Delete(const int index);
  bool DeleteFile();
  void AutoSaveArrayInt(string filename);
};
//---------------------------------------------
bool AutoSaveArrayInt::DeleteFile()
{
  if(FileIsExist(p_filename))
   {
    return(FileDelete(p_filename));
   } 
  else   
   {
    Alert("Error: could not delete file '",p_filename,"' in ",__FUNCTION__," for file does not exist");
    return(false);
   }
}
//---------------------------------------------
bool AutoSaveArrayInt::Delete(const int index)
{
  bool ans = CArrayInt::Delete(index);
  
  if(!ans) {
   Alert("Error: unable to delete '",m_data[index],"', index = ",index," in ",__FUNCTION__);
   return(false);
  }
  
  int handle = FileOpen(p_filename,FILE_WRITE|FILE_BIN);
  
  if(handle >= 0)
   {
    FileSeek(handle,0,SEEK_SET);
    FileWriteInteger(handle,m_data_total);
    FileWriteArray(handle,m_data);
    FileClose(handle);
    return(true);
   }
  
  Alert("Error: unable to open ",p_filename," in ",__FUNCTION__);
  
  return(false);
}
//---------------------------------------------
bool AutoSaveArrayInt::Update(const int index,const int element)
{
  bool ans = CArrayInt::Update(index, element);
  
  if(!ans) {
   Alert("Error: unable to update ",element,". Index = ",index," in ",__FUNCTION__);
   return(false);
  }
  
  int handle = FileOpen(p_filename,FILE_WRITE|FILE_BIN);
  
  if(handle >= 0)
   {
    FileSeek(handle,0,SEEK_SET);
    FileWriteInteger(handle,m_data_total);
    FileWriteArray(handle,m_data);
    FileClose(handle);
    return(true);
   }
  
  Alert("Error: unable to open ",p_filename," in ",__FUNCTION__);
  
  return(false);
}
//---------------------------------------------
bool AutoSaveArrayInt::Add(const int element)
{
  bool ans = CArrayInt::Add(element);

  if(!ans) {
   Alert("Error: unable to add ",element," in ",__FUNCTION__);
   return(false);
  }
  
  int handle = FileOpen(p_filename,FILE_WRITE|FILE_BIN);
  
  if(handle >= 0)
   {
    FileSeek(handle,0,SEEK_SET);
    FileWriteInteger(handle,m_data_total);
    FileWriteArray(handle,m_data);
    FileClose(handle);
    return(true);
   }
  
  Alert("Error: unable to open ",p_filename," in ",__FUNCTION__);
  
  return(false);
}
//---------------------------------------------
void AutoSaveArrayInt::AutoSaveArrayInt(string filename)
{
  p_filename = ARRAYFILEPATH + filename;
  
  if(FileIsExist(p_filename))
   {
     int handle = FileOpen(p_filename,FILE_READ|FILE_BIN);
     
     m_data_total = FileReadInteger(handle);
     
     FileReadArray(handle, m_data);
     
    //--- re-initialize protected data
     m_data_max=ArraySize(m_data);
     
     FileClose(handle);
   }
}