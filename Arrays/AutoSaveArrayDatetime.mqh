//+------------------------------------------------------------------+
//|                                        AutoSaveArrayDatetime.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#ifndef ARRAYDATETIME
#define ARRAYDATETIME
   #include <Arrays\ArrayDatetime.mqh>
#endif

#ifndef ARRAYFILEPATH
   #define ARRAYFILEPATH               "DataTypes\\ClassArrays\\"
#endif


class AutoSaveArrayDatetime : public CArrayDatetime
{
 private:
  string p_filename;
  
 public:
  bool Add(const datetime element);
  bool Update(const int index,const datetime element);
  bool Delete(const int index);
  bool DeleteFile();
  void AutoSaveArrayDatetime(string filename);
};
//---------------------------------------------
bool AutoSaveArrayDatetime::DeleteFile()
{
  if(FileIsExist(p_filename))
   {
    return(FileDelete(p_filename));
   } 
  else   
   {
    Alert("Error: could not delete file '",p_filename,"' in ",__FUNCTION__," for file does not exist");
    return(false);
   }
}
//---------------------------------------------
bool AutoSaveArrayDatetime::Delete(const int index)
{
  bool ans = CArrayDatetime::Delete(index);
  
  if(!ans) {
   Alert("Error: unable to delete '",m_data[index],"', index = ",index," in ",__FUNCTION__);
   return(false);
  }
  
  int handle = FileOpen(p_filename,FILE_WRITE|FILE_BIN);
  
  if(handle >= 0)
   {
    FileSeek(handle,0,SEEK_SET);
    FileWriteInteger(handle,m_data_total);
    FileWriteArray(handle,m_data);
    FileClose(handle);
    return(true);
   }
  
  Alert("Error: unable to open ",p_filename," in ",__FUNCTION__);
  
  return(false);
}
//---------------------------------------------
bool AutoSaveArrayDatetime::Update(const int index,const datetime element)
{
  bool ans = CArrayDatetime::Update(index, element);
  
  if(!ans) {
   Alert("Error: unable to update ",element,". Index = ",index," in ",__FUNCTION__);
   return(false);
  }
  
  int handle = FileOpen(p_filename,FILE_WRITE|FILE_BIN);
  
  if(handle >= 0)
   {
    FileSeek(handle,0,SEEK_SET);
    FileWriteInteger(handle,m_data_total);
    FileWriteArray(handle,m_data);
    FileClose(handle);
    return(true);
   }
  
  Alert("Error: unable to open ",p_filename," in ",__FUNCTION__);
  
  return(false);
}
//---------------------------------------------
bool AutoSaveArrayDatetime::Add(const datetime element)
{
  bool ans = CArrayDatetime::Add(element);

  if(!ans) {
   Alert("Error: unable to add ",element," in ",__FUNCTION__);
   return(false);
  }
  
  int handle = FileOpen(p_filename,FILE_WRITE|FILE_BIN);
  
  if(handle >= 0)
   {
    FileSeek(handle,0,SEEK_SET);
    FileWriteInteger(handle,m_data_total);
    FileWriteArray(handle,m_data);
    FileClose(handle);
    return(true);
   }
  
  Alert("Error: unable to open ",p_filename," in ",__FUNCTION__);
  
  return(false);
}
//---------------------------------------------
void AutoSaveArrayDatetime::AutoSaveArrayDatetime(string filename)
{
  p_filename = ARRAYFILEPATH + filename;
  
  if(FileIsExist(p_filename))
   {
     int handle = FileOpen(p_filename,FILE_READ|FILE_BIN);
     
     m_data_total = FileReadInteger(handle);
     
     FileReadArray(handle, m_data);
     
    //--- re-initialize protected data
     m_data_max=ArraySize(m_data);
     
     FileClose(handle);
   }
}