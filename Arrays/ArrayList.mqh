//+------------------------------------------------------------------+
//|                                                    ArrayList.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#ifndef ARRAYINT
#define ARRAYINT
   #include <Arrays\ArrayInt.mqh>
#endif

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
#endif 

template <typename CArrayType>
class CArrayList
{
 private:
  Logger             logger;
 
 protected:
  CArrayType         p_list[];
  int                p_index_count;
  
 public:
  CArrayType* Add(int& OUT_index);
  CArrayType* Get(int index);
  bool Delete(int index);
  int Total() { return p_index_count; }
  
  CArrayList() : logger(__FILE__, "helper_processes") { p_index_count = 0; }
};

template <typename CArrayType>
bool CArrayList::Delete(int index)
{
  __TRACE__
  
  if(index < p_index_count)
   {
    //--- Shift p_list[] down started from the item to be deleted
    for(int i=index; i < p_index_count-1; i++)
     {
       p_list[i].AssignArray(GetPointer(p_list[i+1]));   
     }
     
    //--- Decrement tracker variables
    p_index_count--;

    if(p_index_count >= 0)
     ArrayResize(p_list, p_index_count);

    return(true);
   }
   
  LOG_HIGH("index=" + (string)index + " >= p_index_total=" + (string)p_index_count);
  return(false); 
}


template <typename CArrayType>
CArrayType* CArrayList::Get(int index)
{
 __TRACE__
 
  if(index >= 0 && index < p_index_count)
   { 
     return(GetPointer(p_list[index]));
   }
  
  LOG_HIGH("Invalid index=" + (string)index + ", p_index_total=" + (string)p_index_count);
  return(NULL);
}


template <typename CArrayType>
CArrayType* CArrayList::Add(int& OUT_index)
{ 
  ArrayResize(p_list, ++p_index_count);
  
  OUT_index = p_index_count-1;
    
  return(GetPointer(p_list[OUT_index]));
}