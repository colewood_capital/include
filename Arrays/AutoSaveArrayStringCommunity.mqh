//+------------------------------------------------------------------+
//|                                 AutoSaveArrayStringCommunity.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict
#property description "Same as AutoSaveArrayString, except allows multiple programs to use shared data structures"

#ifndef ARRAYSTRING
#define ARRAYSTRING
   #include <Arrays\ArrayString.mqh>
#endif

#ifndef ARRAYFILEPATH
   #define ARRAYFILEPATH               "DataTypes\\ClassArrays\\"
#endif

class AutoSaveArrayStringCommunity : public CArrayString
{
 private:
  string p_filename;
  void Load();
  
 public:
  bool Add(const string element);
  bool Update(const int index,const string element);
  bool Delete(const int index);
  bool DeleteFile();
  int  SearchLinear(const string element);
  int  Total(void);
  void AutoSaveArrayStringCommunity(string filename);
  void AutoSaveArrayStringCommunity(string filename, float max_array_age_in_minutes);
};
//---------------------------------------------
int AutoSaveArrayStringCommunity::Total(void)
{
  Load();
  return CArrayString::Total();
}
//---------------------------------------------
int AutoSaveArrayStringCommunity::SearchLinear(const string element) 
{
  Load();
  return CArrayString::SearchLinear(element);
}
//---------------------------------------------
bool AutoSaveArrayStringCommunity::DeleteFile()
{
  Load();
  
  if(FileIsExist(p_filename))
   {
    return(FileDelete(p_filename));
   } 
  else   
   {
    Alert("Error: could not delete file '",p_filename,"' in ",__FUNCTION__," for file does not exist");
    return(false);
   }
}
//---------------------------------------------
bool AutoSaveArrayStringCommunity::Delete(const int index)
{
  Load();
  
  bool ans = CArrayString::Delete(index);
  
  if(!ans) {
   Alert("Error: unable to delete '",m_data[index],"', index = ",index," in ",__FUNCTION__);
   return(false);
  }
  
  int handle = FileOpen(p_filename,FILE_WRITE|FILE_TXT);
  
  if(handle >= 0)
   {
    FileWrite(handle,(int)TimeCurrent());
    FileWrite(handle,m_data_total);
    FileWriteArray(handle,m_data);
    FileClose(handle);
    return(true);
   }
  
  Alert("Error: unable to open ",p_filename," in ",__FUNCTION__," for e = ",GetLastError());
  
  return(false);
}
//---------------------------------------------
bool AutoSaveArrayStringCommunity::Update(const int index,const string element)
{
  Load();
  
  bool ans = CArrayString::Update(index, element);
  
  if(!ans) {
   Alert("Error: unable to update ",element,". Index = ",index," in ",__FUNCTION__);
   return(false);
  }
  
  int handle = FileOpen(p_filename,FILE_WRITE|FILE_TXT);
  
  if(handle >= 0)
   {
    FileWrite(handle,(int)TimeCurrent());
    FileWrite(handle,m_data_total);
    FileWriteArray(handle,m_data);
    FileClose(handle);
    return(true);
   }

  Alert("Error: unable to open ",p_filename," in ",__FUNCTION__," for e = ",GetLastError());
  
  return(false);
}
//---------------------------------------------
bool AutoSaveArrayStringCommunity::Add(const string element)
{
  Load();

  bool ans = CArrayString::Add(element);

  if(!ans) {
   Alert("Error: unable to add ",element," in ",__FUNCTION__); 
   return(false);
  }
  
  int handle = FileOpen(p_filename,FILE_WRITE|FILE_TXT);
  
  if(handle >= 0)
   {
    FileWrite(handle,(int)TimeCurrent());
    FileWrite(handle,m_data_total);
    FileWriteArray(handle,m_data);
    FileClose(handle);
    return(true);
   }

  Alert("Error: unable to open ",p_filename," in ",__FUNCTION__," for e = ",GetLastError());
  
  return(false);
}
//---------------------------------------------
void AutoSaveArrayStringCommunity::AutoSaveArrayStringCommunity(string filename, float max_array_age_in_minutes)
{
  p_filename = ARRAYFILEPATH + filename;
  
  if(FileIsExist(p_filename))
   {
     int handle = FileOpen(p_filename,FILE_READ|FILE_TXT);
     
     if(handle >= 0)
      {
          int last_save_timestamp = (int)FileReadString(handle);

          if(((int)TimeCurrent() - last_save_timestamp) / 60.0 < max_array_age_in_minutes)
           {
              m_data_total = (int)FileReadString(handle);
              
              FileReadArray(handle, m_data);
            
             //--- re-initialize protected data
              m_data_max=ArraySize(m_data);
              
              FileClose(handle);
              
              return;
          } //--- if false fall through
     }
    else
     {
       Alert("Error: could not access '",p_filename," for read, e: ",GetLastError()," in ",__FUNCSIG__);
       return;
     } 
     
    FileClose(handle);
   }
}

//---------------------------------------------
void AutoSaveArrayStringCommunity::Load()
{
  if(FileIsExist(p_filename))
  {
   int handle = FileOpen(p_filename,FILE_READ|FILE_TXT);
     
   FileSeek(handle,0,SEEK_SET);
   
   int last_save_timestamp = (int)FileReadString(handle);
     
   m_data_total = (int)FileReadString(handle);
     
   FileReadArray(handle, m_data);
   
   //--- re-initialize protected data
    m_data_max=ArraySize(m_data);
     
   FileClose(handle);
  }
}
//---------------------------------------------
void AutoSaveArrayStringCommunity::AutoSaveArrayStringCommunity(string filename)
{
  p_filename = ARRAYFILEPATH + filename;
  Load();
}