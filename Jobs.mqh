//+------------------------------------------------------------------+
//|                                                         Jobs.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#ifndef UTILS
#define UTILS
   #include <Utils.mqh>
#endif

#ifndef ARRAYDOUBLE
#define ARRAYDOUBLE
   #include <Arrays/ArrayDouble.mqh>
#endif 

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
#endif 

Logger jobs_logger(__FILE__, "helper_processes");

#ifndef IPADDRESS
#define IPADDRESS
   input string IP_ADDRESS = "http://127.0.0.1";
#endif 

#ifndef JOBS
#define JOBS
   input string URL_GET_JOBS = "/jobs";
   int primary_key;
#endif 

CArrayString job_variable;
CArrayString job_comparison;
CArrayDouble job_value;
CArrayString job_logical_operator;
CArrayInt job_result;

string cmt_jobs;

struct Job
{
 int id;
 string variable;
 string comparison;
 double value;
 string logical_operator;
 bool result;
 
 bool all_set();
 string display();
 Job();
};

Job::Job(void) : id(-1), variable(NULL), comparison(NULL), value(DBL_MIN), result(false)
{
}

bool Job::all_set()
{
 if(id >= 0 && variable != NULL && comparison != NULL && value != DBL_MIN)
  return true;
  
 return false;
}

string Job::display()
{
 return variable + " " + comparison + " " + (string)value + ", logical=" + logical_operator;
}

Job new_job[];

void FetchJob(string& _ids)
{
 cmt_jobs = "";
 _ids = "";
 string headers;
 char post[], response[];
 string data = "acctNumber=" + (string)AccountNumber() + ";pcName=" + GetMachineName() + ";";

 StringToCharArray(data, post, 0, WHOLE_ARRAY, CP_UTF8);
 int res = WebRequest("GET", StringConcatenate(IP_ADDRESS, URL_GET_JOBS, "/get/", primary_key, "/"),
            "", NULL, 15000, post, ArraySize(post), response, headers);
            
 jobs_logger.log("query result: " + (string)res, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
 
 if(res==200)
 {
  string job[];
  string response_str = CharArrayToString(response);
  
  if(response_str == "wait") {
   string msg = "Waiting for another job to complete";
   jobs_logger.log(msg, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
   cmt_jobs = msg;
   return;
  }
  
  if(response_str == "empty") {
   string msg = "Job queue is empty";
   jobs_logger.log(msg, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
   cmt_jobs = msg;
   return;
  }
  
  if(response_str == "no_mirror") {
   string msg = "Please link mirror terminal to a demo / live terminal";
   jobs_logger.log(msg, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
   cmt_jobs = msg;
   return;
  }
  
  StringSplit(response_str, ';', job);
  int job_count = ArraySize(job) - 1;
  ArrayResize(new_job, job_count);
  CArrayString key;
  CArrayString value;
  
  if(job_count == 0) {
   jobs_logger.log("no jobs found", LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
   return;
  }

  for(int i=0; i<job_count; i++)
  {
   if(StringLen(job[i]) > 0) 
   {
    SplitArgs(job[i], key, value, ',', '=');
    
    for(int j=0; j<key.Total(); j++) 
    {
     if(key[j]=="id") {
      new_job[i].id = (int)value[j];
      _ids += (string)new_job[i].id + ", ";
     }
     
     else if(key[j]=="variable")
      new_job[i].variable = value[j];
      
     else if(key[j]=="comparison")
      new_job[i].comparison = value[j];
      
     else if(key[j]=="value")
      new_job[i].value = (double)value[j];
      
     else if(key[j]=="operator")
      new_job[i].logical_operator = value[j];
      
     else
      ;
    }
   }
  }
  
  string ret_msg = NULL;
  for(int i=0; i<job_count; i++)
  {
   if(!new_job[i].all_set()) {
    jobs_logger.log("could not set: " + new_job[i].display(), LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
    cmt_jobs = "no jobs found";
    return;
   }
   else {
    ret_msg += new_job[i].display() + "; ";
    cmt_jobs += new_job[i].display() + "; ";
   }
  }
    
  jobs_logger.log("new job created: " + ret_msg, LOG_TYPE_INFO, __LINE__, __FUNCTION__);
 }
}

bool CheckJobStatus(string& id)
{
 int sz = ArraySize(new_job);
 for(int i=0; i<sz; i++)
  CheckJob(new_job[i]);
  
 for(int i=0; i<sz; i++)
 {
  if(new_job[i].logical_operator == "and" && new_job[i].result == false) 
   return false;
   
  id += (string)new_job[i].id + ", ";
 }
 
 for(int i=0; i<sz; i++)
 {
  if(new_job[i].logical_operator != "and" && new_job[i].result == true) {
   jobs_logger.log("Job conditions satisfied", LOG_TYPE_INFO, __LINE__, __FUNCTION__);
   return true;
  }
 }
 
 return false;
}

void CheckJob(Job& j)
{
 if(j.variable == "equity")
 {
  if(j.comparison == "gte") 
  {
   if(AccountEquity() >= j.value)
    j.result = true;
   else
    j.result = false;
    
   return;
  }
  
  if(j.comparison == "lte")
  {
   if(AccountEquity() <= j.value)
    j.result = true;
   else
    j.result = false;
    
   return;
  }
  
  jobs_logger.log("Unknown comparison = " + j.comparison, LOG_TYPE_LOW, __LINE__, __FUNCTION__);
 }
 
 jobs_logger.log("Unknown variable = " + j.variable, LOG_TYPE_LOW, __LINE__, __FUNCTION__);
}

void ResetLastJob()
{
 ArrayResize(new_job, 0);
}