//+------------------------------------------------------------------+
//|                                               site_reporting.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#ifndef AUTOSAVEARRAYSTRING
#define AUTOSAVEARRAYSTRING
   #include <Arrays/AutoSaveArrayString.mqh>
#endif 

#ifndef AUTOSAVEARRAYINT
#define AUTOSAVEARRAYINT
   #include <Arrays/AutoSaveArrayInt.mqh>
#endif

#ifndef AUTOSAVEARRAYDOUBLE
#define AUTOSAVEARRAYDOUBLE
   #include <Arrays/AutoSaveArrayDouble.mqh>
#endif

#ifndef AUTOSAVEARRAYDATETIME
#define AUTOSAVEARRAYDATETIME
   #include <Arrays/AutoSaveArrayDatetime.mqh>
#endif

#ifndef AUTOSAVEORDEREDBIN
#define AUTOSAVEORDEREDBIN
   #include <Arrays/AutoSaveOrderedBin.mqh>
#endif

#ifndef MT4GUI2
#define MT4GUI2
   #include <mt4gui2.mqh>
#endif 

#ifndef UTILS
#define UTILS
   #include <utils.mqh>
#endif

#ifndef BASEDIR
#define BASEDIR
   input string BASE_DIR = "";
#endif

#ifndef FILEMESSENGER
#define FILEMESSENGER   
   #include <FileMessenger.mqh>
#endif 

#ifndef IPADDRESS
#define IPADDRESS
   input string IP_ADDRESS = "http://127.0.0.1";
#endif 

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
#endif 

#define OK_200         200 

// global variables
input string URL_LOGIN = "/accounts/login";
input string URL_GET_JOBS = "/jobs";
input double DOLLAR_PL_UPDATE_THRESHOLD = 0;
const int MILLISECOND_SET_TIMER = 300;
string FILE_DELIMITER = "\n";
string QUEUE_SYNC_PATH = BASE_DIR + "\\queue";
string SERVER_SYNC_PATH = BASE_DIR + "\\python\\queue";
Logger logger(__FILE__, "helper_processes");
FileMessenger messenger(SERVER_SYNC_PATH);
int primary_key;
string job_ids;

#ifndef JOBS
#define JOBS
   #include <Jobs.mqh>
#endif 

// global variables - disable trading GUI
int hwnd = 0; 
int panel1,display_panel,symbol_list,add_button,remove_button;
int symbols_market_watch_total;
AutoSaveArrayStringCommunity disable_trading_symbol("disable_trading_symbol");

ENUM_INIT_RETCODE Init()
{
  //--- create folder for python server
   FolderCreate(SERVER_SYNC_PATH);

  //--- login to server or create a new account
   string headers;
   char post[], result[];
   string data = "acctNumber=" + (string)AccountNumber() + 
                 ";acctServer=" + GetServerName() + 
                 ";acctName=" + AccountName() +
                 ";acctType=mirror" +
                 ";pcName=" + GetMachineName() + 
                 ";baseDir=" + BASE_DIR + 
                 ";terminalPath=" + TerminalInfoString(TERMINAL_PATH) +
                 ";terminalDataPath=" + TerminalInfoString(TERMINAL_DATA_PATH) + 
                 ";terminalCommonDataPath=" + TerminalInfoString(TERMINAL_COMMONDATA_PATH) + ";";
                 
   string response;
   
   if(BASE_DIR=="")
   {
    string msg = "Init failed for BASE_DIR not set!!!";
    MessageBox(msg, "CRITICAL ERROR", MB_OK|MB_ICONERROR);
    logger.log(msg, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
    return(INIT_FAILED);
   }
   
   ResetLastError();

   StringToCharArray(data, post, 0, WHOLE_ARRAY, CP_UTF8);
   
   int res = WebRequest("POST", IP_ADDRESS + URL_LOGIN, "", NULL, 10000, post, ArraySize(post), result, headers);
   
   if(res!=OK_200) {
    logger.log(StringConcatenate("Error: ", GetLastError()), LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
    return(INIT_FAILED);
   }
   
   response = CharArrayToString(result);
   string message[];
   StringReplace(response,"\"","");
   StringSplit(response,';',message);
   
   logger.log(StringConcatenate("Web server response code: " , res), LOG_INFO, __LINE__, __FUNCTION__);
   logger.log(StringConcatenate("Web server response: ", message[0]), LOG_INFO, __LINE__, __FUNCTION__);
   
   if(ArraySize(message) < 2) {
    string msg = "Init failed for did not receive inital response from server!!!";
    MessageBox(msg, "CRITICAL ERROR", MB_OK|MB_ICONERROR);
    logger.log(msg, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
    return(INIT_PARAMETERS_INCORRECT);
   }
  
   primary_key = (int)message[1];
   logger.log(StringConcatenate("Acquired pk = ", primary_key), LOG_DEBUG, __LINE__, __FUNCTION__);
   
   DisableGUIinit();
   
   FetchJob(job_ids);
   
   return(INIT_SUCCEEDED);
}

void Timer()
{
    
//---- UPDATE DISABLE TRADING GUI------------------------------------|
    DisableGUIengine();

//---- UPDATE JOBS --------------------------------------------------|
    string id;
    if(CheckJobStatus(id)) {
     string headers;
     char post[], result[];
     string msg = "ids=" + (string)id + ";result=completed;";
     StringToCharArray(msg, post, 0, WHOLE_ARRAY, CP_UTF8);
     int response = WebRequest("POST", StringConcatenate(IP_ADDRESS, URL_GET_JOBS, "/set/")
                                    , "", NULL, 8000, post, ArraySize(post), result, headers);
     if(response == OK_200) {
       logger.log("job server response: " + (string)response, LOG_DEBUG, __LINE__, __FUNCTION__);
       job_ids = NULL;
       cmt_jobs = NULL;
     }
     else {
       response = (response < 0) ? GetLastError() : response;
       logger.log("job server response: " + (string)response, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
      }
     
     ResetLastJob();
     //FetchJob();
    }
    
   if(GlobalVariableGet("fetch_new_job") == -1) {
    string res[];
    FetchJob(job_ids);
    
    int pos = StringSplit(job_ids,',',res);
    if(pos > 0) {
     int fetch_id = (int)res[0];
     if(MathIsValidNumber(fetch_id))
      GlobalVariableSet("fetch_new_job", fetch_id);
     else {
      logger.log("Invalid id=" + (string)fetch_id + ", job_ids=" + job_ids, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
      GlobalVariableSet("fetch_new_job", 0);
     }
    }
    else
     GlobalVariableSet("fetch_new_job", 0);
   }

//---- ASSERTION ----------------------------------------------------| 
    if(OrdersTotal() > 99)
     logger.log("ASSERTION max trades < 150 failed !!!", LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
  
//---- Update last update time
    Comment(StringConcatenate("Last Update: ",TimeLocal(),"\nJob ID(s): ", job_ids, "\n", cmt_jobs,"\n"));
    GlobalVariableSet(GetEAName(__FILE__) + "_last_update", TimeCurrent());
}
//+------------------------------------------------------------------+
string RefreshDisabledSymbolsStr()
{
 int total = disable_trading_symbol.Total();
 string ret="";
 
 for(int i=0; i<total; i++) {
  ret += disable_trading_symbol[i] + "\n";
  
  if(disable_trading_symbol[i] == "Disable all symbols")
   return "Trading is disabled";
 }
  
 return ret;
}
//+------------------------------------------------------------------+
void RefreshSymbolListBox()
{
  CArrayString symbols;
  guiRemoveListAll(hwnd, symbol_list);
	
  //-- true -> only symbols in market watch
	int symbols_total = SymbolsTotal(true);
	
   for(int i=0; i<symbols_total; i++)
    symbols.Add(SymbolName(i, true));
   
   symbols.Sort(); 
   symbols.Insert("Disable all symbols", 0);
   symbols_total = symbols.Total();
   
   for(int i=0; i<symbols_total; i++)
    guiAddListItem(hwnd, symbol_list, symbols[i]);
}
//+------------------------------------------------------------------+
void DisableGUIinit()
{
  hwnd = WindowHandle(Symbol(),Period());
  symbols_market_watch_total = SymbolsTotal(true);
  guiRemoveAll(hwnd);	
  // Build the panel
  panel1 = guiAdd(hwnd,"label",30,50,250,120," Select Symbols to Disable"); guiSetTextColor(hwnd,panel1,Black);
  guiSetBgColor(hwnd,panel1,Orange);
  display_panel = guiAdd(hwnd,"label",30,170,250,250,RefreshDisabledSymbolsStr());
  guiSetBgColor(hwnd,display_panel,White);
  // Add buttons to panel
  add_button = guiAdd(hwnd,"button",35,100,240,25,"Add"); guiSetBorderColor(hwnd,add_button,Moccasin);
  remove_button = guiAdd(hwnd,"button",35,130,240,25,"Remove"); guiSetBorderColor(hwnd,remove_button,Chocolate);
  // Add ListBox
  symbol_list = guiAdd(hwnd,"list",35,70,240,150,"");
  RefreshSymbolListBox();
}
//+------------------------------------------------------------------+
void DisableGUIdeinit()
{
  if (hwnd>0) guiRemoveAll(hwnd); 
  // Call this as last mt4gui function
  guiCleanup(hwnd);
} 
//+------------------------------------------------------------------+
void DisableGUIengine()
{
   // Update symbols list
   int t = SymbolsTotal(true);
   if(symbols_market_watch_total != t) {
    RefreshSymbolListBox();
    symbols_market_watch_total = t;
   }

	// Button "Add Symbol"
	if (guiIsClicked(hwnd,add_button))
			{
				// We require that at least one item has been selected
				if (guiGetListSel(hwnd,symbol_list)>-1)
					{
						// We can read the selected items index by guiGetListSel
						// and Selected Item using guiGetText() function
						string selected_symbol = guiGetText(hwnd,symbol_list);
						disable_trading_symbol.Add(selected_symbol);
						guiSetText(hwnd, display_panel, RefreshDisabledSymbolsStr(), 0, "");
                  PlaySound("tick.wav");							
					}
				
			}

	
	// Button "Remove Symbol"
	if (guiIsClicked(hwnd,remove_button))
			{
				// We require that at least one item has been selected
				if (guiGetListSel(hwnd,symbol_list)>-1)
					{
						// We can read the selected items index by guiGetListSel
						// and Selected Item using guiGetText() function
						string selected_symbol = guiGetText(hwnd,symbol_list);
						int pos = disable_trading_symbol.SearchLinear(selected_symbol);
						if(pos >= 0)
						{
						 disable_trading_symbol.Delete(pos);
						 guiSetText(hwnd, display_panel, RefreshDisabledSymbolsStr(), 0, "");
                   PlaySound("tick.wav");
                  }
                  else if("Disable all symbols")
                  {
                   int total = disable_trading_symbol.Total();
                   for(int i=total-1; i>=0; i--)
                    disable_trading_symbol.Delete(i);
                   
                   guiSetText(hwnd, display_panel, RefreshDisabledSymbolsStr(), 0, "");
                   PlaySound("tick.wav");
                  }
                  else
                   PlaySound("timeout.wav");
					}
				
			}
}
//+---------------------------------------------------------------------------------------------+
void DeInit()
{
  DisableGUIdeinit();
}