//+------------------------------------------------------------------+
//|                                                      Logging.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
#endif 

//#include <site_handle_requests.mqh>

class C                                  
{
 private:
  Logger logger;
  
 public:
  void run() 
  { 
   BEGIN
   LOG_WARNING("class C run");  
  }
  
  C() : logger(__FILE__, "Logging.mq4") {};
};