//+------------------------------------------------------------------+
//|                                                    Functions.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#ifndef ENUMS
#define ENUMS
   #include <Super\Enums.mqh>
#endif 

string get_glb_var_name(ENUM_OPERATION operation, string symbol, int cmd)
{
 string type="";
 string action;
 
 if(operation==OPERATION_OPEN_TRADE)
  action="OPEN-";
 else if(operation==OPERATION_CLOSE_TRADE)
  action="CLOSE-";
 else
  action="";
  
 if(cmd==OP_BUY)
 {
  return "DISABLE-" + action + symbol + "-BUY"; 
 }
 
 if(cmd==OP_SELL)
 {
  return "DISABLE-" + action + symbol + "-SELL";
 }
 
 return NULL;
}

bool _isEnabledClose(string symbol, int cmd)
{
 string glb_var_name=get_glb_var_name(OPERATION_CLOSE_TRADE, symbol, cmd);

 return !GlobalVariableCheck(glb_var_name);
}

bool _isEnabledOpen(string symbol, int cmd)
{
 string glb_var_name=get_glb_var_name(OPERATION_OPEN_TRADE, symbol, cmd);

 return !GlobalVariableCheck(glb_var_name);
}