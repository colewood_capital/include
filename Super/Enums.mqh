//+------------------------------------------------------------------+
//|                                                        Enums.mqh |
//|                                                       Jamal Cole |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Jamal Cole"
#property link      "https://www.mql5.com"
#property strict
      
enum LOG_TYPE     { LOG_TYPE_DEBUG,
                    LOG_TYPE_EXTENSIVE,
                    LOG_TYPE_INFO,
                    LOG_TYPE_WARNING,
                    LOG_TYPE_DEV_WARNING,
                    LOG_TYPE_LOW,
                    LOG_TYPE_HIGH, 
                    LOG_TYPE_SYSTEM,
                    LOG_TYPE_NETWORK};
                    
#define SITE_REPORTING_PATH      "SiteReporting\\logger\\"

string DB_UPDATE_FOLDER_NAME="SiteReporting";
//string DB_UPDATE_FOLDER_NAME="SiteReporting" + "\\queue";

#define HOST_SEUOL      "52.192.37.22"
                    
#ifndef MAP
#define MAP
   #include <Map.mqh>
#endif 

#define KwargsDict Map<string,string>

#define REENTRY_PREFIX     "ReEntry"

#define LEVELS_BUY_PREFIX       "Levels - Buy"
#define LEVELS_SELL_PREFIX      "Levels - Sell"
#define STD_END_MSG             "finished successfully"
#define BREAK_TEST               "UNITTESTS_STOP_TEST"

enum ENUM_CHARTEVENT {
   CHARTEVENT_CREATE_UPPER_BAND,
   CHARTEVENT_CREATE_LOWER_BAND,
   CHARTEVENT_CREATE_LEVEL_BUY,
   CHARTEVENT_CREATE_LEVEL_SELL,
   CHARTEVENT_CREATE_LEVEL_BUY_SL,
   CHARTEVENT_CREATE_LEVEL_SELL_SL,
   CHARTEVENT_EXPERT_REMOVE,
   
  //--- Trading Engine
   CHARTEVENT_TRADE_OPEN,
   CHARTEVENT_TRADE_CLOSE,
   CHARTEVENT_TRADE_MODIFY,
   CHARTEVENT_CONFIRM_OPEN,
   CHARTEVENT_CONFIRM_CLOSE,
   
  //--- Signals ENUMS
   SIGNALS_PSAR_TOUCH_BELOW,
   SIGNALS_PSAR_TOUCH_ABOVE,
   SIGNALS_PSAR_NIL,
   SIGNALS_FRACTAL_UPPER,
   SIGNALS_FRACTAL_LOWER,
   SIGNALS_FRACTAL_NIL,
   
   SIGNALS_RETRACE,
   SIGNALS_EXTENSION,
   
  //--- site_reporting
   CHARTEVENT_UPDATE_TRADES,
   CHARTEVENT_HEARTBEAT
};


enum ENUM_CUSTOM_ERRORS {
   ERR_TRADING_DISABLED_ENTRY_BANDS=100,
   ERR_TRADING_DISABLED_LONG,
   ERR_TRADING_DISABLED_SHORT,
};

enum ENUM_MAGIC_NUMBERS {
   MAGIC_NUMBER_BAR_CLOSE_STOP_LOSS=56312,
   MAGIC_NUMBER_CANDLE_RECOGNITION
};

enum ENUM_OPERATION
{
 OPERATION_OPEN_TRADE,
 OPERATION_CLOSE_TRADE,
};
