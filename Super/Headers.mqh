//+------------------------------------------------------------------+
//|                                                      Headers.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

string URL_LOGIN = "/accounts/login";
string URL_GET_ACCOUNT = "/accounts/get";
string URL_SET_EQUITY = "/accounts/setequity";
string URL_GET_ALL_BOOKNUMS = "/trades/all";
string URL_QUERY_TRADES = "/trades/query";
string URL_SET_CLOSED_TRADES = "/trades/set";
string URL_SET_SYMBOL_SET = "/symbolset/new";
string URL_SEND_TICK_DATA = "/tick_data/new";
string URL_SEND_EMAIL = "/email/send";
string URL_POST_LOG = "/logs/new";
string URL_GET_CONFIG = "/config/strategy";


#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
#endif

#ifndef MAP
#define MAP
   #include <Map.mqh>
#endif 

#define POSTDATA              Map<string,string>

#define GLB_HOST_PREFIX              "site_HOST_"
#define GLB_CONFIG_INIT_STATUS       "config_INIT_STATUS"

enum ASYNC_REQUEST { ASYNC_REQUEST_PRICE, ASYNC_REQUEST_TEST };

enum ASYNC_RESPONSE { ASYNC_RESPONSE_FAILED=0, ASYNC_RESPONSE_SUCCEEDED=1 };

struct AsyncRequest
{
 ASYNC_REQUEST type;
 char p_args[100];
 char serverName[100];
 char acctName[100];
 
 string get_args()
 {
  return CharArrayToString(p_args);
 }

 AsyncRequest(string accountName, ASYNC_REQUEST IN_type, string IN_args) {
  type = IN_type;
  StringToCharArray(IN_args, p_args);
  StringToCharArray(AccountServer(), serverName);
  StringToCharArray(accountName, acctName);
 }
 
 AsyncRequest() {};
};

class Credentials
{
private:
 Logger logger;
 
protected:
 int web_post(string http_host_name, string data, string &response);
 string p_host_reporting;
 
public:
 string acct_num;
 string passwd;
 string server_host;
 string server_name;
 
 bool is_valid;
 
 Credentials(string host_reporting, string mt4_acct_name);
};

int Credentials::web_post(string http_full_host_name, string data, string &response)
{
 __TRACE__
 
  string headers;
  char content[], result[];
  bool allow_resend_request=true;
 
  StringToCharArray(data, content, 0, WHOLE_ARRAY, CP_UTF8);
  
  LOG_EXTENSIVE("send post to: " + http_full_host_name + ", data=" + data);
  
  int ret_code = WebRequest("POST", http_full_host_name,
               "", NULL, 10000, content, ArraySize(content), result, headers);
  
  response = CharArrayToString(result);
  
  if(ret_code != 200)
  {
   LOG_HIGH_ERROR("Check web server connection. Failed to fetch account number, using http_full_host_name=" + http_full_host_name + ", data=" + data);
    
   if(StringLen(response) > 0)
     {
      if(StringFind(response, "\n") < 0)
       LOG_HIGH_NO_SITE("[BAD RESPONSE]: " + response);
      
      string ts = (string)(int)TimeLocal();
      FolderCreate("Weblog");
      string err_filename = "Weblog\\" + WindowExpertName() + "\\" + ts + ".html";
      int _handle = FileOpen(err_filename, FILE_WRITE|FILE_TXT);
   
      if(_handle>=0) 
      {
       FileWrite(_handle, response);
       LOG_HIGH_NO_SITE("Creating error webpage: " + err_filename);
       FileClose(_handle);
      }
      else
       LOG_HIGH_ERROR_NO_SITE("Couldn't open file");
     }
     else
      LOG_HIGH_NO_SITE("No server response received");
  }
  
  return ret_code;
}


Credentials::Credentials(string host_reporting, string mt4_acct_name) 
  : p_host_reporting(host_reporting), logger(__FILE__, "helper_processes")
{
 __TRACE__
 
 string result;
 
 string http_full_host_name = p_host_reporting + URL_GET_ACCOUNT;
 string data = StringConcatenate("name=", mt4_acct_name, ";");
 
 __ int res_code=web_post(http_full_host_name, data, result);
 
 if(res_code==200) {
  string c[];
  int sz=StringSplit(result, ';', c);
  
  if(sz==3)
  {
   acct_num=c[0];
   passwd=c[1];
   server_host=c[2];
   is_valid = true;
  }
  
  if((int)acct_num<=0) {
   LOG_HIGH("Failed to parse the account number (from site)");
   is_valid=false;
  }
  
  if(StringLen(passwd)==0 || passwd=="None") {
   LOG_HIGH("Failed to parse the account password (from site)");
   is_valid=false;
  }
  
  if(StringLen(server_host)==0 || server_host=="None") {
   LOG_HIGH("Failed to parse the server host (from site)");
   is_valid=false;
  }
 }
}

class WebServer
{
private:
 Logger logger;
 
protected:
 int p_terminal_pk;
 string p_server_host;
 const Credentials *p_ptr_creds;
 void login(string url_login);
 
public:
 static datetime fromSQLTime(string time);
 static string toSQLTime(datetime time);
 static string SQLTimeCurrent();
 int WebRequest(const string method, const string URL_ROOT, const POSTDATA &data, bool b_allow_resend);
 int WebRequest(const string method, const string URL_ROOT, string data, bool b_allow_resend);
 int WebRequest(const string method, const string URL_ROOT, const POSTDATA &data, string &response, bool b_allow_resend);
 int WebRequest(const string method, const string URL_ROOT, string data, string &response, bool b_allow_resend);
 int WebRequestPOST(const string URL_ROOT, const POSTDATA &data, bool b_allow_resend);
 int WebRequestPOST(const string URL_ROOT, string data, bool b_allow_resend);
 int WebRequestPOST(const string URL_ROOT, const POSTDATA &data, string &response, bool b_allow_resend);
 int WebRequestPOST(const string URL_ROOT, string data, string &response, bool b_allow_resend);
 int terminal_pk();
 
 WebServer(string host, const Credentials *creds, bool login);
};

