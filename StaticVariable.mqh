//+------------------------------------------------------------------+
//|                                               StaticVariable.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#define FILE_ARRAY_NAME                        "Bands_DO_NOT_DELETE_1.txt"
#define FILE_ARRAY_TYPE                        "Bands_DO_NOT_DELETE_2.txt"
#define FILE_ARRAY_VALUE                       "Bands_DO_NOT_DELETE_3.txt"

#include <Arrays\ArrayString.mqh>

class StaticVariable
{
 private:
  CArrayString static_name;
  CArrayString static_type;
  CArrayString static_val;
 
  bool Read();
  
 public:
  bool Add(string name, string value, string type);
  bool Update(string name, string value);
  string Get(string name, string& type);
  void AutoIncrement(string name);
};
//-------------------------------------------------
void StaticVariable::AutoIncrement(string name)
{
  string type;
  
  string temp = Get(name,type);
  
  if(type=="int") {
    int new_val = (int)temp + 1;
    Update(name,(string)new_val);
  }
  else
    Alert("Error: could not increment ",name," in ",__FUNCTION__," for type not integer");
}
//-------------------------------------------------
string StaticVariable::Get(string name, string& type)
{
  int pos = static_name.SearchLinear(name);
  
  if(pos>=0) 
   {
    type = static_type[pos];
    return(static_val[pos]);
   }
  else
   {
    Alert("Error: could not find ",name," in ",__FUNCTION__);
    return("-1");
   }
}
//-------------------------------------------------
bool StaticVariable::Update(string name, string value)
{
  //--- Read Previous Value
   Read();
   
  //--- Look for name
   int pos = static_name.SearchLinear(name);
   
   if(pos < 0)
    {
     Alert("Error: ",name," cannot be found in ",__FUNCTION__);
     return(false);
    }
   
   return(true);
}
//-------------------------------------------------
bool StaticVariable::Read(void)
{
  //--- Open files to save static variables
  int handle1 = FileOpen(FILE_ARRAY_NAME,FILE_READ|FILE_BIN);
  
  //--- Error checking
  if(handle1 < 0) {
   Alert("Error: Could not open ",FILE_ARRAY_NAME," for read");
   Alert("e: ",GetLastError());
   ResetLastError();
   
   return(false);
  }
  
  int handle2 = FileOpen(FILE_ARRAY_TYPE,FILE_READ|FILE_BIN);
  
  if(handle2 < 0) {
   Alert("Error: Could not open ",FILE_ARRAY_TYPE," for read");
   Alert("e: ",GetLastError());
   ResetLastError();
   
   FileClose(handle1);
   return(false);
  }
  int handle3 = FileOpen(FILE_ARRAY_VALUE,FILE_READ|FILE_BIN);
  
  if(handle3 < 0) {
   Alert("Error: Could not open ",FILE_ARRAY_VALUE," for read");
   Alert("e: ",GetLastError());
   ResetLastError();
   
   FileClose(handle1);
   FileClose(handle2);
   return(false);
  }
  
  static_name.Load(handle1);
  static_type.Load(handle2);
  static_val.Load(handle3);
  
  FileClose(handle1);
  FileClose(handle2);
  FileClose(handle3);
  
  return(true);
}
//-------------------------------------------------
bool StaticVariable::Add(string name,string type,string value)
{
  //--- Check that type is correct
   if(type=="string" || type=="int" || type=="double" || type=="bool") {
    ; //DO NOTHING
   }
   else {
    Alert("Incorrect type = '",type,"' entered in ",__FUNCTION__);
    return(false);
   }
   
  //--- Open files to save static variables
  int handle1 = FileOpen(FILE_ARRAY_NAME,FILE_WRITE|FILE_BIN);
  
  //--- Error checking
  if(handle1 < 0) {
   Alert("Error: Could not open ",FILE_ARRAY_NAME," for write");
   Alert("e: ",GetLastError());
   ResetLastError();
   
   return(false);
  }
  
  int handle2 = FileOpen(FILE_ARRAY_TYPE,FILE_WRITE|FILE_BIN);
  
  if(handle2 < 0) {
   Alert("Error: Could not open ",FILE_ARRAY_TYPE," for write");
   Alert("e: ",GetLastError());
   ResetLastError();
   
   FileClose(handle1);
   return(false);
  }
  int handle3 = FileOpen(FILE_ARRAY_VALUE,FILE_WRITE|FILE_BIN);
  
  if(handle3 < 0) {
   Alert("Error: Could not open ",FILE_ARRAY_VALUE," for write");
   Alert("e: ",GetLastError());
   ResetLastError();
   
   FileClose(handle1);
   FileClose(handle2);
   return(false);
  }
  
  //--- Append to end of file
  FileSeek(handle1,0,SEEK_END);
  FileSeek(handle2,0,SEEK_END);
  FileSeek(handle3,0,SEEK_END);
  
  //--- Add variables to ArrayString variables
  static_name.Add(name);
  static_type.Add(type);
  static_val.Add(value);
  
  //--- Save Files
  static_name.Save(handle1);
  static_type.Save(handle2);
  static_val.Save(handle3);
  
  FileClose(handle1);
  FileClose(handle2);
  FileClose(handle3);
  
  return(true);
}