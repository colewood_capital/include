//+------------------------------------------------------------------+
//|                                                 OrderManager.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "CARM"
#property strict

#ifndef ARRAYINT
#define ARRAYINT
   #include <Arrays/ArrayInt.mqh>
#endif 

#ifndef ARRAYDOUBLE
#define ARRAYDOUBLE
   #include <Arrays/ArrayDouble.mqh>
#endif 

#ifndef MQLERRORS
#define MQLERRORS
   #include <MQLErrors.mqh>
#endif

#ifndef ARRAYDOUBLEDOUBLE
#define ARRAYDOUBLEDOUBLE
   #include <Arrays/ArrayDoubleDouble.mqh>
#endif



class OrderManager
{
 private:
  CArrayInt mkt_orders; 
  CArrayInt pending_orders; 
  CArrayDouble mkt_open_lots;
  CArrayDouble mkt_closed_lots;
  CArrayDouble mkt_realized;
  CArrayDoubleDouble mkt_close_prices;
  
  int p_magic;
  int p_retrycount;
  int p_retrydelay;
  int p_slippage;
  
  void AddMarket(int ticket, double lots);
  void AddPending(int ticket);
  void DeleteMarket(int ticket);
  int PlaceTrade(int cmd, double lots, double stop=0.0, double take=0.0, double price=0.0, string comment=NULL);
  double CheckLots(double lots);
  void PartialCloseInfo(int ticket, double& lots, double& realized_pl, CArrayDouble& close_price, int idx=0);
  bool Update(int ticket, double closed_lots);

 public:
  int OpenTrade(int cmd, double lots, double sl=0.0, double tp=0.0, double price=0.0, string comment=NULL);
  bool CloseTrade(int ticket, double lots);
  bool CloseTradePercent(int ticket, double percentage);
  double CloseAll(double percentage);
  bool DeletePending(int ticket);
  bool Info(int ticket,double& open_lots,double& closed_lots, double& realized_pl, CArrayDouble*& close_prices);
  bool isOpen(int ticket);
  void GetOpenTrades(CArrayInt& booknum);
  void GetPendingTrades(CArrayInt& booknum);
  int AlertTriggeredPending();
  bool isSLTriggered(int ticket);
  double LastOpenPrice();
  
  OrderManager(int MAGIC=0, int SLIPPAGE=0, int RETRYCOUNT=10, int RETRYDELAY=500);
};
//+------------------------------------------------------------------+
OrderManager::OrderManager(int MAGIC=0,int SLIPPAGE=0,int RETRYCOUNT=10,int RETRYDELAY=500)
{
  p_magic = MAGIC;
  p_retrycount = RETRYCOUNT;
  p_retrydelay = RETRYDELAY;
  p_slippage = SLIPPAGE;
  
  string symbol = Symbol();
  int total = OrdersTotal();
 
  for(int i=0; i<total; i++)
   {
    if(OrderSelect(i,SELECT_BY_POS))
     {
      if(OrderSymbol()==symbol && OrderMagicNumber()==p_magic)
       {
        int type=OrderType();
        
        if(type==OP_BUY || type==OP_SELL)
         AddMarket(OrderTicket(),OrderLots());      
        
        if(type==OP_BUYLIMIT || type==OP_SELLLIMIT || type==OP_BUYSTOP || type==OP_SELLSTOP)
         AddPending(OrderTicket());
       }
     }
   }
}
//+------------------------------------------------------------------+
void OrderManager::GetPendingTrades(CArrayInt& booknum)
{
  booknum.AssignArray(GetPointer(pending_orders));
}
//+------------------------------------------------------------------+
void OrderManager::GetOpenTrades(CArrayInt& booknum)
{
  booknum.AssignArray(GetPointer(mkt_orders));
}
//+------------------------------------------------------------------+
bool OrderManager::isSLTriggered(int ticket)
{
  if(OrderSelect(ticket,SELECT_BY_TICKET))
   {
    string ord_cmt = OrderComment();
   
    if(OrderCloseTime() > 0 && StringFind(ord_cmt,"[sl]") >= 0)
     {
       DeleteMarket(ticket);
       return(true);
     }
   }
   
  return(false);
}
//+------------------------------------------------------------------+
double OrderManager::LastOpenPrice()
{
   int last_order = mkt_orders.Total()-1;
   
   if(OrderSelect(mkt_orders[last_order], SELECT_BY_TICKET))
    return(OrderOpenPrice());
    
   return(-1);
}
//+------------------------------------------------------------------+
bool OrderManager::isOpen(int ticket)
//Used to check if ticket is in OrderManager queue
{
   int pos = mkt_orders.SearchLinear(ticket);
   
   if(pos>=0)
     return(true);
   else
     return(false);
}
//+------------------------------------------------------------------+
bool OrderManager::Info(int ticket,double& open_lots,double& closed_lots, double& realized_pl, CArrayDouble*& close_prices)
{
   int pos = mkt_orders.SearchLinear(ticket);
  
   if(pos>=0)
    {
     open_lots = mkt_open_lots[pos];
     closed_lots = mkt_closed_lots[pos];
     realized_pl = mkt_realized[pos];
     close_prices = mkt_close_prices.Get(ticket);
 
     return(true);
    }
   else
    {
     open_lots = 0;
     closed_lots = 0;
     realized_pl = 0;
     close_prices = NULL;
     return(false);
    }
}
//+------------------------------------------------------------------+
bool OrderManager::DeletePending(int ticket)
{
  if(OrderDelete(ticket))
   { 
     int pos = pending_orders.SearchLinear(ticket);
     
     if(pos>=0)
      {
        pending_orders.Delete(pos);
      }
     else
      {
        Alert("Error: could not find ticket #",ticket," in ",__FUNCTION__);
      }
      
     return(true);
   }
  else
   {
     Alert("Error: could not delete #",ticket," for, e: ",GetLastError());
     ResetLastError();
     return(false);
   }
}
//+------------------------------------------------------------------+
void OrderManager::AddPending(int ticket)
{
  pending_orders.Add(ticket);
}
//+------------------------------------------------------------------+
void OrderManager::DeleteMarket(int ticket)
{
  int pos = mkt_orders.SearchLinear(ticket);
  
  if(pos>=0)
   {
     mkt_orders.Delete(pos);
     mkt_open_lots.Delete(pos);
     mkt_closed_lots.Delete(pos);
     mkt_realized.Delete(pos);
     mkt_close_prices.Delete(ticket);
   }
  else
   {
     Alert("Error: could not find ticket #",ticket," in ",__FUNCTION__);
   }
}
//+------------------------------------------------------------------+
void OrderManager::AddMarket(int ticket, double lots)
{
  double realized=0.0, cls_lots=0.0;
  CArrayDouble* cls_prices = mkt_close_prices.New(ticket);
  cls_prices = mkt_close_prices.Get(ticket);

  PartialCloseInfo(ticket,cls_lots,realized,cls_prices);
  Alert("HERE AddMarket - #", ticket);
  mkt_orders.Add(ticket);
  mkt_open_lots.Add(lots);
  mkt_closed_lots.Add(cls_lots);
  mkt_realized.Add(realized);
}
//+------------------------------------------------------------------+
bool OrderManager::Update(int ticket, double closed_lots)
{
  int id = mkt_orders.SearchLinear(ticket);
  int new_ticket=-1, type=-1;
  
  if(id < 0) {
    Alert("Error: could not find order #",ticket," in ",__FUNCTION__);
    return(false);
  }
  
  //--- In case of partial close, replace old ticket with new ticket
  if(OrderSelect(ticket,SELECT_BY_TICKET))
   {
    type = OrderType();
    
    if(OrderCloseTime() > 0)
     {
      string order_cmt = OrderComment();
      new_ticket = -1;
      
      int ans = StringFind(order_cmt,"to #");
      
      if(ans==0)
       {
         string res[];
         StringSplit(order_cmt,'#',res); 
         new_ticket = (int)res[1];
       }
       
      if(new_ticket > 0)
       {
         mkt_orders.Update(id, new_ticket);
                  
         mkt_open_lots.Update(id, NormalizeDouble(mkt_open_lots[id] - closed_lots, 2));
         
         mkt_closed_lots.Update(id, NormalizeDouble(mkt_closed_lots[id] + closed_lots, 2));
         
         mkt_close_prices.UpdateIndex(ticket, new_ticket); 
         mkt_close_prices.Get(new_ticket).Add(OrderClosePrice());
         
         mkt_realized.Update(id, NormalizeDouble(mkt_realized[id] + OrderProfit() + OrderSwap() + OrderCommission(), 2)); 
       }
      else
       DeleteMarket(ticket);
       
      return(true);
    }
   else  
    {
      Alert("Error: Order #",ticket," should be updated after it is closed in ",__FUNCTION__);
      return(false);
    }
   }
   
  Alert("Error: could not select order #",ticket," in ",__FUNCTION__);
  return(false);
 }
//+------------------------------------------------------------------+
double OrderManager::CloseAll(double percentage)
{
  int total = mkt_orders.Total();
  double closed_lots = 0.0;
  bool error = false;
  
  for(int i=total-1; i>=0; i--)
   {
    bool ans=false;
  
    if(OrderSelect(mkt_orders[i],SELECT_BY_TICKET))
     {
      int ticket = OrderTicket();
      int type = OrderType();
      double prc = -1;
   
      if(type==OP_BUY)
       prc = Bid;
      else if(type==OP_SELL)
       prc = Ask;
      else
       continue;
 
      for (int j=0; j<p_retrycount; j++) {
         for (int k=0; (k<50) && IsTradeContextBusy(); k++)
             Sleep(100);
         RefreshRates();
     
      double lots = NormalizeDouble(OrderLots()*percentage, 2);
      
      ans = OrderClose(ticket, lots, prc, p_slippage);
    
      if(ans)
       {
         Update(ticket, lots); 
         closed_lots += lots;
       }
      else
       { 
         Print("CloseTrade: error \'"+(string)ErrorDescription(GetLastError())+" when closing #",ticket,", lots=",DoubleToStr(lots,3)+" @"+DoubleToStr(prc,Digits));
         ResetLastError();
         Sleep(p_retrydelay);
       }
      }
    
     }//end if
     
     if(!ans)       error=true;
   }//end for
  
  return(closed_lots);
}
//+------------------------------------------------------------------+
bool OrderManager::CloseTradePercent(int ticket,double percentage=1.0)
{
  int type;
  double lots, prc;
  
  if(OrderSelect(ticket,SELECT_BY_TICKET))
   {
    type = OrderType();
    lots = NormalizeDouble(OrderLots() * percentage, 2);
   }
  else {
   Alert("Error: unable to select ticket #",ticket," in ",__FUNCTION__);
   return(false);
  }
  
  if(type==OP_BUY)
   prc = Bid;
  else if(type==OP_SELL)
   prc = Ask;
  else
   Print("Error: unable to close ticket #",ticket,", cmd=",type," in ",__FUNCTION__);
  
  for (int i=0; i<p_retrycount; i++) {
     for (int j=0; (j<50) && IsTradeContextBusy(); j++)
         Sleep(100);
     RefreshRates();
     
  if(OrderClose(ticket, lots, prc, p_slippage))
   {
     Update(ticket, lots);
     return(true);
   }
  else
   {
     Print("CloseTrade: error \'"+(string)ErrorDescription(GetLastError())+" when closing #",ticket,", lots=",DoubleToStr(lots,3)+" @"+DoubleToStr(prc,Digits));
     ResetLastError();
     Sleep(p_retrydelay);
   }
  }
   
  Print("CloseTrade: can\'t close #",ticket," after "+(string)p_retrycount+" retries");
    
  return(false);
}
//+------------------------------------------------------------------+
bool OrderManager::CloseTrade(int ticket,double lots)
{
  int type;
  double prc;
  
  if(OrderSelect(ticket,SELECT_BY_TICKET))
   type = OrderType();
  else {
   Alert("Error: unable to select ticket #",ticket," in ",__FUNCTION__);
   return(false);
  }
  
  if(type==OP_BUY)
   prc = Bid;
  else if(type==OP_SELL)
   prc = Ask;
  else
   Print("Error: unable to close ticket #",ticket,", cmd=",type," in ",__FUNCTION__);
  
  lots = NormalizeDouble(lots, 2);
  
  for (int i=0; i<p_retrycount; i++) {
     for (int j=0; (j<50) && IsTradeContextBusy(); j++)
         Sleep(100);
     RefreshRates();
     
  if(OrderClose(ticket, lots, prc, p_slippage))
   {
     Update(ticket, lots);
     return(true);
   }
  else
   {
     Print("CloseTrade: error \'"+(string)ErrorDescription(GetLastError())+" when closing #",ticket,", lots=",DoubleToStr(lots,3)+" @"+DoubleToStr(prc,Digits));
     ResetLastError();
     Sleep(p_retrydelay);
   }
  }
   
  Print("CloseTrade: can\'t close #",ticket," after "+(string)p_retrycount+" retries");
    
  return(false);
}
//+------------------------------------------------------------------+
int OrderManager::OpenTrade(int cmd,double lots,double sl=0.0,double tp=0.0,double price=0.0,string comment=NULL)
{
  int ticket=0;
  
  if(cmd >= 0 && cmd <= 1)
   {
     ticket = PlaceTrade(cmd, lots, sl, tp, price, comment);
     
     if(ticket > 0)        
      AddMarket(ticket, lots); 
   }
  else if(cmd > 1 && cmd <= 5)
   {
     ticket = PlaceTrade(cmd, lots, sl, tp, price, comment);
     
     if(ticket > 0)        
      AddPending(ticket);
   }
  else
   {
     Alert("Error: invalid cmd=",cmd," in ",__FUNCTION__);
   }
   
  return(ticket);
}
//-------------------------------------------------
int OrderManager::AlertTriggeredPending()
{
  int booknum = -1;
  int size = pending_orders.Total();
  
  for(int i=0; i<size; i++)
   {
    if(OrderSelect(pending_orders[i],SELECT_BY_TICKET))
     {
      if(OrderCloseTime()==0)
       {
        if(OrderType() < 2)
         {
           pending_orders.Delete(i);
           booknum = OrderTicket();
           AddMarket(booknum, OrderLots());
           break;
         }
       }
      else
       {
         pending_orders.Delete(i);
       }
     }
    else
     {
       Alert("Error: Booknum #", pending_orders[i]," could not be found by OrderSelect() in ",__FUNCTION__);
     }
   }
   
  return(booknum);
}
//+------------------------------------------------------------------+
int OrderManager::PlaceTrade(int cmd, double lots, double stop=0.0, double take=0.0, double price=0.0, string comment=NULL)
{
  int i, j, ticket=-1;
  double prc, sl, tp;
  string order_cmt;
    
  if(lots <= 0) {
    Print("Error lots=",lots," in ",__FUNCTION__,": cmd= ",cmd," prc="+DoubleToStr(price,Digits)+" sl="+DoubleToStr(stop,Digits)+" tp="+DoubleToStr(take,Digits));
    return(0);
  }
    
  lots = CheckLots(lots);
   
  for (i=0; i<p_retrycount; i++) {
      for (j=0; (j<50) && IsTradeContextBusy(); j++)
          Sleep(100);
      RefreshRates();
   
      if (cmd == OP_BUY) {
          prc = Ask;
          sl = stop;
          tp = take;
      }
      else if (cmd == OP_SELL) {
          prc = Bid;
          sl = stop;
          tp = take;
      }
      else if (cmd == OP_BUYLIMIT) {
          prc = NormalizeDouble(price, Digits);
          sl = stop;
          tp = take;
      }
      else if (cmd == OP_SELLLIMIT) {
          prc = NormalizeDouble(price, Digits);
          sl = stop;
          tp = take;
      }
      else if( cmd == OP_BUYSTOP) {
          prc = NormalizeDouble(price, Digits);
          sl = stop;
          tp = take;
      }
      else if( cmd == OP_SELLSTOP) {
          prc = NormalizeDouble(price, Digits);
          sl = stop;
          tp = take;
      }
      else {
       Alert("Invalid direction in ",__FUNCTION__);
       return(-1);
      }
        
      Print("Attempt OpenTrade: cmd= ",cmd," prc="+DoubleToStr(prc,Digits)+" sl="+DoubleToStr(sl,Digits)+" tp="+DoubleToStr(tp,Digits));
   
      ticket = OrderSend(Symbol(), cmd, lots, prc, p_slippage, 0, 0, comment, p_magic);
        
      if (ticket != -1 && ( sl > 0 || tp > 0) ) {
          Print("OpenTrade: opened ticket #" + (string)ticket);  
            
          if(OrderSelect( ticket, SELECT_BY_TICKET, MODE_TRADES))
           {   
             for (i=0; i<p_retrycount; i++) {
                 for (j=0; (j<50) && IsTradeContextBusy(); j++)
                     Sleep(100);
                 RefreshRates();
   
                if ( OrderModify(ticket, prc, sl, tp, 0) ) {
                    Print("OpenTrade #",ticket,": SL/TP set");
                    break;
                }
             }
             
             Print("OpenTrade #",ticket,": error \'"+(string)ErrorDescription(GetLastError())+"\' when setting SL/TP");
             ResetLastError();
             Sleep(p_retrydelay);
          }
   
          return (ticket);
      }
      else{      
       if( ticket >= 0 ) {
          return( ticket );
       }
      }
      Print("OpenTrade: error \'"+(string)ErrorDescription(GetLastError())+"\' when entering with "+DoubleToStr(lots,3)+" @"+DoubleToStr(prc,Digits));
      ResetLastError();
      Sleep(p_retrydelay);
  }
   
  Print("OpenTrade: can\'t enter after "+(string)p_retrycount+" retries");
    
  return(ticket);
}
//+------------------------------------------------------------------+
void OrderManager::PartialCloseInfo(int ticket, double& lots, double& realized_pl, CArrayDouble& close_price, int idx=0)
 { 
  if(OrderSelect(ticket,SELECT_BY_TICKET))
   {
    string result[];
    int substr1_count;
    
    substr1_count = StringSplit(OrderComment(),'#',result);
  
    if(substr1_count > 1 && result[0]=="from ")
     {
      if(OrderSelect((int)result[1],SELECT_BY_TICKET))
       {
        int booknum = OrderTicket();
        lots += OrderLots();
        realized_pl += OrderProfit();
        close_price.Add(OrderClosePrice());
        
        PartialCloseInfo(booknum,lots,realized_pl,close_price);
       }
     }
 
    if(substr1_count > 1 && result[0]=="to ")
     {
      int begin = OrdersHistoryTotal()-idx-1;
   
      for(int i=begin; i>=0; i--)
       {
        if(OrderSelect(i,SELECT_BY_POS,MODE_HISTORY))
         {
          int substr2_count = StringSplit(OrderComment(),'#',result);
          
          if(substr2_count > 1 && result[0]=="to " && result[1]==(string)ticket) 
           {
             int booknum = OrderTicket();
             lots += OrderLots();
             realized_pl += OrderProfit();
             close_price.Add(OrderClosePrice());
           
             PartialCloseInfo(booknum,lots,realized_pl,close_price,begin-i);
           }
         }
       }//end for
     }//end if
   }//end if
  else
   {
    lots=0.0;
    realized_pl=0.0;
   }
   
   return;
 }
//+------------------------------------------------------------------+
double OrderManager::CheckLots(double lots)
 {
  double lot, lotmin, lotmax, lotstep, margin;
 
  lotmin = MarketInfo(Symbol(), MODE_MINLOT);
  lotmax = MarketInfo(Symbol(), MODE_MAXLOT);
  lotstep = MarketInfo(Symbol(), MODE_LOTSTEP);
  margin = MarketInfo(Symbol(), MODE_MARGINREQUIRED);

  if (lots*margin > AccountFreeMargin())
      lots = AccountFreeMargin() / margin;

  lot = MathFloor(lots/lotstep + 0.5) * lotstep;

  if (lot < lotmin)
      lot = lotmin;
  if (lot > lotmax)
      lot = lotmax;

  return (NormalizeDouble(lot, 2));
}