//+------------------------------------------------------------------+
//|                                                   Intrascalp.mqh |
//|                                       Copyright 2016, Jamal Cole |
//+------------------------------------------------------------------+
#property copyright "jcolecorp"
#property strict

#include <Managers\SoftTakeProfit.mqh>
#include <Managers\TradeParameter.mqh>
#include <Packages\Events.mqh>
#include <Packages\Signals.mqh>
#include <Managers\LongShortParameter.mqh>
#include <Super\Enums.mqh>
#include <Managers\ReEntry.mqh>

#ifndef LOGGER 
   #define LOGGER_DEPRECATED
   #include <Logger.mqh>
#endif 

#ifndef UTILS2
#define UTILS2
   #include <Utils2.mqh>
#endif

#define MAGIC        451215

input int MAX_ORDERS = 5;
input double LOT_SIZE = 0.01;
input double MAX_DRAWDOWN_BET_ORDERS_PIPS = 20;
input double PSAR_SIGNAL_MIN_PIPS = 30;
input double EXIT_DRAWDOWN_VOLATILITY_MULTIPLIER = 1;
input double PSAR_CLOSE_MIN_PIPS_PROFIT = 50;

Events event;
Candlestick candle_strong_higher;
Candlestick candle_strong_lower;
Candlestick candle_weak_higher;
Candlestick candle_weak_lower;
TradeParameter *trade_parameter;
Logger logger(__FILE__);
DrawdownRetraceExit *exit;
Psar *psar, *psar_htf;

Manager manager;
ReEntry re;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
ENUM_INIT_RETCODE Init()
  {
//---
 EventSetTimer(60);
 
 candle_strong_higher.subscribe(NULL, CANDLESTICK_STRONG_TREND_HIGHER);
 candle_weak_higher.subscribe(NULL, CANDLESTICK_WEAK_TREND_HIGHER);
 candle_strong_lower.subscribe(NULL, CANDLESTICK_STRONG_TREND_LOWER);
 candle_weak_lower.subscribe(NULL, CANDLESTICK_WEAK_TREND_LOWER);
 
 CArrayInt buy_tickets, sell_tickets;
 GetTickets(buy_tickets, Symbol(), OP_BUY, MAGIC_NUMBER_CANDLE_RECOGNITION);
 GetTickets(sell_tickets, Symbol(), OP_SELL, MAGIC_NUMBER_CANDLE_RECOGNITION);
 
 CArrayInt tickets;
 tickets.AddArray(GetPointer(buy_tickets));
 tickets.AddArray(GetPointer(sell_tickets));
 
 trade_parameter = new TradeParameter(tickets, Symbol(), MAX_ORDERS, 1, MAX_DRAWDOWN_BET_ORDERS_PIPS * 0.5, MAX_DRAWDOWN_BET_ORDERS_PIPS);
 exit = new DrawdownRetraceExit();
 
 psar = new Psar();
 psar.subscribe(NULL, PSAR_SIGNAL_MIN_PIPS, 0.008, 0.2);
 
 psar_htf = new Psar();
 psar.subscribe(PERIOD_H1, PSAR_SIGNAL_MIN_PIPS, 0.008, 0.2);

 #ifndef TEST
 exit.add_rung_to_ladder(50 * EXIT_DRAWDOWN_VOLATILITY_MULTIPLIER, 1.5);
 exit.add_rung_to_ladder(75 * EXIT_DRAWDOWN_VOLATILITY_MULTIPLIER, 1.2);
 exit.add_rung_to_ladder(100 * EXIT_DRAWDOWN_VOLATILITY_MULTIPLIER, 0.85);
 exit.add_rung_to_ladder(150 * EXIT_DRAWDOWN_VOLATILITY_MULTIPLIER, 0.6);
 #else
 exit.add_rung_to_ladder(10, 2.0);
 exit.add_rung_to_ladder(25, 1.2);
 exit.add_rung_to_ladder(40, 0.6);
 #endif 
 
 exit.backfill(tickets);
 
 manager.add(GetPointer(re));

  return(INIT_SUCCEEDED);
  }

//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void Deinit(const int reason)
  {
//---
   delete trade_parameter;
   delete exit;
   delete psar;
   delete psar_htf;
  }

//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void Tick()
  {
//---
    ENUM_CHARTEVENT signal;
    ENUM_TIMEFRAMES tf;
    
    manager.tick_engine();
    exit.tick_engine(Bid,Ask);
    psar.tick_engine(signal, tf);
    psar_htf.tick_engine(signal, tf);

    if(event.NewBar())
    {
     logger.log("New bar found @ " + TimeToStr(TimeCurrent(), TIME_DATE|TIME_MINUTES|TIME_SECONDS), LOG_TYPE_DEBUG, __LINE__, __FUNCTION__, true);
     
     //--- check lower candle
     {
      //--- EXTENSIVE DEBUG
       logger.log("check for long trade", LOG_TYPE_DEBUG, __LINE__, __FUNCTION__, true);
        
      string msg = "";
      bool strong_bar = candle_strong_lower.tick_engine();
      bool weak_bar = candle_weak_lower.tick_engine();
       
      if(strong_bar) msg += "STRONG_BAR; ";
      if(weak_bar) msg += "WEAK_BAR; ";
      
      if(strong_bar || weak_bar)
      {
       if(trade_parameter.is_long_trading_allowed())
       {
        KwargsDict kwargs;
        kwargs.set("reentry", "2");
        string reason = msg;
        TradeEngine::open(Symbol(), OP_BUY, LOT_SIZE, kwargs, __LINE__, __FUNCTION__, reason, Ask, NULL, MAGIC_NUMBER_CANDLE_RECOGNITION);
       }
       else
        logger.log("long_trade_parameter.is_trading_allowed() returned false", LOG_TYPE_DEBUG, __LINE__, __FUNCTION__, true);
      }
      else
       logger.log("no [LOWER] candlebar signal found", LOG_TYPE_DEBUG, __LINE__, __FUNCTION__, true);
     }
      
     //--- check for upper candle
      {
      //--- EXTENSIVE DEBUG
       logger.log("check for short trade", LOG_TYPE_DEBUG, __LINE__, __FUNCTION__, true);
        
      string msg = "";
      bool strong_bar = candle_strong_higher.tick_engine();
      bool weak_bar = candle_weak_higher.tick_engine();
       
      if(strong_bar) msg += "STRONG_BAR; ";
      if(weak_bar) msg += "WEAK_BAR; ";
       
      if(strong_bar || weak_bar)
      {
       if(trade_parameter.is_short_trading_allowed())
       {
        KwargsDict kwargs;
        kwargs.set("reentry", "2");
        string reason = msg;
        TradeEngine::open(Symbol(), OP_SELL, LOT_SIZE, kwargs, __LINE__, __FUNCTION__, reason, Bid, NULL, MAGIC_NUMBER_CANDLE_RECOGNITION);
       }
       else
        logger.log("short_trade_parameter.is_trading_allowed() returned false", LOG_TYPE_DEBUG, __LINE__, __FUNCTION__, true);
      }
      else
       logger.log("no [HIGHER] candlebar signal found", LOG_TYPE_DEBUG, __LINE__, __FUNCTION__, true);
     }
    }
    
    Comment("Last Update: \n" + TimeToString(TimeLocal(), TIME_DATE|TIME_MINUTES|TIME_SECONDS));
    
  }
//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
void Timer()
  {
//---

  }

//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void ChartEvent(const int id,
                const long &lparam,
                const double &dparam,
                const string &sparam)
  {
   int ticket=-1;
   bool res=false;
   
   if(id == CHARTEVENT_CUSTOM + CHARTEVENT_CONFIRM_OPEN)
   {
    KwargsDict kwargs;
    ticket = TradeEngine::trade_confirmation_open(lparam, dparam, sparam, kwargs);
    
    if(ticket > 0)
     manager.chart_event_open_engine(ticket, kwargs);
     
    //--- SHOULD BE CONVERTED TO MANAGER CLASS
     if(OrderSelect(ticket,SELECT_BY_TICKET))
     {
      if(OrderSymbol()==Symbol())
      {
       trade_parameter.add(ticket);
       exit.add(ticket);
       logger.log("Adding #" + (string)ticket + " to trading_parameter and exit managers", LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
      }
     }
     else
      logger.log("Could not select #" + (string)ticket, LOG_TYPE_INFO, __LINE__, __FUNCTION__);
   }
   
   if(id == CHARTEVENT_CUSTOM + CHARTEVENT_CONFIRM_CLOSE)
   {
    res = TradeEngine::trade_confirmation_close(lparam, dparam, sparam);
    int booknum = (int)lparam;
  
    if(res)
     manager.chart_event_close_engine(booknum);
   }
   
   //--- SIGNALS can be implemented like managers with an abstract method that determines what to do when signal hit
   if(id == CHARTEVENT_CUSTOM + SIGNALS_PSAR_TOUCH_BELOW)
   {
    ENUM_TIMEFRAMES tf = (ENUM_TIMEFRAMES)lparam;
    
    int total = OrdersTotal();
    for(int i=total-1; i>=0; i--)
    {
     if(OrderSelect(i,SELECT_BY_POS) && OrderSymbol()==Symbol() && OrderType()==OP_BUY)
     {
      double pips_gained = Bid - OrderOpenPrice();
      double min_pips = PSAR_CLOSE_MIN_PIPS_PROFIT * Point * 10;
      if(pips_gained >= min_pips)
      {
       string log_msg = "Closing #" + (string)ticket + " on signal=" + EnumToString(SIGNALS_PSAR_TOUCH_BELOW) + ", tf=" + EnumToString(tf) + ", pips_gained=" + DoubleToStr(pips_gained, Digits);
       TradeEngine::close(OrderTicket(), __LINE__, __FUNCTION__, log_msg);
      }
      else
       logger.log("Did not close on PSAR for pips_gained(" + DoubleToStr(pips_gained,Digits) + ") < min_pips(" + DoubleToStr(min_pips,Digits) + ")", LOG_TYPE_INFO, __LINE__, __FUNCTION__);
     }
    }
   }
   
   if(id == CHARTEVENT_CUSTOM + SIGNALS_PSAR_TOUCH_ABOVE)
   {
    ENUM_TIMEFRAMES tf = (ENUM_TIMEFRAMES)lparam;
    
    int total = OrdersTotal();
    for(int i=total-1; i>=0; i--)
    {
     if(OrderSelect(i,SELECT_BY_POS) && OrderSymbol()==Symbol() && OrderType()==OP_SELL)
     {
      double pips_gained = OrderOpenPrice() - Ask;
      double min_pips = PSAR_CLOSE_MIN_PIPS_PROFIT * Point * 10;
      if(pips_gained >= min_pips)
      {
       string log_msg = "Closing #" + (string)ticket + " on signal=" + EnumToString(SIGNALS_PSAR_TOUCH_ABOVE) + ", tf=" + EnumToString(tf) + ", pips_gained=" + DoubleToStr(pips_gained, Digits); 
       TradeEngine::close(OrderTicket(), __LINE__, __FUNCTION__, log_msg);
      }
      else
       logger.log("Did not close on PSAR for pips_gained(" + DoubleToStr(pips_gained,Digits) + ") < min_pips(" + DoubleToStr(min_pips,Digits) + ")", LOG_TYPE_INFO, __LINE__, __FUNCTION__);
     }
    }
   }
   
  }