//+------------------------------------------------------------------+
//|                                                 OrderTracker.mqh |
//|                                       Copyright 2016, Jamal Cole |
//+------------------------------------------------------------------+
#property copyright "jcolecorp"
#property strict

#ifndef ARRAYINT
#define ARRAYINT
   #include <Arrays\ArrayInt.mqh>
#endif 

#ifndef ARRAYDOUBLE
#define ARRAYDOUBLE
   #include <Arrays\ArrayDouble.mqh>
#endif 

#ifndef ARRAYLIST
#define ARRAYLIST
   #include <Arrays\ArrayList.mqh>
#endif 

#ifndef LOGGER
#define LOGGER
   #define LOGGER_DEPRECATED
   #include <Logger.mqh>
#endif

#ifndef STDLIB
#define STDLIB
   #include <stdlib.mqh>
#endif 

#ifndef MATH
#define MATH
   #include <Packages\Math.mqh>
#endif 

#ifndef UTILS2
#define UTILS2
   #include <Utils2.mqh>
#endif 

extern double PIPS_REQ_INITIAL_HIGH = 10;
extern int STOP_TRACKING_AFTER_CLOSE_IN_HOURS = 168;
extern int PRINT_ORDERS_IN_HOURS = 1;

double retrace_levels[] = { 0.382, 0.5, 0.618, 1.0 };

class OrderTracker
{
protected:
 CArrayInt p_booknum;
 CArrayInt p_cmd;
 CArrayInt p_state;
 CArrayDouble p_open_price;
 CArrayDouble p_best_price;
 CArrayDouble p_max_profit;
 CArrayDatetime p_max_profit_tstamp;
 CArrayDouble p_max_loss;
 CArrayDatetime p_max_loss_tstamp;
 CArrayDatetime p_close_time;
 int p_retrace_levels_size;
 datetime p_last_print_orders;
 
 CArrayList<CArrayString> p_history;
 CArrayList<CArrayDatetime> p_tstamp;
 CArrayList<CArrayDouble> p_price;
 
 void remove(int pos);
 void add_entry(int pos, string msg, datetime tstamp, double prc);
 int add_new_order(int ticket, int type, double open_price, double profit, datetime time_current);
 int add_new_order(int ticket, int type, double open_price, double best_price, double max_profit, datetime max_profit_tstamp, double max_loss, datetime max_loss_tstamp);
 
 Logger logger;
 
public:
 OrderTracker(const CArrayInt &tickets);
 OrderTracker() : logger(__FILE__, "helper_processes") { p_retrace_levels_size = ArraySize(retrace_levels); p_last_print_orders = TimeCurrent(); }
 void check_for_new_order();
 void remove_old_orders();
 void get_history(int ticket, CArrayString* &OUT_history, CArrayDatetime* &OUT_tstamp,CArrayDouble* &OUT_price);
 void tick_engine();
 void timer_engine();
 string print_history(int ticket);
};

OrderTracker::OrderTracker(const CArrayInt &tickets)
 : logger(__FILE__, "helper_processes")
{
 p_retrace_levels_size = ArraySize(retrace_levels); 
 p_last_print_orders = TimeCurrent();
 
 int total = tickets.Total();
 
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(tickets[i], SELECT_BY_TICKET))
  {
   string symbol = OrderSymbol();
   double lots = OrderLots();
   int type = OrderType();
   double open_price =  OrderOpenPrice();
   datetime open_time = OrderOpenTime();
   datetime min_price_tstamp = NULL;
   datetime max_price_tstamp = NULL;
   double min_price = MathLowestPrice(open_time, min_price_tstamp);
   double max_price = MathHighestPrice(open_time, max_price_tstamp);
    
   if(type==OP_BUY)
   {
    double max_pips_attained = max_price - open_price;
    int k = -1;
    
    if(max_pips_attained >= (PIPS_REQ_INITIAL_HIGH * Point * 10))
    {
     double max_profit = MathCalculateProfitLoss(type, open_price, max_price, lots, symbol);
     double max_loss = MathCalculateProfitLoss(type, open_price, min_price, lots, symbol);
      
     k = add_new_order(tickets[i], type, open_price, max_price, max_profit, max_price_tstamp, max_loss, min_price_tstamp);
     logger.log("Backfilling #" + (string)tickets[i] + ": max_pips_attained=" + DoubleToStr(max_pips_attained, Digits) + " >= " + (string)(PIPS_REQ_INITIAL_HIGH * Point * 10), LOG_INFO, __LINE__, __FUNCTION__);
     logger.log("#" + (string)tickets[i] + " backfill data: best_price=" + DoubleToStr(max_price, Digits) + ", max_profit=" + DoubleToStr(max_profit, Digits) + ", max_profit_tstamp=" + TimeToStr(max_price_tstamp, TIME_DATE|TIME_MINUTES|TIME_SECONDS) + ", max_loss=" + DoubleToStr(max_loss, Digits) + ", max_loss_tstmp=" + TimeToStr(min_price_tstamp, TIME_DATE|TIME_MINUTES|TIME_SECONDS), LOG_DEBUG, __LINE__, __FUNCTION__);
    }
     
    if(k < 0) {
     logger.log("Don't backfill #" + (string)tickets[i] + ": max_pips_attained=" + DoubleToStr(max_pips_attained, Digits) + " < " + (string)(PIPS_REQ_INITIAL_HIGH * Point * 10), LOG_INFO, __LINE__, __FUNCTION__);
     continue;
    }
    
    for(int j=p_retrace_levels_size-1; j>=0; j--)
    {
      if(min_price <= p_open_price[k] + ((p_best_price[k] - p_open_price[k]) * (1 - retrace_levels[j])))
      {
       add_entry(k, "RETRACE_" + DoubleToStr(retrace_levels[j] * 100, 1), min_price_tstamp, min_price);
       p_state.Update(k, j+1);
       logger.log((string)p_booknum[k] + ": " + DoubleToStr(retrace_levels[j], 3) + " retrace", LOG_INFO, __LINE__, __FUNCTION__);
       logger.log((string)p_booknum[k] + ": state = " + (string)p_state[k], LOG_DEBUG, __LINE__, __FUNCTION__);
       logger.log((string)p_booknum[k] + ": Bid=" + DoubleToStr(min_price, Digits) + ", open_prc=" + DoubleToStr(p_open_price[k], Digits) + ", best_prc=" + DoubleToStr(p_best_price[k], Digits) + ", retrace_lvl=" + (string)retrace_levels[j], LOG_DEBUG, __LINE__, __FUNCTION__);
      }
      break; //don't check states once retrace level found
    }
   }
   
   if(type==OP_SELL)
   {
    double max_pips_attained = open_price - min_price;
    int k = -1;
    
    if(max_pips_attained >= (PIPS_REQ_INITIAL_HIGH * Point * 10))
    {
     double max_profit = MathCalculateProfitLoss(type, open_price, min_price, lots, symbol);
     double max_loss = MathCalculateProfitLoss(type, open_price, max_price, lots, symbol);
      
     k = add_new_order(tickets[i], type, open_price, min_price, max_profit, min_price_tstamp, max_loss, max_price_tstamp);
     logger.log("Backfilling #" + (string)tickets[i] + ": max_pips_attained=" + DoubleToStr(max_pips_attained, Digits) + " >= " + (string)(PIPS_REQ_INITIAL_HIGH * Point * 10), LOG_INFO, __LINE__, __FUNCTION__);
     logger.log("#" + (string)tickets[i] + " backfill data: best_price=" + DoubleToStr(min_price, Digits) + ", max_profit=" + DoubleToStr(max_profit, Digits) + ", max_profit_tstamp=" + TimeToStr(max_price_tstamp, TIME_DATE|TIME_MINUTES|TIME_SECONDS) + ", max_loss=" + DoubleToStr(max_loss, Digits) + ", max_loss_tstmp=" + TimeToStr(min_price_tstamp, TIME_DATE|TIME_MINUTES|TIME_SECONDS), LOG_DEBUG, __LINE__, __FUNCTION__);
    }
    
    if(k < 0) {
     logger.log("Don't backfill #" + (string)tickets[i] + ": max_pips_attained=" + DoubleToStr(max_pips_attained, Digits) + " < " + (string)(PIPS_REQ_INITIAL_HIGH * Point * 10), LOG_INFO, __LINE__, __FUNCTION__);
     continue;
    }
     
    for(int j=p_retrace_levels_size-1; j>=0; j--)
    {
      if(max_price >= p_open_price[k] - ((p_open_price[k] - p_best_price[k]) * (1 - retrace_levels[j])))
      {
       add_entry(k, "RETRACE_" + DoubleToStr(retrace_levels[j] * 100, 1), max_price_tstamp, max_price);
       p_state.Update(k, j+1);
       logger.log((string)p_booknum[k] + ": " + DoubleToStr(retrace_levels[j], 3) + " retrace", LOG_INFO, __LINE__, __FUNCTION__);
       logger.log((string)p_booknum[k] + ": state = " + (string)p_state[k], LOG_DEBUG, __LINE__, __FUNCTION__);
       logger.log((string)p_booknum[k] + ": Ask=" + DoubleToStr(max_price, Digits) + ", open_prc=" + DoubleToStr(p_open_price[k], Digits) + ", best_prc=" + DoubleToStr(p_best_price[k], Digits) + ", retrace_lvl=" + (string)retrace_levels[j], LOG_DEBUG, __LINE__, __FUNCTION__);
      }
      break; //don't check states once retrace level found
    } 
   }
  }
 }
}

void OrderTracker::timer_engine(void)
{
 int total = p_booknum.Total();
 datetime current = TimeCurrent();
 
 for(int i=total-1; i>=0; i--)
 {
  if(p_close_time[i] > 0 && current - p_close_time[i] > STOP_TRACKING_AFTER_CLOSE_IN_HOURS * 3600)
  {
   logger.log("Stop tracking #" + (string)p_booknum[i], LOG_INFO, __LINE__, __FUNCTION__);
   remove(i);
  }
 }
 
 int time_elapsed = (int)current - (int)p_last_print_orders;
 
 //extensive
 logger.log("check print orders: current-last: " + (string)time_elapsed, LOG_DEBUG, __LINE__, __FUNCTION__, true);
 
 if(time_elapsed >= PRINT_ORDERS_IN_HOURS * 3600)
 {
  string date = TimeToStr(TimeCurrent(), TIME_DATE);
  
  StringReplace(date, " ", "__");
  StringReplace(date, ".", "");
  StringReplace(date, ":", "_");
  
  string folder_name = "OrderTracker";
  FolderCreate(folder_name);
  string filename = folder_name + "\\" + date + ".csv";
  
  int handle = FileOpen(filename, FILE_READ|FILE_WRITE|FILE_SHARE_READ|FILE_SHARE_WRITE|FILE_CSV, ',');
 
  if(handle >= 0)
  {
   FileSeek(handle, 0, SEEK_END);
   
   int count = p_booknum.Total();
   
   for(int i=0; i<count; i++)
   { 
    int booknum = p_booknum[i];
    FileWrite(handle, booknum, print_history(booknum));
   }
   
   FileClose(handle);
   p_last_print_orders = current;
   logger.log("Created " + filename, LOG_DEBUG, __LINE__, __FUNCTION__, true);
  }
  else
   logger.log("Could not open " + filename + " for e = " + ErrorDescription(GetLastError()), LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
 }
}

string OrderTracker::print_history(int ticket)
{
 int pos = p_booknum.SearchLinear(ticket);

 if(pos >=0 )
 {
  CArrayString *history = p_history.Get(pos);
  CArrayDatetime *tstamp = p_tstamp.Get(pos);
  CArrayDouble *price = p_price.Get(pos);
  
  int size = history.Total();
  
  string str = "";
  
  for(int i=0; i<size; i++)
   str += history[i] + "," + TimeToStr(tstamp[i], TIME_DATE|TIME_MINUTES|TIME_SECONDS) + "," + DoubleToStr(price[i], Digits) + ",";
  
  int len = StringLen(str);
  
  if(len > 0)
   return StringSubstr(str, 0, len-1);
  else
   return NULL;
 }
 else
  logger.log("Could not find #" + (string)ticket, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);

 return NULL;
}

void OrderTracker::add_entry(int pos,string msg,datetime tstamp,double prc)
{
  p_history.Get(pos).Add(msg);
  p_tstamp.Get(pos).Add(tstamp);
  p_price.Get(pos).Add(prc);
}

void OrderTracker::tick_engine(void)
{
 int total = p_booknum.Total();
 
 for(int i=0; i<total; i++)
 {
  if(p_close_time[i] == 0)
  {
   if(p_best_price[i] > 0)
   {
    if(p_cmd[i] == OP_BUY)
    { 
     //--- Check max profit update
      if(OrderSelect(p_booknum[i], SELECT_BY_TICKET))
      {
       double profit = OrderProfit();
   
       if(profit > p_max_profit[i])
       {
        p_max_profit.Update(i, profit);
        p_max_profit_tstamp.Update(i, TimeCurrent());
        p_best_price.Update(i, Bid);
       }
      }
      else
       logger.log("Error selecting #" + (string)p_booknum[i], LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
      
    //--- Check retrace update
     for(int j=p_retrace_levels_size-1; j>=0; j--)
     {
      if(p_state[i] >= j  && p_state[i] < p_retrace_levels_size)
      {
       if(Bid <= p_open_price[i] + ((p_best_price[i] - p_open_price[i]) * (1 - retrace_levels[j])))
       {
        add_entry(i, "RETRACE_" + DoubleToStr(retrace_levels[j] * 100, 1), TimeCurrent(), Bid);
        p_state.Update(i, j+1);
        logger.log((string)p_booknum[i] + ": " + DoubleToStr(retrace_levels[j], 3) + " retrace", LOG_INFO, __LINE__, __FUNCTION__);
        logger.log((string)p_booknum[i] + ": state = " + (string)p_state[i], LOG_DEBUG, __LINE__, __FUNCTION__);
        logger.log((string)p_booknum[i] + ": Bid=" + DoubleToStr(Bid, Digits) + ", open_prc=" + DoubleToStr(p_open_price[i], Digits) + ", best_prc=" + DoubleToStr(p_best_price[i], Digits) + ", retrace_lvl=" + (string)retrace_levels[j], LOG_DEBUG, __LINE__, __FUNCTION__);
       }
      
       break; //don't check states once retrace level found
      }
     }
    
    //--- Check [reverse] retrace update
     for(int j=0; j<p_retrace_levels_size; j++)
     {
      if(p_state[i] > j  && p_state[i] <= p_retrace_levels_size)
      {
       if(Bid > p_open_price[i] + ((p_best_price[i] - p_open_price[i]) * (1 - retrace_levels[j])))
       {
        if(j==0)
        {
         add_entry(i, "NEW_BEST_PRICE", TimeCurrent(), Bid);
         p_state.Update(i, 0);
         logger.log((string)p_booknum[i] + ": New high", LOG_INFO, __LINE__, __FUNCTION__);
         logger.log((string)p_booknum[i] + ": state = " + (string)p_state[i], LOG_DEBUG, __LINE__, __FUNCTION__);
         logger.log((string)p_booknum[i] + ": Bid=" + DoubleToStr(Bid, Digits) + ", open_prc=" + DoubleToStr(p_open_price[i], Digits) + ", best_prc=" + DoubleToStr(p_best_price[i], Digits) + ", retrace_lvl=" + (string)retrace_levels[j], LOG_DEBUG, __LINE__, __FUNCTION__);
        }
        else
        {
         add_entry(i, "RETRACE_" + DoubleToStr(retrace_levels[j-1] * 100, 1), TimeCurrent(), Bid);
         p_state.Update(i, j-1);
         logger.log((string)p_booknum[i] + ": " + DoubleToStr(retrace_levels[j-1], 3) + " retrace", LOG_INFO, __LINE__, __FUNCTION__);
         logger.log((string)p_booknum[i] + ": state = " + (string)p_state[i], LOG_DEBUG, __LINE__, __FUNCTION__);
         logger.log((string)p_booknum[i] + ": Bid=" + DoubleToStr(Bid, Digits) + ", open_prc=" + DoubleToStr(p_open_price[i], Digits) + ", best_prc=" + DoubleToStr(p_best_price[i], Digits) + ", retrace_lvl=" + (string)retrace_levels[j], LOG_DEBUG, __LINE__, __FUNCTION__);
        }
       }
      }
      else
       break; //don't check levels less that state[i]
     }
    }
     
    if(p_cmd[i] == OP_SELL)
    {
     //--- Check max profit update
      if(OrderSelect(p_booknum[i], SELECT_BY_TICKET))
      {
       double profit = OrderProfit();
    
       if(profit > p_max_profit[i])
       {
        p_max_profit.Update(i, profit);
        p_max_profit_tstamp.Update(i, TimeCurrent());
        p_best_price.Update(i, Ask);
       }
      }
      else
       logger.log("Error selecting #" + (string)p_booknum[i], LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
       
    //--- Check retrace update
     for(int j=p_retrace_levels_size-1; j>=0; j--)
     {
      if(p_state[i] >= j && p_state[i] < p_retrace_levels_size)
      {
       if(Ask >= p_open_price[i] - ((p_open_price[i] - p_best_price[i]) * (1 - retrace_levels[j])))
       {
        add_entry(i, "RETRACE_" + DoubleToStr(retrace_levels[j] * 100, 1), TimeCurrent(), Ask);
        p_state.Update(i, j+1);
        logger.log((string)p_booknum[i] + ": " + DoubleToStr(retrace_levels[j], 3) + " retrace", LOG_INFO, __LINE__, __FUNCTION__);
        logger.log((string)p_booknum[i] + ": state = " + (string)p_state[i], LOG_DEBUG, __LINE__, __FUNCTION__);
        logger.log((string)p_booknum[i] + ": Ask=" + DoubleToStr(Ask, Digits) + ", open_prc=" + DoubleToStr(p_open_price[i], Digits) + ", best_prc=" + DoubleToStr(p_best_price[i], Digits) + ", retrace_lvl=" + (string)retrace_levels[j], LOG_DEBUG, __LINE__, __FUNCTION__);
       }
      
       break; //don't check states once retrace level found
      }
     }

   //--- Check [reverse] retrace update
    for(int j=0; j<p_retrace_levels_size; j++)
    {
     if(p_state[i] > j  && p_state[i] <= p_retrace_levels_size)
     {
      if(Ask < p_open_price[i] - ((p_open_price[i] - p_best_price[i]) * (1 - retrace_levels[j])))
      {
       if(j==0)
       {
        add_entry(i, "NEW_BEST_PRICE", TimeCurrent(), Ask);
        p_state.Update(i, 0);
        logger.log((string)p_booknum[i] + ": New high", LOG_INFO, __LINE__, __FUNCTION__);
        logger.log((string)p_booknum[i] + ": state = " + (string)p_state[i], LOG_DEBUG, __LINE__, __FUNCTION__);
        logger.log((string)p_booknum[i] + ": Ask=" + DoubleToStr(Ask, Digits) + ", pen_prc=" + DoubleToStr(p_open_price[i], Digits) + ", best_prc=" + DoubleToStr(p_best_price[i], Digits) + ", retrace_lvl=" + (string)retrace_levels[j], LOG_DEBUG, __LINE__, __FUNCTION__);
       }
       else
       {
        add_entry(i, "RETRACE_" + DoubleToStr(retrace_levels[j-1] * 100, 1), TimeCurrent(), Ask);
        p_state.Update(i, j-1);
        logger.log((string)p_booknum[i] + ": " + DoubleToStr(retrace_levels[j-1], 3) + " retrace", LOG_INFO, __LINE__, __FUNCTION__);
        logger.log((string)p_booknum[i] + ": state = " + (string)p_state[i], LOG_DEBUG, __LINE__, __FUNCTION__);
        logger.log((string)p_booknum[i] + ": Ask=" + DoubleToStr(Ask, Digits) + ", open_prc=" + DoubleToStr(p_open_price[i], Digits) + ", best_prc=" + DoubleToStr(p_best_price[i], Digits) + ", retrace_lvl=" + (string)retrace_levels[j], LOG_DEBUG, __LINE__, __FUNCTION__);
       }
      }
     }
     else
      break; //don't check levels less that state[i]
    }
   }
  }
  else
   {
    if(p_cmd[i] == OP_BUY)
    {
     if(Bid >= p_open_price[i] + (PIPS_REQ_INITIAL_HIGH * Point * 10))
     {
      p_best_price.Update(i, Bid);
     
      if(OrderSelect(p_booknum[i], SELECT_BY_TICKET))
      {
       p_max_profit.Update(i, OrderProfit());
       p_max_profit_tstamp.Update(i, TimeCurrent());
      }
      else
       logger.log("Error selecting #" + (string)p_booknum[i], LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
      
      add_entry(i, "NEW_BEST_PRICE", TimeCurrent(), Bid);
      p_state.Update(i, 0);
     }
    }
   
    if(p_cmd[i] == OP_SELL)
    {
     if(Ask <= p_open_price[i] - (PIPS_REQ_INITIAL_HIGH * Point * 10))
     {
      p_best_price.Update(i, Ask);
     
      if(OrderSelect(p_booknum[i], SELECT_BY_TICKET))
      {
       p_max_profit.Update(i, OrderProfit());
       p_max_profit_tstamp.Update(i, TimeCurrent());
      }
      else
       logger.log("Error selecting #" + (string)p_booknum[i], LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
      
      add_entry(i, "NEW_BEST_PRICE", TimeCurrent(), Ask);
      p_state.Update(i, 0);
     }
    }
   }
  }
 }
}

void OrderTracker::get_history(int ticket,CArrayString* &OUT_history,CArrayDatetime* &OUT_tstamp,CArrayDouble* &OUT_price)
{
 int pos = p_booknum.SearchLinear(ticket);
 
 if(pos >=0)
 {
  OUT_history = p_history.Get(pos);
  OUT_tstamp = p_tstamp.Get(pos);
  OUT_price = p_price.Get(pos);
 }
 else
 {
  OUT_history = NULL;
  OUT_tstamp = NULL;
  OUT_price = NULL;
  logger.log("Could not find #" + (string)ticket, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
 }
}


void OrderTracker::remove_old_orders(void)
{
 int total = p_booknum.Total();
 
 for(int i=total-1; i>=0; i--)
 {  
  if(OrderSelect(p_booknum[i], SELECT_BY_TICKET))
  {
   if(p_close_time[i] == 0 && OrderCloseTime() > 0)
   {
    logger.log("Detect close #" + (string)p_booknum[i], LOG_DEBUG, __LINE__, __FUNCTION__, true);
    add_entry(i, "ORDER_CLOSE", OrderCloseTime(), OrderClosePrice());
    p_close_time.Update(i, OrderCloseTime());
   }
  }
  else
  {
   string error = ErrorDescription(GetLastError());
   logger.log("Could not select #" + (string)p_booknum[i] + " for " + error, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__, true);
  }
 }
}

void OrderTracker::remove(int pos)
{
 p_booknum.Delete(pos);
 p_cmd.Delete(pos);
 p_open_price.Delete(pos);
 p_history.Delete(pos);
 p_tstamp.Delete(pos);
 p_price.Delete(pos);
 p_best_price.Delete(pos);
 p_max_profit.Delete(pos);
 p_max_profit_tstamp.Delete(pos);
 p_max_loss.Delete(pos);
 p_max_loss_tstamp.Delete(pos);
 p_state.Delete(pos);
 p_close_time.Delete(pos);
}

int OrderTracker::add_new_order(int ticket,int type,double open_price,double profit,datetime time_current)
{
 p_booknum.Add(ticket);
 p_cmd.Add(type);
 p_open_price.Add(open_price);
 p_best_price.Add(0);
 p_max_profit.Add(profit);
 p_max_profit_tstamp.Add(time_current);
 p_max_loss.Add(profit);
 p_max_loss_tstamp.Add(time_current);
 p_state.Add(-1);
 p_close_time.Add(0);
 
 int index;
 p_history.Add(index);
 p_tstamp.Add(index);
 p_price.Add(index);
 
 int pos = p_booknum.SearchLinear(ticket);
 if(pos < 0 || pos != index)
  logger.log("Invalid pos=" + (string)pos + " and/or index=" + (string)index, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
 //extensive debug
 else
  logger.log((string)ticket + ": pos=" + (string)pos + ", index=" + (string)index, LOG_DEBUG, __LINE__, __FUNCTION__);
  
 return pos;
}

int OrderTracker::add_new_order(int ticket, int type, double open_price, double best_price, double max_profit, datetime max_profit_tstamp, double max_loss, datetime max_loss_tstamp)
{
 p_booknum.Add(ticket);
 p_cmd.Add(type);
 p_open_price.Add(open_price);
 p_best_price.Add(best_price);
 p_max_profit.Add(max_profit);
 p_max_profit_tstamp.Add(max_profit_tstamp);
 p_max_loss.Add(max_loss);
 p_max_loss_tstamp.Add(max_loss_tstamp);
 p_state.Add(-1);
 p_close_time.Add(0);
 
 int index;
 p_history.Add(index);
 p_tstamp.Add(index);
 p_price.Add(index);
 
 int pos = p_booknum.SearchLinear(ticket);
 if(pos < 0 || pos != index)
  logger.log("Invalid pos=" + (string)pos + " and/or index=" + (string)index, LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
 //extensive debug
 else
  logger.log((string)ticket + ": pos=" + (string)pos + ", index=" + (string)index, LOG_DEBUG, __LINE__, __FUNCTION__);
  
 return pos;
}

void OrderTracker::check_for_new_order(void)
{
 int total = OrdersTotal();
 string symbol = Symbol();
 
 for(int i=total-1; i>=0; i--)
 {
  if(OrderSelect(i, SELECT_BY_POS) && OrderSymbol()==symbol)
  {
   int ticket = OrderTicket();
   int type = OrderType();
   
   if(ticket==p_booknum[p_booknum.Total()-1])
    return;
   
   if(type==OP_BUY || type==OP_SELL)
   {
    int pos = p_booknum.SearchLinear(ticket);
   
    if(pos < 0)
    {
     add_new_order(ticket, type, OrderOpenPrice(), OrderProfit(), OrderOpenTime());
     logger.log("Tracking #" + (string)ticket, LOG_DEBUG, __LINE__, __FUNCTION__);
    }
   }
  }
 }
}

OrderTracker *tracker;

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
ENUM_INIT_RETCODE Init()
  {
//---
 EventSetTimer(60);
 
 CArrayInt tickets;
 
 GetTickets(tickets, Symbol());
 
 tracker = new OrderTracker(tickets);

 return(INIT_SUCCEEDED);
  }

//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void Deinit(const int reason)
  {
//---
    delete tracker;

  }

//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void Tick()
  {
//---
    tracker.remove_old_orders();
    tracker.check_for_new_order();
    tracker.tick_engine();
  }

//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
void Timer()
  {
//---
   tracker.timer_engine();
  }

//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void ChartEvent(const int id,
                const long &lparam,
                const double &dparam,
                const string &sparam)
  {

  }
