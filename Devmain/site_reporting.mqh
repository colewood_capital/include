//+------------------------------------------------------------------+
//|                                               site_reporting.mqh |
//|                                       Copyright 2016, Jamal Cole |
//+------------------------------------------------------------------+
#property copyright "jcolecorp"
#property strict

#define END_TAG_LEN     6

#define __NAME__     "site_reporting"

#ifndef BASEDIR
#define BASEDIR
   string BASE_DIR = "site";
#endif

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
#endif 

string HOST_REPORTING;
string STRATEGY;
string MT4_ACCOUNT_NAME;

#ifndef HEADERS
#define HEADERS
   #define HEADERS_WEB
   #include <Super\Headers.mqh>
   #undef HEADERS_WEB
#endif 

bool DISABLE_BACKFILL_TRADES;
uint EA_MIN_UPDATE_IN_SECONDS;
uint PRICE_FEED_MIN_UPDATE_IN_SECONDS;
double MAX_ALLOWABLE_MEMORY_PERCENTAGE_USED;
double MIN_FILE_DISK_SPACE_IN_MB;

#ifndef TESTING
#import "projects\\TerminalInterface\\TerminalInterface.dll"
   void TerminalLogin(int handle, string login, string passwd, string server);
#import
#endif 

#ifndef ARRAYLONG
#define ARRAYLONG
   #include <Arrays\ArrayLong.mqh>
#endif 

#ifndef ARRAYINT
#define ARRAYINT
   #include <Arrays\ArrayInt.mqh>
#endif 

#ifndef DLLUTILS
#define DLLUTILS
   #include <Packages\DllUtils.mqh>
#endif 

#ifndef ENUMS
#define ENUMS
   #include <Super\Enums.mqh>
#endif 

#ifndef HEARTBEAT
#define HEARTBEAT
   #include <Packages\Heartbeat.mqh>
#endif 

#ifndef SUPERVISOR
#define SUPERVISOR
   #include <Packages\Supervisor.mqh>
#endif 

#include <Packages\TickData.mqh>

string FILENAME_BACKLOG = "equity_candlestick_backlog.txt";

#ifndef WEBINTERFACE
#define WEBINTERFACE
   #include <Packages\WebInterface.mqh>
#endif 

#ifndef SAFETYCHECKS
#define SAFETYCHECKS
   #include <Packages\SafetyChecks.mqh>
#endif 

#ifndef UTILS2
#define UTILS2
   #include <Utils2.mqh>
#endif 

class OrderBin
{
private:
 CArrayInt orders;
 Logger logger;
 datetime p_last_history_close_time;
 CArrayInt p_last_history_close_booknum;
 
 void check_order_queue(CArrayInt& opens, int &open_count, CArrayInt& closes, int &close_count);
 void check_history_queue(CArrayInt& opens, int &open_count, CArrayInt& closes, int &close_count);

public:
 void timer_engine(CArrayInt& opens, int &open_count, CArrayInt& closes, int &close_count);
 OrderBin();
};


class EquityCandle
{
 private:
  Logger logger;
  string m_timestamp;
  AutoSaveArrayString* backlog;
  MqlDateTime prev_tstamp;
  string p_symbol;
  double p_prev_equity_at_update;
  
 public:
  double high;
  double low;
  double open;
  double close;
  string GetTimestamp() { return m_timestamp; }
  void back_log_clean_up();
  void reset();
  bool b_new_minute_candle();
  double total_open_lots();
  void POST_equity();
  
  
  EquityCandle();
  ~EquityCandle() { delete backlog; }
};

EquityCandle::EquityCandle() : logger(__FILE__)
{
  reset(); 
  backlog = new AutoSaveArrayString(FILENAME_BACKLOG); 
  TimeCurrent(prev_tstamp);
}

double EquityCandle::total_open_lots()
{
 int total = OrdersTotal();
 double lots = 0;
 
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(i,SELECT_BY_POS))
   lots += OrderLots();
 }
 
 return lots;
}


bool EquityCandle::b_new_minute_candle()
{
 __TRACE__
 
 MqlDateTime tstamp;
 datetime time_current=TimeCurrent();
 TimeToStruct(time_current, tstamp);
 bool update_allowed=true;
 bool ans=false;
 
 if(MathAbs(AccountEquity() - p_prev_equity_at_update) < 0.01)
 {
  LOG_EXTENSIVE("returning false account equity equal to previous equity at last update, " + (string)p_prev_equity_at_update);
  update_allowed=false;
 }
 
 if(update_allowed && tstamp.sec < prev_tstamp.sec)
  ans=true;
 
 prev_tstamp = tstamp;
 return ans;
}


void EquityCandle::reset()
{
  double equity = AccountEquity();
  
 //--- set new timestamp
  m_timestamp = WebServer::SQLTimeCurrent();
  
 //--- reset candle
  open = equity;
  high = equity;
  low = equity;
  close = equity;
}


OrderBin::OrderBin(void) : logger(__FILE__, "helper_processes")
{ 
 __TRACE__
 
 orders.Sort();
 
 //--- check state of order history queue
  int hist_total=OrdersHistoryTotal();
 
  if(hist_total > 0)
  {
   if(OrderSelect(hist_total-1,SELECT_BY_POS,MODE_HISTORY))
   {
    p_last_history_close_time=OrderCloseTime();
    p_last_history_close_booknum.Add(OrderTicket());
   }
   else
    LOG_HIGH_ERROR("Could not select i=" + (string)(hist_total-1)); 
  }
  else
   p_last_history_close_time=0;
}


void OrderBin::timer_engine(CArrayInt& opens, int &open_count, CArrayInt& closes, int &close_count)
{
 opens.Clear();
 closes.Clear();
 open_count=0;
 close_count=0;
 
 check_order_queue(opens, open_count, closes, close_count);
 check_history_queue(opens, open_count, closes, close_count);
}


void OrderBin::check_history_queue(CArrayInt& opens, int &open_count, CArrayInt& closes, int &close_count)
{
  __TRACE__
  
  int total=OrdersHistoryTotal();
  datetime updated_last_history_close_time=0;
  CArrayInt updated_last_history_booknum;
  
  for(int i=total-1; i>=0; i--)
  {
   if(OrderSelect(i,SELECT_BY_POS,MODE_HISTORY))
   {
    int type=OrderType();
    
    if(type==OP_BUY || type==OP_SELL)
    {
     LOG_EXTENSIVE("back check #" + (string)OrderTicket());
    
     datetime close_time=OrderCloseTime();
    
     LOG_EXTENSIVE_2(close_time, "<", p_last_history_close_time);
    
     if(close_time < p_last_history_close_time)
      break;
     else if(close_time > p_last_history_close_time)
     {
       LOG_EXTENSIVE("attempting to backfill");
      
       int ticket=OrderTicket();
      
       int N1 = closes.SearchLinear(ticket);
       int N2 = orders.Search(ticket);
       int N3 = p_last_history_close_booknum.SearchLinear(ticket);
      
       LOG_EXTENSIVE_3_RAW(N1,N2,N3);
      
       if(N1 < 0 && N2 < 0 && N3 < 0)
       {
        LOG_DEBUG_1_RAW(closes); //remove
        LOG_DEBUG_1_RAW(p_last_history_close_booknum); //remove
   
        LOG_DEBUG("Adding #" + (string)ticket + " from history");
        opens.Add(ticket);
        open_count++;
        closes.Add(ticket);
        close_count++;
       }
    
       updated_last_history_close_time=close_time;
       updated_last_history_booknum.Add(ticket);
     }
    }
   }
  }
  
 if(updated_last_history_close_time > 0) {
  p_last_history_close_time = updated_last_history_close_time;
  p_last_history_close_booknum.AssignArray(GetPointer(updated_last_history_booknum));
 }
}


void OrderBin::check_order_queue(CArrayInt& opens, int &open_count, CArrayInt& closes, int &close_count)
{
  __TRACE__
  
  int total=OrdersTotal();
  int i=0;
 
 //---
  LOG_EXTENSIVE_1(orders);
 
  while(i<total)
  {
   if(OrderSelect(i,SELECT_BY_POS))
   {
    int type=OrderType();
    
    if(type==OP_BUY || type==OP_SELL)
    {
     int ticket = OrderTicket();
    
     LOG_EXTENSIVE_2_RAW(orders[i], "=", ticket);
   
     if(orders[i] < ticket)
     {
      int closed_booknum=orders[i];
      closes.Add(closed_booknum);
      orders.Delete(i);
      close_count++;
      LOG_DEBUG("#" + (string)closed_booknum + " closed");
     }
     else if(orders[i] > ticket || orders[i]==INT_MAX)      //orders[i]==INT_MAX on index out of range
     {
      orders.InsertSort(ticket);
      opens.Add(ticket);
      LOG_DEBUG("#" + (string)ticket + " opened");
      open_count++;
      i++;   
     }
     else
      i++;
    }
   }
   else {
    LOG_HIGH_ERROR("Could not select i=" + (string)i);
    break;
   }
  }
  
 //--- remove any extra booknums, defined by orders.Total() > OrdersTotal()
  int new_total=orders.Total();
  int remaining=new_total - total;
  
  LOG_EXTENSIVE_2(new_total, "", total);
  
  for(int j=remaining-1; j>=0; j--)
  {
   int index = new_total-j-1;
   int closed_booknum=orders[index];
   closes.Add(closed_booknum);
   orders.Delete(index);
   close_count++;
   LOG_DEBUG("#" + (string)closed_booknum + " closed");
  }
}

OrderBin *bin;
TradeTable *server;
Reporting *site;
Logger *logger;
EquityCandle *candle;
TerminalComment terminal_comment;
TickDataMachine *tdm;
bool glb_shutdown=true;

struct ConfigChart
{
 int pk;
 long id;
 string symbol;
 ENUM_TIMEFRAMES period;
 string template_name;
};

struct ConfigTemplate
{
 string name;
 string content;
};

struct ConfigFile
{
 string name;
 string content;
};

class Configuration
{
private:
 Logger logger;
 
public:
 bool create_config_file(const ConfigFile &config_file);
 bool create_template(const ConfigTemplate &config_template);
 bool get(string &OUT_config, WebServer *web_server);
 bool parse(string &IN_config,
            ConfigChart &OUT_config_chart[],
            int &config_chart_size,
            ConfigTemplate &OUT_config_template[],
            int &config_template_size,
            ConfigFile &OUT_config_file[],
            int &config_file_size,
            ConfigFile &OUT_extern_var_file[],
            int &extern_var_file_size);
            
 Configuration() : logger(__FILE__) {};
};


bool Configuration::create_config_file(const ConfigFile &config_file)
{
 __TRACE__
 
 LOG_DEBUG("creating config file: " + config_file.name);
 
 string filepath, filename;
 
 FilePathSplit(config_file.name, filepath, filename);
 
 FolderCreate(filepath);
 
 int handle=FileOpen(config_file.name, FILE_WRITE);
 
 if(handle>=0)
 {
  uint bytes=FileWrite(handle, config_file.content);
  
  FileClose(handle);
  
  if(bytes==0) {
   LOG_HIGH_ERROR("Did not write any content to " + config_file.content); 
   return false;
  }
  
  return true;
 }
 else
  LOG_FILE_OPENING_ERROR(config_file.name);
 
 return false;
}


bool Configuration::create_template(const ConfigTemplate &config_template)
{
 __TRACE__
 
 string TEMPLATE_PATH = TerminalInfoString(TERMINAL_DATA_PATH) + "\\templates\\";
 
 string full_path=TEMPLATE_PATH+config_template.name;
 
 if(CFileExists(full_path))
 {
  if(!CFileDelete(full_path)) {
   LOG_HIGH("Failed to delete " + full_path);
   return false;
  }
 }
 
 string TEMP_DIR="SiteReporting\\temp\\";
 FolderCreate(TEMP_DIR);
 string temp_full_path=TerminalInfoString(TERMINAL_DATA_PATH) + "\\MQL4\\Files\\" + TEMP_DIR + config_template.name;
 
 int handle=FileOpen(TEMP_DIR+config_template.name, FILE_WRITE);
 if(handle>=0)
 {
  FileWrite(handle, config_template.content);
  FileClose(handle);
  
  LOG_DEBUG("copy from -> to");
  LOG_DEBUG_1(temp_full_path);
  LOG_DEBUG_1(full_path);
  CFileCopy(temp_full_path, full_path);
  return true;
 }
 else
  LOG_FILE_OPENING_ERROR(temp_full_path);
  
 return false;
}


bool Configuration::parse(string &IN_config,
                          ConfigChart &OUT_config_chart[],
                          int &config_chart_size,
                          ConfigTemplate &OUT_config_template[],
                          int &config_template_size,
                          ConfigFile &OUT_config_file[],
                          int &config_file_size,
                          ConfigFile &OUT_extern_var_file[],
                          int &extern_var_file_size)
{
 __TRACE__

 int pos=0, pos1=0, pos2=0, pos3=0;
 int opt=0, opt1=0, opt2=0;
 
 //--- reset variables
  config_chart_size=0;
  config_file_size=0;
  config_template_size=0;
  ArrayResize(OUT_config_chart, 0);
  ArrayResize(OUT_config_template, 0);
  ArrayResize(OUT_config_file, 0);
  
 
 if(IN_config=="None")
  return true;
 
 opt=StringFind(IN_config, "{</X>}");
 if(opt>=0)
 {
  do
  {
  pos1=0;
  pos2=StringFind(IN_config, "{</N>}");
  ArrayResize(OUT_config_file, ++config_file_size);
       
  OUT_config_file[config_file_size-1].name = StringSubstr(IN_config, pos1, pos2);
  LOG_DEBUG_1_RAW(config_file_size);
  LOG_DEBUG_1_RAW(OUT_config_file[config_file_size-1].name);
       
  pos1 = pos2 + END_TAG_LEN;
  pos2 = StringFind(IN_config, "{</C>}");
  OUT_config_file[config_file_size-1].content = StringSubstr(IN_config, pos1, pos2-pos1); 
  LOG_DEBUG_1_RAW(OUT_config_file[config_file_size-1].content);
  
  //--- shift to next file
   IN_config = StringSubstr(IN_config, pos2+END_TAG_LEN);
  } while(StringFind(IN_config, "{</X>}") > 0);
  
  //--- shift past {</X>}
   IN_config = StringSubstr(IN_config, END_TAG_LEN);
 }

 pos=StringFind(IN_config, "{</E>}");
 if(pos > 0)
 {
  IN_config = StringSubstr(IN_config, 0, pos);
 
  do
  {
   pos=StringFind(IN_config, "{</R>}");
   if(pos==0)
   {
    IN_config = StringSubstr(IN_config, END_TAG_LEN);
    break;
   }
   else if(pos > 0)
   {
    pos1=StringFind(IN_config, "{</S>}");
    if(pos1>=0)
    {
     ArrayResize(OUT_config_chart, ++config_chart_size);
     
     //---
     OUT_config_chart[config_chart_size-1].symbol = StringSubstr(IN_config, 0, pos1);
     LOG_DEBUG_1_RAW(config_chart_size);
     LOG_DEBUG_1_RAW(OUT_config_chart[config_chart_size-1].symbol);
     pos1+=END_TAG_LEN;
    }
    else {
     LOG_HIGH("Could not locate {</S>}");
     return false;
    }
    
    pos2=StringFind(IN_config, "{</P>}");
    if(pos2>=0)
    {
     OUT_config_chart[config_chart_size-1].period = (ENUM_TIMEFRAMES)StringSubstr(IN_config, pos1, pos2-pos1);
     LOG_DEBUG_1_RAW(OUT_config_chart[config_chart_size-1].period);
    }
    else {
     LOG_HIGH("Could not locate {</P>}");
     return false;
    }
    
    pos2+=END_TAG_LEN;
    opt=StringFind(IN_config, "{</V>}");
    if(opt>=0 && opt<StringFind(IN_config, "{</K>}"))
    {
     int k=StringFind(IN_config, "{</N>}");
     
     OUT_extern_var_file[extern_var_file_size-1].name = StringSubstr(IN_config, pos2, k-pos2);
     
     pos2=k+END_TAG_LEN;
     pos3=opt;
     OUT_extern_var_file[extern_var_file_size-1].content = StringSubstr(IN_config, pos2, pos3-pos2);
     
     LOG_DEBUG_1(OUT_extern_var_file[extern_var_file_size-1].name);
     LOG_DEBUG_1(OUT_extern_var_file[extern_var_file_size-1].content);
     pos2=opt + END_TAG_LEN;
    }
    
    pos3=StringFind(IN_config, "{</K>}");
    if(pos3>=0)
    {
     OUT_config_chart[config_chart_size-1].pk = (int)StringSubstr(IN_config, pos2, pos3-pos2);
     LOG_DEBUG_1_RAW(OUT_config_chart[config_chart_size-1].pk);
     IN_config = StringSubstr(IN_config, pos3+END_TAG_LEN);
    }
    else {
     LOG_HIGH("Could not locate {</K>}");
     return false;
    }
   
    pos=StringFind(IN_config, "{</N>}");
    if(pos>0)
    {
     OUT_config_chart[config_chart_size-1].template_name = StringSubstr(IN_config, 0, pos);
     
     ArrayResize(OUT_config_template, ++config_template_size);
    
     OUT_config_template[config_template_size-1].name = StringSubstr(IN_config, 0, pos);
     LOG_DEBUG_1_RAW(config_template_size);
     LOG_DEBUG_1_RAW(OUT_config_template[config_template_size-1].name);
    
     pos1 = pos + END_TAG_LEN;
     pos2 = StringFind(IN_config, "{</T>}");
     OUT_config_template[config_template_size-1].content = StringSubstr(IN_config, pos1, pos2-pos1); 
     LOG_DEBUG_1_RAW(OUT_config_template[config_template_size-1].content);
    
     IN_config = StringSubstr(IN_config, pos2+END_TAG_LEN);
    }
    else {
     LOG_HIGH("[INVALID CONFIG] Could not find {</N>}");
     return false;
    }
    
    //--- check for config file [if_exists]
     opt1 = StringFind(IN_config, "{</C>}");
     opt2 = StringFind(IN_config, "{</R>}");
 
     if(opt1 >= 0 && opt1 < opt2)
     {
      while(True)
      {
       pos=StringFind(IN_config, "{</N>}");
       if(pos > 0)
       {    
        ArrayResize(OUT_config_file, ++config_file_size);
       
        string config_file_name = StringSubstr(IN_config, 0, pos);
        OUT_config_file[config_file_size-1].name = config_file_name;
        LOG_DEBUG_1_RAW(config_file_size);
        LOG_DEBUG_1_RAW(OUT_config_file[config_file_size-1].name);
       
        pos1 = pos + END_TAG_LEN;
        pos2 = StringFind(IN_config, "{</C>}");
        OUT_config_file[config_file_size-1].content = StringSubstr(IN_config, pos1, pos2-pos1); 
        LOG_DEBUG_1_RAW(OUT_config_file[config_file_size-1].content);
        
        //--- shift to next file in json dictionary
         
        opt1=StringFind(IN_config, "{</C>}");
        opt2=StringFind(IN_config, "{</R>}");
     
        if(opt1 >= 0 && opt1 < opt2)
         IN_config = StringSubstr(IN_config, opt1+END_TAG_LEN);
        else
         break;
         
        //--- end config: fetch next symbol
         if(StringSubstr(IN_config, 0, END_TAG_LEN) == "{</R>}")
          break;
       }
       else
        break;
      }
     }

     //IN_config = StringSubstr(IN_config, opt2+END_TAG_LEN); 
     IN_config = StringSubstr(IN_config, END_TAG_LEN); 
   }
   else {
    LOG_HIGH("[INVALID CONFIG] Could not find {</R>}");
    return false;
   }
  } while(StringFind(IN_config, "{</R>}") >= 0);
  
  return true;
 }
 else
  LOG_HIGH("[INVALID CONFIG] Could not find {</E>}");
  
 return false;
}


bool Configuration::get(string &OUT_config,WebServer *web_server)
/*
Uploads config file located in /Files/site/config.txt and returns OK_200 if
0-pile is correctly formatted.
*/
{
 __TRACE__
 
 POSTDATA data;
 
 string name="site\\config.txt";
 int handle=FileOpen(name, FILE_READ|FILE_SHARE_READ);
 
 if(handle>=0)
 {
  string content="";
  
  while(!FileIsEnding(handle))
   content += FileReadString(handle) + "\n";
  FileClose(handle);
  
  data.set("content", content);
  int res_code=web_server.WebRequestPOST(URL_GET_CONFIG, data, OUT_config);
  
  if(res_code==OK_200)
   return true;
   
  return false;
 }
 
 LOG_FILE_OPENING_ERROR(name);
 return false;
}


void EquityCandle::POST_equity()
{
 __TRACE__
 
 //--- Attempt 1: post equity to server
  string data;
  
  LOG_EXTENSIVE_1(server.terminal_pk());
  
  data += "terminal_pk=" + (string)server.terminal_pk() + ";";
  data += "timestamp=" + WebServer::SQLTimeCurrent() + ";";
  data += "openLots=" + DoubleToStr(total_open_lots(), 2) + ";";
  data += "equityOpen=" + (string)round(open) + ";";
  data += "equityLow=" + (string)round(low) + ";";
  data += "equityHigh=" + (string)round(high) + ";";
  data += "equityClose=" + (string)round(close) + ";";
  
  int response = server.WebRequestPOST(URL_SET_EQUITY, data);
  
  if(response==OK_200)
   return; 
  
 //--- Attempt 2: save to auto save array
  if(backlog.Add(data))
   return;
  
 //--- Error message
  logger.log("site_reporting - Critical error: unable to post equity to server nor save to file", LOG_TYPE_LOW, __LINE__, __FUNCTION__);
  logger.log(data, LOG_TYPE_LOW, __LINE__, __FUNCTION__);
  return;
}

void EquityCandle::back_log_clean_up()
{
 __TRACE__
 
 for(int i=0; i<backlog.Total(); i++)
 {
  string data = backlog[i];
  
  LOG_DEBUG_1(data);
  
  int response = server.WebRequestPOST(URL_SET_EQUITY, data);
  
  if(response==OK_200)
  {
   backlog.Delete(i);
   i--;
   continue;
  }
  
  break;
 }
}

Configuration site_config;
Supervisor *charts[];
int glb_charts_count=0;
CArrayLong glb_ignore_charts;
ConfigChart config_chart[];
Heartbeat *price_feed[];
int glb_price_feed_count=0;
Credentials *glb_creds;
Heartbeat *beat;

ENUM_INIT_RETCODE InitVariables(string &host,
                   string &account_name,
                   bool &disable_backfill_trades,
                   uint &ea_min_update_in_seconds,
                   double &max_used_memory_percentage,
                   double &min_disk_space_in_mb,
                   uint &price_feed_min_update_in_seconds)
{
 __TRACE__
 
 string name="site\\config.txt";
 
 //----
  bool b_host=false;
  bool b_account_name=false;
  bool b_disable_backfill_trades=false;
  bool b_ea_min_update_in_seconds=false;
  bool b_max_used_memory_percentage=false;
  bool b_min_disk_space_in_mb=false;
  bool b_price_feed_min_update_in_seconds=false;
 
 int handle=FileOpen(name, FILE_READ);
 
 if(handle>=0)
 {
  while(!FileIsEnding(handle))
  {
   string line=StringTrim(FileReadString(handle));
   
   if(StringSubstr(line,0,9)=="[settings")
   {
    string search_val;
    
    while(line!="[chart]" && !FileIsEnding(handle))
    {
     line=StringTrim(FileReadString(handle));
     
     search_val="host:";
     if(!b_host && StringSubstr(line, 0, StringLen(search_val))==search_val) {
      host = StringTrim(StringSubstr(line, StringLen(search_val)));
      b_host = true;
      Print("setting host=", host);
     }
     
     search_val="account_name:";
     if(!b_account_name && StringSubstr(line, 0, StringLen(search_val))==search_val) {
      account_name = StringTrim(StringSubstr(line, StringLen(search_val)));
      b_account_name = true;
      Print("setting account_name=", account_name);
     }
     
     search_val="disable_backfill_trades:";
     if(!b_disable_backfill_trades && StringSubstr(line, 0, StringLen(search_val))==search_val) {
      disable_backfill_trades = Strings::StringToBool(StringTrim(StringSubstr(line, StringLen(search_val))));
      b_disable_backfill_trades = true;
      Print("setting disable_backfill_trades=", disable_backfill_trades);
     }
      
     search_val="ea_min_update_in_seconds:";
     if(!b_ea_min_update_in_seconds && StringSubstr(line, 0, StringLen(search_val))==search_val) {
      ea_min_update_in_seconds = (uint)StringTrim(StringSubstr(line, StringLen(search_val)));
      b_ea_min_update_in_seconds = true;
      Print("setting ea_min_update_in_seconds=", ea_min_update_in_seconds);
     }
     
     search_val="price_feed_min_update_in_seconds:";
     if(!b_price_feed_min_update_in_seconds && StringSubstr(line, 0, StringLen(search_val))==search_val) {
      price_feed_min_update_in_seconds = (uint)StringTrim(StringSubstr(line, StringLen(search_val))); 
      b_price_feed_min_update_in_seconds = true;
      Print("setting price_feed_min_update_in_seconds=", price_feed_min_update_in_seconds);
     }
     
     search_val="max_used_memory_percentage:";
     if(!b_max_used_memory_percentage && StringSubstr(line, 0, StringLen(search_val))==search_val) {
      max_used_memory_percentage = (double)StringTrim(StringSubstr(line, StringLen(search_val)));
      b_max_used_memory_percentage = true;
      Print("setting max_used_memory_percentage=", max_used_memory_percentage);
     }
     
     search_val="min_disk_space_in_mb:";
     if(!b_min_disk_space_in_mb && StringSubstr(line, 0, StringLen(search_val))==search_val) {
      min_disk_space_in_mb = (double)StringTrim(StringSubstr(line, StringLen(search_val)));
      b_min_disk_space_in_mb = true;
      Print("setting min_disk_space_in_mb=", min_disk_space_in_mb);
     }
    }
   }
  }
  
  FileClose(handle);
  
  //--- check variables
     bool b_found_all_variables=true;
     
     if(!b_host) {
      LOG_HIGH("Could not find 'host:' in " + name);
      b_found_all_variables=false;
     }
     
     if(!b_account_name) {
      LOG_HIGH("Could not find 'account_name:' in " + name);
      b_found_all_variables=false;
     }
     
     if(!b_disable_backfill_trades) {
      LOG_HIGH("Could not find 'disable_backfill_trades:' in " + name);
      b_found_all_variables=false;
     }
     
     if(!b_ea_min_update_in_seconds) {
      LOG_HIGH("Could not find 'ea_min_update_in_seconds:' in " + name);
      b_found_all_variables=false;
     }
     
     if(!b_max_used_memory_percentage) {
      LOG_HIGH("Could not find 'max_used_memory_percentage:' in " + name);
      b_found_all_variables=false;
     }
     
     if(!b_min_disk_space_in_mb) {
      LOG_HIGH("Could not find 'min_disk_space_in_mb:' in " + name);
      b_found_all_variables=false;
     }
     
     if(!b_price_feed_min_update_in_seconds) {
      LOG_HIGH("Could not find 'price_feed_min_update_in_seconds:' in " + name);
      b_found_all_variables=false;
     }
     
     if(!b_found_all_variables)
      return INIT_PARAMETERS_INCORRECT;
      
   return INIT_SUCCEEDED;
 }
 
 LOG_FILE_OPENING_ERROR(name);
 return INIT_PARAMETERS_INCORRECT;
}

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
ENUM_INIT_RETCODE Init()
  {
//---
 __TRACE__
 
 EventSetTimer(3);
 
 logger = new Logger("default");
 
 CArrayInt booknum;
 
 GlobalVarDelAll();
 
 ENUM_INIT_RETCODE b_init_variables_result = InitVariables(
       HOST_REPORTING, 
       MT4_ACCOUNT_NAME, 
       DISABLE_BACKFILL_TRADES, 
       EA_MIN_UPDATE_IN_SECONDS, 
       MAX_ALLOWABLE_MEMORY_PERCENTAGE_USED, 
       MIN_FILE_DISK_SPACE_IN_MB, 
       PRICE_FEED_MIN_UPDATE_IN_SECONDS);
 
 if(b_init_variables_result != INIT_SUCCEEDED)
  return b_init_variables_result;

 
 GlobalVariableSetString(GLB_HOST_PREFIX, HOST_REPORTING);
 
 
 //--- close all old charts
  long my_id=ChartID();
  long ch_id=ChartFirst();
  
  do {
   if(ch_id != my_id)
    if(!ChartClose(ch_id))
     LOG_HIGH_ERROR("Failed to close chart #" + (string)ch_id);
     
   ch_id = ChartNext(ch_id);
  } while(ch_id >= 0);

 //--- get credentials to connect to trade server
  __ glb_creds = new Credentials(HOST_REPORTING, MT4_ACCOUNT_NAME);
  
  if(glb_creds.is_valid)
  {
   if(!IsConnected() || AccountNumber()!=(int)glb_creds.acct_num || GetServerName()!=glb_creds.server_name)
    Login(glb_creds);
   else
    LOG_DEBUG("Already logged into trade server");
  }
  else {
   LOG_NETWORK("Invalid login creditions for acct_name='" + MT4_ACCOUNT_NAME + "'");
   return INIT_FAILED;
  }
  
  if(!IsConnected()) {
   LOG_HIGH("Not connected to trade server. Removing expert ...");
   ExpertRemove();
  }
  
 //--- connect to site report server
  __ server = new TradeTable(HOST_REPORTING, glb_creds);
  tdm = new TickDataMachine(server, URL_SEND_TICK_DATA, 10);
  site = new Reporting(BASE_DIR, server.terminal_pk());
  
 //--- download configurations
  string raw_config;
  bool ans=site_config.get(raw_config, server);
  if(!ans) 
  {
   LOG_HIGH("Invalid configuration");
   return(INIT_PARAMETERS_INCORRECT);
  }

  int sz_config_template, sz_config_file, sz_extern_var_file;
  ConfigTemplate config_template[];
  ConfigFile config_file[];
  ConfigFile extern_var_file[];
  
  if(site_config.parse(raw_config, config_chart, glb_charts_count, config_template, sz_config_template, config_file, sz_config_file, extern_var_file, sz_extern_var_file)) 
  {
   for(int i=0; i<sz_config_template; i++)
    if(!site_config.create_template(config_template[i]))
    {
     LOG_HIGH("Failed to create template: " + config_template[i].name);
     return(INIT_FAILED);
    }
   
   for(int i=0; i<sz_config_file; i++)
    if(!site_config.create_config_file(config_file[i]))
    {
     LOG_HIGH("Failed to create file: " + config_file[i].name);
     return(INIT_FAILED);
    }
  } 
  else
  {
   LOG_HIGH("Failed to parse configuration: " + raw_config);
   return(INIT_FAILED);
  }
  
 //--- initialze objects that require logger downloaded from web server
  delete logger;
  logger = new Logger(__FILE__);
  bin = new OrderBin();
  candle = new EquityCandle();
 
 if(!DISABLE_BACKFILL_TRADES)
  server.backfill(booknum);

 int total=booknum.Total();
 
 Print("Begin backfilling ", total, " trades");
 
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(booknum[i],SELECT_BY_TICKET))
  {
   int type=OrderType();
   
   if(type==OP_BUY || type==OP_SELL)
   {
    if(server.POST_open(booknum[i], 
                     OrderSymbol(), 
                     OrderType(), 
                     OrderLots(), 
                     OrderOpenTime(), 
                     OrderOpenPrice(), 
                     OrderStopLoss(), 
                     OrderTakeProfit(), 
                     OrderComment()))
    {
     server.POST_close(booknum[i],
                       OrderSymbol(),
                       OrderStopLoss(),
                       OrderTakeProfit(),
                       OrderCloseTime(),
                       OrderClosePrice(),
                       OrderCommission(),
                       OrderSwap(),
                       OrderComment(),
                       OrderProfit());
    }
    else
     LOG_HIGH("Failed to backfill #" + (string)booknum[i]);
   
    UpdateBalanceCommission(booknum[i], 0);
   }
  }
  else
   LOG_HIGH_ERROR("Could not select #" + (string)booknum[i]);
 }
 

  LOG_DEBUG_RAW("POST_update: DB_UPDATE_FOLDER_NAME=" + DB_UPDATE_FOLDER_NAME);
  server.POST_update(DB_UPDATE_FOLDER_NAME, charts);
 
 
 //--- initialize heartbeat
  beat =new Heartbeat(MODE_SERVER, EA_MIN_UPDATE_IN_SECONDS); 
  
 //--- find server offset
  int local_server_offset=0;
  datetime t0=TimeCurrent();
  Print("Waiting for next tick to determine server offset ...");
  while(!IsStopped())
  {
   if(TimeCurrent() > t0)
   {
    local_server_offset = (int)TimeCurrent() - (int)TimeLocal();
    LOG_INFO("Server offset set at " + DoubleToString(local_server_offset/3600.0, 1) + " hours");
    break;
   }
   Sleep(100);
  }
  
  if(IsStopped()) {
   glb_charts_count=0;
   return(INIT_FAILED);
  }
  
 //--- initialize supervisor charts
  CArrayString symbol;
  ArrayResize(charts, glb_charts_count);
  for(int i=0; i<glb_charts_count; i++)
  {
   LOG_DEBUG_1("creating new supervisor obj for site chart_pk# " + (string)config_chart[i].pk + " (" + config_chart[i].symbol + ")");
    
   charts[i] = new Supervisor(config_chart[i].pk, config_chart[i].symbol, config_chart[i].period, EA_MIN_UPDATE_IN_SECONDS, config_chart[i].template_name);
   
   if(symbol.SearchLinear(config_chart[i].symbol) < 0)
    symbol.Add(config_chart[i].symbol);
  }
  
   
 //--- create a heartbeat to check each symbol
  glb_price_feed_count=symbol.Total();
  ArrayResize(price_feed, glb_price_feed_count);
  
  for(int i=0; i<glb_price_feed_count; i++)
  {
   price_feed[i] = new Heartbeat(MODE_PRICE_FEED, PRICE_FEED_MIN_UPDATE_IN_SECONDS, symbol[i], local_server_offset);
  }

  terminal_comment.engine();
  
  glb_ignore_charts.Add(ChartID());
  
 return(INIT_SUCCEEDED);
  }
  
void GlobalVarDelAll()
{ 
  GlobalVariablesDeleteAll("HEARTBEAT_");
  GlobalVariablesDeleteAll(GLB_HOST_PREFIX);
  GlobalVariablesDeleteAll(GLB_CONFIG_INIT_STATUS);
}

void Shutdown()
{
  for(int i=glb_charts_count-1; i>=0; i--)
   delete charts[i];
  glb_charts_count=0;
 
  GlobalVarDelAll();
  
  for(int i=glb_price_feed_count-1; i>=0; i--)
   delete price_feed[i];
  glb_price_feed_count=0;
}

//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void Deinit(const int reason)
 {
//---
  EventKillTimer();
  
  if(glb_shutdown)
   Shutdown();
   
  delete beat;
   
  delete glb_creds;
  
  delete site;
  
  delete tdm;
  
  delete server;
  
  delete candle;
  
  delete bin;
  
  delete logger;
 }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void Tick()
 {
//---
   
 }

//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
void Timer()
  {
//---
   __TRACE__
   
   CArrayInt open, close;
   int open_count, close_count;
   bin.timer_engine(open, open_count, close, close_count);
   static SafetyChecks safeties(BASE_DIR);
   static uint tick_data_counter=0;
   
   //---- record new opens
    for(int i=0; i<open_count; i++)
    { 
     if(OrderSelect(open[i],SELECT_BY_TICKET))
     {
      int type=OrderType();
      
      if(type==OP_BUY || type==OP_SELL)
      { 
       server.POST_open(open[i], 
                        OrderSymbol(), 
                        OrderType(), 
                        OrderLots(), 
                        OrderOpenTime(), 
                        OrderOpenPrice(), 
                        OrderStopLoss(), 
                        OrderTakeProfit(), 
                        OrderComment());                      
      }
     }
    }
   
   //---- record new closes
     for(int i=0; i<close_count; i++)
     {
      if(OrderSelect(close[i],SELECT_BY_TICKET))
      {
       int type=OrderType();
      
       if(type==OP_BUY || type==OP_SELL)
       { 
        server.POST_close(close[i],
                          OrderSymbol(),
                          OrderStopLoss(),
                          OrderTakeProfit(),
                          OrderCloseTime(),
                          OrderClosePrice(),
                          OrderCommission(),
                          OrderSwap(),
                          OrderComment(),
                          OrderProfit());
       }
      }
      
      //--- check for balance commission
       UpdateBalanceCommission(close[i]);
     }
     
   //--- Update equity candlesticks
    double equity = AccountEquity();
    candle.close = equity;
   
    if(equity < candle.low)    
     candle.low = equity;
      
    if(equity > candle.high)   
     candle.high = equity;
   
    candle.back_log_clean_up();
   
    if(candle.b_new_minute_candle())
    {
     candle.POST_equity();
     candle.reset();
    }
    
   //--- Post tick data to site
    if(MathMod(++tick_data_counter, 5)==0)
     tdm.timer_engine();
  
    
   //--- Post logs to site
    string fname;
    long search_handle=FileFindFirst(SITE_REPORTING_PATH + "*.txt", fname);

    if(search_handle != INVALID_HANDLE)
    {
     do
     {
      if(StringLen(fname) > 0) 
      {
       string fullpath=StringConcatenate(SITE_REPORTING_PATH, fname);
       int handle=FileOpen(fullpath,FILE_READ,'\t');
       LOG_EXTENSIVE("Opening: " + fullpath);
      
       if(handle>=0)
       {
        if(FileSize(handle)==0) {
         FileClose(handle);
         FileDelete(fullpath);
         continue;
        }
        string terminal_pk=(string)server.terminal_pk();
        string timestamp=server.toSQLTime((datetime)FileReadString(handle));
        string level=FileReadString(handle);
        string message=FileReadString(handle);
        string _traceback=FileReadString(handle);
        string symbol=FileReadString(handle);
        string timeframe=FileReadString(handle);
       
        POSTDATA data;
        data.set("terminal_pk", terminal_pk);
        data.set("timestamp", timestamp);
        data.set("level", level);
        data.set("message", message);
        data.set("traceback", _traceback);
        data.set("symbol", symbol);
        data.set("timeframe", timeframe);
       
        FileClose(handle);
       
        int res_code=server.WebRequestPOST(URL_POST_LOG, data);
       
        if(res_code==OK_200)
        {
         if(!FileDelete(fullpath))
         { 
          LOG_HIGH_ERROR("Error deleting " + fullpath);
         }
        }
       
        if(res_code==OK_NOCONTENT)
        {
         LOG_DEBUG("Returned NO_CONTENT");
        }
       }
       else
        LOG_FILE_OPENING_ERROR(fullpath) 
      } 
     }
     while(FileFindNext(search_handle, fname));
     
     FileFindClose(search_handle);
    }
    
   //--- safety checks  
   {
    //--- check equity (percentage change)
     ENUM_SAFETY_CHECK_EQUITY eq_type;
     double perc_change;
     double time_in_minutes;
     double old_equity;
     string action;
    
     if(safeties.check_equity(eq_type, perc_change, time_in_minutes, old_equity, action))
     {
      string status="equity", message="";
      string delta=DoubleToStr(perc_change*100.0, 1);
      
      if(eq_type==CHECK_EQUITY_PERCENT_CHANGE_LESS_THAN)
       status+=" has decreased ";
      if(eq_type==CHECK_EQUITY_PERCENT_CHANGE_GREATER_THAN)
       status+=" has increased ";
       
      status += "$" + DoubleToStr(old_equity, 0) + " -> $" + DoubleToStr(AccountEquity(),0);
       
      string suffix = status + " (>=" + delta + "%) within " + (string)time_in_minutes + " minutes";
      
      if(action=="shut_down")
       message = "[SAFETY] Shutting down terminal for equity " + suffix;
      
      if(action=="warning")
       message = "[SAFETY][WARNING] " + suffix;
       
      if(action=="close_all_positions_and_shutdown")
        message = "[SAFETY] Closing all positions and shutting down terminal for " + suffix;
       
       __ ReportSafetyAlert(action, message);
     }
    }
    
    {
     //--- check volume
      string symbol;
      double lots;
      double time_in_minutes;
      string action;
      
      if(safeties.check_volume(symbol, lots, time_in_minutes, action))
      {
       string message;
       string suffix=symbol + " traded volume >= " + (string)lots + " lots within " + (string)time_in_minutes + " minutes";
       
       if(action=="shut_down")
        message = "[SAFETY] Shutting down terminal for " + suffix;
       
       if(action=="warning")
        message = "[SAFETY][WARNING] " + suffix;
       
       if(action=="close_all_positions_and_shutdown")
        message = "[SAFETY] Closing all positions and shutting down terminal for " + suffix;
        
       __ ReportSafetyAlert(action, message);
      }
    }
    
    {
     //--- check position size
      string symbol;
      double lots;
      string action;
      
      if(safeties.check_positions(symbol, lots, action))
      {
       string message;
       string suffix =  symbol + " position size >= " + (string)lots + " lots";
       
       if(action=="shut_down")
        message = "[SAFETY] Shutting down terminal for " + suffix;
       
       if(action=="warning")
        message = "[SAFETY][WARNING] " + suffix;
        
       if(action=="close_all_positions_and_shutdown")
        message = "[SAFETY] Closing all positions and shutting down terminal for " + suffix;
      
       __ ReportSafetyAlert(action, message);
      }
    }
   
   
   //--- update charts supervisor
    long prev_chart_id;
     for(int i=0; i<glb_charts_count; i++) {  
      __ charts[i].update(prev_chart_id);
     }
   /*
   //--- close any charts not registered with supervisor
    CloseNonRegisteredCharts();
    
   //--- check terminal vitals
    int memory_used=TerminalInfoInteger(TERMINAL_MEMORY_USED) / 10;
    double memory_available=(double)TerminalInfoInteger(TERMINAL_MEMORY_AVAILABLE) / 10.0;
    double memory_percentage_used= (memory_used/memory_available) * 100;
    
    if(memory_percentage_used >= MAX_ALLOWABLE_MEMORY_PERCENTAGE_USED*100)
     LOG_SYSTEM_LINE_FILTERING("Low memory - percentage used (" + DoubleToStr(memory_percentage_used, 0) + "%) >= MAX_ALLOWABLE_MEMORY_PERCENTAGE_USED");
    */
    int terminal_disk_space=TerminalInfoInteger(TERMINAL_DISK_SPACE);
    
    if(terminal_disk_space < MIN_FILE_DISK_SPACE_IN_MB)
     LOG_SYSTEM_LINE_FILTERING("Low disk space (" + (string)terminal_disk_space + " MB) < MIN_FILE_DISK_SPACE_IN_MB");
   
    //terminal_comment.set("Memory Used", (string)memory_used + " MB");
    //terminal_comment.set("Memory Percentaged Used", DoubleToStr(memory_percentage_used, 0) + "%");
    terminal_comment.set("Terminal Disk Space", (string)terminal_disk_space + " MB");
    
   //--- check price feed
    for(int i=0; i<glb_price_feed_count; i++)
    {
     datetime last_update_time;
     double last_rec_bid, last_rec_ask;
     uint elapsed_secs;
     
     if(price_feed[i].price_feed_timer_engine(last_update_time, last_rec_bid, last_rec_ask, elapsed_secs))
      LOG_NETWORK(price_feed[i].symbol() + " tick has not changed from (" + (string)last_rec_bid + ", " + (string)last_rec_ask + ") in more than " + (string)price_feed[i].min_sec_update() + " seconds");
    }
    
   //--- check trading conditions
    if(!IsTradeAllowed())
     LOG_SYSTEM("Trading not allowed");
     
    if(!IsExpertEnabled())
     LOG_SYSTEM("Experts not allowed");
     
    if(!IsDllsAllowed())
     LOG_SYSTEM("DLLs not allowed");
     
    if(!IsLibrariesAllowed())
     LOG_SYSTEM("Expert libraries not allowed"); 
     
    if(!IsConnected())
     LOG_NETWORK("Not connected to trade server");
    
   //--- check that account credentials
    if(!IsConnected() || AccountNumber()!=(int)glb_creds.acct_num || GetServerName()!=glb_creds.server_name)
     Login(glb_creds);
     
   //--- send heartbeat to python server
    int handle=FileOpen("SiteReporting\\last_heartbeat.txt", FILE_WRITE|FILE_READ|FILE_SHARE_READ);
    MqlDateTime local;
    TimeLocal(local);
    datetime utclocal=TimeGMT(local);
    FileWrite(handle, TimeToStr(utclocal,TIME_DATE|TIME_MINUTES|TIME_SECONDS));
    FileClose(handle);
     
    terminal_comment.engine();
  }
//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void ChartEvent(const int id,
                const long &lparam,
                const double &dparam,
                const string &sparam)
  {
   __TRACE__
   
   if(id==CHARTEVENT_CUSTOM + CHARTEVENT_UPDATE_TRADES)
   {
    LOG_DEBUG_RAW("POST_update: DB_UPDATE_FOLDER_NAME=" + DB_UPDATE_FOLDER_NAME);
    server.POST_update(DB_UPDATE_FOLDER_NAME, charts);
   }
   
   if(id==CHARTEVENT_CUSTOM + CHARTEVENT_HEARTBEAT)
   {
    long chart_id=lparam;
    
    for(int i=0; i<glb_charts_count; i++)
    {
     if(charts[i].id()==chart_id)
     {
      beat.chart_event_engine(charts[i], lparam, dparam, sparam);
      return;
     }
    }
    
    //--- known issue, the Event queue overload and cannot find chart_id b.c. it is looking at past
    //--- event messages and charts[] has already been updated with the new chart_id
    LOG_DEBUG("Could not find chart_id=" + (string)chart_id);
   }
  }
//+------------------------------------------------------------------+
bool CloseNonRegisteredCharts()
{
 __TRACE__
 
 int total=ChartsTotal();
 long my_id=ChartID();
 
 if(total!=glb_charts_count+1)  // add 1 for the chart site_reporting is running on
 {
  LOG_LOW_RAW("# of detected charts (" + (string)total + ") != # of registered charts (" + (string)(glb_charts_count+1) + ")");
  
  long k=ChartFirst();
  while(k>0)
  {
   if(k!=my_id)
   {
    bool b_found_chart=false;
   
    for(int i=0; i<glb_charts_count; i++)
    {
     long id=charts[i].id();
     if(id==k) {
      b_found_chart=true;
      break;
     }
    }
   
    if(!b_found_chart) {
     LOG_LOW_RAW("Closing #" + (string)k + " (" + ChartSymbol(k) + ", " + EnumToString((ENUM_TIMEFRAMES)ChartPeriod(k)) + ") for not registed");
     Charts::ChartCloseSafe(k);
    }
   }
   
   k=ChartNext(k);
  }
  
  return true;
 }
 
 return false;
}
//+------------------------------------------------------------------+
bool Login(Credentials &screds)
{
 __TRACE__
 
  int MIN_LOGIN_DELAY=10;
  static datetime last_login_attempt;
  
  if(TimeLocal() - last_login_attempt >= MIN_LOGIN_DELAY) 
  {
   last_login_attempt = TimeLocal();
   
   LOG_INFO("Attempting to log into account (name=" + MT4_ACCOUNT_NAME + ", server_host=" + screds.server_host + ")");
   ResetLastError();
 
   int handle = WindowHandle(Symbol(), Period());
   const int RETRIES = 5;
   const int TIMEOUT_SECS = 12;
   ulong _start = GetMicrosecondCount();
  
   for(int i=0; i<RETRIES; i++)
   {
    TerminalLogin(handle, screds.acct_num, screds.passwd, screds.server_host);
   
    while((GetMicrosecondCount()-_start) / 1000000.0 < TIMEOUT_SECS)
    {
     if(IsConnected()) {
      LOG_INFO("Successfully logged in");
      screds.server_name = GetServerName();
      return true;
     }
    
     Sleep((int)MathMax(10000, (i*1.5)*10000));
    }
   }//end for
  
   LOG_NETWORK("Failed to log into account (name=" + MT4_ACCOUNT_NAME + ")");
   return false;
  }
  
  return true;
}
//+------------------------------------------------------------------+
void UpdateBalanceCommission(int ticket, int history_min_booknum=0)
{
  __TRACE__
  
  static int most_recent_closed_ticket;
       
  __ double balance_commission=CheckCommissionBalance(ticket, most_recent_closed_ticket);
 
  if(MathAbs(balance_commission) > DBL_EPSILON)
  {
   double original_commission=0;
  
   if(OrderSelect(ticket,SELECT_BY_TICKET))
   {
    original_commission=OrderCommission();
   }
   else {
    LOG_HIGH("Error selecting #" + (string)ticket);
    return;
   }
  
   POSTDATA data;
  data.set("terminal_pk", (string)server.terminal_pk());
  data.set("booknum", (string)ticket);
  data.set("commission", (string)(original_commission+balance_commission));
  int response = server.WebRequestPOST(URL_SET_CLOSED_TRADES, data);
        
  if(response==OK_200)
  {
   LOG_EXTENSIVE("Balance commission update POST Successful");
  }
  else
   LOG_HIGH("Failed to update balance commission");
 }
 
 if(history_min_booknum > 0)
  most_recent_closed_ticket = ticket;
} 
//+------------------------------------------------------------------+
double CheckCommissionBalance(int ticket, int history_min_booknum=0)
{
 __TRACE__
 
 static CArrayInt old_booknum;
 
 int total=OrdersHistoryTotal();
 double commission=0;
 
 for(int i=total-1; i>=0; i--)
 {
  if(OrderSelect(i,SELECT_BY_POS,MODE_HISTORY))
  {
   int hist_ticket=OrderTicket();
   
   if(history_min_booknum > 0 && hist_ticket <= history_min_booknum)
    break;
    
   if(OrderType()==ORDER_TYPE_BALANCE)
   {
    string cmt=OrderComment();
    string search="Commission - ";
    int pos=StringFind(cmt, search);
    
    if(pos>=0)
    {
     int booknum=(int)StringSubstr(cmt, StringLen(search));
     
     LOG_DEBUG("searching #" + (string)hist_ticket + " comment '" + cmt + "'");
     LOG_DEBUG_1(booknum);
     
     if(booknum==ticket && old_booknum.SearchLinear(hist_ticket) < 0)
     {
      double balance_commission=OrderProfit();
      LOG_DEBUG("Adding commission of " + (string)balance_commission + " to ticket #" + (string)ticket);
      commission += balance_commission;
      old_booknum.Add(hist_ticket);
     }
    }
   }
  }
 }
 
 LOG_DEBUG_1(commission);
 return commission;
}
//+------------------------------------------------------------------+
void ReportSafetyAlert(string action, string message)
{
  __TRACE__
  
  if(action=="shut_down")
  {
   LOG_HIGH(message);
   
   if(glb_shutdown) {
    Shutdown();
    glb_shutdown=false;
   }
  }
  else if(action=="warning")
  {
   LOG_HIGH(message);
  }
  else if(action=="close_all_positions_and_shutdown")
  {
   LOG_HIGH(message);
   
   int total=OrdersTotal();
   for(int i=total-1; i>=0; i--)
   {
    if(OrderSelect(i,SELECT_BY_POS))
     site.OrderClose(message, OrderTicket(), OrderClosePrice(), OrderLots());
   }
   
   if(glb_shutdown) {
    Shutdown();
    glb_shutdown=false;
   }
  }
  else
   LOG_HIGH("Unknown action=" + action);
}

