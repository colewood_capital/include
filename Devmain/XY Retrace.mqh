//+------------------------------------------------------------------+
//|                                                   XY Retrace.mqh |
//|                                       Copyright 2016, Jamal Cole |
//+------------------------------------------------------------------+
#property copyright "jcolecorp"
#property strict

#ifndef CONFIG
#define CONFIG
   #include <Packages\Config.mqh>
#endif 

EXTERN(double, BUY_RETRACE_PIPS);
EXTERN(double, SELL_RETRACE_PIPS);
double last_bid;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
ENUM_INIT_RETCODE Init()
  {
//---
 EventSetTimer(60);
 
 INIT(double, BUY_RETRACE_PIPS, INIT_PARAMETERS_INCORRECT);
 
 INIT(double, SELL_RETRACE_PIPS, INIT_PARAMETERS_INCORRECT);
 
 last_bid = Bid;

 return(INIT_SUCCEEDED);
  }

//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void Deinit(const int reason)
  {
//---


  }

//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void Tick()
  {
//---
   if(last_bid - Bid >= BUY_RETRACE_PIPS * 10 * Point)
    int ticket=OrderSend(Symbol(), OP_BUY, 0.1, Ask, 5, 0, 0);
    
   if(Bid - last_bid >= SELL_RETRACE_PIPS * 10 * Point)
    int ticket=OrderSend(Symbol(), OP_SELL, 0.1, Bid, 5, 0, 0);
  }

//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
void Timer()
  {
//---

  }

//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void ChartEvent(const int id,
                const long &lparam,
                const double &dparam,
                const string &sparam)
  {

  }
