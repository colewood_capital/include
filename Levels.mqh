//+------------------------------------------------------------------+
//|                                                       Levels.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#define BASEDIR
string BASE_DIR = "Levels";

#ifndef ENUMS
#define ENUMS
   #include <Super/Enums.mqh>
#endif 

#ifndef STRINGS
#define STRINGS
   #include <Packages/Strings.mqh>
#endif 

#ifndef LOGGER
#define LOGGER
   #define LOGGER_DEPRECATED
   #include <Logger.mqh>
#endif 

Logger levelslogger(__FILE__, "helper_processes");

enum EnumLevels { ENUM_LEVELS_WAITING_TO_OPEN = -10,
                  ENUM_LEVELS_CANCELLED,
                  ENUM_LEVELS_NEW_TICKET_OPENED };
                  
                  
class Level
{
 private:
  bool p_ready_to_open;
  double p_target_price;
  double p_open_price;
  double p_lots;
  
 public:
  
  Level(double target_price, double open_price, double lots);
  ~Level();
};

Level::Level(double target_price,double open_price,double lots)
{
 p_target_price = target_price;
 p_open_price = open_price;
 p_lots = lots;
 p_ready_to_open = false;
}

Level::~Level()
{
}
                
EnumLevels LevelsOpenFunction(double target_price, double open_price, double lots, int& ticket, double lower_cancel_price=0, double upper_cancel_price=0)
{
 static bool ready_to_open = false;
 static bool cancelled = false;
 static double past_target_price;
 static double past_lots;
 
 if(target_price != past_target_price || lots * past_lots < 0)
 {
  levelslogger.log("Resetting open function", LOG_DEBUG, __LINE__, __FUNCTION__);
  ready_to_open = false;
  cancelled = false;
  past_target_price = target_price;
  past_lots = lots;
 }

 if(lots > 0)
 {
  if(upper_cancel_price && Bid >= upper_cancel_price) {
   levelslogger.log("Cancelling open [BUY] attempt for bid=" + (string)Bid + " >= upper_cancel_price=" + (string)upper_cancel_price, LOG_INFO, __LINE__, __FUNCTION__);
   cancelled = true;
  }
  
  if(lower_cancel_price && Bid <= lower_cancel_price) {
   levelslogger.log("Cancelling open [BUY] attempt for bid=" + (string)Bid + " <= lower_cancel_price=" + (string)lower_cancel_price, LOG_INFO, __LINE__, __FUNCTION__);
   cancelled = true;
  }
  
  if(cancelled)
   return ENUM_LEVELS_CANCELLED; 
  
  if(!ready_to_open && Bid >= target_price) {
   levelslogger.log("ready to open [BUY], lot=" + (string)lots + ", when Bid <= " + (string)open_price, LOG_INFO, __LINE__, __FUNCTION__);
   ready_to_open = true;
  }
   
  if(ready_to_open && Bid <= open_price)
  {
   ticket = OrderSend(Symbol(), OP_BUY, lots, Ask, 5, 0, 0);
   
   if(ticket > 0) {
    ready_to_open = false;
    return ENUM_LEVELS_NEW_TICKET_OPENED;
   }
  }
 }
 else if(lots < 0)
 {
  lots = MathAbs(lots);
 
  if(lower_cancel_price && Bid <= lower_cancel_price) {
   levelslogger.log("Cancelling open [SELL] attempt for bid=" + (string)Bid + " <= lower_cancel_price=" + (string)lower_cancel_price, LOG_INFO, __LINE__, __FUNCTION__);
   cancelled = true;
  }
  
  if(upper_cancel_price && Bid >= upper_cancel_price) {
   levelslogger.log("Cancelling open [SELL] attempt for bid=" + (string)Bid + " <= upper_cancel_price=" + (string)upper_cancel_price, LOG_INFO, __LINE__, __FUNCTION__);
   cancelled = true;
  }
  
  if(cancelled)
   return ENUM_LEVELS_CANCELLED;
  
  if(Bid <= target_price) {
   levelslogger.log("ready to open [SELL], lot=" + (string)lots + ", when Bid >= " + (string)open_price, LOG_INFO, __LINE__, __FUNCTION__);
   ready_to_open = true;
  }
   
  if(ready_to_open && Bid >= open_price)
  {
   ticket = OrderSend(Symbol(), OP_SELL, lots, Bid, 5, 0, 0);
   
   if(ticket > 0) {
    ready_to_open = false;
    return ENUM_LEVELS_NEW_TICKET_OPENED;
   }
  }
 }
 else {
  levelslogger.log("Invalid lot size = 0", LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
  return ENUM_LEVELS_CANCELLED;
 }
 
 return ENUM_LEVELS_WAITING_TO_OPEN;
}

//+------------------------------------------------------------------+
void CreateBuy(double prc, long chart_id=0)
{
  prc = NormalizeDouble(prc, Digits);
  string tag = "LEVELS-BUY-MAX_ENTRY_COUNT";
   
  if(GlobalVariableCheck(tag))
  {
   long max_entries = (long)GlobalVariableGet(tag);
   Sleep(300); // Avoids MT4 internal error bug9
   bool result = EventChartCustom(chart_id, CHARTEVENT_CREATE_LEVEL_BUY, max_entries, prc, NULL);
   if(!result)
    Alert("Error sending chart event, e=" + (string)GetLastError());
  }
  else
  {
   GlobalVariableSet(tag, 0);
   MessageBox("Please set global variable:\n  " + tag, "Error", MB_OK | MB_ICONHAND); 
  }
}

//+------------------------------------------------------------------+
void CreateSell(double prc, long chart_id=0)
{
  prc = NormalizeDouble(prc, Digits);
  string tag = "LEVELS-SELL-MAX_ENTRY_COUNT";
   
  if(GlobalVariableCheck(tag))
  {
   long max_entries = (long)GlobalVariableGet(tag);
   Sleep(300);
   bool result = EventChartCustom(chart_id, CHARTEVENT_CREATE_LEVEL_SELL, max_entries, prc, NULL);
   if(!result)
    Alert("Error sending chart event, e=" + (string)GetLastError());
  }
  else
  {
   GlobalVariableSet(tag, 0);
   MessageBox("Please set global variable:\n  " + tag, "Error", MB_OK | MB_ICONHAND);
  }
}

//+------------------------------------------------------------------+
void CreateBuySL(double prc, long chart_id=0)
{
 prc = NormalizeDouble(prc, Digits);
 
 int total = ObjectsTotal(chart_id);
 
 double closest_prc=-1;
   int closest_level=-1;

   for(int i=0; i<total; i++)
   {
    string name = ObjectName(chart_id, i);
    string result[];

    int size = StringParse(name, LEVELS_BUY_PREFIX, '-', result);

    if(size==3) 
    {
    
     string program_name = StringTrim(result[0]);
     string side = StringTrim(result[1]);
     int level = (int)result[2];
    
     if(program_name=="Levels")
     {
      if(side=="Buy")
      {
       double obj_prc = ObjectGetDouble(chart_id, name, OBJPROP_PRICE1);
    
       double distance = obj_prc - prc;

       if(distance > 0 &&
          distance < MathAbs(prc - closest_prc))
       {
        closest_prc = obj_prc;
        closest_level = level;
       }
      }
     }
    }
   }
   
   if(closest_level >= 0) {
    Sleep(300);
    bool result = EventChartCustom(chart_id, CHARTEVENT_CREATE_LEVEL_BUY_SL, closest_level, prc, NULL);
    if(!result)
     Alert("Error sending chart event, e=" + (string)GetLastError());
   }
}

void CreateSellSL(double prc, long chart_id=0)
{
   prc = NormalizeDouble(prc, Digits);
   int total = ObjectsTotal(chart_id);

   double closest_prc=-1;
   int closest_level=-1;

   for(int i=0; i<total; i++)
   {
    string name = ObjectName(chart_id, i);
    string result[];
    
    int size = StringParse(name, LEVELS_SELL_PREFIX, '-', result);
   
    if(size==3) 
    {
     string program_name = StringTrim(result[0]);
     string side = StringTrim(result[1]);
     int level = (int)result[2];
     
     if(program_name=="Levels")
     {
      if(side=="Sell")
      {
       double obj_prc = ObjectGetDouble(chart_id, name, OBJPROP_PRICE1);
       double distance = prc - obj_prc;
       
       if(distance > 0 &&
          distance < MathAbs(prc - closest_prc))
       {
        closest_prc = obj_prc;
        closest_level = level;
       }
      }
     }
    }
   }
   
   if(closest_level >= 0) {
    Sleep(300);
    bool result = EventChartCustom(chart_id, CHARTEVENT_CREATE_LEVEL_SELL_SL, closest_level, prc, NULL);
    if(!result)
     Alert("Error sending chart event, e=" + (string)GetLastError());
   }
}