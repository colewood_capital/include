//+------------------------------------------------------------------+
//|                                                   Intrascalp.mqh |
//|                                       Copyright 2016, Jamal Cole |
//+------------------------------------------------------------------+
#property copyright "jcolecorp"
#property strict

#include <Packages\Config.mqh>

EXTERN(int, NO_OF_LEVELS);
EXTERN(double, PERC_RISK);
EXTERN(string, WEIGHT);

double weight[];
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
ENUM_INIT_RETCODE Init()
  {
//---
 EventSetTimer(60);
 
 EXPERT_INIT(int, NO_OF_LEVELS);
 EXPERT_INIT(double, PERC_RISK);
 EXPERT_INIT(string, WEIGHT);
 
 //--- parse weight
  string result[];
  ArrayResize(weight, NO_OF_LEVELS);
  StringSplit(WEIGHT, ',', result);

  for(int i=0; i<NO_OF_LEVELS; i++)
   weight[i] = (double)result[i];

 return(INIT_SUCCEEDED);
  }

//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void Deinit(const int reason)
  {
//---


  }

//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void Tick()
  {
//---

  }

//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
void Timer()
  {
//---

  }

//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void ChartEvent(const int id,
                const long &lparam,
                const double &dparam,
                const string &sparam)
  {

  }
  
void CalculateOpenPrice(int order_type, double first_price, double last_price, double sl, const double& IN_weights[], double& OUT_prices[], double& OUT_lots[])
{
 double allowed_risk = AccountEquity() * PERC_RISK;
 double interval = MathAbs(first_price - last_price) / (NO_OF_LEVELS - 1);
 double pip_risk = 0;
 double tv = MarketInfo(NULL, MODE_TICKVALUE);
 double actual_risk = 0;
 double INVERSE_POINT = 1 / Point;
 
 if(order_type==OP_BUY)
 {
  for(int i=0; i<NO_OF_LEVELS; i++)
  {
   OUT_prices[i] = first_price - (interval*i);
   double pips = (first_price - (interval*i)) - sl;
   pips *= IN_weights[i];
   actual_risk += pips * (INVERSE_POINT) * tv;
  }
 }
 
 if(order_type==OP_SELL)
 {
  for(int i=0; i<NO_OF_LEVELS; i++)
  {
   OUT_prices[i] = last_price + (interval*i);
   double pips = sl - (last_price + (interval*i));
   pips *= IN_weights[i];
   actual_risk += pips * (INVERSE_POINT) * tv;
  }
 }
 
 double alpha = NormalizeDouble(allowed_risk / actual_risk, 2);

 for(int i=0; i<NO_OF_LEVELS; i++)
 { 
  OUT_lots[i] = alpha * IN_weights[i];
 }
}