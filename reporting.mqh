//+------------------------------------------------------------------+
//|                                                    reporting.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#ifndef BASEDIR
#define BASEDIR
   extern string BASE_DIR = "";
   string DB_UPDATE_FOLDER_NAME = BASE_DIR + "\\queue";
#endif 

#ifndef UTILS
#define UTILS
   #include <utils.mqh>
#endif

#ifndef REPORTING
#define REPORTING
   #define CUSTOM_CHART_ID   102 
#endif

//+------------------------------------------------------------------+
bool ReportSiteOpen(int booknum, 
                    int parent_booknum=0, 
                    int parent_login=0, 
                    string parent_server="",
                    string position="",
                    double open_requested=0, 
                    uint open_latency=0, 
                    double open_spread_before=0, 
                    double open_spread_after=0)
{   
  string filename = DB_UPDATE_FOLDER_NAME + "\\" + (string)booknum + "_update" + ".csv";
     
  int handle = FileOpen(filename, FILE_WRITE|FILE_CSV);
  
  if(handle >= 0)
  {   
   //--- Log trade data
     FileWrite(handle,"booknum=" + (string)booknum);
     FileWrite(handle,"acctNumber=" + (string)AccountNumber());
     FileWrite(handle,"acctServer=" + GetServerName());
     FileWrite(handle,"position="+position);
     
    if(parent_booknum > 0 && parent_login > 0 && StringLen(parent_server) > 0)
    {
     FileWrite(handle,"parent_booknum="+(string)parent_booknum);
     FileWrite(handle,"parent_login="+(string)parent_login);
     FileWrite(handle,"parent_server="+(string)parent_server);
    }
      
    if(open_requested!=0)
     FileWrite(handle,"open_requested="+(string)open_requested);
     
    if(open_latency!=0)
     FileWrite(handle,"open_latency="+(string)open_latency);
     
    if(open_spread_before!=0)
     FileWrite(handle,"open_spread_before="+(string)open_spread_before);
     
    if(open_spread_after!=0)
     FileWrite(handle,"open_spread_after="+(string)open_spread_after);
     
    FileWrite(handle,"upload=TRUE");
   
   //--- Close File
    FileClose(handle);
     
    return(true);
  }
  
 Alert("Error, e: ",GetLastError()," in ",__FUNCSIG__);
 ResetLastError();
 return(false);
}
//+------------------------------------------------------------------+
bool ReportSiteCloseBy(int booknum, int opposite_booknum)
 {   
   string filename = DB_UPDATE_FOLDER_NAME + "\\" + (string)booknum + "_closebyupdate" + ".csv";
     
   int handle = FileOpen(filename, FILE_WRITE|FILE_CSV);
  
   if(handle >= 0)
   {   
    //--- Log trade data
     FileWrite(handle,"booknum="+(string)booknum);
     FileWrite(handle,"opposite_booknum="+(string)opposite_booknum);
     FileWrite(handle,"upload=TRUE");
   
    //--- Close File
     FileClose(handle);

    //--- Send Chart Event to tell 'site_reporting.ex4' to search for file and update DB
     long id=ChartFirst();
     while(id>=0)
      {  
       EventChartCustom(id, CUSTOM_CHART_ID, 0, 0, "");
       id = ChartNext(id);
      }
     
     return(true);
   }
  
  Alert("Error, e: ",GetLastError()," in ",__FUNCSIG__);
  ResetLastError();
  return(false);
 }
//+------------------------------------------------------------------+
bool ReportSiteClose(int booknum, double close_requested=0, uint close_latency=0, double close_spread_before=0, 
                     double close_spread_after=0, string reason="")
 {   
   string filename = DB_UPDATE_FOLDER_NAME + "\\" + (string)booknum + "_update" + ".csv";
     
   int handle = FileOpen(filename, FILE_READ|FILE_WRITE|FILE_CSV);
  
   if(handle >= 0)
   {   
    FileSeek(handle, 0, SEEK_END);
   
    //--- Log trade data
     FileWrite(handle,"booknum=" + (string)booknum);
     FileWrite(handle,"acctNumber=" + (string)AccountNumber());
     FileWrite(handle,"acctServer=" + GetServerName());
     
     if(close_requested!=0)
      FileWrite(handle,"close_requested="+(string)close_requested);
      
     if(close_latency!=0)
      FileWrite(handle,"close_latency="+(string)close_latency);
     
     if(close_spread_before!=0)
      FileWrite(handle,"close_spread_before="+(string)close_spread_before);
     
     if(close_spread_after!=0)
      FileWrite(handle,"close_spread_after="+(string)close_spread_after);
     
     if(StringLen(reason) > 0)
      FileWrite(handle,"close_reason="+reason);
      
     FileWrite(handle,"upload=TRUE");
   
    //--- Close File
     FileClose(handle);

    //--- Send Chart Event to tell 'site_reporting.ex4' to search for file and update DB
     long id=ChartFirst();

     while(id>=0)
      {  
       EventChartCustom(id, CUSTOM_CHART_ID, 0, 0, "");
       id = ChartNext(id);
      }
     
     return(true);
   }
  
  Alert("Error, e: ",GetLastError()," in ",__FUNCSIG__);
  ResetLastError();
  return(false);
 }