//+------------------------------------------------------------------+
//|                                                       Config.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#ifndef ARRAYSTRING
#define ARRAYSTRING
   #include <Arrays\ArrayString.mqh>
#endif 

#ifndef ARRAYLIST
#define ARRAYLIST
   #include <Arrays\ArrayList.mqh>
#endif 

#ifndef CONFIGUTILS
#define CONFIGUTILS
   #include <Packages\ConfigUtils.mqh>
#endif 

#ifndef STRINGS
#define STRINGS
   #include <Packages\Strings.mqh>
#endif 

#define LOG_SIGNATURE      __FILE__ + " (" + (string)__LINE__ + ")::"

//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
bool ___init_result;

#define EXTERN(type, name)                   extern type name=NULL;
#define INIT(type, name, RET_TYPE)           if(name==NULL || (string)name=="") { \
                                               name = (type)___ConfigFileLoader(#name, ___init_result); \
                                               if(!___init_result) { Print(LOG_SIGNATURE, "Calling ExpertRemove()");  ExpertRemove();  return RET_TYPE; } } 
#define INIT_BOOL(name, RET_TYPE)              name = StringToBool(___ConfigFileLoader(#name, ___init_result)); \
                                               if(!___init_result) { Print(LOG_SIGNATURE, "Calling ExpertRemove()");  ExpertRemove();  return RET_TYPE; }
#define SCRIPT_INIT(type, name)              INIT(type, name, )
#define CLASS_INIT(type, name)               INIT(type, name, )
#define EXPERT_INIT(type, name)              INIT(type, name, INIT_PARAMETERS_INCORRECT)
#define SCRIPT_INIT_BOOL(name)               INIT_BOOL(name, )
#define CLASS_INIT_BOOL(name)                INIT_BOOL(name, )
#define EXPERT_INIT_BOOL(name)               INIT_BOOL(name, INIT_PARAMETERS_INCORRECT)
//+------------------------------------------------------------------+
string ___ConfigFileLoader(string variable_name, bool& result)
/*
Delimiter set to '='
For each variable = value pair, white space is stripped
*/
{
 result = true;
 const string CONFIG_FILE_PATH = "Config\\" + WindowExpertName() + ".txt";
 
 if(!FileIsExist(CONFIG_FILE_PATH)) {
  int handle = FileOpen(CONFIG_FILE_PATH, FILE_TXT|FILE_WRITE);
  FileClose(handle);
 }
 
 int handle = FileOpen(CONFIG_FILE_PATH, FILE_TXT|FILE_READ);
 string value = NULL;
 bool b_found_var=false;
 
 if(handle >=0)
 {
  Print(LOG_SIGNATURE, "Searching for ", variable_name, " in " + CONFIG_FILE_PATH);
  
  while(!FileIsEnding(handle))
  {
   string arr[];
   string line = FileReadString(handle);
    
   //Comment line (symbol)
   if(StringFind(line,"#") == 0)
     continue;
    
   StringSplit(line,'=',arr);
   if(ArraySize(arr) == 2)
   {
    arr[0]=StringTrimLeft(StringTrimRight(arr[0]));
    arr[1]=StringTrimLeft(StringTrimRight(arr[1])); 
    
    if(arr[0]==variable_name) {
     Print("input: " + variable_name + "=" + (string)(arr[1]));
     value = arr[1];
     b_found_var=true;
     break;
    }
   }
  }
  
  if(!b_found_var) {
    Print(LOG_SIGNATURE, "[WARNING] could not find " + variable_name + " in " + CONFIG_FILE_PATH);
  }
    
  FileClose(handle);
 }
 else {
  Print(LOG_SIGNATURE, "Could not open " + CONFIG_FILE_PATH + ", e=" + (string)GetLastError());
  result = false;
 }
 
 return value;
}
//+------------------------------------------------------------------+
int _ConfigParseFile(string filename, string& buffer_variables[], string& buffer_values[])
{
 int ret_code;
 const int ATTEMPTS=5;
 int k=0;
 
 do
 {
  int handle = FileOpen(filename,FILE_TXT|FILE_READ|FILE_SHARE_READ);

  if(handle >=0)
  {
   int size=-1;
   
   while(!FileIsEnding(handle))
   {
    string result[];
    string line = FileReadString(handle);
    
    //Skip comment line
    if(StringFind(line,"#") == 0)
      continue;
    
    StringSplit(line,'=',result);
    
    if(ArraySize(result) == 1)
    {
     size = ArraySize(buffer_variables);
     StringReplace(result[0]," ","");
     ArrayResize(buffer_variables,size+1);
     ArrayResize(buffer_values,size+1);
     buffer_variables[size] = "";
     buffer_values[size] = result[0];
    }
    
    if(ArraySize(result) == 2)
    {
     size = ArraySize(buffer_variables);
     StringReplace(result[0]," ","");
     StringReplace(result[1]," ","");
     ArrayResize(buffer_variables,size+1);
     ArrayResize(buffer_values,size+1);
     buffer_variables[size] = result[0];
     buffer_values[size] = result[1];
    }
   }
   
   
   FileClose(handle);
   ret_code = IDOK;
   return ++size;
  }
  
   k++;
   Sleep(250);
    
 }  while(k < ATTEMPTS);
  
 Print(LOG_SIGNATURE, "Could not load " + filename + ", e=" + ErrorDescription(GetLastError()));
 ExpertRemove();
 
 return 0;
}
//+------------------------------------------------------------------+
void ConfigCSVFileLoader(string filename, CArrayList<CArrayString> &csv)
{
 int handle=FileOpen(filename, FILE_READ|FILE_CSV, ',');
 int index;
 
 if(handle>=0)
 {
  bool b_new_line=true;
  CArrayString *row=NULL;
  
  while(!FileIsEnding(handle))
  {
   string val=StringTrim(FileReadString(handle));
   
   if(StringLen(val) > 0)
   {
    if(b_new_line)
    {
     row = csv.Add(index);
     b_new_line=false;
    }
   
    row.Add(val);
    
    if(FileIsLineEnding(handle))
     b_new_line=true;
    }
  }
  
  FileClose(handle);
 }
 else {
   Print(LOG_SIGNATURE, "Could not load " + filename);
   ExpertRemove();
  }
}
//+------------------------------------------------------------------+
void ConfigFileLoader(string filename, CArrayString& values, 
                       const string& expected[], string& not_found[], bool warn_only=False)
/*
Delimiter set to '='
For each variable = value pair, white space is stripped
Returned array values[] is of equal size ORDERED BY expected[]
Any variable not in expected[] will be excluded, regardless if if found in config file, unless warn_only=True
Returned array not_found[] lists any variable found in expected[], but not found in the config file
*/
{
 
 string buffer_variables[];
 string buffer_values[];
 
 int size=_ConfigParseFile(filename, buffer_variables, buffer_values);
 
 if(!warn_only)
 {
  CArrayString inspect;
  inspect.AddArray(expected);
  
  for(int i=0; i<size; i++)
  {
   int pos = inspect.SearchLinear(buffer_variables[i]);
 
   if(pos>=0)
   {
    values.Add(buffer_values[i]);
    inspect.Delete(pos);
   }
  }
  
  //--- get remaining variables not found in config
   int remaining = inspect.Total();
   for(int i=0; i<remaining; i++)
   {
    ArrayResize(not_found, ArraySize(not_found)+1);
    not_found[i] = inspect[i];
   }
 }
 else
 {
  for(int i=0; i<size; i++)
  {
   values.Add(buffer_values[i]);
  }
  
  //---
   ArrayResize(not_found, 0);
 }
 
 
}

bool StringToBool(string str)
{
 if(!StringToUpper(str))
  Print("StringToUpper() ", __FUNCTION__, "(", __LINE__, ") failed, str=" + str);
 
 return (str=="TRUE") ? true : false;
}