//+------------------------------------------------------------------+
//|                                                       Charts.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#ifndef ARRAYINT
#define ARRAYINT
   #include <Arrays/ArrayInt.mqh>
#endif 

#ifndef ARRAYSTRING
#define ARRAYSTRING
   #include <Arrays/ArrayString.mqh>
#endif 

#ifndef STRINGS
#define STRINGS
   #include <Packages/Strings.mqh>
#endif 

#ifndef MAP
#define MAP
   #include <Map.mqh>
#endif 

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
   Logger logger(__FILE__, "helper_processes");
#endif 

class Charts
{
private:
 static Logger logger;
 
public:
 static ENUM_TIMEFRAMES StringToTimeframe(string period);
 static void ArgsParser(string args, int count, CArrayString& symbol, CArrayInt& timeframe, CArrayString& chart_template);
 static bool ChartCloseSafe(long chart_id);
 static bool ChartExists(long chart_id);
};

static Logger Charts::logger(__FILE__, "helper_processes");

//+------------------------------------------------------------------+
static ENUM_TIMEFRAMES Charts::StringToTimeframe(string period)
{
 __TRACE__
 
 if(period==EnumToString(PERIOD_M1))            return PERIOD_M1;
 else if(period==EnumToString(PERIOD_M5))       return PERIOD_M5;
 else if(period==EnumToString(PERIOD_M15))      return PERIOD_M15;
 else if(period==EnumToString(PERIOD_M30))      return PERIOD_M30;
 else if(period==EnumToString(PERIOD_H1))       return PERIOD_H1;
 else if(period==EnumToString(PERIOD_H4))       return PERIOD_H4;
 else if(period==EnumToString(PERIOD_D1))       return PERIOD_D1;
 else if(period==EnumToString(PERIOD_W1))       return PERIOD_W1;
 else if(period==EnumToString(PERIOD_MN1))      return PERIOD_MN1;
 
 else
  LOG_HIGH("could not match period = " + period);
  
 return 0;
}
//+------------------------------------------------------------------+
int ChartsTotal()
{
 int counter=0;
 
 long chart_id=ChartFirst();
 
 do
 {
  counter++;
  chart_id=ChartNext(chart_id);
 } while(chart_id>=0);
 
 return counter;
}
//+------------------------------------------------------------------+
static bool Charts::ChartExists(long chart_id)
{
 __TRACE__
 
 LOG_DEBUG_RAW("searching chart #" + (string)chart_id + " ... ");
 
 if(chart_id==0) {
  LOG_DEBUG_RAW("chart_id==0, returning true");
  return true;
 }
  
 long _chart_id=ChartFirst();
 
 do
 {
  if(_chart_id==chart_id) { 
   LOG_DEBUG_RAW("found #" + (string)chart_id);
   return true; 
  }
   
  _chart_id=ChartNext(_chart_id);
 }
 while(_chart_id>=0);
 
 LOG_DEBUG_RAW("could not find chart #" + (string)chart_id);
 return false;
}
//+------------------------------------------------------------------+
static bool Charts::ChartCloseSafe(long chart_id=0)
{
 __TRACE__
 
 // protects against scenario where chart is closed before MT4 'fully' opens chart
 // (even though ChartOpen() returns a chart id). This causes unexpected behavior
 // hence, sleep in case chart has not yet opened
 // issue #2: sometimes ChartCloses causes a 'dead' chart to appear which needs to
 // be closed manually
 
 static Map<long,uint> id_closeAttempts;
 
 uint attempt=id_closeAttempts[chart_id];
 
 if(attempt==5)
  LOG_SYSTEM("[MT4_BUG] Unable to close #" + (string)chart_id + "(" + ChartSymbol(chart_id) + ", " + EnumToString((ENUM_TIMEFRAMES)ChartPeriod(chart_id)) + ") " + (string)attempt + " times. Known solution: close manually");
 
 if(attempt>=5)
  return true;    //return true since it is a known bug and to avoid throwing errors upstream
 
 attempt++;
 
 Sleep(100);
  
 if(ChartExists(chart_id)) 
 { 
  LOG_DEBUG_RAW("closing chart #" + (string)chart_id + ", attempt=" + (string)attempt);
  id_closeAttempts.set(chart_id, attempt);
  return ChartClose(chart_id);
 }
  
 return false;
}
//+------------------------------------------------------------------+
static void Charts::ArgsParser(string args, int count, CArrayString& symbol, CArrayInt& timeframe, CArrayString& chart_template)
{
 //should write unit tests: "EURUSD=rsi;USDJPY=hog;USDCAD;EURAUD=rsi"; -> fail
 //"EURUSD=rsi;USDJPY=hog;USDCAD;EURAUD=rsi"; -> pass
 //test count not equal -> fail
 //test a parameter has more than two values -> fail
 //test pass invalid period -> fail
 __TRACE__

 string res[];
 symbol.Clear();
 chart_template.Clear();
 
 StringSplit(args, ';', res);
 
 int size = ArraySize(res);
 
 args = StringTrim(args);
 
 if(StringSubstr(args, StringLen(args)-1) == ";")
 {
  //--- strip terminating semi-colon
   ArrayResize(res, --size);
   args = StringSubstr(args, 0, StringLen(args)-1);
  
  if(size == count)
  {
   for(int i=0; i<size; i++)
   {
    string val[];
    StringSplit(res[i], '=', val);
    int val_size = ArraySize(val);
    
    LOG_DEBUG("Parsing res[" + (string)i + "] = " + res[i]);
   
    if(val_size == 2)
    {
     symbol.Add(val[0]);

     //--- Add timeframe
      ENUM_TIMEFRAMES tframe = Charts::StringToTimeframe(val[1]);
      
      if(tframe > 0)
       timeframe.Add(tframe);
      else 
      {
       symbol.Clear();
       chart_template.Clear();
       return;
      }
      
     chart_template.Add(NULL);
    }
    else if(val_size == 3)
    {
     symbol.Add(val[0]);
     
     //--- Add timeframe
      ENUM_TIMEFRAMES tframe = Charts::StringToTimeframe(val[1]);
      
      if(tframe > 0)
       timeframe.Add(tframe);
      else 
      {
       symbol.Clear();
       chart_template.Clear();
       return;
      }
     
     chart_template.Add(val[2]);
    }
    else {
     LOG_HIGH("Unable to parse: " + res[i] + ", should val_size should be 2 or 3");
     symbol.Clear();
     chart_template.Clear();
     return;
    }
   }
  }
  else {
   LOG_HIGH("Invalid args: " + args + " ... with size = " + (string)size + ", count = " + (string)count);
   return;
  }
 }
 else {
  LOG_HIGH("args must terminate with semi-colon, args: " + args);
  return;
 }
  
 return;
}
//+------------------------------------------------------------------+
void ChartsCreate(const CArrayString& symbol, const CArrayInt& timeframe, const CArrayString& chart_template)
{
 __TRACE__
 
 int size = symbol.Total();
 
 for(int i=0; i<size; i++)
 {
  long chart_id = ChartOpen(symbol[i], timeframe[i]);
  ChartApplyTemplate(chart_id, chart_template[i]);
 }
}
//+------------------------------------------------------------------+
void ChartsCloseAll()
{
 __TRACE__
 
 long chart_id = ChartFirst();
 
 do {
   ChartClose(chart_id);
   chart_id = ChartNext(chart_id);
  } 
  while(chart_id >= 0);
}
//+------------------------------------------------------------------+
void ChartsCloseAll(string symbol, int& count)
{
 long chart_id = ChartFirst();
 count = 0;
 
 do {
   if(ChartSymbol(chart_id)==symbol) {
    ChartClose(chart_id);
    count++;
   }
    
   chart_id = ChartNext(chart_id);
  } 
  while(chart_id >= 0);
}
//+----------------------------------------------------------------------+
//| Send command to the terminal to display the chart above all others.  |
//| Only works on next chart tick :(                                     |
//+----------------------------------------------------------------------+
bool ChartBringToTop(const long chart_ID=0)
  {
//--- reset the error value
   ResetLastError();
//--- show the chart on top of all others
   if(!ChartSetInteger(chart_ID,CHART_BRING_TO_TOP,0,true))
     {
      //--- display the error message in Experts journal
      Print(__FUNCTION__+", Error Code = ",GetLastError());
      return(false);
     }
//--- successful execution
   return(true);
  }
