//+------------------------------------------------------------------+
//|                                                     Terminal.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#define EXPERT_REMOVE(SPARAM) { string result[]; \
    StringSplit(SPARAM, ';', result); \
    string ea_name = (ArraySize(result) >=1 && result[0] != "NULL") ? result[0]: NULL; \
    string symbol = (ArraySize(result) >= 2) ? result[1] : NULL; \
    ExpertRemove(ea_name, __FILE__, symbol); }

void ExpertRemove(string ea_name, string __FILE__macro, string symbol=NULL)
{
 string res[];
 StringSplit(__FILE__macro, '.', res);
 
 Alert(__FILE__);
 if(ea_name==NULL || ea_name==__FILE__macro || ea_name==res[0])
 {
  if(symbol==NULL || symbol==Symbol())
   ExpertRemove(); 
 }
}