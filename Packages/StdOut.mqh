//+------------------------------------------------------------------+
//|                                                        Files.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#include <Packages/Config.mqh>
#include <Super/Enums.mqh>

#ifndef LOGGER
#define LOGGER
  #define LOGGER_DEPRECATED   //overloading not compatible with traceback obj, build LOGGER_NO_TRACEBACK macro
  #include <Logger.mqh>
#endif 

class StdOut
{
 private:
  string p_filepath;
  void write(const string msg);
  Logger logger;
 
 public:
  void operator<<(const string msg) { write(msg); }
  
  StdOut(string OUTPUT_TYPE);
  ~StdOut()  { write(WindowExpertName() + " " + STD_END_MSG); }
};

StdOut::StdOut(string OUTPUT_TYPE) : logger(__FILE__, "helper_processes")
{
 p_filepath = "StdOut\\" + WindowExpertName() + "__" + OUTPUT_TYPE + ".txt";

 int handle = FileOpen(p_filepath, FILE_WRITE);

 if(handle < 0)
  logger.log("Unable to open: " + p_filepath + ", e=" + (string)GetLastError(), LOG_TYPE_WARNING, __LINE__, __FUNCTION__);
 else 
  FileWrite(handle, WindowExpertName() + " initialized");
  
 FileClose(handle);
}

void StdOut::write(const string msg)
{
 int handle = FileOpen(p_filepath, FILE_WRITE|FILE_READ);
 
 if(handle < 0)
  logger.log("Unable to open: " + p_filepath + ", e=" + (string)GetLastError(), LOG_TYPE_WARNING, __LINE__, __FUNCTION__);
 else
 { 
  FileSeek(handle, 0, SEEK_END);
  FileWrite(handle, msg);
  FileClose(handle);
 }
}