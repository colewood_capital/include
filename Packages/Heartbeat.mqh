//+------------------------------------------------------------------+
//|                                                    Heartbeat.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

enum ENUM_HEARTBEAT_MODE { MODE_CLIENT, MODE_SERVER, MODE_PRICE_FEED };

#ifndef SUPERVISOR
#define SUPERVISOR
   #include <Packages\Supervisor.mqh>
#endif 

#ifndef HEADERS
#define HEADERS
   #include <Super\Headers.mqh>
#endif 

#ifndef UTILS2
#define UTILS2
   #include <Utils2.mqh>
#endif

#define __GLB_VAR_NAME_PREFIX                "HEARTBEAT_"
#define __GLB_VAR_NAME_ID_PREFIX             __GLB_VAR_NAME_PREFIX + (string)ChartID()
#define __GLB_VAR_NAME                       __GLB_VAR_NAME_ID_PREFIX + "_" + GetStatusCode()
#define HEARTBEAT_SERVER_ID_FILENAME         "heartbeat_server_id.txt"

class Heartbeat
{
private:
 Logger logger;
 
protected:
 ENUM_HEARTBEAT_MODE p_mode;
 uint p_min_sec_update;
 int server_engine(long &chart_id[], string &status_code[], datetime &last_update[]);
 string GetStatusCode();
 string p_symbol;
 double p_last_bid;
 double p_last_ask;
 datetime p_last_symbol_timestamp;
 int p_server_offset;
 long p_server_chart_id;
 
public:
 Heartbeat(ENUM_HEARTBEAT_MODE mode, uint server_mode_min_update_in_secs, string symbol, int server_offset);
 void client_timer_engine(TerminalComment &cmt);
 int server_timer_engine(TerminalComment &cmt, long &chart_id_no_update[], string &no_heartbeat_status_code[], uint &time_elapsed_no_last_update[]);
 bool price_feed_timer_engine(datetime &last_update_time, double &last_recorded_bid, double &last_recorded_ask, uint &elapsed_secs);
 int get_last_update(long &chart_id[], string &status_code[], datetime &last_update[]);
 void deinit_engine(const int reason);
 string symbol() { return p_symbol; }
 void chart_event_engine(Supervisor &chart,
                  const long &lparam,
                  const double &dparam,
                  const string &sparam);
 uint min_sec_update() { return p_min_sec_update; }
};


bool Heartbeat::price_feed_timer_engine(datetime &last_update_time, double &last_recorded_bid, double &last_recorded_ask, uint &elapsed_secs)
{
 __TRACE__
 
 datetime now=TimeLocal() + p_server_offset;
 MqlDateTime server_time;
 TimeToStruct(now, server_time);
 MqlTick tick;
 SymbolInfoTick(p_symbol, tick);
 
 datetime from,to;
 
 if(!SymbolInfoSessionQuote(p_symbol, server_time.day_of_week, 0, from, to))  {
  LOG_DEBUG("Quote session not set on server for symbol=" + p_symbol + ", server_time.day_of_week=" + EnumToString((ENUM_DAY_OF_WEEK)server_time.day_of_week)); 
  return false;
 }
 
 MqlDateTime begin,end;
 TimeToStruct(from, begin);
 TimeToStruct(to, end);
 
 if(server_time.hour > begin.hour || (server_time.hour==begin.hour && server_time.min >= begin.min))
 {
  if((end.hour==0 && end.min==0) || server_time.hour < end.hour || (server_time.hour==end.hour && server_time.min <= end.min))
  {
   datetime time_local=TimeLocal();
   
   if(tick.bid != p_last_bid || tick.ask != p_last_ask)
   {
    p_last_symbol_timestamp=time_local;
    p_last_bid = tick.bid;
    p_last_ask = tick.ask;
    LOG_EXTENSIVE("Updating " + p_symbol + " last time (" + TimeToStr(p_last_symbol_timestamp,TIME_DATE|TIME_MINUTES|TIME_SECONDS) + ")");
   } 

   elapsed_secs = (int)(time_local - p_last_symbol_timestamp);
   
   if(elapsed_secs >= p_min_sec_update) {
    last_update_time=p_last_symbol_timestamp;
    last_recorded_bid = p_last_bid;
    last_recorded_ask = p_last_ask;
    return true;
   }
  }
  else
   LOG_DEBUG("Ignoring " + p_symbol + " price feed for current_time(" + TimeToStr(now) + ") > end_time(" + TimeToStr(to) + ")");
 }
 else
  LOG_DEBUG("Ignoring " + p_symbol + " price feed for current_time(" + TimeToStr(now) + ") < start_time(" + TimeToStr(from) + ")");
 
 return false;
}


void Heartbeat::Heartbeat(ENUM_HEARTBEAT_MODE mode=MODE_CLIENT, uint min_update_in_secs=10, string symbol=NULL, int server_offset=0)
  : logger(__FILE__, "helper_processes"), p_mode(mode), p_min_sec_update(min_update_in_secs), p_symbol(symbol), p_server_offset(server_offset)
{
 __TRACE__
 
 if(symbol!=NULL) {
  p_last_bid=MarketInfo(symbol,MODE_BID);
  p_last_ask=MarketInfo(symbol,MODE_ASK);
  p_last_symbol_timestamp=TimeLocal();
 } 
 else {
  p_last_bid=0;
  p_last_ask=0;
  p_last_symbol_timestamp=0;
 }
 
 if(mode==MODE_CLIENT) {
  GlobalVariableSet(GLB_CONFIG_INIT_STATUS + "_" + (string)ChartID(), 1.0);
  
  int handle=FileOpen(HEARTBEAT_SERVER_ID_FILENAME, FILE_READ|FILE_SHARE_READ|FILE_TXT);
  if(handle>=0)
  {
   p_server_chart_id = (long)FileReadString(handle);
   FileClose(handle);
  }
  else
   LOG_FILE_OPENING_ERROR(HEARTBEAT_SERVER_ID_FILENAME);
  
  if(p_server_chart_id <= 0)
   LOG_DEV_WARNING("p_server_chart_id (" + (string)p_server_chart_id + ") <= 0");
   
  LOG_DEBUG_1(p_server_chart_id);
 }
  
 if(mode==MODE_SERVER)
 {
  int handle=FileOpen(HEARTBEAT_SERVER_ID_FILENAME, FILE_WRITE|FILE_TXT);
  if(handle>=0)
  {
   FileWrite(handle, ChartID());
   FileClose(handle);
  }
 }
}

void Heartbeat::deinit_engine(const int reason)
{
 if(p_mode==MODE_CLIENT)
 {
  bool remove=false;
  
  switch(reason)
  {
   case REASON_REMOVE:
    remove=true;
    break;
    
   case REASON_CHARTCLOSE:
    remove=true;
    break;
  }
 } 
 
 GlobalVariableDel(GLB_CONFIG_INIT_STATUS + "_" + (string)ChartID());
}


int Heartbeat::get_last_update(long &chart_id[],string &status_code[],datetime &last_update[])
{
 return server_engine(chart_id, status_code, last_update);
}

void Heartbeat::chart_event_engine(Supervisor &chart,
                  const long &lparam,
                  const double &dparam,
                  const string &sparam)
{
 __TRACE__
 
 long chart_id = lparam;
 string ea_name,status_code;
 string res[];
 
 int size=StringSplit(sparam, ';', res);
 
 if(size==2)
 {
  ea_name = res[0];
  status_code = res[1];
 }
 else {
  LOG_HIGH("error parsing sparam=" + sparam);
  return;
 }
 
 string chart_symbol=ChartSymbol(chart_id);
 string chart_period=EnumToString(ChartPeriod(chart_id));

 if((int)CharToStr((uchar)StringGetChar(status_code, 0))==0)
  LOG_SYSTEM(ea_name + ".ex4" + " trading not allowed (" + chart_symbol + ", " + chart_period + ", chart_id=" + (string)chart_id + ")");
  
 if((int)CharToStr((uchar)StringGetChar(status_code, 1))==0)
  LOG_SYSTEM(ea_name + ".ex4" + " experts not allowed (" + chart_symbol + ", " + chart_period + ", chart_id=" + (string)chart_id + ")");
      
 if((int)CharToStr((uchar)StringGetChar(status_code, 2))==0)
  LOG_SYSTEM(ea_name + ".ex4" + " dll's not allowed (" + chart_symbol + ", " + chart_period + ", chart_id=" + (string)chart_id + ")");
      
 if((int)CharToStr((uchar)StringGetChar(status_code, 3))==0)
  LOG_SYSTEM(ea_name + ".ex4" + " expert libraries not allowed (" + chart_symbol + ", " + chart_period + ", chart_id=" + (string)chart_id + ")");
 
 LOG_EXTENSIVE("updating heartbeat for chart_id=" + (string)chart_id);
 chart.update_heartbeat();
}


int Heartbeat::server_engine(long &chart_id[],string &status_code[],datetime &last_update[])
{
  __TRACE__
  
  int arr_sz=10;
  ArrayResize(chart_id, arr_sz);
  ArrayResize(status_code, arr_sz);
  ArrayResize(last_update, arr_sz);
  
  int total=GlobalVariablesTotal();
  int size=0;
   
  for(int i=0; i<total; i++)
  {
   string name=GlobalVariableName(i);
   string args[];
   
   StringSplit(name, '_', args);
   
   if(args[0]=="HEARTBEAT")
   {
    if(ArraySize(args)==3)
    {
     chart_id[size] = (long)args[1];
     status_code[size] = args[2];
     last_update[size] = (datetime)GlobalVariableGet(name);
    
     if(++size==arr_sz)
     {
      arr_sz+=10;
      ArrayResize(chart_id, arr_sz);
      ArrayResize(status_code, arr_sz);
      ArrayResize(last_update, arr_sz);
     }
    }
    else
     LOG_HIGH("Unable to parse heartbeat global variable " + name);
   }
  }

  return size;
}


void Heartbeat::client_timer_engine(TerminalComment& cmt)
{
 __TRACE__
 
 if(p_mode==MODE_CLIENT)
 {
  string msg=WindowExpertName() + ";" + GetStatusCode();
  LOG_EXTENSIVE("Sending heartbeat to chart_id=" + (string)p_server_chart_id);
  
  EventChartCustom(p_server_chart_id, CHARTEVENT_HEARTBEAT, ChartID(), 0, msg);
  cmt.set("Last Heartbeat", (string)TimeLocal());
 }
}


int Heartbeat::server_timer_engine(TerminalComment& cmt, long &no_heartbeat_chart_id[], string &no_heartbeat_status_code[], uint &time_elapsed_no_last_update[])
{
 int no_update_count=0;
 
 if(p_mode==MODE_SERVER)
 {
  long chart_id[];
  datetime last_update[];
  string status_code[];
  
  int size=server_engine(chart_id, status_code, last_update);
  
  datetime current=TimeLocal();
  
  for(int i=0; i<size; i++)
  {
   uint time_elapsed=(int)(current - last_update[i]);

   if(time_elapsed >= p_min_sec_update || status_code[i] != "1111")
   {
    no_update_count++;
    ArrayResize(no_heartbeat_chart_id, no_update_count);
    ArrayResize(no_heartbeat_status_code, no_update_count);
    ArrayResize(time_elapsed_no_last_update, no_update_count);
    no_heartbeat_chart_id[no_update_count-1] = chart_id[i];
    no_heartbeat_status_code[no_update_count-1] = status_code[i];
    time_elapsed_no_last_update[no_update_count-1] = time_elapsed;
   }
  }
  
  cmt.set("Last Update", (string)TimeLocal());
 }
 
 return no_update_count;
}


string Heartbeat::GetStatusCode(void)
{
 string status_code = "";
 
 status_code += IsTradeAllowed() ? "1" : "0";
 status_code += IsExpertEnabled() ? "1" : "0";
 status_code += IsDllsAllowed() ? "1" : "0";
 status_code += IsLibrariesAllowed() ? "1" : "0";
 
 return status_code;
}
