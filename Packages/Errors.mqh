//+------------------------------------------------------------------+
//|                                                       Errors.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#define ERR_CODE(ID)           (ushort)(ERR_USER_ERROR_FIRST + ID)

#define _FILE_NOT_CREATED      1
#define ERR_FILE_NOT_CREATED   ERR_CODE(_FILE_NOT_CREATED)
