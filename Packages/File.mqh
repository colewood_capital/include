//+------------------------------------------------------------------+
//|                                                         File.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

class File
{
private:
 int p_handle;
 
public:
 static string filepath;
 static int delimiter;
 void write(string msg);
 File();
 ~File();
};

string File::filepath = NULL;
int File::delimiter = -1;


void File::write(string msg)
{
 FileWrite(p_handle, msg);
}


File::~File()
{
 if(p_handle >= 0)
  FileClose(p_handle);
}


File::File(void) : p_handle(-1)
{
 if(filepath == NULL)
 {
  Alert("Set File::filepath global variable");
  return;
 }
 
 if(delimiter == -1)
 {
  Alert("Set File::delimiter global variable");
  return;
 }

 p_handle = FileOpen(filepath, FILE_READ|FILE_WRITE, delimiter);
 FileSeek(p_handle, 0, SEEK_END);
}
