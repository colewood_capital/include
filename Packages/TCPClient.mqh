//+------------------------------------------------------------------+
//|                                                    TCPClient.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#include <Packages\DllUtils.mqh>

#ifndef LOGGER
#define LOGGER
   #define LOGGER_DEPRECATED
   #include <Logger.mqh>
#endif 

#import "TCPClientInterface.dll"
   void TCPClientInterfaceLoggerInit(string full_log_path);
   int TCPClientInitialize( char& host[],  char& port[]);
   int TCPCloseSendAllowRecv();
   int TCPClientClose();
   int TCPClientRecv(char& recv[]);
   int TCPClientSend(char& send[]);
#import

enum ENUM_TCP_INIT_RETCODE {
   TCP_INIT_SUCCESS = 0,
   TCP_INIT_FAILED 
};

class TCPClient
{
private:
 char p_host[];
 char p_port[];
 bool p_keep_alive;
 Logger logger;
 bool p_init();
 
public:
 bool reconnect();
 int send(string msg);
 
 TCPClient(string host, int port, bool keep_alive);
 ~TCPClient();
};

TCPClient::~TCPClient(void)
{
 if(p_keep_alive)
   TCPClientClose();
}

bool TCPClient::reconnect(void)
{
 return p_init();
}

bool TCPClient::p_init(void)
{
 int p_init_ret = TCP_INIT_FAILED;
 const int RETRY = 3;
 
 for(int i=0; i<RETRY; i++)
 {
  p_init_ret = TCPClientInitialize(p_host, p_port);

  if(p_init_ret == TCP_INIT_SUCCESS)
   break;
  else {
   Sleep(250);
   Alert("TCPClientInitialize() failed, host = " + CharArrayToString(p_host) + ", port = " + CharArrayToString(p_port));
  }
 }
 
 return p_init_ret == TCP_INIT_SUCCESS;
}

TCPClient::TCPClient(string host, int port, bool keep_alive=false)
 : logger(__FILE__, "helper_processes")
{
 DLL_LOGGER_INIT(TCPClientInterfaceLoggerInit, "TCPClientInterface");
   
 StringToCharArray(host, p_host);
 StringToCharArray((string)port, p_port);
 
 p_keep_alive = keep_alive;
 
 if(p_keep_alive)
  p_init();
}

int TCPClient::send(string msg)
{
 if(p_keep_alive)
 {
  char send[];
  StringToCharArray(msg, send);
  return TCPClientSend(send);
 }
 else
 {
  p_init();
  
  char send[];
  StringToCharArray(msg, send);
  bool res = TCPClientSend(send);
  
  TCPClientClose();
  
  if(!res)
   logger.log("Failed to send message, res=" + (string)res, LOG_TYPE_HIGH, __LINE__, __FUNCTION__,true);
  
  return res;
 }
}
