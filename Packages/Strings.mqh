//+------------------------------------------------------------------+
//|                                                      Strings.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
#endif

class Strings
{
private:
 static Logger logger;
 
public:
 static int StringParse(string str, string string_match, const ushort separator, string& result[]);
 static bool StringToBool(const string str);
 static string StringToUpper(string string_var);
};

static Logger Strings::logger(__FILE__, "helper_processes");

static bool Strings::StringToBool(string str)
{
 __TRACE__
 
 if(!::StringToUpper(str)) {
  LOG_HIGH_ERROR("StringToUpper() failed, str=" + str);
  return false;
 }
 
 return (str=="TRUE") ? true : false;
}

static string Strings::StringToUpper(string string_var)
{
  __TRACE__
  
  bool result=::StringToUpper(string_var);
  
  if(result)
   return string_var;
  else {
   LOG_HIGH_ERROR("Failed to convert string_var=" + string_var);
   return NULL;
  }
}


static int Strings::StringParse(string str, string string_match, const ushort separator, string& result[])
{
  __TRACE__
  
  int pos = StringFind(str, string_match);
  
  if(pos >= 0)
  {
   return StringSplit(StringSubstr(str, pos), separator, result);
  }
  else
   LOG_HIGH("Could not match string, str=" + str + ", string_match=" + string_match + ", separator=" + (string)separator);
  
  return 0;
}

string StringTrim(const string text)
{
 return StringTrimLeft(StringTrimRight(text));
}
