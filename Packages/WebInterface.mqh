//+------------------------------------------------------------------+
//|                                                        Email.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#ifndef OK_200
#define OK_200                        200
#endif

#ifndef OK_NOCONTENT                   
#define OK_NOCONTENT                  204
#endif 

#ifndef HTTP_424_FAILED_DEPENDENCY
#define HTTP_424_FAILED_DEPENDENCY    424
#endif 

#ifndef BASEDIR
#define BASEDIR
   extern string BASE_DIR;
#endif

#ifndef HEADERS
#define HEADERS
   #include <Super\Headers.mqh>
#endif 

#ifndef STRINGS
#define STRINGS
   #include <Packages\Strings.mqh>
#endif

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
#endif

#ifndef SUPERVISOR  
#define SUPERVISOR
   #include <Packages\Supervisor.mqh>
#endif 

#ifndef UTILS2
#define UTILS2
   #include <utils2.mqh>
#endif

struct TradeReport
{
 int account;
 int parent_account;
 int parent_booknum;
 int booknum;
 int opposite_booknum;
 string symbol;
 string type;
 string position;
 double lots;
 double sl;
 double tp;
 datetime open_time;
 double open_requested;
 double open_price;
 uint open_latency;
 double open_bid_before;
 double open_bid_after;
 double open_ask_before;
 double open_ask_after;
 double open_base_conversion;
 string open_reason;
 datetime close_time;
 string close_method;
 double close_requested;
 double close_price;
 uint close_latency;
 double close_bid_before;
 double close_bid_after;
 double close_ask_before;
 double close_ask_after;
 double close_base_conversion;
 string close_reason;
 double commission;
 double swaps;
 string comment_on_open;
 string comment_on_close;
 double profit;
};

int WebServer::WebRequest(const string method, const string URL_ROOT, string data, string &response, bool b_allow_resend=true)
{
  __TRACE__
  
  static CArrayString _data;
  static CArrayInt _attempt;
  static CArrayInt _halt;
  int attempt;
  
  //--- record number of attempts sending the same message
   int p1=_data.SearchLinear(data);
   if(p1>=0)
   {
    attempt=_attempt[p1];
    _attempt.Update(p1, ++attempt);
   }
   else
   {
    _data.Add(data);
    p1=_data.Total()-1;
    attempt=1;
    _attempt.Insert(attempt,p1);
    _halt.Insert(0,p1);
   }
   
  if(_halt[p1]==1)
   return OK_NOCONTENT;
  
  string headers;
  char content[], result[];
  bool allow_resend_request=true;
  
  data += "attempt=" + (string)attempt + ";";
 
  StringToCharArray(data, content, 0, WHOLE_ARRAY, CP_UTF8);
  
  string url = StringConcatenate(p_server_host, URL_ROOT);
  
  LOG_EXTENSIVE("send post to: " + url + ", data=" + data);
  
  int ret_code = WebRequest(method, url,
               "", NULL, 10000, content, ArraySize(content), result, headers);
               
  response = CharArrayToString(result);
  
  switch(ret_code)
  {
   case OK_200:
     LOG_DEBUG("server_ret_code: HTTP_200");
     _data.Delete(p1);
     _attempt.Delete(p1);
     _halt.Delete(p1);
     break;
   
   case HTTP_424_FAILED_DEPENDENCY:
   {
    LOG_DEBUG("[ABORT] update symbol set for already failed attempt");
    ret_code=OK_NOCONTENT;
   }
    
   default:
   {
    if(b_allow_resend)
    {
     LOG_DEBUG_1(b_allow_resend);
     
     string header[];
     StringSplit(headers,'\n',header);
     int header_sz=ArraySize(header);
     bool b_break=false;
     
     for(int i=0; i<header_sz; i++)
     {
      string text[];
      StringSplit(header[i], ':', text);
      if(ArraySize(text)==2)
      {
       if(text[0]=="halt_sending_msg")
       {
        b_break=true;
        
        _halt.Insert(1,p1);
        LOG_DEBUG("Halt sending message: " + data);
       }
       
       if(text[0]=="request_symbol_info")
       {
        b_break=true;
        
        LOG_DEBUG_1(header[i]);
        string symbol = StringTrimLeft(StringTrimRight(text[1]));
        if(SymbolSelect(symbol, true))
        {
         POSTDATA sym_data;
         string base_currency = SymbolInfoString(symbol, SYMBOL_CURRENCY_BASE);
         string profit_currency = SymbolInfoString(symbol, SYMBOL_CURRENCY_PROFIT);
         string tick_size = (string)SymbolInfoDouble(symbol, SYMBOL_TRADE_TICK_SIZE);
         string contract_size = (string)SymbolInfoDouble(symbol, SYMBOL_TRADE_CONTRACT_SIZE);
         
         sym_data.set("name", symbol);
         sym_data.set("base_currency", base_currency);
         sym_data.set("profit_currency", profit_currency);
         sym_data.set("tick_size", tick_size);
         sym_data.set("contract_size", contract_size);
        
         ret_code = WebRequestPOST(URL_SET_SYMBOL_SET, sym_data, false);
         
         if(ret_code != OK_200)
         {
          LOG_HIGH_NO_SITE("Failed to set symbolset");
          LOG_DEBUG_5(symbol, base_currency, profit_currency, tick_size, contract_size);
          allow_resend_request=false;
         }
        }
        else {
         LOG_HIGH("Cannot select symbol=" + symbol);
         Sleep(1000);
        }
       }
       
       if(text[0]=="allow_resend_request")
       {
        b_break=true;
        
        if(allow_resend_request)
        {
         LOG_DEBUG_1(header[i]);
         bool resend = Strings::StringToBool(StringTrimLeft(StringTrimRight(text[1])));
         if(resend)
         {
          ret_code = WebRequestPOST(URL_ROOT, data, false);
          LOG_DEBUG("resend_request=true");
         }
         else
          LOG_DEBUG("resend_request=false");
        }
       }
       else
        LOG_DEBUG("[ABORT] resend_request for allow_resend=false");
      }
   
      LOG_EXTENSIVE((string)i + "=" + header[i]);
     }
     
     if(ret_code >= 0) {
      LOG_DEBUG("server_ret_code: " + (string)ret_code);
     }
     else {
      LOG_HIGH_ERROR_NO_SITE("server_ret_code=" + (string)ret_code);
     }
     
     ret_code=OK_NOCONTENT;
     
     if(b_break)
      break;
    }
    
    //---- handle uncaught exceptions
     LOG_HIGH_NO_SITE("Post failed!!!");
   
     if(StringLen(response) > 0)
     {
      if(StringFind(response, "\n") < 0)
       LOG_HIGH_NO_SITE("[BAD RESPONSE]: " + response);
      
      string ts = (string)(int)TimeLocal();
      FolderCreate("Weblog");
      string err_filename = "Weblog\\" + WindowExpertName() + "\\" + ts + ".html";
      int handle = FileOpen(err_filename, FILE_WRITE|FILE_TXT);
   
      if(handle>=0) 
      {
       FileWrite(handle, response);
       LOG_HIGH_NO_SITE_UUID("Creating error webpage: " + err_filename);
       FileClose(handle);
      }
      else
       LOG_HIGH_ERROR_NO_SITE("Couldn't open file");
     }
     else
      LOG_HIGH_NO_SITE("No server response received @ " + url);
    
     ret_code=OK_NOCONTENT;
     break;
   }
  }
  
 return ret_code;
}


int WebServer::WebRequest(const string method, const string URL_ROOT, string data, bool b_allow_resend=true)
{
 string response;
 return WebRequest(method, URL_ROOT, data, response, b_allow_resend);
}

int WebServer::WebRequest(const string method, const string URL_ROOT, const POSTDATA &IN_data, bool b_allow_resend=true)
{
 string response;
 return WebRequest(method, URL_ROOT, IN_data, response, b_allow_resend);
}

int WebServer::WebRequest(const string method, const string URL_ROOT, const POSTDATA &IN_data, string &response, bool b_allow_resend=true)
{
 string data_str="";
 string k[], v[];
 
 int sz=IN_data.copy(k,v);
 for(int i=0; i<sz; i++)
  data_str += k[i]+"="+v[i]+";"; 
  
 return WebRequest(method, URL_ROOT, data_str, response, b_allow_resend);
} 


int WebServer::WebRequestPOST(const string URL_ROOT, string data, string &response, bool b_allow_resend=true)
{
  return WebServer::WebRequest("POST", URL_ROOT, data, response, b_allow_resend);
}

int WebServer::WebRequestPOST(const string URL_ROOT, string data, bool b_allow_resend=true)
{
 string response;
 return WebRequestPOST(URL_ROOT, data, response, b_allow_resend);
}

int WebServer::WebRequestPOST(const string URL_ROOT, const POSTDATA &IN_data, bool b_allow_resend=true)
{
 string response;
 return WebRequestPOST(URL_ROOT, IN_data, response, b_allow_resend);
}


int WebServer::WebRequestPOST(const string URL_ROOT, const POSTDATA &IN_data, string &response, bool b_allow_resend=true)
{
 string data_str="";
 string k[], v[];
 
 int sz=IN_data.copy(k,v);
 for(int i=0; i<sz; i++)
  data_str += k[i]+"="+v[i]+";"; 
  
 return WebRequestPOST(URL_ROOT, data_str, response, b_allow_resend);
}


void WebServer::login(string url_login)
{
  __TRACE__
  
  POSTDATA data;
  
  data.set("acctNumber", (string)p_ptr_creds.acct_num);
  data.set("acctServer", GetServerName());
  data.set("acctServerHost", p_ptr_creds.server_host);
  data.set("acctName", AccountName());
  data.set("acctType", AccountType());
  data.set("pcName", GetMachineName());
  data.set("baseDir", BASE_DIR);
  data.set("terminalPath", TerminalInfoString(TERMINAL_PATH));
  data.set("terminalDataPath", TerminalInfoString(TERMINAL_DATA_PATH));
  data.set("terminalCommonDataPath", TerminalInfoString(TERMINAL_COMMONDATA_PATH));
   
  string response;

  int ret_code=WebRequestPOST(url_login, data, response);
   
  if(ret_code!=OK_200) {
   LOG_DEBUG("Calling ExpertRemove()")
   ExpertRemove();
  }
  
  string message[];
  StringReplace(response, "\"", "");
  StringSplit(response, ';', message);
   
  if(ArraySize(message) > 0)
   LOG_DEBUG_1(message); 
  
  if(ArraySize(message) < 2)
  {
   LOG_HIGH("Did not recdeive initial response from server !!!");
   ExpertRemove();
  }
  else
  { 
   p_terminal_pk = (int)message[1];
   LOG_DEBUG(StringConcatenate("Acquired pk = ", p_terminal_pk));
  }
}


WebServer::WebServer(string host, const Credentials *creds, bool login=true) 
 : logger(__FILE__, "helper_processes"), p_ptr_creds(creds), p_terminal_pk(0)
{
 __TRACE__
 
 LOG_DEBUG("Connecting to site with host: " + (string)host);
 
 p_server_host = host;
 
 if(login)
  login(URL_LOGIN);
}

int WebServer::terminal_pk(void)
{
 return p_terminal_pk;
}

datetime WebServer::fromSQLTime(string time)
{
 StringReplace(time, "-", ".");
 return StringToTime(time);
}

string WebServer::toSQLTime(datetime time)
{
 string str_time = (string)time;
 
 StringReplace(str_time,".","-");
 
 return str_time;
}

string WebServer::SQLTimeCurrent()
{
 string time = (string)TimeCurrent();
 
 StringReplace(time,".","-");
 
 return time;
}


class TradeTable : public WebServer
{
 private:
  Logger logger;
  
 protected:
  string get_close_method(int ticket);
  
 public:
  bool request_all(CArrayInt &booknum);
  bool backfill(CArrayInt &missing_booknums_on_server);
  bool POST_open(int booknum, 
                 string symbol, 
                 int cmd, 
                 double lots, 
                 datetime open_time, 
                 double open_price, 
                 double sl, 
                 double tp, 
                 string order_comment);
  bool POST_close(int booknum, 
                    string symbol,
                    double sl, 
                    double tp, 
                    datetime close_time, 
                    double close_price,
                    double commission, 
                    double swaps, 
                    string order_comment,
                    double profit);
  bool POST_update(const string db_update_folder_name, const Supervisor* &_charts[]);
  bool query(const int &IN_orders[], const int IN_orders_size, TradeReport &OUT_trades[], int &trades_size);
  TradeTable(string host, const Credentials *creds, bool login);
};


bool TradeTable::query(const int &IN_orders[], const int IN_orders_size, TradeReport &OUT_trades[],int &trades_size)
{
  __TRACE__
  
  string response;
  POSTDATA data;
  
  string booknums="";
  for(int i=0; i<IN_orders_size; i++)
   booknums += IntegerToString(IN_orders[i]) + ",";
  
  data.set("terminal_pk", (string)p_terminal_pk);
  data.set("booknums", booknums);

  int ret_code = WebServer::WebRequestPOST(URL_QUERY_TRADES, data, response);
 
  if(ret_code!=OK_200)
   return false;

  string res[];
  StringSplit(response,';',res);
  trades_size=ArraySize(res);
  ArrayResize(OUT_trades, trades_size);
  
  if(IN_orders_size != trades_size) {
   LOG_HIGH_2(IN_orders_size, "!=", trades_size);
   return false;
  }
  
  for(int i=0; i<trades_size; i++)
  {
   string t[];
   StringSplit(res[i],',',t);
   LOG_EXTENSIVE_1(res[i]);
   
   OUT_trades[i].account = (int)t[0];
   OUT_trades[i].parent_account = (int)t[1];
   OUT_trades[i].parent_booknum = (int)t[2];
   OUT_trades[i].booknum = (int)t[3];
   OUT_trades[i].opposite_booknum = (int)t[4];
   OUT_trades[i].symbol = t[5];
   OUT_trades[i].type = t[6];
   OUT_trades[i].position = t[7];
   OUT_trades[i].lots = (double)t[8];
   OUT_trades[i].sl = (double)t[9];
   OUT_trades[i].tp = (double)t[10];
   OUT_trades[i].open_time = fromSQLTime(t[11]);
   OUT_trades[i].open_requested = (double)t[12];
   OUT_trades[i].open_price = (double)t[13];
   OUT_trades[i].open_latency = (int)t[14];
   OUT_trades[i].open_bid_before = (double)t[15];
   OUT_trades[i].open_bid_after = (double)t[16];
   OUT_trades[i].open_ask_before = (double)t[17];
   OUT_trades[i].open_ask_after = (double)t[18];
   OUT_trades[i].open_base_conversion = (double)t[19];
   OUT_trades[i].open_reason = t[20];
   OUT_trades[i].close_time = fromSQLTime(t[21]);
   OUT_trades[i].close_method = t[22];
   OUT_trades[i].close_requested = (double)t[23];
   OUT_trades[i].close_price = (double)t[24];
   OUT_trades[i].close_latency = (int)t[25];
   OUT_trades[i].close_bid_before = (double)t[26];
   OUT_trades[i].close_bid_after = (double)t[27];
   OUT_trades[i].close_ask_before = (double)t[28];
   OUT_trades[i].close_ask_after = (double)t[29];
   OUT_trades[i].close_base_conversion = (double)t[30];
   OUT_trades[i].close_reason = t[31];
   OUT_trades[i].commission = (double)t[32];
   OUT_trades[i].swaps = (double)t[33];
   OUT_trades[i].comment_on_open = t[34];
   OUT_trades[i].comment_on_close = t[35];
   OUT_trades[i].profit = (double)t[36];
   
   LOG_EXTENSIVE_1(OUT_trades[i].account);
   LOG_EXTENSIVE_1(OUT_trades[i].parent_account);
   LOG_EXTENSIVE_1(OUT_trades[i].parent_booknum);
   LOG_EXTENSIVE_1(OUT_trades[i].booknum);
   LOG_EXTENSIVE_1(OUT_trades[i].opposite_booknum);
   LOG_EXTENSIVE_1(OUT_trades[i].symbol);
   LOG_EXTENSIVE_1(OUT_trades[i].type);
   LOG_EXTENSIVE_1(OUT_trades[i].position);
   LOG_EXTENSIVE_1(OUT_trades[i].lots);
   LOG_EXTENSIVE_1(OUT_trades[i].sl);
   LOG_EXTENSIVE_1(OUT_trades[i].tp);
   LOG_EXTENSIVE_1(OUT_trades[i].open_time);
   LOG_EXTENSIVE_1(OUT_trades[i].open_requested);
   LOG_EXTENSIVE_1(OUT_trades[i].open_price);
   LOG_EXTENSIVE_1(OUT_trades[i].open_latency);
   LOG_EXTENSIVE_1(OUT_trades[i].open_bid_before);
   LOG_EXTENSIVE_1(OUT_trades[i].open_bid_after);
   LOG_EXTENSIVE_1(OUT_trades[i].open_ask_before);
   LOG_EXTENSIVE_1(OUT_trades[i].open_ask_after);
   LOG_EXTENSIVE_1(OUT_trades[i].open_base_conversion);
   LOG_EXTENSIVE_1(OUT_trades[i].open_reason);
   LOG_EXTENSIVE_1(OUT_trades[i].close_time);
   LOG_EXTENSIVE_1(OUT_trades[i].close_method);
   LOG_EXTENSIVE_1(OUT_trades[i].close_requested);
   LOG_EXTENSIVE_1(OUT_trades[i].close_price);
   LOG_EXTENSIVE_1(OUT_trades[i].close_latency);
   LOG_EXTENSIVE_1(OUT_trades[i].close_bid_before);
   LOG_EXTENSIVE_1(OUT_trades[i].close_bid_after);
   LOG_EXTENSIVE_1(OUT_trades[i].close_ask_before);
   LOG_EXTENSIVE_1(OUT_trades[i].close_ask_after);
   LOG_EXTENSIVE_1(OUT_trades[i].close_base_conversion);
   LOG_EXTENSIVE_1(OUT_trades[i].close_reason);
   LOG_EXTENSIVE_1(OUT_trades[i].commission);
   LOG_EXTENSIVE_1(OUT_trades[i].swaps);
   LOG_EXTENSIVE_1(OUT_trades[i].comment_on_open);
   LOG_EXTENSIVE_1(OUT_trades[i].comment_on_close);
   LOG_EXTENSIVE_1(OUT_trades[i].profit);
  }

  return true;
}


string TradeTable::get_close_method(int ticket)
{
 if(OrderSelect(ticket,SELECT_BY_TICKET))
  {
   string cmt = OrderComment();

   //--- search comments for close reason
    if(StringFind(cmt,"[sl]") >= 0)
     return("sl");
      
    if(StringFind(cmt,"[tp]") >= 0)
     return("tp");
       
    if(StringFind(cmt,"so:") >= 0)
     return("s.o.");
  }
  
 return("");
}

bool TradeTable::backfill(CArrayInt &missing_booknum_on_server)
{
 __TRACE__
 
 CArrayInt server_history;
 
 if(!request_all(server_history))
  return false;
  
 server_history.Sort();
 int total=OrdersHistoryTotal();
 
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(i,SELECT_BY_POS,MODE_HISTORY))
  {
   int ticket=OrderTicket();
   
   if(server_history.Search(ticket) < 0)
   {
    missing_booknum_on_server.Add(ticket);
    LOG_DEBUG("retrieving #" + (string)ticket);
   }
  }
 }

 return true;
}

bool TradeTable::POST_close(int booknum,
                            string symbol,
                            double sl,
                            double tp,
                            datetime close_time,
                            double close_price,
                            double commission,
                            double swaps,
                            string order_comment,
                            double profit)
{
  int digits = (int)MarketInfo(symbol, MODE_DIGITS);
  
  POSTDATA data;
  data.set("terminal_pk", (string)p_terminal_pk);
  data.set("booknum", IntegerToString(booknum));
  data.set("sl", DoubleToStr(sl, digits));
  data.set("tp", DoubleToStr(tp, digits));
  data.set("close_time", toSQLTime(close_time));
  data.set("close_price", DoubleToStr(close_price, digits));
  data.set("commission", DoubleToStr(commission, 2));
  data.set("swaps", DoubleToStr(swaps, 2));
  data.set("close_base_conversion", (string)SymbolInfoDouble(symbol, SYMBOL_TRADE_TICK_VALUE));
  data.set("comment_on_close", order_comment);
  data.set("close_method", get_close_method(booknum));
  data.set("profit", DoubleToStr(profit, 2));
  
  int response = WebServer::WebRequestPOST(URL_SET_CLOSED_TRADES, data);
  
  return response==OK_200;
}


bool TradeTable::POST_open(int booknum, string symbol, int cmd, double lots, datetime open_time, double open_price, double sl, double tp, string order_comment)
{
  __TRACE__
  
  int digits = (int)MarketInfo(symbol, MODE_DIGITS);

 //--- Attempt 1: post equity to server
  POSTDATA data;
  string type;
  
  switch(cmd) {
   case OP_BUY:  
     type = "BUY"; 
     break;
   case OP_SELL: 
     type = "SELL"; 
     break;
   default:
     LOG_HIGH("Can only post market orders");
     LOG_HIGH_1(cmd);
     return false;
  }
  
  data.set("terminal_pk", (string)p_terminal_pk);
  data.set("booknum", IntegerToString(booknum));
  data.set("symbol", symbol);
  data.set("type", type);
  data.set("lots", DoubleToStr(lots, 2));
  data.set("open_time", toSQLTime(open_time));
  data.set("open_price", DoubleToStr(open_price, digits));
  data.set("open_base_conversion", (string)SymbolInfoDouble(symbol, SYMBOL_TRADE_TICK_VALUE));
  data.set("sl", DoubleToStr(sl, digits));
  data.set("tp", DoubleToStr(tp, digits));
  data.set("comment_on_open", order_comment);
  
  int response = WebServer::WebRequestPOST(URL_SET_CLOSED_TRADES, data);

  
  return response==OK_200;
}


//+------------------------------------------------------------------+
bool TradeTable::POST_update(const string db_update_folder_name, const Supervisor* &_charts[])
 {
   __TRACE__ 
   
   string base_name;
   int    i=1;
   long    site_chart_pk=0;
  
//--- receive search handle in local folder's root
   string filepath = StringConcatenate(db_update_folder_name + "\\*" + "_update" + "*.csv");
   
   long search_handle=FileFindFirst(filepath, base_name);
   
   bool reset_file_handler = false;
   
//--- check if FileFindFirst() function executed successfully
   if(search_handle!=INVALID_HANDLE)
     {
      bool all_updated = true;
      
     //--- check if the passed strings are file or directory names in the loop
      do
        {
         if(reset_file_handler) {
          search_handle=FileFindFirst(filepath, base_name);
          reset_file_handler=false;
         } 
      
       //--- Append directory to filename
         string filename = StringConcatenate(db_update_folder_name,"\\",base_name);
   
         ResetLastError();
         
       //--- if this is a file, then send to webrequest
         if(FileIsExist(filename))
          {
           int handle = FileOpen(filename, FILE_READ|FILE_CSV);
           LOG_EXTENSIVE("Opening filename=" + filename);
           string query = "";
           int booknum = -1;
            
           if(handle>=0)
            {
             bool send=false;
             
             //--- search for update and booknum keywords
              while(!FileIsEnding(handle))
              {
               string value = FileReadString(handle);
               string res[];
               StringSplit(value,'=',res);      //Expecting format "key=value" from each line in file
                
               if(ArraySize(res)==2)
               {
                if(res[0]=="upload" && res[1]=="TRUE") 
                {
                 send=true;
                 continue;
                }

                if(res[0]=="chart_id")
                {
                 int sz=ArraySize(_charts);
                 for(int j=0; j<sz; j++)
                 {
                  if((long)res[1]==_charts[j].id())
                   site_chart_pk=_charts[j].pk();
                 }
                }
              
                if(res[0]=="booknum")
                 booknum = (int)res[1];
                   
                query = StringConcatenate(query,res[0],"=",res[1],";");        //Add value of each line in file to query
               }
              }
               
            //--- close file
             FileClose(handle);
               
            //--- post if upload keyword is found
             if(send) 
             {
               LOG_EXTENSIVE("Posting " + filename);
               
               query += "terminal_pk=" + (string)terminal_pk() + ";";
               
               if(site_chart_pk > 0)
                query += "chart_pk=" + (string)site_chart_pk + ";";
   
               int response = WebRequestPOST(URL_SET_CLOSED_TRADES, query);
              
               if(response==OK_200)
               {
                LOG_EXTENSIVE("POST Successful");  
                
                if(!FileDelete(filename))
                 LOG_HIGH_ERROR("Failed to delete file: " + filename);
                 
                reset_file_handler = true;
                continue;
               }
               else
               {
                FolderCreate(StringConcatenate(db_update_folder_name, "\\archives"));
                string archive_fname = StringConcatenate(db_update_folder_name,"\\archives\\",base_name);
                
                LOG_DEBUG("Attempting to move " + filename + " -> " + archive_fname);
                
                if(!FileMove(filename, 0, archive_fname, FILE_REWRITE))
                 LOG_DEV_WARNING_ERROR("Failed to move " + filename + " -> " + archive_fname);
                 
                LOG_HIGH_ERROR("Post failed, web_response: " + (string)response);
               }
            }
            
            all_updated = false;
          }
        } 
       //---- Increment index: open next file 
        i++;
   
     }//end do
     while(FileFindNext(search_handle, base_name));
     
     //--- close search handle
       FileFindClose(search_handle);
 
     return all_updated;     
    }
   
   LOG_DEBUG_ERROR("Invalid search handle for filepath=" + filepath);
   return false;        //DB state was not changed
 }


bool TradeTable::request_all(CArrayInt &booknum)
{
  __TRACE__
  
  string headers;
  char get[], result[];
  
  LOG_DEBUG_1_RAW(p_terminal_pk);
  
  int res_code = WebRequest("GET", StringConcatenate(p_server_host, URL_GET_ALL_BOOKNUMS, "/", p_terminal_pk),
               "", NULL, 10000, get, ArraySize(get), result, headers);
 
  string response = CharArrayToString(result);
  
  if(res_code==OK_200)
   LOG_DEBUG_1(res_code)
  else {
   LOG_HIGH("Unable to fetch booknums from server!!!")
   LOG_HIGH("[BAD RESPONSE]: " + response)
   return false;
  }            
  
  string res[];
  StringSplit(response,';',res);
  int size=ArraySize(res);
  int closed_trades_total=-1;
  
  LOG_EXTENSIVE_1(size);
  
  if(size > 0)
  {
   closed_trades_total=(int)res[0];
   LOG_EXTENSIVE_1(closed_trades_total);
   
   for(int i=1; i<size; i++) {
    int bk=(int)res[i];
    booknum.Add(bk);
    LOG_EXTENSIVE("Adding #" + (string)bk);
   }
  }
  else
   LOG_HIGH("res[0] should be number of closed trades");
  
  int booknum_total=booknum.Total();
  if(booknum_total != closed_trades_total)
   LOG_HIGH_2(booknum_total, "!=", closed_trades_total);
  
  return true;
}

TradeTable::TradeTable(string host, const Credentials *creds, bool login=true) : logger(__FILE__, "helper_processes"),
 WebServer(host, creds)
{
}


class Email
{
 private:
  string p_HOST;
  Logger logger;
  WebServer web_server;
  
 public:
  bool send(string subject, string content);
  Email(const string host, const Credentials *creds);
};

Email::Email(const string host, const Credentials *creds) : logger(__FILE__), web_server(host, creds)
{
 p_HOST = host;
}

bool Email::send(string subject,string content)
{
  __TRACE__
  
  POSTDATA data;
  data.set("subject", subject);
  data.set("html_content", content);
  
  int response=web_server.WebRequestPOST(URL_SEND_EMAIL, data);
  
  if(response!=OK_200) {
   LOG_HIGH_ERROR("Send email failed");
  }
  
  return response==OK_200;
}
