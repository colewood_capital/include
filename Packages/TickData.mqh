//+------------------------------------------------------------------+
//|                                                     TickData.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#ifndef ARRAYSTRING
#define ARRAYSTRING
   #include  <Arrays\ArrayString.mqh>
#endif 

#ifndef WEBINTERFACE
#define WEBINTERFACE
   #include <Packages\WebInterface.mqh>
#endif 

#ifndef LOGGER
#define LOGGER
   #define LOGGER_DEPRECATED
   #include <Logger.mqh>
#endif 

#ifndef OK_200
#define OK_200       200
#endif 

#define DELIMITER                         "$"
#define FLUSH_FILE_FREQ_IN_SECS           15

class TickDataMachine
{
private:
 Logger logger;

protected:
 WebServer *p_server;
 string p_symbol;
 string p_url_post_tick_data;
 string p_buffer;
 int p_file_update_in_secs;
 datetime p_last_timestamp;
 
 bool post_to_web_server(string tick_data, string name);
 string get_filepath();
 string mySqlTime(datetime time);
 void writeToFile();
 
public:
 void timer_engine();
 void tick_engine();
 TickDataMachine(WebServer *server, string url_post_tick_data, int update_frequency_in_secs);
};

TickDataMachine::TickDataMachine(WebServer *_server, string url_post_tick_data, int update_frequency_in_secs)
 : logger(__FILE__, "helper_processes")
{
 p_url_post_tick_data=url_post_tick_data;
 p_server=_server;
 p_file_update_in_secs=MathMin(FLUSH_FILE_FREQ_IN_SECS, update_frequency_in_secs);
 p_last_timestamp=TimeLocal();
 p_symbol=Symbol();
}

string TickDataMachine::get_filepath(void)
{ 
 __TRACE__
 
 while(StringLen(AccountServer())==0 && !IsStopped())
 {
  LOG_HIGH_RAW("Not connected to server");
  Sleep(100);
 }
 
 string filepath="TickData\\" + AccountServer() + "\\" + Symbol() + "\\"; //Replace spaces with underscores
 StringReplace(filepath, " ", "_");
 return filepath;
}

void TickDataMachine::timer_engine()
{
 __TRACE__
 
 CArrayString opened_filename;
 string PATH = "TickData\\";
 
 //--- flush any ticks in buffer to file
  __ writeToFile();
  
 string fname,currpath;
 string data;
  
  //--- RECURSION LEVEL 1: server
    long search_handle_server=FileFindFirst(PATH + "*", fname);

    if(search_handle_server != INVALID_HANDLE)
    {
     do
     {
       currpath=StringConcatenate(PATH, fname);
     
       //--- RECURSION LEVEL 2: symbols
       long search_handle_symbol=FileFindFirst(currpath + "\\*", fname);
     
       if(search_handle_symbol != INVALID_HANDLE)
       {
        string symbol;
        string server_basepath = currpath;
      
        do
        {
          currpath=StringConcatenate(server_basepath, fname);
          symbol = StringSubstr(fname, 0, StringLen(fname)-1);    //Strips a '\' from the filepath
          
          //--- RECURSION LEVEL 3: ticks
          long search_handle_ticks=FileFindFirst(currpath + "\\*", fname);
          int MAX_FILE_UPLOAD_COUNT = 500; //Limits trying to upload too many files at once
          int file_counter = 0;
              
          if(search_handle_ticks != INVALID_HANDLE)
          {
           string sym_basepath=currpath;
     
           do
           { 
            currpath=StringConcatenate(sym_basepath, fname);

            if(StringLen(fname) > 0 && StringLen(currpath) > 0)
            { 
             LOG_EXTENSIVE("extracting " + fname + " ...");
             
             int handle=FileOpen(currpath, FILE_READ);
             if(handle != INVALID_HANDLE)
             {
              while(!FileIsEnding(handle)) 
               data+=FileReadString(handle) + DELIMITER;
           
              //--- remove final delimiter
               int len=StringLen(data);
               if(len > 0)
                data = StringSubstr(data, 0, len-1);
              
              opened_filename.Add(currpath);
              FileClose(handle);    
             }
             else
              LOG_HIGH_ERROR("Error opening file=" + currpath);
            }
           }//--- end RECURSION LEVEL 3
           while(FileFindNext(search_handle_ticks, fname) && ++file_counter < MAX_FILE_UPLOAD_COUNT);
          
        //--- POST to server && delete files if post_to_web_server returns true
          __ if(StringLen(data) > 0) 
          { 
           LOG_EXTENSIVE_RAW("Attempting to post " + data);
           
           if(post_to_web_server(data, symbol))
           {
            LOG_DEBUG_1(data);
            
            int total=opened_filename.Total();
           
            for(int j=0; j<total; j++) {
             LOG_EXTENSIVE_RAW("Deleting " + opened_filename[j]);
             FileDelete(opened_filename[j]);
            }
           } 
           else
           {
            LOG_DEBUG(symbol + " post failed: " + data);
           }
          }
              
         //--- find next symbol
          data = "";
          opened_filename.Clear();
          FileFindClose(search_handle_ticks);
         }
        }
        while(FileFindNext(search_handle_symbol, fname));
        FileFindClose(search_handle_symbol);
       }//--- end RECURSION LEVEL 2
      }
      while(FileFindNext(search_handle_server, fname));
      FileFindClose(search_handle_server);
    }//--- end RECURSION LEVEL 1
}


bool TickDataMachine::post_to_web_server(string tick_data, string symbol=NULL)
{
 __TRACE__
 
 if(symbol==NULL)
  symbol = p_symbol;
 
 int digits = (int)MarketInfo(symbol, MODE_DIGITS);
  
 POSTDATA data;
 data.set("name", symbol);
 data.set("data", tick_data);

 __ int response = p_server.WebRequestPOST(p_url_post_tick_data, data);
  
 return response==OK_200;
}


void TickDataMachine::tick_engine(void)
{
 __TRACE__
 
 static MqlTick tick;
   
 if(SymbolInfoTick(p_symbol, tick))
 {
  p_buffer += mySqlTime(tick.time) + ",";
  p_buffer += DoubleToStr(tick.bid, Digits) + ",";
  p_buffer += DoubleToStr(tick.ask, Digits) + "\n";
  //LOG_EXTENSIVE("incoming: " + TimeToStr(tick.time, TIME_DATE|TIME_MINUTES|TIME_SECONDS) + ", " + (string)tick.bid + ", " + (string)tick.ask);
 }
 else {
  LOG_HIGH_ERROR("Could not execute SymbolInfoTick()");
 }
 
 if(TimeLocal()-p_last_timestamp >= p_file_update_in_secs)
  writeToFile();
}


string TickDataMachine::mySqlTime(datetime dt)
{
 //--- Convert program datetime format to MySQL datetime format
  string ret = TimeToStr(dt, TIME_DATE|TIME_MINUTES|TIME_SECONDS);
  StringReplace(ret, ".", "-");
  return ret;
}


void TickDataMachine::writeToFile()
{
 __TRACE__
 
 if(StringLen(p_buffer) > 0)
 {
  //--- Create unique filename
   string filepath = get_filepath();
   string fullpath, fname;
   int i=1;
   
   do {
    fname = StringConcatenate((int)TimeLocal(), "_", i, ".txt");
    fullpath = StringConcatenate(filepath, fname);
    i++;
   }
   while(FileIsExist(fullpath));
   
  //--- Write recorded tick data to file
   int handle = FileOpen(fullpath, FILE_WRITE|FILE_TXT);
   
   if(handle >= 0) {
    uint bytes_written = FileWrite(handle, p_buffer);
    
    LOG_EXTENSIVE("Flush buffer (->" + fname + "): " + p_buffer);
    LOG_EXTENSIVE_1(bytes_written);
    
    if(bytes_written > 0)     p_buffer = "";
    
    FileClose(handle);
   }
   else
    LOG_HIGH_ERROR("Could not open file");
 }
 
 p_last_timestamp=TimeLocal();
}
