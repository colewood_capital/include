//+------------------------------------------------------------------+
//|                                                     DllUtils.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#define DLL_LOGGER_INIT(func, name)        string path = TerminalInfoString(TERMINAL_DATA_PATH) + "\\MQL4\\Libraries\\" + name + ".log"; \
                                           StringReplace(path, "\\", "\\\\"); \
                                           func(path)

#define DLL_UTILS_LOGGER_INIT              DLL_LOGGER_INIT(UtilsLoggerInit, "Utils")

struct SYSTEMTIME
{
 unsigned short wYear;
 unsigned short wMonth;
 unsigned short wDayOfWeek;
 unsigned short wDay;
 unsigned short wHour;
 unsigned short wMinute;
 unsigned short wSecond;
 unsigned short wMilliseconds;
};
                              
#import "projects\\Utils\\Utils.dll"
   // void LoggerInit(string full_log_path);       Needs to be called from the import section of the compiled dll
   void UtilsLoggerInit(string full_log_path);
   bool CFileDelete(string full_path);
   bool CFileExists(string full_path);
   void CFileCopy(const string from_path, const string to_path);
   void CFileLastWriteTime(string full_path, SYSTEMTIME& utctime);
   int DirectoryDelete(string root_directory, bool bDeleteSubdirectories=true);
   bool DirectoryExists(const string folder_name);
   int LaunchProcess(string abs_path, string args="", string output_file_path="");
#import

//+------------------------------------------------------------------+
string DllLoggerPath(string dll_name)
{
 string path = TerminalInfoString(TERMINAL_DATA_PATH) + "\\MQL4\\Libraries\\" + dll_name + ".log";
 StringReplace(path, "\\", "\\\\");
 return path;
}
//+------------------------------------------------------------------+
bool CheckHistoryFileLastWriteTime(string symbol, int period, datetime& update_time, string script_name)
{
 string filename = symbol + (string)period + ".hst";
 
 string path = TerminalInfoString(TERMINAL_DATA_PATH) + "\\history\\" + AccountServer() + "\\" + filename;
 StringReplace(path, "\\", "\\\\");
 
 update_time = FileLastWriteTime(path);
 
 return (update_time <= TimeCurrent() && update_time > 0);
}
//+------------------------------------------------------------------+
bool CopyHistoryFileFromTemp(string symbol, int period, string script_name)
{
 string filename = symbol + (string)period + ".hst";
 
 string f1 = TerminalInfoString(TERMINAL_DATA_PATH) + "\\history\\" + AccountServer() + "\\" + filename;
 StringReplace(f1, "\\", "\\\\");
 
 string f2 = TerminalInfoString(TERMINAL_DATA_PATH) + "\\history\\" + AccountServer() + "\\temp\\" + script_name + "\\" + filename;
 StringReplace(f2, "\\", "\\\\");
 
 if(CFileExists(f2))
 {
  CFileCopy(f2, f1);
  return true;
 }
  
 Alert("[ERROR (", __LINE__, ": file DNE] ", f2);

 return false;
}
//+------------------------------------------------------------------+
bool DeleteHistoryFile(string symbol, int period, string script_name)
{
 string filename = symbol + (string)period + ".hst";
 
 string path = TerminalInfoString(TERMINAL_DATA_PATH) + "\\history\\" + AccountServer() + "\\" + script_name + "\\" + filename;
 StringReplace(path, "\\", "\\\\");

 return(CFileDelete(path));
}
//+------------------------------------------------------------------+
bool SafeDeleteHistoryFile(string symbol, int period, string script_name)
/* Closing the current chart creates a new .hst file. SafeDeleteHistoryFile() attempts to
   first close all charts and then check for the new .hst file to be created before
   deleting it  */
{
 int count = 0;
 string filename = symbol + (string)period + ".hst";
 
 string path = TerminalInfoString(TERMINAL_DATA_PATH) + "\\history\\" + AccountServer() + "\\" + script_name + "\\" + filename;
 StringReplace(path, "\\", "\\\\");
 
 datetime last_modified = FileLastWriteTime(path);
 bool first_iteration = true;
 
 while(count < 5)
 {
  if(_DeleteHistoryFile(symbol, period, path, last_modified, first_iteration))
   break;
  
  first_iteration = false;  
  count++;
 }
 
 if(count==5) {
  Alert("ERROR: DeleteHistoryFile(): Could not confirm ", filename, " has been deleted");
  return false;
 }
 
 return true;
}
//+------------------------------------------------------------------+
bool _DeleteHistoryFile(string symbol, int period, string path, datetime last_modified, bool first_iteration)
{
 if(!IsConnected()) {
  Alert("ERROR: _DeleteHistoryFile(): not conncected to server");
  return true;
 }
 
 if(Symbol()==symbol && Period()==period) {
  Alert("ERROR: _DeleteHistoryFile(): cannot run script on (", symbol, ",", period, ")");
  return true;
 }
 
 int count = _ChartsCloseAll(symbol, period);
 
 if(first_iteration)
  Sleep(12000);
 else
  Sleep(4000);

  if(count > 0 && MathAbs(FileLastWriteTime(path) - last_modified) < DBL_EPSILON)
    return false;

 if(!CFileDelete(path))
  Alert("ERROR: _DeleteHistoryFile(): could not delete " + path);
  
 return true;
}
//+------------------------------------------------------------------+
int _ChartsCloseAll(string symbol)
{
 long chart_id = ChartFirst();
 int count = 0;
 
 do {
   if(ChartSymbol(chart_id)==symbol) {
    ChartClose(chart_id);
    count++;
   }
    
   chart_id = ChartNext(chart_id);
  } 
  while(chart_id >= 0);
  
  return count;
}
//+------------------------------------------------------------------+
int _ChartsCloseAll(string symbol, int period)
{
 long chart_id = ChartFirst();
 int count = 0;
 
 do {
   if(ChartSymbol(chart_id)==symbol && ChartPeriod(chart_id)==period) {
    ChartClose(chart_id);
    count++;
   }
    
   chart_id = ChartNext(chart_id);
  } 
  while(chart_id >= 0);
  
  return count;
}
//+------------------------------------------------------------------+
datetime FileLastWriteTime(string full_path)
{
 SYSTEMTIME s;

 CFileLastWriteTime(full_path, s);

 MqlDateTime t;
 t.day = s.wDay;
 t.hour = s.wHour;
 t.min = s.wMinute;
 t.sec = s.wSecond;
 t.mon = s.wMonth;
 t.year = s.wYear;
 
 return StructToTime(t);
} 
