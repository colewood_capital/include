//+------------------------------------------------------------------+
//|                                                         Math.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#ifndef LOGGER
#define LOGGER
   #define LOGGER_DEPRECATED
   #include <Logger.mqh>
#endif

Logger mathlogger(__FILE__, "helper_processes");

//Representaion of a DOUBLE_VALUE=8 rational number
struct Rational
{
 long whole;
 long frac[DOUBLE_VALUE];
 bool negative;
 
 Rational();
};

Rational::Rational(void)
{
 negative = false;
 whole = 0;
 for(int i=0; i<DOUBLE_VALUE; i++)  frac[i]=0;
}
//+------------------------------------------------------------------+
// [IN} number of type double
// [OUT] rational number representation of input
Rational DoubleToRational(double x)
{
 Rational r;
 r.negative = (x < 0) ? True : False;
 
 string str_x = DoubleToStr(MathAbs(x));
 int decimal_separator_position = StringFind(str_x, ".");
 
 if(decimal_separator_position >= 0)
 {
  int j=0;
  r.whole = StringToInteger(StringSubstr(str_x, 0, decimal_separator_position));
  
  for(int i=decimal_separator_position+1; i<decimal_separator_position+9; i++) {
   r.frac[j] = StringToInteger(StringSubstr(str_x, i, 1));
   j++;
  }
 }
 else
  Alert("decimal_separator_position < 0, str_x=" + str_x);
 
 return r;
}
//+------------------------------------------------------------------+
// [IN] rational number stuct (defined above)
// [OUT] double representation of a rational number struct
double RationalToDouble(const Rational& x)
{
 string str_x = NULL;

 str_x += IntegerToString(x.whole);
 str_x += ".";

 for(int i=0; i<DOUBLE_VALUE; i++)
  str_x += IntegerToString(x.frac[i]);
 
 int multiplier = x.negative ? -1 : 1;
 
 return StringToDouble(str_x) * multiplier;
};
//+------------------------------------------------------------------+
// [IN] timestamp
// [OUT] Highest price between now and the given timestamp
double MathHighestPrice(datetime end_timestamp, datetime &OUT_high_tstamp, string symbol=NULL, ENUM_TIMEFRAMES timeframe=PERIOD_CURRENT, datetime begin_timestamp=0, bool include_current_bar=true)
{
 int s=0;
 
 if(!include_current_bar)
  s=1;
  
 int offset = (begin_timestamp > 0) ? iBarShift(symbol, timeframe, begin_timestamp) : 0;
 
 int count = iBarShift(symbol, timeframe, end_timestamp);
 
 int index = (count > 0) ? iHighest(symbol, timeframe, MODE_HIGH, count, offset+s) : 0;
 
 double highest = iHigh(symbol, timeframe, index);
 OUT_high_tstamp = iTime(symbol, timeframe, index);
 
 string begin_tstamp = (begin_timestamp > 0) ? TimeToStr(begin_timestamp,TIME_DATE|TIME_MINUTES|TIME_SECONDS) : "0";
 string debug_msg = StringConcatenate("highest_prc=", DoubleToStr(highest, Digits), ", highest_prc_timestamp=", TimeToStr(OUT_high_tstamp), ", begin_timestamp=", begin_tstamp, ", end_timestamp=" + TimeToStr(end_timestamp), ", count=", count, ", index=", index, ", offset=", offset, ", include_current_bar=", include_current_bar, ", symbol=", symbol, ", timeframe=", EnumToString(timeframe));
 mathlogger.log(debug_msg, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
 
 return highest;
}
//+------------------------------------------------------------------+
// [IN] timestamp
// [OUT] Lowest price between now and the given timestamp
double MathLowestPrice(datetime end_timestamp, datetime &OUT_low_tstamp, string symbol=NULL, ENUM_TIMEFRAMES timeframe=PERIOD_CURRENT, datetime begin_timestamp=0, bool include_current_bar=true)
{
 int s=0;
 
 if(!include_current_bar)
  s=1;
  
 int offset = (begin_timestamp > 0) ? iBarShift(symbol, timeframe, begin_timestamp) : 0;
 
 int count = iBarShift(symbol, timeframe, end_timestamp);
 
 int index = (count > 0) ? iLowest(symbol, timeframe, MODE_LOW, count, offset+s) : 0;
 
 double lowest = iLow(symbol, timeframe, index);
 OUT_low_tstamp = iTime(symbol, timeframe, index);
 
 string begin_tstamp = (begin_timestamp > 0) ? TimeToStr(begin_timestamp,TIME_DATE|TIME_MINUTES|TIME_SECONDS) : "0";
 string debug_msg = StringConcatenate("lowest_prc=", DoubleToStr(lowest, Digits), ", lowest_prc_timestamp=", TimeToStr(OUT_low_tstamp), ", begin_timestamp=", begin_tstamp + ", end_timestamp=", TimeToStr(end_timestamp), ", count=", count, ", index=", index, ", offset=", offset, ", include_current_bar=" , include_current_bar, ", symbol=", symbol, ", timeframe=", EnumToString(timeframe));
 mathlogger.log(debug_msg, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
 
 return lowest;
}
//+------------------------------------------------------------------+
double MathCalculateProfitLoss(int type, double open_price, double max_min_price, double lots, string symbol=NULL)
{
 double profit = 0.0;
 double contract_size = MarketInfo(symbol, MODE_LOTSIZE);
 
 if(type==OP_BUY)
  profit = (max_min_price - open_price) * (lots * contract_size);
 
 if(type==OP_SELL)
  profit = (open_price - max_min_price) * (lots * contract_size);
  
 mathlogger.log("profit=" + DoubleToStr(profit, 2) + ", type=" + EnumToString((ENUM_ORDER_TYPE)type) + ", open_price=" + DoubleToStr(open_price, 5) + ", max_min_price=" + DoubleToStr(max_min_price, 5) + ", symbol=" + symbol, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
 
 return profit * MarketInfo(symbol, MODE_TICKVALUE);
}
//+------------------------------------------------------------------+
double MathCalculateLotSize(string symbol, double percent_risk, double price, double sl, uint reentries=0)
{
 double MIN_LOTSIZE = 0.01;

 double lots = 0;
 
 if(percent_risk >= 0 && percent_risk <= 1)
 {
  double dollar_risk = AccountEquity() * percent_risk;
 
  double point_size = MarketInfo(symbol, MODE_POINT);
  double standard_lot = MarketInfo(symbol,MODE_LOTSIZE);

  double point_distance = MathAbs((price - sl) * (1/point_size)) / standard_lot;
 
  double tick_value = MarketInfo(symbol,MODE_TICKVALUE); 

  double volume = (point_distance * tick_value != 0) ? dollar_risk / (point_distance * tick_value) : 0;

  lots = volume / standard_lot;
  
  lots /= (1+reentries);
 
  lots = NormalizeDouble(lots, 2);
 
  if(lots < MIN_LOTSIZE)
   lots = MIN_LOTSIZE;
 }
 else
  mathlogger.log("percent_risk=" + DoubleToStr(percent_risk, 2) + " must be between zero and one", LOG_TYPE_HIGH, __LINE__, __FUNCTION__);

 return(lots);
}