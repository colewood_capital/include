//+------------------------------------------------------------------+
//|                                                   Supervisor.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#include <DataTypes\AutoSaveLong.mqh>

#ifndef CHARTS 
#define CHARTS 
   #include <Packages\Charts.mqh>
#endif 

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
#endif 

#ifndef HEADERS
#define HEADERS
   #include <Super\Headers.mqh>
#endif 

string UPSTART_DIR = "Supervisor\\";

class Supervisor
{
private:
 Logger logger;
 
protected:
 long chart_id;
 long p_pk;
 string p_symbol;
 string p_chart_template_name;
 ENUM_TIMEFRAMES p_period;
 datetime p_last_update_local_time;
 int p_min_ea_update_in_seconds;
 string heartbeat_glb_var_name;
 void wait_for_ea_init(long chart_id);
 

public:
 Supervisor(long pk, string symbol, ENUM_TIMEFRAMES period, int min_ea_update_in_seconds, string template_name);
 virtual ~Supervisor();
 void update(long &prev_chart_id);
 long id() const { return chart_id; }
 long pk() const { return p_pk; };
 void update_heartbeat();
};

void Supervisor::update_heartbeat()
{
 p_last_update_local_time = TimeLocal();
}

void Supervisor::wait_for_ea_init(long ea_chart_id)
{
 __TRACE__

 string target_name = GLB_CONFIG_INIT_STATUS + "_" + (string)ea_chart_id;
 const int TIMEOUT_IN_SECS = 25;
 bool b_confirmed=false;
 LOG_DEBUG("Waiting for chart id #" + (string)ea_chart_id + " to initialize ...");
 datetime now=TimeLocal();
 
 while(TimeLocal()-now < TIMEOUT_IN_SECS && !IsStopped())
 {
  if(GlobalVariableGet(target_name)==1) {
   LOG_DEBUG("confirmed chart id #" + (string)ea_chart_id + " initialized !!!");
   b_confirmed=True;
   break;
  }
   
  Sleep(200);
 }
 
 if(!b_confirmed)
  LOG_LOW_RAW("failed to confirm chart #" + (string)ea_chart_id + " initialized :( [TIMEOUT=" + (string)TIMEOUT_IN_SECS + " seconds]");
 
 GlobalVariableDel(target_name);
}

void Supervisor::update(long &prev_chart_id)
{
 __TRACE__
 
 prev_chart_id=chart_id;
 bool b_in_process_of_closing_chart=false;
 
 //--- check for heartbeat
  if(chart_id > 0) // safety measure to prevent accidently closing of chart running supervisor
  {
   if(TimeLocal()-p_last_update_local_time >= p_min_ea_update_in_seconds)
   {
    string chart_sym=ChartSymbol(chart_id);
    string chart_period=EnumToString(ChartPeriod(chart_id));
    string msg="No heartbeat detected in more than " + (string)p_min_ea_update_in_seconds + " seconds on chart (" + chart_sym + ", " + chart_period + ", pk=" + (string)p_pk + ", chart_id=" + (string)chart_id + ")";
    LOG_SYSTEM(msg);
   
    //if(Charts::ChartCloseSafe(chart_id))
    // b_in_process_of_closing_chart=true; // needed b.c. ChartExists (below) will still return true
   }
  }
 /*
 // the idea for reopening if chart_id<=0 is that prev attempt to open chart 3 times failed and now we try again
 __ if(b_in_process_of_closing_chart || chart_id<=0 || !Charts::ChartExists(chart_id)) //create chart if pk exists, but cannot find matching chart
 {
  const int ATTEMPTS=1;
  int k=0;
  
  do
  {
   chart_id = ChartOpen(p_symbol, p_period);
   Sleep(500); // Opening a series of charts too fast causes the ChartOpen() function to stop working

   if(chart_id <= 0)
   {
    LOG_DEV_WARNING_RAW("Failed to open chart, symbol=" + ChartSymbol(chart_id) + ", period=" + EnumToString(ChartPeriod(chart_id)));
   }
   else
    Sleep(300);
   
   k++;
  } while(chart_id<=0 && k<ATTEMPTS);
  
  if(chart_id <= 0)
  {
   LOG_HIGH("[MT4_BUG] Failed MAX_ATTEMPTS(=" + (string)ATTEMPTS + ") times to open chart, symbol=" + ChartSymbol(chart_id) + ", period=" + EnumToString(ChartPeriod(chart_id))+ ". Known solution=reapply site_reporting EA");
   return false;
  }
  
  p_last_update_local_time = TimeLocal();
  
  if(p_chart_template_name!=NULL)
  {
   const int _ATTEMPTS=3;
   bool b_success=false;
   
   for(int i=0; i<_ATTEMPTS && !b_success; i++) {
    b_success=ChartApplyTemplate(chart_id, p_chart_template_name);
    Sleep(200);
   }
    
   if(b_success)        // ea is applied to chart when template is loaded
   {
    __ wait_for_ea_init(chart_id);
   }
   else
   {
    int e=GetLastError();
    if(e==4051)    // ERR_INVALID_FUNCTION_PARAMETER_VALUE.
                   // EA did in fact load. Known issue: caused by MQL4 incorrectly returning false while searching the path for chart_template_name
    { 
     __ wait_for_ea_init(chart_id);
    }
    else
    {
     LOG_HIGH_ERROR("[CLOSE CHART] Failed to apply template " + p_chart_template_name + " (ATTEMPTS=" + (string)_ATTEMPTS + "), chart_id=" + (string)chart_id);
     Charts::ChartCloseSafe(chart_id);
    }
   }
  }

  LOG_SYSTEM("[OPEN CHART] symbol=" + p_symbol + ", period=" + EnumToString(p_period) + ", pk=" + (string)p_pk + ", chart_id=" + (string)prev_chart_id + " -> " + (string)chart_id);
  return true;
 }

 if(chart_id > 0)
 {
  //--- check symbol and timeframe correct 
  string chart_symbol=ChartSymbol(chart_id);
  if(chart_symbol==NULL)
   chart_symbol=Symbol();
  
  ENUM_TIMEFRAMES chart_period=ChartPeriod(chart_id);

  if(chart_symbol != p_symbol)
  {
   LOG_SYSTEM("[MODIFY] incorrect symbol(" + chart_symbol + ") -> " + p_symbol + ", id=" + (string)chart_id);
   if(ChartSetSymbolPeriod(chart_id, p_symbol, p_period))
    return true;
   else
    LOG_HIGH_ERROR("Failed to modify incorrect symbol(" + chart_symbol + ") -> " + p_symbol + ", id=" + (string)chart_id);
  }

  if(chart_period != p_period)
  {
   bool modify=true;
  
   if(chart_period==PERIOD_CURRENT && Period()==p_period)
   {
    LOG_EXTENSIVE("[ABORT] modify for chart_period=PERIOD_CURRENT=" + (string)Period() + "=" + (string)p_period); 
    modify=false;
   }
 
   if(modify)
   {
    LOG_LOW("[MODIFY] incorrect period(" + EnumToString(chart_period) + ") -> " + EnumToString(p_period) + ", id=" + (string)chart_id);
   
    ChartSetSymbolPeriod(chart_id, p_symbol, p_period);
    
    return true;
   }
  }
 }
 
 return false;
 */
}

Supervisor::Supervisor(long pk, string symbol, ENUM_TIMEFRAMES period, int min_ea_update_in_seconds, string template_name=NULL)
 : p_pk(pk), 
   p_symbol(symbol), 
   p_period(period), 
   p_chart_template_name(template_name),
   p_min_ea_update_in_seconds(min_ea_update_in_seconds),
   logger(__FILE__, "helper_processes")
{
 __TRACE__
 
 const int ATTEMPTS=3;
 int k=0;
 
 do
 {
  chart_id = ChartOpen(symbol, period);
  
  if(chart_id > 0)
  {
   p_last_update_local_time = TimeLocal();
   
   LOG_DEBUG("[OPEN CHART] symbol=" + symbol + ", period=" + EnumToString(period) + ", pk=" + (string)pk + ", chart_id=" + (string)chart_id);
 
   if(p_chart_template_name!=NULL && !ChartApplyTemplate(chart_id, p_chart_template_name))
   {
    int e=GetLastError();
    if(e!=4051)    // ERR_INVALID_FUNCTION_PARAMETER_VALUE. Known issues: caused by MQL4 incorrectly returning false while searching the path for chart_template_name
    { 
     LOG_HIGH("Failed to apply template " + p_chart_template_name + ", chart_id=" + (string)chart_id + ", e=" + ErrorDescription(e));
    }
    else 
    {
     __ wait_for_ea_init(chart_id);
    }
   }
   long prev_chart_id;
  
   __ update(prev_chart_id);
  
   LOG_EXTENSIVE_3_RAW(p_symbol, p_period, p_pk);
  }
  else
  {
   LOG_HIGH_ERROR("Failed to open chart, symbol=" + symbol + ", period=" + EnumToString(period));
   Sleep(300);
  }
  
  k++;
 } while(chart_id<=0 && k<ATTEMPTS);
 
 if(chart_id <= 0)
 {
  LOG_LOW_RAW("[CHART_INIT] failed MAX_ATTEMPTS(=" + (string)ATTEMPTS + ") times to open chart, symbol=" + ChartSymbol(chart_id) + ", period=" + EnumToString(ChartPeriod(chart_id)));
 }
 
 p_last_update_local_time = TimeLocal();
}

Supervisor::~Supervisor()
{
 __TRACE__

 bool b_exists = Charts::ChartExists(chart_id);

 if(b_exists)
  Charts::ChartCloseSafe(chart_id);
}
