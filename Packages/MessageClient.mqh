//+------------------------------------------------------------------+
//|                                                MessageClient.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#ifndef UUID
#define UUID
   #include <Packages\UUID.mqh>
#endif 

#ifndef HEADERS
#define HEADERS
   #include <Super\Headers.mqh>
#endif 

#ifndef LOGGER 
#define LOGGER 
   #define LOGGER_DEPRECATED
   #include <Logger.mqh>
#endif 

#define ASCII_RECORD_SEPERATOR         30

class MessageClient
{
private:
 Logger logger;
 
protected:
 UUIDGenerator uuid;

public:
 MessageClient() : logger(__FILE__, "helper_processes") {};
 bool send(string command, POSTDATA &args, string &response);
};

bool MessageClient::send(string command,POSTDATA &args,string &OUT_response)
{
 int TIMEOUT = 10;
 string str_uuid = (string)uuid.retrive(__FILE__, __LINE__);
 string IN_filepath = "MessageServer\\server\\queue\\" + str_uuid + "__request.txt";
 string OUT_filepath = "MessageServer\\server\\queue\\" + str_uuid + "__response.txt";
 LOG_EXTENSIVE_1(IN_filepath);
 LOG_EXTENSIVE_1(OUT_filepath);
 
 int handle=FileOpen(IN_filepath, FILE_WRITE|FILE_TXT);
 if(handle >= 0)
 {
  FileWrite(handle, "request=" + command);
  
  //--- separate args
   string k[],v[];
   int args_sz=args.copy(k,v);
   string _args="args=";
  
   for(int i=0; i<args_sz; i++)
    _args += k[i] + "\t" + v[i] + CharToString(ASCII_RECORD_SEPERATOR);
   
   _args = StringSubstr(_args, 0, StringLen(_args)-1);
    
   FileWrite(handle, _args);
  
  FileClose(handle);
  
  datetime now = TimeLocal();
  while(TimeLocal() - now < TIMEOUT)
  {
   if(FileIsExist(OUT_filepath))
   {
    LOG_DEBUG("[recv] response filepath=" + OUT_filepath);
    
    int OUT_handle=FileOpen(OUT_filepath, FILE_READ|FILE_TXT);
    
    if(OUT_handle >= 0)
    {
     bool b_success = false;
    
     while(!FileIsEnding(OUT_handle))
     {
      string line=FileReadString(OUT_handle);
     
      if(StringSubstr(line,0,7)=="status=")
      {
       string status=StringSubstr(line,7);
       
       if(status=="success")
        b_success=true;
       else if(status=="failed")
        b_success=false;
       else
        LOG_LOW("Could not find known status in response");
      }
     
      OUT_response += line + "\n";
     }
     
     FileClose(OUT_handle);
     
     if(b_success)
     {
      LOG_EXTENSIVE("Deleting " + OUT_filepath);
      if(!FileDelete(OUT_filepath))
       LOG_HIGH_1_ERROR("Could not delete " + OUT_filepath);
     }
     
     return b_success;
    }
    else
     LOG_HIGH_ERROR("Could not open OUT_filepath=" + OUT_filepath);
    
    break;
   }
   
   Sleep(300);
  }
  
  LOG_HIGH("timed out after " + (string)TIMEOUT + " seconds");
  
  LOG_DEBUG("Deleting " + IN_filepath);
  if(!FileDelete(IN_filepath))
    LOG_HIGH_1_ERROR("Could not delete " + IN_filepath);
 }
 else
  LOG_HIGH_ERROR("Could not open IN_filepath=" + IN_filepath);
  
 return false;
}