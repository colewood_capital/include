//+------------------------------------------------------------------+
//|                                                       Events.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#ifndef MAP
#define MAP
   #include <Map.mqh>
#endif 

class Events
{
private:
 Map<int, datetime> p_map_last_time;
 datetime p_last_time;
 
public:
 bool NewBar(int period);
 bool NewBar();  
 Events();
};

Events::Events(void)
{
 p_last_time = iTime(NULL, 0, 0);
}

//+------------------------------------------------------------------+
bool Events::NewBar(int period)
{
 if(p_map_last_time[period]==0)
  p_map_last_time.set(period, iTime(NULL, period, 0));
  
 if(iTime(NULL, period, 1)>=p_map_last_time[period])
  {
   p_map_last_time.set(period, iTime(NULL, period, 0));
   return(true);
  }
   
 return(false);
}
//+------------------------------------------------------------------+
bool Events::NewBar()
{
 if(iTime(NULL, 0, 1)>=p_last_time)
  {
   p_last_time=iTime(NULL, 0, 0);
   return(true);
  }
   
 return(false);
}