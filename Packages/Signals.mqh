//+------------------------------------------------------------------+
//|                                                      Signals.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#ifndef LOGGER 
#define LOGGER 
   #define LOGGER_DEPRECATED
   #include <Logger.mqh>
#endif 

#ifndef ENUMS
#define ENUMS
   #include <Super\Enums.mqh>
#endif 

#ifndef CANDLESTICKRECOGNITION
#define CANDLESTICKRECOGNITION
   #include <CandleStickRecognition.mqh>
#endif 

#ifndef ARRAYINT
#define ARRAYINT
   #include <Arrays/Arrayint.mqh>
#endif 

#ifndef ARRAYDOUBLE
#define ARRAYDOUBLE
   #include <Arrays/ArrayDouble.mqh>
#endif

#ifndef MATH
#define MATH
   #include <Packages/Math.mqh>
#endif 

#ifndef UTILS2
#define UTILS2
   #include <Utils2.mqh>
#endif 


class DrawdownRetraceExit
{
private:
 Logger logger;
 
protected:
 //--- adding tickets
  CArrayInt p_ticket;
  CArrayDouble p_close_price;
  CArrayDouble p_open_price;
  CArrayDouble p_max_drawdown_reached;
  CArrayInt p_cmd;
  CArrayInt p_drawdown_level_reached;
  
  int ticket_size;
 
 //--- settings
  CArrayDouble p_drawdown;
  CArrayDouble p_retrace;
 
 int drawdown_retrace_size;
 
 void check_drawdown(const CArrayInt& ticket, const CArrayDouble& bid, const CArrayDouble& ask, const CArrayDouble& open_price);
 void check_drawdown(double bid, double ask);
 
public:
 void tick_engine(double prc);
 void tick_engine(double bid, double ask);
 void remove(int ticket);
 void add(int ticket);
 void add_rung_to_ladder(double pip_drawdown_against_open_price, double exit_on_percentage_retrace_against_open_price);
 void backfill(const CArrayInt& tickets);
 
 DrawdownRetraceExit() : ticket_size(0), drawdown_retrace_size(0), logger(__FILE__) { p_drawdown.Sort(); }
};

void DrawdownRetraceExit::backfill(const CArrayInt &tickets)
{
 p_drawdown.Sort();
 
 CArrayDouble bid, ask, open_price;
 int total = tickets.Total();
 
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(tickets[i], SELECT_BY_TICKET))
  {
   datetime open_time = NULL;
   double b = MathLowestPrice(OrderOpenTime(), open_time, OrderSymbol(), (ENUM_TIMEFRAMES)Period());
   double a = MathHighestPrice(OrderOpenTime(), open_time, OrderSymbol(), (ENUM_TIMEFRAMES)Period());
 
   add(tickets[i]);
   bid.Add(b);
   ask.Add(a);
   open_price.Add(OrderOpenPrice());
  }
  else
   logger.log("Could not add #" + (string)tickets[i], LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
 }
 
 check_drawdown(tickets, bid, ask, open_price);
}

void DrawdownRetraceExit::check_drawdown(const CArrayInt& ticket, const CArrayDouble& bid, const CArrayDouble& ask, const CArrayDouble& open_price)
{
 int backorders_size = ticket.Total();
 
 for(int i=0; i<ticket_size; i++)
 {
  for(int k=0; k< backorders_size; k++)
  {
   if(p_ticket[i]==ticket[k])
   {
    for(int j=drawdown_retrace_size-1; j>=p_drawdown_level_reached[i]; j--)
    {
     if(p_cmd[i]==OP_BUY)
     {
      p_max_drawdown_reached.Update(i, open_price[k] - bid[k]); 
     
      if(p_open_price[i] - bid[k] >= p_drawdown[j] * Point * 10)
      {
       //--- increment level reached
        p_drawdown_level_reached.Update(i, j);
      
       //--- set price to close price
        double delta_drawdown = p_drawdown[j] * Point * 10;
        double cls_price = p_open_price[i] + ((p_retrace[j]-1) * p_max_drawdown_reached[i]);
        p_close_price.Update(i, cls_price);
      
       logger.log("#" + (string)p_ticket[i] + " reached drawdown lvl = " + (string)p_drawdown_level_reached[i] + ", close @ " + DoubleToStr(cls_price, 5) + ", type=OP_BUY, drawdown=" + DoubleToStr(p_drawdown[j], 2) + ", retrace=" + DoubleToStr(p_retrace[j], 2), LOG_TYPE_INFO, __LINE__, __FUNCTION__);
      } 
     }
   
     if(p_cmd[i]==OP_SELL)
     {
      p_max_drawdown_reached.Update(i, ask[k] - open_price[k]);
     
      if(ask[k] - p_open_price[i] >= p_drawdown[j] * Point * 10)
      {
       //--- increment level reached
        p_drawdown_level_reached.Update(i, j);
       
       //--- set price to close price
        double delta_drawdown = p_drawdown[j] * Point * 10;
        double cls_price = p_open_price[i] - ((p_retrace[j]-1) * p_max_drawdown_reached[i]);
        p_close_price.Update(i, cls_price);
      
       logger.log("#" + (string)p_ticket[i] + " reached drawdown lvl = " + (string)p_drawdown_level_reached[i] + ", close @ " + DoubleToStr(cls_price, 5) + ", type=OP_BUY, drawdown=" + DoubleToStr(p_drawdown[j], 2) + ", retrace=" + DoubleToStr(p_retrace[j], 2), LOG_TYPE_INFO, __LINE__, __FUNCTION__);
      } 
     }
    }
    
    break; //(p_ticket[i]==ticket[k])
   }
  }
 }
}

void DrawdownRetraceExit::check_drawdown(double bid, double ask)
{ 
 for(int i=0; i<ticket_size; i++)
 {
  double current_drawdown;
  
  if(p_cmd[i]==OP_BUY)
  {
   current_drawdown = p_open_price[i] - bid;
  }
  else if(p_cmd[i]==OP_SELL)
  {
   current_drawdown = ask - p_open_price[i];
  }
  else
  {
   logger.log((string)p_ticket[i] + ": cmd must be OP_BUY or OP_SELL, cmd=" + EnumToString((ENUM_TIMEFRAMES)p_cmd[i]), LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
   continue;
  }
  
  //--- Update max drawdown
  bool new_drawdown_reached=false, new_max=false;
  
  if(current_drawdown > p_max_drawdown_reached[i]) 
  {
   new_max = true;
   logger.log((string)p_ticket[i] + ": [NEW MAX] update max_drawdown: " + DoubleToStr(p_max_drawdown_reached[i], Digits) + " -> " + DoubleToStr(current_drawdown, Digits), LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
   p_max_drawdown_reached.Update(i, current_drawdown);
  } 
    
  for(int j=drawdown_retrace_size-1; j>=p_drawdown_level_reached[i]; j--)
  {
   if(p_drawdown_level_reached[i] != j && current_drawdown >=p_drawdown[j] * Point * 10) 
   {
    new_drawdown_reached = true;
    p_drawdown_level_reached.Update(i, j);
    logger.log("#" + (string)p_ticket[i] + " reached drawdown lvl = " + (string)p_drawdown_level_reached[i]+ ", drawdown=" + DoubleToStr(p_drawdown[j], 2) + ", retrace=" + DoubleToStr(p_retrace[j], 2), LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
   }
   
   if(new_drawdown_reached || (current_drawdown >= p_drawdown[j] * Point * 10 && new_max))
   {
    //--- set price to close price
     double cls_price=0;
     
     if(p_cmd[i]==OP_BUY)
      cls_price = p_open_price[i] + ((p_retrace[j]-1) * p_max_drawdown_reached[i]);
     
     else if(p_cmd[i]==OP_SELL)
      cls_price = p_open_price[i] + ((1-p_retrace[j]) * p_max_drawdown_reached[i]);
     
     else
      logger.log("Invalid ticket #" + (string)p_ticket[i] + " must be a market order, cmd=" + (string)p_cmd[i], LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
     
     p_close_price.Update(i, cls_price);
      
    logger.log("#" + (string)p_ticket[i] + ", close @ " + DoubleToStr(cls_price, 5) + ", type=" + EnumToString((ENUM_ORDER_TYPE)p_cmd[i]) + ", drawdown=" + DoubleToStr(p_drawdown[j], 2) + ", retrace=" + DoubleToStr(p_retrace[j], 2), LOG_TYPE_INFO, __LINE__, __FUNCTION__, true);
   } 
  }
 }
}





void DrawdownRetraceExit::tick_engine(double bid, double ask)
{
 for(int i=ticket_size-1; i>=0; i--)
 {
  if(OrderSelect(p_ticket[i], SELECT_BY_TICKET))
  {
   if(OrderCloseTime() > 0) {
    remove(i);
    continue;
   }
  
   if(p_close_price[i] > 0)
   {
    if(p_cmd[i]==OP_BUY)
    {
     if(bid >= p_close_price[i]) 
     {
      string log_msg = "bid(" + DoubleToStr(bid) + " >= target_cls_prc(" + DoubleToStr(p_close_price[i],Digits) + ")";
      TradeEngine::close(p_ticket[i], __LINE__, __FUNCTION__, log_msg, OrderLots(), bid);
     }
    }
  
    if(p_cmd[i]==OP_SELL)
    {
     if(ask <= p_close_price[i])
     {
      string log_msg = "ask(" + DoubleToStr(ask) + " <= target_cls_prc(" + DoubleToStr(p_close_price[i],Digits) + ")";
      TradeEngine::close(p_ticket[i], __LINE__, __FUNCTION__, log_msg, OrderLots(), ask);
     }
    }
   }
  }
  else
   logger.log("Could not select #" + (string)p_ticket[i], LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
 }

 check_drawdown(bid, ask);
}

void DrawdownRetraceExit::remove(int pos)
{
 if(pos >= 0) {
  p_ticket.Delete(pos);
  p_close_price.Delete(pos);
  p_open_price.Delete(pos);
  p_cmd.Delete(pos);
  p_drawdown_level_reached.Delete(pos);
  p_max_drawdown_reached.Delete(pos);
  ticket_size--;
 }
 else
  logger.log("Could not remove pos=" + (string)pos, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
}

void DrawdownRetraceExit::add(int ticket)
{
 int pos = p_ticket.SearchLinear(ticket);
 
 if(pos < 0) {
  if(OrderSelect(ticket, SELECT_BY_TICKET))
  {
   if(OrderSymbol() != Symbol())
   {
    logger.log("Cannot add #" + (string)ticket + " for order symbol(" + OrderSymbol() + ") != chart symbol(" + Symbol() + ")", LOG_TYPE_LOW, __LINE__, __FUNCTION__);
    return;
   }
  
   int type = OrderType();
   
   if(type==OP_BUY || type==OP_SELL)
   {
    p_ticket.Add(ticket); 
    p_close_price.Add(0);
    p_open_price.Add(OrderOpenPrice());
    p_cmd.Add(type);
    p_drawdown_level_reached.Add(0);
    p_max_drawdown_reached.Add(0);
    ticket_size++;
    
    logger.log("Added #" + (string)ticket, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
   }
   else
    logger.log("Can only add market orders", LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
  }
  else
   logger.log("Unable to select #" + (string)ticket, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
 }
 else
  logger.log("#" + (string)ticket + " already added", LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
}

void DrawdownRetraceExit::add_rung_to_ladder(double pip_drawdown_against_open_price,double exit_on_percentage_retrace_against_open_price)
{
  p_drawdown.InsertSort(pip_drawdown_against_open_price);
  
  int pos = p_drawdown.SearchLessOrEqual(pip_drawdown_against_open_price);
  
  p_retrace.Insert(exit_on_percentage_retrace_against_open_price, pos);
 
  drawdown_retrace_size++;
  
  logger.log("Adding tick_engineg to ladder, pip_drawdown=" + DoubleToStr(pip_drawdown_against_open_price, Digits) + ", percentage_retrace=" + DoubleToStr(exit_on_percentage_retrace_against_open_price, Digits), LOG_TYPE_INFO, __LINE__, __FUNCTION__);
}


class Signal
{
private:
 Logger logger;
 
protected:
 string p_symbol;
 ENUM_TIMEFRAMES p_timeframe;
 void subscribe(string name,int timeframe,string symbol);

public:
 virtual void subscribe(int timeframe,string symbol)=0;
 virtual bool tick_engine(ENUM_CHARTEVENT &signal, ENUM_TIMEFRAMES &timeframe)=0;
 Signal():logger(__FILE__) {};
};


void Signal::subscribe(string name,int timeframe,string symbol=NULL)
{
 p_timeframe = (ENUM_TIMEFRAMES)timeframe;
 p_symbol = (symbol==NULL) ? Symbol() : symbol;
 logger.log("Subscribed to signal [" + name + "](" + p_symbol + "), timeframe=" + (string)timeframe, LOG_TYPE_INFO, __LINE__, __FUNCTION__); 
}


class RetraceAndExtension : Signal 
{
protected:
 double p_best_price;
 double p_retrace_rate;
 double p_extension_rate;
 datetime p_time;
 double p_prc;
 int p_cmd;
 double p_threshold;
 bool p_is_retrace_signal_ready_to_fire;
 double p_extension_signal_prc;

 Logger logger;
 
public:
 void set(int cmd, datetime time, double prc, double min_pip_threshold, double retrace_rate, double extension_rate);
 void set(int ticket, double min_pip_threshold, double retrace_rate, double extension_rate);
 
 void subscribe(int timeframe, string symbol) { subscribe("RetraceAndExtension", timeframe, symbol); }
 bool tick_engine(ENUM_CHARTEVENT &signal, ENUM_TIMEFRAMES &timeframe, double bid, double ask);
 bool tick_engine(ENUM_CHARTEVENT &signal, ENUM_TIMEFRAMES &timeframe); 
 RetraceAndExtension() : p_best_price(0), p_retrace_rate(0), p_extension_signal_prc(-1), p_is_retrace_signal_ready_to_fire(false), logger(__FILE__) {};
};


bool RetraceAndExtension::tick_engine(ENUM_CHARTEVENT &signal,ENUM_TIMEFRAMES &timeframe)
{
 return tick_engine(signal, timeframe, Bid, Ask);
}


bool RetraceAndExtension::tick_engine(ENUM_CHARTEVENT &signal,ENUM_TIMEFRAMES &timeframe,double bid,double ask)
{
 if(p_best_price > 0)
 {
  if(p_cmd==OP_BUY)
  {
   if(bid >= p_best_price) {
    logger.log("Updating p_best_prc(" + DoubleToStr(p_best_price,Digits) + ") -> " + DoubleToStr(bid,Digits), LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
    p_best_price = bid;
    p_is_retrace_signal_ready_to_fire = true;
    double retrace_rate = NormalizeDouble(p_prc + ((p_best_price - p_prc) * p_retrace_rate), Digits);
    logger.log("Send RETRACE signal @ bid=" + DoubleToStr(retrace_rate,Digits), LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
   }

   double extension_prc = NormalizeDouble(p_extension_signal_prc, Digits);
   
   if(extension_prc > 0 && p_extension_rate > 0 && bid >= extension_prc)
   {
    logger.log("Extension reached, p_extension_rate=" + (string)extension_prc, LOG_TYPE_INFO, __LINE__, __FUNCTION__);
    
    p_extension_signal_prc = -1;
    
    signal = SIGNALS_EXTENSION;
    timeframe = p_timeframe;
    
    EventChartCustomBroadcast(p_symbol, signal, timeframe, p_prc, NULL);
   
    return true;
   }
   
   if(p_is_retrace_signal_ready_to_fire && p_retrace_rate > 0 
        && bid <= NormalizeDouble(p_prc + ((p_best_price - p_prc) * p_retrace_rate), Digits)  + DBL_EPSILON)
   { 
    signal = SIGNALS_RETRACE;
    timeframe = p_timeframe;
    
    p_is_retrace_signal_ready_to_fire = false;
    
    p_extension_signal_prc = p_prc + ((p_best_price - p_prc) * (1+p_extension_rate));
    
    logger.log("Retrace reached, retrace_rate=" + (string)p_retrace_rate + ", extension_rate=" + (string)p_extension_signal_prc, LOG_TYPE_INFO, __LINE__, __FUNCTION__);
    
    EventChartCustomBroadcast(p_symbol, signal, timeframe, p_prc, NULL);
    
    return true;
   }
  }
 
  if(p_cmd==OP_SELL)
  {
   if(bid <= p_best_price) {
    logger.log("Updating p_best_prc(" + DoubleToStr(p_best_price,Digits) + ") -> " + DoubleToStr(bid,Digits), LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
    p_best_price = bid;
    p_is_retrace_signal_ready_to_fire = true;
    double retrace_rate = NormalizeDouble(p_prc - ((p_prc - p_best_price) * p_retrace_rate), Digits);
    logger.log("Send RETRACE signal @ bid=" + DoubleToStr(retrace_rate,Digits), LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
   }
 
   double extension_prc = NormalizeDouble(p_extension_signal_prc, Digits);
    
   if(extension_prc > 0 && p_extension_rate > 0 && bid <= extension_prc)
   {
    logger.log("Extension reached, p_extension_rate=" + (string)extension_prc, LOG_TYPE_INFO, __LINE__, __FUNCTION__);
    
    p_extension_signal_prc = -1;
    
    signal = SIGNALS_EXTENSION;
    timeframe = p_timeframe;
    
    EventChartCustomBroadcast(p_symbol, signal, timeframe, p_prc, NULL);
   
    return true;
   }
   
   if(p_is_retrace_signal_ready_to_fire && p_retrace_rate > 0 
        && bid >= NormalizeDouble(p_prc - ((p_prc - p_best_price) * p_retrace_rate), Digits) - DBL_EPSILON)
   { 
    signal = SIGNALS_RETRACE;
    timeframe = p_timeframe;
    
    p_is_retrace_signal_ready_to_fire = false;
    
    p_extension_signal_prc = p_prc - ((p_prc - p_best_price) * (1+p_extension_rate));
    
    logger.log("Retrace reached, retrace_rate=" + (string)p_retrace_rate + ", extension_rate=" + (string)p_extension_signal_prc, LOG_TYPE_INFO, __LINE__, __FUNCTION__);
    
    EventChartCustomBroadcast(p_symbol, signal, timeframe, p_prc, NULL);
    
    return true;
   }
  }
 }
 else
 {
  if(p_cmd==OP_BUY)
  {
   if(bid >= p_prc + p_threshold)
   {
    p_best_price = bid;
    p_is_retrace_signal_ready_to_fire = true;
    logger.log("threshold crossed: prc=" + DoubleToStr(p_prc,Digits) + ", time=" + TimeToStr(p_time) + ", threshold=" + DoubleToStr(p_threshold,Digits) + ", best_prc=" + DoubleToStr(p_best_price, Digits), LOG_TYPE_INFO, __LINE__, __FUNCTION__);
   }
  }
 
  if(p_cmd==OP_SELL)
  {
   if(bid <= p_prc - p_threshold)
   {
    p_best_price = bid;
    p_is_retrace_signal_ready_to_fire = true;
    logger.log("threshold crossed: prc=" + DoubleToStr(p_prc,Digits) + ", time=" + TimeToStr(p_time) + ", threshold=" + DoubleToStr(p_threshold,Digits) + ", best_prc=" + DoubleToStr(p_best_price, Digits), LOG_TYPE_INFO, __LINE__, __FUNCTION__);
   }
  }
 }
 
 return false;
}


void RetraceAndExtension::set(int ticket, double min_pip_threshold, double retrace_rate, double extension_rate=0)
{
 if(OrderSelect(ticket,SELECT_BY_TICKET))
 {
  set(OrderType(), OrderOpenTime(), OrderOpenPrice(), min_pip_threshold, retrace_rate, extension_rate);
 }
}


void RetraceAndExtension::set(int cmd,datetime time,double prc,double min_pip_threshold,double retrace_rate,double extension_rate=0)
{
 p_time = time;
 p_prc = prc;
 p_cmd =cmd;
 p_retrace_rate=retrace_rate;
 p_extension_rate=extension_rate;
 
 double threshold = min_pip_threshold * Point * 10;
 p_threshold = threshold;
 
 //--- backfill
 if(p_time < TimeCurrent())
 {
  ENUM_CHARTEVENT s;
  ENUM_TIMEFRAMES tf;
  int start_index=iBarShift(p_symbol, p_timeframe, time); 
  
  LOG_INFO("Backfilling, start_index=" + (string)start_index);
  
  for(int i=start_index; i>=0; i--)
  {
   double open=iOpen(p_symbol, p_timeframe, i);
   double low=iLow(p_symbol, p_timeframe, i);
   double high=iHigh(p_symbol, p_timeframe, i);
   double close=iClose(p_symbol, p_timeframe, i);
    
   tick_engine(s, tf, open, open);
   tick_engine(s, tf, low, low);
   tick_engine(s, tf, high, high);
   tick_engine(s, tf, close, close);
  }
 }
 
 logger.log("Setting level, cmd=" + EnumToString((ENUM_ORDER_TYPE)cmd) + ", time=" + TimeToStr(time) + ", " + DoubleToStr(prc, Digits) + ", best_prc=" + DoubleToStr(p_best_price,Digits) + ", retrace_rate=" + DoubleToStr(retrace_rate, 3) + ", threshold=" + DoubleToStr(threshold,1), LOG_TYPE_INFO, __LINE__, __FUNCTION__);
}


class Fractal : public Signal
{
protected:
 datetime p_last_upper_timestamp;
 datetime p_last_lower_timestamp;
 
public:
 virtual void subscribe(ENUM_TIMEFRAMES timeframe,string symbol);
 virtual bool tick_engine(ENUM_CHARTEVENT &signal, ENUM_TIMEFRAMES &timeframe);
};

void Fractal::subscribe(ENUM_TIMEFRAMES timeframe,string symbol=NULL)
{ 
 p_last_lower_timestamp = TimeCurrent();
 p_last_upper_timestamp = TimeCurrent();
 
 Signal::subscribe("Fractal", timeframe, symbol);
}

bool Fractal::tick_engine(ENUM_CHARTEVENT &signal,ENUM_TIMEFRAMES &timeframe)
{
 int upper_bar, lower_bar;
 datetime tstamp;
 
 //--- check upper fractal
  double prevUpperFractal = prevFractal(1, upper_bar, MODE_UPPER, p_timeframe, p_symbol);
  tstamp = iTime(p_symbol, p_timeframe, upper_bar);
 
  if(tstamp > p_last_upper_timestamp)
  {
   signal = SIGNALS_FRACTAL_UPPER;
   timeframe = p_timeframe;
   p_last_upper_timestamp = tstamp;
   
   EventChartCustomBroadcast(p_symbol, signal, timeframe, 0, NULL);
   
   return true;
  }
 
 //--- check lower fractal
  double prevLowerFractal = prevFractal(1, lower_bar, MODE_LOWER, p_timeframe, p_symbol);
  tstamp = iTime(p_symbol, p_timeframe, lower_bar);
  
  if(tstamp > p_last_lower_timestamp)
  {
   signal = SIGNALS_FRACTAL_LOWER;
   timeframe = p_timeframe;
   p_last_lower_timestamp = tstamp;
   
   EventChartCustomBroadcast(p_symbol, signal, timeframe, 0, NULL);
   
   return true;
  }
  
 signal = SIGNALS_FRACTAL_NIL;
 return false;
}


class Psar
{
 private:
  double p_min_pips[];
  double p_step[];
  double p_maximum[];
  ENUM_TIMEFRAMES p_timeframe[];
  datetime p_last_timestamp[];
  int p_count;
  Logger logger;
 
 public:
  void subscribe(ENUM_TIMEFRAMES timeframe, double min_pips, double step, double maximum);
  int _tick_engine(ENUM_CHARTEVENT& signal_type[], ENUM_TIMEFRAMES& timeframe[]);
  bool tick_engine(ENUM_CHARTEVENT &signal, ENUM_TIMEFRAMES &timeframe);
  double min_pips() { return p_min_pips[0]; }
  Psar() : p_count(0), logger(__FILE__) {};
};

class Candlestick
{
private:
 ENUM_CANDLESTICK p_candlestick[];
 int p_candle_indicator_buffer[];
 ENUM_TIMEFRAMES p_timeframe[];
 int p_side[];
 int p_count;
 Logger logger;
 
public:
 void subscribe(ENUM_TIMEFRAMES timeframe, ENUM_CANDLESTICK candlestick);
 bool tick_engine();
 Candlestick() : p_count(0), logger(__FILE__)  {};
};

void Candlestick::subscribe(ENUM_TIMEFRAMES timeframe, ENUM_CANDLESTICK candlestick)
{
 timeframe = (timeframe==PERIOD_CURRENT) ? (ENUM_TIMEFRAMES)Period() : timeframe;
 
 if(p_count > 0) {
  logger.log("Can only subscribe to one signal per Candlestick object", LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
  return;
 }

 p_count++;
 
 ArrayResize(p_candlestick, p_count);
 ArrayResize(p_candle_indicator_buffer, p_count);
 ArrayResize(p_timeframe, p_count);
 ArrayResize(p_side, p_count);

 p_timeframe[p_count-1] = timeframe;
 
 switch(candlestick)
 {
  case CANDLESTICK_STRONG_TREND_HIGHER:
     p_candlestick[p_count-1] = CANDLESTICK_STRONG_TREND_HIGHER;
     p_candle_indicator_buffer[p_count-1] = 0;
     p_side[p_count-1] = OP_BUY;
     break;
  
  case CANDLESTICK_DOJI:
     p_candlestick[p_count-1] = CANDLESTICK_DOJI;
     p_candle_indicator_buffer[p_count-1] = 1;
     p_side[p_count-1] = OP_BUY;
     break;
  
  case CANDLESTICK_UP_PINBAR_HIGHER:
     p_candlestick[p_count-1] = CANDLESTICK_UP_PINBAR_HIGHER;
     p_candle_indicator_buffer[p_count-1] = 2;
     p_side[p_count-1] = OP_BUY;
     break;
  
  case CANDLESTICK_DOWN_PINBAR_HIGHER:
     p_candlestick[p_count-1] = CANDLESTICK_DOWN_PINBAR_HIGHER;
     p_candle_indicator_buffer[p_count-1] = 3;
     p_side[p_count-1] = OP_BUY;
     break;
  
  case CANDLESTICK_UP_CONSOLIDATION_HIGHER:
     p_candlestick[p_count-1] = CANDLESTICK_UP_CONSOLIDATION_HIGHER;
     p_candle_indicator_buffer[p_count-1] = 4;
     p_side[p_count-1] = OP_BUY;
     break;
  
  case CANDLESTICK_DOWN_CONSOLIDATION_HIGHER:
     p_candlestick[p_count-1] = CANDLESTICK_DOWN_CONSOLIDATION_HIGHER;
     p_candle_indicator_buffer[p_count-1] = 5;
     p_side[p_count-1] = OP_BUY;
     break;
  
  case CANDLESTICK_WEAK_TREND_HIGHER:
     p_candlestick[p_count-1] = CANDLESTICK_WEAK_TREND_HIGHER;
     p_candle_indicator_buffer[p_count-1] = 6;
     p_side[p_count-1] = OP_BUY;
     break;
  
  case CANDLESTICK_STRONG_TREND_LOWER:
     p_candlestick[p_count-1] = CANDLESTICK_STRONG_TREND_LOWER;
     p_candle_indicator_buffer[p_count-1] = 0;
     p_side[p_count-1] = OP_SELL;
     break;
  
  case CANDLESTICK_UP_PINBAR_LOWER:
     p_candlestick[p_count-1] = CANDLESTICK_UP_PINBAR_LOWER;
     p_candle_indicator_buffer[p_count-1] = 2;
     p_side[p_count-1] = OP_SELL;
     break;
  
  case CANDLESTICK_DOWN_PINBAR_LOWER:
     p_candlestick[p_count-1] = CANDLESTICK_DOWN_PINBAR_LOWER;
     p_candle_indicator_buffer[p_count-1] = 3;
     p_side[p_count-1] = OP_SELL;
     break;
  
  case CANDLESTICK_UP_CONSOLIDATION_LOWER:
     p_candlestick[p_count-1] = CANDLESTICK_UP_CONSOLIDATION_LOWER;
     p_candle_indicator_buffer[p_count-1] = 4;
     p_side[p_count-1] = OP_SELL;
     break;
  
  case CANDLESTICK_DOWN_CONSOLIDATION_LOWER:
     p_candlestick[p_count-1] = CANDLESTICK_DOWN_CONSOLIDATION_LOWER;
     p_candle_indicator_buffer[p_count-1] = 5;
     p_side[p_count-1] = OP_SELL;
     break;
  
  case CANDLESTICK_WEAK_TREND_LOWER:
     p_candlestick[p_count-1] = CANDLESTICK_WEAK_TREND_LOWER;
     p_candle_indicator_buffer[p_count-1] = 6;
     p_side[p_count-1] = OP_SELL;
     break;  
 }
 
 logger.log("Subscribed to " + EnumToString(p_candlestick[p_count-1]) + ", count=" + (string)p_count, LOG_TYPE_INFO, __LINE__, __FUNCTION__);
}

bool Candlestick::tick_engine()
{
 const int SHIFT = 1;
 bool success = false;
 string b0, b1, b2, b3, b4, b5, b6;
 
 //RefreshRates(); Print(__FUNCTION__, " RefreshRates()");
 
 //--- log CandleStick Recognition @ t=0
  b0 = (string)iCustom(NULL, p_timeframe[0], "Candlestick Recognition", clrWhite, 0, 0);
  b1 = (string)iCustom(NULL, p_timeframe[0], "Candlestick Recognition", clrWhite, 1, 0);
  b2 = (string)iCustom(NULL, p_timeframe[0], "Candlestick Recognition", clrWhite, 2, 0);
  b3 = (string)iCustom(NULL, p_timeframe[0], "Candlestick Recognition", clrWhite, 3, 0);
  b4 = (string)iCustom(NULL, p_timeframe[0], "Candlestick Recognition", clrWhite, 4, 0);
  b5 = (string)iCustom(NULL, p_timeframe[0], "Candlestick Recognition", clrWhite, 5, 0);
  b6 = (string)iCustom(NULL, p_timeframe[0], "Candlestick Recognition", clrWhite, 6, 0);
  logger.log("SHIFT=0" + ", TIMEFRAME=" + EnumToString(p_timeframe[0]) + ", Candlestick buffer[0-6]: " + b0 + ", " + b1 + ", " + b2 + ", " + b3 + ", " + b4 + ", " + b5 + ", " + b6, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__, true); 
 
 //--- log CandleStick Recognition @ t=SHIFT
  b0 = (string)iCustom(NULL, p_timeframe[0], "Candlestick Recognition", clrWhite, 0, SHIFT);
  b1 = (string)iCustom(NULL, p_timeframe[0], "Candlestick Recognition", clrWhite, 1, SHIFT);
  b2 = (string)iCustom(NULL, p_timeframe[0], "Candlestick Recognition", clrWhite, 2, SHIFT);
  b3 = (string)iCustom(NULL, p_timeframe[0], "Candlestick Recognition", clrWhite, 3, SHIFT);
  b4 = (string)iCustom(NULL, p_timeframe[0], "Candlestick Recognition", clrWhite, 4, SHIFT);
  b5 = (string)iCustom(NULL, p_timeframe[0], "Candlestick Recognition", clrWhite, 5, SHIFT);
  b6 = (string)iCustom(NULL, p_timeframe[0], "Candlestick Recognition", clrWhite, 6, SHIFT);
  logger.log("SHIFT=" + (string)SHIFT + ", TIMEFRAME=" + EnumToString(p_timeframe[0]) + ", Candlestick buffer[0-6]: " + b0 + ", " + b1 + ", " + b2 + ", " + b3 + ", " + b4 + ", " + b5 + ", " + b6, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__, true); 
 
 logger.log("[ALGO] SHIFT=0: " + EnumToString(algo(p1(0), p2(0))) + ", SHIFT=1: " + EnumToString(algo(p1(1), p2(1))), LOG_TYPE_DEBUG, __LINE__, __FUNCTION__, true);
 
 for(int i=0; i<p_count; i++)
 {
  ENUM_CANDLESTICK candlestick = CandlestickRecognition(SHIFT, p_timeframe[i]);
  
  logger.log("Fire on match candlestick=" + EnumToString(candlestick) + " w/ " + EnumToString(p_candlestick[i]), LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
  
  if(candlestick == p_candlestick[i])
  {
   logger.log("Firing " + EnumToString(p_candlestick[i]) + ", timeframe=" + EnumToString((ENUM_TIMEFRAMES)p_timeframe[i]), LOG_TYPE_INFO, __LINE__, __FUNCTION__, true);
   return true;
  }
 }
 
 return false;
}

void Psar::subscribe(ENUM_TIMEFRAMES timeframe,double min_pips,double step,double maximum)
{
 if(timeframe==NULL)
  timeframe = (ENUM_TIMEFRAMES)Period();
  
 p_count++;
 ArrayResize(p_min_pips, p_count);
 ArrayResize(p_step, p_count);
 ArrayResize(p_maximum, p_count);
 ArrayResize(p_timeframe, p_count);
 ArrayResize(p_last_timestamp, p_count);
 
 p_timeframe[p_count-1] = timeframe;
 p_min_pips[p_count-1] = min_pips;
 p_step[p_count-1] = step;
 p_maximum[p_count-1] = maximum;
 p_last_timestamp[p_count-1] = iTime(NULL, timeframe, 0);
}


int Psar::_tick_engine(ENUM_CHARTEVENT& signal_type[], ENUM_TIMEFRAMES& timeframe[])
{
 int count = 0;
 
 for(int i=0; i<p_count; i++)
 {
  bool match = false;
  
  double current = iSAR(NULL, p_timeframe[i], p_step[i], p_maximum[i], 0);
  double past = iSAR(NULL, p_timeframe[i], p_step[i], p_maximum[i], 1);
  double close = iClose(NULL, p_timeframe[i], 0);
  double past_close = iClose(NULL, p_timeframe[i], 1);
  datetime bar_open_time = iTime(NULL, p_timeframe[i], 0);
  double point = MarketInfo(NULL, MODE_POINT);
  int side = -1;
  ENUM_CHARTEVENT signal = SIGNALS_PSAR_NIL;

  //Extensive debug
  //logger.log("bar_open_time=" + TimeToStr(bar_open_time) + " .. last_tstamp=" + TimeToStr(p_last_timestamp[i]) + " .. past=" + DoubleToStr(past,Digits) + " .. current=" + DoubleToStr(current,Digits) + " .. close=" + DoubleToStr(close,Digits) + " .. past_close=" + DoubleToStr(close,Digits), LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);

  //--- check psar logic
  if(bar_open_time > p_last_timestamp[i] && past >= past_close && current <= close) {
   signal = SIGNALS_PSAR_TOUCH_ABOVE;
   side = OP_SELL;
   match = true;
  }
  
  if(bar_open_time > p_last_timestamp[i] && past <= past_close && current >= close) {
   signal = SIGNALS_PSAR_TOUCH_BELOW;
   side = OP_BUY;
   match = true;
  }
  
  if(match) {
   //--- make sure range is >= min set pips
    datetime ts;
    datetime past_tstamp = PsarStartingTime(side);
    logger.log("Using past_tstamp=" + TimeToStr(past_tstamp) + " for calculating highest/lowest price", LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
    double highest = MathHighestPrice(past_tstamp, ts, NULL, p_timeframe[i]);
    double lowest = MathLowestPrice(past_tstamp, ts, NULL, p_timeframe[i]);
  
    double range = highest - lowest;
    double min_range = p_min_pips[i] * Point * 10;
    if(range < min_range)
    {
     logger.log("Do not send PSAR signal for range(" + DoubleToStr(range, Digits) + ") < min_range(" + DoubleToStr(min_range, Digits) + ", highest=" + DoubleToStr(highest, Digits) + ", lowest=" + DoubleToStr(lowest, Digits), LOG_TYPE_INFO, __LINE__, __FUNCTION__);
     continue;
    }
  
   //--- add signal
    count++;
    ArrayResize(signal_type, count);
    ArrayResize(timeframe, count);
    signal_type[count-1] = signal;
    timeframe[count-1] = p_timeframe[i];
    p_last_timestamp[i] = bar_open_time;
   
    logger.log("Firing " + EnumToString(signal) + ", timeframe=" + EnumToString(timeframe[count-1]), LOG_TYPE_INFO, __LINE__, __FUNCTION__);
  }
 }

 return count;
}


bool Psar::tick_engine(ENUM_CHARTEVENT &signal, ENUM_TIMEFRAMES &timeframe)
{
 ENUM_CHARTEVENT signal_type[];
 ENUM_TIMEFRAMES tf[];
 
 int count = _tick_engine(signal_type, tf);
 
 if(count > 0)
 {
  for(int i=0; i<count; i++)
  {
   EventChartCustomBroadcast(Symbol(), signal_type[i], tf[i], 0, NULL);
  }
  
  timeframe = tf[0];
  signal = signal_type[0];
  return true;
 }
 
 return false;
}
