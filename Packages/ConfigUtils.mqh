//+------------------------------------------------------------------+
//|                                                  ConfigUtils.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#ifndef ARRAYSTRING
#define ARRAYSTRING
   #include <Arrays\ArrayString.mqh>
#endif 


void ConfigFileLoader(string filename, CArrayString& variables, CArrayString& values, 
                       const string& expected[], string& not_found[], bool warn_only=False)
                       
/*
Delimiter set to '='
For each variable = value pair, white space is stripped
Returned arrays variables[] and values[] are of equal size ORDERED BY expected[]
Any variable not in expected[] will be excluded, regardless if if found in config file, unless warn_only=True
Returned array not_found[] lists any variable found in expected[], but not found in the config file
*/
{
 string buffer_variables[];
 string buffer_values[];
 
  int handle = FileOpen(filename,FILE_TXT|FILE_READ|FILE_SHARE_READ);

  if(handle >=0)
  {
   while(!FileIsEnding(handle))
   {
    string result[];
    string line = FileReadString(handle);
    
    //Skip comment line
    if(StringFind(line,"#") == 0)
      continue;
    
    StringSplit(line,'=',result);
    if(ArraySize(result) == 2)
    {
     int size = ArraySize(buffer_variables);
     StringReplace(result[0]," ","");
     StringReplace(result[1]," ","");
     ArrayResize(buffer_variables,size+1);
     ArrayResize(buffer_values,size+1);
     buffer_variables[size] = result[0];
     buffer_values[size] = result[1];
    }
   }
   
   FileClose(handle);
  }
  else {
   Print("[HIGH]", __FILE__, "::", __FUNCTION__, "(", __LINE__, ")::", "Could not load " + filename);
   ExpertRemove();
  }
 
 CArrayString inspect;
 inspect.AddArray(expected);
 int size = ArraySize(buffer_variables);
 for(int i=0; i<size; i++)
 {
  int pos = inspect.SearchLinear(buffer_variables[i]);

  if(pos>=0)
  {
   variables.Add(buffer_variables[i]);
   values.Add(buffer_values[i]);
   inspect.Delete(pos);
  }
  else
  {
   if(warn_only)
   {
    variables.Add(buffer_variables[i]);
    values.Add(buffer_values[i]);
   }
  }
 }
 
 int remaining = inspect.Total();
 for(int i=0; i<remaining; i++)
 {
  ArrayResize(not_found, ArraySize(not_found)+1);
  not_found[i] = inspect[i];
 }
}