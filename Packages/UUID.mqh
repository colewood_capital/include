//+------------------------------------------------------------------+
//|                                                         UUID.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#ifndef MAP
#define MAP
   #include <Map.mqh>
#endif

#ifndef ARRAYINT
#define ARRAYINT
   #include <Arrays/ArrayInt.mqh>
#endif 

#ifndef ARRAYSTRING
#define ARRAYSTRING
   #include <Arrays/ArrayString.mqh>
#endif

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
#endif

class UUIDGeneratorIncreasing
{
 private:
  uint counter;
  
 public:
  uint retrieve() { return ++counter; }
};

class UUIDGenerator
{
 private:
  CArrayInt p_uuid_bank;
  CArrayString p_file;
  CArrayInt p_line;
  CArrayInt p_for_loop_index;
  Logger logger;
  int counter;
 
 public:
  uint retrive(string __FILE__macro, int __LINE__macro, int i=1);
  UUIDGenerator() : logger(__FILE__, "helper_processes") {};
};

uint UUIDGenerator::retrive(string __FILE__macro,int __LINE__macro, int i)
{
 for(int k=0; k<counter; k++)
 {
  if(p_file[k]==__FILE__macro && p_line[k]==__LINE__macro && p_for_loop_index[k]==i)
  {
   return p_uuid_bank[k];
  }
 }
 
 counter++;
 
 p_uuid_bank.Add(counter);
 p_file.Add(__FILE__macro);
 p_line.Add(__LINE__macro);
 p_for_loop_index.Add(i);
 
 logger.log(StringConcatenate("Assigned uuid=", counter, " to [file=", __FILE__macro, ", line=", __LINE__macro, ", i=", i, "]"), LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
 
 return counter;
}

class UUIDNameGenerator
{
 private:
  uint p_counter;
  Map<string, uint> p_name_dict;
  uint get(string name);
  
 public:
  UUIDNameGenerator() : p_counter(0) {};
  uint operator[](string name) { return get(name); }
};

uint UUIDNameGenerator::get(string name)
{
 if(p_name_dict.find(name))
 {
  return p_name_dict[name];
 }
 
 p_counter++;
 p_name_dict.set(name, p_counter);
 return p_counter;;
}
