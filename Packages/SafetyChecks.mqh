//+------------------------------------------------------------------+
//|                                                 SafetyChecks.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
#endif 

#ifndef CONFIG
#define CONFIG
   #include <Packages\Config.mqh>
#endif 

#ifndef ARRAYLIST
#define ARRAYLIST
   #include <Arrays\ArrayList.mqh>
#endif 

#ifndef MAP
#define MAP
   #include <Map.mqh>
#endif 

//EXTERN(double, ACCOUNT_EQUITY_MINIMUM);

enum ENUM_SAFETY_CHECK_EQUITY { 
            CHECK_EQUITY_NIL,
            CHECK_EQUITY_PERCENT_CHANGE_LESS_THAN,
            CHECK_EQUITY_PERCENT_CHANGE_GREATER_THAN
          };

struct SafetyCheckEquity
{ 
 double starting_equity;
 datetime start_time;
 double percentage;
 double time_in_seconds;
 string action;
 ENUM_SAFETY_CHECK_EQUITY type;
 
 string repr() { return StringConcatenate(EnumToString(type)," - ", percentage*100, "% - ", time_in_seconds/60.0, " mins"); }
};


struct SafetyCheckVolume
{
 string symbol_set;
 datetime start_time;
 double lots;
 double time_in_seconds;
 string action;
 
 string repr() { return StringConcatenate("'", symbol_set, "' - ", lots, " lots in ", time_in_seconds/60.0, " minutes"); }
};


struct SafetyCheckPositionSize
{
 string symbol_set;
 double lots;
 string action;
};

class SafetyChecks
{
private:
 Logger logger;
 
protected:
 SafetyCheckEquity p_eq[];
 SafetyCheckVolume p_vol[];
 SafetyCheckPositionSize p_pos[];
 int p_eq_count, p_vol_count, p_pos_count;
 Map<string,string> p_sym_dict;
 
 void load_equity_config(string base_directory);
 void load_volume_config(string base_directory);
 void load_position_config(string base_directory);
 void load_symbol_sets(string base_directory);
 string get_symbol_set(string symbol);
 
 void reset_equity(int index);
 void reset_volume(int index);
 
 void calculate_position_size(const string symbol, double &OUT_open_lots);
 void calculate_volume(const string symbol, double &OUT_open_lots, double &OUT_closed_lots, const datetime begin_time);
 datetime most_recent_trade_time();

public:
 SafetyChecks(string base_directory);
 bool check_equity(ENUM_SAFETY_CHECK_EQUITY &type, double &time_in_minutes, double &percentage_change, double &old_equity, string &action);
 //bool check_absolute_equity();
 bool check_volume(string &symbol, double &lots, double &time_in_minutes, string &action);
 bool check_positions(string &symbol, double &lots, string &action);
};

/*
bool SafetyChecks::check_absolute_equity()
{
 if(AccountEquity() > ACCOUNT_EQUITY_MINIMUM)
  return false;
 
 return true;
}
*/

datetime SafetyChecks::most_recent_trade_time()
{
 datetime max=0;
 
 if(OrderSelect(OrdersTotal()-1, SELECT_BY_POS, MODE_TRADES))
  max=OrderOpenTime();
  
 if(OrderSelect(OrdersHistoryTotal()-1, SELECT_BY_POS, MODE_HISTORY))
  max=MathMax(max, OrderOpenTime());
  
 return max;
}


void SafetyChecks::calculate_position_size(const string symbol, double &OUT_open_lots)
{
 OUT_open_lots=0.0;
 
 //--- query open trades
  int _open_orders_total=OrdersTotal();
  for(int k=_open_orders_total-1; k>=0; k--)
  {
   if(OrderSelect(k,SELECT_BY_POS,MODE_TRADES) && OrderSymbol()==symbol)
   {
    int type=OrderType();
    
    if(type==OP_BUY)
     OUT_open_lots+=OrderLots();
  
    if(type==OP_SELL)
     OUT_open_lots-=OrderLots();
   }
  }
}


void SafetyChecks::calculate_volume(const string symbol,double &OUT_open_lots,double &OUT_closed_lots,const datetime begin_time)
{
  OUT_open_lots=0.0;
  OUT_closed_lots=0.0;
 
 //--- query closed trades
  int history_total=OrdersHistoryTotal();
  for(int k=history_total-1; k>=0; k--)
  {
   if(OrderSelect(k,SELECT_BY_POS,MODE_HISTORY) && OrderSymbol()==symbol)
   {  
    if(OrderCloseTime() < begin_time)
     break;
    else
     OUT_closed_lots+=OrderLots();
   
    if(OrderOpenTime() >= begin_time)
     OUT_closed_lots+=OrderLots();
   }
  }
   
 //--- query open trades
  int _open_orders_total=OrdersTotal();
  for(int k=_open_orders_total-1; k>=0; k--)
  {
   if(OrderSelect(k,SELECT_BY_POS,MODE_TRADES) && OrderSymbol()==symbol)
   {
    if(OrderOpenTime() < begin_time)
     break;
    
    OUT_open_lots+=OrderLots();
   }
  }
}


bool SafetyChecks::check_positions(string &symbol,double &lots,string &action)
{
 __TRACE__

 string order_symbol=NULL;
 bool position_size_alert_triggered=false;
 double total_open_lots=0.0;
 
 for(int i=0; i<p_pos_count; i++)
 {
  if(p_pos[i].symbol_set=="default" || p_pos[i].symbol_set=="DEFAULT")
  {
   //--- find all symbols in order and order history queue
    CArrayString symb;
    int orders_total=OrdersTotal();
    int history_total=OrdersHistoryTotal();
    
    for(int t=0; t<orders_total; t++)
    {
     if(OrderSelect(t,SELECT_BY_POS,MODE_TRADES))
     {
      if(symb.SearchLinear(OrderSymbol()) < 0)
       symb.Add(OrderSymbol());
     }
    }
    
   //--- check position size for all open symbols
    int symbols_total=symb.Total();
    
    for(int t=0; t<symbols_total; t++)
    {
     order_symbol=symb[t];
     calculate_position_size(order_symbol, total_open_lots); 

     //--- 
      if(MathAbs(total_open_lots) + DBL_EPSILON >= p_pos[i].lots)
      {
       position_size_alert_triggered=true;
       break;
      }
    }
  }
  else
  {
   int sz;
   string sym[], sym_set[];
   sz=p_sym_dict.copy(sym, sym_set);
  
   for(int j=0; j<sz; j++)
   {
    if(sym_set[j]==p_pos[i].symbol_set)
    {
     //--- calculate volume for given symbol (for all symbols listed in symbol set configuration)
      order_symbol=sym[j];
      calculate_position_size(order_symbol, total_open_lots); 
    
     //--- 
      if(MathAbs(total_open_lots) + DBL_EPSILON >= p_pos[i].lots)
      {
       position_size_alert_triggered=true;
       break;
      }
    }
   }
  }
  
  if(position_size_alert_triggered)
  {
   LOG_HIGH("[SAFETY] " + order_symbol + " position size reached " + (string)total_open_lots + " lot(s)=" + (string)total_open_lots);
   symbol = order_symbol;
   lots = p_pos[i].lots;
   action = p_pos[i].action;
   return true;
  }
 }
 
 
 return false;
}

bool SafetyChecks::check_volume(string &symbol,double &lots,double &time_in_minutes,string &action)
{
 __TRACE__
 
 string order_symbol=NULL;
 bool volume_alert_triggered=false;
 double total_open_lots=0.0, total_closed_lots=0.0, total_lots=0.0;
 
 for(int i=0; i<p_vol_count; i++)
 {
  if(p_vol[i].symbol_set=="default" || p_vol[i].symbol_set=="DEFAULT")
  {
   //--- find all symbols in order and order history queue
    CArrayString symb;
    int orders_total=OrdersTotal();
    int history_total=OrdersHistoryTotal();
    
    for(int t=0; t<orders_total; t++)
    {
     if(OrderSelect(t,SELECT_BY_POS,MODE_TRADES))
     {
      if(OrderCloseTime() < p_vol[i].start_time)
       break;
    
      if(symb.SearchLinear(OrderSymbol()) < 0 && OrderOpenTime() >= p_vol[i].start_time)
       symb.Add(OrderSymbol());
     }
    }
    
    for(int t=0; t<history_total; t++)
    {
     if(OrderSelect(t,SELECT_BY_POS,MODE_HISTORY))
      if(symb.SearchLinear(OrderSymbol()) < 0  && OrderOpenTime() >= p_vol[i].start_time)
       symb.Add(OrderSymbol());
    }
    
   //--- check value for all symbols traded
    int symbols_total=symb.Total();
    
    for(int t=0; t<symbols_total; t++)
    {
     order_symbol=symb[t];
     calculate_volume(order_symbol, total_open_lots, total_closed_lots, p_vol[i].start_time); 

     //--- 
      total_lots = total_open_lots + total_closed_lots;
      if(total_lots + DBL_EPSILON >= p_vol[i].lots)
      {
       volume_alert_triggered=true;
       break;
      }
    }
  }
  else
  {
   int sz;
   string sym[], sym_set[];
   sz=p_sym_dict.copy(sym, sym_set);
  
   for(int j=0; j<sz; j++)
   {
    if(sym_set[j]==p_vol[i].symbol_set)
    {
     //--- calculate volume for given symbol (for all symbols listed in symbol set configuration)
      order_symbol=sym[j];
      calculate_volume(order_symbol, total_open_lots, total_closed_lots, p_vol[i].start_time); 
    
     //--- 
      total_lots = total_open_lots + total_closed_lots;
      if(total_lots >= p_vol[i].lots)
      {
       volume_alert_triggered=true;
       break;
      }
    }
   }
  }

  if(volume_alert_triggered)
  {
   LOG_HIGH("[SAFETY] " + order_symbol + " volume traded reached " + (string)total_lots + " lot(s) traded (open=" + (string)total_open_lots + ", closed=" + (string)total_closed_lots + ") within " + DoubleToStr(p_vol[i].time_in_seconds / 60.0, 1) + " minutes");
   symbol = order_symbol;
   lots = p_vol[i].lots;
   action = p_vol[i].action;
   time_in_minutes = p_vol[i].time_in_seconds / 60.0;
   __ reset_volume(i);
   return true;
  }
 }
 
 return false;
}


string SafetyChecks::get_symbol_set(string symbol)
{
 __TRACE__
 
 string symbol_set=p_sym_dict[symbol];
 
 if(symbol_set!=NULL && symbol_set!="")
  return symbol_set;
 else
  return "default";
}


void SafetyChecks::reset_volume(int index)
{
 __TRACE__
 
 p_vol[index].start_time = MathMax(TimeCurrent(), most_recent_trade_time());
 LOG_INFO("[SAFETY] reset start time of volume level: " + p_vol[index].repr() + " to " + TimeToStr(p_vol[index].start_time, TIME_DATE|TIME_MINUTES|TIME_SECONDS) + ", index=" + (string)index);
}


void SafetyChecks::reset_equity(int index)
{
 __TRACE__
 
 p_eq[index].start_time = TimeLocal();
 p_eq[index].starting_equity = AccountEquity();
 LOG_INFO("[SAFETY] reset start time and equity of level: " + p_eq[index].repr() + " to " + TimeToStr(p_eq[index].start_time, TIME_DATE|TIME_MINUTES|TIME_SECONDS) + " and " + (string) p_eq[index].starting_equity + " respectively, index=" + (string)index);
}


bool SafetyChecks::check_equity(ENUM_SAFETY_CHECK_EQUITY &type,double &percentage_change,double &time_in_minutes,double &old_equity,string &action)
{
  __TRACE__
 
 //--- check for equity reset
  //-- NOT TESTED (UNIT TEST)
  for(int i=0; i<p_eq_count; i++)
  {
   if(TimeLocal()-p_eq[i].start_time > p_eq[i].time_in_seconds) {
    __ reset_equity(i);
   }
  }
 
 //--- check if equity threshold crossed
  double equity=AccountEquity();
  
  for(int i=0; i<p_eq_count; i++)
  {
   if(p_eq[i].type==CHECK_EQUITY_PERCENT_CHANGE_GREATER_THAN &&
       equity >= p_eq[i].starting_equity * (1+p_eq[i].percentage))
   {
    type=CHECK_EQUITY_PERCENT_CHANGE_GREATER_THAN;
    percentage_change=p_eq[i].percentage;
    action=p_eq[i].action;
    time_in_minutes=p_eq[i].time_in_seconds / 60.0;
    old_equity = p_eq[i].starting_equity;
    __ reset_equity(i);
    return true;
   }
   
   if(p_eq[i].type==CHECK_EQUITY_PERCENT_CHANGE_LESS_THAN &&
       equity <= p_eq[i].starting_equity * (1-p_eq[i].percentage))
   {
    type=CHECK_EQUITY_PERCENT_CHANGE_LESS_THAN;
    percentage_change=p_eq[i].percentage;
    action=p_eq[i].action;
    time_in_minutes=p_eq[i].time_in_seconds / 60.0;
    old_equity = p_eq[i].starting_equity;
    __ reset_equity(i);
    return true;
   }
  }
  
 return false;
}


void SafetyChecks::load_symbol_sets(string base_directory)
{
 __TRACE__
 
 string readme = "alias, symobl_list";
 
 string filename= base_directory+"\\safeties\\symbol_sets.csv";
 
 //--- create file if not exists
  if(!FileIsExist(filename))
  {
   int h=FileOpen(filename, FILE_WRITE|FILE_CSV);
   FileWrite(h,readme);
   FileClose(h);
  }
  
 CArrayList<CArrayString> csv;
 ConfigCSVFileLoader(filename, csv);
 string key, val;
 
 int row_total=csv.Total();
 for(int i=1; i<row_total; i++)
 {
  CArrayString *row = csv.Get(i);
  val = row[0];
 
  int column_total=row.Total();
  for(int j=1; j<column_total; j++) 
  {
    key = row[j];
    p_sym_dict.set(key, val);
    LOG_INFO("loading symbol set config ... ");
    LOG_INFO_2(key, ":", val);
  }
 }
}


void SafetyChecks::load_position_config(string base_directory)
{
 __TRACE__
 
 const int VAR_COUNT_PER_CONFIGURATION = 3;
 string readme = "symbol_set, maximum_lots, action";
                 
 string filename= base_directory+"\\safeties\\position_size.csv";
 
 //--- create file if not exists
  if(!FileIsExist(filename))
  {
   int h=FileOpen(filename, FILE_WRITE|FILE_CSV);
   FileWrite(h,readme);
   FileClose(h);
  }
 
 CArrayList<CArrayString> csv;
 ConfigCSVFileLoader(filename, csv);
 
 int row_total=csv.Total();
 for(int i=1; i<row_total; i++)
 {
  CArrayString *row = csv.Get(i);
  ArrayResize(p_pos, ++p_pos_count);
 
  int column_total=row.Total();
  
  //--- sanity check
   if(column_total != VAR_COUNT_PER_CONFIGURATION) {
    LOG_HIGH_2(column_total, "!=", VAR_COUNT_PER_CONFIGURATION);
    ExpertRemove();
   }
  
  //---
   p_pos[p_pos_count-1].symbol_set=row[0];
   p_pos[p_pos_count-1].lots=(double)row[1];
   p_pos[p_pos_count-1].action=row[2];
   
   LOG_INFO("loading position size config (row=" + (string)(p_pos_count-1) + "): ");
   LOG_INFO_3(p_pos[p_pos_count-1].symbol_set, p_pos[p_pos_count-1].lots, p_pos[p_pos_count-1].action);
  }
}


void SafetyChecks::load_volume_config(string base_directory)
{
 __TRACE__
 
 const int VAR_COUNT_PER_CONFIGURATION=4;
 string readme = "symbol set, maximum_lots, time_in_minutes, action";
                 
 string filename= base_directory+"\\safeties\\volume.csv";
 
 //--- create file if not exists
  if(!FileIsExist(filename))
  {
   int h=FileOpen(filename, FILE_WRITE|FILE_CSV);
   FileWrite(h,readme);
   FileClose(h);
  }
 
 CArrayList<CArrayString> csv;
 ConfigCSVFileLoader(filename, csv);
 
 int row_total=csv.Total();
 for(int i=1; i<row_total; i++)
 {
  CArrayString *row = csv.Get(i);
  ArrayResize(p_vol, ++p_vol_count);
 
  int column_total=row.Total();
  
  //--- sanity check
   if(column_total != VAR_COUNT_PER_CONFIGURATION) {
    LOG_HIGH_2(column_total, "!=", VAR_COUNT_PER_CONFIGURATION);
    ExpertRemove();
   }
  
  //---
   p_vol[p_vol_count-1].symbol_set=row[0];
   p_vol[p_vol_count-1].lots=(double)row[1];
   p_vol[p_vol_count-1].time_in_seconds=(double)row[2] * 60.0;
   p_vol[p_vol_count-1].action=row[3];
   p_vol[p_vol_count-1].start_time=MathMax(TimeCurrent(), most_recent_trade_time());
   
   LOG_INFO("loading equity config (row=" + (string)(p_vol_count-1) + "): ");
   LOG_INFO_5(p_vol[p_vol_count-1].symbol_set, p_vol[p_vol_count-1].lots, p_vol[p_vol_count-1].time_in_seconds,
              p_vol[p_vol_count-1].action, p_vol[p_vol_count-1].start_time);
  }
}


void SafetyChecks::load_equity_config(string base_directory)
{
 __TRACE__
 
 const int VAR_COUNT_PER_CONFIGURATION=4;
 string readme = "above/below, percentage, time (in minutes), action (shut_down, warning, close_all_positions_and_shutdown)";
 
 string filename= base_directory+"\\safeties\\equity.csv";
 
 //--- create file if not exists
  if(!FileIsExist(filename))
  {
   int h=FileOpen(filename, FILE_WRITE|FILE_CSV);
   FileWrite(h, readme);
   FileClose(h);
  }
 
 CArrayList<CArrayString> csv;
 ConfigCSVFileLoader(filename, csv);
 
 int row_total=csv.Total();
 for(int i=1; i<row_total; i++)
 {
  CArrayString *row = csv.Get(i); 
  ArrayResize(p_eq, ++p_eq_count);
 
  int column_total=row.Total();
  
  //--- sanity check
   if(column_total != VAR_COUNT_PER_CONFIGURATION) {
    LOG_HIGH_2(column_total, "!=", VAR_COUNT_PER_CONFIGURATION);
    ExpertRemove();
   }
  
  //---
   ENUM_SAFETY_CHECK_EQUITY type=CHECK_EQUITY_NIL;
   string temp = row[0];
   StringToUpper(temp);
   
   if(temp=="BELOW")
    type=CHECK_EQUITY_PERCENT_CHANGE_LESS_THAN;

   else if(temp=="ABOVE")
    type=CHECK_EQUITY_PERCENT_CHANGE_GREATER_THAN;
    
   else {
    LOG_HIGH("[SKIP] Invalid configuration, type=" + row[0]);
    ArrayResize(p_eq, --p_eq_count);
    continue;
   }
     
   p_eq[p_eq_count-1].type=type;
   p_eq[p_eq_count-1].percentage=(double)row[1];
   p_eq[p_eq_count-1].time_in_seconds=(double)row[2] * 60.0;
   p_eq[p_eq_count-1].action=row[3];
   p_eq[p_eq_count-1].starting_equity=AccountEquity();
   p_eq[p_eq_count-1].start_time=TimeLocal();
   
   LOG_INFO("loading equity config (row=" + (string)(p_eq_count-1) + "): ");
   LOG_INFO_6(p_eq[p_eq_count-1].type, p_eq[p_eq_count-1].percentage, p_eq[p_eq_count-1].time_in_seconds,
              p_eq[p_eq_count-1].action, p_eq[p_eq_count-1].starting_equity, p_eq[p_eq_count-1].start_time);        
  }
}


SafetyChecks::SafetyChecks(string base_directory)
 : p_eq_count(0), p_vol_count(0), p_pos_count(0), logger(__FILE__, "helper_processes")
{
  __TRACE__
  
  //CLASS_INIT(double, ACCOUNT_EQUITY_MINIMUM);
  
  FolderCreate("safeties");
 
  __ load_equity_config(base_directory);
  __ load_volume_config(base_directory);
  __ load_position_config(base_directory);
  __ load_symbol_sets(base_directory);
}
