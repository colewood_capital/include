//+------------------------------------------------------------------+
//|                                              StopLossManager.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "CARM INC"
#property strict

#ifndef ARRAYINT
#define ARRAYINT
   #include <Arrays/ArrayInt.mqh>
#endif

#ifndef ARRAYDOUBLE
#define ARRAYDOUBLE
   #include <Arrays/ArrayDouble.mqh>
#endif

#ifndef ARRAYSTRING
#define ARRAYSTRING
   #include <Arrays/ArrayString.mqh>
#endif

#ifndef TRADINGFUNCTIONS
#define TRADINGFUNCTIONS
   #include <TradingFunctions.mqh>
#endif 

#ifndef MQLERRORS
#define MQLERRORS
   #include <MQLErrors.mqh>
#endif

enum ENUM_SL_MODE { SL_MODE_NULL, SL_MODE_FIXED_PIP_TRAIL, SL_MODE_BREAKEVEN, SL_MODE_PERCENTAGE_TRAIL, SL_MODE_MANUAL, SL_MODE_FRACTAL_TRAIL, SL_MODE_FRACTAL };

class StopLossManager
{
 private:
  CArrayInt order_queue;
  CArrayInt order_type;
  CArrayInt order_sl_mode;
  CArrayDouble order_trigger_price;
  CArrayInt order_trigger_count;
  CArrayDouble order_deviation;
  CArrayDouble order_sl_pip_dist;
  CArrayInt order_max_times_trigger;
  CArrayDouble order_percentage;
  CArrayDouble order_open_price;
  CArrayString order_obj_name;
  CArrayInt order_monotonic;
  CArrayInt order_ma_period;
  CArrayInt order_update_count;
  
  double HighLowFractalMax(int direction, int lookback, double pip_buffer=0.0, bool exact=false);
  double FractalSignal(int type, double fractal_sl_buffer, int ma_period=25);
  bool Triggered(int pos);
  int SetSL(int ticket, int mode, double trigger_price);
  void HandleBreakevenSL();
  
 public:  
  bool SetNullSL(int ticket);
  bool SetFractalSL(int ticket, double trigger_price, double pip_deviation=0);
  bool SetBreakevenSL(int ticket, double trigger_price, double pip_deviation=0);   //Takes commission into account when setting SL
  bool SetFixedPipTrailingSL(int ticket, double trigger_price, double modify_on_pips_gained, double sl_pip_dist, int max_times=0);
  bool SetPercentTrailingSL(int ticket, double trigger_price, double percent);
  bool SetFractalTrailingSL(int ticket, double trigger_price, double fractal_sl_pip_buffer=0, int ma_period=25, bool mode_monotonic=false);  
  bool SetManualSL(int ticket, string obj_name);
  bool ModifySL(int booknum, double sl); 
  int Update();  //returns the booknum of any order closed by [sl]; else returns -1
  bool UpdateBooknum(int old_booknum, int new_booknum);
  bool Remove(int ticket);
  int GetUpdateCount(int ticket);
  
  StopLossManager();
};
//+------------------------------------------------------------------+
StopLossManager::StopLossManager()
{
 
}
//+------------------------------------------------------------------+
bool StopLossManager::UpdateBooknum(int old_booknum, int new_booknum)
{
  int pos = order_queue.SearchLinear(old_booknum);
  
  if(pos>=0)
   {
    if(order_queue.Update(pos, new_booknum))
     return(true);
    else
     Alert("Error: could not update order_queue. pos=",pos,". old_booknum=",old_booknum,". new_booknum=",new_booknum);
   }
  else
   Alert("Error: could not update old_booknum=",old_booknum," to new_booknum=",new_booknum," in ",__FUNCTION__);
    
  return(false);
}
//+------------------------------------------------------------------+
int StopLossManager::GetUpdateCount(int ticket)
{
  int pos = order_queue.SearchLinear(ticket);
  
  if(pos>=0)
   return(order_update_count[pos]);
   
  Alert("Error: could not find ticket=",ticket," in ",__FUNCTION__);
   
  return(-1);
}
//+------------------------------------------------------------------+
double StopLossManager::FractalSignal(int type, double fractal_sl_buffer, int ma_period=25)
{
  string sym = Symbol();
  int shift = 2;
  double ma = iMA(NULL,0,ma_period,0,MODE_SMA,PRICE_CLOSE,shift);
  double frac;

  if(type==OP_BUY)
   {
    frac = iFractals(NULL,0,MODE_LOWER,shift);
    
    if(frac > 0 && frac < ma) 
     return(frac - fractal_sl_buffer); 
   }
  else if(type==OP_SELL)
   {
    frac = iFractals(NULL,0,MODE_UPPER,shift);
    
    if(frac > 0 && frac > ma) 
     return(frac + fractal_sl_buffer); 
   }
  else
   Alert("Error: invalid type=",type," in ",__FUNCTION__);
  
  return(0);
}
//+------------------------------------------------------------------+
bool StopLossManager::SetFractalTrailingSL(int ticket, double trigger_price, double fractal_sl_pip_buffer=0, int ma_period=25, bool mode_monotonic=false)
{
  int pos = SetSL(ticket, SL_MODE_FRACTAL_TRAIL, trigger_price);
  
  if(pos>=0)
   {
    if(order_deviation.Update(pos, fractal_sl_pip_buffer * Point * 10) && order_ma_period.Update(pos, ma_period) && order_monotonic.Update(pos, (int)mode_monotonic))
     return(true);
    else
     {
      Alert("Error: could not update fractal_sl_buffer=",fractal_sl_pip_buffer," ma_period=",ma_period,", mode_monotonic=",mode_monotonic,". pos=",pos," in ",__FUNCTION__);
      return(false);
     }
   }
   
  Alert("Error: Setting SL in ",__FUNCTION__,". ticket=",ticket,", trigger_price=",trigger_price,", fractal_sl_pip_buffer=",fractal_sl_pip_buffer,", ma_period=",ma_period,", mode_monotonic=",mode_monotonic," in ",__FUNCTION__);
   
  return(false);
}
//+------------------------------------------------------------------+
bool StopLossManager::SetManualSL(int ticket,string obj_name)
{
  int pos = SetSL(ticket, SL_MODE_MANUAL, 0);
  
  if(pos>=0)
   {
    if(order_obj_name.Update(pos, obj_name))
     return(true);
    else
     {
      Alert("Error: could not update obj_name=",obj_name,". pos=",pos," in ",__FUNCTION__);
      return(false);
     }
   }
   
  Alert("Error: Setting SL in ",__FUNCTION__,". ticket=",ticket,", obj_name=",obj_name," in ",__FUNCTION__);
   
  return(false);
}
//+------------------------------------------------------------------+
bool StopLossManager::SetPercentTrailingSL(int ticket,double trigger_price,double trailing_percent)
{
  int pos = SetSL(ticket, SL_MODE_PERCENTAGE_TRAIL, trigger_price);
  
  if(pos>=0)
   {
    if(OrderSelect(ticket,SELECT_BY_TICKET))
     {
      if(order_percentage.Update(pos, trailing_percent) && order_open_price.Update(pos, OrderOpenPrice())) 
       return(true); 
      else
       {
        Alert("Error: unable to update ticket #",ticket,". trigger_price=",trigger_price,". trailing_percent=",trailing_percent," in ",__FUNCTION__);
        return(false);
       }
     }
    else
     {
      Alert("Error: unable to select ticket #",ticket," in ",__FUNCTION__);
      return(false);
     } 
   }
   
  Alert("Error: Setting SL in ",__FUNCTION__,". ticket=",ticket,", trigger_price=",trigger_price,". trailing_percent=",trailing_percent," in ",__FUNCTION__);
   
  return(false);
}
//+------------------------------------------------------------------+
bool StopLossManager::SetFixedPipTrailingSL(int ticket, double trigger_price, double modify_on_pips_gained, double sl_pip_dist, int max_times=0)
{
  int pos = SetSL(ticket, SL_MODE_FIXED_PIP_TRAIL, trigger_price);
  
  if(pos>=0)
   {
    if(order_deviation.Update(pos, modify_on_pips_gained * Point * 10) && order_max_times_trigger.Update(pos, max_times) && order_sl_pip_dist.Update(pos, sl_pip_dist * Point * 10)) 
     return(true); 
     
    else
     {
      Alert("Error: unable to update ticket #",ticket,". modify_on_pips_gained=",modify_on_pips_gained,". max_times=",max_times,". sl_pip_dist=",sl_pip_dist," in ",__FUNCTION__);
      return(false);
     }
   }
   
  Alert("Error: Setting SL in ",__FUNCTION__,". ticket=",ticket,", trigger_price=",trigger_price,", modify_on_pips_gained=",modify_on_pips_gained,", max_times=", max_times);
  
  return(false);
}
//+------------------------------------------------------------------+
int StopLossManager::Update()
{ 
  int total = order_queue.Total();
  
  for(int i=total-1; i>=0; i--) 
   { 
    // int booknum = order_queue[i];
     
    //--- Check and delete any order from the order_queue[] whose SL has been hit 
    // if(pending_trades.SearchLinear(booknum) >= 0 && p_orders.isSLTriggered(booknum))
    //  {  Alert("A");
    //   Delete(booknum);
    //   return(booknum);
   //   }
      
        if(order_sl_mode[i]==SL_MODE_NULL)      continue;
    
       //--- Handle any order set to Manual Mode
        if(order_sl_mode[i]==SL_MODE_MANUAL)
         {
           double mySL = -1;
            
           if(ObjectFind(order_obj_name[i]) >= 0)
             mySL = NormalizeDouble(ObjectGet(order_obj_name[i], OBJPROP_PRICE1), Digits);
           else
             ModifySL(order_queue[i], 0);
             
           if(mySL >= 0 && MathAbs(OrderStopLoss() - mySL) > DBL_EPSILON)
            { 
             int type = order_type[i];
                
             if(((type==OP_BUY||type==OP_BUYLIMIT||type==OP_BUYSTOP) && mySL < Bid) || ((type==OP_SELL||type==OP_SELLLIMIT||type==OP_SELLSTOP) && mySL > Ask))
              {
                ModifySL(order_queue[i], mySL);
              }
            }
   
           continue;
         }
   
       //--- Handle any order whose trigger price has been hit
        if(Triggered(i))
         { 
          switch(order_sl_mode[i]) 
           {
            case SL_MODE_NULL:                        continue;
            
            case SL_MODE_FIXED_PIP_TRAIL:
             {
               double new_sl, new_trigger_price;
               
               if(order_type[i]==OP_BUY) 
                {
                 new_sl = order_trigger_price[i] - order_sl_pip_dist[i];
                 new_trigger_price = order_trigger_price[i] + order_deviation[i];
                }
               else if(order_type[i]==OP_SELL)
                {
                 new_sl = order_trigger_price[i] + order_sl_pip_dist[i];
                 new_trigger_price = order_trigger_price[i] - order_deviation[i];
                }
               else
                {
                 Alert("Error: invalid type=",order_type[i]," in ",__FUNCTION__);
                 continue;
                }
     
               ModifySL(order_queue[i], new_sl);
               
               order_trigger_price.Update(i, new_trigger_price);
               
               if(order_max_times_trigger[i] > 0 && order_trigger_count[i] > order_max_times_trigger[i])
                SetNullSL(order_queue[i]); 
                
               break;
             }
             
            case SL_MODE_FRACTAL:
             { 
               int type = -1;
               double sl, new_sl;
               int lookback;
                
               if(OrderSelect(order_queue[i],SELECT_BY_TICKET)) 
                { 
                 sl = OrderStopLoss();
                 type = OrderType();
                 lookback = iBarShift(NULL, 0, OrderOpenTime());
                }
               else
                {
                 Alert("Error: could not select ticket #",order_queue[i]," in ",__FUNCTION__);
                 break;
                }
                 
               if(type==OP_BUY) {
                double fractal = HighLowFractalMax(OP_BUY, lookback, order_deviation[i]);
                new_sl = MathMax(sl, fractal); 
               }
               
               else if(type==OP_SELL) {
                double fractal = HighLowFractalMax(OP_SELL, lookback, order_deviation[i]);
                new_sl = (sl>0) ? MathMin(sl, fractal): fractal;
               }
               
               else {
                Alert("Error: invalid type=",type," in ",__FUNCTION__);
                continue;
               }
                
               Alert("Modifying with sl=",sl,". new_sl=",new_sl," and lookback=",lookback);
               ModifySL(order_queue[i], new_sl);
     
               SetNullSL(order_queue[i]); 
             
               break;
             }
             
            case SL_MODE_BREAKEVEN:
             {
               int type = -1;
               double commission = 0;
               double tick_value = MarketInfo(Symbol(), MODE_TICKVALUE);
               double open_price, lots;
                
               if(OrderSelect(order_queue[i],SELECT_BY_TICKET)) 
                {
                 type = OrderType();
                 lots = OrderLots();
                 commission = MathAbs(OrderCommission());
                 open_price = OrderOpenPrice();
                }
               else
                {
                 Alert("Error: could not select ticket #",order_queue[i]," in ",__FUNCTION__);
                 break;
                }
                 
               if(type==OP_BUY)
                {
                 open_price += MathCeil(commission / (tick_value * lots)) * Point;
                 open_price += order_deviation[i];
                }
                
               if(type==OP_SELL)
                {
                 open_price -= MathCeil(commission / (tick_value * lots)) * Point;
                 open_price -= order_deviation[i];
                }
          
               ModifySL(order_queue[i], open_price);
     
               SetNullSL(order_queue[i]); 
               
               break;
             }
             
            case SL_MODE_PERCENTAGE_TRAIL:
             {
               double new_sl, new_trigger_price;
               
               if(order_type[i]==OP_BUY) 
                {
                 new_sl = order_open_price[i] + ((order_trigger_price[i]-order_open_price[i]) * (1 - order_percentage[i]));
                 new_trigger_price = MathMax(order_trigger_price[i], Bid);
                }
               else if(order_type[i]==OP_SELL)
                {
                 new_sl = order_open_price[i] - ((order_open_price[i]-order_trigger_price[i]) * (1 - order_percentage[i]));
                 new_trigger_price = MathMin(order_trigger_price[i],Bid);
                }
               else
                {
                 Alert("Error: invalid type=",order_type[i]," in ",__FUNCTION__);
                 continue;
                }
     
               ModifySL(order_queue[i], new_sl);
               
               order_trigger_price.Update(i, new_trigger_price);
              
               break;
             }
             
            case SL_MODE_FRACTAL_TRAIL:
             {
               int type = order_type[i];
               
               double frac = FractalSignal(type, order_deviation[i], order_ma_period[i]);
               
               if(frac > 0)
                {
                  ModifySL(order_queue[i], frac);
                  
                  if(order_monotonic[i])
                   order_trigger_price.Update(i, (Bid+Ask)/2);
                }
             
               break;
             }
                 
            default:
             Alert("Error: could not find 'sl_mode'=",order_sl_mode[i]," for ticket #",order_queue[i]," in ",__FUNCTION__);
             
             break;
           }
         }//end if
     // }//end if
   }//end for
   
  return(-1);
}
//+------------------------------------------------------------------+
bool StopLossManager::Remove(int ticket)
 { 
   int pos = order_queue.SearchLinear(ticket);
   
   if(pos >= 0)
    {
      order_queue.Delete(pos);
      order_type.Delete(pos);
      order_sl_mode.Delete(pos);
      order_trigger_price.Delete(pos);
      order_trigger_count.Delete(pos);
      order_deviation.Delete(pos);
      order_max_times_trigger.Delete(pos);
      order_sl_pip_dist.Delete(pos);
      order_percentage.Delete(pos);
      order_open_price.Delete(pos);
      order_obj_name.Delete(pos);
      order_monotonic.Delete(pos);
      order_ma_period.Delete(pos);
      order_update_count.Delete(pos);
      
      return(true);
    }
    
   Alert("Error: could not delete ticket #",ticket," in ",__FUNCTION__);
   return(false);
 }
//+------------------------------------------------------------------+
bool StopLossManager::SetFractalSL(int ticket, double trigger_price, double pip_deviation=0)
{ 
  int pos = SetSL(ticket, SL_MODE_FRACTAL, trigger_price);
  
  if(pos>=0)
   {
    if(order_deviation.Update(pos, pip_deviation * Point * 10)) 
     return(true); 
    else
     {
      Alert("Error: unable to update ticket #",ticket,". pip_deviation=",pip_deviation," in ",__FUNCTION__);
      return(false);
     }
   }
  
  if(pos < 0)  
   Alert("Error: Setting SL in ",__FUNCTION__,". ticket=",ticket,", trigger_price=",trigger_price,", pip_deviation=",pip_deviation);
   
  return(false);
}
//+------------------------------------------------------------------+
bool StopLossManager::SetBreakevenSL(int ticket, double trigger_price, double pip_deviation=0)
{ 
  int pos = SetSL(ticket, SL_MODE_BREAKEVEN, trigger_price);
  
  if(pos>=0)
   {
    if(order_deviation.Update(pos, pip_deviation * Point * 10)) 
     return(true); 
    else
     {
      Alert("Error: unable to update ticket #",ticket,". pip_deviation=",pip_deviation," in ",__FUNCTION__);
      return(false);
     }
   }
  
  if(pos < 0)  
   Alert("Error: Setting SL in ",__FUNCTION__,". ticket=",ticket,", trigger_price=",trigger_price,", pip_deviation=",pip_deviation);
   
  return(false);
}
//+------------------------------------------------------------------+
bool StopLossManager::SetNullSL(int ticket)
{
  int pos = SetSL(ticket, SL_MODE_NULL, 0);
  
  if(pos < 0)  
   Alert("Error: Setting SL in ",__FUNCTION__,". ticket=",ticket);
   
  return(false);
}
//+------------------------------------------------------------------+
int StopLossManager::SetSL(int ticket, int mode, double trigger_price)
{
  int type = -1;
  
  if(OrderSelect(ticket,SELECT_BY_TICKET)) 
   type = OrderType();
            
  if(type >= 0)
   {
     int pos = order_queue.SearchLinear(ticket);
     
     if(pos >= 0)
      {
        order_trigger_price.Update(pos, trigger_price);
        order_sl_mode.Update(pos, mode); 
        if(mode!=SL_MODE_NULL)    order_update_count.Update(pos, order_update_count[pos]+1);
        return(pos);
      }
     else
      {
        order_queue.Add(ticket);
        order_type.Add(type);
        order_sl_mode.Add(mode); 
        order_trigger_price.Add(trigger_price);
        order_trigger_count.Add(0);
        order_deviation.Add(0);
        order_max_times_trigger.Add(0);
        order_sl_pip_dist.Add(0);
        order_percentage.Add(0);
        order_open_price.Add(0);
        order_obj_name.Add(NULL);
        order_monotonic.Add(0);
        order_ma_period.Add(0);
        order_update_count.Add(0);
        return(order_queue.Total()-1);
      }  
   }
  else
   {
    Alert("Error: unable to set SL in ",__FUNCTION__,", ticket=",ticket,", mode=",mode,", trigger_price: ",trigger_price,", type=",type);
    return(-1);
   }
}
//+-----------------------------------------------------------------+
bool StopLossManager::ModifySL(int booknum, double sl)
{
 return(TradingFunctionsModifySL(booknum, sl));
}
//+------------------------------------------------------------------+
bool StopLossManager::Triggered(int pos)
{
 switch(order_type[pos])
  {
   case OP_BUY:
    {
     if(order_trigger_price[pos] > 0 && High[0] >= order_trigger_price[pos]) 
      {
        order_trigger_count.Update(pos, order_trigger_count[pos] + 1);
        return(true);
      }
     
     break;
    }
       
   case OP_SELL:
    {
     if(order_trigger_price[pos] > 0 && Low[0] <= order_trigger_price[pos]) 
      {
        order_trigger_count.Update(pos, order_trigger_count[pos] + 1);
        return(true);
      }
     
     break;
    }  
      
   default:
      Alert("Error: invalid direction in ",__FUNCTION__);

 }//end switch

  
 return(false);
}
//+------------------------------------------------------------------+
double StopLossManager::HighLowFractalMax(int direction, int lookback, double pip_buffer=0.0, bool exact=false)
 //exact = true -> return -1 if no fractal is found within the lookback period
 {
  double minmax;
  double fractal;
  int i=0;
  
  switch(direction)
   {
    case OP_BUY:  
      minmax = 0;
      
      if(exact)
       {
        while(i<lookback)
        {
          fractal = iFractals(NULL,0,MODE_LOWER,i);
            
          if(fractal > 0 && fractal < Bid - (pip_buffer*Point*10))
            minmax = MathMax(minmax,fractal);
              
          i++;
        }
        
        if(minmax==0)
         minmax = -1;
       }
      else
       {
        while(i<lookback || (i>=lookback && minmax==INT_MAX))
         {
          fractal = iFractals(NULL,0,MODE_LOWER,i);
         
          if(fractal > 0 && fractal < Bid - (pip_buffer*Point*10))
            minmax = MathMin(minmax,fractal);
           
          i++;
         }        
       }
       
     break;
    
    case OP_SELL:
      minmax = DBL_MAX;
      
      if(exact)
       {
        while(i<lookback)
         {
           fractal = iFractals(NULL,0,MODE_UPPER,i);
         
           if(fractal > Ask + (pip_buffer*Point*10))
             minmax = MathMin(minmax, fractal);
           
           i++;
         }
         
        if(minmax==DBL_MAX)
         minmax = -1;
       }
      else
       {
        while(i<lookback || (i>=lookback && minmax==0))
         {
           fractal = iFractals(NULL,0,MODE_UPPER,i);
         
           if(fractal > Ask + (pip_buffer*Point*10))
             minmax = MathMin(minmax, fractal);
           
           i++;
         }
       }
       
     break;
       
    default:
      Alert("Invalid direction parameter in ",__FUNCTION__);
      return(0);
   }
  
  return(minmax);
 }