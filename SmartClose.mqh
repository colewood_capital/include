//+------------------------------------------------------------------+
//|                                                   SmartClose.mqh |
//|                                                         Carm Inc |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Carm Inc"
#property link      "https://www.mql5.com"
#property strict

enum ENUM_RSI { RSI_NIL, RSI_BUY, RSI_SELL };

enum ENUM_STOCHASTIC { STOCHASTIC_NIL, STOCHASTIC_BUY, STOCHASTIC_SELL };

#ifndef ARRAYINT
#define ARRAYINT
  #include <Arrays/ArrayInt.mqh>
#endif

class SmartClose
{
 private:
  int magic;
  CArrayInt m_ticket_chain;
  int m_type;
  double m_lots;
  double m_last_close_price;
  double m_open_price;
  double m_profit;
  double m_activation_price;
  double m_MIN_LOTSIZE;
  bool enable_long;
  bool enable_short;
  double rsi_min;
  double rsi_max;
  bool rsi_active;
  double stochastic_min;
  double stochastic_max;
  bool stochastic_active;
  
  int OpenLots(double lots);
  void CloseLots(double lots);
  
  ENUM_RSI rsi();
  ENUM_STOCHASTIC stochastic();
  void GetTicketInfo(double &open_lots, double &closed_lots, double &profit);
  void SmartClose::GetTicketInfo(double &profit);
  int GetNewBooknum(int old_ticket);
  int GetOriginalBooknum(int booknum);
  
  static SmartClose* cur;
  
 public:
  void run();
  SmartClose* next;
  SmartClose* prev;
  
  SmartClose(int ticket, int MAGIC=0, double MIN_LOTSIZE=0.01);
  ~SmartClose();
};

SmartClose* SmartClose::cur = NULL;

void SmartClose::~SmartClose(void)
{
 double profit;
 
 GetTicketInfo(profit);
 
 Alert("ticket chain #", m_ticket_chain[0]," DEACTIVATED with profit = $", DoubleToStr(profit, 2));
 
 //--- shift pointer chain
 if(CheckPointer(next) > POINTER_INVALID)
 {
  if(next != NULL)   next.prev = prev;
  if(prev != NULL)   prev.next = next;
 }
 
 int total = m_ticket_chain.Total();
 
 for(int i=total-1; i>=0; i--)
 {
  if(OrderSelect(m_ticket_chain[i],SELECT_BY_TICKET))
  {
   int type = OrderType();
   double cls_price;
   
   if(type==OP_BUY)
    cls_price = Bid;
   else if(type==OP_SELL)
    cls_price = Ask;
   else
    continue;
    
   if(OrderCloseTime()==0)
    bool ans = OrderClose(OrderTicket(), OrderLots(), cls_price, 5);
  }
 }
}

void SmartClose::SmartClose(int ticket, int MAGIC=0, double MIN_LOTSIZE=0.01) : prev(NULL), next(NULL), enable_long(false), enable_short(false)
{
 m_activation_price = (Bid+Ask) / 2;
 
 if(CheckPointer(cur) > POINTER_INVALID)
 {
  prev = cur;
  cur.next = GetPointer(this);
 }
 
 cur = GetPointer(this);
 
 m_MIN_LOTSIZE = MIN_LOTSIZE;
 magic = MAGIC;
 rsi_max = iRSI(NULL, 0, 14, PRICE_CLOSE, 0);
 rsi_min = rsi_max;
 rsi_active = true;
 stochastic_max = iStochastic(NULL, 0, 14, 8, 3, MODE_SMA, 0, MODE_MAIN, 0);
 stochastic_min = stochastic_max;
 stochastic_active = true;
 
 if(OrderSelect(ticket,SELECT_BY_TICKET) && (OrderType()==OP_BUY || OrderType()==OP_SELL))
 {
  m_ticket_chain.Add(ticket);
  m_type = OrderType();
  m_open_price = OrderOpenPrice();
  m_lots = OrderLots();

  double close_lots = NormalizeDouble((m_lots*2) / 3, 2);
  double cls_price = (OrderType()==OP_BUY) ? Bid : Ask;
  
  bool res = OrderModify(ticket, 0, 0, 0, 0);    //--- remove SL and TP
  bool ans = OrderClose(ticket, close_lots, cls_price, 5);
  
  if(ans)
  { 
   if(OrderSelect(ticket, SELECT_BY_TICKET))
   {
    m_last_close_price = OrderClosePrice();
   }
   
   int new_ticket = GetNewBooknum(ticket);
  
   if(new_ticket > 0)
   {
    m_ticket_chain.Add(new_ticket);
   }
  }
 }
 
 return;
}

ENUM_STOCHASTIC SmartClose::stochastic(void)
{
 const int BUY_LEVEL = 20;
 const int SELL_LEVEL = 80;
 const int BUY_TRIGGER_LEVEL = 30;
 const int SELL_TRIGGER_LEVEL = 70;
 const int BUY_RESET_LEVEL = 40;
 const int SELL_RESET_LEVEL = 60;
 
 double main = iStochastic(NULL, 0, 14, 8, 3, MODE_SMA, 0, MODE_MAIN, 0);
 double signal = iStochastic(NULL, 0, 14, 8, 3, MODE_SMA, 0, MODE_SIGNAL, 0);
 
 //--- update min/max rsi
 if(stochastic_active && main > stochastic_max) stochastic_max = main;
 if(stochastic_active && main < stochastic_min) stochastic_min = main;
 
 if(stochastic_active && stochastic_max >= SELL_LEVEL && main <= signal && main >= SELL_TRIGGER_LEVEL)
 { 
  //Alert("Throw RSI sell signal");
  stochastic_max = 0;
  stochastic_active = false;
  return STOCHASTIC_SELL;
 }
 
 if(stochastic_active && stochastic_min <= BUY_LEVEL && main >= signal && main <= BUY_TRIGGER_LEVEL)
 { 
  //Alert("Throw RSI buy signal");
  stochastic_min = 100;
  stochastic_active = false;
  return STOCHASTIC_BUY;
 }
 
 if(signal > BUY_RESET_LEVEL && signal < SELL_RESET_LEVEL)
  stochastic_active = true;
 
 return STOCHASTIC_NIL;
}

ENUM_RSI SmartClose::rsi(void)
{
 const int BUY_LEVEL = 30;
 const int SELL_LEVEL = 70;
 const int BUY_RESET_LEVEL = 40;
 const int SELL_RESET_LEVEL = 60;
 const int CONFIRMATION_BUFFER = 8;
 
 double rsi = iRSI(NULL, 0, 14, PRICE_CLOSE, 0);
 
 //--- update min/max rsi
 if(rsi_active && rsi > rsi_max) rsi_max = rsi;
 if(rsi_active && rsi < rsi_min) rsi_min = rsi;
 
 if(rsi_active && rsi_max >= SELL_LEVEL && rsi_max - rsi >= CONFIRMATION_BUFFER)
 { 
  //Alert("Throw RSI sell signal");
  rsi_max = 0;
  rsi_active = false;
  return RSI_SELL;
 }
 
 if(rsi_active && rsi_min <= BUY_LEVEL && rsi - rsi_min >= CONFIRMATION_BUFFER)
 { 
  //Alert("Throw RSI buy signal");
  rsi_min = 100;
  rsi_active = false;
  return RSI_BUY;
 }
 
 if(rsi > BUY_RESET_LEVEL && rsi < SELL_RESET_LEVEL)
  rsi_active = true;
 
 return RSI_NIL;
}

void SmartClose::GetTicketInfo(double &open_lots, double &closed_lots, double &profit)
{
 int total = m_ticket_chain.Total();
 open_lots=0; closed_lots=0; profit=0;
 
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(m_ticket_chain[i],SELECT_BY_TICKET))
  {
   if(OrderCloseTime()==0)
    open_lots += OrderLots();
    
   profit += OrderProfit();
  }
 }
 
 closed_lots = m_lots - open_lots;
 
 return;
}

void SmartClose::GetTicketInfo(double &profit)
{
 int total = m_ticket_chain.Total();
 profit=0;
 
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(m_ticket_chain[i],SELECT_BY_TICKET))
  {  
   profit += OrderProfit();
  }
 }
 
 return;
}

int SmartClose::OpenLots(double lots)
{
  double open_price = (m_type==OP_BUY) ? Ask : Bid;
  
  int ticket = OrderSend(Symbol(), m_type, lots, open_price, 5, 0, 0,NULL,magic);
     
  if(ticket > 0)
  {
   m_ticket_chain.Add(ticket);
  }
  
  return ticket;
}

void SmartClose::CloseLots(double lots)
{
 int total = m_ticket_chain.Total();
 double cls_price = (m_type==OP_BUY) ? Bid : Ask;
 double cls_lots = 0;
 
 for(int i=0; i<total && cls_lots < lots; i++)
 {
  if(OrderSelect(m_ticket_chain[i],SELECT_BY_TICKET))
  {
   if(OrderCloseTime() > 0 || OrderProfit()>0)
    continue;
   
   double order_lots = MathMin(OrderLots(), lots);
   
   bool ans = OrderClose(m_ticket_chain[i], order_lots, cls_price, 5);
  
   if(ans)
   {
    if(OrderSelect(m_ticket_chain[i],SELECT_BY_TICKET))
    { 
     cls_lots += order_lots;
     m_last_close_price = OrderClosePrice();
     m_profit += OrderCommission() + OrderSwap() + OrderProfit();
    }
    
    int new_ticket = GetNewBooknum(m_ticket_chain[i]);

    if(new_ticket > 0)
     m_ticket_chain.Add(new_ticket);
   }
  }
 }
}


void SmartClose::run(void)
{   
  ENUM_RSI t = rsi();
  ENUM_STOCHASTIC s = stochastic();
  double open_lots, cls_lots, profit;
  GetTicketInfo(open_lots, cls_lots, profit);
  
  if(profit >= 0 && CheckPointer(GetPointer(this))==POINTER_DYNAMIC)
  {   
   delete GetPointer(this);
   return;
  }
  
  //--- Open on Stochastic

   if(m_type==OP_BUY && s==STOCHASTIC_SELL)
   { 
    //Alert("Catch Stochastic close long position signal");
    double prc = (Bid + Ask) / 2;
    
    if(prc < m_activation_price)
    {
     CloseLots(open_lots);
    }
   }
  
   if(m_type==OP_SELL && s==STOCHASTIC_BUY)
   { 
    //Alert("Catch Stochastic close short position signal");
   
    double prc = (Bid + Ask) / 2;
    
    if(prc > m_activation_price)
    {
     CloseLots(open_lots);
    }
   }
  
  //--- Open on RSI
  if(enable_long || enable_short || t > RSI_NIL)
  {
   double double_standard_unit = iATR(Symbol(), NULL, Bars-1, 0) * 4;

   if(enable_long || (m_type==OP_BUY && t==RSI_BUY))
   {
    enable_long = true;
   
    if(cls_lots > 0)
    {
     //Alert("Catch RSI reopen long position signal");
    
     double lots = NormalizeDouble(MathMin(cls_lots, (open_lots+cls_lots) / 3), 2);
    
     int ticket = OpenLots(lots);
     
     if(ticket > 0)
     {
      enable_long = false;  
     }
    }
   }
  
 
   if(enable_short || (m_type==OP_SELL && t==RSI_SELL))
   {
    enable_short = true;

    if(cls_lots > 0)
    {
     //Alert("Catch RSI reopen short position signal");
      
     double lots = NormalizeDouble(MathMin(cls_lots, (open_lots+cls_lots) / 3), 2);
    
     int ticket = OpenLots(lots);
     
     if(ticket > 0)
     {
      enable_short = false;
     }
    }
   }
 }//emd open on RSI
}
//+------------------------------------------------------------------+
int SmartClose::GetNewBooknum(int old_booknum)
{
 int total = OrdersTotal();
 string result[];
 
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(i,SELECT_BY_POS))
  {
   int count = StringSplit(OrderComment(),'#',result);
  
   if(count > 1 && result[0]=="from ")
   {
    if((int)result[1]==old_booknum)
    {
     int new_booknum = OrderTicket();
     
     //--- reselect original ticket
      bool ans = OrderSelect(old_booknum, SELECT_BY_TICKET);
     
     return new_booknum;
    }
   }
  }
 }
 
 //--- reselect original ticket
  bool ans = OrderSelect(old_booknum, SELECT_BY_TICKET);
        
 return -1;
}
//+------------------------------------------------------------------+
int SmartClose::GetOriginalBooknum(int booknum)
{
 if(OrderSelect(booknum,SELECT_BY_TICKET))
 {
  string result[];
    
  int cnt1 = StringSplit(OrderComment(),'#',result);
  
  if(cnt1 > 1 && result[0]=="from ")
  {
   int begin = OrdersHistoryTotal();
   
   for(int i=begin-1; i>=0; i--)
    {
     if(OrderSelect(i,SELECT_BY_POS,MODE_HISTORY))
      {
       int cnt2 = StringSplit(OrderComment(),'#',result);
          
       if(cnt2 > 1 && result[0]=="to " && result[1]==(string)booknum) 
       {
        //--- reselect original ticket
        bool ans = OrderSelect(booknum, SELECT_BY_TICKET);
        
        return OrderTicket();
       }
      }
    }//end for
  }
 }
 
 //--- reselect original ticket
 bool ans = OrderSelect(booknum, SELECT_BY_TICKET);
 
 return -1;
}