//+------------------------------------------------------------------+
//|                                          AlphaInputVariables.mqh |

//+------------------------------------------------------------------+
#property copyright "CW Capital"

#property strict

#define REPORTING
#define CUSTOM_CHART_ID                102       

input float FX_MARKUP = 0;
input float METALS_MARKUP = 0; 
input bool CLOSE_ALL_ON_EXTERNAL_CHANGE_OF_DIRECTION = true;

//extern string STOPOUT_QUERY_START_TIME = "1970-01-01 00:00";

#ifndef BASEDIR
#define BASEDIR
   string BASE_DIR = "Alpha HOG";
#endif

string ACCOUNT_SETTINGS_FILE_NAME = BASE_DIR + "\\settings\\account_settings.csv";
string ALPHA_OPEN_CONFIG_FILE_NAME = BASE_DIR + "\\settings\\open_config.csv";
string ALPHA_CLOSE_CONFIG_FILE_NAME = BASE_DIR + "\\settings\\close_config.csv";
string ALPHA_STOPOUT_FILE_NAME = BASE_DIR + "\\settings\\stopout_settings.csv";
string ALPHA_SYMBOL_SETS_FILE_NAME = BASE_DIR + "\\settings\\symbol_sets.csv";