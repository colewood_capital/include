//+------------------------------------------------------------------+
//|                                                   Accounting.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+

#property copyright "Copyright 2013, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property version   "1.00"
#property strict

#define LONG         0
#define SHORT        1
#define OPEN         100
#define CLOSE        101

#include <Arrays\ArrayInt.mqh>
#include  <TradeEvent.mqh>
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class Exposure
  {
private:
    double     counter_volume;
    double     base_volume;
    double     realized_pl;
    double     fees;
    string     accounting_symbol;
   
    void       adjust_exposure(string symbol, int type, double lots, double order_price);
    void       adjust_hedged_exposure(string symbol, int type, string cmt, double order_price);
public:
    CArrayInt  ticket;
    string     symbol()       { return(accounting_symbol); }
    bool       open(int);
    bool       close(int);
    double     vwap()         {        
                                if(base_volume!=0) 
                                 return(counter_volume/base_volume); 
                                else 
                                 return(0); }
                                                  
    double     realized()     { return(realized_pl); }
    double     pl();
    double     net_exposure()     { return(base_volume); }
                     Exposure(void);
                     Exposure(int);
                    ~Exposure();                 
  };

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
Exposure::Exposure(void)
  {
     counter_volume=0;
     base_volume=0;
     realized_pl=0;
     fees=0;
     accounting_symbol=Symbol();
  }
Exposure::Exposure(const int booknum)
  {  
     counter_volume=0;
     base_volume=0;
     realized_pl=0;
     fees=0;
     
     if(OrderSelect(booknum,SELECT_BY_TICKET))
       accounting_symbol=OrderSymbol();
       
     this.open(booknum);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
Exposure::~Exposure()
  {
  }
//+------------------------------------------------------------------+
bool Exposure::open(const int booknum)
  { 
     int type=-1; string symbol=""; double open_price=0; double lots=0;

     if(OrderSelect(booknum,SELECT_BY_TICKET)) 
      {
        symbol = OrderSymbol();
        open_price = OrderOpenPrice();
        lots = OrderLots();
        type = OrderType();
        const string cmt = OrderComment();
        
        if(symbol != accounting_symbol)      return(false);
 
        if(ticket.Add(booknum)) {
        
          if(StringFind(cmt,"from #") >= 0)   //HANDLES MT4 PARTIAL CLOSES 
            return(false);
          
          adjust_exposure(symbol, type, lots, open_price);
          
          return(true);
        }
      } 
     
    return(false);
  }
//+------------------------------------------------------------------+
bool Exposure::close(const int booknum)
  {
     int open_type, close_type=-1; string symbol=""; double close_price=0; double lots=0;
     
     int i=0;
     int ti=0;
     
     while(ti < INT_MAX)
      {
       ti = ticket[i];
       
       if(ti == booknum) 
       {
         ticket.Delete(i);
        
         if(OrderSelect(booknum,SELECT_BY_TICKET)) {
           
           symbol = OrderSymbol();
           open_type = OrderType();
           lots = OrderLots();
           close_price = OrderClosePrice();    
         
         } else return(false);
           
           if(open_type==LONG)      
             close_type=SHORT;
           
           if(open_type==SHORT)
             close_type=LONG;
             
           if(lots>0) {   
             adjust_exposure(symbol, close_type, lots, close_price);
           }
           else {  //HANDLES MT4 CLOSE BY     
            const string cmt=OrderComment();       
            adjust_hedged_exposure(symbol, close_type, cmt, close_price);
           }

         return(true);
       }
        
       i++;
      }
     
     return(false);
  }
//+------------------------------------------------------------------+
void Exposure::adjust_hedged_exposure(string symbol, int type, string cmt, double ticket_price)
  {
    int k = StringFind(cmt,"#");
    const string substr = StringSubstr(cmt,k+1,StringLen(cmt)-k);
    int hedged_ticket = StringToInteger(substr);
    double lots=0.0;
           
    if(OrderSelect(hedged_ticket,SELECT_BY_TICKET))
      lots = OrderLots();
      
    double contract_size = SymbolInfoDouble(symbol,SYMBOL_TRADE_CONTRACT_SIZE);
    
    double volume = lots * contract_size;
    
    switch(type) 
     {
       case LONG:  base_volume += volume; 
                   counter_volume += (ticket_price * volume);
                   return;
       
       case SHORT: base_volume -= volume; 
                   counter_volume -= (ticket_price * volume);
                   return;      
     }              
  }
//+------------------------------------------------------------------+
void Exposure::adjust_exposure(string symbol, int type, double lots, double ticket_price)
  {
    double delta_realized=0;
    
    double contract_size = SymbolInfoDouble(symbol,SYMBOL_TRADE_CONTRACT_SIZE);
    
    double volume = lots * contract_size;
       
          switch(type) 
           {
             case LONG:   if(base_volume < 0) 
                          {
                            if(base_volume <= -volume)                  
                              delta_realized = (vwap() - ticket_price) * volume;                                                 
                            else                                          
                              delta_realized += (vwap() - ticket_price) * -base_volume;          
                          }     
                          
                            base_volume += volume; 
                 
                          counter_volume += (ticket_price * volume) + delta_realized;
                   
                          break;
                       
             case SHORT: 
                          if(base_volume > 0) 
                          {
                            if(base_volume >= volume)                                                         
                              delta_realized = (ticket_price - vwap()) * volume;                                                                   
                            else                                                         
                              delta_realized = (ticket_price - vwap()) * base_volume;
                          }

                           base_volume -= volume;
                      
                         counter_volume +=  -(ticket_price * volume) + delta_realized;
                      
                         break;               
            }
       
      realized_pl += delta_realized;
     
  }
//+------------------------------------------------------------------+
double Exposure::pl()
  {
     double price=0;
     double unrealized_pl=0;
     
     if(base_volume>=0) {
       price = MarketInfo(accounting_symbol,MODE_BID);
       unrealized_pl = (price-vwap()) * base_volume;
     }
     else {
       price=MarketInfo(accounting_symbol,MODE_ASK);
       unrealized_pl = (vwap()-price) * base_volume;
     }
            
     return(unrealized_pl + realized_pl - fees);
  }
  
