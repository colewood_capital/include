//+------------------------------------------------------------------+
//|                                                   TradeEvent.mqh |
//|                        Copyright 2013, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2013, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#include <Arrays\ArrayInt.mqh>

struct evnt_trade
 {
   int booknum;
   bool open;
   
   evnt_trade() {
    booknum = -1; 
    open = false;
   }
 };
 
static CArrayInt orders;

evnt_trade TradeEvent()
 {
   int trade_index=0, ticket=0;
   int i=0;
   int total=OrdersTotal();
   evnt_trade results;
   
   orders.Sort();
   
   for(i=total-1;i>=0;i--)
    {  
      if(OrderSelect(i,SELECT_BY_POS)) {
       ticket=OrderTicket();
        trade_index=orders.Search(ticket);
      }
      
      if(trade_index < 0) {
       orders.Add(ticket);
       
       results.booknum = ticket; 
       results.open = true;
       return(results);
      }
    }
   
   i=0;
 
   while(true) {
    
     ticket = orders[i];
     
     if(ticket == INT_MAX)     break;
 
     if(OrderSelect(ticket,SELECT_BY_TICKET))
     {
       if(OrderCloseTime() > 0) {
        orders.Delete(i);
        
        results.booknum = ticket;
        results.open = false;
        
        return(results);
       }    
     }
     
     i++;
   }
   
   return(results);
 }