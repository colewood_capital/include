//+------------------------------------------------------------------+
//|                                          ClustersAndFlusters.mqh |
//|                                                         Carm Inc |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Carm Inc"
#property link      "https://www.mql5.com"
#property strict

//Use FlusterCreate and FlusterRun to set flusters

#ifndef ARRAYDOUBLE
#define ARRAYDOUBLE
  #include <Arrays/ArrayDouble.mqh>
#endif 

#ifndef ARRAYINT
#define ARRAYINT
  #include  <Arrays/ArrayInt.mqh>
#endif

#ifndef CLUSTERSANDFLUSTERS
#define CLUSTERSANDFLUSTERS
  #define MAGIC_CLUSTER_PREFIX           3368
  #define MAGIC_FLUSTER_PREFIX           3369
  
  class Cluster
  {
   private:
    int m_id;
  
   public:
    int Assign();
    void GetInfo(CArrayInt &cluster_id, CArrayInt &first_entry_ticket, CArrayDouble &profit);
    void GetInfo(const int cluster_id, int &first_entry_ticket, double &profit);
  
    Cluster();
  };
  
  class Fluster
  {
   private:
    int cluster_id;
    int m_type;
    double m_lots;
    double m_last_price;
    double m_last_avg_up_price;
    int m_count;
    int m_avg_up_count;
  
   public:
    void run();
    void close();
    void GetInfo(int &first_entry_ticket, double &profit);
    bool GarbageCollection();
    Fluster(int id, int type, double cluster_open_price);
  };
  
  Cluster* cluster;
  Fluster* fluster[];
  
#endif 

input ENUM_TIMEFRAMES LOWER_TIMEFRAME = PERIOD_M5;
//---------------------------------------
// Clusters
//---------------------------------------
int Cluster::Assign()
{
 if(m_id < 9999)
  m_id++;
 else
  m_id = 1;
  
 return m_id;
}
//+------------------------------------------------------------------+
void Cluster::GetInfo(const int cluster_id, int &first_entry_ticket, double &profit)
{
 int total = OrdersTotal();
 first_entry_ticket = -1;
 profit = 0;
 
 //--- Derive PnL of each cluster
 int cluster_magic = (int)StringConcatenate(MAGIC_CLUSTER_PREFIX, cluster_id);
 double first_entry_price = -1;
 
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(i,SELECT_BY_POS) && OrderMagicNumber()==cluster_magic)
  {
    profit += OrderProfit();

    //--- Calculate first entry price
    int type = OrderType();
    
    if(type==OP_BUY || type==OP_BUYLIMIT || type==OP_BUYSTOP)
    {
     if(OrderOpenPrice() > first_entry_price)
     {
      first_entry_price = OrderOpenPrice();
      first_entry_ticket = OrderTicket();
     }
    }
    
    if(type==OP_SELL || type==OP_SELLLIMIT || type==OP_SELLSTOP)
    {
     if(first_entry_price <= 0 || OrderOpenPrice() < first_entry_price)
     {
      first_entry_price = OrderOpenPrice();
      first_entry_ticket = OrderTicket();
     }
    }
   }
 }//end for
}
//+------------------------------------------------------------------+
void Cluster::GetInfo(CArrayInt &cluster_id, CArrayInt &first_entry_ticket, CArrayDouble &profit)
{
 int id[];
 int ticket[];
 double entry[];
 double pl[];
 
 ArrayResize(id, m_id);
 ArrayResize(ticket, m_id);
 ArrayResize(entry, m_id);
 ArrayResize(pl, m_id);
 
 int total = OrdersTotal();
 
 cluster_id.Clear();
 first_entry_ticket.Clear();
 profit.Clear();
 
 //--- Derive PnL of each cluster
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(i,SELECT_BY_POS) && OrderType() < 6)
  {
   int magic = OrderMagicNumber();
   string prefix = (string)MAGIC_CLUSTER_PREFIX;
   int c_id;
   
   int pos = StringFind((string)magic, prefix);
 
   if(pos >= 0)
   {
    //--- Calculate cluster id
    c_id = (int)StringSubstr((string)magic, StringLen(prefix));
    
    //--- Calculate cluster profit
    pl[c_id-1] += OrderProfit();
    
    //--- Calculate first entry price
    int type = OrderType();
    
    if(type==OP_BUY || type==OP_BUYLIMIT || type==OP_BUYSTOP)
    {
     if(OrderOpenPrice() > entry[c_id-1])
     {
      id[c_id-1] = c_id;
      entry[c_id-1] = OrderOpenPrice();
      ticket[c_id-1] = OrderTicket();
     }
    }
    
    if(type==OP_SELL || type==OP_SELLLIMIT || type==OP_SELLSTOP)
    {
     if(entry[c_id-1] <= 0 || OrderOpenPrice() < entry[c_id-1])
     {
      id[c_id-1] = c_id;
      entry[c_id-1] = OrderOpenPrice();
      ticket[c_id-1] = OrderTicket();
     }
    }
   }
  }
 }
 
 //--- Record cluster PnL
 for(int i=0; i<m_id; i++)
 { 
  if(MathAbs(pl[i]) > DBL_EPSILON)
  { 
   cluster_id.Add(id[i]);
   first_entry_ticket.Add(ticket[i]);
   profit.Add(pl[i]);
  }
 }
 
 ArrayFree(id);
 ArrayFree(ticket);
 ArrayFree(pl);
 ArrayFree(entry);
}
//+------------------------------------------------------------------+
Cluster::Cluster(void)
{
 int total = OrdersTotal();
 int max_id = 0;
 
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(i,SELECT_BY_POS))
  {
   string magic = (string)OrderMagicNumber();
   string prefix = (string)MAGIC_CLUSTER_PREFIX;
   
   int pos = StringFind(magic, prefix);
 
   if(pos >= 0)
    max_id = MathMax(max_id, (int)StringSubstr(magic, StringLen(prefix)));
  }
 }
 
 m_id = max_id;
}

//---------------------------------------
// Flusters
//---------------------------------------
Fluster::Fluster(int id, int type, double cluster_open_price)
{
 int total = OrdersTotal();
 double total_lots = 0;
 
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(i,SELECT_BY_POS) && OrderMagicNumber()==(int)StringConcatenate(MAGIC_CLUSTER_PREFIX, id))
  {
   total_lots += OrderLots(); 
  }
 }
 
 cluster_id = id;
 m_type = type;
 m_last_price = cluster_open_price;
 m_last_avg_up_price = m_last_price;
 m_lots = NormalizeDouble(total_lots / 4, 2);
 m_count = 0;
 m_avg_up_count = 0;
}
//+------------------------------------------------------------------+
bool Fluster::GarbageCollection()
{
 int total = OrdersTotal();
 
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(i,SELECT_BY_POS))
  {
   if(OrderMagicNumber()==(int)StringConcatenate(MAGIC_CLUSTER_PREFIX, cluster_id))
    return false;  
  }
 }
 
 return true;
}
//+------------------------------------------------------------------+
void Fluster::GetInfo(int &first_entry_ticket, double &profit)
{
 int total = OrdersTotal();
 first_entry_ticket = -1;
 profit = 0;
 
 //--- Derive PnL of each cluster
 int fluster_magic = (int)StringConcatenate(MAGIC_FLUSTER_PREFIX, cluster_id);
 double first_entry_price = -1;
 
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(i,SELECT_BY_POS) && OrderSymbol()==Symbol() && OrderMagicNumber()==fluster_magic)
  {
    profit += OrderProfit();
    
    //--- Calculate first entry price
    int type = OrderType();
    
    if(type==OP_BUY || type==OP_BUYLIMIT || type==OP_BUYSTOP)
    {
     if(OrderOpenPrice() > first_entry_price)
     {
      first_entry_price = OrderOpenPrice();
      first_entry_ticket = OrderTicket();
     }
    }
    
    if(type==OP_SELL || type==OP_SELLLIMIT || type==OP_SELLSTOP)
    {
     if(first_entry_price <= 0 || OrderOpenPrice() < first_entry_price)
     {
      first_entry_price = OrderOpenPrice();
      first_entry_ticket = OrderTicket();
     }
    }
   }
 }//end for
}
//+------------------------------------------------------------------+
void Fluster::close()
{
 int total = OrdersTotal();
 int magic = (int)StringConcatenate(MAGIC_FLUSTER_PREFIX, cluster_id);
 
 for(int i=total-1; i>=0; i--)
 {
  if(OrderSelect(i,SELECT_BY_POS) && OrderSymbol()==Symbol() && OrderMagicNumber()==magic)
  {
   double cls_price;
   int type = OrderType();
   
   if(type==OP_BUY)
    cls_price = Bid;
   else if(type==OP_SELL)
    cls_price = Ask;
   else
    continue;
  
   bool ans = OrderClose(OrderTicket(), OrderLots(), cls_price, 5);
  }
 }
}
//+------------------------------------------------------------------+
void Fluster::run()
{
 int timeframe = LOWER_TIMEFRAME;
 int first_cluster_ticket, first_fluster_ticket;
 double cluster_profit, fluster_profit;
 
 double atr = iATR(Symbol(), timeframe, 255, 0);
 
 cluster.GetInfo(cluster_id, first_cluster_ticket, cluster_profit);
 GetInfo(first_fluster_ticket, fluster_profit);
 double realized = GetRealizedPL(cluster_id);
 double profit = cluster_profit + fluster_profit + realized;

 if(m_type==OP_BUY)
 { 
  if(Ask >= m_last_price + (2*atr) && m_count < 6)
  {  
   double lots = NormalizeDouble(m_lots, 2);
   double breakeven_sl = CalculateGroupTrailingSL(cluster_id, lots, profit);
 
   if(breakeven_sl > 0 && Bid > breakeven_sl)
   { 
    int ticket = OrderSend(Symbol(), OP_BUY, lots, Ask, 5, breakeven_sl, 0, NULL, (int)StringConcatenate(MAGIC_FLUSTER_PREFIX, cluster_id));
    
    if(ticket > 0)  {
     ModifyAllCluster((int)StringConcatenate(MAGIC_CLUSTER_PREFIX, cluster_id), breakeven_sl);
     ModifyAllCluster((int)StringConcatenate(MAGIC_FLUSTER_PREFIX, cluster_id), breakeven_sl);
    }
     
    m_last_price = Ask;
    m_last_avg_up_price = m_last_price;
    m_avg_up_count = 0;
    m_count++;
   }
  }
  
  if(Ask <= m_last_avg_up_price - (3*atr) && m_avg_up_count < 3)
  {
   double lots = NormalizeDouble(m_lots*2, 2);
   double breakeven_sl = CalculateGroupTrailingSL(cluster_id, lots, profit);
   
   if(breakeven_sl > 0 && Bid > breakeven_sl)
   { 
    int ticket = OrderSend(Symbol(), OP_BUY, lots, Ask, 5, breakeven_sl, 0, NULL, (int)StringConcatenate(MAGIC_FLUSTER_PREFIX, cluster_id));
   
    if(ticket > 0)  {
     ModifyAllCluster((int)StringConcatenate(MAGIC_CLUSTER_PREFIX, cluster_id), breakeven_sl);
     ModifyAllCluster((int)StringConcatenate(MAGIC_FLUSTER_PREFIX, cluster_id), breakeven_sl);
    }
    
    m_last_avg_up_price = Ask;
    m_avg_up_count++;
    
    if(m_count >= 2)
     m_count = m_count - 2;
   }
  }
 }
 
 if(m_type==OP_SELL)
 {
  if(Bid <= m_last_price - (2*atr))
  {
   double lots = NormalizeDouble(m_lots, 2);
   double breakeven_sl = CalculateGroupTrailingSL(cluster_id, lots, profit);
   
   if(breakeven_sl > 0 && Ask < breakeven_sl && m_count < 4)
   {
    int ticket = OrderSend(Symbol(), OP_SELL, lots, Bid, 5, breakeven_sl, 0, NULL, (int)StringConcatenate(MAGIC_FLUSTER_PREFIX, cluster_id));
   
    if(ticket > 0)  {
     ModifyAllCluster((int)StringConcatenate(MAGIC_CLUSTER_PREFIX, cluster_id), breakeven_sl);
     ModifyAllCluster((int)StringConcatenate(MAGIC_FLUSTER_PREFIX, cluster_id), breakeven_sl);
    }
   
    m_last_price = Bid;
    m_last_avg_up_price = m_last_price;
    m_avg_up_count = 0;
    m_count++;
   }
  }
  
  if(Bid >= m_last_avg_up_price + (3*atr) && m_avg_up_count < 3)
  {
   double lots = NormalizeDouble(m_lots*2, 2);
   double breakeven_sl = CalculateGroupTrailingSL(cluster_id, lots, profit);
   
   if(breakeven_sl > 0 && Ask < breakeven_sl)
   {
    int ticket = OrderSend(Symbol(), OP_SELL, lots, Bid, 5, breakeven_sl, 0, NULL, (int)StringConcatenate(MAGIC_FLUSTER_PREFIX, cluster_id));
   
    if(ticket > 0)  {
     ModifyAllCluster((int)StringConcatenate(MAGIC_CLUSTER_PREFIX, cluster_id), breakeven_sl);
     ModifyAllCluster((int)StringConcatenate(MAGIC_FLUSTER_PREFIX, cluster_id), breakeven_sl);
    }
    
    m_last_avg_up_price = Bid;
    m_avg_up_count++;
    
    if(m_count >= 2)
     m_count = m_count - 2;
   }
  }
 }
}
//+------------------------------------------------------------------+
//Stand-alone functions
//+------------------------------------------------------------------+
double CalculateGroupTrailingSL(int cluster_id, double lots, double max_risk)
{
 double base_vol=0, counter_vol=0;
 double lot_size = 1 / Point;
 int total = OrdersTotal();
 
 //--- Get current open exposure  
 int cluster_magic = (int)StringConcatenate(MAGIC_CLUSTER_PREFIX, cluster_id);
 int fluster_magic = (int)StringConcatenate(MAGIC_FLUSTER_PREFIX, cluster_id);
 int type=-1;
 
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(i,SELECT_BY_POS) && OrderSymbol()==Symbol() && (OrderMagicNumber()==cluster_magic || OrderMagicNumber()==fluster_magic))
  {
   base_vol += OrderLots() * lot_size;
   counter_vol += OrderLots() * lot_size * OrderOpenPrice();
   
   int order_type = OrderType();
   
   if(order_type==OP_BUY || order_type==OP_SELL)
    type = order_type;
  }
 }

 //--- Add theoretical order
 base_vol += lots * lot_size;
 if(type==OP_BUY)
  counter_vol += lots * lot_size * Ask;
 
 else if(type==OP_SELL)
  counter_vol += lots * lot_size * Bid;
 
 else {
  Alert("Error: invalid value, type=", type,", cluster_id=",cluster_id," in ",__FUNCTION__);
  return(-1);
 }
  
 //--- Calculate VWAP
 double vwap = (base_vol > 0) ? counter_vol / base_vol : 0;
 double target_sl = 0;
 
 double tick_value = MarketInfo(Symbol(), MODE_TICKVALUE);
 
 if(vwap > 0)
 {
  if(base_vol * tick_value > 0)
  {
   switch(type)
   {
    case OP_BUY:
     target_sl = Bid - (max_risk / (base_vol * tick_value));
     break;
    
    case OP_SELL:
     target_sl = Ask + (max_risk / (base_vol * tick_value));
     break;
   }
  }
  else {
   Alert("Error: invalid values, base_vol=", base_vol,", tick_value=",tick_value," in ",__FUNCTION__);
   return(-1);
  }
 }
 else {
  Alert("Error: invalid values, counter_vol=", counter_vol,", base_vol=", base_vol," in ",__FUNCTION__);
  return(-1);
 }
 
 return NormalizeDouble(target_sl, Digits);
}
//+------------------------------------------------------------------+
void ModifyAllCluster(int magic, double sl)
{
 int total = OrdersTotal();
 
 for(int i=total-1; i>=0; i--)
 {
  if(OrderSelect(i,SELECT_BY_POS) && OrderSymbol()==Symbol() && OrderMagicNumber()==magic)
  {
   //--- Cancel pending order of cluster
   int type = OrderType();
   bool ans;
   
   if(type==OP_BUYLIMIT || type==OP_SELLLIMIT || type==OP_BUYSTOP || type==OP_SELLSTOP)
    ans = OrderDelete(OrderTicket());
   
   //--- Modify stop loss of cluster
   if(type==OP_BUY)
   {
    if(sl > OrderStopLoss())
     {
      ans = OrderModify(OrderTicket(), OrderOpenPrice(), sl, OrderTakeProfit(), 0);
     }
   }  
   
   if(type==OP_SELL)
   {
    if(sl < OrderStopLoss())
    {
     ans = OrderModify(OrderTicket(), OrderOpenPrice(), sl, OrderTakeProfit(), 0);
    }
   }
  }
 }
}
//+------------------------------------------------------------------+
void FlusterCreate(int id, int type, double cluster_open_price)
{
 int N = ArraySize(fluster) + 1;
 ArrayResize(fluster, N);
 fluster[N-1] = new Fluster(id, type, cluster_open_price);
}
//+------------------------------------------------------------------+
void FlusterRun()
{
 for(int i=0; i<ArraySize(fluster); i++)
 {
  if(fluster[i].GarbageCollection()==true)
  {
   //close associated trades
   fluster[i].close();
   
   //delete memory associated with array
   delete fluster[i];

   //shift array
   for(int j=i; j<ArraySize(fluster)-1; j++)
    fluster[j] = fluster[j+1];
    
   //resize
   ArrayResize(fluster, ArraySize(fluster)-1);
  }
  else
    fluster[i].run();
 }
}
//+------------------------------------------------------------------+
void FlusterRunDeprecated()
{
 int size = ArraySize(fluster);
 int k=0;
 
 for(int i=0; i<size-k; i++)
 {
  if(fluster[i].GarbageCollection()==true)
  {
   //close associated trades
   fluster[i].close();
   
   //delete memory associated with array
   delete fluster[i];

   //shift array
   for(int j=i; j<size-1; j++)
    fluster[j] = fluster[j+1];
    
   //resize
   ArrayResize(fluster, size-1);
   
   k++;
  }
  else
    fluster[i].run();
 }
}
//+------------------------------------------------------------------+
double GetRealizedPL(int cluster_id)
{
 int total = OrdersHistoryTotal();
 int cluster_magic = (int)StringConcatenate(MAGIC_CLUSTER_PREFIX, cluster_id);
 int fluster_magic = (int)StringConcatenate(MAGIC_FLUSTER_PREFIX, cluster_id);
 double profit = 0;
 
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(i,SELECT_BY_POS,MODE_HISTORY))
  {
   int magic = OrderMagicNumber();
   
   if(magic==cluster_magic || magic==fluster_magic)
    profit += OrderProfit();
  }
 }
 
 return profit;
}
//+------------------------------------------------------------------+
void PartialCloseInfo(int booknum, double& closed_lots, double& realized_pl)
 { 
  closed_lots = 0;
  realized_pl = 0;
  
  bool partial_found = false;
  string search = NULL;
  
  if(OrderSelect(booknum,SELECT_BY_TICKET))
   {
    string result[];
    int cnt1, cnt2;
    
    cnt1 = StringSplit(OrderComment(),'#',result);
     
    if(cnt1 > 1 && result[0]=="from ")
     {
      if(OrderSelect((int)result[1],SELECT_BY_TICKET))
       {
        int new_ticket = OrderTicket(); 
        closed_lots += OrderLots();
        realized_pl += OrderProfit();
        
        partial_found = true;
        search = (string)new_ticket;
       }
     }
 
    if((cnt1 > 1 && result[0]=="to ") || partial_found)
     {
      if(search==NULL)
       search = (string)booknum;
      
      int begin = OrdersHistoryTotal()-1;
   
      for(int i=begin; i>=0; i--)
       {
        if(OrderSelect(i,SELECT_BY_POS,MODE_HISTORY))
         {
          cnt2 = StringSplit(OrderComment(),'#',result);
          
          if(cnt2 > 1 && result[0]=="to " && result[1]==search) 
           {
             int new_ticket = OrderTicket();
             closed_lots += OrderLots();
             realized_pl += OrderProfit();
            
             //--- search for additional partial closes
             i = OrdersHistoryTotal();
             search = (string)new_ticket;
           }
         }
       }//end for
     }//end if
   }//end if
   
   //--- Re-select original booknum
   bool ans = OrderSelect(booknum,SELECT_BY_TICKET);
   
   return;
 }