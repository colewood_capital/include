//+------------------------------------------------------------------+
//|                                             PyramidUpManager.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#ifndef ARRAYDOUBLE
#define ARRAYDOUBLE
   #include <Arrays/ArrayDouble.mqh>
#endif 

#ifndef TRADINGFUNCTIONS
#define TRADINGFUNCTIONS
   #include <TradingFunctions.mqh>
#endif 

class PyramidUpManager
{
 private:
 
  double p_magic;
  double p_vol_percentage;
  double p_sl_percentage;
  double p_vol_bank;
  double p_sl_bank;
  
  double CalcDollarRisk(double curr_sl, double lots, double new_sl);
  double CalcNewSL(int type, double curr_sl, double lots, double dollar_risk);
  bool ModifySL(int ticket, double sl);
  void UpdateSL();
  
 public:
 
  void BankAdjustment(double realized_pl);
  void run();
  
  PyramidUpManager(double volume_bank_percentage, double stoploss_bank_percentage, int magic=0);
};
//+------------------------------------------------------------------+
PyramidUpManager::PyramidUpManager(double volume_bank_percentage, double stoploss_bank_percentage, int magic=0)
{
 p_vol_percentage = volume_bank_percentage;
 p_sl_percentage = stoploss_bank_percentage;
 p_magic = magic;
}
//+------------------------------------------------------------------+
void PyramidUpManager::BankAdjustment(double realized_pl)
{
 //--- Step 1: adjust banks to reflect new realized pl
  p_vol_bank = realized_pl * p_vol_percentage;
  p_sl_bank = realized_pl * p_sl_percentage;
  
 //--- Step 2: update SL to reflect new realized pl 
  UpdateSL();
} 
//+------------------------------------------------------------------+
void PyramidUpManager::UpdateSL(void)
{
 double sl_lots_ticket[][3];
 int total = OrdersTotal();
 ArrayResize(sl_lots_ticket, total);
 string symbol = Symbol();
 int type=-1;
 
 for(int i=0; i<total; i++)
  {
   if(OrderSelect(i, SELECT_BY_POS))
    {
     if(OrderSymbol()==symbol && OrderMagicNumber()==p_magic)
      {
       int ord_type = OrderType();
       
       if(ord_type==OP_BUY)
        {
         sl_lots_ticket[i][0] = OrderStopLoss();
         sl_lots_ticket[i][1] = OrderLots();
         sl_lots_ticket[i][2] = OrderTicket();
         type = OP_BUY;
        }
      }
    }
  }
  
 int max = ArrayMaximum(sl_lots_ticket);
 int min = ArrayMinimum(sl_lots_ticket);
 
 double currSL = sl_lots_ticket[max][0];
 double lots = sl_lots_ticket[max][1];
 double newSL = sl_lots_ticket[min][0];
 
 double dollarRisk = CalcDollarRisk(currSL, lots, newSL);
 
 
 if(dollarRisk <= p_sl_bank)
  {
   int ticket = (int)sl_lots_ticket[max][2];
   
   if(ModifySL(ticket, newSL))
    p_sl_bank -= dollarRisk;
  }
 else
  {
   int ticket = (int)sl_lots_ticket[max][2];
   newSL = CalcNewSL(type, currSL, lots, p_sl_bank);
   
   if(ModifySL(ticket, newSL))
    p_sl_bank = 0;
  }
}
//+------------------------------------------------------------------+
double PyramidUpManager::CalcDollarRisk(double curr_sl, double lots, double new_sl)
{
 if(Point==0)
  {
   Alert("Error: invalid parameters, point=", Point, " in ",__FUNCTION__);
   return(-1);
  }
  
 double points = MathAbs(curr_sl - new_sl) / Point;
 double tickValue = MarketInfo(Symbol(), MODE_TICKVALUE);
 
 return(points * lots * tickValue);
}
//+------------------------------------------------------------------+
double PyramidUpManager::CalcNewSL(int type, double curr_sl, double lots, double dollar_risk)
{
 double tickValue = MarketInfo(Symbol(), MODE_TICKVALUE);
 double ret;
 
 if(lots==0 || tickValue==0)
  {
   Alert("Error: invalid parameters, lots=",lots," tickValue: ",tickValue," in ",__FUNCTION__);
   return(-1);
  }
  
 if(type==OP_BUY)
   ret = curr_sl - ((dollar_risk / (lots*tickValue)) * Point);
 else if(type==OP_SELL)
   ret = curr_sl + ((dollar_risk / (lots*tickValue)) * Point);
 else
  {
   Alert("Error: invalid type in ",__FUNCTION__);
   return(-1);
  }
  
 return(ret);
}
//+------------------------------------------------------------------+
bool PyramidUpManager::ModifySL(int ticket,double sl)
{
 return(TradingFunctionsModifySL(ticket, sl));
}
//+------------------------------------------------------------------+