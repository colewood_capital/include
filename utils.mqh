//+------------------------------------------------------------------+
//|                                                        utils.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#import "kernel32.dll" 
   int GetComputerNameA(char& lpBuffer[], string nSize);
#import

#ifndef ARRAYINT
#define ARRAYINT
   #include <Arrays/ArrayInt.mqh>
#endif

#ifndef ARRAYSTRING
#define ARRAYSTRING
   #include <Arrays/ArrayString.mqh>
#endif

#ifndef AUTOSAVEARRAYSTRINGCOMMUNITY
#define AUTOSAVEARRAYSTRINGCOMMUNITY   
   #include <Arrays/AutoSaveArrayStringCommunity.mqh>
#endif 

#ifndef ARRAYDATETIME
#define ARRAYDATETIME
   #include <Arrays/ArrayDatetime.mqh>
#endif

#ifndef MAGIC
   #define MAGIC     0
#endif

#ifndef LOGGER
#define LOGGER
   #define LOGGER_DEPRECATED
   #include <Logger_deprecated.mqh>
#endif

#ifndef IPADDRESS
#define IPADDRESS
   input string IP_ADDRESS = "http://127.0.0.1";
#endif 

#ifndef EMAILALERTS
#define EMAILALERTS
   input string URL_SENDEMAILALERT = "/sendemailalert";
#endif 

#ifndef BASEDIR
#define BASEDIR
 extern string BASE_DIR = "";
#endif

#ifndef UTILS2
#define UTILS2
   #include <utils2.mqh>
#endif

#ifndef ASCII_RECORD_SEPERATOR
#define ASCII_RECORD_SEPERATOR         30
#endif 

Logger utilslogger(__FILE__, "helper_processes");

string StringChangeToUpperCase(string sText) {
  // Example: StringChangeToUpperCase("oNe mAn"); // ONE MAN 
  int iLen=StringLen(sText), i, iChar;
  for(i=0; i < iLen; i++) {
    iChar=StringGetChar(sText, i);
    if(iChar >= 97 && iChar <= 122) sText=StringSetChar(sText, i, (ushort)(iChar-32));
  }
  return(sText);
}
 
string StringChangeToLowerCase(string sText) {
  // Example: StringChangeToLowerCase("oNe mAn"); // one man
  int iLen=StringLen(sText), i, iChar;
  for(i=0; i < iLen; i++) {
    iChar=StringGetChar(sText, i);
    if(iChar >= 65 && iChar <= 90) sText=StringSetChar(sText, i, (ushort)(iChar+32));
  }
  return(sText);  
}
 
string StringChangeFirstToUpperCase(string sText) {
  // Example: StringChangeFirstToUpperCase("oNe mAn"); // One Man
  int iLen=StringLen(sText), i, iChar, iLast=32;
  for(i=0; i < iLen; i++) {
    iChar=StringGetChar(sText, i);
    if(iLast==32 && iChar >= 97 && iChar <= 122) sText=StringSetChar(sText, i, (ushort)(iChar-32));
    else if(iLast!=32 && iChar >= 65 && iChar <= 90) sText=StringSetChar(sText, i, (ushort)(iChar+32));
    iLast=iChar;
  }
  return(sText);  
}

int getNewTicketFromPartialClose(int original_ticket)
{
 int total;
 
 total = OrdersTotal();
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(i,SELECT_BY_POS))
  {
   string cmt = OrderComment();
   string prefix = "from #";
   
   int pos = StringFind(cmt, prefix);
   
   if(pos==0)
   {
    int search_ticket = (int)StringSubstr(cmt, StringLen(prefix));
    
    if(search_ticket==original_ticket)
      return OrderTicket();
   }
  }
 }
 
 total = OrdersHistoryTotal();
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(i,SELECT_BY_POS,MODE_HISTORY))
  {
   string cmt = OrderComment();
   string prefix = "from #";
   
   int pos = StringFind(cmt, prefix);
   
   if(pos==0)
   {
    int search_ticket = (int)StringSubstr(cmt, StringLen(prefix));
    
    if(search_ticket==original_ticket)
      return OrderTicket();
   }
  }
 }
 
 return -1;
}
//+------------------------------------------------------------------+
void ReadRAWConfig(string path, CArrayString& output)
{
 int handle = FileOpen(path, FILE_READ|FILE_TXT);
 
 if(handle >= 0)
 {
  while(!FileIsEnding(handle))
  {
   string line = FileReadString(handle);
  
   //Skip comment line
     if(StringFind(line,"#") == 0)
      continue;
      
   //Skip blank line
     if(StringLen(line)==0)
      continue;
      
   output.Add(line);
  }
  
  FileClose(handle);
 }
 else
  utilslogger.log("Unable to open file=" + path, LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
}
//+------------------------------------------------------------------+
bool MyOrderCloseBy(bool& has_child_ticket,
                    int booknum,
                    int opposite,
                    color arrow_color=clrNONE)
{
 if(OrderSelect(booknum, SELECT_BY_TICKET))
 {
  double lots_A = OrderLots();
  
  if(OrderSelect(opposite, SELECT_BY_TICKET))
  {
   double lots_B = OrderLots();
   
   bool ans = OrderCloseBy(booknum, opposite, arrow_color);
   
   if(MathAbs(lots_A - lots_B) > DBL_EPSILON)
    has_child_ticket = true;
   else
    has_child_ticket = false;
   
   return ans;
  }
 }
 
 return false;
}
/* deprecated
//+------------------------------------------------------------------+
bool MyOrderClose(uint& latency,
                  double& spread_before,
                  double& spread_after,
                  uint retries,
                  int ticket, 
                  double lots, 
                  double price, 
                  int slippage, 
                  color arrow_color=clrNONE)
{
 if(OrderSelect(ticket, SELECT_BY_TICKET))
 {
  uint counter = 0;
  bool ans = false;
 
  do
  {
   spread_before = MarketInfo(OrderSymbol(), MODE_SPREAD) / 10;
 
   uint t0 = GetTickCount();
   ans = OrderClose(ticket, lots, price, slippage, arrow_color);
   uint t1 = GetTickCount();
 
   spread_after = MarketInfo(OrderSymbol(), MODE_SPREAD) / 10;
 
   latency = t1 - t0;
   
   if(!ans) {
    int e=GetLastError();
    
    if(e==131)    //ERR_INVALID_TRADE_VOLUME
    {
     lots=MathMin(lots, OrderLots());
    }
   
    Sleep(250);
   }
  }
  while(!ans && counter++ < retries);
 
  return ans;
 }
 
 return false;
}
//+------------------------------------------------------------------+
int MyOrderSend(uint& latency,
                double& spread_before,
                double& spread_after,
                string& position,
                uint retries,
                const string symbol,
                int cmd,
                double volume,
                double price,
                int slippage,
                double sl,
                double tp,
                const string comment=NULL,
                datetime expiration=0,
                color arrow_color=clrNONE)
{
 AutoSaveArrayStringCommunity disable_trading_symbol("disable_trading_symbol");

 if(disable_trading_symbol.SearchLinear("Disable all symbols") >= 0) {
  utilslogger.log(StringConcatenate("Did not open ", symbol, ", cmd=", cmd, ", volume=", volume, " for trading is disabled"), LOG_TYPE_INFO, __LINE__, __FUNCTION__); 
  return -1;
 }
  
 if(disable_trading_symbol.SearchLinear(symbol) >= 0) {
  utilslogger.log(StringConcatenate("Did not open ", symbol, ", cmd=", cmd, ", volume=", volume, " for trading is disabled on symbol level"), LOG_TYPE_INFO, __LINE__, __FUNCTION__); 
  return -1;
 }
 
 int ticket = 0;
 uint counter = 0;

 do 
 {
  spread_before = MarketInfo(symbol,MODE_SPREAD) / 10;
 
  uint t0 = GetTickCount();
  ticket = OrderSend(symbol,cmd,volume,price,slippage,sl,tp,comment,MAGIC,expiration,arrow_color);
  uint t1 = GetTickCount();
 
  spread_after = MarketInfo(symbol,MODE_SPREAD) / 10;
 
  latency = t1 - t0;
 
  if(ticket > 0)
   position = getTradePositionID(symbol);
  else {
   position = NULL;
   Sleep(250);
  }
 }
 while(ticket <= 0 && counter++ < retries);
 
 return ticket;
} */
//+------------------------------------------------------------------+
string getTradePositionID(string symbol)
/*
Counter starts from zero
Counts all open trades and tracks the min open tim
Counts any historical trade that was closed after the min open time,
 then considering that the historical trade was part of the trade bundle,
 it updates the min open time to the open time of the historical trade
Assumes history queue is ordered by close time
*/
{
 int history_total = OrdersHistoryTotal();
 int total = OrdersTotal();
 int count = 0;
 datetime min_tstamp = INT_MAX;
 
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES) && (MAGIC==0 || OrderMagicNumber()==MAGIC))
  {
   if(OrderSymbol() == symbol) {
    min_tstamp = MathMin(min_tstamp, OrderOpenTime());
    count++;
   }
  }
 }
  
 if(min_tstamp != INT_MAX)
 {
  for(int i=history_total-1; i>=0; i--)
  {
   if(OrderSelect(i,SELECT_BY_POS,MODE_HISTORY) && (MAGIC==0 || OrderMagicNumber()==MAGIC))
   {
    if(OrderCloseTime() > min_tstamp && OrderLots() > 0 && OrderSymbol() == symbol) {
     min_tstamp = MathMin(min_tstamp, OrderOpenTime());
     count++;
    }
   }
  }
 }
 
 return convert(count);
}
//+------------------------------------------------------------------+
string convert(int count)
{
 switch(count)
 {
  case 1: return "A";
  case 2: return "B";
  case 3: return "C";
  case 4: return "D";
  case 5: return "E";
  case 6: return "F";
  case 7: return "G";
  case 8: return "H";
  case 9: return "I";
  case 10: return "J";
  case 11: return "K";
  case 12: return "L";
  case 13: return "M";
  case 14: return "N";
  case 15: return "O";
  case 16: return "P";
  case 17: return "Q";
  case 18: return "R";
  case 19: return "S";
  case 20: return "T";
  case 21: return "U";
  case 22: return "V";
  case 23: return "W";
  case 24: return "X";
  case 25: return "Y";
  case 26: return "Z";
  case 27: return "AA";
  case 28: return "AB";
  case 29: return "AC";
  case 30: return "AD";
  default: return "";
 }
}
//+------------------------------------------------------------------+
template <typename T1, typename T2>
bool IsExact(const T1& array1, const T2& array2)
{
 int total = array2.Total();
 if(array1.Total()==total)
 {
  T1 temp1;
  T2 temp2;
  temp1.AssignArray(GetPointer(array1));
  temp2.AssignArray(GetPointer(array2));
  temp1.Sort();
  temp2.Sort();
  
  for(int i=0; i<total; i++)
   if(temp1[i]!=temp2[i])
    return false;
    
  return true;
 }
 
 return false;
} 
//+------------------------------------------------------------------+
template <typename T1, typename T2>
bool IsASubsetB(const T1& A, const T2& B)
{
 if(A.Total() <= B.Total())
 {
  int total = A.Total();
  for(int i=0; i<total; i++)
   if(B.SearchLinear(A[i]) < 0)
    return false;
    
  return true;
 }

 return false;
} 
//+------------------------------------------------------------------+
int SplitArgs(string args, CArrayString& key, CArrayString& value, char delimiter1=ASCII_RECORD_SEPERATOR, char delimiter2='\t')
{
 string row[];
 StringSplit(args, delimiter1, row);
 int sz = ArraySize(row);
 
 for(int i=0; i<sz; i++)
 {
  string col[];
  int ret = StringSplit(row[i], delimiter2, col);
  if(ret >= 2) {
   key.Add(col[0]);
   value.Add(col[1]);
  }
  else if(ret==1)
   key.Add(col[0]);
  else
   utilslogger.log("Error splitting args=\"" + (string)args + "\", possible unnecessary trailing ','", LOG_TYPE_WARNING, __LINE__, __FUNCTION__);
 } 
 
 return sz;
}
//+------------------------------------------------------------------+
string GetEAName(string __FILE__macro)
{
 int delimiter = StringFind(__FILE__macro, ".");
 string ea_name = StringSubstr(__FILE__macro, 0, delimiter);
 
 StringReplace(ea_name, " ", "_");
 
 return ea_name;
}
//+------------------------------------------------------------------+
string FileStripExtension(string filename)
{
 int delimiter = StringFind(filename, ".");
 return StringSubstr(filename, 0, delimiter);
}
//+------------------------------------------------------------------+
string SemiOpen(string symbol, double lots, double sl=0, double tp=0, string comment=NULL, int magic=0, string uuid=NULL)
{
 string filename = StringConcatenate(Period(), "Minute_open", ".gif");
 string filepath = StringConcatenate("screenshots\\", OrderSymbol(), "\\", filename);
 string screenshot_name = NULL;
 
 if(MathAbs(lots) < DBL_EPSILON) {
   utilslogger.log(symbol + " lot size must be a non-zero", LOG_TYPE_LOW, __LINE__, __FUNCTION__);
   return "failed";
 }
 
 if(WindowScreenShot(filepath, 3060, 1820, -1, -1)) // && WindowScreenShot(filepathname2, 3060, 1820, -1, 4)
 {
  if(SendFTP(filepath))
   screenshot_name = filename;
  else
   utilslogger.log("Error sending screenshot, e=" + (string)GetLastError(), LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
 }
 else
  utilslogger.log("Error taking screenshot, e=" + (string)GetLastError(), LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
   
 //---
  string open_time = (string)TimeCurrent();
  StringReplace(open_time, ".", "-");
  
 //---
  string open_price = NULL;
  int digits = (int)MarketInfo(symbol, MODE_DIGITS);
  if(lots > 0) open_price = DoubleToStr(MarketInfo(symbol, MODE_ASK), digits);
  if(lots < 0) open_price = DoubleToStr(MarketInfo(symbol, MODE_BID), digits);
   
 //--- Send web request
  string headers;
  char post[], result[];
  string data =  "command=open;";
         data += "acctNum=" + (string)AccountNumber() + ";";
         data += "acctServer=" + GetServerName() + ";";
         data += "pc=" + GetMachineName() + ";";
         data += "screenshot_filename=" + screenshot_name + ";";
         data += "open_time=" + open_time + ";";
         data += "open_price=" + open_price + ";";
         data += "symbol=" + symbol + ";";
         data += "sl=" + (string)sl + ";";
         data += "tp=" + (string)tp + ";";
         data += "comment=" + comment + ";";
         data += "magic_number=" + (string)magic + ";";
         data += "requested_lots=" + (string)lots + ";";
           
  StringToCharArray(data, post, 0, WHOLE_ARRAY, CP_UTF8);
  int response = WebRequest("POST", StringConcatenate(IP_ADDRESS, URL_SENDEMAILALERT)
                                  , "", NULL, 20000, post, ArraySize(post), result, headers);
  
  string ret_uuid = CharArrayToString(result);
  
  if(response == 200)
  {
   if(uuid != "failed")
    utilslogger.log("Command web server result: " + CharArrayToString(result), LOG_TYPE_INFO, __LINE__, __FUNCTION__);
   else
    utilslogger.log("Command web server result: " + CharArrayToString(result), LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
  }
  else
   utilslogger.log("Command web server response: " + (string)response + ", e=" + (string)GetLastError(), LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
     
  return ret_uuid;
}
//+------------------------------------------------------------------+
bool SemiClose(int ticket, double lots)
{
 if(OrderSelect(ticket, SELECT_BY_TICKET))
 {
  if(OrderCloseTime() > 0) {
   utilslogger.log("ticket #" + (string)ticket + " is already closed", LOG_TYPE_LOW, __LINE__, __FUNCTION__);
   return false;
  }
  
  if(lots <= 0) {
   utilslogger.log("ticket #" + (string)ticket + ": lots=" + (string)lots + " must be a positive number", LOG_TYPE_LOW, __LINE__, __FUNCTION__);
   return false;
  }
  
  if(OrderLots() - lots < 0) {
   utilslogger.log("changing requested lots=" + (string)lots + " to OrderLots()=" + (string)OrderLots(), LOG_TYPE_INFO, __LINE__, __FUNCTION__);
   lots = OrderLots();
  }
 
  string filename = StringConcatenate(Period(), "Minute_close", ".gif");
  string filepath = StringConcatenate("screenshots\\", OrderSymbol(), "\\", filename);
  string screenshot_name = NULL;
  
  if(WindowScreenShot(filepath, 3060, 1820, -1, -1))
  {
   if(SendFTP(filepath))
    screenshot_name = filename;
   else
    utilslogger.log("Error sending screenshot, e=" + (string)GetLastError(), LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
  }
  else
   utilslogger.log("Error taking screenshot, e=" + (string)GetLastError(), LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
   
  //--- get order type
   string type = "?";
   string pips = NULL;
   double val;
   
   switch(OrderType())
   {
    case OP_BUY:
     val = (MarketInfo(OrderSymbol(), MODE_BID) - OrderOpenPrice()) / MarketInfo(OrderSymbol(), MODE_POINT);
     type = "BUY";
     pips = DoubleToStr(val / 10, 1);
     break;
     
    case OP_SELL:
     val = (OrderOpenPrice() - MarketInfo(OrderSymbol(), MODE_ASK)) / MarketInfo(OrderSymbol(), MODE_POINT);
     type = "SELL";
     pips = DoubleToStr(val / 10, 1);
     break;
   }
   
  //---
   string open_time = (string)OrderOpenTime();
   StringReplace(open_time, ".", "-");
   
  //--- Send web request
   string headers;
   char post[], result[];
   string data =  "command=close;";
          data += "acctNum=" + (string)AccountNumber() + ";";
          data += "acctServer=" + GetServerName() + ";";
          data += "pc=" + GetMachineName() + ";";
          data += "screenshot_filename=" + screenshot_name + ";";
          data += "ticket=" + (string)ticket + ";";
          data += "open_time=" + open_time + ";";
          data += "symbol=" + OrderSymbol() + ";";
          data += "requested_lots=" + (string)lots + ";";
          data += "total_lots=" + (string)OrderLots() + ";";
          data += "type=" + type + ";";
          data += "profit=" + DoubleToStr(OrderProfit(), 2) + ";";
          data += "pips=" + pips + ";";
           
    StringToCharArray(data, post, 0, WHOLE_ARRAY, CP_UTF8);
    int response = WebRequest("POST", StringConcatenate(IP_ADDRESS, URL_SENDEMAILALERT)
                                   , "", NULL, 6000, post, ArraySize(post), result, headers);
    if(response == 200)
    {
     if(CharArrayToString(result) != "success")
      utilslogger.log("Command web server result: " + CharArrayToString(result), LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
    }
    else
     utilslogger.log("Command web server response: " + (string)response + ", e=" + (string)GetLastError(), LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
     
    return true;
 }
 else
  utilslogger.log("Error selecting #" + (string)ticket + ", e=" + (string)GetLastError(), LOG_TYPE_HIGH, __LINE__, __FUNCTION__);

 return false;
}