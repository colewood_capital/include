//+------------------------------------------------------------------+
//|                                            FileMessengerBase.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Jamal Cole"
#property strict

#ifndef FILEBASE
#define FILEBASE
   #include <FileBase.mqh>
#endif 

#ifndef ARRAYSTRING
#define ARRAYSTRING
   #include <Arrays/ArrayString.mqh>
#endif 

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
#endif 

class FileMessengerBase : public FileBase
{
 private:
  CArrayString _key;
  CArrayString _value;
  int p_write_handle;
  Logger logger;
  
 protected:
  int read(CArrayString& key, CArrayString& value);
  void write(string key, string message);
  void send();
  
  FileMessengerBase(string path, string extension, string tag="");
};
//-------------------------------------------------------------------+
void FileMessengerBase::send()
{
 int key_total = _key.Total();
 if(key_total == _value.Total())
 {
  for(int i=0; i<key_total; i++)
   FileWrite(p_write_handle, _key[i], "=", _value[i]);
   
  _key.Clear();
  _value.Clear();
  logger.log("clearing buffers _key and _value", LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
 }
 else
  logger.log("non-matching key sizes in " + filename(), LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
}
//-------------------------------------------------------------------+
void FileMessengerBase::write(string key, string message)
{
 int key_total = _key.Total();
 int value_total = _value.Total();
 
 if(key_total==0 && value_total==0) {
  p_write_handle = write_handle();
  logger.log("get new handle=" + (string)p_write_handle, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
 }
 
 _key.Add(key);
 _value.Add(message);
}
//-------------------------------------------------------------------+
int FileMessengerBase::read(CArrayString &key,CArrayString &value)
{
 int handle = read_handle(true);
 int counter = 0;
 
 if(handle >= 0)
 {
  while(!FileIsEnding(handle))
  {
   string result[];
   
   string line = FileReadString(handle);
   StringSplit(line,'=',result);
  
   if(ArraySize(result)==2)
   {
    result[0] = StringTrimLeft(StringTrimRight(result[0]));
    result[1] = StringTrimLeft(StringTrimRight(result[1]));
    key.Add(result[0]);
    value.Add(result[1]);
    counter++;
   }
   else {
    logger.log(filename() + " - Invalid array size = " + (string)ArraySize(result), LOG_TYPE_WARNING, __LINE__, __FUNCTION__);
    logger.log("line=" + line, LOG_TYPE_WARNING, __LINE__, __FUNCTION__);
   }
  }
 } 
 
 LOG_EXTENSIVE_1_RAW(key);
 LOG_EXTENSIVE_1_RAW(value);
 
 return counter;
}
//-------------------------------------------------------------------+
FileMessengerBase::FileMessengerBase(string path,string extension,string tag="")
  : FileBase(path, extension, tag), logger(__FILE__, "helper_processes")
{

}