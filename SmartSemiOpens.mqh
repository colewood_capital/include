//+------------------------------------------------------------------+
//|                                               SmartSemiOpens.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
#endif

#ifndef UTILS
#define UTILS
   #include <Utils.mqh>
#endif

enum RENKO_SWITCH { RENKO_SWITCH_NIL, RENKO_SWITCH_RED, RENKO_SWITCH_GREEN };

class OrderOpenEngine
{
 private:
  RENKO_SWITCH p_switch;
  bool all_tickets_in_profit();
  
 public:
  string renko_switch(int pips, string order_comment);
  string intital_retracement(int ticket, string order_comment);
  string simple_pyramid(int ticket, double pips, string order_comment);
  OrderOpenEngine() : p_switch(RENKO_SWITCH_NIL) {};
};

string OrderOpenEngine::simple_pyramid(int ticket, double pips, string order_comment)
{
 string uuid = NULL;
 
 if(all_tickets_in_profit())
 {
  if(OrderSelect(ticket,SELECT_BY_TICKET))
  {
   string ord_symbol = OrderSymbol();
   int type = OrderType();
   double open_price = OrderOpenPrice();
   double prc = iClose(ord_symbol, 0, 0);
  
   switch(type)
   {
    case OP_BUY:
     if(prc - open_price >= pips * Point * 10) {
      //new_ticket = OrderSend(ord_symbol, OP_BUY, 1.0, Ask, 5, 0, 0, order_comment);
      uuid = SemiOpen(ord_symbol, 0.1, 0, 0, order_comment);
      Alert("Open " + uuid + " in " + __FUNCTION__);
     }
     break;
    
    case OP_SELL:
     if(open_price - prc >= pips * Point * 10) {
      //new_ticket = OrderSend(ord_symbol, OP_SELL, 1.0, Bid, 5, 0, 0, order_comment);
      uuid = SemiOpen(ord_symbol, -0.1, 0, 0, order_comment);
      Alert("Open " + uuid + " in " + __FUNCTION__);
     }
     break;
   }
  }
 }
 
 return uuid;
}

bool OrderOpenEngine::all_tickets_in_profit(void)
{
 int total = OrdersTotal();
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(i,SELECT_BY_POS) && OrderSymbol()==Symbol())
  {
   if(OrderProfit() < 0)
    return false;
  }
 }
 
 return true;
}

string OrderOpenEngine::intital_retracement(int ticket, string order_comment)
{
 //int new_ticket = -1;
 string uuid = NULL;
 
 if(OrderSelect(ticket,SELECT_BY_TICKET))
 {
  string ord_symbol = OrderSymbol();
  double envelope, band;
  double limit_prc;
  double ma = iMA(ord_symbol, 0, 50, 0, MODE_SMA, PRICE_CLOSE, 0);
  double prc = iClose(ord_symbol, 0, 0);
  
  switch(OrderType())
  {
   case OP_BUY:
    envelope = iEnvelopes(ord_symbol, 0, 50, MODE_SMA, 0, PRICE_CLOSE, 0.3, MODE_LOWER, 0);
    band = iBands(ord_symbol, 0, 50, 2.5, 0, PRICE_CLOSE, MODE_LOWER, 0);
    limit_prc = (ma * 0.25) + (MathMin(envelope, band) * 0.75);
    
    if(prc < limit_prc) {
     //new_ticket = OrderSend(ord_symbol, OP_BUY, 1, Ask, 5, 0, 0, order_comment);
     uuid = SemiOpen(ord_symbol, 0.1, 0, 0, order_comment);
     Alert("Open " + uuid + " in " + __FUNCTION__);
    }
    
    break;
    
   case OP_SELL:
    envelope = iEnvelopes(ord_symbol, 0, 50, MODE_SMA, 0, PRICE_CLOSE, 0.3, MODE_UPPER, 0);
    band = iBands(ord_symbol, 0, 50, 2.5, 0, PRICE_CLOSE, MODE_UPPER, 0);
    limit_prc = (MathMax(envelope, band) * 0.75) + (ma * 0.25);
    
    if(prc > limit_prc) {
     //new_ticket = OrderSend(ord_symbol, OP_SELL, 1, Bid, 5, 0, 0, order_comment);
     uuid = SemiOpen(ord_symbol, -0.1, 0, 0, order_comment);
     Alert("Open " + uuid + " in " + __FUNCTION__);
    }
   
    break;
  }
 }
 
 return uuid;
}

string OrderOpenEngine::renko_switch(int pips, string order_comment)
{
 string symbol = Symbol();
 double high = iCustom(symbol, 0, "Renko Bars", pips, 0, 1);
 double low = iCustom(symbol, 0, "Renko Bars", pips, 1, 1);
 double prev_high = iCustom(symbol, 0, "Renko Bars", pips, 0, 2);
 double prev_low = iCustom(symbol, 0, "Renko Bars", pips, 1, 2); 
 string uuid = NULL;
 
 //--- low > high => red bar
 if(low > high && prev_low < prev_high && (p_switch==RENKO_SWITCH_GREEN || p_switch==RENKO_SWITCH_NIL)) {
  CloseAll();
  //ticket = OrderSend(symbol, OP_BUY, 1.0, Ask, 5, 0, 0, "RENKO");
  uuid = SemiOpen(symbol, 0.1, 0, 0, order_comment);
  Alert("Open " + uuid + " in " + __FUNCTION__);
  p_switch = RENKO_SWITCH_RED;
 }
     
 //--- high > low => green bar
 if(high > low && prev_high < prev_low && (p_switch==RENKO_SWITCH_RED || p_switch==RENKO_SWITCH_NIL)) {
  CloseAll();
  //ticket = OrderSend(symbol, OP_SELL, 1.0, Bid, 5, 0, 0, "RENKO");
  uuid = SemiOpen(symbol, -0.1, 0, 0, order_comment);
  Alert("Open " + uuid + " in " + __FUNCTION__);
  p_switch = RENKO_SWITCH_GREEN;
 }
 
 return uuid;
}

OrderOpenEngine order_open_engine;

//+------------------------------------------------------------------+
void CloseAll()
{
 int total = OrdersTotal();
 for(int i=total-1; i>=0; i--)
 {
  if(OrderSelect(i,SELECT_BY_POS) && OrderSymbol()==Symbol())
  {
   int ticket = OrderTicket();
   int type = OrderType();
   double prc = (type==OP_BUY) ? Bid : (type==OP_SELL) ? Ask : -1;
   bool ans = OrderClose(ticket, OrderLots(), prc, 5);
   if(ans)
    Alert("Closed #" + (string)ticket + " in " + __FUNCTION__);
  }
 }
}