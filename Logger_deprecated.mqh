//+------------------------------------------------------------------+
//|                                                      Logging.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#ifndef ARRAYINT
#define ARRAYINT
   #include <Arrays/ArrayInt.mqh>
#endif

#ifndef ARRAYSTRING
#define ARRAYSTRING
   #include <Arrays/ArrayString.mqh>
#endif

#ifndef AUTOSAVEARRAYSTRING
#define AUTOSAVEARRAYSTRING
   #include <Arrays/AutoSaveArrayString.mqh>
#endif 

#ifndef ARRAYDATETIME
#define ARRAYDATETIME
   #include <Arrays/ArrayDatetime.mqh>
#endif

#ifndef BASEDIR
#define BASEDIR
 extern string BASE_DIR = "";
#endif

#ifndef STDLIB
#define STDLIB 
   #include <stdlib.mqh>
#endif 

#ifndef ENUMS
#define ENUMS
   #include <Super\Enums.mqh>
#endif 

#define LOG_DEBUG(message)                               logger.log(message, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__)
#define LOG_EXTENSIVE(message)                           logger.log(message, LOG_TYPE_EXTENSIVE, __LINE__, __FUNCTION__)
#define LOG_INFO(message)                                logger.log(message, LOG_TYPE_INFO, __LINE__, __FUNCTION__)
#define LOG_WARNING(message)                             logger.log(message, LOG_TYPE_WARNING, __LINE__, __FUNCTION__)
#define LOG_DEV_WARNING(message)                         logger.log(message, LOG_TYPE_DEV_WARNING, __LINE__, __FUNCTION__)
#define LOG_LOW(message)                                 logger.log(message, LOG_TYPE_LOW, __LINE__, __FUNCTION__)
#define LOG_HIGH(message)                                logger.log(message, LOG_TYPE_HIGH, __LINE__, __FUNCTION__)
#define LOG_NETWORK(message)                             logger.log(message, LOG_TYPE_NETWORK, __LINE__, __FUNCTION__)

//--- ERROR messages: attach error description to end of message
#define LOG_DEBUG_ERROR(message)                         LOG_DEBUG(message+", e="+ErrorDescription(GetLastError()))
#define LOG_INFO_ERROR(message)                          LOG_INFO(message + ", e="+ErrorDescription(GetLastError()))
#define LOG_LOW_ERROR(message)                           LOG_LOW(message+", e="+ErrorDescription(GetLastError()))
#define LOG_HIGH_ERROR(message)                          LOG_HIGH(message+", e="+ErrorDescription(GetLastError()))

//--- RAW messages: override_filtering=true
#define LOG_DEBUG_RAW(message)                           logger.log(message, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__, true)
#define LOG_EXTENSIVE_RAW(message)                       logger.log(message, LOG_TYPE_EXTENSIVE, __LINE__, __FUNCTION__, true)
#define LOG_INFO_RAW(message)                            logger.log(message, LOG_TYPE_INFO, __LINE__, __FUNCTION__, true)
#define LOG_LOW_RAW(message)                             logger.log(message, LOG_TYPE_LOW, __LINE__, __FUNCTION__, true)
#define LOG_HIGH_RAW(message)                            logger.log(message, LOG_TYPE_HIGH, __LINE__, __FUNCTION__, true)

//--- ERROR and RAW
#define LOG_INFO_ERROR_RAW(message)                      logger.log(message + ", e="+ErrorDescription(GetLastError()), LOG_TYPE_INFO, __LINE__, __FUNCTION__, true)
#define LOG_LOW_ERROR_RAW(message)                       logger.log(message+", e="+ErrorDescription(GetLastError()), LOG_TYPE_LOW, __LINE__, __FUNCTION__, true)
#define LOG_HIGH_ERROR_RAW(message)                      logger.log(message+", e="+ErrorDescription(GetLastError()), LOG_TYPE_HIGH, __LINE__, __FUNCTION__, true)

//--- Log one variable
#define LOG_DEBUG_1(variable)                            logger.log_variable(#variable, variable, LOG_TYPE_DEBUG, __LINE__,__FUNCTION__)
#define LOG_DEBUG_1_RAW(variable)                        logger.log_variable(#variable, variable, LOG_TYPE_DEBUG, __LINE__,__FUNCTION__,true)
#define LOG_EXTENSIVE_1(variable)                        logger.log_variable(#variable, variable, LOG_TYPE_EXTENSIVE, __LINE__,__FUNCTION__)
#define LOG_EXTENSIVE_1_RAW(variable)                    logger.log_variable(#variable, variable, LOG_TYPE_EXTENSIVE, __LINE__,__FUNCTION__,true)
#define LOG_HIGH_1(variable)                             logger.log_variable(#variable, variable, LOG_TYPE_HIGH, __LINE__,__FUNCTION__)
#define LOG_HIGH_1_ERROR(variable)                       logger.log_variable(#variable, variable, "e", ErrorDescription(GetLastError()), LOG_TYPE_HIGH, __LINE__, __FUNCTION__, "")

//--- Log two variables
#define LOG_EXTENSIVE_2(var1,compare,var2)               logger.log_variable(#var1,var1,#var2,var2,LOG_TYPE_EXTENSIVE,__LINE__,__FUNCTION__,compare)
#define LOG_EXTENSIVE_2_RAW(var1,compare,var2)           logger.log_variable(#var1,var1,#var2,var2,LOG_TYPE_EXTENSIVE,__LINE__,__FUNCTION__,compare,true)
#define LOG_HIGH_2(var1,compare,var2)                    logger.log_variable(#var1,var1,#var2,var2,LOG_TYPE_HIGH,__LINE__,__FUNCTION__,compare)

//--- Log three variables
#define LOG_EXTENSIVE_3_RAW(var1,var2,var3)              logger.log_variable(#var1,var1,#var2,var2,#var3,var3,LOG_TYPE_EXTENSIVE,__LINE__,__FUNCTION__,true)
#define LOG_HIGH_3(var1,var2,var3)                       logger.log_variable(#var1,var1,#var2,var2,#var3,var3,LOG_TYPE_HIGH,__LINE__,__FUNCTION__)


bool DISABLE_LOGGING = false;
    
string LOGGER_TEMPLATE = 
"# Prority delay is minutes before an identical message will be logged\n"
"# LOW, HIGH, NETWORK messages always printed to terminal logs; additional option to open alert message\n"
"\n"
"# DEBUG		         Typically of interest only when diagnosing problems.\n"
"# DEBUG_EXTENSIVE   Detailed debug messages"
"# INFO		         Confirmation that things are working as expected.\n"
"# WARNING	         An indication that something unexpected happened, or indicative of some problem in the near future (e.g. ‘disk space low’). The software is still working as expected.\n"
"# DEV_WARNING       A warning message intended for dev use only\n"
"# LOW		         An error that is not critical to trading operations.\n"
"# HIGH   	         A serious error, indicating that the program itself may be unable to continue running.\n"
"# NETWORK           A problem occurring with connect to terminal and/or internet, etc.\n"
"\n"
"LOGGING_DEBUG = TRUE\n"
"LOGGING_DEBUG_EXTENSIVE = TRUE\n"
"LOGGING_INFO = TRUE\n"
"LOGGING_WARNING = TRUE\n"
"LOGGING_DEV_WARNING = TRUE\n"
"ALERT_LOGGING_LOW = TRUE\n"
"ALERT_LOGGING_HIGH = TRUE\n"
"ALERT_LOGGING_NETWORK = TRUE\n"
"\n"
"SITE_LOGGING_INFO = FALSE\n"
"SITE_LOGGING_WARNING = FALSE\n"
"SITE_LOGGING_DEV_WARNING = FALSE\n"
"SITE_LOGGING_LOW = TRUE\n"
"SITE_LOGGING_HIGH = TRUE\n"
"SITE_LOGGING_NETWORK = TRUE\n"
"\n"
"LOGGING_DEBUG_DELAY = 5\n"
"LOGGING_EXTENSIVE_DELAY = 5\n"
"LOGGING_INFO_DELAY = 5\n"
"LOGGING_WARNING_DELAY = 5\n"
"LOGGING_DEV_WARNING_DELAY = 5\n"
"LOGGING_LOW_DELAY = 2.5\n"
"LOGGING_HIGH_DELAY = 1\n"
"LOGGING_NETWORK_DELAY = 0\n"
"\n"
"SHOW_LOG_VARIABLES_ON_EA_ATTACH = FALSE";

class Logger
{
 protected:
  CArrayDatetime timestamp;
  CArrayString msg;
  CArrayInt p_uuid;
  CArrayInt p_message_type;
  static CArrayString p_static_config_files;
  string p_program_name;
  string p_config_file_path;
  string p_display_name;
  
  template<typename T>
  void string_repr(const T &val, string &OUT_repr[], int &size);
  
  template<typename T>
  string string_repr(T val);
  
  template<typename T>
  void string_repr(const T &val[], string &OUT_repr[], int &size);
  
  bool LOGGING_DEBUG;
  bool LOGGING_EXTENSIVE;
  bool LOGGING_INFO;
  bool LOGGING_WARNING;
  bool LOGGING_DEV_WARNING;
  bool ALERT_LOGGING_LOW;
  bool ALERT_LOGGING_HIGH;
  bool ALERT_LOGGING_NETWORK;
  
  bool SITE_LOGGING_INFO;
  bool SITE_LOGGING_WARNING;
  bool SITE_LOGGING_DEV_WARNING;
  bool SITE_LOGGING_LOW;
  bool SITE_LOGGING_HIGH;
  bool SITE_LOGGING_NETWORK;
  
  double LOGGING_DEBUG_DELAY;
  double LOGGING_EXTENSIVE_DELAY;  
  double LOGGING_INFO_DELAY;
  double LOGGING_WARNING_DELAY;
  double LOGGING_DEV_WARNING_DELAY;
  double LOGGING_LOW_DELAY;
  double LOGGING_HIGH_DELAY;
  double LOGGING_NETWORK_DELAY;
  
  bool SHOW_LOG_VARIABLES_ON_EA_ATTACH;
 
 public:
  void log(const string message, LOG_TYPE, int __LINE__macro, string __FUNCTION__macro, bool override_filtering, uint uuid);
  
  //--- log_variable: simple types, CArrayObj, arrays of simple types
   template <typename T1>
   void log_variable(const string var1_name, T1 var1, LOG_TYPE, int __LINE__macro, string __FUNCTION__macro, bool override_filtering, uint uuid);
  
   template <typename T1>
   void log_variable(const string var1_name, const T1 &var1, LOG_TYPE, int __LINE__macro, string __FUNCTION__macro, bool override_filtering, uint uuid);
  
   template <typename T1>
   void log_variable(const string var1_name, const T1 &var1[], LOG_TYPE, int __LINE__macro, string __FUNCTION__macro, bool override_filtering, uint uuid);
  
  template <typename T1, typename T2>
  void log_variable(string var1_name, T1 var1, string var2_name, T2 var2, LOG_TYPE, int __LINE__macro, string __FUNCTION__macro, string comparison, bool override_filtering, uint uuid);
  
  template <typename T1, typename T2, typename T3>
  void log_variable(string var1_name, T1 var1, string var2_name, T2 var2, string var3_name, T3 var3, LOG_TYPE, int __LINE__macro, string __FUNCTION__macro, bool override_filtering, uint uuid);
  
  template <typename T1, typename T2, typename T3, typename T4>
  void log_variable(string var1_name, T1 var1, string var2_name, T2 var2, string var3_name, T3 var3, string var4_name, T4 var4, LOG_TYPE, int __LINE__macro, string __FUNCTION__macro, bool override_filtering, uint uuid);
  
  template <typename T1, typename T2, typename T3, typename T4, typename T5>
  void log_variable(string var1_name, T1 var1, string var2_name, T2 var2, string var3_name, T3 var3, string var4_name, T4 var4, string var5_name, T5 var5, LOG_TYPE, int __LINE__macro, string __FUNCTION__macro, bool override_filtering, uint uuid);
  
  void disable(LOG_TYPE type);
  void enable(LOG_TYPE type);
  void load();
 
 Logger(string __FILE__macro, string config_file_name=NULL);
};

//--------------------------------------------------------------
template <typename T1, typename T2, typename T3, typename T4, typename T5>
void Logger::log_variable(string var1_name, T1 var1, string var2_name, T2 var2, string var3_name, T3 var3, string var4_name, T4 var4, string var5_name, T5 var5, LOG_TYPE log_type, int __LINE__macro, string __FUNCTION__macro, bool override_filtering=false, uint uuid=0)
{
 string str1=NULL, str2=NULL, str3=NULL, str4=NULL, str5=NULL;

 string type1=typename(T1), type2=typename(T2), type3=typename(T3), type4=typename(T4), type5=typename(T5);
 
 if(type1=="datetime")
  str1=TimeToStr((datetime)var1, TIME_DATE|TIME_MINUTES|TIME_SECONDS);  //cast to datetime to remove complier warning message if T is type!=datetime
 else
  str1=(string)var1;
  
  if(type2=="datetime")
  str2=TimeToStr((datetime)var2, TIME_DATE|TIME_MINUTES|TIME_SECONDS);  //cast to datetime to remove complier warning message if T is type!=datetime
 else
  str2=(string)var2;
  
 if(type3=="datetime")
  str3=TimeToStr((datetime)var3, TIME_DATE|TIME_MINUTES|TIME_SECONDS);  //cast to datetime to remove complier warning message if T is type!=datetime
 else
  str3=(string)var3;
  
 if(type4=="datetime")
  str4=TimeToStr((datetime)var4, TIME_DATE|TIME_MINUTES|TIME_SECONDS);  //cast to datetime to remove complier warning message if T is type!=datetime
 else
  str4=(string)var4;
  
 if(type5=="datetime")
  str5=TimeToStr((datetime)var5, TIME_DATE|TIME_MINUTES|TIME_SECONDS);  //cast to datetime to remove complier warning message if T is type!=datetime
 else
  str5=(string)var5;
 
 log("[VAR] " + var1_name + "=" + str1 + ", " + var2_name + "=" + str2 + ", " + var3_name + "=" + str3 + ", " + var4_name + "=" + str4 + var5_name + "=" + str5, log_type, __LINE__macro, __FUNCTION__macro, override_filtering, uuid);
}
//--------------------------------------------------------------
template <typename T1, typename T2, typename T3, typename T4>
void Logger::log_variable(string var1_name, T1 var1, string var2_name, T2 var2, string var3_name, T3 var3, string var4_name, T4 var4, LOG_TYPE log_type, int __LINE__macro, string __FUNCTION__macro, bool override_filtering=false, uint uuid=0)
{
 string str1=NULL, str2=NULL, str3=NULL, str4=NULL;

 string type1=typename(T1), type2=typename(T2), type3=typename(T3), type4=typename(T4);
 
 if(type1=="datetime")
  str1=TimeToStr((datetime)var1, TIME_DATE|TIME_MINUTES|TIME_SECONDS);  //cast to datetime to remove complier warning message if T is type!=datetime
 else
  str1=(string)var1;
  
  if(type2=="datetime")
  str2=TimeToStr((datetime)var2, TIME_DATE|TIME_MINUTES|TIME_SECONDS);  //cast to datetime to remove complier warning message if T is type!=datetime
 else
  str2=(string)var2;
  
 if(type3=="datetime")
  str3=TimeToStr((datetime)var3, TIME_DATE|TIME_MINUTES|TIME_SECONDS);  //cast to datetime to remove complier warning message if T is type!=datetime
 else
  str3=(string)var3;
  
 if(type4=="datetime")
  str4=TimeToStr((datetime)var4, TIME_DATE|TIME_MINUTES|TIME_SECONDS);  //cast to datetime to remove complier warning message if T is type!=datetime
 else
  str4=(string)var4;
 
 log("[VAR] " + var1_name + "=" + str1 + ", " + var2_name + "=" + str2 + ", " + var3_name + "=" + str3 + ", " + var4_name + "=" + str4, log_type, __LINE__macro, __FUNCTION__macro, override_filtering, uuid);
}
//--------------------------------------------------------------
template <typename T1, typename T2, typename T3>
void Logger::log_variable(string var1_name, T1 var1, string var2_name, T2 var2, string var3_name, T3 var3, LOG_TYPE log_type, int __LINE__macro, string __FUNCTION__macro, bool override_filtering=false, uint uuid=0)
{
 string str1=NULL, str2=NULL, str3=NULL;

 string type1=typename(T1), type2=typename(T2), type3=typename(T3);
 
 if(type1=="datetime")
  str1=TimeToStr((datetime)var1, TIME_DATE|TIME_MINUTES|TIME_SECONDS);  //cast to datetime to remove complier warning message if T is type!=datetime
 else
  str1=(string)var1;
  
  if(type2=="datetime")
  str2=TimeToStr((datetime)var2, TIME_DATE|TIME_MINUTES|TIME_SECONDS);  //cast to datetime to remove complier warning message if T is type!=datetime
 else
  str2=(string)var2;
  
 if(type3=="datetime")
  str3=TimeToStr((datetime)var3, TIME_DATE|TIME_MINUTES|TIME_SECONDS);  //cast to datetime to remove complier warning message if T is type!=datetime
 else
  str3=(string)var3;
 
 log("[VAR] " + var1_name + "=" + str1 + ", " + var2_name + "=" + str2 + ", " + var3_name + "=" + str3, log_type, __LINE__macro, __FUNCTION__macro, override_filtering, uuid);
}
//--------------------------------------------------------------
template <typename T1, typename T2>
void Logger::log_variable(string var1_name, T1 var1, string var2_name, T2 var2, LOG_TYPE log_type, int __LINE__macro, string __FUNCTION__macro, string comparison="", bool override_filtering=false, uint uuid=0)
{
 string str1=NULL, str2=NULL;
 string type1=typename(T1), type2=typename(T2);
 
 if(type1=="datetime")
  str1=TimeToStr((datetime)var1, TIME_DATE|TIME_MINUTES|TIME_SECONDS);  //cast to datetime to remove complier warning message if T is type!=datetime
 else
  str1=(string)var1;
  
  if(type2=="datetime")
  str2=TimeToStr((datetime)var2, TIME_DATE|TIME_MINUTES|TIME_SECONDS);  //cast to datetime to remove complier warning message if T is type!=datetime
 else
  str2=(string)var2;
  
 log("[VAR] " + var1_name + "=" + str1 + ", " + var2_name + "=" + str2, log_type, __LINE__macro, __FUNCTION__macro, override_filtering, uuid);
}
//--------------------------------------------------------------
template <typename T1>
void Logger::log_variable(const string var1_name, const T1 &var1[], LOG_TYPE log_type, int __LINE__macro, string __FUNCTION__macro, bool override_filtering=false, uint uuid=0)
{
  string str1=NULL;
  string type1 = typename(T1);
  string repr[];
  int size;
  
  string_repr(var1, repr, size);
  
  string output="";
  for(int i=0; i<size; i++)
  {
   output += "[" + (string)i + "]=" + repr[i] + ", ";
  }
  
  //--- Remove last comma
   int len=StringLen(output);
   if(len > 0)
    output = StringSubstr(output, 0, len-2);

  log("[VAR] " + var1_name + ": " + output, log_type, __LINE__macro, __FUNCTION__macro, override_filtering, uuid);
}
//--------------------------------------------------------------
template <typename T1>
void Logger::log_variable(const string var1_name, const T1 &var1, LOG_TYPE log_type, int __LINE__macro, string __FUNCTION__macro, bool override_filtering=false, uint uuid=0)
{
  string str1=NULL;
  string type1 = typename(T1);
  string repr[];
  int size;
  
  string_repr(var1, repr, size);
  
  string output="";
  for(int i=0; i<size; i++)
  {
   output += "[" + (string)i + "]=" + repr[i] + ", ";
  }
  
  //--- Remove last comma
   int len=StringLen(output);
   if(len > 0)
    output = StringSubstr(output, 0, len-2);
  
  log("[VAR] " + var1_name + "=" + output, log_type, __LINE__macro, __FUNCTION__macro, override_filtering, uuid);
}
//--------------------------------------------------------------
template <typename T1>
void Logger::log_variable(const string var1_name, T1 var1, LOG_TYPE log_type, int __LINE__macro, string __FUNCTION__macro, bool override_filtering=false, uint uuid=0)
{
  string str1=string_repr(var1);

  log("[VAR] " + var1_name + "=" + str1, log_type, __LINE__macro, __FUNCTION__macro, override_filtering, uuid);
}
//--------------------------------------------------------------
template <typename T>
void Logger::string_repr(const T &val, string &OUT_repr[], int &size)
{
 string type = typename(T);
 size=val.Total();
 ArrayResize(OUT_repr, size);
 
 if(type=="CArrayDatetime")
 {
  for(int i=0; i<size; i++)
   OUT_repr[i]=TimeToStr((datetime)val[i], TIME_DATE|TIME_MINUTES|TIME_SECONDS);  //cast to datetime to remove complier warning message if T is type!=datetime
 }
 else
 {
  for(int i=0; i<size; i++)
   OUT_repr[i]=(string)val[i];
 }
  
  return;
}
//--------------------------------------------------------------
template <typename T>
void Logger::string_repr(const T &val[], string &OUT_repr[], int &size)
{
 string type = typename(T);
 size=ArraySize(val);
 ArrayResize(OUT_repr, size);
 
 if(type=="datetime")
 {
  for(int i=0; i<size; i++)
   OUT_repr[i]=TimeToStr((datetime)val[i], TIME_DATE|TIME_MINUTES|TIME_SECONDS);  //cast to datetime to remove complier warning message if T is type!=datetime
 }
 else
 {
  for(int i=0; i<size; i++)
   OUT_repr[i]=(string)val[i];
 }
 
 return;
}
//--------------------------------------------------------------
template <typename T>
string Logger::string_repr(T val)
{
 string type = typename(T);
 
 if(type=="datetime")
  return TimeToStr((datetime)val, TIME_DATE|TIME_MINUTES|TIME_SECONDS);  //cast to datetime to remove complier warning message if T is type!=datetime
 else 
  return (string)val;
  
 return "";
}
//--------------------------------------------------------------
void Logger::disable(LOG_TYPE type)
{
 switch(type)
 {
  case LOG_TYPE_DEBUG:
   LOGGING_DEBUG = false;
   break;
   
  case LOG_TYPE_EXTENSIVE:
   LOGGING_EXTENSIVE = false;
   break;
   
  case LOG_TYPE_INFO:
   LOGGING_INFO = false;
   break;
   
  case LOG_TYPE_WARNING:
   LOGGING_WARNING = false;
   break;
   
  case LOG_TYPE_DEV_WARNING:
   LOGGING_DEV_WARNING = false;
   break;
   
  case LOG_TYPE_LOW:
   ALERT_LOGGING_LOW = false;
   break;
   
  case LOG_TYPE_HIGH:
   ALERT_LOGGING_HIGH = false;
   break;
  
  case LOG_TYPE_NETWORK:
   ALERT_LOGGING_NETWORK = false;
   break;
 }
}
//--------------------------------------------------------------
void Logger::enable(LOG_TYPE type)
{
 switch(type)
 {
  case LOG_TYPE_DEBUG:
   LOGGING_DEBUG = true;
   break;
   
  case LOG_TYPE_EXTENSIVE:
   LOGGING_EXTENSIVE = true;
   break;
   
  case LOG_TYPE_INFO:
   LOGGING_INFO = true;
   break;
   
  case LOG_TYPE_WARNING:
   LOGGING_WARNING = true;
   break;
   
  case LOG_TYPE_DEV_WARNING:
   LOGGING_DEV_WARNING = true;
   break;
   
  case LOG_TYPE_LOW:
   ALERT_LOGGING_LOW = true;
   break;
   
  case LOG_TYPE_HIGH:
   ALERT_LOGGING_HIGH = true;
   break;
  
  case LOG_TYPE_NETWORK:
   ALERT_LOGGING_NETWORK = true;
   break;
 }
}
//--------------------------------------------------------------

//initalization of of static variables at the global level
CArrayString Logger::p_static_config_files;

//--------------------------------------------------------------
void Logger::load()
{
 string filename = BASE_DIR + "\\logger\\" + p_program_name + "_config.txt";
 
 if(CreateFileIfNotExists(filename)) {
  int handle = FileOpen(filename,FILE_WRITE);
  if(handle >= 0)
  {
   string display = "Creating the following file: Files\\" + filename;
   FileWrite(handle, LOGGER_TEMPLATE);
   MessageBox(display, "INFO", MB_ICONINFORMATION);
   FileClose(handle);
  }
 }
 
 CArrayString variable, value;
 string expected[] = {  "LOGGING_DEBUG",
                        "LOGGING_DEBUG_EXTENSIVE",
                        "LOGGING_INFO",
                        "LOGGING_WARNING",
                        "LOGGING_DEV_WARNING",
                        "ALERT_LOGGING_LOW",
                        "ALERT_LOGGING_HIGH",
                        "ALERT_LOGGING_NETWORK",
                        
                        "SITE_LOGGING_INFO",
                        "SITE_LOGGING_WARNING",
                        "SITE_LOGGING_DEV_WARNING",
                        "SITE_LOGGING_LOW",
                        "SITE_LOGGING_HIGH",
                        "SITE_LOGGING_NETWORK",
                        
                        "LOGGING_DEBUG_DELAY",
                        "LOGGING_EXTENSIVE_DELAY",
                        "LOGGING_INFO_DELAY",
                        "LOGGING_WARNING_DELAY",
                        "LOGGING_DEV_WARNING_DELAY",
                        "LOGGING_LOW_DELAY",
                        "LOGGING_HIGH_DELAY",
                        "LOGGING_NETWORK_DELAY",
                        
                        "SHOW_LOG_VARIABLES_ON_EA_ATTACH"};
 string not_found[];
 
 ConfigFileLoader(filename, variable, value, expected, not_found);
 
 int err_size = ArraySize(not_found);
 if(err_size > 0)
 {
  string err_msg = "Load failed for unable to load following variable(s) from " + filename + ":\n";
  for(int i=0; i<err_size; i++)
   err_msg += not_found[i] + "\n";
  MessageBox(err_msg,"CRITICAL ERROR",MB_OK|MB_ICONERROR);
  ExpertRemove();
 }

 int expected_total = ArraySize(expected);
 for(int i=0; i<expected_total; i++) 
 {
  int pos = variable.SearchLinear(expected[i]);
  
  if(expected[i]=="LOGGING_DEBUG")
   LOGGING_DEBUG = (bool)value[pos];
  else if(expected[i]=="LOGGING_DEBUG_EXTENSIVE")
   LOGGING_EXTENSIVE = (bool)value[pos];
  else if(expected[i]=="LOGGING_INFO")
   LOGGING_INFO = (bool)value[pos];
  else if(expected[i]=="LOGGING_WARNING")
   LOGGING_WARNING = (bool)value[pos];
  else if(expected[i]=="LOGGING_DEV_WARNING")
   LOGGING_DEV_WARNING = (bool)value[pos];
  else if(expected[i]=="ALERT_LOGGING_LOW")
   ALERT_LOGGING_LOW = (bool)value[pos];
  else if(expected[i]=="ALERT_LOGGING_HIGH")
   ALERT_LOGGING_HIGH = (bool)value[pos];
  else if(expected[i]=="ALERT_LOGGING_NETWORK")
   ALERT_LOGGING_NETWORK = (bool)value[pos];
    
  else if(expected[i]=="SITE_LOGGING_INFO")
   SITE_LOGGING_INFO = (bool)value[pos];
  else if(expected[i]=="SITE_LOGGING_WARNING")
   SITE_LOGGING_WARNING = (bool)value[pos];
  else if(expected[i]=="SITE_LOGGING_DEV_WARNING")
   SITE_LOGGING_DEV_WARNING = (bool)value[pos];
  else if(expected[i]=="SITE_LOGGING_LOW")
   SITE_LOGGING_LOW = (bool)value[pos];
  else if(expected[i]=="SITE_LOGGING_HIGH")
   SITE_LOGGING_HIGH = (bool)value[pos];
  else if(expected[i]=="SITE_LOGGING_NETWORK")
   SITE_LOGGING_NETWORK = (bool)value[pos];
   
  else if(expected[i]=="LOGGING_DEBUG_DELAY")
   LOGGING_DEBUG_DELAY = (double)value[pos];
  else if(expected[i]=="LOGGING_EXTENSIVE_DELAY")
   LOGGING_EXTENSIVE_DELAY = (bool)value[pos];
  else if(expected[i]=="LOGGING_INFO_DELAY")
   LOGGING_INFO_DELAY = (double)value[pos];
  else if(expected[i]=="LOGGING_WARNING_DELAY")
   LOGGING_WARNING_DELAY = (double)value[pos];
  else if(expected[i]=="LOGGING_DEV_WARNING_DELAY")
   LOGGING_DEV_WARNING_DELAY = (bool)value[pos];
  else if(expected[i]=="LOGGING_LOW_DELAY")
   LOGGING_LOW_DELAY = (double)value[pos];
  else if(expected[i]=="LOGGING_HIGH_DELAY")
   LOGGING_HIGH_DELAY = (double)value[pos];
  else if(expected[i]=="LOGGING_NETWORK_DELAY")
   LOGGING_NETWORK_DELAY = (double)value[pos];
   
  else if(expected[i]=="SHOW_LOG_VARIABLES_ON_EA_ATTACH")
   SHOW_LOG_VARIABLES_ON_EA_ATTACH = (bool)value[pos];
  else {
   MessageBox("Bug: unable to load logger!!!", "CRITICAL ERROR", MB_OK|MB_ICONERROR);
   ExpertRemove();
  }
 }

 string success_msg = StringConcatenate("Setting following variables from ", filename,":\n\n");
  
 success_msg += "LOGGING_DEBUG = " + (string)LOGGING_DEBUG + "\n";
 success_msg += "LOGGING_DEBUG_EXTENSIVE = " + (string)LOGGING_EXTENSIVE + "\n";
 success_msg += "LOGGING_INFO = " + (string)LOGGING_INFO + "\n";
 success_msg += "LOGGING_WARNING = " + (string)LOGGING_WARNING + "\n";    
 success_msg += "LOGGING_DEV_WARNING = " + (string)LOGGING_DEV_WARNING + "\n";
 success_msg += "ALERT_LOGGING_LOW = " + (string)ALERT_LOGGING_LOW + "\n";
 success_msg += "ALERT_LOGGING_HIGH = " + (string)ALERT_LOGGING_HIGH + "\n";
 success_msg += "ALERT_LOGGING_NETWORK = " + (string)ALERT_LOGGING_NETWORK + "\n";
 
 success_msg += "SITE_LOGGING_INFO = " + (string)SITE_LOGGING_INFO + "\n";
 success_msg += "SITE_LOGGING_WARNING = " + (string)SITE_LOGGING_WARNING + "\n";
 success_msg += "SITE_LOGGING_DEV_WARNING = " + (string)SITE_LOGGING_DEV_WARNING + "\n";
 success_msg += "SITE_LOGGING_LOW = " + (string)SITE_LOGGING_LOW + "\n";
 success_msg += "SITE_LOGGING_HIGH = " + (string)SITE_LOGGING_HIGH + "\n";
 success_msg += "SITE_LOGGING_NETWORK = " + (string)SITE_LOGGING_NETWORK + "\n";
 
 success_msg += "LOGGING_DEBUG_DELAY = " + (string)LOGGING_DEBUG_DELAY + "\n";
 success_msg += "LOGGING_EXTENSIVE_DELAY = " + (string)LOGGING_EXTENSIVE_DELAY + "\n";
 success_msg += "LOGGING_INFO_DELAY = " + (string)LOGGING_INFO_DELAY + "\n";
 success_msg += "LOGGING_WARNING_DELAY = " + (string)LOGGING_WARNING_DELAY + "\n";    
 success_msg += "LOGGING_DEV_WARNING_DELAY = " + (string)LOGGING_DEV_WARNING_DELAY + "\n";
 success_msg += "LOGGING_LOW_DELAY = " + (string)LOGGING_LOW_DELAY + "\n";
 success_msg += "LOGGING_HIGH_DELAY = " + (string)LOGGING_HIGH_DELAY + "\n";
 success_msg += "LOGGING_NETWORK_DELAY = " + (string)LOGGING_NETWORK_DELAY + "\n";
 
 success_msg += "SHOW_LOG_VARIABLES_ON_EA_ATTACH = " + (string)SHOW_LOG_VARIABLES_ON_EA_ATTACH + "\n";
 
 if(SHOW_LOG_VARIABLES_ON_EA_ATTACH && p_static_config_files.SearchLinear(p_program_name) < 0)
  MessageBox(success_msg,"INFO",MB_OK|MB_ICONINFORMATION);
  
 p_config_file_path = filename;
}
//--------------------------------------------------------------
Logger::log(const string message, LOG_TYPE type, int __LINE__macro, string __FUNCTION__macro, bool override_filtering=false, uint log_uuid=0)
/*
switch must match order of LOG_TYPE enum (defined in Super\Enums.mqh)
*/
{
 if(DISABLE_LOGGING)
  return;
  
 int pos;
 
 if(log_uuid > 0)
   pos = p_uuid.SearchLinear(log_uuid);
 else
   pos = msg.SearchLinear(message);
 
 if(pos>=0 && p_message_type.SearchLinear(pos)==type)
 {
  switch(type)
  {
   case LOG_TYPE_DEBUG:
    if(LOGGING_DEBUG)
    {
     if(TimeLocal() - timestamp[pos] >= LOGGING_DEBUG_DELAY * 60 || override_filtering)
     {
      Print("[DEBUG] ", p_display_name, " (", (string)__LINE__macro, "): ", __FUNCTION__macro, ": ", message);
      
      timestamp.Update(pos, TimeLocal());
     }
    }
    
    break;
    
   case LOG_TYPE_EXTENSIVE:
    if(LOGGING_EXTENSIVE)
    {
     if(TimeLocal() - timestamp[pos] >= LOGGING_EXTENSIVE_DELAY * 60 || override_filtering)
     {
      Print("[DEBUG_EXT] ", p_display_name, " (", (string)__LINE__macro, "): ", __FUNCTION__macro, ": ", message);
      
      timestamp.Update(pos, TimeLocal());
     }
    }
    
    break;
    
   case LOG_TYPE_INFO:
    if(LOGGING_INFO)
    {
     if(TimeLocal() - timestamp[pos] >= LOGGING_INFO_DELAY * 60 || override_filtering)
     {
      Print("[INFO] ", p_display_name, " (", (string)__LINE__macro, "): ", __FUNCTION__macro, ": ", message);
      
      timestamp.Update(pos, TimeLocal());
     }
    }
    
    break;
    
   case LOG_TYPE_WARNING:
    if(LOGGING_WARNING)
    {
     if(TimeLocal() - timestamp[pos] >= LOGGING_WARNING_DELAY * 60 || override_filtering)
     {
      Print("[WARNING] ", p_display_name, " (", (string)__LINE__macro, "): ", __FUNCTION__macro, ": ", message);
      
      timestamp.Update(pos, TimeLocal());
     }
    }
    break;
  
  case LOG_TYPE_DEV_WARNING:
    if(LOGGING_DEV_WARNING)
    {
     if(TimeLocal() - timestamp[pos] >= LOGGING_DEV_WARNING_DELAY * 60 || override_filtering)
     {
      Print("[DEV_WARNING] ", p_display_name, " (", (string)__LINE__macro, "): ", __FUNCTION__macro, ": ", message);
      
      timestamp.Update(pos, TimeLocal());
     }
    }
    break;
    
      
   case LOG_TYPE_LOW:
    if(TimeLocal() - timestamp[pos] >= LOGGING_LOW_DELAY * 60 || override_filtering)
    {
     if(ALERT_LOGGING_LOW)
      Alert("[LOW] ", p_display_name, " (", (string)__LINE__macro, "): ", __FUNCTION__macro, ": ", message);
     else
      Print("[LOW] ", p_display_name, " (", (string)__LINE__macro, "): ", __FUNCTION__macro, ": ", message);
      
     timestamp.Update(pos, TimeLocal());
    }   
    
    break;
    
   case LOG_TYPE_HIGH:
    if(TimeLocal() - timestamp[pos] >= LOGGING_HIGH_DELAY * 60 || override_filtering)
    {
     if(ALERT_LOGGING_HIGH)
      Alert("[HIGH] ", p_display_name, " (", (string)__LINE__macro, "): ", __FUNCTION__macro, ": ", message);
     else
      Print("[HIGH] ", p_display_name, " (", (string)__LINE__macro, "): ", __FUNCTION__macro, ": ", message);
      
     timestamp.Update(pos, TimeLocal());
    }
    
    break;
    
  case LOG_TYPE_NETWORK:
    if(TimeLocal() - timestamp[pos] >= ALERT_LOGGING_NETWORK * 60 || override_filtering)
    {
     if(ALERT_LOGGING_NETWORK)
      Alert("[NETWORK] ", p_display_name, " (", (string)__LINE__macro, "): ", __FUNCTION__macro, ": ", message);
     else
      Print("[NETWORK] ", p_display_name, " (", (string)__LINE__macro, "): ", __FUNCTION__macro, ": ", message);
      
     timestamp.Update(pos, TimeLocal());
    }
    
    break; 
  }
 }
 else
 {
  timestamp.Add(TimeLocal());
  msg.Add(message);
  p_message_type.Add(type);
  p_uuid.Add(log_uuid);
 
  switch(type)
  {
   case LOG_TYPE_DEBUG:
    if(LOGGING_DEBUG)
     Print("[DEBUG] ", p_display_name, " (", (string)__LINE__macro, "): ", __FUNCTION__macro, ": ", message);
    
    break;
    
   case LOG_TYPE_EXTENSIVE:
     if(LOGGING_EXTENSIVE)
      Print("[DEBUG_EXT] ", p_display_name, " (", (string)__LINE__macro, "): ", __FUNCTION__macro, ": ", message);
    
    break;
    
   case LOG_TYPE_INFO:
    if(LOGGING_INFO)
     Print("[INFO] ", p_display_name, " (", (string)__LINE__macro, "): ", __FUNCTION__macro, ": ", message);
     
    break;
    
   case LOG_TYPE_WARNING:
    if(LOGGING_WARNING)
     Print("[WARNING] ", p_display_name, " (", (string)__LINE__macro, "): ", __FUNCTION__macro, ": ", message);
     
    break;
    
   case LOG_TYPE_DEV_WARNING:
    if(LOG_TYPE_DEV_WARNING)
     Print("[DEV_WARNING] ", p_display_name, " (", (string)__LINE__macro, "): ", __FUNCTION__macro, ": ", message);
     
    break;
    
   case LOG_TYPE_LOW:
     if(ALERT_LOGGING_LOW)
      Alert("[LOW] ", p_display_name, " (", (string)__LINE__macro, "): ", __FUNCTION__macro, ": ", message);
     else
      Print("[LOW] ", p_display_name, " (", (string)__LINE__macro, "): ", __FUNCTION__macro, ": ", message);

    break;
    
   case LOG_TYPE_HIGH:
     if(ALERT_LOGGING_HIGH)
      Alert("[HIGH] ", p_display_name, " (", (string)__LINE__macro, "): ", __FUNCTION__macro, ": ", message);
     else
      Print("[HIGH] ", p_display_name, " (", (string)__LINE__macro, "): ", __FUNCTION__macro, ": ", message);
    
    break;
    
   case LOG_TYPE_NETWORK:
     if(ALERT_LOGGING_NETWORK)
      Alert("[NETWORK] ", p_display_name, " (", (string)__LINE__macro, "): ", __FUNCTION__macro, ": ", message);
     else
      Print("[NETWORK] ", p_display_name, " (", (string)__LINE__macro, "): ", __FUNCTION__macro, ": ", message);
    
    break;
  }
 }
}
//--------------------------------------------------------------
Logger::Logger(string __FILE__macro, string config_file_name=NULL)
{ 
 string res[];

 if(BASE_DIR=="")
  BASE_DIR = "test";
 
 StringSplit(__FILE__macro, '.', res);
 p_display_name = res[0];
 
 if(config_file_name==NULL)
  p_program_name = p_display_name;
 else {
  StringSplit(config_file_name, '.', res);
  p_program_name = res[0];
 }
 
 load();
 
 p_static_config_files.Add(p_program_name);
}
//+------------------------------------------------------------------+
void ConfigFileLoader(string filename, CArrayString& variables, CArrayString& values, 
                       const string& expected[], string& not_found[])
/*
Delimiter set to '='
For each variable = value pair, white space is stripped
Returned arrays variables[] and values[] are of equal size ORDERED BY expected[]
Any variable not in expected[] will be excluded, regardless if if found in config file
Returned array not_found[] lists any variable found in expected[], but not found in the config file
*/
{
 int ret_code;
 string buffer_variables[];
 string buffer_values[];
 
 do
 {
  int handle = FileOpen(filename,FILE_TXT|FILE_READ|FILE_SHARE_READ);

  if(handle >=0)
  {
   while(!FileIsEnding(handle))
   {
    string result[];
    string line = FileReadString(handle);
    
    //Skip comment line
    if(StringFind(line,"#") == 0)
      continue;
    
    StringSplit(line,'=',result);
    if(ArraySize(result) == 2)
    {
     int size = ArraySize(buffer_variables);
     StringReplace(result[0]," ","");
     StringReplace(result[1]," ","");
     ArrayResize(buffer_variables,size+1);
     ArrayResize(buffer_values,size+1);
     buffer_variables[size] = result[0];
     buffer_values[size] = result[1];
    }
   }
   
   FileClose(handle);
   ret_code = IDOK;
  }
  else
   ret_code = MessageBox("Could not load " + filename + ", e= " + (string)GetLastError() + "\nDetaching EA on cancel ...", 
                            "CRITICAL ERROR", MB_RETRYCANCEL);

  if(ret_code==IDCANCEL)
   ExpertRemove();
 }
 while(ret_code==IDRETRY);
 
 CArrayString inspect;
 inspect.AddArray(expected);
 int size = ArraySize(buffer_variables);
 for(int i=0; i<size; i++)
 {
  int pos = inspect.SearchLinear(buffer_variables[i]);
  
  if(pos>=0)
  {
   variables.Add(buffer_variables[i]);
   values.Add(buffer_values[i]);
   inspect.Delete(pos);
  }
 }
 
 int remaining = inspect.Total();
 for(int i=0; i<remaining; i++)
 {
  ArrayResize(not_found, ArraySize(not_found)+1);
  not_found[i] = inspect[i];
 }
}
//+------------------------------------------------------------------+
bool CreateFileIfNotExists(string path)
{
 string res[];
 StringReplace(path, "//", "/");
 StringSplit(path,'/',res);
 
 string p = "";
 int size = ArraySize(res);
 for(int i=0; i<size-1; i++) 
 {
  FolderCreate(p + res[i]);
  p += res[i] + "//";
 }
    
 if(!FileIsExist(path))
 {
  int handle = FileOpen(path, FILE_WRITE);
  if(handle >= 0)
  FileClose(handle);
  return true;
 }
 
 return false; 
}
