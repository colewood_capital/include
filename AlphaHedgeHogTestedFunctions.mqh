//+------------------------------------------------------------------+
//|                                 AlphaHedgeHogTestedFunctions.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#ifndef ARRAYINT
#define ARRAYINT
   #include <Arrays/ArrayInt.mqh>
#endif 

#ifndef LOGGER
#define LOGGER
   Logger logger(__FILE__, "Hedge HOG");
#endif

//Used for loading config files
#include <AlphaHedgeHogConfig.mqh>

alpha_profile profile;

#import "projects\\ManagerInterfaceHedgeHOG\\Debug\\ManagerInterface.dll"
   bool ManagerConnect(char& ip_address[], int login, char& passwd[]);
   bool ManagerTradeInfo(int booknum, int& login, int& cmd, char& symbol[], int symbol_size, double& lots, double& open_price, uint& open_time, double& sl, double& tp, double& close_price,
                          uint& close_time, double& commission, double& swap, double& profit, char& comment[], int comment_size, int& magic);
   bool ManagerIsConnected();
   void RefreshOrders(int& total);
   uint GetServerTime();
   //void GetRemoteServerName(string& name, int& len);
   void GetOpenOrders(int total, uint& login[], int& booknum[], uint& symbol[], double& lots[], int& cmd[], unsigned int& open_time[], double& open_price[], double& sl[], double& tp[], uint& close_time[], 
                         double& close_price[], double& swaps[], double& profit[], int& method[], int& state[]);
   void GetCloseInfo(int& booknum[], int size, int& symbol[], double& sl[], double& tp[], unsigned int& close_time[], double& close_price[], double& swaps[], double& profit[], int& state[], int& reason[]);
   int GetStopOutCount(uint begin, int login);
   void GetSymbolName(string& symbol, uint index, int& symbol_len);
   void GetAcctInfo(int login, int& equity, int& free_margin); 
   void DLLShutdown();
#import

class SymbolDict
 {
  private:
    string        m_symbol[];
    
  public:
    string        ConvertToSymbol(int index);
    SymbolDict();
 };
 
 SymbolDict::SymbolDict(void)
 {
  ZeroMemory(m_symbol);
 }

 string SymbolDict::ConvertToSymbol(int index)
 {
   int size = ArraySize(m_symbol);

   if(index >= 0)
   {
    if(index < size && m_symbol[index]!=NULL)
     {
       return(m_symbol[index]);
     }
    else              //Get new symbol from DLL and add to m_symbol[]
     {
       string new_symbol = "";
       int symbol_len = 0;
      
       GetSymbolName(new_symbol, index, symbol_len);
      
       new_symbol = StringSubstr(new_symbol,0,symbol_len);
     
       if(index >= size)
        ArrayResize(m_symbol, index+1);

       m_symbol[index] = new_symbol;
      
       return(new_symbol);
     }
   }
   
   string err_msg = "INVALID index=" + (string)index;
   LOG_HIGH(err_msg);
   return err_msg;
 }

SymbolDict symDict;

int GetClosedHOGTrades(const int& login[], int& booknum[], int& symbol[], int& type[])
{
 //Needs to record hogs trades and note when one is missing
 //func w/ static CArrayInt -> prev_booknum
 //-> comapare -> remove from prev_booknum if found
 //.... as we loop, record new trades
 //
 static CArrayInt past_bknum;
 static CArrayInt past_symbol;
 static CArrayInt past_type;
 
 CArrayInt new_trades;
 CArrayInt new_symbol;
 CArrayInt new_type;
 
 //--- loop through current trades
  int size = ArraySize(booknum);
 
  for(int i=0; i<size; i++)
  {
   if(profile.GroupID(login[i]) >= 0)
   {
    new_trades.Add(booknum[i]);
    new_symbol.Add(symbol[i]);
    new_type.Add(type[i]);
    
    int pos = past_bknum.SearchLinear(booknum[i]);
    if(pos >= 0)
    { 
     past_bknum.Delete(pos);
     past_symbol.Delete(pos);
     past_type.Delete(pos);
    }
   }
  }
 
 //--- update: remaining trades in past_bknum must have been closed / deleted
  int past_arr_size = past_bknum.Total();
  ArrayResize(booknum, past_arr_size);
  ArrayResize(symbol, past_arr_size);
  ArrayResize(type, past_arr_size);
  
  for(int i=0; i<past_arr_size; i++)
  {
   booknum[i] = past_bknum[i];
   symbol[i] = past_symbol[i];
   type[i] = past_type[i];
   
   LOG_DEBUG("HOG #" + (string)booknum[i] + " closed, symbol(id)=" + (string)symbol[i] + ", type=" + (string)type[i]);
  }
  
 //--- update past_bknum
  past_bknum.AssignArray(GetPointer(new_trades));
  past_symbol.AssignArray(GetPointer(new_symbol));
  past_type.AssignArray(GetPointer(new_type));
 
 return past_arr_size;
}