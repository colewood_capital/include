//+------------------------------------------------------------------+
//|                                                 PythonAlerts.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
#ifndef PYTHON_PATH
 #define PYTHON_PATH    "C:\\Python27\\mt4sendmail.pyw"
#endif
#ifndef GMAIL_USER
 #define GMAIL_USER     "carm.1.inc@gmail.com"
#endif
#ifndef GMAIL_PASSWD
 #define GMAIL_PASSWD   "Qondawey242836"
#endif
#ifndef NEWLINE
 #define NEWLINE         "<NEWLINE>"
#endif
//+------------------------------------------------------------------+
//| DLL imports                                                      |
//+------------------------------------------------------------------+
#import "EmailAlerts.dll"
   void DLLSendMail(string program_path, string subject, string body, string attachment_path, string email_user, string email_passwd);
#import
//+------------------------------------------------------------------+
//| MQL4 functions                                                   |
//+------------------------------------------------------------------+
void SendMessage(string subject, string body)
{
  DLLSendMail(PYTHON_PATH, subject, body, "", GMAIL_USER, GMAIL_PASSWD);
}
//+------------------------------------------------------------------+
void SendScreenshot(string subject, string body=NULL)
{
  string filepathname = Symbol() + "_" + PeriodStr(Period()) + ".gif";
  string terminal_data_path=TerminalInfoString(TERMINAL_DATA_PATH);
  
  WindowScreenShot(filepathname,960,720);
  
  string path = terminal_data_path + "\\MQL4\\Files\\" + filepathname;
  
  DLLSendMail(PYTHON_PATH, subject, body, path, GMAIL_USER, GMAIL_PASSWD);
}

