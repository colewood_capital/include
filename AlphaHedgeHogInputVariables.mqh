//+------------------------------------------------------------------+
//|                                          AlphaInputVariables.mqh |

//+------------------------------------------------------------------+
#property copyright "CW Capital"

#property strict

input float FX_MARKUP = 0;
input float METALS_MARKUP = 0; 

//extern string STOPOUT_QUERY_START_TIME = "2014-08-01 17:00"; 

#ifndef BASEDIR
#define BASEDIR
   extern string BASE_DIR = "Hedge HOG";
#endif 

string ACCOUNT_SETTINGS_FILE_NAME = BASE_DIR + "\\settings\\account_settings.csv";
string ALPHA_CONFIG_FILE_NAME = BASE_DIR + "\\settings\\config.csv";
string ALPHA_STOPOUT_FILE_NAME = BASE_DIR + "\\settings\\stopout_settings.csv";
string ALPHA_SYMBOL_SETS_FILE_NAME = BASE_DIR + "\\settings\\symbol_sets.csv";
string ALPHA_MULTIPLIER_SETS_FILE_NAME = BASE_DIR + "\\settings\\multiplier.csv";