//+------------------------------------------------------------------+
//|                                                FileMessenger.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Jamal Cole"
#property strict

#ifndef FILEMESSENGERBASE
#define FILEMESSENGERBASE
   #include <FileMessengerBase.mqh>
#endif 

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
#endif

class FileMessenger : public FileMessengerBase
{
 private:
  string p_path;
  string p_request;
  string p_args;
  void send_response(string status, string response);
  Logger logger;
  
 public:
  string get_request(string& response);
  void send_response_success(string response=NULL);
  void send_response_failed(string response=NULL);
  FileMessenger(string path);
};
//-------------------------------------------------------------------+
void FileMessenger::send_response_failed(string response=NULL)
{
 send_response("failed", response);
}
//-------------------------------------------------------------------+
void FileMessenger::send_response_success(string response=NULL)
{
 send_response("success", response);
}
//-------------------------------------------------------------------+
void FileMessenger::send_response(string status, string response)
{
 string file_name_extension = "__request.txt";
 int delimiter = StringFind(filename(), file_name_extension);
 string file_name_base = StringSubstr(filename(), 0, delimiter);
 string file_name_response = file_name_base + "__response.txt";
 string path = p_path + "\\" + file_name_response;
 
 int handle = FileOpen(path, FILE_WRITE|FILE_TXT|FILE_SHARE_READ);
 if(handle >= 0)
 {
  FileWrite(handle, "request=", p_request);
  FileWrite(handle, "args=", p_args);
  FileWrite(handle, "status=", status);
  FileWrite(handle, "response=", response);
  FileClose(handle);
  
  del();
 }
 else
  logger.log("Unable to open " + file_name_extension + "e=" + (string)GetLastError(), LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
}
//-------------------------------------------------------------------+
string FileMessenger::get_request(string& args)
{
 string _request = NULL;
 CArrayString key, value;
 
 int size = read(key, value);

 LOG_EXTENSIVE_1(size);
 LOG_EXTENSIVE_1(key);
 LOG_EXTENSIVE_1(value);

 for(int i=0; i<size; i++) {
 
  if(key[i]=="request")
   _request = value[i];
   
  if(key[i]=="args") {
   args = value[i];
  
   //--- string trailing ';' if exists
    int args_len = StringLen(args);
    
    if(StringGetChar(args, args_len - 1) == ';')
     args = StringSubstr(args, 0, args_len - 1);
  }
 }
 
 if(StringLen(filename()) > 0 && _request==NULL)
  logger.log("Could not find request keyword in " + filename(), LOG_TYPE_WARNING, __LINE__, __FUNCTION__);
 
 p_request = _request;
 p_args = args;
 
 return _request;
}
//-------------------------------------------------------------------+
FileMessenger::FileMessenger(string path)
   : FileMessengerBase(path, "txt", "__request"), logger(__FILE__, "helper_processes")
{
 p_path = path;
}