//+------------------------------------------------------------------+
//|                                                 OrderManager.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "CARM"
#property strict

#ifndef ARRAYINT
#define ARRAYINT
   #include <Arrays/ArrayInt.mqh>
#endif 

#ifndef ARRAYDOUBLE
#define ARRAYDOUBLE
   #include <Arrays/ArrayDouble.mqh>
#endif 

#ifndef MQLERRORS
#define MQLERRORS
   #include <MQLErrors.mqh>
#endif

#ifndef ARRAYDOUBLEDOUBLE
#define ARRAYDOUBLEDOUBLE
   #include <Arrays/ArrayDoubleDouble.mqh>
#endif

#ifndef STOPLOSSMANAGER
#define STOPLOSSMANAGER
   #include <StopLossManager.mqh>
#endif

#ifndef PYRAMIDUPMANAGER
#define PYRAMIDUPMANAGER
   #include <PyramidUpManager.mqh>
#endif 

class OrderManager
{
 private:
  CArrayInt mkt_orders; 
  CArrayInt pending_orders; 
  CArrayDouble mkt_open_lots;
  CArrayDouble mkt_closed_lots;
  CArrayDouble mkt_realized;
  CArrayDoubleDouble mkt_close_prices;
  StopLossManager* p_slManager;
  PyramidUpManager* p_avgUpManager;
  
  int p_magic;
  int p_retrycount;
  int p_retrydelay;
  int p_slippage;
  
  void Init();
  void AddMarket(int ticket, double lots);
  void AddPending(int ticket);
  void DeleteMarket(int ticket);
  int PlaceTrade(int cmd, double lots, double stop=0.0, double take=0.0, double price=0.0, string comment=NULL);
  double CheckLots(double lots);
  void PartialCloseInfo(int ticket, double& lots, double& realized_pl, CArrayDouble& close_price, int idx=0);
  bool Update(int ticket, double closed_lots);
  

 public:
  int OpenTrade(int cmd, double lots, double sl=0.0, double tp=0.0, double price=0.0, string comment=NULL);
  bool CloseTrade(int ticket, double lots);
  bool CloseTradePercent(int ticket, double percentage);
  double CloseAll(double percentage);
  bool DeletePending(int ticket);
  bool Info(int ticket,double& open_lots,double& closed_lots, double& realized_pl);
  bool Info(int ticket,double& open_lots,double& closed_lots, double& realized_pl, CArrayDouble*& close_prices);
  void GetMetaInfo(double& total_pips_profit, bool& is_all_orders_in_profit);
  bool isOpen(int ticket);
  void GetOpenTrades(CArrayInt& booknum);
  void GetPendingTrades(CArrayInt& booknum);
  int AlertTriggeredPending();
  int AlertTriggeredSL();
  double LastOpenPrice();
  double GetBreakevenSL(int ticket);
  void engine();
  
  OrderManager(int MAGIC=0, int SLIPPAGE=5, int RETRYCOUNT=10, int RETRYDELAY=500);
  OrderManager(StopLossManager* slManager, int MAGIC=0, int SLIPPAGE=5, int RETRYCOUNT=10, int RETRYDELAY=500);
  OrderManager(StopLossManager* slManager, PyramidUpManager* avgUpManager, int MAGIC=0, int SLIPPAGE=5, int RETRYCOUNT=10, int RETRYDELAY=500);
};
//+------------------------------------------------------------------+
OrderManager::OrderManager(int MAGIC=0,int SLIPPAGE=5,int RETRYCOUNT=10,int RETRYDELAY=500)
{
  p_magic = MAGIC;
  p_retrycount = RETRYCOUNT;
  p_retrydelay = RETRYDELAY;
  p_slippage = SLIPPAGE;
  p_slManager = NULL;
  p_avgUpManager = NULL;
  
  Init();
}
//+------------------------------------------------------------------+
OrderManager::OrderManager(StopLossManager* ptr_slManager, int MAGIC=0,int SLIPPAGE=5,int RETRYCOUNT=10,int RETRYDELAY=500)
{
  p_magic = MAGIC;
  p_retrycount = RETRYCOUNT;
  p_retrydelay = RETRYDELAY;
  p_slippage = SLIPPAGE;
  p_slManager = ptr_slManager;
  p_avgUpManager = NULL;
  
  Init();
}
//+------------------------------------------------------------------+
OrderManager::OrderManager(StopLossManager* ptr_slManager, PyramidUpManager* ptr_pyramidUpManager, int MAGIC=0,int SLIPPAGE=5,int RETRYCOUNT=10,int RETRYDELAY=500)
{
  p_magic = MAGIC;
  p_retrycount = RETRYCOUNT;
  p_retrydelay = RETRYDELAY;
  p_slippage = SLIPPAGE;
  p_slManager = ptr_slManager;
  p_avgUpManager = ptr_pyramidUpManager;
  
  Init();
}
//+------------------------------------------------------------------+
void OrderManager::engine(void)
{
 int mkt_total = mkt_orders.Total();
 //int pending_total = pending_orders.Total();
 CArrayInt closed_orders, cancelled_orders;
 CArrayDouble closed_order_lots;
 
 //--- check for closed trades
 for(int i=mkt_total-1; i>=0; i--)
 {
  if(OrderSelect(mkt_orders[i], SELECT_BY_TICKET) && OrderCloseTime() > 0)
  {
   closed_orders.Add(mkt_orders[i]);
   closed_order_lots.Add(OrderLots());
  }
 }
 
 //--- update closed trades
 int closed_total = closed_orders.Total();
 
 for(int i=closed_total-1; i>=0; i--)
  Update(closed_orders[i], closed_order_lots[i]);
  
 /* //---- PROCESS FOR CANCELLING LOOKS DIFFERENT THAN MARKET. NEEDS RETHINKING
 //--- check for cancelled trades
 for(int i=pending_total-1; i>=0; i--)
 {
  if(OrderSelect(pending_orders[i], SELECT_BY_TICKET) && OrderCloseTime() > 0)
  {
   cancelled_orders.Add(pending_orders[i]);
  }
 }
 
 //--- update cancelled trades
 int cancelled_total = cancelled_orders.Total();
 
 for(int i=cancelled_total-1; i>=0; i--)
  Update(closed_orders[i], closed_order_lots[i]);
  
 */
}
//+------------------------------------------------------------------+
void OrderManager::Init(void)
{
  string symbol = Symbol();
  int total = OrdersTotal();
    
  for(int i=0; i<total; i++)
   {
    if(OrderSelect(i,SELECT_BY_POS))
     {
      if(OrderSymbol()==symbol && OrderMagicNumber()==p_magic)
       {
        int type=OrderType();
        
        if(type==OP_BUY || type==OP_SELL)
         AddMarket(OrderTicket(),OrderLots());      
        
        if(type==OP_BUYLIMIT || type==OP_SELLLIMIT || type==OP_BUYSTOP || type==OP_SELLSTOP)
         AddPending(OrderTicket());
       }
     }
   }
}
//+------------------------------------------------------------------+
void OrderManager::GetMetaInfo(double &total_pips_profit,bool &is_all_orders_in_profit)
{
 int total = mkt_orders.Total();
 total_pips_profit = 0;

 if(total > 0)
  is_all_orders_in_profit = true;
 else
  is_all_orders_in_profit = false;
  
 for(int i=0; i<total; i++)
 {
  if(OrderSelect(mkt_orders[i], SELECT_BY_TICKET))
  {
   int type = OrderType();
                                        
   double pips_in_profit = (type==OP_BUY) ? Bid - OrderOpenPrice() : (type==OP_SELL) ? OrderOpenPrice() - Ask : 0;
   
   if(pips_in_profit <= 0)
    is_all_orders_in_profit = false;
    
   total_pips_profit += pips_in_profit / (Point * 10);
  }
 }
}
//+------------------------------------------------------------------+
void OrderManager::GetPendingTrades(CArrayInt& booknum)
{
  booknum.AssignArray(GetPointer(pending_orders));
}
//+------------------------------------------------------------------+
void OrderManager::GetOpenTrades(CArrayInt& booknum)
{
  booknum.AssignArray(GetPointer(mkt_orders));
}
//+------------------------------------------------------------------+
double OrderManager::GetBreakevenSL(int ticket)
{
 int total = mkt_orders.Total();
 double realized=0, unrealized=0, sl;
 double baseVolume=0, counterVolume=0;
 
 for(int i=0; i<total; i++)
  {
   if(OrderSelect(mkt_orders[i], SELECT_BY_TICKET))
    { Alert(i,": ",mkt_orders[i]);
     double openLots, closedLots, orderRealizedPl;
     Info(OrderTicket(), openLots, closedLots, orderRealizedPl);
     realized += orderRealizedPl;
     unrealized += OrderCommission() + OrderSwap() + OrderProfit();
     baseVolume += OrderLots();
     counterVolume += OrderLots() * OrderOpenPrice();
     Alert("r: ", realized);
     Alert("u: ", OrderProfit());
    } 
  }
  
 double profit = unrealized + realized;
 
 double vwap = (baseVolume > 0) ? counterVolume / baseVolume : 0; 
 Alert("vwap: ",vwap);
 if(OrderSelect(ticket, SELECT_BY_TICKET))
  {
   int type = OrderType();
   
   if(type==OP_BUY || type==OP_BUYLIMIT || type==OP_BUYSTOP)
     sl = vwap;
   else if(type==OP_SELL || type==OP_SELLLIMIT || type==OP_SELLSTOP)
     sl = vwap;
   else
    {
     Alert("Error: invalid type=",OrderType()," for ticket=",OrderTicket()," in ",__FUNCTION__);
     return(-1);
    }
    //NOTE: Bid for buy
   if(MathAbs(Ask - vwap) > 5 * Point)       //Precaution in case sl is too close to open price
    {
     return(sl);
    }
   else
    {
     Alert("Error: did not modify SL for ticket=",ticket," for D(sl,open_price) <= 5 points. sl=",sl,". open_price=",OrderOpenPrice());
     return(-1);
    }
  }
 
 Alert("Error: failed to select ticket #",ticket," in ",__FUNCTION__);
 
 return(-1);
}
//+------------------------------------------------------------------+
int OrderManager::AlertTriggeredSL()
{
 int total = mkt_orders.Total();
 
 for(int i=total-1; i>=0; i--)
  {
   if(OrderSelect(mkt_orders[i], SELECT_BY_TICKET))
    {
     string ord_cmt = OrderComment();
   
     if(OrderCloseTime() > 0 && StringFind(ord_cmt,"[sl]") >= 0)
      {
       int ticket = OrderTicket();
       DeleteMarket(ticket);
       return(ticket);
      }
    }
  }
   
  return(-1);
}
//+------------------------------------------------------------------+
double OrderManager::LastOpenPrice()
{
   int last_order = mkt_orders.Total()-1;
   
   if(OrderSelect(mkt_orders[last_order], SELECT_BY_TICKET))
    return(OrderOpenPrice());
    
   return(-1);
}
//+------------------------------------------------------------------+
bool OrderManager::isOpen(int ticket)
//Used to check if ticket is in OrderManager queue
{
   int pos = mkt_orders.SearchLinear(ticket);
   
   if(pos>=0)
     return(true);
   else
     return(false);
}
//+------------------------------------------------------------------+
bool OrderManager::Info(int ticket, double& open_lots, double& closed_lots, double& realized_pl)
{
   int pos = mkt_orders.SearchLinear(ticket);
  
   if(pos>=0)
    {
     open_lots = mkt_open_lots[pos];
     closed_lots = mkt_closed_lots[pos];
     realized_pl = mkt_realized[pos];
 
     return(true);
    }
   else
    {
     open_lots = 0;
     closed_lots = 0;
     realized_pl = 0;
     
     Alert("Error: Could not find ticket=",ticket," in ",__FUNCSIG__);
     return(false);
    }
}
//+------------------------------------------------------------------+
bool OrderManager::Info(int ticket, double& open_lots, double& closed_lots, double& realized_pl, CArrayDouble*& close_prices)
{
   int pos = mkt_orders.SearchLinear(ticket);
  
   if(pos>=0)
    {
     open_lots = mkt_open_lots[pos];
     closed_lots = mkt_closed_lots[pos];
     realized_pl = mkt_realized[pos];
     close_prices = mkt_close_prices.Get(ticket);
 
     return(true);
    }
   else
    {
     open_lots = 0;
     closed_lots = 0;
     realized_pl = 0;
     close_prices = NULL;
     
     Alert("Error: Could not find ticket=",ticket," in ",__FUNCSIG__);
     return(false);
    }
}
//+------------------------------------------------------------------+
bool OrderManager::DeletePending(int ticket)
{
  if(OrderDelete(ticket))
   { 
     int pos = pending_orders.SearchLinear(ticket);
     
     if(pos>=0)
      {
        pending_orders.Delete(pos);
      }
     else
      {
        Alert("Error: could not find ticket #",ticket," in ",__FUNCTION__);
      }
      
     return(true);
   }
  else
   {
     Alert("Error: could not delete #",ticket," for, e: ",GetLastError());
     ResetLastError();
     return(false);
   }
}
//+------------------------------------------------------------------+
void OrderManager::AddPending(int ticket)
{
  pending_orders.Add(ticket);
}
//+------------------------------------------------------------------+
void OrderManager::DeleteMarket(int ticket)
{
  int pos = mkt_orders.SearchLinear(ticket);
  
  if(pos>=0)
   {
     mkt_orders.Delete(pos);
     mkt_open_lots.Delete(pos);
     mkt_closed_lots.Delete(pos);
     mkt_realized.Delete(pos);
     mkt_close_prices.Delete(ticket);
     
     if(p_slManager != NULL) 
       p_slManager.Remove(ticket); 
   }
  else
   {
     Alert("Error: could not find ticket #",ticket," in ",__FUNCTION__);
   }
}
//+------------------------------------------------------------------+
void OrderManager::AddMarket(int ticket, double lots)
{
  double realized=0.0, cls_lots=0.0;
  CArrayDouble* cls_prices = mkt_close_prices.New(ticket);
  cls_prices = mkt_close_prices.Get(ticket);

  PartialCloseInfo(ticket,cls_lots,realized,cls_prices);
  
  mkt_orders.Add(ticket);
  mkt_open_lots.Add(lots);
  mkt_closed_lots.Add(cls_lots);
  mkt_realized.Add(realized);
  
  if(p_slManager != NULL) 
   p_slManager.SetNullSL(ticket); 
}
//+------------------------------------------------------------------+
bool OrderManager::Update(int ticket, double closed_lots)
{
  int id = mkt_orders.SearchLinear(ticket);
  int new_ticket=-1, type=-1;
  
  if(id < 0) {
    Alert("Error: could not find order #",ticket," in ",__FUNCTION__);
    return(false);
  } 
        
  //--- In case of partial close, replace old ticket with new ticket
  if(OrderSelect(ticket,SELECT_BY_TICKET))
   {
    //--- Update PyamidUpManager
   if(p_avgUpManager != NULL)
     p_avgUpManager.BankAdjustment(OrderProfit());
     
    //--- Update OrderManager 
    type = OrderType();
    
    if(OrderCloseTime() > 0)
     {
      string order_cmt = OrderComment();
      new_ticket = -1;
      
      int ans = StringFind(order_cmt,"to #");
      
      if(ans==0)
       {
         string res[];
         StringSplit(order_cmt,'#',res); 
         new_ticket = (int)res[1];
       }
       
      if(new_ticket > 0)
       {
         mkt_orders.Update(id, new_ticket);
                  
         mkt_open_lots.Update(id, NormalizeDouble(mkt_open_lots[id] - closed_lots, 2));
         
         mkt_closed_lots.Update(id, NormalizeDouble(mkt_closed_lots[id] + closed_lots, 2));
         
         mkt_close_prices.UpdateIndex(ticket, new_ticket); 
         
         mkt_close_prices.Get(new_ticket).Add(OrderClosePrice());
         
         mkt_realized.Update(id, NormalizeDouble(mkt_realized[id] + OrderProfit() + OrderSwap() + OrderCommission(), 2)); 
         
         if(p_slManager != NULL)
          p_slManager.UpdateBooknum(ticket, new_ticket);
       }
      else
       DeleteMarket(ticket);
       
      return(true);
    }
   else  
    {
      Alert("Error: Order #",ticket," should be updated after it is closed in ",__FUNCTION__);
      return(false);
    }
   }
   
  Alert("Error: could not select order #",ticket," in ",__FUNCTION__);
  return(false);
 }
//+------------------------------------------------------------------+
double OrderManager::CloseAll(double percentage)
{
  int total = mkt_orders.Total();
  double closed_lots = 0.0;
  bool error = false;
  
  for(int i=total-1; i>=0; i--)
   {
    bool ans=false;
  
    if(OrderSelect(mkt_orders[i],SELECT_BY_TICKET))
     {
      int ticket = OrderTicket();
      int type = OrderType();
      double prc = -1;
   
      if(type==OP_BUY)
       prc = Bid;
      else if(type==OP_SELL)
       prc = Ask;
      else
       continue;
 
      for (int j=0; j<p_retrycount; j++) {
         for (int k=0; (k<50) && IsTradeContextBusy(); k++)
             Sleep(100);
         RefreshRates();
     
      double lots = NormalizeDouble(OrderLots()*percentage, 2);
      
      ans = OrderClose(ticket, lots, prc, p_slippage);
      
      if(ans)
       {
         Update(ticket, lots); 
         closed_lots += lots;
         
        //--- Update PyamidUpManager
         if(p_avgUpManager != NULL)
          if(OrderSelect(ticket, SELECT_BY_TICKET))
           p_avgUpManager.BankAdjustment(OrderProfit());
       }
      else
       { 
         Print("CloseTrade: error \'"+(string)ErrorDescription(GetLastError())+" when closing #",ticket,", lots=",DoubleToStr(lots,3)+" @"+DoubleToStr(prc,Digits));
         ResetLastError();
         Sleep(p_retrydelay);
       }
      }
    
     }//end if
     
     if(!ans)       error=true;
   }//end for
  
  return(closed_lots);
}
//+------------------------------------------------------------------+
bool OrderManager::CloseTradePercent(int ticket,double percentage=1.0)
{
  int type;
  double lots, prc;
  
  if(OrderSelect(ticket,SELECT_BY_TICKET))
   {
    type = OrderType();
    lots = NormalizeDouble(OrderLots() * percentage, 2);
   }
  else {
   Alert("Error: unable to select ticket #",ticket," in ",__FUNCTION__);
   return(false);
  }
  
  if(type==OP_BUY)
   prc = Bid;
  else if(type==OP_SELL)
   prc = Ask;
  else
   Print("Error: unable to close ticket #",ticket,", cmd=",type," in ",__FUNCTION__);
  
  for (int i=0; i<p_retrycount; i++) {
     for (int j=0; (j<50) && IsTradeContextBusy(); j++)
         Sleep(100);
     RefreshRates();
     
  if(OrderClose(ticket, lots, prc, p_slippage))
   {
     //--- Update PyamidUpManager
      if(p_avgUpManager != NULL)
       if(OrderSelect(ticket, SELECT_BY_TICKET))
        p_avgUpManager.BankAdjustment(OrderProfit());
           
     Update(ticket, lots);
     return(true);
   }
  else
   {
     Print("CloseTrade: error \'"+(string)ErrorDescription(GetLastError())+" when closing #",ticket,", lots=",DoubleToStr(lots,3)+" @"+DoubleToStr(prc,Digits));
     ResetLastError();
     Sleep(p_retrydelay);
   }
  }
   
  Print("CloseTrade: can\'t close #",ticket," after "+(string)p_retrycount+" retries");
    
  return(false);
}
//+------------------------------------------------------------------+
bool OrderManager::CloseTrade(int ticket,double lots)
{
  int type;
  double prc;
  
  if(OrderSelect(ticket,SELECT_BY_TICKET))
   type = OrderType();
  else {
   Alert("Error: unable to select ticket #",ticket," in ",__FUNCTION__);
   return(false);
  }
  
  if(type==OP_BUY)
   prc = Bid;
  else if(type==OP_SELL)
   prc = Ask;
  else
   Print("Error: unable to close ticket #",ticket,", cmd=",type," in ",__FUNCTION__);
  
  lots = NormalizeDouble(lots, 2);
  
  for (int i=0; i<p_retrycount; i++) {
     for (int j=0; (j<50) && IsTradeContextBusy(); j++)
         Sleep(100);
     RefreshRates();
     
  if(OrderClose(ticket, lots, prc, p_slippage))
   {    
     Update(ticket, lots);
     return(true);
   }
  else
   {
     Print("CloseTrade: error \'"+(string)ErrorDescription(GetLastError())+" when closing #",ticket,", lots=",DoubleToStr(lots,3)+" @"+DoubleToStr(prc,Digits));
     ResetLastError();
     Sleep(p_retrydelay);
   }
  }
   
  Print("CloseTrade: can\'t close #",ticket," after "+(string)p_retrycount+" retries");
    
  return(false);
}
//+------------------------------------------------------------------+
int OrderManager::OpenTrade(int cmd,double lots,double sl=0.0,double tp=0.0,double price=0.0,string comment=NULL)
{
  int ticket=0;
  
  if(cmd >= 0 && cmd <= 1)
   {
     ticket = PlaceTrade(cmd, lots, sl, tp, price, comment);
     
     if(ticket > 0)        
      AddMarket(ticket, lots); 
   }
  else if(cmd > 1 && cmd <= 5)
   {
     ticket = PlaceTrade(cmd, lots, sl, tp, price, comment);
     
     if(ticket > 0)        
      AddPending(ticket);
   }
  else
   {
     Alert("Error: invalid cmd=",cmd," in ",__FUNCTION__);
   }
   
  return(ticket);
}
//-------------------------------------------------
int OrderManager::AlertTriggeredPending()
{
  int booknum = -1;
  int size = pending_orders.Total();
  
  for(int i=0; i<size; i++)
   {
    if(OrderSelect(pending_orders[i],SELECT_BY_TICKET))
     {
      if(OrderCloseTime()==0)
       {
        if(OrderType() < 2)
         {
           pending_orders.Delete(i);
           booknum = OrderTicket();
           AddMarket(booknum, OrderLots());
           break;
         }
       }
      else
       {
         pending_orders.Delete(i);
       }
     }
    else
     {
       Alert("Error: Booknum #", pending_orders[i]," could not be found by OrderSelect() in ",__FUNCTION__);
     }
   }
   
  return(booknum);
}
//+------------------------------------------------------------------+
int OrderManager::PlaceTrade(int cmd, double lots, double stop=0.0, double take=0.0, double price=0.0, string comment=NULL)
{
  int i, j, ticket=-1;
  double prc, sl, tp;
  string order_cmt;
    
  if(lots <= 0) {
    Print("Error lots=",lots," in ",__FUNCTION__,": cmd= ",cmd," prc="+DoubleToStr(price,Digits)+" sl="+DoubleToStr(stop,Digits)+" tp="+DoubleToStr(take,Digits));
    return(0);
  }
    
  lots = CheckLots(lots);
   
  for (i=0; i<p_retrycount; i++) {
      for (j=0; (j<50) && IsTradeContextBusy(); j++)
          Sleep(100);
      RefreshRates();
   
      if (cmd == OP_BUY) {
          prc = Ask;
          sl = stop;
          tp = take;
      }
      else if (cmd == OP_SELL) {
          prc = Bid;
          sl = stop;
          tp = take;
      }
      else if (cmd == OP_BUYLIMIT) {
          prc = NormalizeDouble(price, Digits);
          sl = stop;
          tp = take;
      }
      else if (cmd == OP_SELLLIMIT) {
          prc = NormalizeDouble(price, Digits);
          sl = stop;
          tp = take;
      }
      else if( cmd == OP_BUYSTOP) {
          prc = NormalizeDouble(price, Digits);
          sl = stop;
          tp = take;
      }
      else if( cmd == OP_SELLSTOP) {
          prc = NormalizeDouble(price, Digits);
          sl = stop;
          tp = take;
      }
      else {
       Alert("Invalid direction in ",__FUNCTION__);
       return(-1);
      }
        
      Print("Attempt OpenTrade: cmd= ",cmd," prc="+DoubleToStr(prc,Digits)+" sl="+DoubleToStr(sl,Digits)+" tp="+DoubleToStr(tp,Digits));
   
      ticket = OrderSend(Symbol(), cmd, lots, prc, p_slippage, 0, 0, comment, p_magic);
        
      if (ticket != -1 && ( sl > 0 || tp > 0) ) {
          Print("OpenTrade: opened ticket #" + (string)ticket);  
            
          if(OrderSelect( ticket, SELECT_BY_TICKET, MODE_TRADES))
           {   
             for (i=0; i<p_retrycount; i++) {
                 for (j=0; (j<50) && IsTradeContextBusy(); j++)
                     Sleep(100);
                 RefreshRates();
   
                if ( OrderModify(ticket, prc, sl, tp, 0) ) 
                  break;
                
             }
             
             Print("OpenTrade #",ticket,": error \'"+(string)ErrorDescription(GetLastError())+"\' when setting SL/TP");
             ResetLastError();
             Sleep(p_retrydelay);
          }
   
          return (ticket);
      }
      else{      
       if( ticket >= 0 ) {
          return( ticket );
       }
      }
      Print("OpenTrade: error \'"+(string)ErrorDescription(GetLastError())+"\' when entering with "+DoubleToStr(lots,3)+" @"+DoubleToStr(prc,Digits));
      ResetLastError();
      Sleep(p_retrydelay);
  }
   
  Print("OpenTrade: can\'t enter after "+(string)p_retrycount+" retries");
    
  return(ticket);
}
//+------------------------------------------------------------------+
void OrderManager::PartialCloseInfo(int ticket, double& lots, double& realized_pl, CArrayDouble& close_price, int idx=0)
 { 
  if(OrderSelect(ticket,SELECT_BY_TICKET))
   {
    string result[];
    int substr1_count;
    
    substr1_count = StringSplit(OrderComment(),'#',result);
  
    if(substr1_count > 1 && result[0]=="from ")
     {
      if(OrderSelect((int)result[1],SELECT_BY_TICKET))
       {
        int booknum = OrderTicket();
        lots += OrderLots();
        realized_pl += OrderProfit();
        close_price.Add(OrderClosePrice());
        
        PartialCloseInfo(booknum,lots,realized_pl,close_price);
       }
     }
 
    if(substr1_count > 1 && result[0]=="to ")
     {
      int begin = OrdersHistoryTotal()-idx-1;
   
      for(int i=begin; i>=0; i--)
       {
        if(OrderSelect(i,SELECT_BY_POS,MODE_HISTORY))
         {
          int substr2_count = StringSplit(OrderComment(),'#',result);
          
          if(substr2_count > 1 && result[0]=="to " && result[1]==(string)ticket) 
           {
             int booknum = OrderTicket();
             lots += OrderLots();
             realized_pl += OrderProfit();
             close_price.Add(OrderClosePrice());
           
             PartialCloseInfo(booknum,lots,realized_pl,close_price,begin-i);
           }
         }
       }//end for
     }//end if
   }//end if
  else
   {
    lots=0.0;
    realized_pl=0.0;
   }
   
   return;
 }
//+------------------------------------------------------------------+
double OrderManager::CheckLots(double lots)
 {
  double lot, lotmin, lotmax, lotstep, margin;
 
  lotmin = MarketInfo(Symbol(), MODE_MINLOT);
  lotmax = MarketInfo(Symbol(), MODE_MAXLOT);
  lotstep = MarketInfo(Symbol(), MODE_LOTSTEP);
  margin = MarketInfo(Symbol(), MODE_MARGINREQUIRED);

  if (lots*margin > AccountFreeMargin())
      lots = AccountFreeMargin() / margin;

  lot = MathFloor(lots/lotstep + 0.5) * lotstep;

  if (lot < lotmin)
      lot = lotmin;
  if (lot > lotmax)
      lot = lotmax;

  return (NormalizeDouble(lot, 2));
}