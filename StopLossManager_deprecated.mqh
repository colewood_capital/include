//+------------------------------------------------------------------+
//|                                              StopLossManager.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "CARM INC"
#property strict

#ifndef ORDERMANAGEMENT
#define ORDERMANAGEMENT
   #include <OrderManager.mqh>
#endif

#ifndef ARRAYINT
#define ARRAYINT
   #include <Arrays/ArrayInt.mqh>
#endif

#ifndef ARRAYDOUBLE
#define ARRAYDOUBLE
   #include <Arrays/ArrayDouble.mqh>
#endif

#ifndef ARRAYSTRING
#define ARRAYSTRING
   #include <Arrays/ArrayString.mqh>
#endif

#ifndef MQLERRORS
#define MQLERRORS
   #include <MQLErrors.mqh>
#endif

enum ENUM_SL_MODE { SL_MODE_NIL, SL_MODE_FIXED_PIP, SL_MODE_BREAKEVEN, SL_MODE_PERCENTAGE, SL_MODE_MANUAL, SL_MODE_FRACTAL };

class StopLossManager
{
 private:
  OrderManager* p_orders;
  CArrayInt order_queue;
  CArrayInt order_type;
  CArrayInt order_sl_mode;
  CArrayDouble order_trigger_price;
  CArrayInt order_trigger_count;
  CArrayDouble order_deviation;
  CArrayDouble order_sl_pip_dist;
  CArrayInt order_max_times_trigger;
  CArrayDouble order_percentage;
  CArrayDouble order_open_price;
  CArrayString order_obj_name;
  CArrayInt order_monotonic;
  CArrayInt order_ma_period;
  CArrayInt order_update_count;
  
  bool Delete(int ticket);
  double FractalSignal(int type, double fractal_sl_buffer, int ma_period=25);
  bool Triggered(int pos);
  int SetSL(int ticket, int mode, double trigger_price);
  void HandleBreakevenSL();
  
 public:  
  bool SetBreakevenSL(int ticket, double trigger_price, double pip_deviation=0);   //Takes commission into account when setting SL
  bool SetFixedPipTrailingSL(int ticket, double trigger_price, double modify_on_pips_gained, double sl_pip_dist, int max_times=0);
  bool SetPercentTrailingSL(int ticket, double trigger_price, double percent);
  bool SetFractalTrailingSL(int ticket, double trigger_price, double fractal_sl_pip_buffer=0, int ma_period=25, bool mode_monotonic=false);  
  bool SetManualSL(int ticket, string obj_name);
  bool ModifySL(int booknum, double sl); 
  int Update();  //returns the booknum of any order closed by [sl]; else returns -1
  int GetUpdateCount(int ticket);
  
  StopLossManager(OrderManager* orders);
};
//+------------------------------------------------------------------+
StopLossManager::StopLossManager(OrderManager* ptr_orders)
{
  p_orders = ptr_orders;
}
//+------------------------------------------------------------------+
int StopLossManager::GetUpdateCount(int ticket)
{
  int pos = order_queue.SearchLinear(ticket);
  
  if(pos>=0)
   return(order_update_count[pos]);
   
  Alert("Error: could not find ticket=",ticket," in ",__FUNCTION__);
   
  return(-1);
}
//+------------------------------------------------------------------+
double StopLossManager::FractalSignal(int type, double fractal_sl_buffer, int ma_period=25)
{
  string sym = Symbol();
  int shift = 2;
  double ma = iMA(NULL,0,ma_period,0,MODE_SMA,PRICE_CLOSE,shift);
  double frac;

  if(type==OP_BUY)
   {
    frac = iFractals(NULL,0,MODE_LOWER,shift);
    
    if(frac > 0 && frac < ma) { Alert("Trail SL Up!!!");
     return(frac - fractal_sl_buffer); }
   }
  else if(type==OP_SELL)
   {
    frac = iFractals(NULL,0,MODE_UPPER,shift);
    
    if(frac > 0 && frac > ma) { Alert("Trail SL Down!!!");
     return(frac + fractal_sl_buffer); }
   }
  else
   Alert("Error: invalid type=",type," in ",__FUNCTION__);
  
  return(0);
}
//+------------------------------------------------------------------+
bool StopLossManager::SetFractalTrailingSL(int ticket, double trigger_price, double fractal_sl_pip_buffer=0, int ma_period=25, bool mode_monotonic=false)
{
  int pos = SetSL(ticket, SL_MODE_FRACTAL, trigger_price);
  
  if(pos>=0)
   {
    if(order_deviation.Update(pos, fractal_sl_pip_buffer * Point * 10) && order_ma_period.Update(pos, ma_period) && order_monotonic.Update(pos, (int)mode_monotonic))
     return(true);
    else
     {
      Alert("Error: could not update fractal_sl_buffer=",fractal_sl_pip_buffer," ma_period=",ma_period,", mode_monotonic=",mode_monotonic,". pos=",pos," in ",__FUNCTION__);
      return(false);
     }
   }
   
  Alert("Error: Setting SL in ",__FUNCTION__,". ticket=",ticket,", trigger_price=",trigger_price,", fractal_sl_pip_buffer=",fractal_sl_pip_buffer,", ma_period=",ma_period,", mode_monotonic=",mode_monotonic," in ",__FUNCTION__);
   
  return(false);
}
//+------------------------------------------------------------------+
bool StopLossManager::SetManualSL(int ticket,string obj_name)
{
  int pos = SetSL(ticket, SL_MODE_MANUAL, 0);
  
  if(pos>=0)
   {
    if(order_obj_name.Update(pos, obj_name))
     return(true);
    else
     {
      Alert("Error: could not update obj_name=",obj_name,". pos=",pos," in ",__FUNCTION__);
      return(false);
     }
   }
   
  Alert("Error: Setting SL in ",__FUNCTION__,". ticket=",ticket,", obj_name=",obj_name," in ",__FUNCTION__);
   
  return(false);
}
//+------------------------------------------------------------------+
bool StopLossManager::SetPercentTrailingSL(int ticket,double trigger_price,double trailing_percent)
{
  int pos = SetSL(ticket, SL_MODE_PERCENTAGE, trigger_price);
  
  if(pos>=0)
   {
    if(OrderSelect(ticket,SELECT_BY_TICKET))
     {
      if(order_percentage.Update(pos, trailing_percent) && order_open_price.Update(pos, OrderOpenPrice())) 
       return(true); 
      else
       {
        Alert("Error: unable to update ticket #",ticket,". trigger_price=",trigger_price,". trailing_percent=",trailing_percent," in ",__FUNCTION__);
        return(false);
       }
     }
    else
     {
      Alert("Error: unable to select ticket #",ticket," in ",__FUNCTION__);
      return(false);
     } 
   }
   
  Alert("Error: Setting SL in ",__FUNCTION__,". ticket=",ticket,", trigger_price=",trigger_price,". trailing_percent=",trailing_percent," in ",__FUNCTION__);
   
  return(false);
}
//+------------------------------------------------------------------+
bool StopLossManager::SetFixedPipTrailingSL(int ticket, double trigger_price, double modify_on_pips_gained, double sl_pip_dist, int max_times=0)
{
  int pos = SetSL(ticket, SL_MODE_FIXED_PIP, trigger_price);
  
  if(pos>=0)
   {
    if(order_deviation.Update(pos, modify_on_pips_gained * Point * 10) && order_max_times_trigger.Update(pos, max_times) && order_sl_pip_dist.Update(pos, sl_pip_dist * Point * 10)) 
     return(true); 
     
    else
     {
      Alert("Error: unable to update ticket #",ticket,". modify_on_pips_gained=",modify_on_pips_gained,". max_times=",max_times,". sl_pip_dist=",sl_pip_dist," in ",__FUNCTION__);
      return(false);
     }
   }
   
  Alert("Error: Setting SL in ",__FUNCTION__,". ticket=",ticket,", trigger_price=",trigger_price,", modify_on_pips_gained=",modify_on_pips_gained,", max_times=", max_times);
  
  return(false);
}
//+------------------------------------------------------------------+
int StopLossManager::Update()
{ 
  CArrayInt open_trades, pending_trades;
  
  p_orders.GetOpenTrades(open_trades);
  p_orders.GetPendingTrades(pending_trades);
  
  int total = open_trades.Total();
  
  for(int i=total-1; i>=0; i--) 
   { 
     int booknum = open_trades[i];
     
    //--- Check and delete any order from the order_queue[] whose SL has been hit 
     if(pending_trades.SearchLinear(booknum) >= 0 && p_orders.isSLTriggered(booknum))
      {  Alert("A");
       Delete(booknum);
       return(booknum);
      }
      
     int pos = order_queue.SearchLinear(open_trades[i]);
     
     if(pos >= 0)
      {
       //--- Handle any order set to Manual Mode
        if(order_sl_mode[pos]==SL_MODE_MANUAL)
         {
           double mySL = -1;
            
           if(ObjectFind(order_obj_name[pos]) >= 0)
             mySL = NormalizeDouble(ObjectGet(order_obj_name[pos], OBJPROP_PRICE1), Digits);
           else
             ModifySL(order_queue[pos], 0);
             
           if(mySL >= 0 && MathAbs(OrderStopLoss() - mySL) > DBL_EPSILON)
            { 
             int type = order_type[pos];
                
             if(((type==OP_BUY||type==OP_BUYLIMIT||type==OP_BUYSTOP) && mySL < Bid) || ((type==OP_SELL||type==OP_SELLLIMIT||type==OP_SELLSTOP) && mySL > Ask))
              {
                ModifySL(order_queue[pos], mySL);
              }
            }
   
           continue;
         }
   
       //--- Handle any order whose trigger price has been hit
        if(Triggered(pos))
         { 
          switch(order_sl_mode[pos]) 
           {
            case SL_MODE_NIL:          continue;
            
            case SL_MODE_FIXED_PIP:
             {
               double new_sl, new_trigger_price;
               
               if(order_type[pos]==OP_BUY) 
                {
                 new_sl = order_trigger_price[pos] - order_sl_pip_dist[pos];
                 new_trigger_price = order_trigger_price[pos] + order_deviation[pos];
                }
               else if(order_type[pos]==OP_SELL)
                {
                 new_sl = order_trigger_price[pos] + order_sl_pip_dist[pos];
                 new_trigger_price = order_trigger_price[pos] - order_deviation[pos];
                }
               else
                {
                 Alert("Error: invalid type=",order_type[pos]," in ",__FUNCTION__);
                 continue;
                }
     
               ModifySL(order_queue[pos], new_sl);
               
               order_trigger_price.Update(pos, new_trigger_price);
               
               if(order_max_times_trigger[pos] > 0 && order_trigger_count[pos] > order_max_times_trigger[pos]) { Alert("B");
                Delete(order_queue[pos]); }
                
               break;
             }
             
            case SL_MODE_BREAKEVEN:
             {
               int type = -1;
               double commission = 0;
               double tick_value = MarketInfo(Symbol(), MODE_TICKVALUE);
               double open_price, lots;
                
               if(OrderSelect(order_queue[pos],SELECT_BY_TICKET)) 
                {
                 type = OrderType();
                 lots = OrderLots();
                 commission = MathAbs(OrderCommission());
                 open_price = OrderOpenPrice();
                }
               else
                {
                 Alert("Error: could not select ticket #",order_queue[pos]," in ",__FUNCTION__);
                 break;
                }
                 
               if(type==OP_BUY)
                {
                 open_price += MathCeil(commission / (tick_value * lots)) * Point;
                 open_price += order_deviation[pos];
                }
                
               if(type==OP_SELL)
                {
                 open_price -= MathCeil(commission / (tick_value * lots)) * Point;
                 open_price -= order_deviation[pos];
                }
          
               ModifySL(order_queue[pos], open_price); Alert("C");
               Delete(order_queue[pos]);
               
               break;
             }
             
            case SL_MODE_PERCENTAGE:
             {
               double new_sl, new_trigger_price;
               
               if(order_type[pos]==OP_BUY) 
                {
                 new_sl = order_open_price[pos] + ((order_trigger_price[pos]-order_open_price[pos]) * (1 - order_percentage[pos]));
                 new_trigger_price = MathMax(order_trigger_price[pos], Bid);
                }
               else if(order_type[pos]==OP_SELL)
                {
                 new_sl = order_open_price[pos] - ((order_open_price[pos]-order_trigger_price[pos]) * (1 - order_percentage[pos]));
                 new_trigger_price = MathMin(order_trigger_price[pos],Bid);
                }
               else
                {
                 Alert("Error: invalid type=",order_type[pos]," in ",__FUNCTION__);
                 continue;
                }
     
               ModifySL(order_queue[pos], new_sl);
               
               order_trigger_price.Update(pos, new_trigger_price);
              
               break;
             }
             
            case SL_MODE_FRACTAL:
             {
               int type = order_type[pos];
               
               double frac = FractalSignal(type, order_deviation[pos], order_ma_period[pos]);
               
               if(frac > 0)
                {
                  ModifySL(order_queue[pos], frac);
                  
                  if(order_monotonic[pos])
                   order_trigger_price.Update(pos, (Bid+Ask)/2);
                }
             
               break;
             }
                 
            default:
             Alert("Error: could not find 'sl_mode'=",order_sl_mode[pos]," for ticket #",order_queue[pos]," in ",__FUNCTION__);
             
             break;
           }
         }//end if
      }//end if
     else //open_trade[i] not in order_queue
      {
       SetSL(open_trades[i], SL_MODE_NIL, 0); 
      }
   }//end for
   
  return(-1);
}
//+------------------------------------------------------------------+
bool StopLossManager::Delete(int ticket)
 { Alert("WTF!!!");
   int pos = order_queue.SearchLinear(ticket);
   
   if(pos >= 0)
    {
      order_queue.Delete(pos);
      order_type.Delete(pos);
      order_sl_mode.Delete(pos);
      order_trigger_price.Delete(pos);
      order_trigger_count.Delete(pos);
      order_deviation.Delete(pos);
      order_max_times_trigger.Delete(pos);
      order_sl_pip_dist.Delete(pos);
      order_percentage.Delete(pos);
      order_open_price.Delete(pos);
      order_obj_name.Delete(pos);
      order_monotonic.Delete(pos);
      order_ma_period.Delete(pos);
      order_update_count.Delete(pos);
      
      return(true);
    }
    
   Alert("Error: could not delete ticket #",ticket," in ",__FUNCTION__);
   return(false);
 }
//+------------------------------------------------------------------+
bool StopLossManager::SetBreakevenSL(int ticket, double trigger_price, double pip_deviation=0)
{  Alert("HEY in ",__FUNCTION__);
  int pos = SetSL(ticket, SL_MODE_BREAKEVEN, trigger_price);
  
  if(pos>=0)
   {
    if(order_deviation.Update(pos, pip_deviation * Point * 10)) 
     return(true); 
    else
     {
      Alert("Error: unable to update ticket #",ticket,". pip_deviation=",pip_deviation," in ",__FUNCTION__);
      return(false);
     }
   }
  
  if(pos < 0)  
   Alert("Error: Setting SL in ",__FUNCTION__,". ticket=",ticket,", trigger_price=",trigger_price,", pip_deviation=",pip_deviation);
   
  return(false);
}
//+------------------------------------------------------------------+
int StopLossManager::SetSL(int ticket, int mode, double trigger_price)
{
  int type = -1;
 Alert("SetSL ",ticket," - ", mode);
  if(OrderSelect(ticket,SELECT_BY_TICKET)) 
   type = OrderType();
            
  if(p_orders.isOpen(ticket) && type >= 0)
   {
     int pos = order_queue.SearchLinear(ticket);
     
     if(pos >= 0)
      {
        order_trigger_price.Update(pos, trigger_price);
        order_sl_mode.Update(pos, mode);
        order_update_count.Update(pos, order_update_count[pos]+1);
        return(pos);
      }
     else
      {
        order_queue.Add(ticket);
        order_type.Add(type);
        order_sl_mode.Add(mode); 
        order_trigger_price.Add(trigger_price);
        order_trigger_count.Add(0);
        order_deviation.Add(0);
        order_max_times_trigger.Add(0);
        order_sl_pip_dist.Add(0);
        order_percentage.Add(0);
        order_open_price.Add(0);
        order_obj_name.Add(NULL);
        order_monotonic.Add(0);
        order_ma_period.Add(0);
        order_update_count.Add(0);
        return(order_queue.Total()-1);
      }  
   }
  else
   {
    Alert("Error: unable to set SL in ",__FUNCTION__,", ticket=",ticket,", mode=",mode,", trigger_price: ",trigger_price,", type=",type);
    return(-1);
   }
}
//+-----------------------------------------------------------------+
bool StopLossManager::ModifySL(int booknum, double sl)
{
  if(OrderSelect(booknum,SELECT_BY_TICKET)) 
   {
    if(MathAbs(sl-OrderStopLoss()) > DBL_EPSILON)
     {
      if(OrderModify(booknum,OrderOpenPrice(),sl,OrderTakeProfit(),0))
       {
         return(true);
       }
      else
       {
         Alert("Error: could not modify ticket #", booknum," for e: ", ErrorDescription(GetLastError())," in ",__FUNCTION__);
         ResetLastError();
         return(false);
       }
     }
    else return(false);
   }
   else
   {
     Alert("Error: could not select ticket #", booknum," for e: ", ErrorDescription(GetLastError())," in ",__FUNCTION__);
     ResetLastError();
     return(false);
   }
}
//+------------------------------------------------------------------+
bool StopLossManager::Triggered(int pos)
{
 switch(order_type[pos])
  {
   case OP_BUY:
    {
     if(order_trigger_price[pos] > 0 && High[0] >= order_trigger_price[pos]) 
      {
        order_trigger_count.Update(pos, order_trigger_count[pos] + 1);
        return(true);
      }
     
     break;
    }
       
   case OP_SELL:
    {
     if(order_trigger_price[pos] > 0 && Low[0] <= order_trigger_price[pos]) 
      {
        order_trigger_count.Update(pos, order_trigger_count[pos] + 1);
        return(true);
      }
     
     break;
    }  
      
   default:
      Alert("Error: invalid direction in ",__FUNCTION__);

 }//end switch

  
 return(false);
}