//+------------------------------------------------------------------+
//|                                                        Algos.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#ifndef EVENTS
#define EVENTS
   #include <Packages/Events.mqh>
#endif

#ifndef LOGGER
#define LOGGER
   #include <Logger.mqh>
#endif

enum EVENT_BAR {
   EVENT_BAR_NIL,
   EVENT_BAR_WEAK
};

enum EVENT_BOLLINGER {
   EVENT_BOLLINGER_NIL,
   EVENT_BOLLINGER_OPEN_FIRST,
   EVENT_BOLLINGER_OPEN_SUBSEQUENT,
   EVENT_BOLLINGER_RESET
};

Logger algoslogger(__FILE__, "helper_processes");

input int ALGO_BOLLINGER_PERIOD = 50;
input double ALGO_BOLLINGER_DEVIATION = 2.5;

EVENT_BOLLINGER AlgoOpenOnBollinger(double initial_touch_level, double reset_level, int max_open_orders, int& side)
{
 static int open_orders = 0;
 static int initial_side = -1;
 static datetime initial_trade_timestamp;
 double boll_ma=iBands(NULL, 0, ALGO_BOLLINGER_PERIOD, ALGO_BOLLINGER_DEVIATION, 0, PRICE_CLOSE, MODE_MAIN, 0);
 double boll_upper=iBands(NULL, 0, ALGO_BOLLINGER_PERIOD, ALGO_BOLLINGER_DEVIATION, 0, PRICE_CLOSE, MODE_UPPER, 0);
 double boll_lower=iBands(NULL, 0, ALGO_BOLLINGER_PERIOD, ALGO_BOLLINGER_DEVIATION, 0, PRICE_CLOSE, MODE_LOWER, 0);
 
 //--- Check for reset
 if(open_orders > 0)
 {
  double reset_price = (boll_upper * reset_level) + (boll_lower * (1-reset_level));
  bool reset = false;
  
  if(initial_side==OP_SELL)
  {
   if(Bid < reset_price)
    reset = true;
  }
  
  if(initial_side==OP_BUY)
  {
   if(Bid > reset_price)
    reset = true;
  }
  
  if(reset)
  {
   initial_side = -1;
   open_orders = 0;
   initial_trade_timestamp = 0;
   
   algoslogger.log("Resetting function variables: inital_side=" + (string)initial_side + ", open_orders=" + (string)open_orders + ", initial_trade_timestamp=" + TimeToStr(initial_trade_timestamp, TIME_DATE|TIME_MINUTES|TIME_SECONDS) + ", reset_price=" + DoubleToStr(reset_price, Digits), LOG_DEBUG, __LINE__, __FUNCTION__);
   return EVENT_BOLLINGER_RESET;
  }
 }
 
 if(open_orders < max_open_orders)
 {
  //--- Check for initial open
  if(open_orders==0)
  {
   double bollinger_touch_level;
   
   if(Bid > boll_ma)
   {
    bollinger_touch_level = (boll_upper * initial_touch_level) + (boll_lower * (1-initial_touch_level));
    
    if(Bid >= bollinger_touch_level)
    {
     side = OP_SELL;
     algoslogger.log("returning " + EnumToString(EVENT_BOLLINGER_OPEN_FIRST) + ", side=OP_SELL, bollinger_touch_level=" + DoubleToStr(bollinger_touch_level, Digits), LOG_DEBUG, __LINE__, __FUNCTION__);
     initial_side = side;
     initial_trade_timestamp = Time[0];
     open_orders++;
     return EVENT_BOLLINGER_OPEN_FIRST;
    }
   }
   
   if(Bid < boll_ma)
   {
    bollinger_touch_level = (boll_upper * (1-initial_touch_level)) + (boll_lower * initial_touch_level);
    
    if(Bid <= bollinger_touch_level)
    {
     side = OP_BUY;
     algoslogger.log("returning " + EnumToString(EVENT_BOLLINGER_OPEN_FIRST) + ", side=OP_BUY, bollinger_touch_level=" + DoubleToStr(bollinger_touch_level, Digits), LOG_DEBUG, __LINE__, __FUNCTION__);
     initial_side = side;
     initial_trade_timestamp = Time[0];
     open_orders++;
     return EVENT_BOLLINGER_OPEN_FIRST;
    }
   }
  }
  //--- Check for subsequent opens
  else
  {
   if(NewBar())
   {
    int count = iBarShift(NULL, 0, initial_trade_timestamp);
    bool open_subsequent_order = false;
    
    if(initial_side==OP_SELL)
    {
     double highest = (count >= 2) ? High[iHighest(NULL, 0, MODE_HIGH, count-2, 2)] : 0;
     
     if(highest && Close[1] > highest)
      open_subsequent_order = true;
     
     if(highest)
      algoslogger.log("Open if Close(1)=" + DoubleToStr(Close[1], Digits) + " > " + DoubleToStr(highest, Digits) + " [Highest high since initial open]", LOG_DEBUG, __LINE__, __FUNCTION__);
    }
    
    if(initial_side==OP_BUY)
    { 
     double lowest = (count >= 2) ? Low[iLowest(NULL, 0, MODE_LOW, count-2, 2)] : 0;
     
     if(lowest && Close[1] < lowest)
      open_subsequent_order = true;
     
     if(lowest)
      algoslogger.log("Open if Close(1)=" + DoubleToStr(Close[1], Digits) + " < " + DoubleToStr(lowest, Digits) + " [Lowest low since initial open]", LOG_DEBUG, __LINE__, __FUNCTION__);
    }
       
    if(open_subsequent_order)
    {
     side = initial_side;
     string print_side = (side==OP_BUY) ? "OP_BUY" : (side==OP_SELL) ? "OP_SELL" : "?";
     
     open_orders++;
     
     if(open_orders==max_open_orders)
      algoslogger.log("Max open orders = " + (string)max_open_orders + " has been reached", LOG_INFO, __LINE__, __FUNCTION__);
      
     algoslogger.log("returning " + EnumToString(EVENT_BOLLINGER_OPEN_SUBSEQUENT) + ", side=" + print_side, LOG_DEBUG, __LINE__, __FUNCTION__);
    
     return EVENT_BOLLINGER_OPEN_SUBSEQUENT;
    } 
   }  
  }
 }
 
 return EVENT_BOLLINGER_NIL;
}

EVENT_BAR AlgoCloseOnWeakBar(int cmd, int period=NULL)
{
 if(NewBar())
 {
  algoslogger.log("New Bar, checking previous bar close", LOG_DEBUG, __LINE__, __FUNCTION__);
  
  if(cmd==OP_BUY)
  {
   if(Open[1] > Close[1])
   {
    algoslogger.log("[BEARISH] returning " + EnumToString(EVENT_BAR_WEAK), LOG_DEBUG, __LINE__, __FUNCTION__);
    return EVENT_BAR_WEAK;
   }
  }
  else if(cmd==OP_SELL)
  {
   if(Open[1] < Close[1])
   {
    algoslogger.log("[BULLISH] returning " + EnumToString(EVENT_BAR_WEAK), LOG_DEBUG, __LINE__, __FUNCTION__);
    return EVENT_BAR_WEAK;
   }
  }
  else
   algoslogger.log("cmd=" + (string)cmd + " not supported", LOG_PRIORITY_HIGH, __LINE__, __FUNCTION__);
 }

 return EVENT_BAR_NIL;
}