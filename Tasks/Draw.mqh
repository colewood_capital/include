//+------------------------------------------------------------------+
//|                                                         Draw.mqh |
//|                        Copyright 2014, MetaQuotes Software Corp. |
//|                                              http://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, MetaQuotes Software Corp."
#property link      "http://www.mql5.com"
#property strict

#ifndef ARRAYDOUBLE
#define ARRAYDOUBLE
   #include <Arrays/ArrayDouble.mqh>
#endif 

#ifndef LOGGER
#define LOGGER
   #define LOGGER_DEPRECATED
   #include <Logger.mqh>
#endif 

#ifndef MAP
#define MAP
   #include <Map.mqh>
#endif

#define OBJ_INDEXING_DELIMITER         " - "

Logger drawlogger(__FILE__, "helper_processes");
//+------------------------------------------------------------------+
// [IN] prefix to search for when deleting grid lines
// [OUT] void - deletes grid
void DeleteGrid(long chart_id, string name_prefix)
{
 int total = ObjectsTotal(chart_id);
 
 for(int i=total; i>=0; i--)
 {
  string name = ObjectName(chart_id, i);
  
  if(StringFind(name, name_prefix) == 0)
   ObjectDelete(chart_id, name);
 }
}
//+------------------------------------------------------------------+
// [IN] name_prefix - for grid whose indexing will be modified
// [OUT] void - starting for start_index, will shift each grid line's index down by one
//                useful when a grid line is manually deleted by user
void ShiftGridIndexingDown(long chart_id, string name_prefix, int start_index)//, bool delete_start_index=false)
{
 int total = ObjectsTotal(chart_id);
 Map<int,string> grid;
 
 if(start_index < 0)
 {
  drawlogger.log("start_index < 0 cannot be shifted down. Aborting ... ", LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
  return;
 }

 for(int i=total-1; i>=0; i--)
 {
  string name = ObjectName(chart_id, i);
  
  if(StringFind(name, name_prefix)==0)
  {
   string result[];
   StringSplit(name, '-', result);
   int last_index = ArraySize(result) - 1;
   int pos = (int)result[last_index];
   
   if(pos >= start_index)
   {
    drawlogger.log("obj_name: \"" + name + "\" -> pos=" + (string)pos, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
    grid.set(pos, name);
   }
  }
 }
 
 int index[]; string grid_name[];

 int size = grid.copy(index, grid_name);
 
 if(size > 0)
 {
  ArraySort(index, WHOLE_ARRAY, 0, MODE_ASCEND);
 
  for(int i=0; i<size; i++)
  {
   int k = index[i];
   
   string modified_name = StringConcatenate(name_prefix, OBJ_INDEXING_DELIMITER, k-1);
   
   if(ObjectSetString(chart_id, grid[k], OBJPROP_NAME, modified_name))
    drawlogger.log("Shifted " + name_prefix + " index from " + (string)k + " DOWN to " + (string)(k-1), LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
   else
    drawlogger.log("Unable to change name: error shifting " + name_prefix + " index from " + (string)k + " DOWN to " + (string)(k-1) + ", e=" + (string)GetLastError(), LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
  }
 }
 else
  drawlogger.log("Nothing to shift: name_prefix=" + name_prefix + " size = 0", LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
}
//+------------------------------------------------------------------+
// [IN] name_prefix - for each level in grid
//      new_prc - prices at which to draw the horizontal lines
// [OUT] void - 1. deletes any old grid
//              2. draws new grid based off new_prc
void DrawGrid(long chart_id, string name_prefix, const CArrayDouble& new_prc, const CArrayString* new_description, const color clr=clrRed, const int name_starting_index=1)
{
 //--- Compare object names to be drawn with objects already drawn on chart
 int new_prc_total = new_prc.Total();
 int k = name_starting_index;
 Alert("tot: ", new_prc_total);
 for(int i=0; i<new_prc_total; i++)
 {
  string new_obj_name = StringConcatenate(name_prefix, OBJ_INDEXING_DELIMITER, i);
  Alert(i, ": ", new_obj_name);
  if(ObjectFind(chart_id, new_obj_name)==0)
  {
   double existing_obj_prc = ObjectGetDouble(chart_id, new_obj_name, OBJPROP_PRICE1);
   
   if(MathAbs(existing_obj_prc - new_prc[i]) > DBL_EPSILON)
   {
    if(ObjectSetDouble(chart_id, new_obj_name, OBJPROP_PRICE1, new_prc[i]))
    {
     drawlogger.log("Changed price of obj_name=" + new_obj_name + " from " + DoubleToStr(existing_obj_prc, Digits) + " to " + DoubleToStr(new_prc[i], Digits), LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
    }
    else
     drawlogger.log("Error changing the price of obj_name=" + new_obj_name + " from " + DoubleToStr(existing_obj_prc, Digits) + " to " + DoubleToStr(new_prc[i], Digits), LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
   }
   else
    drawlogger.log("Do nothing: obj_name=" + new_obj_name + " with prc=" + DoubleToStr(new_prc[i], Digits) + " already exists", LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
  }
  else
  {
   //--- Object name doesn't already exist, draw new object
   if(ObjectCreate(chart_id, new_obj_name, OBJ_HLINE, 0, 0, new_prc[i])) 
   {
    ObjectSetInteger(chart_id, new_obj_name, OBJPROP_COLOR, clr);
    
    if(new_description[i])
     ObjectSetString(chart_id, new_obj_name, OBJPROP_TEXT, new_description[i]);
    
    drawlogger.log("Created new level, name = " + new_obj_name, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
   }
  else
   drawlogger.log("Error drawing hline, name=" + new_obj_name + ",e= " + (string)GetLastError(), LOG_TYPE_HIGH, __LINE__, __FUNCTION__); 
  }
 }
}
//+------------------------------------------------------------------+
// [IN] name_prefix - for each level in grid
//      new_prc - prices at which to draw the horizontal lines
// [OUT] void - 1. deletes any old grid
//              2. draws new grid based off new_prc
void DrawGrid(long chart_id, string name_prefix, const CArrayDouble& new_prc, const color clr=clrRed, const int name_starting_index=1)
{
 //--- Compare object names to be drawn with objects already drawn on chart
 int new_prc_total = new_prc.Total();
 int k = name_starting_index;
 
 for(int i=0; i<new_prc_total; i++)
 {
  string new_obj_name = StringConcatenate(name_prefix, OBJ_INDEXING_DELIMITER, i);
  
  if(ObjectFind(chart_id, new_obj_name)==0)
  {
   double existing_obj_prc = ObjectGetDouble(chart_id, new_obj_name, OBJPROP_PRICE1);
   
   if(MathAbs(existing_obj_prc - new_prc[i]) > DBL_EPSILON)
   {
    if(ObjectSetDouble(chart_id, new_obj_name, OBJPROP_PRICE1, new_prc[i]))
    {
     drawlogger.log("Changed price of obj_name=" + new_obj_name + " from " + DoubleToStr(existing_obj_prc, Digits) + " to " + DoubleToStr(new_prc[i], Digits), LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
    }
    else
     drawlogger.log("Error changing the price of obj_name=" + new_obj_name + " from " + DoubleToStr(existing_obj_prc, Digits) + " to " + DoubleToStr(new_prc[i], Digits), LOG_TYPE_HIGH, __LINE__, __FUNCTION__);
   }
   else
    drawlogger.log("Do nothing: obj_name=" + new_obj_name + " with prc=" + DoubleToStr(new_prc[i], Digits) + " already exists", LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
  }
  else
  {
   //--- Object name doesn't already exist, draw new object
   if(ObjectCreate(chart_id, new_obj_name, OBJ_HLINE, 0, 0, new_prc[i])) 
   {
    ObjectSetInteger(chart_id, new_obj_name, OBJPROP_COLOR, clr);
     
    drawlogger.log("Created new level, name = " + new_obj_name, LOG_TYPE_DEBUG, __LINE__, __FUNCTION__);
   }
  else
   drawlogger.log("Error drawing hline, name=" + new_obj_name + ",e= " + (string)GetLastError(), LOG_TYPE_HIGH, __LINE__, __FUNCTION__); 
  }
 }
}
//+------------------------------------------------------------------+
bool HLineCreate(const string          name="HLine",           // line name
                 double                price=0,                // line price
                 const color           clr=clrYellow,          // line color
                 const ENUM_LINE_STYLE style=STYLE_DASHDOTDOT, // line style
                 const int             width=1,                // line width
                 const bool            back=false,             // in the background
                 const bool            selection=true,         // highlight to move
                 const bool            hidden=true,            // hidden in the object list
                 const long            chart_ID=0,             // chart's ID
                 const int             sub_window=0,           // subwindow index
                 const long            z_order=0)              // priority for mouse click
  {
//--- if the price is not set, set it at the current Bid price level
   if(!price)
      price=SymbolInfoDouble(Symbol(),SYMBOL_BID);
//--- reset the error value
   ResetLastError();
//--- create a horizontal line
   if(!ObjectCreate(chart_ID,name,OBJ_HLINE,sub_window,0,price))
     {
      Print(__FUNCTION__,
            ": failed to create a horizontal line! Error code = ",GetLastError());
      return(false);
     }
//--- set line color
   ObjectSetInteger(chart_ID,name,OBJPROP_COLOR,clr);
//--- set line display style
   ObjectSetInteger(chart_ID,name,OBJPROP_STYLE,style);
//--- set line width
   ObjectSetInteger(chart_ID,name,OBJPROP_WIDTH,width);
//--- display in the foreground (false) or background (true)
   ObjectSetInteger(chart_ID,name,OBJPROP_BACK,back);
//--- enable (true) or disable (false) the mode of moving the line by mouse
//--- when creating a graphical object using ObjectCreate function, the object cannot be
//--- highlighted and moved by default. Inside this method, selection parameter
//--- is true by default making it possible to highlight and move the object
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTABLE,selection);
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTED,selection);
//--- hide (true) or display (false) graphical object name in the object list
   ObjectSetInteger(chart_ID,name,OBJPROP_HIDDEN,hidden);
//--- set the priority for receiving the event of a mouse click in the chart
   ObjectSetInteger(chart_ID,name,OBJPROP_ZORDER,z_order);
//--- successful execution
   return(true);
  }
//+------------------------------------------------------------------+
//| Create the button                                                |
//+------------------------------------------------------------------+
bool ButtonCreate(const long              chart_ID=0,               // chart's ID
                  const string            name="Button",            // button name
                  const int               sub_window=0,             // subwindow index
                  const int               x=0,                      // X coordinate
                  const int               y=0,                      // Y coordinate
                  const int               width=50,                 // button width
                  const int               height=18,                // button height
                  const ENUM_BASE_CORNER  corner=CORNER_LEFT_UPPER, // chart corner for anchoring
                  const string            text="Button",            // text
                  const string            font="Arial",             // font
                  const int               font_size=10,             // font size
                  const color             clr=clrBlack,             // text color
                  const color             back_clr=C'236,233,216',  // background color
                  const color             border_clr=clrNONE,       // border color
                  const bool              state=false,              // pressed/released
                  const bool              back=false,               // in the background
                  const bool              selection=false,          // highlight to move
                  const bool              hidden=true,              // hidden in the object list
                  const long              z_order=0)                // priority for mouse click
  {
//--- reset the error value
   ResetLastError();
//--- create the button
   if(!ObjectCreate(chart_ID,name,OBJ_BUTTON,sub_window,0,0))
     {
      Print(__FUNCTION__,
            ": failed to create the button! Error code = ",GetLastError());
      return(false);
     }
//--- set button coordinates
   ObjectSetInteger(chart_ID,name,OBJPROP_XDISTANCE,x);
   ObjectSetInteger(chart_ID,name,OBJPROP_YDISTANCE,y);
//--- set button size
   ObjectSetInteger(chart_ID,name,OBJPROP_XSIZE,width);
   ObjectSetInteger(chart_ID,name,OBJPROP_YSIZE,height);
//--- set the chart's corner, relative to which point coordinates are defined
   ObjectSetInteger(chart_ID,name,OBJPROP_CORNER,corner);
//--- set the text
   ObjectSetString(chart_ID,name,OBJPROP_TEXT,text);
//--- set text font
   ObjectSetString(chart_ID,name,OBJPROP_FONT,font);
//--- set font size
   ObjectSetInteger(chart_ID,name,OBJPROP_FONTSIZE,font_size);
//--- set text color
   ObjectSetInteger(chart_ID,name,OBJPROP_COLOR,clr);
//--- set background color
   ObjectSetInteger(chart_ID,name,OBJPROP_BGCOLOR,back_clr);
//--- set border color
   ObjectSetInteger(chart_ID,name,OBJPROP_BORDER_COLOR,border_clr);
//--- display in the foreground (false) or background (true)
   ObjectSetInteger(chart_ID,name,OBJPROP_BACK,back);
//--- set button state
   ObjectSetInteger(chart_ID,name,OBJPROP_STATE,state);
//--- enable (true) or disable (false) the mode of moving the button by mouse
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTABLE,selection);
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTED,selection);
//--- hide (true) or display (false) graphical object name in the object list
   ObjectSetInteger(chart_ID,name,OBJPROP_HIDDEN,hidden);
//--- set the priority for receiving the event of a mouse click in the chart
   ObjectSetInteger(chart_ID,name,OBJPROP_ZORDER,z_order);
//--- successful execution
   return(true);
  }
//+------------------------------------------------------------------+
//| Move the button                                                  |
//+------------------------------------------------------------------+
bool ButtonMove(const long   chart_ID=0,    // chart's ID
                const string name="Button", // button name
                const int    x=0,           // X coordinate
                const int    y=0)           // Y coordinate
  {
//--- reset the error value
   ResetLastError();
//--- move the button
   if(!ObjectSetInteger(chart_ID,name,OBJPROP_XDISTANCE,x))
     {
      Print(__FUNCTION__,
            ": failed to move X coordinate of the button! Error code = ",GetLastError());
      return(false);
     }
   if(!ObjectSetInteger(chart_ID,name,OBJPROP_YDISTANCE,y))
     {
      Print(__FUNCTION__,
            ": failed to move Y coordinate of the button! Error code = ",GetLastError());
      return(false);
     }
//--- successful execution
   return(true);
  }
//+------------------------------------------------------------------+
//| Change button size                                               |
//+------------------------------------------------------------------+
bool ButtonChangeSize(const long   chart_ID=0,    // chart's ID
                      const string name="Button", // button name
                      const int    width=50,      // button width
                      const int    height=18)     // button height
  {
//--- reset the error value
   ResetLastError();
//--- change the button size
   if(!ObjectSetInteger(chart_ID,name,OBJPROP_XSIZE,width))
     {
      Print(__FUNCTION__,
            ": failed to change the button width! Error code = ",GetLastError());
      return(false);
     }
   if(!ObjectSetInteger(chart_ID,name,OBJPROP_YSIZE,height))
     {
      Print(__FUNCTION__,
            ": failed to change the button height! Error code = ",GetLastError());
      return(false);
     }
//--- successful execution
   return(true);
  }
//+------------------------------------------------------------------+
//| Change corner of the chart for binding the button                |
//+------------------------------------------------------------------+
bool ButtonChangeCorner(const long             chart_ID=0,               // chart's ID
                        const string           name="Button",            // button name
                        const ENUM_BASE_CORNER corner=CORNER_LEFT_UPPER) // chart corner for anchoring
  {
//--- reset the error value
   ResetLastError();
//--- change anchor corner
   if(!ObjectSetInteger(chart_ID,name,OBJPROP_CORNER,corner))
     {
      Print(__FUNCTION__,
            ": failed to change the anchor corner! Error code = ",GetLastError());
      return(false);
     }
//--- successful execution
   return(true);
  }
//+------------------------------------------------------------------+
//| Change button text                                               |
//+------------------------------------------------------------------+
bool ButtonTextChange(const long   chart_ID=0,    // chart's ID
                      const string name="Button", // button name
                      const string text="Text")   // text
  {
//--- reset the error value
   ResetLastError();
//--- change object text
   if(!ObjectSetString(chart_ID,name,OBJPROP_TEXT,text))
     {
      Print(__FUNCTION__,
            ": failed to change the text! Error code = ",GetLastError());
      return(false);
     }
//--- successful execution
   return(true);
  }
//+------------------------------------------------------------------+
//| Delete the button                                                |
//+------------------------------------------------------------------+
bool ButtonDelete(const long   chart_ID=0,    // chart's ID
                  const string name="Button") // button name
  {
//--- reset the error value
   ResetLastError();
//--- delete the button
   if(!ObjectDelete(chart_ID,name))
     {
      Print(__FUNCTION__,
            ": failed to delete the button! Error code = ",GetLastError());
      return(false);
     }
//--- successful execution
   return(true);
  }